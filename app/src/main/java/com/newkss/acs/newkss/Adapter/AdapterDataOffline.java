package com.newkss.acs.newkss.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newkss.acs.newkss.Model.Pending;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Function;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by acs on 1/16/18.
 */

public class AdapterDataOffline extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<Pending> pendingList = null;
    String namaDeb = "";
    String noLoan = "";
    String noRek = "";
    String nominalBayar = "";
    String totalKewajiban = "";
    String tglJanjiBayar = "";
    String nomJanjiBayar = "";
    String tglKunjunganPending = "";
    String kodeKunjungan = "";
    String actionCode = "";
    String status = "";
    String descStatus = "";
    String keterangan = "";

    public AdapterDataOffline(Context context, ArrayList<Pending> list) {
        mContext = context;
        pendingList = list;
        inflater = LayoutInflater.from(mContext);

    }

    @Override
    public int getCount() {
        return pendingList.size();
    }

    @Override
    public Object getItem(int i) {
        return pendingList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder {
        TextView txtNoLoan;
        TextView txtNoRekening;
        TextView txtNama;
        TextView txtNominalBayar;
        TextView txtTotalKewajiban;
        TextView txtTanggalJanjiBayar;
        TextView txtNominalJanjiBayar;
        TextView txtTanggalKunjungan;
        TextView txtKodeKunjungan;
        TextView txtStatus;
        TextView txtActionCode;
        TextView txtKeterangan;

    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;

        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listdataoffline, viewGroup, false);

            holder.txtNoLoan = (TextView) view.findViewById(R.id.txtNoLoan);
            holder.txtNoRekening = (TextView) view.findViewById(R.id.txtNoRekening);
            holder.txtNama = (TextView) view.findViewById(R.id.txtNama);
            holder.txtNominalBayar = (TextView) view.findViewById(R.id.txtNominalBayar);
            holder.txtTotalKewajiban = (TextView) view.findViewById(R.id.txtTotalKewajiban);
            holder.txtNominalJanjiBayar = (TextView) view.findViewById(R.id.txtNominalJanjiBayar);
            holder.txtTanggalKunjungan = (TextView) view.findViewById(R.id.txtTanggalKunjungan);
            holder.txtKodeKunjungan = (TextView) view.findViewById(R.id.txtKodeKunjungan);
            holder.txtStatus = (TextView) view.findViewById(R.id.txtStatus);
            holder.txtActionCode = (TextView) view.findViewById(R.id.txtActionCode);
            holder.txtKeterangan = (TextView) view.findViewById(R.id.txtKeterangan);
            holder.txtTanggalJanjiBayar = (TextView) view.findViewById(R.id.txtTanggalJanjiBayar);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        try {
            JSONObject jsonObject = new JSONObject(pendingList.get(i).getData());
            JSONObject jsonObject1 = new JSONObject(pendingList.get(i).getJson_update());
            JSONArray jsonArray = jsonObject.getJSONArray("hasil_kunjungan");

            noRek = jsonObject1.getString("no_rek");
            totalKewajiban = jsonObject1.getString("total_kewajiban");
            kodeKunjungan = jsonObject1.getString("kode_kunjungan");
            status = jsonObject1.getString("status");

            if (status.equals("0")) {
                descStatus = "Belum Dikirim";
            } else if (status.equals("1")) {
                descStatus = "Sukses Terkirim";
            } else if (status.equals("2")) {
                descStatus = "Gagal Dikirm";
            }
            holder.txtStatus.setText(descStatus);

            holder.txtNoRekening.setText(noRek);
            holder.txtTotalKewajiban.setText(Function.returnSeparatorComa(totalKewajiban));
            holder.txtKodeKunjungan.setText(kodeKunjungan);
            for (int in = 0; in < jsonArray.length(); in++) {
                JSONObject jsonObj = jsonArray.getJSONObject(in);
                namaDeb = jsonObj.getString("nama_debitur");

               // String nama = pendingList.get(i).getData();
                String namaCut = "";
                if (namaDeb.length() <= 14) {
                    namaCut = namaDeb.substring(0, namaDeb.length());
                } else if (namaDeb.length() > 14) {
                    namaCut = namaDeb.substring(0, 14);
                }


                noLoan = jsonObj.getString("no_loan");
                nominalBayar = jsonObj.getString("nominal_bayar");
                nomJanjiBayar = jsonObj.getString("nominal_janji_bayar");
                tglJanjiBayar = jsonObj.getString("tgl_janji_bayar");
                tglKunjunganPending = jsonObj.getString("waktu_survey");
                actionCode = jsonObj.getString("action_code");
                if (jsonObj.has("keterangan")) {
                    keterangan = jsonObj.getString("keterangan");
                    holder.txtKeterangan.setText(keterangan);
                }
                holder.txtNama.setText(namaCut);
                holder.txtNoLoan.setText(noLoan);
                holder.txtNominalBayar.setText(Function.returnSeparatorComa(nominalBayar));
                holder.txtNominalJanjiBayar.setText(Function.returnSeparatorComa(nomJanjiBayar));
                holder.txtTanggalJanjiBayar.setText(tglJanjiBayar);
                holder.txtTanggalKunjungan.setText(tglKunjunganPending);
                holder.txtActionCode.setText(actionCode);

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


//        holder.noLoan.setText(":" + pendingList.get(i).getDataPending());
//        holder.nama.setText(":" + namaCut);
//        holder.norek.setText(":" + pendingList.get(i).getDataPending());
//        holder.nominal.setText(":" + Function.returnSeparatorComaWithout(pendingList.get(i).getHeader()));

        return view;
    }


//    public void filter(String charText) {
//        charText = charText.toLowerCase(Locale.getDefault());
//        settlementList.clear();
//        if (charText.length() == 0) {
//            settlementList.addAll(arraylist);
//        } else {
//            for (Settle_Detil wp : arraylist) {
//                if (wp.getNama().toLowerCase(Locale.getDefault()).contains(charText)) {
//                    settlementList.add(wp);
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }

}
