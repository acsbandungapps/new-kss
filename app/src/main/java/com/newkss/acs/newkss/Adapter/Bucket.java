package com.newkss.acs.newkss.Adapter;

import java.io.Serializable;

/**
 * Created by acs on 21/04/17.
 */

public class Bucket implements Serializable {
    public String Nama;
    public String Cycle;
    public String OS;
    public String TotalBayar;
    public String Alamat;
    public boolean box;

    public String DataBucket;
    public String TotalKewajiban;
    public String DPD;

    public String getDataBucket() {
        return DataBucket;
    }

    public void setDataBucket(String dataBucket) {
        DataBucket = dataBucket;
    }

    public String getTotalKewajiban() {
        return TotalKewajiban;
    }

    public void setTotalKewajiban(String totalKewajiban) {
        TotalKewajiban = totalKewajiban;
    }

    public String getDPD() {
        return DPD;
    }

    public void setDPD(String DPD) {
        this.DPD = DPD;
    }

    public Bucket(String nama, String cycle, String OS, String totalBayar, String alamat) {
        Nama = nama;
        Cycle = cycle;
        this.OS = OS;
        TotalBayar = totalBayar;
        Alamat = alamat;
    }

    public Bucket(String nama, String cycle, String OS, String totalBayar, String alamat,String databucket,String totalKewajiban,String dpd) {
        Nama = nama;
        Cycle = cycle;
        this.OS = OS;
        TotalBayar = totalBayar;
        Alamat = alamat;
        TotalKewajiban=totalKewajiban;
        DataBucket=databucket;
        DPD=dpd;
    }



    public Bucket(){

    }

    public boolean isBox() {
        return box;
    }

    public void setBox(boolean box) {
        this.box = box;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getCycle() {
        return Cycle;
    }

    public void setCycle(String cycle) {
        Cycle = cycle;
    }

    public String getOS() {
        return OS;
    }

    public void setOS(String OS) {
        this.OS = OS;
    }

    public String getTotalBayar() {
        return TotalBayar;
    }

    public void setTotalBayar(String totalBayar) {
        TotalBayar = totalBayar;
    }

    public String getAlamat() {
        return Alamat;
    }

    public void setAlamat(String alamat) {
        Alamat = alamat;
    }
}
