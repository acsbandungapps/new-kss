package com.newkss.acs.newkss.Adapter;

/**
 * Created by acs on 27/04/17.
 */

public class BucketSelected {
    public String Nama;
    public String DataBucket;
    public String OS;
    public String Cycle;
    public String TotalKewajiban;
    public String DPD;
    public String Alamat;

    public BucketSelected(String nama, String DataBucket, String OS, String cycle, String totalKewajiban, String dpd, String alamat) {
        this.Nama = nama;
        this.DataBucket = DataBucket;
        this.OS = OS;
        this.Cycle = cycle;
        this.TotalKewajiban = totalKewajiban;
        this.DPD = dpd;
        this.Alamat = alamat;
    }

    public BucketSelected() {

    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getDataBucket() {
        return DataBucket;
    }

    public void setDataBucket(String dataBucket) {
        DataBucket = dataBucket;
    }

    public String getOS() {
        return OS;
    }

    public void setOS(String OS) {
        this.OS = OS;
    }

    public String getCycle() {
        return Cycle;
    }

    public void setCycle(String cycle) {
        Cycle = cycle;
    }

    public String getTotalKewajiban() {
        return TotalKewajiban;
    }

    public void setTotalKewajiban(String totalKewajiban) {
        TotalKewajiban = totalKewajiban;
    }

    public String getDPD() {
        return DPD;
    }

    public void setDPD(String DPD) {
        this.DPD = DPD;
    }

    public String getAlamat() {
        return Alamat;
    }

    public void setAlamat(String alamat) {
        Alamat = alamat;
    }
}
