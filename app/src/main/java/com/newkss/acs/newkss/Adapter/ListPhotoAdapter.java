package com.newkss.acs.newkss.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.newkss.acs.newkss.Fragment.FragmentPhoto;
import com.newkss.acs.newkss.MenuPhoto.ListPhoto;
import com.newkss.acs.newkss.Model.Detail_Bucket;
import com.newkss.acs.newkss.Model.Photo;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by acs on 6/12/17.
 */

public class ListPhotoAdapter extends BaseAdapter {


    List<Photo> listPhoto = null;
    Context mContext;
    LayoutInflater inflater;

    public ListPhotoAdapter(Context context, List<Photo> list) {
        mContext = context;
        listPhoto = list;
        inflater = LayoutInflater.from(mContext);
    }


    @Override
    public int getCount() {
        return listPhoto.size();
    }

    @Override
    public Object getItem(int i) {
        return listPhoto.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void swapItems(List<Photo> items) {
        this.listPhoto = items;
        notifyDataSetChanged();
    }

    public class ViewHolder {
        // PhotoView photoview;
        ImageView imgPhoto;
        Button change;
        Button delete;
        EditText keterangan;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        final ViewHolder holder;

        if (view == null) {

            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listphotoadapter, viewGroup, false);
            holder.imgPhoto = (ImageView) view.findViewById(R.id.imgPhotoListPhotoAdapter);
            holder.change = (Button) view.findViewById(R.id.btnChangeListPhotoAdapter);
            holder.delete = (Button) view.findViewById(R.id.btnDeletePhotoAdapter);
            holder.keterangan = (EditText) view.findViewById(R.id.etKetListPhotoAdapter);
            //holder.photoview=(PhotoView) view.findViewById(R.id.photo_view);

//            for (Photo photo:listPhoto){
//                System.out.println("Keterangan: " + photo.getUrl_photo());
//                Bitmap bmp = BitmapFactory.decodeFile(photo.getUrl_photo());
//                holder.imgPhoto.setImageBitmap(bmp);
//                holder.keterangan.setText(photo.getId_detail_bucket());
//                //holder.photoview.setImageBitmap(bmp);
//            }


            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }

        Bitmap bmp = BitmapFactory.decodeFile(listPhoto.get(i).getUrl_photo());
        holder.imgPhoto.setImageBitmap(bmp);
        holder.keterangan.setText(listPhoto.get(i).getKeterangan());


        holder.change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Konfirmasi Ubah Keterangan Foto");
                builder.setMessage("Ubah Keterangan Foto?");
                builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Photo p = new Photo();
                        String ketbaru = holder.keterangan.getText().toString();
                        String hasil=p.updateKetPhoto(listPhoto.get(i).getId_photo(), ketbaru);
                       Toast.makeText(view.getContext(), hasil, Toast.LENGTH_SHORT).show();
                        listPhoto = p.getImageFromSqlite(listPhoto.get(i).getId_detail_inventory());
                        notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Konfirmasi Hapus Foto");
                builder.setMessage("Hapus Foto?");
                builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Photo p = new Photo();
                        //  Toast.makeText(mContext, listPhoto.get(i).getNama_photo(), Toast.LENGTH_SHORT).show();
                        p.deletePhoto(listPhoto.get(i).getNama_photo());
                        listPhoto = p.getImageFromSqlite(listPhoto.get(i).getId_detail_inventory());
                        notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();


            }
        });

        holder.imgPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                File file = new File(listPhoto.get(i).getUrl_photo());
                intent.setDataAndType(Uri.fromFile(file),"image/*");
                mContext.startActivity(intent);
            }
        });

        return view;
    }


//    public void takePicture(View view) {
//        String filename = "";
//        String path = "";
//        Uri uriSavedImage;
//        Detail_Bucket d = new Detail_Bucket();
//        //camera stuff
//        Intent imageIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//
//        //folder stuff
//        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "KSP");
//        imagesFolder.mkdirs();
//
//        filename = d.getId_detail_bucket() + "_" + timeStamp + ".png";
//        File image = new File(imagesFolder, filename);
//        uriSavedImage = Uri.fromFile(image);
//
//        path = image.toString();
//        System.out.println("PATH: " + image.getPath());
//
//
////        Activity origin = (Activity) mContext;
////        origin.startActivityForResult(new Intent(mContext, ListPhoto.class), 200);
//
//
//        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
//        imageIntent.putExtra("INIDATA", path);
//        act.setResult(200, imageIntent);
//        act.startActivityForResult(imageIntent, 200);
//
//    }
//
//
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        System.out.println(requestCode + " " + resultCode + " " + data);
//        Log.d("MyAdapter", "onActivityResult");
//
//
//    }
}
