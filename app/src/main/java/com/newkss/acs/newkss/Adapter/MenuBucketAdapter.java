package com.newkss.acs.newkss.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.newkss.acs.newkss.Maps.MapsJarak;
import com.newkss.acs.newkss.Menu.MenuDetailInventoryToday;
import com.newkss.acs.newkss.Menu.MenuListInventoryToday;
import com.newkss.acs.newkss.Model.Detail_Bucket;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.ModelBaru.DataBucketBooked;
import com.newkss.acs.newkss.ModelBaru.Detil_TGT;
import com.newkss.acs.newkss.ModelBaru.TGT;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Function;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Locale;

import static android.widget.CompoundButton.OnCheckedChangeListener;
import static android.widget.CompoundButton.OnClickListener;

/**
 * Created by acs on 21/04/17.
 */

public class MenuBucketAdapter extends BaseAdapter {

    boolean[] itemChecked;
    Context mContext;
    LayoutInflater inflater;
    private ArrayList<Detail_Inventory> dataDetInvList = null;
    private ArrayList<Detail_Inventory> arraylist;

    TGT queryTGT = new TGT();
    String jaraktempuh = "";

    public MenuBucketAdapter(Context context, ArrayList<Detail_Inventory> list) {
        mContext = context;
        dataDetInvList = list;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(dataDetInvList);
        if (dataDetInvList != null) {
            itemChecked = new boolean[dataDetInvList.size()];
        }
    }

    @Override
    public int getCount() {
        return dataDetInvList.size();
    }

    @Override
    public Object getItem(int i) {
        return dataDetInvList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private class ViewHolder {
        TextView nama;
        TextView cycle;
        TextView os;
        TextView totalBayar;
        TextView alamat;
        TextView jarak;
        CheckBox cb;
        Button btnDetail;
        TextView noLoan;
        //Button btnhasilSurvey;
        Button btnMaps;
    }


    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listrowbucket, viewGroup, false);

            holder.noLoan = (TextView) view.findViewById(R.id.txtNoLoan);
            //holder.totalBayar = (TextView) view.findViewById(R.id.txtTotalBayar);
            holder.nama = (TextView) view.findViewById(R.id.txtNama);
            holder.cycle = (TextView) view.findViewById(R.id.txtCycle);
            holder.os = (TextView) view.findViewById(R.id.txtOS);
            holder.alamat = (TextView) view.findViewById(R.id.txtAlamat);
            //holder.jarak = (TextView) view.findViewById(R.id.txtJarak);
            holder.cb = (CheckBox) view.findViewById(R.id.chkBoxListRowBucket);
            holder.btnDetail = (Button) view.findViewById(R.id.btnDetailInventory);
            holder.btnMaps = (Button) view.findViewById(R.id.btnGoogleMaps);

            view.setBackgroundResource(android.R.color.transparent);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        //dpt no loan dan id_tgt
        System.out.println("dataDetInvList" + dataDetInvList.get(position).getNo_loan());
        final String id = queryTGT.getIdTGTFromNoLoan(dataDetInvList.get(position).getNo_loan(), dataDetInvList.get(position).getId_detail_inventory());
        System.out.println("NILAI ID: " + id);
        final TGT dp = queryTGT.getDataPenagihanFromID(id);


        holder.noLoan.setText(dataDetInvList.get(position).getNo_loan());
        holder.nama.setText(dp.getNama());
        holder.cycle.setText(dp.getCycle());
//        holder.cycle.setText(dp.getCycle());
//        holder.os.setText(Function.returnSeparatorComa(dp.getOs()));
        holder.os.setText(Function.returnSeparatorComa(dataDetInvList.get(position).getOs_pinjaman()));
        //holder.totalBayar.setText("");
        holder.alamat.setText(dp.getAlamat());

//        if (dataDetInvList.get(position).getJarak_tempuh() == null) {
//            jaraktempuh = "";
//        }
//        else{
//            jaraktempuh=dataDetInvList.get(position).getJarak_tempuh();
//        }
//        holder.jarak.setText(jaraktempuh);

        final Detail_Inventory b = getBucket(position);
        holder.cb.setOnCheckedChangeListener(myCheckedChangeList);
        holder.cb.setTag(position);
        holder.cb.setChecked(b.box);

        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(mContext, MenuDetailInventoryToday.class);
                in.putExtra("id", dataDetInvList.get(position).getId_detail_inventory());
                in.putExtra("nama", dp.getNama());
                mContext.startActivity(in);
            }
        });

        holder.btnMaps.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(mContext, MapsJarak.class);
                in.putExtra("no_loan", dataDetInvList.get(position).getNo_loan());
                mContext.startActivity(in);
            }
        });

        return view;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        dataDetInvList.clear();
        if (charText.length() == 0) {
            dataDetInvList.addAll(arraylist);
        } else {
            for (Detail_Inventory wp : arraylist) {
                if (wp.getNo_loan().toLowerCase(Locale.getDefault()).contains(charText)||wp.getNama_debitur().toLowerCase(Locale.getDefault()).contains(charText)) {
                    dataDetInvList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public Detail_Inventory getBucket(int position) {
        return ((Detail_Inventory) getItem(position));
    }

    //
    public ArrayList<Detail_Inventory> getBox() {
        ArrayList<Detail_Inventory> box = new ArrayList<>();
        for (Detail_Inventory p : arraylist) {
            if (p.box)
                box.add(p);
        }
        return box;
    }

    CompoundButton.OnCheckedChangeListener myCheckedChangeList = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            getBucket((Integer) buttonView.getTag()).box = isChecked;
        }
    };


    //    LinearLayout llAll;
//    private ArrayList<Bucket> listData = new ArrayList<>();
//    private List<Bucket> listSearch;
//    private Context mContext;
////    private Bucket mBucket;

//    int position;

//    boolean[] itemChecked;
//    Context mContext;
//    LayoutInflater inflater;
//    private ArrayList<TGT> tgtList = null;
//    private ArrayList<TGT> arraylist;
//
//    public MenuBucketAdapter(Context context, ArrayList<TGT> list) {
//        mContext = context;
//        tgtList = list;
//        inflater = LayoutInflater.from(mContext);
//        this.arraylist = new ArrayList<>();
//        this.arraylist.addAll(tgtList);
//        if (tgtList != null) {
//            itemChecked = new boolean[tgtList.size()];
//        }
//    }
//
//    public class ViewHolder {
//        TextView nama;
//        TextView cycle;
//        TextView os;
//        TextView totalbayar;
//        TextView alamat;
//        TextView jarak;
//        CheckBox cb;
//        Button btnDetail;
//        // Button btnGoogleMaps;
//    }
//
//    @Override
//    public int getCount() {
//        return tgtList.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return tgtList.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return 0;
//    }
//
//    @Override
//    public int getViewTypeCount() {
//        return getCount();
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(final int position, View view, ViewGroup parent) {
//        final ViewHolder holder;
//        if (view == null) {
//            holder = new ViewHolder();
//            view = inflater.inflate(R.layout.listrowbucket, parent, false);
//
//            // Locate the TextViews in listview_item.xml
//            holder.nama = (TextView) view.findViewById(R.id.txtNama);
//            holder.cycle = (TextView) view.findViewById(R.id.txtCycle);
//            holder.os = (TextView) view.findViewById(R.id.txtOS);
//            holder.totalbayar = (TextView) view.findViewById(R.id.txtTotalBayar);
//            holder.alamat = (TextView) view.findViewById(R.id.txtAlamat);
//            holder.jarak = (TextView) view.findViewById(R.id.txtJarak);
//            holder.cb = (CheckBox) view.findViewById(R.id.chkBoxListRowBucket);
//
//            holder.btnDetail = (Button) view.findViewById(R.id.btnDetailInventory);
//            //   holder.btnGoogleMaps = (Button) view.findViewById(R.id.btnGoogleMaps);
//
//            // CheckBox cb = (CheckBox) view.findViewById(R.id.chkBoxListRowBucket);
//
//            view.setBackgroundResource(android.R.color.transparent);
//            view.setTag(holder);
//        } else {
//            holder = (ViewHolder) view.getTag();
//        }
//        // Set the results into TextViews
//        holder.nama.setText("Nama :" + tgtList.get(position).getNama());
//        holder.cycle.setText("Cycle :" + tgtList.get(position).getCycle());
//        holder.os.setText("OS :" + tgtList.get(position).getOs());
//        holder.totalbayar.setText("Total Bayar :" + "");
//        holder.alamat.setText("Alamat :" + tgtList.get(position).getAlamat());
//        holder.jarak.setText("Jarak :" + tgtList.get(position).getJarak());
//
//
//        final TGT b = getBucket(position);
//        holder.cb.setOnCheckedChangeListener(myCheckedChangeList);
//        holder.cb.setTag(position);
//        holder.cb.setChecked(b.box);
//
//
//        //Button btnDetailInventory = (Button) view.findViewById(R.id.btnDetailInventory);
//
//        holder.btnDetail.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent in = new Intent(mContext, MenuDetailInventoryToday.class);
//                in.putExtra("id_tgt", tgtList.get(position).getId_tgt());
//                in.putExtra("nama", tgtList.get(position).getNama());
//                mContext.startActivity(in);
////                Toast.makeText(view.getContext(), b.getAlamat(), Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        // Button btnGoogleMaps = (Button) view.findViewById(R.id.btnGoogleMaps);
////        holder.btnGoogleMaps.setOnClickListener(new OnClickListener() {
////            @Override
////            public void onClick(View view) {
//////                Toast.makeText(view.getContext(), b.getTotalBayar(), Toast.LENGTH_SHORT).show();
////            }
////        });
//
//
//        return view;
//    }
//
//    public void filter(String charText) {
//        charText = charText.toLowerCase(Locale.getDefault());
//        tgtList.clear();
//        if (charText.length() == 0) {
//            tgtList.addAll(arraylist);
//        } else {
//            for (TGT wp : arraylist) {
//                if (wp.getNama().toLowerCase(Locale.getDefault()).contains(charText)) {
//                    tgtList.add(wp);
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }
//
//    public TGT getBucket(int position) {
//        return ((TGT) getItem(position));
//    }
//
//    public ArrayList<TGT> getBox() {
//        ArrayList<TGT> box = new ArrayList<>();
//        for (TGT p : arraylist) {
//            if (p.box)
//                box.add(p);
//        }
//        return box;
//    }
//
//    OnCheckedChangeListener myCheckedChangeList = new OnCheckedChangeListener() {
//        public void onCheckedChanged(CompoundButton buttonView,
//                                     boolean isChecked) {
////            int position = (Integer) buttonView.getTag();
////            Bucket b=new Bucket();
////
//            getBucket((Integer) buttonView.getTag()).box = isChecked;
//        }
//    };


//    @Override
//    public int getCount() {
//        return bucketList.size();
////        if (listData != null) {
////            return listData.size();
////        }
////        return 0;
//    }
//
//    @Override
//    public Object getItem(int i) {
//        return bucketList.get(i);
////        if (listData != null) {
////            return listData.get(i);
////        }
////        return 0;
//
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return 0;
//    }
//
//    public Bucket getBucket(int position) {
//        return ((Bucket) getItem(position));
//    }
//
//    public ArrayList<Bucket> getBox() {
//        ArrayList<Bucket> box = new ArrayList<Bucket>();
//        for (Bucket p : bucketList) {
//            if (p.box)
//                box.add(p);
//        }
//        return box;
//    }
//
//    OnCheckedChangeListener myCheckedChangeList = new OnCheckedChangeListener() {
//        public void onCheckedChanged(CompoundButton buttonView,
//                                     boolean isChecked) {
//            getBucket((Integer) buttonView.getTag()).box = isChecked;
//        }
//    };
//
//
//    public void filter(String charText) {
//        charText = charText.toLowerCase(Locale.getDefault());
//        //listSearch=new ArrayList<>();
//        //listSearch=listData;
//        bucketList.clear();
//        if (charText.length() == 0) {
//            bucketList.addAll(arraylist);
//        } else {
//            for (Bucket wp : arraylist) {
//                if (wp.getNama().toLowerCase(Locale.getDefault()).contains(charText)) {
//                    bucketList.add(wp);
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }
//
//    @Override
//    public View getView(final int i, View convertView, ViewGroup viewGroup) {
//
//        View view = convertView;
//        if (view == null) {
//            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            view = inflater.inflate(R.layout.listrowbucket, viewGroup, false);
//
//            final Bucket b = getBucket(i);
//            final TextView nama = (TextView) view.findViewById(R.id.txtNama);
//            final TextView cycle = (TextView) view.findViewById(R.id.txtCycle);
//            final TextView os = (TextView) view.findViewById(R.id.txtOS);
//            final TextView totalBayar = (TextView) view.findViewById(R.id.txtTotalBayar);
//            final TextView Alamat = (TextView) view.findViewById(R.id.txtAlamat);
//
//            CheckBox cb = (CheckBox) view.findViewById(R.id.chkBoxListRowBucket);
//            cb.setOnCheckedChangeListener(myCheckedChangeList);
//            cb.setTag(i);
//            cb.setChecked(b.box);
//
//            Button btnDetailInventory = (Button) view.findViewById(R.id.btnDetailInventory);
//
//            btnDetailInventory.setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Toast.makeText(view.getContext(), b.getAlamat(), Toast.LENGTH_SHORT).show();
//                }
//            });
//
//            Button btnGoogleMaps = (Button) view.findViewById(R.id.btnGoogleMaps);
//            btnGoogleMaps.setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Toast.makeText(view.getContext(), b.getTotalBayar(), Toast.LENGTH_SHORT).show();
//                }
//            });
//
//
////            if(i!=position){
////                view.setBackgroundResource(android.R.color.transparent);
////            }
////            else if(!cb.isChecked()){
////                view.setBackgroundColor(Color.parseColor("#edc683"));
////            }
//
////            if(cb.isChecked()){
////                view.setBackgroundColor(Color.parseColor("#fdc689"));
////            }
////            else{
////                view.setBackgroundResource(android.R.color.transparent);
////            }
//
//            nama.setText("Nama :" + bucketList.get(i).getNama());
//            cycle.setText("Cycle :" + bucketList.get(i).getCycle());
//            os.setText("OS :" + bucketList.get(i).getOS());
//            totalBayar.setText("Total Bayar: " + bucketList.get(i).getTotalBayar());
//            Alamat.setText("Alamat: " + bucketList.get(i).getAlamat());
//        }


//
////        final ViewHolder holder;
////
////        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
////        if (convertView == null) {
////            convertView = inflater.inflate(R.layout.listrowbucket, null);
////            holder = new ViewHolder();
////
////            holder.chkListBucket = (CheckBox) convertView.findViewById(R.id.chkBoxListRowBucket);
////           holder.listBucketName = (TextView) convertView.findViewById(R.id.txtNama);
//////            final TextView nama = (TextView) convertView.findViewById(R.id.txtNama);
////            final TextView cycle = (TextView) convertView.findViewById(R.id.txtCycle);
////            final TextView os = (TextView) convertView.findViewById(R.id.txtOS);
////            final TextView totalBayar = (TextView) convertView.findViewById(R.id.txtTotalBayar);
////            final TextView Alamat = (TextView) convertView.findViewById(R.id.txtAlamat);
////
////            holder.listBucketName.setText(listData.get(i).getNama());
////            cycle.setText(listData.get(i).getCycle());
////            os.setText(listData.get(i).getOS());
////            totalBayar.setText(listData.get(i).getTotalBayar());
////            Alamat.setText(listData.get(i).getAlamat());
////
////            convertView.setTag(holder);
////        } else {
////            holder = (ViewHolder) convertView.getTag();
////        }
////
////
////        if (itemChecked[i]) {
////            holder.chkListBucket.setChecked(true);
////        } else {
////            holder.chkListBucket.setChecked(false);
////        }
////
////        holder.chkListBucket.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                if (holder.chkListBucket.isChecked()) {
////                    itemChecked[i] = true;
////                } else {
////                    itemChecked[i] = false;
////                }
////            }
////        });
//
//      return view;
//   }


}
