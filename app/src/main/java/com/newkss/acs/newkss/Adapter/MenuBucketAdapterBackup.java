package com.newkss.acs.newkss.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.newkss.acs.newkss.Menu.MenuUtama;
import com.newkss.acs.newkss.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.widget.CompoundButton.*;

/**
 * Created by acs on 21/04/17.
 */

public class MenuBucketAdapterBackup extends BaseAdapter {

    LinearLayout llAll;
    private ArrayList<Bucket> listData = new ArrayList<>();
    private List<Bucket> listSearch;
    private Context mContext;
    private Bucket mBucket;
    boolean[] itemChecked;
    int position;

    public MenuBucketAdapterBackup(Context context, ArrayList<Bucket> list, Bucket bucket) {
        mContext = context;
        listData = list;
        mBucket = bucket;
//        listSearch.addAll(listData);
        if (listData != null) {
            itemChecked = new boolean[listData.size()];
        }

    }

    @Override
    public int getCount() {
        return listData.size();
//        if (listData != null) {
//            return listData.size();
//        }
//        return 0;
    }

    @Override
    public Object getItem(int i) {
        return listData.get(i);
//        if (listData != null) {
//            return listData.get(i);
//        }
//        return 0;

    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public Bucket getBucket(int position) {
        return ((Bucket) getItem(position));
    }

    public ArrayList<Bucket> getBox() {
        ArrayList<Bucket> box = new ArrayList<Bucket>();
        for (Bucket p : listData) {
            if (p.box)
                box.add(p);
        }
        return box;
    }

    OnCheckedChangeListener myCheckedChangeList = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            getBucket((Integer) buttonView.getTag()).box = isChecked;
        }
    };


    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        //listSearch=new ArrayList<>();
        //listSearch=listData;
        listData.clear();
        if (charText.length() == 0) {
            listData.addAll(listSearch);
        } else {
            for (Bucket wp : listSearch) {
                if (wp.getNama().toLowerCase(Locale.getDefault()).contains(charText)) {
                    listData.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listrowbucket, viewGroup, false);

            final Bucket b = getBucket(i);
            final TextView nama = (TextView) view.findViewById(R.id.txtNama);
            final TextView cycle = (TextView) view.findViewById(R.id.txtCycle);
            final TextView os = (TextView) view.findViewById(R.id.txtOS);
            final TextView totalBayar = (TextView) view.findViewById(R.id.txtTotalBayar);
            final TextView Alamat = (TextView) view.findViewById(R.id.txtAlamat);

            CheckBox cb = (CheckBox) view.findViewById(R.id.chkBoxListRowBucket);
            cb.setOnCheckedChangeListener(myCheckedChangeList);
            cb.setTag(i);
            cb.setChecked(b.box);

            Button btnDetailInventory = (Button) view.findViewById(R.id.btnDetailInventory);

            btnDetailInventory.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                    Toast.makeText(view.getContext(), b.getAlamat(), Toast.LENGTH_SHORT).show();
                }
            });

            Button btnGoogleMaps = (Button) view.findViewById(R.id.btnGoogleMaps);
            btnGoogleMaps.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(view.getContext(), b.getTotalBayar(), Toast.LENGTH_SHORT).show();
                }
            });


//            if(i!=position){
//                view.setBackgroundResource(android.R.color.transparent);
//            }
//            else if(!cb.isChecked()){
//                view.setBackgroundColor(Color.parseColor("#edc683"));
//            }

//            if(cb.isChecked()){
//                view.setBackgroundColor(Color.parseColor("#fdc689"));
//            }
//            else{
//                view.setBackgroundResource(android.R.color.transparent);
//            }

            nama.setText("Nama :" + listData.get(i).getNama());
            cycle.setText("Cycle :" + listData.get(i).getCycle());
            os.setText("OS :" + listData.get(i).getOS());
            totalBayar.setText("Total Bayar: " + listData.get(i).getTotalBayar());
            Alamat.setText("Alamat: " + listData.get(i).getAlamat());
        }

//        final ViewHolder holder;
//
//        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        if (convertView == null) {
//            convertView = inflater.inflate(R.layout.listrowbucket, null);
//            holder = new ViewHolder();
//
//            holder.chkListBucket = (CheckBox) convertView.findViewById(R.id.chkBoxListRowBucket);
//           holder.listBucketName = (TextView) convertView.findViewById(R.id.txtNama);
////            final TextView nama = (TextView) convertView.findViewById(R.id.txtNama);
//            final TextView cycle = (TextView) convertView.findViewById(R.id.txtCycle);
//            final TextView os = (TextView) convertView.findViewById(R.id.txtOS);
//            final TextView totalBayar = (TextView) convertView.findViewById(R.id.txtTotalBayar);
//            final TextView Alamat = (TextView) convertView.findViewById(R.id.txtAlamat);
//
//            holder.listBucketName.setText(listData.get(i).getNama());
//            cycle.setText(listData.get(i).getCycle());
//            os.setText(listData.get(i).getOS());
//            totalBayar.setText(listData.get(i).getTotalBayar());
//            Alamat.setText(listData.get(i).getAlamat());
//
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//
//
//        if (itemChecked[i]) {
//            holder.chkListBucket.setChecked(true);
//        } else {
//            holder.chkListBucket.setChecked(false);
//        }
//
//        holder.chkListBucket.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (holder.chkListBucket.isChecked()) {
//                    itemChecked[i] = true;
//                } else {
//                    itemChecked[i] = false;
//                }
//            }
//        });

        return view;
    }


}
