package com.newkss.acs.newkss.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.newkss.acs.newkss.Menu.MenuDetailInventoryToday;
import com.newkss.acs.newkss.Menu.MenuListInventoryToday;
import com.newkss.acs.newkss.MenuDetailVisit.MenuDetailVisitSurvey;
import com.newkss.acs.newkss.MenuKunjungan.MenuHasilKunjungan;
import com.newkss.acs.newkss.Model.Detail_Bucket;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.ModelBaru.DataBucketBooked;
import com.newkss.acs.newkss.ModelBaru.TGT;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Function;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by acs on 27/04/17.
 */

public class MenuBucketAdapterSelected extends BaseAdapter {

    boolean[] itemChecked;
//    int position;

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<Detail_Inventory> dataDetInvList = null;
    private ArrayList<Detail_Inventory> arraylist;

    TGT queryTGT = new TGT();
    DataBucketBooked dbb = new DataBucketBooked();

    public MenuBucketAdapterSelected(Context context, ArrayList<Detail_Inventory> list) {
        mContext = context;
        dataDetInvList = list;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(dataDetInvList);
        if (dataDetInvList != null) {
            itemChecked = new boolean[dataDetInvList.size()];
        }
    }
    @Override
    public int getCount() {
        return dataDetInvList.size();
    }

    @Override
    public Object getItem(int i) {
        return dataDetInvList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private class ViewHolder {
        TextView nama;
        TextView cycle;
        TextView os;
        TextView totalKewajiban;
        TextView alamat;
        TextView jarak;

        TextView noLoan;
        Button btnDetail;
        Button btnhasilKunjungan;

    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listrowbucketselected, viewGroup, false);

            holder.noLoan=(TextView) view.findViewById(R.id.txtNoLoan);
            holder.totalKewajiban = (TextView) view.findViewById(R.id.txtTotalKewajiban);
            holder.nama = (TextView) view.findViewById(R.id.txtNama);
            holder.cycle = (TextView) view.findViewById(R.id.txtCycle);
            holder.os = (TextView) view.findViewById(R.id.txtOS);
            holder.alamat = (TextView) view.findViewById(R.id.txtAlamat);
            //holder.jarak = (TextView) view.findViewById(R.id.txtJarak);


//            holder.btnDetail = (Button) view.findViewById(R.id.btnDetailInventoryListRowBucketSelected);
//            holder.btnhasilKunjungan = (Button) view.findViewById(R.id.btnHasilKunjunganListRowBucketSelected);

            view.setBackgroundResource(android.R.color.transparent);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        //dpt no loan dan id_data_pencairan
        System.out.println("dataDetInvList " + dataDetInvList.get(position).getNo_loan());
        final String id = queryTGT.getIdTGTFromNoLoan(dataDetInvList.get(position).getNo_loan(),dataDetInvList.get(position).getId_detail_inventory());
        System.out.println("NILAI ID: "+id);
        final TGT dp = queryTGT.getDataPenagihanFromID(id);

        holder.noLoan.setText(dataDetInvList.get(position).getNo_loan());
        holder.nama.setText(dp.getNama());
        holder.cycle.setText(dp.getCycle());
//        holder.cycle.setText("");
//        holder.os.setText(Function.returnSeparatorComa(dp.getOs()));
        holder.os.setText(Function.returnSeparatorComa(dataDetInvList.get(position).getOs_pinjaman()));
        //String hasil=String.valueOf(Function.convertToBigDecimal(dataDetInvList.get(position).getTotal_kewajiban()));
//        holder.totalKewajiban.setText(Function.returnSeparatorComa(dataDetInvList.get(position).getTotal_kewajiban()));
        holder.totalKewajiban.setText(Function.returnSeparatorComa(dataDetInvList.get(position).getTotal_kewajiban()));

        holder.alamat.setText(dp.getAlamat());
        //holder.jarak.setText(dataDetInvList.get(position).getJarak_tempuh());

//        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent in = new Intent(mContext, MenuDetailInventoryToday.class);
//                in.putExtra("id", dataDetInvList.get(position).getId_detail_inventory());
//                in.putExtra("nama", dp.getNama());
//                mContext.startActivity(in);
//            }
//        });
//
//        holder.btnhasilKunjungan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent in = new Intent(mContext, MenuHasilKunjungan.class);
//                in.putExtra("id", dataDetInvList.get(position).getId_detail_inventory());
//                mContext.startActivity(in);
//            }
//        });

        return view;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        dataDetInvList.clear();
        if (charText.length() == 0) {
            dataDetInvList.addAll(arraylist);
        } else {
            for (Detail_Inventory wp : arraylist) {
                if (wp.getNo_loan().toLowerCase(Locale.getDefault()).contains(charText)||wp.getNama_debitur().toLowerCase(Locale.getDefault()).contains(charText)) {
                    dataDetInvList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }



//    Context mContext;
//    LayoutInflater inflater;
////    private ArrayList<Detail_Bucket> bucketList = null;
////    private ArrayList<Detail_Bucket> arraylist;
//    boolean[] itemChecked;
//
//    private ArrayList<TGT> bucketList = null;
//    private ArrayList<TGT> arraylist;
//
//
//    public MenuBucketAdapterSelected(Context context, ArrayList<TGT> list) {
//        mContext = context;
//        bucketList = list;
//        inflater = LayoutInflater.from(mContext);
//        this.arraylist = new ArrayList<>();
////        this.arraylist = list;
//        this.arraylist.addAll(bucketList);
////        if (bucketList != null) {
////            itemChecked = new boolean[bucketList.size()];
////        }
//    }
//
//    @Override
//    public int getCount() {
//        return bucketList.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return bucketList.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return 0;
//    }
//
//    @Override
//    public int getViewTypeCount() {
//        return getCount();
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return position;
//    }
//
//    public class ViewHolder {
//        TextView nama;
//        TextView jarak;
//        TextView os;
//        TextView cycle;
//        TextView totalbayar;
//        TextView alamat;
////        Button btnDetail;
////        Button btnHasilSurvey;
//    }
//
//    public Detail_Bucket getBucket(int position) {
//        return ((Detail_Bucket) getItem(position));
//    }
//
//    public void filter(String charText) {
//        charText = charText.toLowerCase(Locale.getDefault());
//        bucketList.clear();
//        if (charText.length() == 0) {
//            bucketList.addAll(arraylist);
//        } else {
//            for (TGT wp : arraylist) {
//                if (wp.getNama().toLowerCase(Locale.getDefault()).contains(charText)) {
//                    bucketList.add(wp);
//                }
//            }
//        }
//        notifyDataSetChanged();
//    }
//
//    @Override
//    public View getView(final int i, View view, ViewGroup parent) {
//        final ViewHolder holder;
//        if (view == null) {
//            holder = new ViewHolder();
//            view = inflater.inflate(R.layout.listrowbucketselected, parent, false);
//
//            holder.nama = (TextView) view.findViewById(R.id.txtNama);
//            holder.jarak = (TextView) view.findViewById(R.id.txtJarak);
//            holder.os = (TextView) view.findViewById(R.id.txtOS);
//            holder.cycle = (TextView) view.findViewById(R.id.txtCycle);
//            holder.totalbayar = (TextView) view.findViewById(R.id.txtTotalBayar);
//            holder.alamat = (TextView) view.findViewById(R.id.txtAlamat);
//
////            holder.btnDetail = (Button) view.findViewById(R.id.btnDetailInventoryListRowBucketSelected);
////            holder.btnHasilSurvey = (Button) view.findViewById(R.id.btnHasilSurveyListRowBucketSelected);
//            view.setTag(holder);
//        } else {
//            holder = (ViewHolder) view.getTag();
//        }
//
//        holder.nama.setText(bucketList.get(i).getNama());
//        holder.jarak.setText(bucketList.get(i).getJarak());
//        holder.os.setText(bucketList.get(i).getOs());
//        holder.cycle.setText(bucketList.get(i).getCycle());
//        holder.totalbayar.setText("");
//        holder.alamat.setText(bucketList.get(i).getAlamat());
//
//
//
////        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                Detail_Bucket d = (Detail_Bucket) getBucket(i);
////                Intent in = new Intent(mContext, MenuListInventoryToday.class);
////                in.putExtra("detailbucket", d);
////                mContext.startActivity(in);
////            }
////        });
////
////        holder.btnHasilSurvey.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                Detail_Bucket d = (Detail_Bucket) getBucket(i);
////                Intent in = new Intent(mContext, MenuDetailVisitSurvey.class);
////                in.putExtra("detailbucket", d);
////                mContext.startActivity(in);
//////                Toast.makeText(mContext, "Tes hasil survey", Toast.LENGTH_SHORT).show();
////            }
////        });
//
//        return view;
//    }
}
