package com.newkss.acs.newkss.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.ModelBaru.TGT;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Function;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by acs on 8/22/17.
 */

public class MenuListDetailBucketAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<Detail_Inventory> dataDetInvList = null;
    TGT queryTGT = new TGT();
    private ArrayList<Detail_Inventory> arraylist;
    public MenuListDetailBucketAdapter(Context context, ArrayList<Detail_Inventory> list) {
        mContext = context;
        dataDetInvList = list;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(dataDetInvList);
    }

    @Override
    public int getCount() {
        return dataDetInvList.size();
    }

    @Override
    public Object getItem(int i) {
        return dataDetInvList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private class ViewHolder {
        TextView nama;
        TextView cycle;
        TextView os;
        TextView totalKewajiban;
        TextView alamat;
        TextView noLoan;

    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listrowbucketselected, viewGroup, false);

            holder.noLoan = (TextView) view.findViewById(R.id.txtNoLoan);
            holder.totalKewajiban = (TextView) view.findViewById(R.id.txtTotalKewajiban);
            holder.nama = (TextView) view.findViewById(R.id.txtNama);
            holder.cycle = (TextView) view.findViewById(R.id.txtCycle);
            holder.os = (TextView) view.findViewById(R.id.txtOS);
            holder.alamat = (TextView) view.findViewById(R.id.txtAlamat);
            view.setBackgroundResource(android.R.color.transparent);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        //dpt no loan dan id_data_pencairan
        System.out.println("dataDetInvList " + dataDetInvList.get(position).getNo_loan());
        final String id = queryTGT.getIdTGTFromNoLoan(dataDetInvList.get(position).getNo_loan(), dataDetInvList.get(position).getId_detail_inventory());
        System.out.println("NILAI ID: " + id);
        final TGT dp = queryTGT.getDataPenagihanFromID(id);

        holder.noLoan.setText(dataDetInvList.get(position).getNo_loan());
        holder.nama.setText(dp.getNama());
        holder.cycle.setText(dp.getCycle());
        holder.os.setText(Function.returnSeparatorComa(dataDetInvList.get(position).getOs_pinjaman()));
        holder.totalKewajiban.setText(Function.returnSeparatorComa(dataDetInvList.get(position).getTotal_kewajiban()));
        holder.alamat.setText(dp.getAlamat());

        return view;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        dataDetInvList.clear();
        if (charText.length() == 0) {
            dataDetInvList.addAll(arraylist);
        } else {
            for (Detail_Inventory wp : arraylist) {
                if (wp.getNo_loan().toLowerCase(Locale.getDefault()).contains(charText) || wp.getNama_debitur().toLowerCase(Locale.getDefault()).contains(charText)) {
                    dataDetInvList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}
