package com.newkss.acs.newkss.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.newkss.acs.newkss.Maps.MapsJarak;
import com.newkss.acs.newkss.Menu.MenuDetailInventoryToday;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.ModelBaru.DataBucketBooked;
import com.newkss.acs.newkss.ModelBaru.Data_Pencairan;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Function;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by acs on 6/20/17.
 */

public class MenuListSurveyAdapter extends BaseAdapter {

    boolean[] itemChecked;
    Context mContext;
    LayoutInflater inflater;
    private ArrayList<Detail_Inventory> dataDetInvList = null;
    private ArrayList<Detail_Inventory> arraylist;

    String jaraktempuh;
    Data_Pencairan queryDP = new Data_Pencairan();

    public MenuListSurveyAdapter(Context context, ArrayList<Detail_Inventory> list) {
        mContext = context;
        dataDetInvList = list;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(dataDetInvList);
        if (dataDetInvList != null) {
            itemChecked = new boolean[dataDetInvList.size()];
        }
    }

    @Override
    public int getCount() {
        return dataDetInvList.size();
    }

    @Override
    public Object getItem(int i) {
        return dataDetInvList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    private class ViewHolder {
        TextView nama;
        TextView cycle;
        TextView os;
        TextView tglcair;
        TextView alamat;
        TextView jarak;
        TextView noloan;
        CheckBox cb;
        Button btnDetail;
        //Button btnhasilSurvey;
        Button btnGoogleMaps;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.list_survey, viewGroup, false);

            holder.noloan = (TextView) view.findViewById(R.id.txtNoLoan);
            holder.tglcair = (TextView) view.findViewById(R.id.txtTglCair);
            holder.nama = (TextView) view.findViewById(R.id.txtNama);
            holder.cycle = (TextView) view.findViewById(R.id.txtCycle);
            holder.os = (TextView) view.findViewById(R.id.txtOS);
            holder.alamat = (TextView) view.findViewById(R.id.txtAlamat);
            holder.jarak = (TextView) view.findViewById(R.id.txtJarak);
            holder.cb = (CheckBox) view.findViewById(R.id.chkBoxListSurvey);

            holder.btnDetail = (Button) view.findViewById(R.id.btnDetailInventory);
            holder.btnGoogleMaps = (Button) view.findViewById(R.id.btnGoogleMaps);

            view.setBackgroundResource(android.R.color.transparent);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        //dpt no loan dan id_data_pencairan
        System.out.println("dataDetInvList" + dataDetInvList.get(position).getNo_loan());
        String id = queryDP.getIdDataPencairanFromNoLoan(dataDetInvList.get(position).getNo_loan());
        final Data_Pencairan dp = queryDP.getDataPencairanFromID(id);

        holder.noloan.setText(dataDetInvList.get(position).getNo_loan());
        holder.nama.setText(dp.getNama());
        holder.cycle.setText(dp.getCycle());
//        holder.cycle.setText("");
        holder.os.setText(Function.returnSeparatorComa(dataDetInvList.get(position).getOs_pinjaman()));
//        holder.tglcair.setText("");
        holder.tglcair.setText(dataDetInvList.get(position).getTgl_cair());
        holder.alamat.setText(dp.getAlamat());
        if (dataDetInvList.get(position).getJarak_tempuh()==null) {
            jaraktempuh = "";
        } else {
            jaraktempuh = dataDetInvList.get(position).getJarak_tempuh();
        }
        holder.jarak.setText(jaraktempuh);


//        holder.nama.setText("Nama :" + dataPencarianList.get(position).getNama());
//        holder.cycle.setText("Cycle :" + dataPencarianList.get(position).getCycle());
//        holder.os.setText("OS :" + dataPencarianList.get(position).getOs());
//        holder.tglcair.setText("Tgl Cair :");
//        holder.alamat.setText("Alamat :" + dataPencarianList.get(position).getAlamat());
//        holder.jarak.setText("Jarak :" + dataPencarianList.get(position).getJarak());

        final Detail_Inventory b = getBucket(position);
        holder.cb.setOnCheckedChangeListener(myCheckedChangeList);
        holder.cb.setTag(position);
        holder.cb.setChecked(b.box);

        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext, dataDetInvList.get(position).getId_detail_inventory(), Toast.LENGTH_SHORT).show();
//                System.out.println("WAHAHAHAHA: "+dp.getId_data_pencairan());
                Intent in = new Intent(mContext, MenuDetailInventoryToday.class);
                in.putExtra("id", dataDetInvList.get(position).getId_detail_inventory());
                in.putExtra("nama", dp.getNama());
                mContext.startActivity(in);
            }
        });


        holder.btnGoogleMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(mContext, MapsJarak.class);
                in.putExtra("no_loan", dataDetInvList.get(position).getNo_loan());
                mContext.startActivity(in);
            }
        });
        return view;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        dataDetInvList.clear();
        if (charText.length() == 0) {
            dataDetInvList.addAll(arraylist);
        } else {
            for (Detail_Inventory wp : arraylist) {
                if (wp.getNo_loan().toLowerCase(Locale.getDefault()).contains(charText) || wp.getNama_debitur().toLowerCase(Locale.getDefault()).contains(charText)) {
//                    Data_Pencairan dpbaru = new Data_Pencairan();
//                    dpbaru=queryDP.getDataPencairanFromID(wp.getId_data_pencairan());
                    dataDetInvList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public Detail_Inventory getBucket(int position) {
        return ((Detail_Inventory) getItem(position));
    }

    //
    public ArrayList<Detail_Inventory> getBox() {
        ArrayList<Detail_Inventory> box = new ArrayList<>();
        for (Detail_Inventory p : arraylist) {
            if (p.box)
                box.add(p);
        }
        return box;
    }

    //
    CompoundButton.OnCheckedChangeListener myCheckedChangeList = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            getBucket((Integer) buttonView.getTag()).box = isChecked;
        }
    };
}
