package com.newkss.acs.newkss.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newkss.acs.newkss.Menu.MenuListBucket;
import com.newkss.acs.newkss.ModelBaru.Detil_TGT;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;

import java.util.ArrayList;

/**
 * Created by acs on 17/04/17.
 */

public class MenuPerformanceAdapter extends BaseAdapter {

    private ArrayList<PenagihanLama> listData = new ArrayList<>();
    private Context mContext;
    Detil_TGT dtgt = new Detil_TGT();

    public MenuPerformanceAdapter(Context context, ArrayList<PenagihanLama> list) {
        mContext = context;
        listData = list;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        View view;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.listrowpenagihan, null);
        //final TextView bucketTitle = (TextView) view.findViewById(R.id.bucketlistrow);
        final TextView TGT = (TextView) view.findViewById(R.id.tgtlistrow);
        final TextView FPAY = (TextView) view.findViewById(R.id.fpaylistrow);
        final TextView PPAY = (TextView) view.findViewById(R.id.ppaylistrow);
        final TextView NPAY = (TextView) view.findViewById(R.id.npaylistrow);
        final TextView NVST = (TextView) view.findViewById(R.id.nvstlistrow);

//        bucketTitle.setBackgroundResource(R.drawable.bordertable);
//        TGT.setBackgroundResource(R.drawable.bordertable);
//        FPAY.setBackgroundResource(R.drawable.bordertable);
//        PPAY.setBackgroundResource(R.drawable.bordertable);
//        NPAY.setBackgroundResource(R.drawable.bordertable);
//        NVST.setBackgroundResource(R.drawable.bordertable);

//        a.setWidth(40);
//        b.setWidth(40);
//        c.setWidth(40);
//        d.setWidth(40);
//        e.setWidth(40);
//        f.setWidth(40);
//        a.setGravity(View.TEXT_ALIGNMENT_CENTER);
//        b.setGravity(View.TEXT_ALIGNMENT_CENTER);
//        c.setGravity(View.TEXT_ALIGNMENT_CENTER);
//        d.setGravity(View.TEXT_ALIGNMENT_CENTER);
//        e.setGravity(View.TEXT_ALIGNMENT_CENTER);
//        f.setGravity(View.TEXT_ALIGNMENT_CENTER);

//        a.setText(listData.get(i).getBucket());
//        // a.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
//        b.setText(listData.get(i).getTgt());
//        c.setText(listData.get(i).getFpay());
//        d.setText(listData.get(i).getPpay());
//        e.setText(listData.get(i).getNpay());
//        f.setText(listData.get(i).getNvst());

//        bucketTitle.setBackgroundColor(Color.parseColor("#fdc689"));
//        TGT.setTextColor(Color.parseColor("#ff5300"));
//        FPAY.setTextColor(Color.parseColor("#959595"));

        //bucketTitle.setTextColor(Color.parseColor("#000000"));
        TGT.setTextColor(Color.parseColor("#005ec9"));
        FPAY.setTextColor(Color.parseColor("#005ec9"));
        PPAY.setTextColor(Color.parseColor("#005ec9"));
        NPAY.setTextColor(Color.parseColor("#005ec9"));
        NVST.setTextColor(Color.parseColor("#005ec9"));

        //bucketTitle.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        TGT.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        FPAY.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        PPAY.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        NPAY.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        NVST.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);


        //bucketTitle.setText(listData.get(i).getBucketTitle());
        TGT.setText(dtgt.getCountTgt());
        FPAY.setText(dtgt.getCountfpay());
        PPAY.setText(dtgt.getCountppay());
        NPAY.setText(dtgt.getCountnpay());
        NVST.setText(dtgt.getCountnvst());


        TGT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuListBucket.class);
                Bundle bundle = new Bundle();
                //bundle.putString(Constant.BUNDLE_TITLE,bucketTitle.getText().toString());
                bundle.putString(Constant.BUNDLE_DATA, "tgt");
                i.putExtras(bundle);
                mContext.startActivity(i);
                ((Activity) mContext).finish();

            }
        });

        FPAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuListBucket.class);
                Bundle bundle = new Bundle();
                // bundle.putString(Constant.BUNDLE_TITLE,bucketTitle.getText().toString());
                bundle.putString(Constant.BUNDLE_DATA, "fpay");
                i.putExtras(bundle);
                mContext.startActivity(i);
                ((Activity) mContext).finish();
            }
        });

        PPAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuListBucket.class);
                Bundle bundle = new Bundle();
                //bundle.putString(Constant.BUNDLE_TITLE,bucketTitle.getText().toString());
                bundle.putString(Constant.BUNDLE_DATA, "ppay");
                i.putExtras(bundle);
                mContext.startActivity(i);
                ((Activity) mContext).finish();
            }
        });

        NPAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuListBucket.class);
                Bundle bundle = new Bundle();
                //bundle.putString(Constant.BUNDLE_TITLE,bucketTitle.getText().toString());
                bundle.putString(Constant.BUNDLE_DATA, "npay");
                i.putExtras(bundle);
                mContext.startActivity(i);
                ((Activity) mContext).finish();
                // ((Activity)mContext).finish();
            }
        });

        NVST.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuListBucket.class);
                Bundle bundle = new Bundle();
                //bundle.putString(Constant.BUNDLE_TITLE,bucketTitle.getText().toString());
                bundle.putString(Constant.BUNDLE_DATA, "nvst");
                i.putExtras(bundle);
                mContext.startActivity(i);
                ((Activity) mContext).finish();
            }
        });


//        TGT.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //              Toast.makeText(view.getContext(), bucketTitle.getText() + " " + TGT.getText(), Toast.LENGTH_SHORT).show();
//                //    m = new MenuPerformance(bucketTitle.getText().toString(), TGT.getText().toString());
//
//                dataTransferInterface.setValues(bucketTitle.getText().toString(), TGT.getText().toString());
//            }
//        });
//////
//        FPAY.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //Toast.makeText(view.getContext(), bucketTitle.getText() + " " + FPAY.getText(), Toast.LENGTH_SHORT).show();
//
//                dataTransferInterface.setValues(bucketTitle.getText().toString(), FPAY.getText().toString());
//
//            }
//        });
//        PPAY.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(view.getContext(), bucketTitle.getText() + " " + PPAY.getText(), Toast.LENGTH_SHORT).show();
//            }
//        });
//        NPAY.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(view.getContext(), bucketTitle.getText() + " " + NPAY.getText(), Toast.LENGTH_SHORT).show();
//            }
//        });
//        NVST.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(view.getContext(), bucketTitle.getText() + " " + NVST.getText(), Toast.LENGTH_SHORT).show();
//            }
//        });


        return view;
    }


}
