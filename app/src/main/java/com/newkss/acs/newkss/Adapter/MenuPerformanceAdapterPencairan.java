package com.newkss.acs.newkss.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.newkss.acs.newkss.R;

import java.util.ArrayList;

/**
 * Created by acs on 19/04/17.
 */

public class MenuPerformanceAdapterPencairan extends BaseAdapter {
    private ArrayList<Pencairan> listData = new ArrayList<>();
    private Context mContext;

    public MenuPerformanceAdapterPencairan(Context context, ArrayList<Pencairan> list) {
        mContext = context;
        listData = list;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        View view;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.listrowpencairan, null);

        final TextView PBL = (TextView) view.findViewById(R.id.pbllistrow);
        final TextView PBI = (TextView) view.findViewById(R.id.ppilistrow);
        final TextView SSV = (TextView) view.findViewById(R.id.ssvlistrow);
        final TextView BSV = (TextView) view.findViewById(R.id.bsvlistrow);

//


        PBL.setTextColor(Color.parseColor("#5dc0c1"));
        PBI.setTextColor(Color.parseColor("#5dc0c1"));
        SSV.setTextColor(Color.parseColor("#5dc0c1"));
        BSV.setTextColor(Color.parseColor("#5dc0c1"));


        PBL.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        PBI.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        SSV.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        BSV.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);


        PBL.setText(listData.get(i).getPBL());
        PBI.setText(listData.get(i).getPBI());
        SSV.setText(listData.get(i).getSSV());
        BSV.setText(listData.get(i).getBSV());

        PBL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), PBL.getText() + " PBL", Toast.LENGTH_SHORT).show();
            }
        });

        PBI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), PBI.getText() + " PBI", Toast.LENGTH_SHORT).show();
            }
        });
        SSV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), SSV.getText() + " SSV", Toast.LENGTH_SHORT).show();
            }
        });
        BSV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), BSV.getText() + " BSV", Toast.LENGTH_SHORT).show();
            }
        });


        return view;
    }
}
