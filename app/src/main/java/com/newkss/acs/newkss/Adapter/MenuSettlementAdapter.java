package com.newkss.acs.newkss.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Function;


import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Erdy on 5/8/2017.
 */
public class MenuSettlementAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<Settle_Detil> settlementList = null;
    private ArrayList<Settle_Detil> arraylist;

    public MenuSettlementAdapter(Context context, ArrayList<Settle_Detil> list) {
        mContext = context;
        settlementList = list;
        inflater = LayoutInflater.from(mContext);

    }

    @Override
    public int getCount() {
        return settlementList.size();
    }

    @Override
    public Object getItem(int i) {
        return settlementList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder {
        TextView nama;
        TextView norek;
        TextView nominal;
        TextView noLoan;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listsettlement, viewGroup, false);

            holder.noLoan = (TextView) view.findViewById(R.id.txtNoLoanListSettlement);
            holder.nama = (TextView) view.findViewById(R.id.txtNamaListSettlement);
            holder.norek = (TextView) view.findViewById(R.id.txtNoRekListSettlement);
            holder.nominal = (TextView) view.findViewById(R.id.txtNominalListSettlement);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        String nama = settlementList.get(i).getNama();
        String namaCut = "";
        if (nama.length() <= 14) {
            namaCut = nama.substring(0, nama.length());
        } else if (nama.length() > 14) {
            namaCut = nama.substring(0, 14);
        }

        holder.noLoan.setText(":" + settlementList.get(i).getNo_loan());
        holder.nama.setText(":" + namaCut);
        holder.norek.setText(":" + settlementList.get(i).getNo_rek());
        holder.nominal.setText(":" + Function.returnSeparatorComaWithout(settlementList.get(i).getNominal_bayar()));

        return view;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        settlementList.clear();
        if (charText.length() == 0) {
            settlementList.addAll(arraylist);
        } else {
            for (Settle_Detil wp : arraylist) {
                if (wp.getNama().toLowerCase(Locale.getDefault()).contains(charText)) {
                    settlementList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
