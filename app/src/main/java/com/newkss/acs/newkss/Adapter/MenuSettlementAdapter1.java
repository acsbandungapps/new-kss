package com.newkss.acs.newkss.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.newkss.acs.newkss.Model.SettlementKe;

import java.util.ArrayList;

/**
 * Created by acs on 6/15/17.
 */

public class MenuSettlementAdapter1 extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<SettlementKe> settlementList = null;
    private ArrayList<SettlementKe> arraylist;
    public MenuSettlementAdapter1(Context context, ArrayList<SettlementKe> list) {
        mContext = context;
        settlementList = list;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return settlementList.size();
    }

    @Override
    public Object getItem(int i) {
        return settlementList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public class ViewHolder {


    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }
}
