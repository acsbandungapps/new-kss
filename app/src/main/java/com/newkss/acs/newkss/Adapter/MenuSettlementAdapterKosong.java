package com.newkss.acs.newkss.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newkss.acs.newkss.Model.Settlement;
import com.newkss.acs.newkss.R;

import java.util.ArrayList;

/**
 * Created by acs on 7/17/17.
 */

public class MenuSettlementAdapterKosong extends BaseAdapter {
    Context mContext;
    LayoutInflater inflater;

    public MenuSettlementAdapterKosong(Context context, ArrayList<Settlement> list) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public class ViewHolder {
        TextView nama;

    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listsettlementkosong, viewGroup, false);

            holder.nama = (TextView) view.findViewById(R.id.txtKosong);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        return view;
    }
}
