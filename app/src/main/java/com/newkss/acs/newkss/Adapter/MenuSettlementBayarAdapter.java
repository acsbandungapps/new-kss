package com.newkss.acs.newkss.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.newkss.acs.newkss.DataTransferInterface;
import com.newkss.acs.newkss.Model.Settlement;
import com.newkss.acs.newkss.Model.SettlementKe;
import com.newkss.acs.newkss.R;

import java.util.ArrayList;

/**
 * Created by acs on 6/22/17.
 */

public class MenuSettlementBayarAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<SettlementKe> settlementList = null;
    private ArrayList<SettlementKe> arraylist=new ArrayList<>();

    DataTransferInterface dti;
    SettlementKe sske;
    public MenuSettlementBayarAdapter(Context context, ArrayList<SettlementKe> list, DataTransferInterface data) {
        mContext = context;
        settlementList = list;
        inflater = LayoutInflater.from(mContext);
        dti = data;
    }

    @Override
    public int getCount() {
        return settlementList.size();
    }

    @Override
    public Object getItem(int i) {
        return settlementList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder {
        public  EditText settlementKe;
        EditText nominalDisetor;
        Button btnUpload;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listsettlementbayar, viewGroup, false);

            holder.settlementKe = (EditText) view.findViewById(R.id.etSettlementKe);
            holder.nominalDisetor = (EditText) view.findViewById(R.id.etNominalDisetor);
            holder.btnUpload = (Button) view.findViewById(R.id.btnUpload);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }


        //holder.settlementKe.setText(String.valueOf(settlementList.size()));
        //holder.settlementKe.setText("");

        for(int in=0;in<getCount();in++){
//            sske=new SettlementKe();
//            sske.setNominal(holder.nominalDisetor.getText().toString());
            arraylist.add((SettlementKe)getItem(i));

        }
        dti.setValues(arraylist);
        i++;
        holder.settlementKe.setText(String.valueOf(i));
        holder.nominalDisetor.setText("");



        holder.btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //System.out.println("HOLDER " + holder.nominalDisetor.getText().toString());
                // holder.nominalDisetor.getText().get
            }
        });


//        arraylist.add();
//        dti.setValues(arraylist);
        return view;
    }
}
