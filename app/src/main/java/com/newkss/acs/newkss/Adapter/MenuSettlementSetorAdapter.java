package com.newkss.acs.newkss.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.newkss.acs.newkss.Model.Master_Settle;
import com.newkss.acs.newkss.Model.Settlement;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.test.tambahSetor;

import java.util.ArrayList;

/**
 * Created by acs on 9/19/17.
 */

public class MenuSettlementSetorAdapter extends BaseAdapter {
    Context mContext;
    LayoutInflater inflater;
    private ArrayList<Settlement> settlementList = null;
    private ArrayList<Settlement> arraylist;

    tambahSetor listener;
    private String stra, strb;

    public MenuSettlementSetorAdapter(Context context, ArrayList<Settlement> list) {
        mContext = context;
        settlementList = list;
        inflater = LayoutInflater.from(mContext);
        this.listener = (tambahSetor) context;
    }

    @Override
    public int getCount() {
        return settlementList.size();
    }

    @Override
    public Object getItem(int i) {
        return settlementList.get(i);
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    public class ViewHolder {
        EditText setoran;
        EditText nominal;
        Button upload;
        Button btnHapusSetor;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listsettlementsetor, viewGroup, false);

            holder.setoran = (EditText) view.findViewById(R.id.txtSetoranKe);
            holder.nominal = (EditText) view.findViewById(R.id.txtNominalDisetor);
            holder.upload = (Button) view.findViewById(R.id.btnUploadSetor);
            holder.btnHapusSetor = (Button) view.findViewById(R.id.btnHapusSetor);
            //System.out.println("position: "+settlementList.size());
            holder.setoran.addTextChangedListener(new GenericTextWatcher(holder.setoran));
            holder.setoran.setId('a');
            holder.setoran.setText(String.valueOf(settlementList.size()));
            holder.nominal.addTextChangedListener(new GenericTextWatcher(holder.nominal));
            holder.nominal.setId('b');
            holder.nominal.setText(settlementList.get(i).getTotal_settle());
            holder.btnHapusSetor.setId('c');
            //holder.btnHapusSetor.setTag(holder);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.btnHapusSetor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                System.out.println("Position: " + i);
//                view.getTag();
//                settlementList.remove(i);
//                notifyDataSetChanged();

                //int pos = (int)view.getTag();
                System.out.println("List pos: "+i);
                System.out.println(settlementList.get(i).getTotal_settle());
            }
        });
        holder.upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "aaa", Toast.LENGTH_SHORT).show();
                //
            }
        });

        return view;
    }

    private class GenericTextWatcher implements TextWatcher {

        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            if (view.getId() == 'a') {
                listener.setSetoranke(text);
            } else if (view.getId() == 'b') {
                listener.setNominal(text);
            }
        }

    }
//    Context mContext;
//    LayoutInflater inflater;
//    private ArrayList<Master_Settle> settlementList = null;
//    private ArrayList<Master_Settle> arraylist;
//
//    tambahSetor listener;
//    public MenuSettlementSetorAdapter(Context context, ArrayList<Master_Settle> list) {
//        mContext = context;
//        settlementList = list;
//        inflater = LayoutInflater.from(mContext);
//
//    }
//
//    @Override
//    public int getCount() {
//        return settlementList.size();
//    }
//
//    @Override
//    public Object getItem(int i) {
//        return settlementList.get(i);
//    }
//
//    @Override
//    public int getViewTypeCount() {
//        return getCount();
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return position;
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return 0;
//    }
//
//
//    public class ViewHolder {
//        EditText setoran;
//        EditText nominal;
//        Button upload;
//    }
//
//    @Override
//    public View getView(int i, View view, ViewGroup viewGroup) {
//        final ViewHolder holder;
//        if (view == null) {
//            holder = new ViewHolder();
//            view = inflater.inflate(R.layout.listsettlementsetor, viewGroup, false);
//
//            holder.setoran = (EditText) view.findViewById(R.id.txtSetoranKe);
//            holder.nominal = (EditText) view.findViewById(R.id.txtNominalDisetor);
//            holder.upload = (Button) view.findViewById(R.id.btnUploadSetor);
//
//            view.setTag(holder);
//        } else {
//            holder = (ViewHolder) view.getTag();
//        }
//
////        holder.setoran.setText(settlementList.get(i).getId_settle());
////        holder.nominal.setText(settlementList.get(i).getTotal());
//        holder.upload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(view.getContext(),"aaa",Toast.LENGTH_SHORT).show();
//                //listener.setValues(holder.setoran.getText().toString(),holder.nominal.getText().toString());
//            }
//        });
//
//        return view;
//    }
}
