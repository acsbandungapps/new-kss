package com.newkss.acs.newkss.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.ModelBaru.DataBucketBooked;
import com.newkss.acs.newkss.ModelBaru.Data_Pencairan;
import com.newkss.acs.newkss.R;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by acs on 7/11/17.
 */

public class MenuSudahSurveyAdapter extends BaseAdapter {


    boolean[] itemChecked;
//    int position;

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<Detail_Inventory> dataDetInvList = null;
    private ArrayList<Detail_Inventory> arraylist;

    Data_Pencairan queryDP = new Data_Pencairan();
    DataBucketBooked dbb = new DataBucketBooked();

    public MenuSudahSurveyAdapter(Context context, ArrayList<Detail_Inventory> list) {
        mContext = context;
        dataDetInvList = list;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(dataDetInvList);
        if (dataDetInvList != null) {
            itemChecked = new boolean[dataDetInvList.size()];
        }
    }

    @Override
    public int getCount() {
        return dataDetInvList.size();
    }

    @Override
    public Object getItem(int i) {
        return dataDetInvList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    private class ViewHolder {
        TextView nama;
        TextView cycle;
        TextView os;
        TextView tglcair;
        TextView alamat;
        TextView jarak;
        TextView noloan;

    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listsudahsurvey, viewGroup, false);

            holder.noloan=(TextView) view.findViewById(R.id.txtNoLoan);
            holder.tglcair = (TextView) view.findViewById(R.id.txtTglCair);
            holder.nama = (TextView) view.findViewById(R.id.txtNama);
            holder.cycle = (TextView) view.findViewById(R.id.txtCycle);
            holder.os = (TextView) view.findViewById(R.id.txtOS);
            holder.alamat = (TextView) view.findViewById(R.id.txtAlamat);
            holder.jarak = (TextView) view.findViewById(R.id.txtJarak);


            view.setBackgroundResource(android.R.color.transparent);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        //dpt no loan dan id_data_pencairan
        System.out.println("dataDetInvList" + dataDetInvList.get(position).getNo_loan());
        String id = queryDP.getIdDataPencairanFromNoLoan(dataDetInvList.get(position).getNo_loan());
        final Data_Pencairan dp = queryDP.getDataPencairanFromID(id);

        holder.noloan.setText("No Loan : "+dataDetInvList.get(position).getNo_loan());
        holder.nama.setText("Nama :" + dp.getNama());
        holder.cycle.setText("Cycle :" + dp.getCycle());
        holder.os.setText("OS :" + dp.getOs());
        holder.tglcair.setText("Tgl Cair :");
        holder.alamat.setText("Alamat :" + dp.getAlamat());
        holder.jarak.setText("Jarak :" + dataDetInvList.get(position).getJarak_tempuh());


        return view;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        dataDetInvList.clear();
        if (charText.length() == 0) {
            dataDetInvList.addAll(arraylist);
        } else {
            for (Detail_Inventory wp : arraylist) {
                if (wp.getNo_loan().toLowerCase(Locale.getDefault()).contains(charText)||wp.getNama_debitur().toLowerCase(Locale.getDefault()).contains(charText)) {
//                    Data_Pencairan dpbaru = new Data_Pencairan();
//                    dpbaru=queryDP.getDataPencairanFromID(wp.getId_data_pencairan());
                    dataDetInvList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
