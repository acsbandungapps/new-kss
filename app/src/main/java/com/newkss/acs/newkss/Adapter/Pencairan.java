package com.newkss.acs.newkss.Adapter;

/**
 * Created by acs on 19/04/17.
 */

public class Pencairan {


    public String PBL;
    public String PBI;
    public String SSV;
    public String BSV;

    public String getPBL() {
        return PBL;
    }

    public void setPBL(String PBL) {
        this.PBL = PBL;
    }

    public String getPBI() {
        return PBI;
    }

    public void setPBI(String PBI) {
        this.PBI = PBI;
    }

    public String getSSV() {
        return SSV;
    }

    public void setSSV(String SSV) {
        this.SSV = SSV;
    }

    public String getBSV() {
        return BSV;
    }

    public void setBSV(String BSV) {
        this.BSV = BSV;
    }
}
