package com.newkss.acs.newkss.Fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.newkss.acs.newkss.R;

/**
 * Created by acs on 6/13/17.
 */

public class FragmentPhoto extends DialogFragment {

    Button btnGantiPhoto;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.fragmentphoto);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        btnGantiPhoto = (Button) dialog.findViewById(R.id.btnGantiPhoto);
        btnGantiPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int nilai = getArguments().getInt("num");
                Toast.makeText(getActivity().getApplicationContext(), nilai, Toast.LENGTH_SHORT).show();
            }
        });
        return dialog;
    }
}
