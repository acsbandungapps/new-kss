package com.newkss.acs.newkss;

import android.content.pm.PackageInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText etUsernameLogin, etPasswordLogin;
    private TextView txtLupaPasswordLogin, txtVersionLogin;
    private Button btnResetLogin, btnLogin;
    private CheckBox chkIngatSayaLogin;
    private String username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menulogin);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        initUI();
    }

    private void initUI() {
        etUsernameLogin = (EditText) findViewById(R.id.etUsernameLogin);
        etPasswordLogin = (EditText) findViewById(R.id.etPasswordLogin);
        txtVersionLogin = (TextView) findViewById(R.id.txtVersionLogin);
        txtLupaPasswordLogin = (TextView) findViewById(R.id.txtLupaPasswordLogin);
        btnResetLogin = (Button) findViewById(R.id.btnResetLogin);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        chkIngatSayaLogin = (CheckBox) findViewById(R.id.chkIngatSayaLogin);

        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            txtVersionLogin.setText("Versi "+packageInfo.versionName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        txtLupaPasswordLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Tes", Toast.LENGTH_SHORT).show();
            }
        });


    }
}
