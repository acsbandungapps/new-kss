package com.newkss.acs.newkss.Menu;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.JSONParser;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by acs on 7/26/17.
 */

public class MenuChangePassword extends ParentActivity {

    Button btnKirimMenuChangePassword;
    EditText etPasswordLama, etPasswordBaru, etKonfPasswordBaru;
    ImageView imgBackMenuChangePassword;
    Context mContext;
    String passLama, passBaru, konfPassBaru, usernameShared, resultfromJson;
    ProgressDialog progressDialog;
    SharedPreferences shared;
    JSONParser jsonParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuchangepassword);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        try {
            initUI();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initUI() {
        mContext = this;

        etPasswordLama = (EditText) findViewById(R.id.etPasswordLama);
        etPasswordBaru = (EditText) findViewById(R.id.etPasswordBaru);
        etKonfPasswordBaru = (EditText) findViewById(R.id.etKonfPasswordBaru);

        imgBackMenuChangePassword = (ImageView) findViewById(R.id.imgBackMenuChangePassword);
        imgBackMenuChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MenuChangePassword.this, MenuLogin.class);
                startActivity(i);
                finish();
            }
        });

        etPasswordLama.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_RIGHT = 2;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (etPasswordLama.getRight() - etPasswordLama.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        if (etPasswordLama.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                            etPasswordLama.setInputType(InputType.TYPE_CLASS_TEXT |
                                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        } else {
                            etPasswordLama.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        }
                        etPasswordLama.setSelection(etPasswordLama.getText().length());
                        return true;
                    }
                }
                return false;
            }
        });

        etPasswordBaru.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_RIGHT = 2;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (etPasswordBaru.getRight() - etPasswordBaru.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        if (etPasswordBaru.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                            etPasswordBaru.setInputType(InputType.TYPE_CLASS_TEXT |
                                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        } else {
                            etPasswordBaru.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        }
                        etPasswordBaru.setSelection(etPasswordBaru.getText().length());
                        return true;
                    }
                }
                return false;
            }
        });

        etKonfPasswordBaru.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_RIGHT = 2;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (etKonfPasswordBaru.getRight() - etKonfPasswordBaru.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        if (etKonfPasswordBaru.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                            etKonfPasswordBaru.setInputType(InputType.TYPE_CLASS_TEXT |
                                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        } else {
                            etKonfPasswordBaru.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        }
                        etKonfPasswordBaru.setSelection(etKonfPasswordBaru.getText().length());
                        return true;
                    }
                }
                return false;
            }
        });

        btnKirimMenuChangePassword = (Button) findViewById(R.id.btnKirimMenuChangePassword);
        btnKirimMenuChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                if (shared.contains(Constant.SHARED_USERNAME)) {
                    usernameShared = (shared.getString(Constant.SHARED_USERNAME, ""));
                }
                passLama = etPasswordLama.getText().toString();
                passBaru = etPasswordBaru.getText().toString();
                konfPassBaru = etKonfPasswordBaru.getText().toString();
                if (passLama.isEmpty() || passBaru.isEmpty() || konfPassBaru.isEmpty()) {
                    Function.showAlert(mContext, "Mohon Lengkapi Semua Kolom");
                } else if (!passBaru.equals(konfPassBaru)) {
                    Function.showAlert(mContext, "Password Baru dan Konfirmasi Password Berbeda");
                } else {
                    new execChangePass(mContext).execute();
                }

            }
        });
    }

    class execChangePass extends AsyncTask<String, String, String> {

        Context context;

        private execChangePass(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected String doInBackground(String... strings) {
            JSONObject json = new JSONObject();
            try {
                String header = Function.getImei(mContext) + ":" + Function.getWaktuSekarangMilis();
                System.out.println("Header: " + header);
                System.out.println("Header base64: " + new String(Function.encodeBase64(header)));

                json.put(Constant.USERNAME, usernameShared);
                json.put("password_lama", Function.convertStringtoMD5(passLama));
                json.put("password_baru", Function.convertStringtoMD5(passBaru));
                System.out.println("Json Kirim: " + json.toString());
                resultfromJson = jsonParser.HttpRequestPost(Constant.SERVICE_CHANGE_PASS, json.toString(), Constant.TimeOutConnection, new String(Function.encodeBase64(header)));
                System.out.println("Hasil Json: " + resultfromJson);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle("Mengirim Data");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            try {
                JSONObject jsonAll = new JSONObject(result);
                String keterangan = jsonAll.getString("keterangan");
                String rc = jsonAll.getString("rc");
                if (rc.equals("00")) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Pesan");
                    builder.setMessage(keterangan);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            shared = getApplicationContext().getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                            SharedPreferences.Editor editor = shared.edit();
                            editor.putString(Constant.SHARED_PASSWORD, Function.convertStringtoMD5(passBaru));
                            editor.commit();
                            Intent i = new Intent(MenuChangePassword.this, MenuLogin.class);
                            startActivity(i);
                            finish();
                        }
                    });

                    AlertDialog alert1 = builder.create();
                    alert1.show();
                } else if (rc.equals("T1")) {
                    Function.showAlert(mContext, keterangan);
                } else if (rc.equals("T2")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setIcon(R.drawable.ic_warning_black_24dp);
                    builder.setTitle("Pesan");
                    builder.setMessage(keterangan);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
//                            shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                            shared.edit().clear().commit();
                            Intent i = new Intent(MenuChangePassword.this, MenuInisialisasi.class);
                            startActivity(i);
                            finish();
                        }
                    });

                    AlertDialog alert1 = builder.create();
                    alert1.show();
                } else if (rc.equals("T3")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setIcon(R.drawable.ic_warning_black_24dp);
                    builder.setTitle("Pesan");
                    builder.setMessage(keterangan);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
//                            shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                            shared.edit().clear().commit();
                            Intent i = new Intent(MenuChangePassword.this, MenuInisialisasi.class);
                            startActivity(i);
                            finish();
                        }
                    });

                    AlertDialog alert1 = builder.create();
                    alert1.show();
                } else if (rc.equals("T4")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Pesan");
                    builder.setMessage(keterangan);
                    builder.setIcon(R.drawable.ic_warning_black_24dp);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
//                            shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                            shared.edit().clear().commit();
                            Intent i = new Intent(MenuChangePassword.this, MenuInisialisasi.class);
                            startActivity(i);
                            finish();
                        }
                    });
                    AlertDialog alert1 = builder.create();
                    alert1.show();

                }
                else if (rc.equals("T5")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Pesan");
                    builder.setMessage(keterangan);
                    builder.setIcon(R.drawable.ic_warning_black_24dp);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
//                            shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                            shared.edit().clear().commit();
                            Intent i = new Intent(MenuChangePassword.this, MenuInisialisasi.class);
                            startActivity(i);
                            finish();
                        }
                    });
                    AlertDialog alert1 = builder.create();
                    alert1.show();

                }else {
                    Function.showAlert(mContext, keterangan);
                }
            } catch (JSONException e) {
                Function.showAlert(mContext, e.getMessage());
                e.printStackTrace();
            } catch (Exception e) {
                Function.showAlert(mContext,result);
                e.printStackTrace();
            }
        }
    }
}
