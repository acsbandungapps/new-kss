package com.newkss.acs.newkss.Menu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.newkss.acs.newkss.Adapter.AdapterDataOffline;
import com.newkss.acs.newkss.Adapter.MenuBucketAdapter;
import com.newkss.acs.newkss.Model.Pending;
import com.newkss.acs.newkss.Model.Photo;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.JSONParser;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by acs on 1/16/18.
 */

public class MenuDataOffline extends Activity {
    LinearLayout llPopupMenuOffline;
    ImageView imgBackMenuOffline;
    AdapterDataOffline adapterDataOffline;
    Context mContext;
    ListView lvDataOffline;
    ArrayList<Pending> listPending = new ArrayList<>();
    Pending pDao = new Pending();
    Pending pData;
    String resultfromJson = "";
    JSONParser jsonParser = new JSONParser();
    Settle_Detil dataSettleDetil;
    Settle_Detil querySettle_detil = new Settle_Detil();
    String keterangan = "";
    String rc = "";
    ProgressDialog progressDialog;
    Pending dataPending;
    SharedPreferences shared;
    String namaShared = "";
    String divisiShared = "";
    TextView txtDivisiMenuOffline, txtNamaMenuOffline;

    //kalo status 1 data offline berhasil terkirim
    //kalo status 2 data offline belum dikirim
    //kalo status 0 data offline coba dikirim tp gagal
    //kalo status 3 data offline dihapus oleh user
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuoffline);
        initUI();
    }

    private void initUI() {
        mContext = this;
        lvDataOffline = (ListView) findViewById(R.id.lvDataOffline);
        llPopupMenuOffline = (LinearLayout) findViewById(R.id.llPopupMenuOffline);
        imgBackMenuOffline = (ImageView) findViewById(R.id.imgBackMenuOffline);
        txtNamaMenuOffline = (TextView) findViewById(R.id.txtNamaMenuOffline);
        txtDivisiMenuOffline = (TextView) findViewById(R.id.txtDivisiMenuOffline);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtNamaMenuOffline.setText(namaShared);
            txtDivisiMenuOffline.setText(divisiShared);
        }

        MySQLiteHelper.initDB(mContext);
        pDao.synchronizeTablePending(Constant.TABLE_PENDING, "desc", "TEXT");

        refreshList();


        llPopupMenuOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(MenuDataOffline.this, llPopupMenuOffline);
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_REFRESH)) {
                            // initUI();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            Intent i = new Intent(getApplicationContext(), MenuLogin.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });

        imgBackMenuOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, MenuUtama.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });

        lvDataOffline.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pData = new Pending();
                pData = (Pending) parent.getAdapter().getItem(position);
                //Toast.makeText(mContext, p.getId_pending(), Toast.LENGTH_SHORT).show();
                String namaDeb = "";
                String noLoan = "";

                System.out.println("pData " + pData.getId_pending());
                //dataPending=pDao.getDataPendingById(pData.getId_pending());
                //System.out.println("Ketetetetet "+dataPending.getKet_update());
                try {
                    JSONObject jsonObject = new JSONObject(pData.getData());
                    JSONArray jsonArray = jsonObject.getJSONArray("hasil_kunjungan");

                    for (int in = 0; in < jsonArray.length(); in++) {
                        JSONObject jsonObj = jsonArray.getJSONObject(in);
                        namaDeb = jsonObj.getString("nama_debitur");
                        noLoan = jsonObj.getString("no_loan");
                    }
                } catch (Exception e) {
                    Function.showAlert(mContext, e.getMessage());
                    e.printStackTrace();
                }
                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Konfirmasi Kirim Data");
                builder.setMessage("Nama Debitur: \n" + namaDeb + "\nNo Loan: \n" + noLoan);
                builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            Pending pDataNew = pDao.getDataPendingById(pData.getId_pending());
                            if (pDataNew.getStatus().equals("1")) {
                                Function.showAlert(mContext, "Data Sudah Pernah Dikirim");
                                refreshList();
                            } else {
                                if (pData.getKet_update().equals("Collect")) {
                                    //Function.showAlert(mContext,"Collect");
                                    new SendOfflineCollect(mContext).execute();
                                } else if (pData.getKet_update().equals("Visit")) {
//                                Function.showAlert(mContext,"Visit");
                                    new SendOfflineVisit(mContext).execute();
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });
                builder.setNeutralButton("Hapus Data", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        pDao.hapusPendingData(pData.getId_pending());
                        refreshList();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
                refreshList();
                if (listPending.size() == 0) {
                    Intent i = new Intent(mContext, MenuUtama.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }

            }
        });
    }

    private void refreshList() {
        getListDataPending();
//        adapterDataOffline = new AdapterDataOffline(mContext, listPending);
//        lvDataOffline.setAdapter(adapterDataOffline);
    }

    private void getListDataPending() {
        listPending = pDao.getArrayListDataPending();
        for (Pending pA : listPending) {
            System.out.println("listpending" + pA.getId_pending());
        }
        System.out.println("datapen" + listPending);
        if (listPending.isEmpty()) {
            lvDataOffline.setEmptyView(findViewById(R.id.emptyElement));
        } else {
            adapterDataOffline = new AdapterDataOffline(mContext, listPending);
            lvDataOffline.setAdapter(adapterDataOffline);
        }

    }

    private class SendOfflineVisit extends AsyncTask<String, String, String> {
        Context context;

        private SendOfflineVisit(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Mengirim Data...");
            progressDialog.setTitle("Pesan");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }


        @Override
        protected String doInBackground(String... strings) {
            try {
                JSONObject json = new JSONObject(pData.getDataPending().getData());
                //json.put("password", passwordShared);
                System.out.println("New Json Visit: " + json.toString());
                resultfromJson = jsonParser.HttpRequestPost(pData.getUrl(), pData.getData(), Constant.TimeOutConnection, pData.getHeader());
                return resultfromJson;
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            System.out.println("Masuk Async Visit");
            System.out.println("Result: " + result);

            if (result.contains(Constant.CONNECTION_LOST)) {
                System.out.println(Constant.CONNECTION_LOST);
            } else if (result.contains(Constant.CONNECTION_ERROR)) {
                System.out.println(Constant.CONNECTION_ERROR);
            } else {
                try {
                    JSONObject jsonR = new JSONObject(result);
                    String rc = jsonR.getString("rc");
                    String keterangan = jsonR.getString("keterangan");
                    if (rc.equals("00")) {
                        System.out.println("Data visit Terkirim");
                        //    queryPhoto.updateStatusPhoto(dataPending.getJson_update());
                        pDao.updateStatusPendingData(pData.getId_pending());
                    } else if (rc.equals("T1")) {
                        Intent i = new Intent(context, MenuLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();

                    } else if (rc.equals("T2")) {
                        Intent i = new Intent(context, MenuLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();

//                        shared = context.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                        shared.edit().clear().commit();

                    } else if (rc.equals("T3")) {
                        Intent i = new Intent(context, MenuLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();

//                        shared = context.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                        shared.edit().clear().commit();
                    } else if (rc.equals("T4")) {
                        Intent i = new Intent(context, MenuLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();

//                        shared = context.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                        shared.edit().clear().commit();
                    } else if (rc.equals("T5")) {
                        Intent i = new Intent(context, MenuLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();
//                        shared = context.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                        shared.edit().clear().commit();
                    } else {
                        //Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();
                        Function.showAlert(mContext, keterangan);
                        pDao.updateStatusPendingDataFailed(pData.getId_pending());
                        pDao.updateKeteranganPendingData(pData.getId_pending(), keterangan + " " + (rc));
                    }
                    refreshList();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                    Function.showAlert(mContext, keterangan);
                    pDao.updateStatusPendingDataFailed(pData.getId_pending());
                    pDao.updateKeteranganPendingData(pData.getId_pending(), e.getMessage());
                }
            }
        }
    }

    private class SendOfflineCollect extends AsyncTask<String, String, String> {
        Context context;

        private SendOfflineCollect(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Mengirim Data...");
            progressDialog.setTitle("Pesan");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                JSONObject json = new JSONObject(pData.getData());
//                JSONObject json = new JSONObject(pData.getDataPending().getData());
                //json.put("password", passwordShared);
                System.out.println("New Json Collect: " + pData.getData());
                resultfromJson = jsonParser.HttpRequestPost(pData.getUrl(), pData.getData(), Constant.TimeOutConnection, pData.getHeader());
                return resultfromJson;
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            System.out.println("Masuk Async Collect");
            System.out.println("Result: " + result);
            progressDialog.dismiss();
            if (result.contains(Constant.CONNECTION_LOST)) {
                System.out.println(Constant.CONNECTION_LOST);
                Function.showAlert(mContext, Constant.CONNECTION_LOST);
            } else if (result.contains(Constant.CONNECTION_ERROR)) {
                System.out.println(Constant.CONNECTION_ERROR);
                Function.showAlert(mContext, Constant.CONNECTION_ERROR);
            } else {
                try {
                    JSONObject jsonR = new JSONObject(result);
                    rc = jsonR.getString("rc");
                    keterangan = jsonR.getString("keterangan");
                    if (rc.equals("00")) {

                        JSONObject json = new JSONObject(pData.getJson_update());
                        String no_loan = json.getString("no_loan");
                        String no_rek = json.getString("no_rek");
                        String nama = json.getString("nama");
                        String nominal_bayar = json.getString("nominal_bayar");
                        String total_kewajiban = json.getString("total_kewajiban");
                        String id_det_inv = json.getString("id_det_inv");
                        String status = json.getString("status");
                        String kode_kunjungan = json.getString("kode_kunjungan");

                        if (kode_kunjungan.equals("NN") || kode_kunjungan.equals("NP") || kode_kunjungan.equals("JB")) {

                        } else if (kode_kunjungan.equals("FP") || kode_kunjungan.equals("PP")) {
                            //masukin data ke settle detil, data ini buat nanti settlement
                            dataSettleDetil = new Settle_Detil();
                            dataSettleDetil.setNo_loan(no_loan);
                            dataSettleDetil.setNo_rek(no_rek);
                            dataSettleDetil.setNama(nama);
                            dataSettleDetil.setNominal_bayar(nominal_bayar);
                            dataSettleDetil.setTotal_kewajiban(total_kewajiban);
                            dataSettleDetil.setId_detail_inventory(id_det_inv);
                            dataSettleDetil.setStatus("0");
//                            dataSettleDetil.setSisa_bayar(sisa_bayar);
                            querySettle_detil.insertSettleDetil(dataSettleDetil);
//                            Function.showAlert(mContext, keterangan);
                        }

                        pDao.updateStatusPendingData(pData.getId_pending());
                        Function.showAlert(mContext, keterangan);
                        refreshList();
                        Photo queryPhoto = new Photo();
                        queryPhoto.updateStatusPhoto(no_loan);
//                        Function.showAlert(mContext,keterangan);
                    } else {
                        Function.showAlert(mContext, keterangan);
                        refreshList();
                        pDao.updateStatusPendingDataFailed(pData.getId_pending());
                        pDao.updateKeteranganPendingData(pData.getId_pending(), keterangan + " " + (rc));
//                        Function.showAlert(mContext,keterangan);
                    }

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                    Function.showAlert(mContext, keterangan);
                    pDao.updateStatusPendingDataFailed(pData.getId_pending());
                    pDao.updateKeteranganPendingData(pData.getId_pending(), e.getMessage());
                }
            }
        }
    }
}
