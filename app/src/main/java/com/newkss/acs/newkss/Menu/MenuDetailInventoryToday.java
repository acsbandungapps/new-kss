package com.newkss.acs.newkss.Menu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.newkss.acs.newkss.MenuSettlement.MenuSettlement;
import com.newkss.acs.newkss.Model.Detail_Bucket;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.Function;

/**
 * Created by acs on 02/05/17.
 */

public class MenuDetailInventoryToday extends ParentActivity {
    EditText etNamaDebiturMenuDetailInventoryToday, etAlamatUsahaMenuDetailInventoryToday, etNoHandphoneMenuDetailInventoryToday, etNoTelpRumahMenuDetailInventoryToday, etEmailMenuDetailInventoryToday, etJenisPinjamanMenuDetailInventoryToday, etNoLoanRumahMenuDetailInventoryToday, etDPDTodayMenuDetailInventoryToday, etNoRekMenuDetailInventoryToday;
    EditText etAlamatRumahMenuDetailInventoryToday, ettanggalPTPMenuDetailInventoryToday, etnominalPTPMenuDetailInventoryToday, etsumberPembayaranMenuDetailInventoryToday, etactionPlanMenuDetailInventoryToday, etnominalJanjiBayarTerakhirMenuDetailInventoryToday, ettanggalJanjiBayarTerakhirMenuDetailInventoryToday, etnominalPembayaranTerakhirMenuDetailInventoryToday, ettanggalPembayaranTerakhirMenuDetailInventoryToday, etkeberadaanJaminanMenuDetailInventoryToday, ettanggalGajianMenuDetailInventoryToday, etPekerjaanMenuDetailInventoryToday, etGenderMenuDetailInventoryToday, etpolaBayarMenuDetailInventoryToday, etnoTelpRekomendatorMenuDetailInventoryToday, etnamaRekomendatorMenuDetailInventoryToday, etnoTelpUplinerMenuDetailInventoryToday, etnamaUplinerMenuDetailInventoryToday, etnoTelpMogenMenuDetailInventoryToday, etnamaMogenMenuDetailInventoryToday, etnamaEconMenuDetailInventoryToday, etresumeNasabahMenuDetailInventoryToday, ettglPemberianspMenuDetailInventoryToday, ethistorySPMenuDetailInventoryToday, ettglJatuhTempoMenuDetailInventoryToday, etTenorMenuDetailInventoryToday, etangsurankeMenuDetailInventoryToday, etyangharusDibayarMenuDetailInventoryToday, etTotalKewajibanMenuDetailInventoryToday, etDendaMenuDetailInventoryToday, etBungaMenuDetailInventoryToday, etPokokMenuDetailInventoryToday, etKewajibanMenuDetailInventoryToday, etSaldoMinimumMenuDetailInventoryToday, etSaldoMenuDetailInventoryToday, etospinjamanRumahMenuDetailInventoryToday, etAngsuranMenuDetailInventoryToday;
    Detail_Bucket d = new Detail_Bucket();
    Detail_Inventory di = new Detail_Inventory();
    Detail_Inventory diData = new Detail_Inventory();

    String id, nama, type;
    ImageView imgBackMenuDetailInventoryToday;

    TextView txtNamaMenuDetailInventoryToday, txtDivisiMenuDetailInventoryToday;
    String namaShared, divisiShared;
    SharedPreferences shared;
    Context mContext;
    LinearLayout llMenuDetailInventoryTodayPopup;
    Settle_Detil querySD = new Settle_Detil();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menudetailinventorytoday);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = this;
        try {
            initUI();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initUI() {
        txtDivisiMenuDetailInventoryToday = (TextView) findViewById(R.id.txtDivisiMenuDetailInventoryToday);
        txtNamaMenuDetailInventoryToday = (TextView) findViewById(R.id.txtNamaMenuDetailInventoryToday);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtNamaMenuDetailInventoryToday.setText(namaShared);
            txtDivisiMenuDetailInventoryToday.setText(divisiShared);
        }

        llMenuDetailInventoryTodayPopup = (LinearLayout) findViewById(R.id.llMenuDetailInventoryTodayPopup);
        llMenuDetailInventoryTodayPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MenuDetailInventoryToday.this, llMenuDetailInventoryTodayPopup);
                popup.getMenuInflater().inflate(R.menu.popup_inbox, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
//                        if (menuItem.getTitle().equals(Constant.POPUP_REFRESH)) {
//                            initUI();
//                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
//                            Intent i = new Intent(mContext, MenuLogin.class);
//                            startActivity(i);
//                            finish();
//                        }
//                        return true;

                        if (menuItem.getTitle().equals(Constant.POPUP_DATAPENDING)) {
                            Intent i = new Intent(getApplicationContext(), MenuDataOffline.class);
                            startActivity(i);
                            finish();
                        }
                        else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            if (querySD.isSettleAvailable().equals("0")) {
                                Intent i = new Intent(getApplicationContext(), MenuLogin.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                                finish();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Pesan");
                                builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Mohon Lakukan Settlement Terlebih Dahulu");
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent i = new Intent(MenuDetailInventoryToday.this, MenuSettlement.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });
                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            }
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });

        imgBackMenuDetailInventoryToday = (ImageView) findViewById(R.id.imgBackMenuDetailInventoryToday);
        imgBackMenuDetailInventoryToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        etNamaDebiturMenuDetailInventoryToday = (EditText) findViewById(R.id.etNamaDebiturMenuDetailInventoryToday);
        etAlamatRumahMenuDetailInventoryToday = (EditText) findViewById(R.id.etAlamatRumahMenuDetailInventoryToday);
        etAlamatUsahaMenuDetailInventoryToday = (EditText) findViewById(R.id.etAlamatUsahaMenuDetailInventoryToday);
        etNoHandphoneMenuDetailInventoryToday = (EditText) findViewById(R.id.etNoHandphoneMenuDetailInventoryToday);
        etNoTelpRumahMenuDetailInventoryToday = (EditText) findViewById(R.id.etNoTelpRumahMenuDetailInventoryToday);
        etEmailMenuDetailInventoryToday = (EditText) findViewById(R.id.etEmailMenuDetailInventoryToday);
        etJenisPinjamanMenuDetailInventoryToday = (EditText) findViewById(R.id.etJenisPinjamanMenuDetailInventoryToday);
        etNoLoanRumahMenuDetailInventoryToday = (EditText) findViewById(R.id.etNoLoanRumahMenuDetailInventoryToday);
        etDPDTodayMenuDetailInventoryToday = (EditText) findViewById(R.id.etDPDTodayMenuDetailInventoryToday);
        etNoRekMenuDetailInventoryToday = (EditText) findViewById(R.id.etNoRekMenuDetailInventoryToday);
        etSaldoMenuDetailInventoryToday = (EditText) findViewById(R.id.etSaldoMenuDetailInventoryToday);
        etospinjamanRumahMenuDetailInventoryToday = (EditText) findViewById(R.id.etospinjamanRumahMenuDetailInventoryToday);
        etAngsuranMenuDetailInventoryToday = (EditText) findViewById(R.id.etAngsuranMenuDetailInventoryToday);
        etSaldoMinimumMenuDetailInventoryToday = (EditText) findViewById(R.id.etSaldoMinimumMenuDetailInventoryToday);

        etKewajibanMenuDetailInventoryToday = (EditText) findViewById(R.id.etKewajibanMenuDetailInventoryToday);
        etPokokMenuDetailInventoryToday = (EditText) findViewById(R.id.etPokokMenuDetailInventoryToday);
        etBungaMenuDetailInventoryToday = (EditText) findViewById(R.id.etBungaMenuDetailInventoryToday);
        etDendaMenuDetailInventoryToday = (EditText) findViewById(R.id.etDendaMenuDetailInventoryToday);
        etTotalKewajibanMenuDetailInventoryToday = (EditText) findViewById(R.id.etTotalKewajibanMenuDetailInventoryToday);
        etyangharusDibayarMenuDetailInventoryToday = (EditText) findViewById(R.id.etyangharusDibayarMenuDetailInventoryToday);
        etangsurankeMenuDetailInventoryToday = (EditText) findViewById(R.id.etangsurankeMenuDetailInventoryToday);


        etTenorMenuDetailInventoryToday = (EditText) findViewById(R.id.etTenorMenuDetailInventoryToday);
        ettglJatuhTempoMenuDetailInventoryToday = (EditText) findViewById(R.id.ettglJatuhTempoMenuDetailInventoryToday);
        ethistorySPMenuDetailInventoryToday = (EditText) findViewById(R.id.ethistorySPMenuDetailInventoryToday);
        //ettglPemberianspMenuDetailInventoryToday=(EditText) findViewById(R.id.ettglPemberianspMenuDetailInventoryToday);
        etresumeNasabahMenuDetailInventoryToday = (EditText) findViewById(R.id.etresumeNasabahMenuDetailInventoryToday);
        etnamaEconMenuDetailInventoryToday = (EditText) findViewById(R.id.etnamaEconMenuDetailInventoryToday);
        etnamaMogenMenuDetailInventoryToday = (EditText) findViewById(R.id.etnamaMogenMenuDetailInventoryToday);


        etnoTelpMogenMenuDetailInventoryToday = (EditText) findViewById(R.id.etnoTelpMogenMenuDetailInventoryToday);
        etnamaUplinerMenuDetailInventoryToday = (EditText) findViewById(R.id.etnamaUplinerMenuDetailInventoryToday);
        etnoTelpUplinerMenuDetailInventoryToday = (EditText) findViewById(R.id.etnoTelpUplinerMenuDetailInventoryToday);
        etnamaRekomendatorMenuDetailInventoryToday = (EditText) findViewById(R.id.etnamaRekomendatorMenuDetailInventoryToday);
        etnoTelpRekomendatorMenuDetailInventoryToday = (EditText) findViewById(R.id.etnoTelpRekomendatorMenuDetailInventoryToday);
        etpolaBayarMenuDetailInventoryToday = (EditText) findViewById(R.id.etpolaBayarMenuDetailInventoryToday);
        etGenderMenuDetailInventoryToday = (EditText) findViewById(R.id.etGenderMenuDetailInventoryToday);


        etPekerjaanMenuDetailInventoryToday = (EditText) findViewById(R.id.etPekerjaanMenuDetailInventoryToday);
        ettanggalGajianMenuDetailInventoryToday = (EditText) findViewById(R.id.ettanggalGajianMenuDetailInventoryToday);
        etkeberadaanJaminanMenuDetailInventoryToday = (EditText) findViewById(R.id.etkeberadaanJaminanMenuDetailInventoryToday);
        ettanggalPembayaranTerakhirMenuDetailInventoryToday = (EditText) findViewById(R.id.ettanggalPembayaranTerakhirMenuDetailInventoryToday);
        etnominalPembayaranTerakhirMenuDetailInventoryToday = (EditText) findViewById(R.id.etnominalPembayaranTerakhirMenuDetailInventoryToday);
        ettanggalJanjiBayarTerakhirMenuDetailInventoryToday = (EditText) findViewById(R.id.ettanggalJanjiBayarTerakhirMenuDetailInventoryToday);
        etnominalJanjiBayarTerakhirMenuDetailInventoryToday = (EditText) findViewById(R.id.etnominalJanjiBayarTerakhirMenuDetailInventoryToday);
        etactionPlanMenuDetailInventoryToday = (EditText) findViewById(R.id.etactionPlanMenuDetailInventoryToday);

        etsumberPembayaranMenuDetailInventoryToday = (EditText) findViewById(R.id.etsumberPembayaranMenuDetailInventoryToday);
        etnominalPTPMenuDetailInventoryToday = (EditText) findViewById(R.id.etnominalPTPMenuDetailInventoryToday);
        ettanggalPTPMenuDetailInventoryToday = (EditText) findViewById(R.id.ettanggalPTPMenuDetailInventoryToday);
        Intent intent = getIntent();
        if (getIntent().getExtras() != null) {
            id = intent.getStringExtra("id");
            nama = intent.getStringExtra("nama");

//        d = (Detail_Bucket) getIntent().getParcelableExtra("detailbucket");
            diData = di.getDetInventoryFromIdDetInv(id);
        }


        etNamaDebiturMenuDetailInventoryToday.setText(nama);
        etNoHandphoneMenuDetailInventoryToday.setText(diData.getNo_hp());
        etAlamatUsahaMenuDetailInventoryToday.setText(diData.getAlmt_usaha());
        etAlamatRumahMenuDetailInventoryToday.setText(diData.getAlmt_rumah());
        etNoTelpRumahMenuDetailInventoryToday.setText(diData.getNo_tlpn());

        if (diData.getEmail().equals("[text]")) {
            etEmailMenuDetailInventoryToday.setText("");
        } else {
            etEmailMenuDetailInventoryToday.setText(diData.getEmail());
        }

        etJenisPinjamanMenuDetailInventoryToday.setText(diData.getJns_pinjaman());
        etNoLoanRumahMenuDetailInventoryToday.setText(diData.getNo_loan());
        etDPDTodayMenuDetailInventoryToday.setText(diData.getDpd_today());
        etNoRekMenuDetailInventoryToday.setText(diData.getNo_rekening());
        etSaldoMenuDetailInventoryToday.setText(Function.returnSeparatorComa(diData.getSaldo()));
        etospinjamanRumahMenuDetailInventoryToday.setText(Function.returnSeparatorComa(diData.getOs_pinjaman()));
        etAngsuranMenuDetailInventoryToday.setText(Function.returnSeparatorComa(diData.getAngsuran()));
        etSaldoMinimumMenuDetailInventoryToday.setText(Function.returnSeparatorComa(diData.getSaldo_min()));

        if (diData.getKewajiban().isEmpty()) {
            etKewajibanMenuDetailInventoryToday.setText("");
        } else {
            etKewajibanMenuDetailInventoryToday.setText(Function.returnSeparatorComa(diData.getKewajiban()));
        }

        etPokokMenuDetailInventoryToday.setText(Function.returnSeparatorComa(diData.getPokok()));
        etBungaMenuDetailInventoryToday.setText(Function.returnSeparatorComa(diData.getBunga()));
        etDendaMenuDetailInventoryToday.setText(Function.returnSeparatorComa(diData.getDenda()));
        etTotalKewajibanMenuDetailInventoryToday.setText(Function.returnSeparatorComa(diData.getTotal_kewajiban()));

        etangsurankeMenuDetailInventoryToday.setText(diData.getAngsuran_ke());

        etTenorMenuDetailInventoryToday.setText(diData.getTenor());
        ettglJatuhTempoMenuDetailInventoryToday.setText(diData.getTgl_jth_tempo());

        // ettglPemberianspMenuDetailInventoryToday.setText(diData.gettgl());

        etnamaEconMenuDetailInventoryToday.setText(diData.getNama_econ());
        etnamaMogenMenuDetailInventoryToday.setText(diData.getNama_mogen());


        etnoTelpMogenMenuDetailInventoryToday.setText(diData.getTlpn_mogen());
        etnamaUplinerMenuDetailInventoryToday.setText(diData.getNama_upliner());
        etnoTelpUplinerMenuDetailInventoryToday.setText(diData.getTlpn_upliner());
        etnamaRekomendatorMenuDetailInventoryToday.setText(diData.getNama_rekomendator());

        etGenderMenuDetailInventoryToday.setText(diData.getGender());


        etPekerjaanMenuDetailInventoryToday.setText(diData.getPekerjaan());
        ettanggalGajianMenuDetailInventoryToday.setText(diData.getTgl_gajian());

        ettanggalPembayaranTerakhirMenuDetailInventoryToday.setText(diData.getTgl_bayar_terakhir());

        //etyangharusDibayarMenuDetailInventoryToday.setText(diData.getNominal_bayar_terakhir());
//        etresumeNasabahMenuDetailInventoryToday.setText(diData.getResume_nsbh());
//        ethistorySPMenuDetailInventoryToday.setText(diData.getHistori_sp());
//        etnominalPembayaranTerakhirMenuDetailInventoryToday.setText(diData.getNominal_bayar_terakhir());
//        ettanggalJanjiBayarTerakhirMenuDetailInventoryToday.setText(diData.getTgl_janji_bayar_terakhir());
//        etnominalJanjiBayarTerakhirMenuDetailInventoryToday.setText(diData.getNominal_bayar_terakhir());
//        etactionPlanMenuDetailInventoryToday.setText(diData.getAction_plan());
//        etsumberPembayaranMenuDetailInventoryToday.setText(diData.getSumber_bayar());
//        etnominalPTPMenuDetailInventoryToday.setText(diData.getNominal_ptp());
//        ettanggalPTPMenuDetailInventoryToday.setText(diData.getTgl_ptp());
//        etnoTelpRekomendatorMenuDetailInventoryToday.setText(diData.getTlpn_rekomendator());
//        etpolaBayarMenuDetailInventoryToday.setText(diData.getPola_bayar());
//        etkeberadaanJaminanMenuDetailInventoryToday.setText(diData.getKeberadaan_jaminan());

        etyangharusDibayarMenuDetailInventoryToday.setText(Function.returnSeparatorComa(diData.getHarus_bayar()));
        etresumeNasabahMenuDetailInventoryToday.setText(diData.getResume_nsbh());
        etpolaBayarMenuDetailInventoryToday.setText(diData.getPola_bayar());
        etnominalPembayaranTerakhirMenuDetailInventoryToday.setText(Function.returnSeparatorComa(diData.getNominal_bayar_terakhir()));


        ethistorySPMenuDetailInventoryToday.setText(diData.getHistori_sp());
        ettanggalJanjiBayarTerakhirMenuDetailInventoryToday.setText(diData.getTgl_janji_bayar_terakhir());
        etnominalJanjiBayarTerakhirMenuDetailInventoryToday.setText(diData.getNominal_janji_bayar_terakhir());
        etactionPlanMenuDetailInventoryToday.setText(diData.getAction_plan());
        etsumberPembayaranMenuDetailInventoryToday.setText(diData.getSumber_bayar());
        etnominalPTPMenuDetailInventoryToday.setText("");
        ettanggalPTPMenuDetailInventoryToday.setText("");
        etnoTelpRekomendatorMenuDetailInventoryToday.setText(diData.getTlpn_rekomendator());

        etkeberadaanJaminanMenuDetailInventoryToday.setText(diData.getKeberadaan_jaminan());

    }

    @Override
    public void onBackPressed() {

    }
}
