package com.newkss.acs.newkss.Menu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.JSONParser;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by acs on 5/18/17.
 */

public class MenuInisialisasi extends Activity {

    private EditText etInitKode, etPassword;
    private Button btnKirimInitKode;
    String divisi,passwordget, rc, keterangan, initkode, resultfromJson, username, password, waktu, nik, nama;
    Context mContext;
    ProgressDialog progressDialog;
    JSONParser jsonParser = new JSONParser();
    SharedPreferences sharedPreferences;
    TextView txtVersionInisialisasi;

    private String getVersion() {
        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return packageInfo.versionName;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuinisialisasi);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = MenuInisialisasi.this;
        etPassword = (EditText) findViewById(R.id.etPassword);
        etInitKode = (EditText) findViewById(R.id.etInitKode);
        txtVersionInisialisasi = (TextView) findViewById(R.id.txtVersionInisialisasi);
        btnKirimInitKode = (Button) findViewById(R.id.btnKirimInitKode);

        etPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_RIGHT = 2;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (etPassword.getRight() - etPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        if (etPassword.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                            etPassword.setInputType(InputType.TYPE_CLASS_TEXT |
                                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        } else {
                            etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        }
                        etPassword.setSelection(etPassword.getText().length());
                        return true;
                    }
                }
                return false;
            }
        });

        txtVersionInisialisasi.setText("Version " + getVersion());
        btnKirimInitKode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nilai = "aHR0cHM6Ly9sYWt1cGFuZGFpLWJqdG0uYWtzZXNjaXB0YXNvbHVzaS5jb20vbGFrdXBhbmRhaS8=";
                String nilai1 = "https://lakupandai-bjtm.aksesciptasolusi.com/lakupandai/";

                System.out.println(Function.getImei(getApplicationContext()));

                String url = "http://172.168.100.51:8052/api/v.1/init";
                //contoh encode
                //System.out.println("Imei + waktu " + new String(Function.encodeBase64(nilai1)));
                //contoh decode
                //System.out.println(new String(Function.decodeBase64(new String(Function.encodeBase64(nilai)).getBytes())));

                //System.out.println(Constant.SERVICE_INIT);
                initkode = etInitKode.getText().toString();
                passwordget = etPassword.getText().toString();
                if (initkode.isEmpty() || passwordget.isEmpty()) {
                    Function.showAlert(mContext, "Mohon Lengkapi Kolom");
                } else {
                    new executeInitKode(mContext).execute();
                }

//                MySQLiteHelper.initApp(mContext);
//                System.out.println(Constant.APP_PATH+Function.getPackageName(mContext)+Constant.DB_NAME);


            }
        });
    }


    class executeInitKode extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeInitKode(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle("Mengirim Data...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                String header = Function.getImei(getApplicationContext()) + ":" + Function.getWaktuSekarangMilis();
                System.out.println("Header: " + header);
                System.out.println("Header base64: " + new String(Function.encodeBase64(header)));
                json.put(Constant.INIT_KODE, initkode);
                json.put(Constant.PASSWORD, Function.convertStringtoMD5(passwordget));
                System.out.println("Json Kirim: " + json.toString());
                resultfromJson = jsonParser.HttpRequestPost(Constant.SERVICE_INIT, json.toString(), Constant.TimeOutConnection, new String(Function.encodeBase64(header)));
                System.out.println("HASIL JSON: " + resultfromJson);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            try {
                if (result.contains(Constant.CONNECTION_LOST)) {
                    Function.showAlert(mContext, Constant.CONNECTION_LOST);
                } else if (result.contains(Constant.CONNECTION_ERROR)) {
                    Function.showAlert(mContext, Constant.CONNECTION_ERROR);
                } else {
                    try {
                        try {
                            jsonObject = new JSONObject(result);

                            keterangan = jsonObject.getString("keterangan");
                            rc = jsonObject.getString("rc");
                            if (rc.equals("00")) {
                                MySQLiteHelper.deleteDB(mContext);
                                MySQLiteHelper.initApp(mContext);
                                username = jsonObject.getString(Constant.JSON_USERNAME);
                                password = jsonObject.getString(Constant.JSON_PASSWORD);
                                waktu = jsonObject.getString(Constant.JSON_WAKTU);
                                nik = jsonObject.getString(Constant.NIK);
                                nama = jsonObject.getString(Constant.JSON_NAMA);
                                try{
                                    System.out.println("Divisi ada");
                                    divisi=jsonObject.getString("divisi");
                                }
                                catch (Exception e){
                                    System.out.println("divisi kosong");
                                    divisi="";
                                    e.printStackTrace();
                                }


                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Pesan");
                                builder.setMessage("Sukses Aktivasi");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        sharedPreferences = getApplicationContext().getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putString(Constant.SHARED_USERNAME, username);
                                        editor.putString(Constant.SHARED_PASSWORD, password);
                                        editor.putString("nama", nama);
                                        editor.putString(Constant.NIK, nik);
                                        editor.putString("divisi",divisi);
                                        editor.commit();

                                        Intent i = new Intent(mContext, MenuLogin.class);
//                                        Bundle bundle = new Bundle();
//                                        bundle.putString(Constant.JSON_USERNAME, username);
//                                        bundle.putString(Constant.JSON_PASSWORD, password);
//                                        bundle.putString(Constant.JSON_WAKTU, waktu);
//                                        bundle.putString(Constant.JSON_NIK, nik);
//                                        bundle.putString(Constant.JSON_NAMA, nama);
//                                        i.putExtras(bundle);
                                        startActivity(i);
                                        finish();
                                    }
                                });

                                AlertDialog alert1 = builder.create();
                                alert1.show();


                            } else if (rc.equals("11")) {
                                Function.showAlert(mContext, keterangan);
                            }else if(rc.equals("T3")){
                                Function.showAlert(mContext, keterangan);
                            }
                            else if(rc.equals("T4")){
                                Function.showAlert(mContext, keterangan);
                            }
                            else if(rc.equals("T5")){
                                Function.showAlert(mContext, keterangan);
                            }
                            else if(rc.equals("T6")){
                                Function.showAlert(mContext, keterangan);
                            }
                            else if (rc.equals("10")) {
                                Function.showAlert(mContext, keterangan);
                            } else {
                                Function.showAlert(mContext, keterangan);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Function.showAlert(mContext, e.getMessage());
                        }
                    } catch (Exception e) {
                        Function.showAlert(mContext, result);
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                Function.showAlert(mContext, e.getMessage());
                e.printStackTrace();
            }


        }
    }


}
