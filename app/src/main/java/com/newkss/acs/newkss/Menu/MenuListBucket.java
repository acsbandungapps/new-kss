package com.newkss.acs.newkss.Menu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.newkss.acs.newkss.Adapter.MenuBucketAdapter;
import com.newkss.acs.newkss.Model.Detail_Bucket;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.ModelBaru.DataBucketBooked;
import com.newkss.acs.newkss.ModelBaru.TGT;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by acs on 21/04/17.
 */

public class MenuListBucket extends ParentActivity {


    Context mContext;
    ArrayList<TGT> listTGT = new ArrayList<>();
    ListView listBucket;
    EditText etSearchMenuListBucket;
    ImageView imgSaveMenuListBucket, imgBackMenuListBucket;
    ArrayList<TGT> bucketChecked;
    MenuBucketAdapter adapter;
    TGT bucket;

    ArrayList<DataBucketBooked> listDataBooked = new ArrayList<>();
    ArrayList<Detail_Inventory> listDataInventory = new ArrayList<>();

    Detail_Inventory querydi = new Detail_Inventory();

    SharedPreferences shared;
    TextView txtNamaMenuListBucket, txtDivisiMenuListBucket;
    String namaShared, divisiShared;

    LinearLayout llMenuListBucketPopup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menulistbucket);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = this;
        MySQLiteHelper.initDB(mContext);
        initUI();
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initUI() {

        txtDivisiMenuListBucket = (TextView) findViewById(R.id.txtDivisiMenuListBucket);
        txtNamaMenuListBucket = (TextView) findViewById(R.id.txtNamaMenuListBucket);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtNamaMenuListBucket.setText(namaShared);
            txtDivisiMenuListBucket.setText(divisiShared);
        }

        llMenuListBucketPopup = (LinearLayout) findViewById(R.id.llMenuListBucketPopup);
        llMenuListBucketPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MenuListBucket.this, llMenuListBucketPopup);
                popup.getMenuInflater().inflate(R.menu.popup_inbox, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_DATAPENDING)) {
                            Intent i = new Intent(getApplicationContext(), MenuDataOffline.class);
                            startActivity(i);
                            finish();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_REFRESH)) {
                            initUI();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            Intent i = new Intent(getApplicationContext(), MenuLogin.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });
        listBucket = (ListView) findViewById(R.id.listBucket);
        etSearchMenuListBucket = (EditText) findViewById(R.id.etSearchMenuListBucket);
        imgSaveMenuListBucket = (ImageView) findViewById(R.id.imgSaveMenuListBucket);
        imgBackMenuListBucket = (ImageView) findViewById(R.id.imgBackMenuListBucket);
        imgBackMenuListBucket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        bucketChecked = new ArrayList<>();
        listDataInventory = querydi.getAllDataPenagihan();
        if (listDataInventory.isEmpty()) {
            listBucket.setEmptyView(findViewById(R.id.emptyElement));
        } else {
            adapter = new MenuBucketAdapter(this, listDataInventory);
            listBucket.setAdapter(adapter);
        }

        etSearchMenuListBucket.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    adapter.notifyDataSetChanged();
                    String text = etSearchMenuListBucket.getText().toString().toLowerCase(Locale.getDefault());
                    adapter.filter(text);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        listBucket.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                CheckBox cb = (CheckBox) view.findViewById(R.id.chkBoxListRowBucket);
                cb.performClick();
                if (cb.isChecked()) {
                    bucketChecked.add(bucket);
                } else if (!cb.isChecked()) {
                    bucketChecked.remove(bucket);
                }
            }
        });


        imgSaveMenuListBucket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listDataInventory.isEmpty()) {
                    Function.showAlert(mContext, "Data Kosong");
                } else {
                    String result = "";
                    for (Detail_Inventory b : adapter.getBox()) {
                        if (b.box) {
                            result += b.getId_detail_inventory();
                        }

                    }
                    if (adapter.getBox().isEmpty()) {
                        Toast.makeText(mContext, "Mohon Pilih Data", Toast.LENGTH_SHORT).show();
                    } else {
                        for (Detail_Inventory g : adapter.getBox()) {

                            System.out.println("ID Booked: " + g.getId_detail_inventory());
                        }

                        ArrayList<Detail_Inventory> arrDI = new ArrayList<Detail_Inventory>();
                        arrDI = querydi.getDetailInventorySelectedPenagihan();

                        DataBucketBooked dbb = new DataBucketBooked();
                        dbb.deleteSequence();
                        dbb.deleteSelectedDataBookedPenagihan(adapter.getBox());
//                        dbb.deleteAllDataBookedPenagihan();
                        dbb.insertDataBookedIntoSqlitePenagihan(adapter.getBox());


//
//
                        Intent i = new Intent(MenuListBucket.this, MenuUtama.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable("ARRAYLIST", (Serializable) adapter.getBox());
//                    i.putExtra("BUNDLE", bundle);
                        startActivity(i);
                        finish();
                    }
                }


            }
        });
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        listDataInventory = querydi.getAllDataPenagihan();
        adapter = new MenuBucketAdapter(this, listDataInventory);
        listBucket.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {

    }
}

