package com.newkss.acs.newkss.Menu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.newkss.acs.newkss.Adapter.BucketSelected;
import com.newkss.acs.newkss.Adapter.MenuBucketAdapterSelected;
import com.newkss.acs.newkss.MenuKunjungan.MenuKunjunganRev;
import com.newkss.acs.newkss.MenuSettlement.MenuSettlement;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.ModelBaru.DataBucketBooked;
import com.newkss.acs.newkss.Model.Detail_Bucket;
import com.newkss.acs.newkss.Model.Detail_Bucket_Booked;
import com.newkss.acs.newkss.ModelBaru.TGT;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by acs on 27/04/17.
 */

public class MenuListBucketSelected extends ParentActivity {
    Settle_Detil querySD = new Settle_Detil();
    ListView listBucketSelected;
    MenuBucketAdapterSelected adapter;

    ImageView imgBackMenuListBucketSelected, imgSearchMenuListBucketSelected;
    EditText etSearchMenuListBucketSelected;
    ArrayList<DataBucketBooked> listDataBooked = new ArrayList<>();
    ArrayList<Detail_Inventory> listDInv = new ArrayList<>();
    Detail_Inventory queryDi = new Detail_Inventory();
    Context mContext;

    TextView txtNamaMenuListBucketSelected, txtDivisiMenuListBucketSelected;
    SharedPreferences shared;
    String namaShared, divisiShared;

    LinearLayout llMenuListBucketSelectedPopup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menulistbucketselected);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        try {
            initUI();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initUI() {
        mContext = this;
        txtNamaMenuListBucketSelected = (TextView) findViewById(R.id.txtNamaMenuListBucketSelected);
        txtDivisiMenuListBucketSelected = (TextView) findViewById(R.id.txtDivisiMenuListBucketSelected);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtNamaMenuListBucketSelected.setText(namaShared);
            txtDivisiMenuListBucketSelected.setText(divisiShared);
        }

        llMenuListBucketSelectedPopup = (LinearLayout) findViewById(R.id.llMenuListBucketSelectedPopup);
        llMenuListBucketSelectedPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MenuListBucketSelected.this, llMenuListBucketSelectedPopup);
                popup.getMenuInflater().inflate(R.menu.popup_inbox, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_DATAPENDING)) {
                            Intent i = new Intent(getApplicationContext(), MenuDataOffline.class);
                            startActivity(i);
                            finish();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_REFRESH)) {
                            initUI();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            if (querySD.isSettleAvailable().equals("0")) {
                                Intent i = new Intent(getApplicationContext(), MenuLogin.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                                finish();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Pesan");
                                builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Mohon Lakukan Settlement Terlebih Dahulu");
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent i = new Intent(MenuListBucketSelected.this, MenuSettlement.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });
                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            }
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });

        listBucketSelected = (ListView) findViewById(R.id.listBucketSelected);
        imgBackMenuListBucketSelected = (ImageView) findViewById(R.id.imgBackMenuListBucketSelected);
        imgBackMenuListBucketSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        imgSearchMenuListBucketSelected = (ImageView) findViewById(R.id.imgSearchMenuListBucketSelected);
        etSearchMenuListBucketSelected = (EditText) findViewById(R.id.etSearchMenuListBucketSelected);

        MySQLiteHelper.initDB(this);

        TextView tvEmpty = new TextView(mContext);
        tvEmpty.setText("list kosong");

        queryDi = new Detail_Inventory();
        listDInv = queryDi.getDetailInventorySelectedPenagihan();
        if (listDInv.isEmpty()) {
            //listBucketSelected.setVisibility(View.GONE);
            //listBucketSelected.setEmptyView(tvEmpty);
            listBucketSelected.setEmptyView(findViewById(R.id.emptyElement));
        } else {
            adapter = new MenuBucketAdapterSelected(mContext, listDInv);
            listBucketSelected.setAdapter(adapter);
            //listBucketSelected.setEmptyView(tvEmpty);

        }

        listBucketSelected.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Detail_Inventory dii;
                dii = (Detail_Inventory) parent.getAdapter().getItem(position);
                Intent i = new Intent(mContext, MenuListInventoryToday.class);
                i.putExtra("id_detail_inventory", dii.getId_detail_inventory());
                startActivity(i);

//                Toast(mContext,dii.getId_detail_inventory());
            }
        });


        etSearchMenuListBucketSelected.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    adapter.notifyDataSetChanged();
                    String text = etSearchMenuListBucketSelected.getText().toString().toLowerCase(Locale.getDefault());
                    adapter.filter(text);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }


    @Override
    public void onBackPressed() {

    }
}
