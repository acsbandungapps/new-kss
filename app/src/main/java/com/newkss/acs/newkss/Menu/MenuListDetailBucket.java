package com.newkss.acs.newkss.Menu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.newkss.acs.newkss.Adapter.MenuListDetailBucketAdapter;
import com.newkss.acs.newkss.MenuSettlement.MenuSettlement;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.ModelBaru.Detil_TGT;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by acs on 8/21/17.
 */

public class MenuListDetailBucket extends ParentActivity {

    TextView txtNamaMenuListDetailBucket, txtDivisiMenuListDetailBucket, txtNamaBucketMenuListDetailBucket;
    EditText etSearchMenuListDetailBucket;
    ListView listDetailBucket;
    ImageView imgBackMenuListDetailBucket;
    Context mContext;
    Detail_Inventory queryDi = new Detail_Inventory();
    Detil_TGT queryDTGT = new Detil_TGT();
    ArrayList<Detail_Inventory> listDInv = new ArrayList<>();
    ArrayList<Detil_TGT> listFPAY = new ArrayList<>();
    ArrayList<Detil_TGT> listPPAY = new ArrayList<>();
    ArrayList<Detil_TGT> listNPAY = new ArrayList<>();
    ArrayList<Detil_TGT> listNVST = new ArrayList<>();
    SharedPreferences shared;
    String namaShared, divisiShared;
    LinearLayout llMenuListDetailBucketPopup;
    MenuListDetailBucketAdapter adapter;
    Settle_Detil querySD = new Settle_Detil();

    String titleBucket = "Bucket";
    String dataBucket = "tgt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menulistdetailbucket);
        initUI();
        initEvent();
    }

    private void initUI() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = this;
        txtNamaBucketMenuListDetailBucket = (TextView) findViewById(R.id.txtNamaBucketMenuListDetailBucket);
        listDetailBucket = (ListView) findViewById(R.id.listDetailBucket);
        llMenuListDetailBucketPopup = (LinearLayout) findViewById(R.id.llMenuListDetailBucketPopup);
        txtNamaMenuListDetailBucket = (TextView) findViewById(R.id.txtNamaMenuListDetailBucket);
        txtDivisiMenuListDetailBucket = (TextView) findViewById(R.id.txtDivisiMenuListDetailBucket);
        etSearchMenuListDetailBucket = (EditText) findViewById(R.id.etSearchMenuListDetailBucket);
        listDetailBucket = (ListView) findViewById(R.id.listDetailBucket);
        imgBackMenuListDetailBucket = (ImageView) findViewById(R.id.imgBackMenuListDetailBucket);

        llMenuListDetailBucketPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popup = new PopupMenu(MenuListDetailBucket.this, llMenuListDetailBucketPopup);
                popup.getMenuInflater().inflate(R.menu.popup_inbox, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_DATAPENDING)) {
                            Intent i = new Intent(getApplicationContext(), MenuDataOffline.class);
                            startActivity(i);
                            finish();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)){
                            if (querySD.isSettleAvailable().equals("0")) {
                                Intent i = new Intent(getApplicationContext(), MenuLogin.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                                finish();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Pesan");
                                builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Mohon Lakukan Settlement Terlebih Dahulu");
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent i = new Intent(MenuListDetailBucket.this, MenuSettlement.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });
                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            }
                        }

                        return true;
                    }
                });



            }
        });
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            titleBucket = bundle.getString(Constant.BUNDLE_TITLE);
            dataBucket = bundle.getString(Constant.BUNDLE_DATA);
            txtNamaBucketMenuListDetailBucket.setText("[" + titleBucket + "]");
        }
    }

    private void initEvent() {
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtNamaMenuListDetailBucket.setText(namaShared);
            txtDivisiMenuListDetailBucket.setText(divisiShared);
        }

        imgBackMenuListDetailBucket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        MySQLiteHelper.initDB(mContext);
        listFPAY = queryDTGT.getAllBucket(dataBucket);
        //TODO validasi search tagihan kosong
        if (listFPAY.isEmpty()) {
            System.out.println("List Kosong");
            listDetailBucket.setEmptyView(findViewById(R.id.emptyElement));
        } else {
            listDInv = queryDi.getArrayListDetInvFromArrListNoLoan(listFPAY);
            adapter = new MenuListDetailBucketAdapter(mContext, listDInv);
            listDetailBucket.setAdapter(adapter);
        }

        etSearchMenuListDetailBucket.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    if (!listFPAY.isEmpty()) {
                        adapter.notifyDataSetChanged();
                        String text = etSearchMenuListDetailBucket.getText().toString().toLowerCase(Locale.getDefault());
                        adapter.filter(text);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
    }
}
