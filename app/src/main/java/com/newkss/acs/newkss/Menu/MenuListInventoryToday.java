package com.newkss.acs.newkss.Menu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.newkss.acs.newkss.MenuKunjungan.MenuHasilKunjungan;
import com.newkss.acs.newkss.MenuKunjungan.MenuKunjunganRev;
import com.newkss.acs.newkss.MenuSettlement.MenuSettlement;
import com.newkss.acs.newkss.Model.Detail_Bucket;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.ModelBaru.TGT;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.NumberTextWatcherForThousand;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Erdy on 4/28/2017.
 */
public class MenuListInventoryToday extends ParentActivity {
    EditText etDPDMenuListInventoryToday, etNamaMenuListInventoryToday, etOSMenuListInventoryToday, etTotalBayarMenuListInventoryToday, etCycleMenuListInventoryToday, etAlamatMenuListInventoryToday, etNoTelpMenuListInventoryToday, etBucketMenuListInventoryToday;
    Detail_Bucket d = new Detail_Bucket();
    ImageView imgBackMenuListInventoryToday;
    Button btnDetailInventoryMenuListInventoryToday, btnHasilkunjunganMenuListInventoryToday;

    LinearLayout llMenuListInventoryTodayPopup;
    TGT querytgt = new TGT();
    TGT datatgt = new TGT();
    String idtgt, namatgt, iddetinv;
    Context mContext;

    Detail_Inventory queryDi = new Detail_Inventory();
    Detail_Inventory dataDi = new Detail_Inventory();

    TextView txtNamaMenuListInventoryToday, txtDivisiMenuListInventoryToday;
    SharedPreferences shared;
    String namaShared, divisiShared, current;
    Settle_Detil querySD = new Settle_Detil();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menulistinventorytoday);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        initUI();
    }

    private void initUI() {
        mContext = this;
        llMenuListInventoryTodayPopup = (LinearLayout) findViewById(R.id.llMenuListInventoryTodayPopup);
        txtNamaMenuListInventoryToday = (TextView) findViewById(R.id.txtNamaMenuListInventoryToday);
        txtDivisiMenuListInventoryToday = (TextView) findViewById(R.id.txtDivisiMenuListInventoryToday);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtNamaMenuListInventoryToday.setText(namaShared);
            txtDivisiMenuListInventoryToday.setText(divisiShared);
        }

        imgBackMenuListInventoryToday = (ImageView) findViewById(R.id.imgBackMenuListInventoryToday);
        etNamaMenuListInventoryToday = (EditText) findViewById(R.id.etNamaMenuListInventoryToday);
        etOSMenuListInventoryToday = (EditText) findViewById(R.id.etOSMenuListInventoryToday);
        etTotalBayarMenuListInventoryToday = (EditText) findViewById(R.id.etTotalBayarMenuListInventoryToday);
        etCycleMenuListInventoryToday = (EditText) findViewById(R.id.etCycleMenuListInventoryToday);

        etAlamatMenuListInventoryToday = (EditText) findViewById(R.id.etAlamatMenuListInventoryToday);
        etNoTelpMenuListInventoryToday = (EditText) findViewById(R.id.etNoTelpMenuListInventoryToday);
        etBucketMenuListInventoryToday = (EditText) findViewById(R.id.etBucketMenuListInventoryToday);

        etDPDMenuListInventoryToday = (EditText) findViewById(R.id.etDPDMenuListInventoryToday);
        etNamaMenuListInventoryToday.requestFocus();


        imgBackMenuListInventoryToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(mContext, MenuListBucketSelected.class);
//                startActivity(i);
                finish();
            }
        });
        //   d = (Detail_Bucket) getIntent().getParcelableExtra("detailbucket");
        Intent i = getIntent();
        iddetinv = i.getStringExtra("id_detail_inventory");
        idtgt = queryDi.getIdTGTFromIdDetInv(iddetinv);
        dataDi = queryDi.getDetInventoryFromIdDetInv(iddetinv);

//        idtgt = i.getStringExtra("id_tgt");
//        namatgt = i.getStringExtra("nama_tgt");

        datatgt = querytgt.getAllTGTFromIdTGT(idtgt);

        etNamaMenuListInventoryToday.setText(datatgt.getNama());
        etOSMenuListInventoryToday.setText(Function.returnSeparatorComa(dataDi.getOs_pinjaman()));
        etTotalBayarMenuListInventoryToday.setText(Function.returnSeparatorComa(dataDi.getTotal_kewajiban()));
        //etCycleMenuListInventoryToday.setText("");
        etCycleMenuListInventoryToday.setText(datatgt.getCycle());
        etAlamatMenuListInventoryToday.setText(datatgt.getAlamat());
        etDPDMenuListInventoryToday.setText(dataDi.getDpd_today());
        etNoTelpMenuListInventoryToday.setText(dataDi.getNo_hp());
//        etNoTelpMenuListInventoryToday.setText("asdas");
        etBucketMenuListInventoryToday.setText(dataDi.getBucket());
        btnDetailInventoryMenuListInventoryToday = (Button) findViewById(R.id.btnDetailInventoryMenuListInventoryToday);
        btnDetailInventoryMenuListInventoryToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MenuListInventoryToday.this, MenuDetailInventoryToday.class);
                i.putExtra("id", iddetinv);
                i.putExtra("nama", dataDi.getNama_debitur());
                startActivity(i);

            }
        });
        btnHasilkunjunganMenuListInventoryToday = (Button) findViewById(R.id.btnHasilkunjunganMenuListInventoryToday);
        btnHasilkunjunganMenuListInventoryToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(getApplicationContext(), MenuKunjunganRev.class);
                in.putExtra("id", iddetinv);
                //in.putExtra("nama_tgt", namatgt);
                startActivity(in);

            }
        });

        llMenuListInventoryTodayPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MenuListInventoryToday.this, llMenuListInventoryTodayPopup);
                popup.getMenuInflater().inflate(R.menu.popup_inbox, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_DATAPENDING)) {
                            Intent i = new Intent(getApplicationContext(), MenuDataOffline.class);
                            startActivity(i);
                            finish();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            if (querySD.isSettleAvailable().equals("0")) {
                                Intent i = new Intent(getApplicationContext(), MenuLogin.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                                finish();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Pesan");
                                builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Mohon Lakukan Settlement Terlebih Dahulu");
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent i = new Intent(MenuListInventoryToday.this, MenuSettlement.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });
                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            }
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });

    }
}
