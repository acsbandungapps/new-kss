package com.newkss.acs.newkss.Menu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.net.http.HttpsConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.newkss.acs.newkss.MenuSettlement.MenuSettlement;
import com.newkss.acs.newkss.Model.Bucket;
import com.newkss.acs.newkss.Model.Config;
import com.newkss.acs.newkss.Model.DeleteData;
import com.newkss.acs.newkss.Model.Detail_Bucket;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.IsiPenagihan;
import com.newkss.acs.newkss.Model.ObjectActionCode;
import com.newkss.acs.newkss.Model.Penagihan;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Service.ServiceSendLocation;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.JSONParser;
import com.newkss.acs.newkss.Util.MySQLiteHelper;
import com.newkss.acs.newkss.Util.MySSLSocketFactory;
import com.newkss.acs.newkss.Util.SyncData;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import io.fabric.sdk.android.Fabric;

/**
 * Created by acs on 11/04/17.
 */

public class MenuLogin extends Activity {
    private EditText etUsernameLogin, etPasswordLogin;
    private TextView txtSyncPassword, txtVersionLogin;
    private Button btnResetLogin, btnLogin;

    private CheckBox chkIngatSayaLogin;
    private String username, password, resultfromJson;

    JSONParser jsonParser = new JSONParser();

    ProgressDialog progressDialog;
    Context mContext;

    MySQLiteHelper mm;

    //npay fpay
    List<String> namaBucket = new ArrayList<>();
    //current 1-30
    List<Penagihan> namaPenagihan;
    List<Config> listConfig = new ArrayList<>();

    //Buat bedain nama penagihan, current,1-30 dll
    List<IsiPenagihan> listisiPenagihan;
    Detail_Inventory querydetinv = new Detail_Inventory();
    Penagihan p = new Penagihan();
    Penagihan p1 = new Penagihan();
    Bucket b = new Bucket();
    Config con = new Config();
    Config cc;
    Detail_Bucket ddb = new Detail_Bucket();

//    Detail_Inventory_Pencairan queryDIP = new Detail_Inventory_Pencairan();

    SharedPreferences shared;
    private String usernameShared, passwordShared, nikShared;
    private String JsonSyncData = "";
    private String hasilJson = "";

    Config queryConfig = new Config();
    DeleteData queryDD = new DeleteData();


    Settle_Detil querySD = new Settle_Detil();

    ProgressDialog pd;
    String url = "";
    private boolean bUpgrade;
    String szError = null;
    SyncData sd = new SyncData();

    String path = "";
    String divisi = "";
    String version = "";
    String periodik_track = "";
    String nama = "";

    ServiceSendLocation querySSL;
    private static final String TAG = "SelfSignedCertActivity";

    // Verifier that verifies all hosts
    private static final HostnameVerifier DUMMY_VERIFIER = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.menulogin);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = this;

//        File file = new File(Constant.APP_PATH + Function.getPackageName(mContext) + File.separator + Constant.HIDDEN_FOLDER + File.separator + Constant.DB_NAME);
        File file = new File(Constant.APP_PATH + Constant.HIDDEN_FOLDER + File.separator + Constant.DB_NAME);
        if (!file.exists()) {
            MySQLiteHelper.initApp(mContext);
        } else {
        }

//        Function.triggerAlarm(mContext);
//        Function.triggerAlarmSendOfflineData(mContext);
//        MySQLiteHelper.initDB(mContext);

        etUsernameLogin = (EditText) findViewById(R.id.etUsernameLogin);
        etPasswordLogin = (EditText) findViewById(R.id.etPasswordLogin);
        txtVersionLogin = (TextView) findViewById(R.id.txtVersionLogin);
        txtSyncPassword = (TextView) findViewById(R.id.txtSyncPassword);
        btnResetLogin = (Button) findViewById(R.id.btnResetLogin);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        chkIngatSayaLogin = (CheckBox) findViewById(R.id.chkIngatSayaLogin);


        //checkFirstActivation();
        initUI();
    }


    private class OpenConnectionTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Dummy trust manager that trusts all certificates
            TrustManager localTrustmanager = new X509TrustManager() {

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                @Override
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            };

            // Create SSLContext and set the socket factory as default
            try {
                SSLContext sslc = SSLContext.getInstance("TLS");
                sslc.init(null, new TrustManager[]{localTrustmanager},
                        new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sslc
                        .getSocketFactory());
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {

                // URL url = new URL("https://google.com/");
                //              URL url = new URL("https://74.125.200.95/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=");
                URL url = new URL("https://10.50.238.12/api/v.1/login");

                //URL url = new URL("https://74.125.200.138/");
                //URL url = new URL("https://google.com/");
                HttpsURLConnection connection = (HttpsURLConnection) url
                        .openConnection();
                connection.setHostnameVerifier(DUMMY_VERIFIER);
                connection.setRequestMethod("POST");

                connection.setRequestProperty("Authentication", "Basic " + "aa");

                // Log the server response code
                int responseCode = connection.getResponseCode();
                Log.i(TAG, "Server responded with: " + responseCode);

                // And if the code was HTTP_OK then return true
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    return true;
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            // Hide progressbar
            setProgressBarIndeterminateVisibility(false);

            if (result != null) {

                // Create a dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(MenuLogin.this);
                if (result) {
                    builder.setMessage("Connection was opened successfully");
                } else {
                    builder.setMessage("Connection failed");
                }
                builder.setPositiveButton("OK", null);

                // and show it
                builder.create().show();
            }
        }
    }


    private boolean checkFirstActivation() {


        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            usernameShared = (shared.getString(Constant.SHARED_USERNAME, ""));
            passwordShared = (shared.getString(Constant.SHARED_PASSWORD, ""));
            nikShared = (shared.getString(Constant.NIK, ""));
            etUsernameLogin.setText(usernameShared);

            // etPasswordLogin.setText(passwordShared);
            System.out.println("Ada Data");
            return true;
        } else {
            Intent i = new Intent(mContext, MenuInisialisasi.class);
            startActivity(i);
            finish();
            System.out.println("Data Kosong");
            return false;
        }
    }

    private String getVersion() {
        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return packageInfo.versionName;
    }

    private void initUI() {
//        etUsernameLogin.setText("SUP738832");
//        etPasswordLogin.setText("123456");
        MySQLiteHelper.initApp(mContext);

        MySQLiteHelper.initDB(mContext);
        System.out.println("tes waktu " + queryConfig.getDateConfig());

        querydetinv.synchronizeTableDetailInventory(Constant.TABLE_DETAIL_INVENTORY, "tgl_cair", "TEXT");
        querySD.synchronizeTableSettleDetil(Constant.TABLE_DETIL_SETTLEMENT, "waktu", "TEXT");
        querySD.synchronizeTableSettleDetil(Constant.TABLE_DETIL_SETTLEMENT, "unique_code", "TEXT");

        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            usernameShared = (shared.getString(Constant.SHARED_USERNAME, ""));
            passwordShared = (shared.getString(Constant.SHARED_PASSWORD, ""));
            nikShared = (shared.getString(Constant.NIK, ""));
            etUsernameLogin.setText(usernameShared);
            // etPasswordLogin.setText(passwordShared);
            System.out.println("Ada Data");
        }
//        Bundle bundle = getIntent().getExtras();
//        username = bundle.getString(Constant.JSON_USERNAME);
//        password = bundle.getString(Constant.JSON_PASSWORD);
//        etUsernameLogin.setText(username);
//        etPasswordLogin.setText(password);
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            txtVersionLogin.setText("Versi " + packageInfo.versionName);
//            txtVersionLogin.setText("For Training Only");

        } catch (Exception e) {
            e.printStackTrace();
        }


        etPasswordLogin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_RIGHT = 2;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (etPasswordLogin.getRight() - etPasswordLogin.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        if (etPasswordLogin.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                            etPasswordLogin.setInputType(InputType.TYPE_CLASS_TEXT |
                                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        } else {
                            etPasswordLogin.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        }
                        etPasswordLogin.setSelection(etPasswordLogin.getText().length());
                        return true;
                    }
                }
                return false;
            }
        });

        txtSyncPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuChangePassword.class);
                startActivity(i);
                finish();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File file = new File(Constant.APP_PATH + File.separator + Constant.HIDDEN_FOLDER + File.separator + Constant.DB_NAME);
                if (!file.exists()) {
                    MySQLiteHelper.initApp(mContext);
                } else {
                }
                //MySQLiteHelper.initDB(mContext);
                username = etUsernameLogin.getText().toString().replaceAll("\\p{Z}","");
                password = etPasswordLogin.getText().toString().replaceAll("\\p{Z}","");

                if(getLocationMode(mContext)==3){
                    if (username.isEmpty() || password.isEmpty()) {
                        Function.showAlert(mContext, "Mohon Lengkapi Kolom");
                    } else {
                        try {
                            Location location;
                            querySSL=new ServiceSendLocation(mContext);
                            location=querySSL.getLocation();
                            if(location==null){
                                location=querySSL.getLastKnownLocation();
                            }
                            System.out.println("Long "+location.getLongitude());
                            System.out.println("Lat "+location.getLatitude());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        new execLogin(mContext).execute();
                        // new execUpdateApk().execute();

                        // updateAPK();

                    }
                }
                else{
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
                    // Setting Dialog Title
                    alertDialog.setTitle("Setting GPS");
                    // Setting Dialog Message
                    alertDialog.setMessage("Mohon Set GPS ke Mode High Accuracy");
                    // On pressing Settings button
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            mContext.startActivity(intent);
                        }
                    });
                    // Showing Alert Message
                    alertDialog.show();
                }

            }
        });
    }
    public int getLocationMode(Context context)
    {

        try {
            return Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return 0;

    }

    private void upgradeAppsHTTPS(String urlPath) {

        url = urlPath;
        new DownloadFileHTTPS().execute();


    }

    private void upgradeApps(String urlPath) {

        url = urlPath;
        //upgradeProcessHandler.sendEmptyMessage(0);
        new DownloadFile().execute();
    }

    public HttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

    public class NullHostNameVerifier implements HostnameVerifier {

        @Override
        public boolean verify(String hostname, SSLSession session) {
            Log.i("RestUtilImpl", "Approving certificate for " + hostname);
            return true;
        }

    }

    class DownloadFileHTTPS extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setProgress(0);
            progressDialog.setTitle("Mobile Collsys");
            progressDialog.setMessage("Update Application");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(true);
            progressDialog.setMax(100);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                String PATH = "";
                File file = null;
                File outputFile = null;
                FileOutputStream fos = null;
                TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                return null;
                            }

                            public void checkClientTrusted(
                                    java.security.cert.X509Certificate[] certs, String authType) {
                            }

                            public void checkServerTrusted(
                                    java.security.cert.X509Certificate[] certs, String authType) {
                            }
                        }
                };

// Activate the new trust manager
                try {
                    SSLContext sc = SSLContext.getInstance("SSL");
                    sc.init(null, trustAllCerts, new java.security.SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                } catch (Exception e) {
                }

// And as before now you can use URL and URLConnection
                URL url1 = new URL(url);
                URLConnection connection = url1.openConnection();
                InputStream in = connection.getInputStream();

                System.out.println("in " + in.toString());
//
                int lengthOfFile = connection.getContentLength();
                System.out.println("Length: " + lengthOfFile);
                PATH = Environment.getExternalStorageDirectory()
                        + "/download/";
                file = new File(PATH);
                file.mkdirs();
                outputFile = new File(file,
                        "Mobile Collsys.apk");
                fos = new FileOutputStream(outputFile);
                //in = c.getInputStream();

                long total = 0;
                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = in.read(buffer)) != -1) {
                    total += len1;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    fos.write(buffer, 0, len1);
                }
                fos.close();
                in.close();// till here, it works fine - .apk is
                // download to my
                // sdcard in download file
                bUpgrade = true;

            } catch (Exception e) {

                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            progressDialog.setProgress(Integer.parseInt(values[0]));
        }


        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            if (szError == null) {
                if (bUpgrade) {
                    System.out.println("BUPGRADE1");
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    System.out.println("BUPGRADE2");
                    intent.setDataAndType(Uri.fromFile(new File(Environment
                                    .getExternalStorageDirectory()
                                    + "/download/"
                                    + "Mobile Collsys.apk")),
                            "application/vnd.android.package-archive");
                    System.out.println("BUPGRADE3");
                    startActivity(intent);
                    System.out.println("BUPGRADE4");
                } else {
                    Toast.makeText(getApplicationContext(), "Update tidak berhasil. Cek Koneksi ke server.", Toast.LENGTH_SHORT).show();
                }
            } else if (szError.equals("Download Cancelled")) {
                Toast.makeText(getApplicationContext(), "Download Cancelled " + szError, Toast.LENGTH_SHORT).show();

            } else {
                System.out.println("Gagal " + szError);
                Toast.makeText(getApplicationContext(), "Gagal " + szError, Toast.LENGTH_SHORT).show();

            }

        }
    }

    class DownloadFile extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setProgress(0);
            progressDialog.setTitle("Mobile Collsys");
            progressDialog.setMessage("Update Application");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(true);
            progressDialog.setMax(100);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                HttpURLConnection c = null;
                String PATH = "";
                File file = null;
                File outputFile = null;
                FileOutputStream fos = null;
                InputStream is = null;

//                        URL apkurl = new URL(url
//                                + "MobileSurvey/Mobile_CIS_BPRKS.apk");
                URL apkurl = new URL(url);
                c = (HttpURLConnection) apkurl.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.setConnectTimeout(20 * 1000);
                c.connect();

                int lengthOfFile = c.getContentLength();
                PATH = Environment.getExternalStorageDirectory()
                        + "/download/";
                file = new File(PATH);
                file.mkdirs();
                outputFile = new File(file,
                        "Mobile Collsys.apk");
                fos = new FileOutputStream(outputFile);
                is = c.getInputStream();

                long total = 0;
                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    total += len1;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    fos.write(buffer, 0, len1);
                }
                fos.close();
                is.close();// till here, it works fine - .apk is
                // download to my
                // sdcard in download file
                bUpgrade = true;
                c.disconnect();

            } catch (IOException e) {
                System.out.println("IOException " + e.getMessage());
                e.printStackTrace();
                szError = e.getMessage().toString();
            } catch (Exception e) {
                System.out.println("Exception " + e.getMessage());
                e.printStackTrace();
                szError = e.getMessage().toString();
            } finally {
                // upgradeHandler.sendEmptyMessage(0);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            progressDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            if (szError == null) {
                if (bUpgrade) {
                    System.out.println("BUPGRADE1");
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    System.out.println("BUPGRADE2");
                    intent.setDataAndType(Uri.fromFile(new File(Environment
                                    .getExternalStorageDirectory()
                                    + "/download/"
                                    + "Mobile Collsys.apk")),
                            "application/vnd.android.package-archive");
                    System.out.println("BUPGRADE3");
                    startActivity(intent);
                    System.out.println("BUPGRADE4");
                } else {
                    Toast.makeText(getApplicationContext(), "Upgrade tidak berhasil. Cek Koneksi ke server.", Toast.LENGTH_SHORT).show();
                }
            } else {
                System.out.println("Gagal " + szError);
                Toast.makeText(getApplicationContext(), "Gagal " + szError, Toast.LENGTH_SHORT).show();

            }

        }
    }

    private class execLogin extends AsyncTask<String, String, String> {
        Context context;

        private execLogin(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle("Validasi Data");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            JSONObject json = new JSONObject();
            try {

                String n1 = "0";
                String n2 = "0";
                String n3 = "0";
                String n4 = n1 + n2 + n3;
                String header = Function.getImei(mContext) + ":" + n4 + ":" + Function.getWaktuSekarangMilis();
                System.out.println("Header: " + header);
                System.out.println("Header base64: " + new String(Function.encodeBase64(header)));
                json.put(Constant.USERNAME, username);
                json.put(Constant.PASSWORD, Function.convertStringtoMD5(password));
                json.put(Constant.WAKTU, Function.getWaktuSekarangMilis());
                System.out.println("Data Dikirim " + json.toString());
                System.out.println("URL: " + Constant.SERVICE_LOGIN);


                hasilJson = jsonParser.HttpRequestPost(Constant.SERVICE_LOGIN, json.toString(), Constant.TimeOutConnection, new String(Function.encodeBase64(header)));

                longLog(hasilJson);
                System.out.println("HASIL JSON: " + hasilJson);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return hasilJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            try {
                JSONObject jsonAll = new JSONObject(result);

                if (result.contains(Constant.CONNECTION_LOST)) {
                    Function.showAlert(mContext, Constant.CONNECTION_LOST);
                } else if (result.contains(Constant.CONNECTION_ERROR)) {
                    Function.showAlert(mContext, Constant.CONNECTION_ERROR);
                } else {
                    String keterangan = jsonAll.getString("keterangan");
                    String rc = jsonAll.getString("rc");

                    if (rc.equals("00")) {
                        String nik = jsonAll.getString(Constant.NIK);
                       // version = jsonAll.getString("version");
                        version = "1.0.31 TEST ONLY";
//                        String versiSkrg = getVersion();
                        String versiSkrg = "1.0.31 TEST ONLY";
                        nama = jsonAll.getString("nama");
                        path = jsonAll.getString("path");
                        divisi = jsonAll.getString("divisi");
                        periodik_track = jsonAll.getString("periodik_track");
                        String namaCut = "";
                        if (nama.length() <= 14) {
                            namaCut = nama.substring(0, nama.length());
                        } else if (nama.length() > 14) {
                            namaCut = nama.substring(0, 14);
                        }

                        //TODO CHECK VERSION
                        if (!version.equals("-")) {
                            if (versiSkrg.equals(version)) {
                                if (querySD.isSettleAvailable().equals("0")) {
                                    shared = getApplicationContext().getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                                    SharedPreferences.Editor editor = shared.edit();
                                    editor.putString(Constant.SHARED_USERNAME, username);
                                    editor.putString(Constant.SHARED_PASSWORD, Function.convertStringtoMD5(password));
                                    editor.putString(Constant.NIK, nik);
                                    editor.putString("divisi", divisi);
                                    editor.putString("nama", namaCut);
                                    editor.commit();
                                    new getResponse(mContext).execute();
                                } else {
                                    //kasus kalo dia pindah apk, username null tapi db ada sisa settlement
                                    if (usernameShared == null) {
                                        queryDD.deleteAllDetailInventory();
                                        queryDD.deleteAllPhoto();
                                        queryDD.deleteAllDataPencairan();
                                        queryDD.deleteAllTGT();
                                        queryDD.deleteAllDetilDataPencairan();
                                        queryDD.deleteAllDetilTGT();
                                        queryDD.deleteAllSettle();
                                        queryDD.deleteAllDetilSettle();
                                        queryDD.deleteAllDataBooked();
                                        queryDD.deleteAllMasterSettle();
                                        queryDD.deleteAllSequence();
                                        queryDD.deleteAllParameter();
                                        shared = getApplicationContext().getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                                        SharedPreferences.Editor editor = shared.edit();
                                        editor.putString(Constant.SHARED_USERNAME, username);
                                        editor.putString(Constant.SHARED_PASSWORD, Function.convertStringtoMD5(password));
                                        editor.putString(Constant.NIK, nik);
                                        editor.putString("divisi", divisi);
                                        editor.putString("nama", namaCut);
                                        editor.commit();
                                        new getResponse(mContext).execute();
                                    } else if (usernameShared != null) {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                        builder.setTitle("Pesan");
                                        builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Dari User: " + usernameShared + " Mohon Lakukan Settlement Terlebih Dahulu");
                                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                System.out.println("username shared 1: " + usernameShared + " and " + username);
                                                if (usernameShared.equals(username)) {
                                                    Intent i = new Intent(MenuLogin.this, MenuSettlement.class);
                                                    startActivity(i);
                                                    finish();
                                                } else {

                                                }

                                            }
                                        });
                                        AlertDialog alert1 = builder.create();
                                        alert1.show();
                                    }

                                }

                            } else if (!versiSkrg.equals(version)) {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Konfirmasi Update");
                                builder.setMessage("Terdapat Versi Baru, Versi " + version + "  Mohon Lakukan Update");
                                builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        String typejaringan = String.valueOf(path.charAt(4));
                                        if (typejaringan.equals(":")) {
                                            upgradeApps(path);
                                        } else if (typejaringan.equals("s")) {
                                            upgradeAppsHTTPS(path);
                                        } else {
                                            upgradeApps(path);
                                        }
                                        //upgradeApps(path);
//                                        upgradeAppsHTTPS(path);
                                    }
                                });
                                builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int i) {
                                        dialog.dismiss();
                                    }
                                });
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        } else {
                            if (querySD.isSettleAvailable().equals("0")) {
                                shared = getApplicationContext().getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                                SharedPreferences.Editor editor = shared.edit();
                                editor.putString(Constant.SHARED_USERNAME, username);
                                editor.putString(Constant.SHARED_PASSWORD, Function.convertStringtoMD5(password));
                                editor.putString(Constant.NIK, nik);
                                editor.putString("divisi", divisi);
                                editor.putString("nama", namaCut);
                                editor.commit();
                                new getResponse(mContext).execute();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Pesan");
                                builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Dari User: " + usernameShared + " Mohon Lakukan Settlement Terlebih Dahulu");
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        System.out.println("username shared 2: " + usernameShared + " and " + username);
                                        if (usernameShared.equals(username)) {
                                            Intent i = new Intent(MenuLogin.this, MenuSettlement.class);
                                            startActivity(i);
                                            finish();
                                        } else {

                                        }
                                    }
                                });
                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            }
                        }
                    } else if (rc.equals("T1")) {
                        Function.showAlert(mContext, keterangan);
                    } else if (rc.equals("T2")) {
                        Function.showAlert(mContext, keterangan);
                    } else if (rc.equals("T3")) {
                        Function.showAlert(mContext, keterangan);
                    } else if (rc.equals("T4")) {
                        Function.showAlert(mContext, keterangan);
                    } else if (rc.equals("10")) {
                        Function.showAlert(mContext, keterangan);
                    } else {
                        Function.showAlert(mContext, keterangan);
                    }
                }
            } catch (Exception e) {
                Function.showAlert(mContext, result);
                e.printStackTrace();

            }
        }
    }

    class getResponse extends AsyncTask<String, String, String> {
        Context context;

        private getResponse(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle("Mengecek Data");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            JSONObject json = new JSONObject();
            try {
                shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                if (shared.contains(Constant.NIK)) {
                    nikShared = (shared.getString(Constant.NIK, ""));
                }
                String header = Function.getImei(mContext) + ":" + nikShared + ":" + Function.getWaktuSekarangMilis();
                System.out.println("Header: " + header);
                System.out.println("Header base64: " + new String(Function.encodeBase64(header)));
                json.put(Constant.USERNAME, username);
                json.put(Constant.PASSWORD, Function.convertStringtoMD5(password));
                json.put(Constant.WAKTU, Function.getWaktuSekarangMilis());
                System.out.println("Data Dikirim " + json.toString());
                System.out.println("URL: " + Constant.SERVICE_DATA_SYNC);
                JsonSyncData = jsonParser.HttpRequestPost(Constant.SERVICE_DATA_SYNC, json.toString(), Constant.TimeOutConnection, new String(Function.encodeBase64(header)));
                longLog(JsonSyncData);

                System.out.println("HASIL JSON SYNC: " + JsonSyncData);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return JsonSyncData;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            try {
                JSONObject jsonAll = new JSONObject(result);
                if (result.contains(Constant.CONNECTION_LOST)) {
                    Function.showAlert(mContext, Constant.CONNECTION_LOST);
                } else if (result.contains(Constant.CONNECTION_ERROR)) {
                    Function.showAlert(mContext, Constant.CONNECTION_ERROR);
                } else {
                    try {
//                        String waktu = jsonAll.getString("periodik_track");

                        String rc = jsonAll.getString("rc");
                        String keterangan = jsonAll.getString("keterangan");
                        System.out.println("rc datasync: " + rc);
                        System.out.println("keterangan datasync: " + keterangan);
                        if (rc.equals("00")) {
                            String waktu = jsonAll.getString("waktu");
                            System.out.println("String waktu: " + waktu + " --- " + queryConfig.getDateConfig());

                            if (usernameShared == null) {
                                System.out.println("Username di Device Null");
                                File file = new File(Constant.APP_PATH);
                                if (!file.exists()) {
                                    MySQLiteHelper.initApp(mContext);
                                } else {
                                }
//                                if (querySD.isSettleAvailable().equals("0")) {
                                queryDD.deleteAllDetailInventory();
                                queryDD.deleteAllPhoto();
                                queryDD.deleteAllDataPencairan();
                                queryDD.deleteAllTGT();
                                queryDD.deleteAllDetilDataPencairan();
                                queryDD.deleteAllDetilTGT();
                                queryDD.deleteAllSettle();
                                queryDD.deleteAllDetilSettle();
                                queryDD.deleteAllDataBooked();
                                queryDD.deleteAllMasterSettle();
                                queryDD.deleteAllSequence();
                                queryDD.deleteAllParameter();
                                new syncData(mContext).execute();
                            } else if (usernameShared != null) {
                                //Kasus kalo misal sukses login tapi beda username, maka harus sync data ulang
                                if (usernameShared.equals(username)) {
                                    if (Function.compareDate(waktu, queryConfig.getDateConfig()) == "sama") {
                                        // Function.showAlert(mContext, Function.compareDate(waktu, queryConfig.getDateConfig()));
                                        Intent i = new Intent(MenuLogin.this, MenuUtama.class);
                                        startActivity(i);
                                        finish();
                                        System.out.println("Waktu Sama");

//                                if (querySD.isSettleAvailable().equals("0")) {
//
//                                } else {
//                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                                    builder.setTitle("Pesan");
//                                    builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Dari User: " + usernameShared + " Mohon Lakukan Settlement Terlebih Dahulu");
//                                    builder.setIcon(R.drawable.ic_warning_black_24dp);
//                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            Intent i = new Intent(MenuLogin.this, MenuSettlement.class);
//                                            startActivity(i);
//                                            finish();
//                                        }
//                                    });
//                                    AlertDialog alert1 = builder.create();
//                                    alert1.show();
//                                }

                                    } else if (Function.compareDate(waktu, queryConfig.getDateConfig()) == "beda") {
                                        //Function.showAlert(mContext, Function.compareDate(waktu, queryConfig.getDateConfig()));
                                        System.out.println("Waktu Beda");
                                        File file = new File(Constant.APP_PATH);
                                        if (!file.exists()) {
                                            MySQLiteHelper.initApp(mContext);
                                        } else {
                                        }
//                                if (querySD.isSettleAvailable().equals("0")) {
                                        queryDD.deleteAllDetailInventory();
                                        queryDD.deleteAllPhoto();
                                        queryDD.deleteAllDataPencairan();
                                        queryDD.deleteAllTGT();
                                        queryDD.deleteAllDetilDataPencairan();
                                        queryDD.deleteAllDetilTGT();
                                        queryDD.deleteAllSettle();
                                        queryDD.deleteAllDetilSettle();
                                        queryDD.deleteAllDataBooked();
                                        queryDD.deleteAllMasterSettle();
                                        queryDD.deleteAllSequence();
                                        queryDD.deleteAllParameter();
                                        new syncData(mContext).execute();
                                    }
                                } else if (!usernameShared.equals(username)) {
                                    System.out.println("Kasus Beda Device");
                                    File file = new File(Constant.APP_PATH);
                                    if (!file.exists()) {
                                        MySQLiteHelper.initApp(mContext);
                                    } else {
                                    }
                                    queryDD.deleteAllDetailInventory();
                                    queryDD.deleteAllPhoto();
                                    queryDD.deleteAllDataPencairan();
                                    queryDD.deleteAllTGT();
                                    queryDD.deleteAllDetilDataPencairan();
                                    queryDD.deleteAllDetilTGT();
                                    queryDD.deleteAllSettle();
                                    queryDD.deleteAllDetilSettle();
                                    queryDD.deleteAllDataBooked();
                                    queryDD.deleteAllMasterSettle();
                                    queryDD.deleteAllSequence();
                                    queryDD.deleteAllParameter();
                                    new syncData(mContext).execute();
                                }
                            }


                        } else if (rc.equals("T1")) {
                            Function.showAlert(mContext, keterangan);
                        } else if (rc.equals("T2")) {
                            Function.showAlert(mContext, keterangan);
                        } else if (rc.equals("T3")) {
                            Function.showAlert(mContext, keterangan);
                        } else if (rc.equals("T4")) {
                            Function.showAlert(mContext, keterangan);
                        } else if (rc.equals("T5")) {
                            Function.showAlert(mContext, keterangan);
                        } else {
                            Function.showAlert(mContext, keterangan);
                        }
                    } catch (JSONException e) {
                        Function.showAlert(mContext, result);
                        e.printStackTrace();
                    } catch (Exception e) {
                        Function.showAlert(mContext, result);
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Function.showAlert(mContext, result);
            }


            // new syncData(mContext).execute();
        }
    }

    class syncData extends AsyncTask<String, String, String> {
        Context context;

        private syncData(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle("Sync Data");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            SyncData querySD = new SyncData();
            System.out.println("Masuk SyncData");
            querySD.insertNewDataToDB(mContext, JsonSyncData, version, periodik_track);
//            Function.generateNoteOnSD(mContext, "syncdata.txt", JsonSyncData);
            //putJson();
            return "Sukses";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            //Toast.makeText(mContext, result, Toast.LENGTH_SHORT).show();

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Pesan");
            builder.setMessage("Sukses Sync Data");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent i = new Intent(MenuLogin.this, MenuUtama.class);
                    startActivity(i);
                    finish();
                }
            });

            AlertDialog alert1 = builder.create();
            alert1.show();


        }

    }


    public static void longLog(String str) {
        if (str.length() > 4000) {
            Log.d("ini contoh json", str.substring(0, 4000));
            //System.out.println("return jsonnya: "+str.substring(0, 4000));
            longLog(str.substring(4000));
        } else
            Log.d("ini contoh json", str);
        //System.out.println("return jsonnya: "+str);
    }

}
