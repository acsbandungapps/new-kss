package com.newkss.acs.newkss.Menu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.newkss.acs.newkss.Model.Bucket;
import com.newkss.acs.newkss.Model.Config;
import com.newkss.acs.newkss.Model.DeleteData;
import com.newkss.acs.newkss.Model.Detail_Bucket;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.IsiPenagihan;
import com.newkss.acs.newkss.Model.Penagihan;
import com.newkss.acs.newkss.Model.Pencairan;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.ModelBaru.Data_Pencairan;
import com.newkss.acs.newkss.ModelBaru.Detil_Data_Pencairan;
import com.newkss.acs.newkss.ModelBaru.Detil_TGT;
import com.newkss.acs.newkss.ModelBaru.TGT;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.JSONParser;
import com.newkss.acs.newkss.Util.MySQLiteHelper;
import com.newkss.acs.newkss.Util.SyncData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by acs on 9/4/17.
 */

public class MenuLoginBackup extends Activity{

    private EditText etUsernameLogin, etPasswordLogin;
    private TextView txtSyncPassword, txtVersionLogin;
    private Button btnResetLogin, btnLogin;

    private CheckBox chkIngatSayaLogin;
    private String username, password, resultfromJson;

    JSONParser jsonParser = new JSONParser();

    ProgressDialog progressDialog;
    Context mContext;

    MySQLiteHelper mm;

    //npay fpay
    List<String> namaBucket = new ArrayList<>();
    //current 1-30
    List<Penagihan> namaPenagihan;
    List<Config> listConfig = new ArrayList<>();

    //Buat bedain nama penagihan, current,1-30 dll
    List<IsiPenagihan> listisiPenagihan;
    Detail_Inventory querydetinv = new Detail_Inventory();
    Penagihan p = new Penagihan();
    Penagihan p1 = new Penagihan();
    Bucket b = new Bucket();
    Config con = new Config();
    Config cc;
    Detail_Bucket ddb = new Detail_Bucket();

//    Detail_Inventory_Pencairan queryDIP = new Detail_Inventory_Pencairan();

    SharedPreferences shared;
    private String usernameShared, passwordShared, nikShared;
    private String contohJson = "";
    private String hasilJson = "";

    Config queryConfig = new Config();
    DeleteData queryDD = new DeleteData();

    String periodik_track = "";
    Settle_Detil querySD = new Settle_Detil();

    ProgressDialog pd;
    String url = "";
    private boolean bUpgrade;
    String szError = null;
    SyncData sd = new SyncData();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.menulogin);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = this;

        File file = new File(Constant.APP_PATH + Function.getPackageName(mContext) + File.separator + Constant.HIDDEN_FOLDER + File.separator + Constant.DB_NAME);
        if (!file.exists()) {
            MySQLiteHelper.initApp(mContext);
        } else {
        }

        Function.triggerAlarm(mContext);
        Function.triggerAlarmSendOfflineData(mContext);
        MySQLiteHelper.initDB(mContext);

        etUsernameLogin = (EditText) findViewById(R.id.etUsernameLogin);
        etPasswordLogin = (EditText) findViewById(R.id.etPasswordLogin);
        txtVersionLogin = (TextView) findViewById(R.id.txtVersionLogin);
        txtSyncPassword = (TextView) findViewById(R.id.txtSyncPassword);
        btnResetLogin = (Button) findViewById(R.id.btnResetLogin);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        chkIngatSayaLogin = (CheckBox) findViewById(R.id.chkIngatSayaLogin);

        //checkFirstActivation();
        initUI();
    }


    private boolean checkFirstActivation() {


        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            usernameShared = (shared.getString(Constant.SHARED_USERNAME, ""));
            passwordShared = (shared.getString(Constant.SHARED_PASSWORD, ""));
            nikShared = (shared.getString(Constant.NIK, ""));
            etUsernameLogin.setText(usernameShared);

            // etPasswordLogin.setText(passwordShared);
            System.out.println("Ada Data");
            return true;
        } else {
            Intent i = new Intent(mContext, MenuInisialisasi.class);
            startActivity(i);
            finish();
            System.out.println("Data Kosong");
            return false;
        }
    }

    private String getVersion() {
        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return packageInfo.versionName;
    }

    private void initUI() {
        etUsernameLogin.setText("SUP738832");
        etPasswordLogin.setText("123456");

        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            usernameShared = (shared.getString(Constant.SHARED_USERNAME, ""));
            passwordShared = (shared.getString(Constant.SHARED_PASSWORD, ""));
            nikShared = (shared.getString(Constant.NIK, ""));
            etUsernameLogin.setText(usernameShared);
            // etPasswordLogin.setText(passwordShared);
            System.out.println("Ada Data");
//        Bundle bundle = getIntent().getExtras();
//        username = bundle.getString(Constant.JSON_USERNAME);
//        password = bundle.getString(Constant.JSON_PASSWORD);
//        etUsernameLogin.setText(username);
//        etPasswordLogin.setText(password);
            try {
                PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                txtVersionLogin.setText("Versi " + packageInfo.versionName);
            } catch (Exception e) {
                e.printStackTrace();
            }


            etPasswordLogin.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    final int DRAWABLE_RIGHT = 2;
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        if (motionEvent.getRawX() >= (etPasswordLogin.getRight() - etPasswordLogin.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            // your action here
                            if (etPasswordLogin.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                                etPasswordLogin.setInputType(InputType.TYPE_CLASS_TEXT |
                                        InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            } else {
                                etPasswordLogin.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            }
                            etPasswordLogin.setSelection(etPasswordLogin.getText().length());
                            return true;
                        }
                    }
                    return false;
                }
            });

            txtSyncPassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(mContext, MenuChangePassword.class);
                    startActivity(i);
                    finish();
                }
            });

            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    File file = new File(Constant.APP_PATH + Function.getPackageName(mContext) + File.separator + Constant.HIDDEN_FOLDER + File.separator + Constant.DB_NAME);
                    if (!file.exists()) {
                        MySQLiteHelper.initApp(mContext);
                    } else {
                    }
                    MySQLiteHelper.initDB(mContext);
                    username = etUsernameLogin.getText().toString();
                    password = etPasswordLogin.getText().toString();

                    if (username.isEmpty() || password.isEmpty()) {
                        Function.showAlert(mContext, "Mohon Lengkapi Kolom");
                    } else {
                        new execLogin(mContext).execute();
                        //new execUpdateApk().execute();

                        // updateAPK();
//                    for(int i=0;i<5;i++){
//                        System.out.println(queryConfig.getDataConfig().getNama_config());
//                    }


                        // new getResponse(mContext).execute();
                        // System.out.println("exist "+queryConfig.checkExistPeriodicTrack());
//                    Config cc=new Config();
//                    cc.deleteAllDataConfig();


//                    String header = Function.getImei(mContext) + ":" + Function.getWaktuSekarangMilis();
//                    System.out.println("Header: " + header);
//                    System.out.println("Header base64: " + new String(Function.encodeBase64(header)));
//                    new syncData(mContext).execute();

                    }


                }
            });
        }
    }

    private void upgradeApps() {
        url = "http://202.159.100.162:81/ksp-sms-apk/Mobile_BSS_1.0.6.apk";
        //upgradeProcessHandler.sendEmptyMessage(0);
        new DownloadFile().execute();
    }

    class DownloadFile extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setProgress(0);
            progressDialog.setTitle("Mobile BSS");
            progressDialog.setMessage("Update Application");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(true);
            progressDialog.setMax(100);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                HttpURLConnection c = null;
                String PATH = "";
                File file = null;
                File outputFile = null;
                FileOutputStream fos = null;
                InputStream is = null;

//                        URL apkurl = new URL(url
//                                + "MobileSurvey/Mobile_CIS_BPRKS.apk");
                URL apkurl = new URL(url);
                c = (HttpURLConnection) apkurl.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.setConnectTimeout(20 * 1000);
                c.connect();

                int lengthOfFile = c.getContentLength();
                PATH = Environment.getExternalStorageDirectory()
                        + "/download/";
                file = new File(PATH);
                file.mkdirs();
                outputFile = new File(file,
                        "Mobile BSS.apk");
                fos = new FileOutputStream(outputFile);
                is = c.getInputStream();

                long total = 0;
                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    total += len1;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    fos.write(buffer, 0, len1);
                }
                fos.close();
                is.close();// till here, it works fine - .apk is
                // download to my
                // sdcard in download file
                bUpgrade = true;
                c.disconnect();

            } catch (IOException e) {
                e.printStackTrace();
                szError = e.getMessage().toString();
            } catch (Exception e) {
                e.printStackTrace();
                szError = e.getMessage().toString();
            } finally {
                // upgradeHandler.sendEmptyMessage(0);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            progressDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            if (szError == null) {
                if (bUpgrade) {
                    System.out.println("BUPGRADE1");
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    System.out.println("BUPGRADE2");
                    intent.setDataAndType(Uri.fromFile(new File(Environment
                                    .getExternalStorageDirectory()
                                    + "/download/"
                                    + "Mobile BSS.apk")),
                            "application/vnd.android.package-archive");
                    System.out.println("BUPGRADE3");
                    startActivity(intent);
                    System.out.println("BUPGRADE4");
                } else {
                    Toast.makeText(getApplicationContext(), "Upgrade tidak berhasil. Cek Koneksi ke server.", Toast.LENGTH_SHORT).show();
                }
            } else {
                System.out.println("Gagal " + szError);
                Toast.makeText(getApplicationContext(), "Gagal " + szError, Toast.LENGTH_SHORT).show();

            }

        }
    }

//    private Handler upgradeProcessHandler = new Handler() {
//        public void handleMessage(android.os.Message msg) {
//            pd = new ProgressDialog(mContext);
//            pd.setProgress(0);
//            pd.setTitle("Mobile BSS");
//            pd.setMessage("Update Application");
//            pd.setIndeterminate(false);
//            pd.setCancelable(true);
//            pd.setMax(100);
//            pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            pd.show();
//            final int totalProgressTime = 100;
//            new Thread(new Runnable() {
//                public void run() {
//                    // TODO Auto-generated method stub
//                    try {
//                        int jumpTime = 0;
//
//                        HttpURLConnection c = null;
//                        String PATH = "";
//                        File file = null;
//                        File outputFile = null;
//                        FileOutputStream fos = null;
//                        InputStream is = null;
//
////                        URL apkurl = new URL(url
////                                + "MobileSurvey/Mobile_CIS_BPRKS.apk");
//                        URL apkurl = new URL(url);
//                        c = (HttpURLConnection) apkurl.openConnection();
//                        c.setRequestMethod("GET");
//                        c.setDoOutput(true);
//                        c.setConnectTimeout(20 * 1000);
//                        c.connect();
//
//                        PATH = Environment.getExternalStorageDirectory()
//                                + "/download/";
//                        file = new File(PATH);
//                        file.mkdirs();
//                        outputFile = new File(file,
//                                "Mobile BSS.apk");
//                        fos = new FileOutputStream(outputFile);
//                        is = c.getInputStream();
//
//                        byte[] buffer = new byte[1024];
//                        int len1 = 0;
//                        while ((len1 = is.read(buffer)) != -1) {
//                            fos.write(buffer, 0, len1);
//                        }
//                        fos.close();
//                        is.close();// till here, it works fine - .apk is
//                        // download to my
//                        // sdcard in download file
//                        bUpgrade = true;
//                        c.disconnect();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                        szError = e.getMessage().toString();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        szError = e.getMessage().toString();
//                    } finally {
//                        upgradeHandler.sendEmptyMessage(0);
//                    }
//                }
//            }).start();
//        }
//    };
//
//    private Handler upgradeHandler = new Handler() {
//        public void handleMessage(android.os.Message msg) {
//            pd.dismiss();
//            if (szError == null) {
//                if (bUpgrade) {
//                    System.out.println("BUPGRADE1");
//                    Intent intent = new Intent(Intent.ACTION_VIEW);
//                    System.out.println("BUPGRADE2");
//                    intent.setDataAndType(Uri.fromFile(new File(Environment
//                                    .getExternalStorageDirectory()
//                                    + "/download/"
//                                    + "Mobile BSS.apk")),
//                            "application/vnd.android.package-archive");
//                    System.out.println("BUPGRADE3");
//                    startActivity(intent);
//                    System.out.println("BUPGRADE4");
//                } else {
//                    Toast.makeText(getApplicationContext(), "Upgrade tidak berhasil. Cek Koneksi ke server.", Toast.LENGTH_SHORT).show();
//                }
//            } else {
//                System.out.println("Gagal " + szError);
//                Toast.makeText(getApplicationContext(), "Gagal " + szError, Toast.LENGTH_SHORT).show();
//
//            }
//        }
//
//        ;
//    };


    private void updateAPK() {
        try {
            int count;
            URL url = new URL("https://drive.google.com/open?id=0B-ZgQiKG0ybTOThnQ3ctb2R6SzA");
            URLConnection conection = url.openConnection();
            conection.connect();
            // getting file length
            int lenghtOfFile = conection.getContentLength();

            // input stream to read file - with 8k buffer
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream to write file
            OutputStream output = new FileOutputStream("/sdcard/downloadedfile.jpg");

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
//                publishProgress(""+(int)((total*100)/lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

//
//            //get destination to update file and set Uri
//            //TODO: First I wanted to store my update .apk file on internal storage for my app but apparently android does not allow you to open and install
//            //aplication with existing package from there. So for me, alternative solution is Download directory in external storage. If there is better
//            //solution, please inform us in comment
//            String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
//            String fileName = "BSS.apk";
//            destination += fileName;
//            final Uri uri = Uri.parse("file://" + destination);
//
//            //Delete update file if exists
//            File file = new File(destination);
//            if (file.exists())
//                //file.delete() - test this, I think sometimes it doesnt work
//                file.delete();
//            String url = "https://drive.google.com/open?id=0B-ZgQiKG0ybTOThnQ3ctb2R6SzA";
//            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
//            request.setDescription("tes download apk");
//            request.setTitle(MenuLogin.this.getString(R.string.app_name));
////set destination
//            request.setDestinationUri(uri);
//
//            // get download service and enqueue file
//            final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
//            final long downloadId = manager.enqueue(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class execUpdateApk extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle("Update APK");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            //String url = "https://drive.google.com/open?id=0B-ZgQiKG0ybTOThnQ3ctb2R6SzA";

//            try {


//                HttpURLConnection c = null;
//                String PATH = "";
//                File file = null;
//                File outputFile = null;
//                FileOutputStream fos = null;
//                InputStream is = null;
//                        /*
//						 * if(checkMenuXML(url + "MenuXML.xml")){ URL menuURL =
//						 * new URL(url + "MenuXML.xml");
//						 *
//						 * c = (HttpURLConnection) menuURL.openConnection();
//						 *
//						 * c.setRequestMethod("GET"); c.setDoOutput(true);
//						 * c.connect();
//						 *
//						 * PATH = GlobalVar.szPath + "MenuXML.xml"; file = new
//						 * File(PATH); file.mkdirs(); outputFile = new
//						 * File(file, "MenuXML.xml"); fos = new
//						 * FileOutputStream(outputFile); is =
//						 * c.getInputStream();
//						 *
//						 * byte[] buffer = new byte[1024]; int len1 = 0; while
//						 * ((len1 = is.read(buffer)) != -1) { fos.write(buffer,
//						 * 0, len1); } fos.close(); is.close(); c.disconnect();
//						 * }
//						 */
//
//						/*URL apkurl = new URL(url
//								+ "MobileSurvey/Mobile_Survey_Collector.apk");*/
//
//                URL apkurl = new URL(url);
//                c = (HttpURLConnection) apkurl.openConnection();
//                c.setRequestMethod("GET");
//                c.setDoOutput(true);
//                c.setConnectTimeout(20 * 1000);
//                c.connect();
//
//                PATH = Environment.getExternalStorageDirectory()
//                        + "/download/";
//                file = new File(PATH);
//                file.mkdirs();
//                outputFile = new File(file,
//                        "app.apk");
//                fos = new FileOutputStream(outputFile);
//                is = c.getInputStream();
//
//                byte[] buffer = new byte[1024];
//                int len1 = 0;
//                while ((len1 = is.read(buffer)) != -1) {
//                    fos.write(buffer, 0, len1);
//                }
//                fos.close();
//                is.close();// till here, it works fine - .apk is
//                // download to my
//                // sdcard in download file
//               // bUpgrade = true;
//                c.disconnect();


//                URL url = new URL("https://drive.google.com/open?id=0B-ZgQiKG0ybTOThnQ3ctb2R6SzA");
//                HttpsURLConnection c = (HttpsURLConnection) url.openConnection();
//                c.setRequestMethod("GET");
//                c.setDoOutput(true);
//                c.connect();
//
//                String PATH = Environment.getExternalStorageDirectory() + "/Download/";
//                File file = new File(PATH);
//                file.mkdirs();
////                File outputFile = new File(file, "app.apk");
////                FileOutputStream fos = new FileOutputStream(outputFile);
////
////                InputStream is = c.getInputStream();
////
////                byte[] buffer = new byte[1024];
////                int len1 = 0;
////                while ((len1 = is.read(buffer)) != -1) {
////                    fos.write(buffer, 0, len1);
////                }
////                fos.close();
////                is.close();//till here, it works fine - .apk is download to my sdcard in download file
//
//                URL u = new URL("https://drive.google.com/open?id=0B-ZgQiKG0ybTOThnQ3ctb2R6SzA");
//                URLConnection conn = u.openConnection();
//                int contentLength = conn.getContentLength();
//
//                DataInputStream stream = new DataInputStream(u.openStream());
//
//                byte[] buffer = new byte[contentLength];
//                stream.readFully(buffer);
//                stream.close();
//
//                DataOutputStream fos = new DataOutputStream(new FileOutputStream(file));
//                fos.write(buffer);
//                fos.flush();
//                fos.close();
//
//
//            } catch (Exception e) {
//                e.printStackTrace();
////                Toast.makeText(getApplicationContext(), "Update error!", Toast.LENGTH_LONG).show();
//            }


//                URL url = new URL("https://drive.google.com/open?id=0B-ZgQiKG0ybTOThnQ3ctb2R6SzA");
//                HttpsURLConnection c = (HttpsURLConnection) url.openConnection();
//                c.setRequestMethod("GET");
//                c.setDoOutput(true);
//                c.connect();
//
//                String PATH = Environment.getExternalStorageDirectory() + "/Download/";
//                File file = new File(PATH);
//                file.mkdirs();
//                File outputFile = new File(file, "update.apk");
//                if(outputFile.exists()){
//                    outputFile.delete();
//                }
//                FileOutputStream fos = new FileOutputStream(outputFile);
//
//                InputStream is = c.getInputStream();
//
//                byte[] buffer = new byte[1024];
//                int len1 = 0;
//                while ((len1 = is.read(buffer)) != -1) {
//                    fos.write(buffer, 0, len1);
//                }
//                fos.close();
//                is.close();
//
////                Intent intent = new Intent(Intent.ACTION_VIEW);
////                intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/Download/"+"update.apk")), "application/vnd.android.package-archive");
////                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag android returned a intent error!
////                mContext.startActivity(intent);
//
//
//            } catch (Exception e) {
//                Log.e("UpdateAPP", "Update error! " + e.getMessage());
//            }


//            try{
//                InputStream is;
//                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//                StrictMode.setThreadPolicy(policy);
//
//                URL url = new URL("https://drive.google.com/open?id=0B-ZgQiKG0ybTOThnQ3ctb2R6SzA");
//
//                HttpURLConnection c = (HttpURLConnection) url.openConnection();
//                c.setRequestMethod("GET");
//                c.connect();
//
//                String PATH = Environment.getExternalStorageDirectory() + "/Download/";
//                File file = new File(PATH);
//                file.mkdirs();
//                File outputFile = new File(file, "app.apk");
//                FileOutputStream fos = new FileOutputStream(outputFile);
//
//                is = c.getInputStream();
//
//                byte[] buffer = new byte[1024];
//                int len1 = 0;
//                while ((len1 = is.read(buffer)) != -1) {
//                    fos.write(buffer, 0, len1);
//                }
//                fos.close();
//                is.close();
//
//
//
//
//                Intent promptInstall = new Intent(Intent.ACTION_VIEW);
//                promptInstall.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/Download/" + "app.apk")), "application/vnd.andriod.package-archive");
//                promptInstall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(promptInstall);
//
//            } catch (IOException e) {
//                Toast.makeText(getApplicationContext(), "Update error!" + e.toString() + e.getStackTrace().toString(), Toast.LENGTH_LONG).show();
//                //TextView txtQuestion = (TextView) findViewById(R.id.txtViewQuestion);
//              //  txtQuestion.setText(e.toString());
//            }

//                try {
//                    //get destination to update file and set Uri
//                    //TODO: First I wanted to store my update .apk file on internal storage for my app but apparently android does not allow you to open and install
//                    //aplication with existing package from there. So for me, alternative solution is Download directory in external storage. If there is better
//                    //solution, please inform us in comment
//                    String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
//                    String fileName = "BSS.apk";
//                    destination += fileName;
//                    final Uri uri = Uri.parse("file://" + destination);
//
//                    //Delete update file if exists
//                    File file = new File(destination);
//                    if (file.exists())
//                        //file.delete() - test this, I think sometimes it doesnt work
//                        file.delete();
//                    String url = "https://drive.google.com/open?id=0B-ZgQiKG0ybTOThnQ3ctb2R6SzA";
//                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
//                    request.setDescription("tes download apk");
//                    request.setTitle(MenuLogin.this.getString(R.string.app_name));
////set destination
//                    request.setDestinationUri(uri);
//
//                    // get download service and enqueue file
//                    final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
//                    final long downloadId = manager.enqueue(request);
            //set BroadcastReceiver to install app when .apk is downloaded
//                BroadcastReceiver onComplete = new BroadcastReceiver() {
//                    public void onReceive(Context ctxt, Intent intent) {
//                        Intent install = new Intent(Intent.ACTION_VIEW);
//                        install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//install.setDataAndType(uri,manager.getMimeTypeForDownloadedFile(downloadId));
//                     //   install.setDataAndType(uri, "application/vnd.android.package-archive");
//                        startActivity(install);
//
//                        unregisterReceiver(this);
//                        finish();
//                    }
//                };
//                //register receiver for when .apk download is compete
//                registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));


//                    result = "sukses";
//                } catch (Exception e) {
//                    result = e.getMessage();
//                    e.printStackTrace();
//                }


//            try {
//                int count;
//                URL url = new URL("https://drive.google.com/open?id=0B-ZgQiKG0ybTOThnQ3ctb2R6SzA");
//                URLConnection conection = url.openConnection();
//                conection.connect();
//                // getting file length
//                int lenghtOfFile = conection.getContentLength();
//
//                // input stream to read file - with 8k buffer
//                InputStream input = new BufferedInputStream(url.openStream(), 8192);
//
//                // Output stream to write file
//                OutputStream output = new FileOutputStream("/sdcard/downloadedfile.jpg");
//
//                byte data[] = new byte[1024];
//
//                long total = 0;
//
//                while ((count = input.read(data)) != -1) {
//                    total += count;
//                    // publishing the progress....
//                    // After this onProgressUpdate will be called
////                publishProgress(""+(int)((total*100)/lenghtOfFile));
//
//                    // writing data to file
//                    output.write(data, 0, count);
//                }
//
//                // flushing output
//                output.flush();
//
//                // closing streams
//                output.close();
//                input.close();
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }


//            InputStream input = null;
//            OutputStream output = null;
//            HttpURLConnection connection = null;
//            try {
//                URL url = new URL("https://drive.google.com/open?id=0B-ZgQiKG0ybTOThnQ3ctb2R6SzA");
//                connection = (HttpURLConnection) url.openConnection();
//                connection.connect();
//
//                // expect HTTP 200 OK, so we don't mistakenly save error report
//                // instead of the file
//                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
//                    return "Server returned HTTP " + connection.getResponseCode()
//                            + " " + connection.getResponseMessage();
//                }
//
//                // this will be useful to display download percentage
//                // might be -1: server did not report the length
//                int fileLength = connection.getContentLength();
//
//                // download the file
//                input = connection.getInputStream();
//                output = new FileOutputStream("/sdcard/file_name.extension");
//
//                byte data[] = new byte[4096];
//                long total = 0;
//                int count;
//                while ((count = input.read(data)) != -1) {
//                    // allow canceling with back button
//                    if (isCancelled()) {
//                        input.close();
//                        return null;
//                    }
//                    total += count;
//                    // publishing the progress....
//                    if (fileLength > 0) // only if total length is known
//                        publishProgress((int) (total * 100 / fileLength));
//                    output.write(data, 0, count);
//                }
//            } catch (Exception e) {
//                return e.toString();
//            } finally {
//                try {
//                    if (output != null)
//                        output.close();
//                    if (input != null)
//                        input.close();
//                } catch (IOException ignored) {
//                }
//
//                if (connection != null)
//                    connection.disconnect();
//            }
//            return null;

            try {
                //String url = "https://drive.google.com/open?id=0B-ZgQiKG0ybTOThnQ3ctb2R6SzA";
                String url = "http://202.159.100.162:81/ksp-sms-apk/Mobile_BSS_1.0.6.apk";
//    String s = url.replaceAll(" " , "%20");
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                request.setDescription("Some description");
                request.setTitle("Some title");
// in order for this if to run, you must use the android 3.2 to compile your app
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                }
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "tes.apk");

// get download service and enqueue file
                DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                manager.enqueue(request);
            } catch (Exception e) {
                e.printStackTrace();
            }


            return "sukses";
            //return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/Download/" + "app.apk")), "application/vnd.android.package-archive");
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);


            // Function.showAlert(mContext, result);
        }
    }

    public class execLogin extends AsyncTask<String, String, String> {
        Context context;

        private execLogin(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle("Validasi Data");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            JSONObject json = new JSONObject();
            try {
                String n1 = "0";
                String n2 = "0";
                String n3 = "0";
                String n4 = n1 + n2 + n3;
                String header = Function.getImei(mContext) + ":" + n4 + ":" + Function.getWaktuSekarangMilis();
                System.out.println("Header: " + header);
                System.out.println("Header base64: " + new String(Function.encodeBase64(header)));
                json.put(Constant.USERNAME, username);
                json.put(Constant.PASSWORD, Function.convertStringtoMD5(password));
                json.put(Constant.WAKTU, Function.getWaktuSekarangMilis());
                System.out.println("Data Dikirim " + json.toString());
                System.out.println("URL: " + Constant.SERVICE_LOGIN);
                hasilJson = jsonParser.HttpRequestPost(Constant.SERVICE_LOGIN, json.toString(), Constant.TimeOutConnection, new String(Function.encodeBase64(header)));
                longLog(hasilJson);
                System.out.println("HASIL JSON: " + hasilJson);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return hasilJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            try {
                JSONObject jsonAll = new JSONObject(result);

                if (result.contains(Constant.CONNECTION_LOST)) {
                    Function.showAlert(mContext, Constant.CONNECTION_LOST);
                } else if (result.contains(Constant.CONNECTION_ERROR)) {
                    Function.showAlert(mContext, Constant.CONNECTION_ERROR);
                } else {
                    String keterangan = jsonAll.getString("keterangan");
                    String rc = jsonAll.getString("rc");
                    if (rc.equals("00")) {
                        shared = getApplicationContext().getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                        SharedPreferences.Editor editor = shared.edit();
                        editor.putString(Constant.SHARED_USERNAME, username);
                        editor.putString(Constant.SHARED_PASSWORD, Function.convertStringtoMD5(password));
                        editor.commit();

                    } else if (rc.equals("T4")) {
                        Function.showAlert(mContext, keterangan);
                    } else if (rc.equals("10")) {
                        Function.showAlert(mContext, keterangan);
                    } else if (rc.equals("T3")) {
                        Function.showAlert(mContext, keterangan);
                    }
                }


            } catch (Exception e) {
                Function.showAlert(mContext, result);
                e.printStackTrace();

            }

//            try {
//                JSONObject jsonAll = new JSONObject(result);
//                if (result.contains(Constant.CONNECTION_LOST)) {
//                    Function.showAlert(mContext, Constant.CONNECTION_LOST);
//                } else if (result.contains(Constant.CONNECTION_ERROR)) {
//                    Function.showAlert(mContext, Constant.CONNECTION_ERROR);
//                } else {
//                    try {
//                        //String periodik_track = "50";
//                        String keterangan = jsonAll.getString("keterangan");
//                        // String waktu = jsonAll.getString("waktu");
//                        String rc = jsonAll.getString("rc");
//                        System.out.println("rc " + rc);
//                        // System.out.println("String waktu: " + waktu + " --- " + queryConfig.getDateConfig());
//
//                        if (rc.equals("00")) {
//                            periodik_track = jsonAll.getString(Constant.PERIODIK_TRACK);
//                            System.out.println("Masuk 00");
//                            System.out.println(queryConfig.checkExistPeriodicTrack());
////                            cc = new Config();
////                            cc.setNama_config("periodik_track");
////                            cc.setValue(periodik_track);
////                            cc.insertPeriodikTrack(cc);
//
//                            shared = getApplicationContext().getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
//                            SharedPreferences.Editor editor = shared.edit();
//                            editor.putString(Constant.SHARED_USERNAME, username);
//                            editor.putString(Constant.SHARED_PASSWORD, Function.convertStringtoMD5(password));
//                            editor.commit();
//
//                            if (queryConfig.checkExistPeriodicTrack().equals("0")) {
//                                System.out.println("Masuk not exist");
////                                cc = new Config();
////                                cc.setNama_config("periodik_track");
////                                cc.setValue(periodik_track);
////                                cc.insertPeriodikTrack(cc);
//                            } else if (queryConfig.checkExistPeriodicTrack().equals("1")) {
//                                System.out.println("Masuk exist");
//                                cc = new Config();
//                                cc.updatePeriodikTrack(periodik_track);
//                            }
//
//
//                            //TODO Check Settle Available
//                            // check kalo misal pas esok hari masih ada settle yang blm dikirim maka harus kirim dulu
//
////                            File file = new File(Constant.APP_PATH + Function.getPackageName(mContext) + File.separator + Constant.HIDDEN_FOLDER + File.separator + Constant.DB_NAME);
////                            if (file.exists()) {
////
////                            }
////                            else if(!file.exists()){
////
////                            }
//                            MySQLiteHelper.initDB(mContext);
//                            if (querySD.isSettleAvailable().equals("0")) {
//                                new getResponse(mContext).execute();
//                            } else {
//                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                                builder.setTitle("Pesan");
//                                builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Mohon Lakukan Settlement Terlebih Dahulu");
//                                builder.setIcon(R.drawable.ic_warning_black_24dp);
//                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        Intent i = new Intent(MenuLogin.this, MenuSettlement.class);
//                                        startActivity(i);
//                                        finish();
//                                    }
//                                });
//
//                                AlertDialog alert1 = builder.create();
//                                alert1.show();
//
//                            }
//                        } else if (rc.equals("T1")) {
//                            Function.showAlert(mContext, keterangan);
//
//                        } else if (rc.equals("T2")) {
//                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                            builder.setTitle("Pesan");
//                            builder.setMessage(keterangan);
//                            builder.setIcon(R.drawable.ic_warning_black_24dp);
//                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                    shared.edit().clear().commit();
//                                    Intent i = new Intent(MenuLogin.this, MenuInisialisasi.class);
//                                    startActivity(i);
//                                    finish();
//
//                                }
//                            });
//
//                            AlertDialog alert1 = builder.create();
//                            alert1.show();
//                        } else if (rc.equals("T3")) {
//                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                            builder.setTitle("Pesan");
//                            builder.setMessage(keterangan);
//                            builder.setIcon(R.drawable.ic_warning_black_24dp);
//                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                    shared.edit().clear().commit();
//                                    Intent i = new Intent(MenuLogin.this, MenuInisialisasi.class);
//                                    startActivity(i);
//                                    finish();
//                                }
//                            });
//
//                            AlertDialog alert1 = builder.create();
//                            alert1.show();
//                        } else if (rc.equals("T4")) {
//                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                            builder.setTitle("Pesan");
//                            builder.setMessage(keterangan);
//                            builder.setIcon(R.drawable.ic_warning_black_24dp);
//                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                    shared.edit().clear().commit();
//                                    Intent i = new Intent(MenuLogin.this, MenuInisialisasi.class);
//                                    startActivity(i);
//                                    finish();
//                                }
//                            });
//
//                            AlertDialog alert1 = builder.create();
//                            alert1.show();
//                        } else if (rc.equals("T5")) {
//                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                            builder.setTitle("Pesan");
//                            builder.setMessage(keterangan);
//                            builder.setIcon(R.drawable.ic_warning_black_24dp);
//                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                    shared.edit().clear().commit();
//                                    Intent i = new Intent(MenuLogin.this, MenuInisialisasi.class);
//                                    startActivity(i);
//                                    finish();
//                                }
//                            });
//
//                            AlertDialog alert1 = builder.create();
//                            alert1.show();
//                        } else {
//                            Function.showAlert(mContext, keterangan);
//                        }
//
//                    } catch (JSONException e) {
//                        Function.showAlert(mContext, e.getMessage());
//                        e.printStackTrace();
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                Function.showAlert(mContext, result);
//            }


        }
    }

    class getResponse extends AsyncTask<String, String, String> {
        Context context;

        private getResponse(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle("Mengecek Data");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            JSONObject json = new JSONObject();
            try {
                String header = Function.getImei(mContext) + ":" + nikShared + ":" + Function.getWaktuSekarangMilis();
                System.out.println("Header: " + header);
                System.out.println("Header base64: " + new String(Function.encodeBase64(header)));
                json.put(Constant.USERNAME, username);
                json.put(Constant.PASSWORD, Function.convertStringtoMD5(password));
                json.put(Constant.WAKTU, Function.getWaktuSekarangMilis());
                System.out.println("Data Dikirim " + json.toString());
                System.out.println("URL: " + Constant.SERVICE_DATA_SYNC);
                contohJson = jsonParser.HttpRequestPost(Constant.SERVICE_DATA_SYNC, json.toString(), Constant.TimeOutConnection, new String(Function.encodeBase64(header)));
                longLog(contohJson);
                shared = getApplicationContext().getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                //SharedPreferences.Editor editor = shared.edit();
//                editor.putString(Constant.SHARED_USERNAME, username);
                //editor.putString(Constant.SHARED_PASSWORD, "e10adc3949ba59abbe56e057f20f883e");
                //editor.commit();
                System.out.println("HASIL JSON: " + contohJson);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return contohJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            try {
                JSONObject jsonAll = new JSONObject(result);
                if (result.contains(Constant.CONNECTION_LOST)) {
                    Function.showAlert(mContext, Constant.CONNECTION_LOST);
                } else if (result.contains(Constant.CONNECTION_ERROR)) {
                    Function.showAlert(mContext, Constant.CONNECTION_ERROR);
                } else {
                    try {
//                        String waktu = jsonAll.getString("periodik_track");

                        String rc = jsonAll.getString("rc");
                        String keterangan = jsonAll.getString("keterangan");
                        System.out.println("rc datasync: " + rc);
                        System.out.println("keterangan datasync: " + keterangan);
                        if (rc.equals("00")) {
                            String waktu = jsonAll.getString("waktu");
                            System.out.println("String waktu: " + waktu + " --- " + queryConfig.getDateConfig());
                            if (Function.compareDate(waktu, queryConfig.getDateConfig()) == "sama") {
                                // Function.showAlert(mContext, Function.compareDate(waktu, queryConfig.getDateConfig()));
                                Intent i = new Intent(MenuLoginBackup.this, MenuUtama.class);
                                startActivity(i);
                                finish();
                            } else if (Function.compareDate(waktu, queryConfig.getDateConfig()) == "beda") {
                                //Function.showAlert(mContext, Function.compareDate(waktu, queryConfig.getDateConfig()));

                                queryDD.deleteAllDetailInventory();
                                queryDD.deleteAllPhoto();
                                queryDD.deleteAllDataPencairan();
                                queryDD.deleteAllTGT();
                                queryDD.deleteAllDetilDataPencairan();
                                queryDD.deleteAllDetilTGT();
                                queryDD.deleteAllSettle();
                                queryDD.deleteAllDetilSettle();
                                queryDD.deleteAllDataBooked();
                                queryDD.deleteAllMasterSettle();
                                queryDD.deleteAllSequence();

                                new syncData(mContext).execute();
                            }
                        } else if (rc.equals("T1")) {
                            Function.showAlert(mContext, keterangan);
                        } else if (rc.equals("T2")) {
                            Function.showAlert(mContext, keterangan);
                        } else if (rc.equals("T3")) {
                            Function.showAlert(mContext, keterangan);
                        } else if (rc.equals("T4")) {
                            Function.showAlert(mContext, keterangan);
                        } else if (rc.equals("T5")) {
                            Function.showAlert(mContext, keterangan);
                        } else {
                            Function.showAlert(mContext, keterangan);
                        }


                    } catch (Exception e) {
                        Function.showAlert(mContext, result);
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Function.showAlert(mContext, result);
            }


            // new syncData(mContext).execute();
        }
    }

    class syncData extends AsyncTask<String, String, String> {
        Context context;

        private syncData(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle("Sync Data");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            putJson();
            return "Sukses";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            //Toast.makeText(mContext, result, Toast.LENGTH_SHORT).show();

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Pesan");
            builder.setMessage("Sukses Sync Data");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent i = new Intent(MenuLoginBackup.this, MenuUtama.class);
                    startActivity(i);
                    finish();
                }
            });

            AlertDialog alert1 = builder.create();
            alert1.show();


        }
    }

    private void putJson() {
        try {

            JSONObject jsonAll = new JSONObject(contohJson);
            String waktu = jsonAll.getString("waktu");

            String pencairan = jsonAll.getString("pencairan");
            String penagihan = jsonAll.getString("penagihan");

//            String rc = jsonAll.getString("rc");
//            String keterangan = jsonAll.getString("keterangan");

            cc = new Config();
            listConfig = new ArrayList<>();
            cc.setNama_config("keterangan");
            //cc.setValue(keterangan);
            listConfig.add(cc);
            cc = new Config();
            cc.setNama_config("waktu");
            cc.setValue(waktu);
            listConfig.add(cc);
            cc = new Config();
            cc.setNama_config("rc");
            //  cc.setValue(rc);
            listConfig.add(cc);
            cc = new Config();
            cc.setNama_config("Versi App");
            cc.setValue(getVersion());
            listConfig.add(cc);
            cc = new Config();
            cc.setNama_config(Constant.PERIODIK_TRACK);
            cc.setValue(periodik_track);
            listConfig.add(cc);
            con.insertDataConfig(listConfig);

//            System.out.println("Json Waktu: " + waktu);
//            System.out.println("Json Keterangan: " + keterangan);
//            System.out.println("Json Pencairan: " + pencairan);
//            System.out.println("Json PenagihanLama: " + penagihan);
//            System.out.println("Json RC: " + rc);

            //DATA PENCAIRAN
            JSONObject jsonPencairan = new JSONObject(pencairan);
            JSONArray jsonArrayPencairan = jsonPencairan.getJSONArray("data_pencairan");
            //System.out.println("JsonArrayPencairan: " + jsonArrayPencairan);
            JSONArray jsonArrayPencairan1 = new JSONArray(jsonArrayPencairan.toString());

            //System.out.println("JsonArrayPencairan1: " + jsonArrayPencairan1);

            Data_Pencairan queryDP = new Data_Pencairan();
            Pencairan queryPencairan = new Pencairan();

            for (int i = 0; i < jsonArrayPencairan1.length(); i++) {
                String dtlPencairan = jsonArrayPencairan1.getString(i);
                System.out.println("NILAI: " + i + "-" + dtlPencairan);

                JSONObject jsonDetilPencairan = new JSONObject(dtlPencairan);
                String cycleDetilPencairan = jsonDetilPencairan.getString("cycle");
                String detilInventoryPencairan = jsonDetilPencairan.getString("detail_inventory");
                //String jarakDetilPencairan = jsonDetilPencairan.getString("jarak");
                String osDetilPencairan = jsonDetilPencairan.getString("OS");
                String namaDetilPencairan = jsonDetilPencairan.getString("nama");
                String alamatDetilPencairan = jsonDetilPencairan.getString("alamat");


//                System.out.println("cycleDetilPencairan: " + cycleDetilPencairan);
//                System.out.println("detilInventoryPencairan: " + detilInventoryPencairan);
//                System.out.println("jarakDetilPencairan: " + jarakDetilPencairan);
//                System.out.println("osDetilPencairan: " + osDetilPencairan);
//                System.out.println("namaDetilPencairan: " + namaDetilPencairan);
//                System.out.println("alamatDetilPencairan: " + alamatDetilPencairan);


                String id_data_pencairan_generate = queryDP.generateIDDataPencairan();

//                queryDP.getIdDataPencairanSqlite()
                System.out.println("ID Pencairan Generate: " + id_data_pencairan_generate);
                Data_Pencairan dataPencairan = new Data_Pencairan();
                dataPencairan.setId_data_pencairan(id_data_pencairan_generate);
                dataPencairan.setCycle(cycleDetilPencairan);
                //  dataPencairan.setJarak(jarakDetilPencairan);
                dataPencairan.setOs(osDetilPencairan);
                dataPencairan.setNama(namaDetilPencairan);
                dataPencairan.setAlamat(alamatDetilPencairan);
//                queryDP.deleteAllDataPencairan();
                queryDP.insertDataPencairan(dataPencairan);

                JSONArray jsonArrDetInvPencairan = new JSONArray(detilInventoryPencairan.toString());
                for (int j = 0; j < jsonArrDetInvPencairan.length(); j++) {
                    String isiDetInvPencairan = jsonArrDetInvPencairan.getString(j);
                    JSONObject jsonIsiDetInvPencairan = new JSONObject(isiDetInvPencairan);

                    //System.out.println("Saldo Min: " + j + "-" + jsonIsiDetInvPencairan.getString("saldo_min"));
                    //String saldoMin = jsonIsiDetInvPencairan.getString("saldo_min");
                    //MASUKIN KE DETAIL INVENTORY DAN KE DETAIL BUCKET DARI NILAI DATA PENCAIRAN

                    Detail_Inventory dip = new Detail_Inventory();
                    dip.setSaldo_min(jsonIsiDetInvPencairan.getString(Constant.SALDO_MIN));
                    dip.setDpd_today(jsonIsiDetInvPencairan.getString(Constant.DPD_TODAY));
                    dip.setResume_nsbh(jsonIsiDetInvPencairan.getString(Constant.RESUME_NSBH));
                    dip.setAlmt_rumah(jsonIsiDetInvPencairan.getString(Constant.ALMT_RUMAH));
                    dip.setTgl_janji_bayar_terakhir(jsonIsiDetInvPencairan.getString(Constant.TGL_JANJI_BAYAR_TERAKHIR));
                    dip.setDenda(jsonIsiDetInvPencairan.getString(Constant.DENDA));
                    dip.setPola_bayar(jsonIsiDetInvPencairan.getString(Constant.POLA_BAYAR));

                    dip.setTgl_bayar_terakhir(jsonIsiDetInvPencairan.getString(Constant.TGL_BAYAR_TERAKHIR));

                    dip.setPokok(jsonIsiDetInvPencairan.getString(Constant.POKOK));
                    dip.setTgl_sp(jsonIsiDetInvPencairan.getString(Constant.TGL_SP));
                    dip.setTenor(jsonIsiDetInvPencairan.getString(Constant.TENOR));
                    dip.setAngsuran(jsonIsiDetInvPencairan.getString(Constant.ANGSURAN));

                    //dip.setTgl_ptp(jsonIsiDetInvPencairan.getString(Constant.TGL_PTP));
                    dip.setAction_plan(jsonIsiDetInvPencairan.getString(Constant.ACTION_PLAN));
                    dip.setNo_rekening(jsonIsiDetInvPencairan.getString(Constant.NO_REKENING));
                    dip.setHistori_sp(jsonIsiDetInvPencairan.getString(Constant.HISTORI_SP));
                    dip.setNominal_janji_bayar_terakhir(jsonIsiDetInvPencairan.getString(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR));
                    dip.setTlpn_rekomendator(jsonIsiDetInvPencairan.getString(Constant.TLPN_REKOMENDATOR));

                    dip.setSumber_bayar(jsonIsiDetInvPencairan.getString(Constant.SUMBER_BAYAR));
                    dip.setBunga(jsonIsiDetInvPencairan.getString(Constant.BUNGA));
//
                    dip.setJns_pinjaman(jsonIsiDetInvPencairan.getString(Constant.JNS_PINJAMAN));
                    dip.setNominal_bayar_terakhir(jsonIsiDetInvPencairan.getString(Constant.NOMINAL_BAYAR_TERAKHIR));
//
                    //dip.setNominal_ptp(jsonIsiDetInvPencairan.getString(Constant.NOMINAL_PTP));
                    dip.setSaldo(jsonIsiDetInvPencairan.getString(Constant.SALDO));

                    dip.setOs_pinjaman(jsonIsiDetInvPencairan.getString(Constant.OS_PINJAMAN));
//
                    dip.setKeberadaan_jaminan(jsonIsiDetInvPencairan.getString(Constant.KEBERADAAN_JAMINAN));
                    dip.setEmail(jsonIsiDetInvPencairan.getString(Constant.SUMBER_BAYAR));
//
                    dip.setTotal_kewajiban(jsonIsiDetInvPencairan.getString(Constant.TOTAL_KEWAJIBAN));
//
                    dip.setNo_loan(jsonIsiDetInvPencairan.getString(Constant.NO_LOAN));
                    //   dd.setNo_loan(noloangenerated);
                    dip.setNama_debitur(jsonIsiDetInvPencairan.getString(Constant.NAMA_DEBITUR));
                    dip.setTgl_jth_tempo(jsonIsiDetInvPencairan.getString(Constant.TGL_JTH_TEMPO));
                    dip.setNo_tlpn(jsonIsiDetInvPencairan.getString(Constant.NO_TLPN));
                    dip.setTgl_gajian(jsonIsiDetInvPencairan.getString(Constant.TGL_GAJIAN));
                    dip.setPekerjaan(jsonIsiDetInvPencairan.getString(Constant.PEKERJAAN));
                    dip.setAlmt_usaha(jsonIsiDetInvPencairan.getString(Constant.ALMT_USAHA));
                    dip.setId_data_pencairan(id_data_pencairan_generate);
                    dip.setStatus("0");


                    //data yang tadinya tidak ada
                    dip.setNo_hp(jsonIsiDetInvPencairan.getString(Constant.NO_HP));
                    dip.setAngsuran_ke(jsonIsiDetInvPencairan.getString(Constant.ANGSURAN_KE));
                    dip.setNama_upliner(jsonIsiDetInvPencairan.getString(Constant.NAMA_UPLINER));
                    dip.setGender(jsonIsiDetInvPencairan.getString(Constant.GENDER));
                    dip.setTlpn_upliner(jsonIsiDetInvPencairan.getString(Constant.TLPN_UPLINER));
                    dip.setTlpn_econ(jsonIsiDetInvPencairan.getString(Constant.TLPN_ECON));
                    dip.setHarus_bayar(jsonIsiDetInvPencairan.getString(Constant.HARUS_BAYAR));
                    dip.setNama_rekomendator(jsonIsiDetInvPencairan.getString(Constant.NAMA_REKOMENDATOR));
                    dip.setTlpn_mogen(jsonIsiDetInvPencairan.getString(Constant.TLPN_MOGEN));
                    dip.setNama_econ(jsonIsiDetInvPencairan.getString(Constant.NAMA_ECON));
                    dip.setKewajiban(jsonIsiDetInvPencairan.getString(Constant.KEWAJIBAN));
                    dip.setNama_mogen(jsonIsiDetInvPencairan.getString(Constant.NAMA_MOGEN));
                    dip.setBucket(jsonIsiDetInvPencairan.getString("bucket"));

                    querydetinv.insertDetailInventoryPencairan(dip);
                }
            }

            JSONArray jsonArraySS = jsonPencairan.getJSONArray("ss");
            JSONArray jsonArraySS1 = new JSONArray(jsonArraySS.toString());
            System.out.println("Jsonarrayss: " + jsonArraySS1);
            for (int i = 0; i < jsonArraySS1.length(); i++) {
                String noloan = jsonArraySS1.getString(i);
                JSONObject jsonNoLoanSS = new JSONObject(noloan);
                System.out.println("No_Loan ss: " + jsonNoLoanSS.getString("no_loan"));


                Detil_Data_Pencairan ddp = new Detil_Data_Pencairan();
                ddp.setId_data_pencairan(queryPencairan.getIdPencairanfromNama("ss"));
                ddp.setNo_loan(jsonNoLoanSS.getString("no_loan"));
                ddp.setTgl_kunjungan("");
                ddp.setStatus("0");
                Detil_Data_Pencairan queryddp = new Detil_Data_Pencairan();
                queryddp.insertDetilDataPencairan(ddp);
            }

            JSONArray jsonArrayBI = jsonPencairan.getJSONArray("bi");
            JSONArray jsonArrayBI1 = new JSONArray(jsonArrayBI.toString());
            System.out.println("Jsonarraybi: " + jsonArrayBI1);
            for (int i = 0; i < jsonArrayBI1.length(); i++) {
                String noloan = jsonArrayBI1.getString(i);
                JSONObject jsonNoLoanBI = new JSONObject(noloan);
                System.out.println("No_Loan bi: " + jsonNoLoanBI.getString("no_loan"));


                Detil_Data_Pencairan ddp = new Detil_Data_Pencairan();
                ddp.setId_data_pencairan(queryPencairan.getIdPencairanfromNama("bi"));
                ddp.setNo_loan(jsonNoLoanBI.getString("no_loan"));
                ddp.setTgl_kunjungan("");
                ddp.setStatus("0");
                Detil_Data_Pencairan queryddp = new Detil_Data_Pencairan();
                queryddp.insertDetilDataPencairan(ddp);
            }

            JSONArray jsonArrayBS = jsonPencairan.getJSONArray("bs");
            JSONArray jsonArrayBS1 = new JSONArray(jsonArrayBS.toString());
            System.out.println("Jsonarraybs: " + jsonArrayBS1);
            for (int i = 0; i < jsonArrayBS1.length(); i++) {
                String noloan = jsonArrayBS1.getString(i);
                JSONObject jsonNoLoanBS = new JSONObject(noloan);
                System.out.println("No_Loan bs: " + jsonNoLoanBS.getString("no_loan"));

                Detil_Data_Pencairan ddp = new Detil_Data_Pencairan();
                ddp.setId_data_pencairan(queryPencairan.getIdPencairanfromNama("bs"));
                ddp.setNo_loan(jsonNoLoanBS.getString("no_loan"));
                ddp.setTgl_kunjungan("");
                ddp.setStatus("0");
                Detil_Data_Pencairan queryddp = new Detil_Data_Pencairan();
                queryddp.insertDetilDataPencairan(ddp);
            }

            JSONArray jsonArrayBLBS = jsonPencairan.getJSONArray("blbs");
            JSONArray jsonArrayBLBS1 = new JSONArray(jsonArrayBLBS.toString());
            System.out.println("Jsonarrayblbs: " + jsonArrayBLBS1);
            for (int i = 0; i < jsonArrayBLBS1.length(); i++) {
                String noloan = jsonArrayBLBS1.getString(i);
                JSONObject jsonNoLoanBLBS = new JSONObject(noloan);
                System.out.println("No_Loan blbs: " + jsonNoLoanBLBS.getString("no_loan"));

                Detil_Data_Pencairan ddp = new Detil_Data_Pencairan();
                ddp.setId_data_pencairan(queryPencairan.getIdPencairanfromNama("blbs"));
                ddp.setNo_loan(jsonNoLoanBLBS.getString("no_loan"));
                ddp.setTgl_kunjungan("");
                ddp.setStatus("0");
                Detil_Data_Pencairan queryddp = new Detil_Data_Pencairan();
                queryddp.insertDetilDataPencairan(ddp);
            }

            //DATA PENAGIHAN
            JSONObject jsonPenagihan = new JSONObject(penagihan);
            JSONArray jsonArrayPenagihan = jsonPenagihan.getJSONArray("tgt");
            System.out.println("JsonArrayPenagihan: " + jsonArrayPenagihan);
            JSONArray jsonArrayPenagihan1 = new JSONArray(jsonArrayPenagihan.toString());
            System.out.println("JsonArrayPenagihan1: " + jsonArrayPenagihan1);
            TGT queryTGT = new TGT();
            Penagihan queryPenagihan = new Penagihan();
//            String noloangenerated = d.generateNoLoanSqlite();

            for (int i = 0; i < jsonArrayPenagihan1.length(); i++) {
//
                String dtlPenagihan = jsonArrayPenagihan1.getString(i);
//                System.out.println("NILAI: " + i + "-" + dtlPenagihan);

                JSONObject jsonDetilPenagihan = new JSONObject(dtlPenagihan);
                String cycleDetilPenagihan = jsonDetilPenagihan.getString("cycle");
                String detilInventoryPenagihan = jsonDetilPenagihan.getString("detail_inventory");
//                String jarakDetilPenagihan = jsonDetilPenagihan.getString("jarak");
                String osDetilPenagihan = jsonDetilPenagihan.getString("OS");
                String namaDetilPenagihan = jsonDetilPenagihan.getString("nama");
                String alamatDetilPenagihan = jsonDetilPenagihan.getString("alamat");

                TGT dataTGT = new TGT();
                String id_tgt_generate = queryTGT.generateIDTGT();
                System.out.println("id_tgt_generate: " + id_tgt_generate);

                dataTGT.setId_tgt(id_tgt_generate);
                dataTGT.setCycle(cycleDetilPenagihan);
//                dataTGT.setJarak(jarakDetilPenagihan);
                dataTGT.setOs(osDetilPenagihan);
                dataTGT.setNama(namaDetilPenagihan);
                dataTGT.setAlamat(alamatDetilPenagihan);
//                queryTGT.deleteAllTGT();
                queryTGT.insertIdTGT(dataTGT);


//                System.out.println("cycleDetilPenagihan: "+cycleDetilPenagihan);
//                System.out.println("detilInventoryPenagihan: "+detilInventoryPenagihan);
//                System.out.println("jarakDetilPenagihan: "+jarakDetilPenagihan);
//                System.out.println("osDetilPenagihan: "+osDetilPenagihan);
//                System.out.println("namaDetilPenagihan: "+namaDetilPenagihan);
//                System.out.println("alamatDetilPenagihan: "+alamatDetilPenagihan);


                JSONArray jsonArrDetInvPenagihan = new JSONArray(detilInventoryPenagihan.toString());
                for (int j = 0; j < jsonArrDetInvPenagihan.length(); j++) {
                    String isiDetInvPenagihan = jsonArrDetInvPenagihan.getString(j);
                    JSONObject jsonIsiDetInvPenagihan = new JSONObject(isiDetInvPenagihan);

                    //System.out.println("Saldo Min PenagihanLama : " + j + "-" + jsonIsiDetInvPenagihan.getString("saldo_min"));
                    String saldoMin = jsonIsiDetInvPenagihan.getString("saldo_min");
                    //MASUKIN KE DETAIL INVENTORY DAN KE DETAIL BUCKET DARI NILAI DATA PENAGIHAN


                    Detail_Inventory dd = new Detail_Inventory();
                    dd.setSaldo_min(jsonIsiDetInvPenagihan.getString(Constant.SALDO_MIN));
                    dd.setDpd_today(jsonIsiDetInvPenagihan.getString(Constant.DPD_TODAY));
                    dd.setResume_nsbh(jsonIsiDetInvPenagihan.getString(Constant.RESUME_NSBH));
                    dd.setAlmt_rumah(jsonIsiDetInvPenagihan.getString(Constant.ALMT_RUMAH));
                    dd.setTgl_janji_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.TGL_JANJI_BAYAR_TERAKHIR));
                    dd.setDenda(jsonIsiDetInvPenagihan.getString(Constant.DENDA));
                    dd.setPola_bayar(jsonIsiDetInvPenagihan.getString(Constant.POLA_BAYAR));

                    dd.setTgl_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.TGL_BAYAR_TERAKHIR));

                    dd.setPokok(jsonIsiDetInvPenagihan.getString(Constant.POKOK));
                    dd.setTgl_sp(jsonIsiDetInvPenagihan.getString(Constant.TGL_SP));
                    dd.setTenor(jsonIsiDetInvPenagihan.getString(Constant.TENOR));
                    dd.setAngsuran(jsonIsiDetInvPenagihan.getString(Constant.ANGSURAN));

                    // dd.setTgl_ptp(jsonIsiDetInvPenagihan.getString(Constant.TGL_PTP));
                    dd.setAction_plan(jsonIsiDetInvPenagihan.getString(Constant.ACTION_PLAN));
                    dd.setNo_rekening(jsonIsiDetInvPenagihan.getString(Constant.NO_REKENING));
                    dd.setHistori_sp(jsonIsiDetInvPenagihan.getString(Constant.HISTORI_SP));
                    dd.setNominal_janji_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR));
                    dd.setTlpn_rekomendator(jsonIsiDetInvPenagihan.getString(Constant.TLPN_REKOMENDATOR));

                    dd.setSumber_bayar(jsonIsiDetInvPenagihan.getString(Constant.SUMBER_BAYAR));
                    dd.setBunga(jsonIsiDetInvPenagihan.getString(Constant.BUNGA));
//
                    dd.setJns_pinjaman(jsonIsiDetInvPenagihan.getString(Constant.JNS_PINJAMAN));
                    dd.setNominal_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.NOMINAL_BAYAR_TERAKHIR));
//
                    //  dd.setNominal_ptp(jsonIsiDetInvPenagihan.getString(Constant.NOMINAL_PTP));
                    dd.setSaldo(jsonIsiDetInvPenagihan.getString(Constant.SALDO));
//
                    dd.setOs_pinjaman(jsonIsiDetInvPenagihan.getString(Constant.OS_PINJAMAN));
//
                    dd.setKeberadaan_jaminan(jsonIsiDetInvPenagihan.getString(Constant.KEBERADAAN_JAMINAN));
                    dd.setEmail(jsonIsiDetInvPenagihan.getString(Constant.SUMBER_BAYAR));
//
                    dd.setTotal_kewajiban(jsonIsiDetInvPenagihan.getString(Constant.TOTAL_KEWAJIBAN));
//
                    dd.setNo_loan(jsonIsiDetInvPenagihan.getString(Constant.NO_LOAN));
                    //   dd.setNo_loan(noloangenerated);
                    dd.setNama_debitur(jsonIsiDetInvPenagihan.getString(Constant.NAMA_DEBITUR));
                    dd.setTgl_jth_tempo(jsonIsiDetInvPenagihan.getString(Constant.TGL_JTH_TEMPO));
                    dd.setNo_tlpn(jsonIsiDetInvPenagihan.getString(Constant.NO_TLPN));
//
                    dd.setId_tgt(id_tgt_generate);
                    dd.setStatus("0");


                    dd.setNo_hp(jsonIsiDetInvPenagihan.getString(Constant.NO_HP));
                    dd.setAngsuran_ke(jsonIsiDetInvPenagihan.getString(Constant.ANGSURAN_KE));
                    dd.setNama_upliner(jsonIsiDetInvPenagihan.getString(Constant.NAMA_UPLINER));
                    dd.setGender(jsonIsiDetInvPenagihan.getString(Constant.GENDER));
                    dd.setTlpn_upliner(jsonIsiDetInvPenagihan.getString(Constant.TLPN_UPLINER));
                    dd.setTlpn_econ(jsonIsiDetInvPenagihan.getString(Constant.TLPN_ECON));
                    dd.setHarus_bayar(jsonIsiDetInvPenagihan.getString(Constant.HARUS_BAYAR));
                    dd.setNama_rekomendator(jsonIsiDetInvPenagihan.getString(Constant.NAMA_REKOMENDATOR));
                    dd.setTlpn_mogen(jsonIsiDetInvPenagihan.getString(Constant.TLPN_MOGEN));
                    dd.setNama_econ(jsonIsiDetInvPenagihan.getString(Constant.NAMA_ECON));
                    dd.setKewajiban(jsonIsiDetInvPenagihan.getString(Constant.KEWAJIBAN));
                    dd.setNama_mogen(jsonIsiDetInvPenagihan.getString(Constant.NAMA_MOGEN));
                    dd.setTgl_gajian(jsonIsiDetInvPenagihan.getString(Constant.TGL_GAJIAN));
                    dd.setPekerjaan(jsonIsiDetInvPenagihan.getString(Constant.PEKERJAAN));
                    dd.setAlmt_usaha(jsonIsiDetInvPenagihan.getString(Constant.ALMT_USAHA));
                    dd.setBucket(jsonIsiDetInvPenagihan.getString("bucket"));
                    querydetinv.insertDetailInventoryIdTGT(dd);
//                    d.deleteAllDetailInventory();
                }
            }

            JSONArray jsonArraynpay = jsonPenagihan.getJSONArray("npay");
            JSONArray jsonArraynpay1 = new JSONArray(jsonArraynpay.toString());
            System.out.println("Jsonarraynpay: " + jsonArraynpay1);
            for (int i = 0; i < jsonArraynpay1.length(); i++) {
                String noloan = jsonArraynpay1.getString(i);
                JSONObject jsonNoLoanNpay = new JSONObject(noloan);

                Detil_TGT dtgt = new Detil_TGT();
                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("npay"));
                dtgt.setNo_loan(jsonNoLoanNpay.getString("no_loan"));
                dtgt.setTgl_kunjungan("");
                dtgt.setStatus("0");
                Detil_TGT querydtgt = new Detil_TGT();
                querydtgt.insertDetilTGT(dtgt);
                // querydtgt.deleteAllDetilTGT();
                System.out.println("No_Loan npay: " + jsonNoLoanNpay.getString("no_loan"));
            }

            JSONArray jsonArrayfpay = jsonPenagihan.getJSONArray("fpay");
            JSONArray jsonArrayfpay1 = new JSONArray(jsonArrayfpay.toString());
            System.out.println("Jsonarrayfpay: " + jsonArrayfpay1);
            for (int i = 0; i < jsonArrayfpay1.length(); i++) {
                String noloan = jsonArrayfpay1.getString(i);
                JSONObject jsonNoLoanFpay = new JSONObject(noloan);

                Detil_TGT dtgt = new Detil_TGT();
                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("fpay"));
                dtgt.setNo_loan(jsonNoLoanFpay.getString("no_loan"));
//                dtgt.setTgl_kunjungan("");
                dtgt.setStatus("0");
                Detil_TGT querydtgt = new Detil_TGT();
                querydtgt.insertDetilTGT(dtgt);
                // querydtgt.deleteAllDetilTGT();

                System.out.println("No_Loan fpay: " + jsonNoLoanFpay.getString("no_loan"));
            }

            JSONArray jsonArraynvst = jsonPenagihan.getJSONArray("nvst");
            JSONArray jsonArraynvst1 = new JSONArray(jsonArraynvst.toString());
            System.out.println("Jsonarraynvst: " + jsonArraynvst1);
            for (int i = 0; i < jsonArraynvst1.length(); i++) {
                String noloan = jsonArraynvst1.getString(i);
                JSONObject jsonNoLoannvst = new JSONObject(noloan);

                Detil_TGT dtgt = new Detil_TGT();
                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("nvst"));
                dtgt.setNo_loan(jsonNoLoannvst.getString("no_loan"));
                dtgt.setTgl_kunjungan("");
                dtgt.setStatus("0");
                Detil_TGT querydtgt = new Detil_TGT();
                querydtgt.insertDetilTGT(dtgt);
                // querydtgt.deleteAllDetilTGT();

                System.out.println("No_Loan nvst: " + jsonNoLoannvst.getString("no_loan"));
            }

            JSONArray jsonArrayppay = jsonPenagihan.getJSONArray("ppay");
            JSONArray jsonArrayppay1 = new JSONArray(jsonArrayppay.toString());
            System.out.println("Jsonarrayppay: " + jsonArrayppay1);
            for (int i = 0; i < jsonArrayppay1.length(); i++) {
                String noloan = jsonArrayppay1.getString(i);
                JSONObject jsonNoLoanPpay = new JSONObject(noloan);

                Detil_TGT dtgt = new Detil_TGT();
                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("ppay"));
                dtgt.setNo_loan(jsonNoLoanPpay.getString("no_loan"));
//                dtgt.setTgl_kunjungan(jsonNoLoanPpay.getString(Constant.TGL_KUNJUNGAN));
                dtgt.setTgl_kunjungan("");
                dtgt.setStatus("0");
                Detil_TGT querydtgt = new Detil_TGT();
                querydtgt.insertDetilTGT(dtgt);
                // querydtgt.deleteAllDetilTGT();

                System.out.println("No_Loan ppay: " + jsonNoLoanPpay.getString("no_loan"));
            }

        } catch (Exception e) {
//            Function.showAlert(mContext, e.getMessage());
            e.printStackTrace();
        }

    }

    public static void longLog(String str) {
        if (str.length() > 4000) {
            Log.d("ini contoh json", str.substring(0, 4000));
            //System.out.println("return jsonnya: "+str.substring(0, 4000));
            longLog(str.substring(4000));
        } else
            Log.d("ini contoh json", str);
        //System.out.println("return jsonnya: "+str);
    }


//    class executeLogin extends AsyncTask<String, JSONObject, String> {
//        Context context;
//
//        private executeLogin(Context mContext) {
//            this.context = mContext;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(mContext);
//            progressDialog.setMessage(Constant.LOADING);
//            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            JSONObject json = new JSONObject();
//            try {
//                //String nik = "C001234";
//                //String header = nik+":"+Function.getImei(getApplicationContext()) + ":" + Function.getWaktuSekarangMilis();
//
//                System.out.println("Header: " + Function.getHeader(mContext, nikShared));
//                System.out.println("Header base64: " + new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));
//                json.put(Constant.USERNAME, username);
//                json.put(Constant.PASSWORD, password);
//                json.put(Constant.WAKTU, Function.getWaktuSekarangMilis());
//                System.out.println(json.toString());
//                resultfromJson = jsonParser.HttpRequestPost(Constant.SERVICE_DATA_SYNC, json.toString(), Constant.TimeOutConnection, new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));
//                System.out.println("HASIL JSON: " + resultfromJson);
//                return resultfromJson;
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return resultfromJson;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            progressDialog.dismiss();
//            JSONObject jsonObject;
////                if (result.contains(Constant.CONNECTION_LOST)) {
////                    Function.showAlert(mContext, Constant.CONNECTION_LOST);
////                } else if (result.contains(Constant.CONNECTION_ERROR)) {
////                    Function.showAlert(mContext, Constant.CONNECTION_ERROR);
////                } else {
////                    try {
////                        jsonObject = new JSONObject(result);
////
////                        username = jsonObject.getString(Constant.JSON_USERNAME);
////                        password = jsonObject.getString(Constant.JSON_PASSWORD);
////                        waktu = jsonObject.getString(Constant.JSON_WAKTU);
////                        nik = jsonObject.getString(Constant.JSON_NIK);
////                        nama = jsonObject.getString(Constant.JSON_NAMA);
////
////                        Intent i = new Intent(mContext, MenuLogin.class);
////                        Bundle bundle = new Bundle();
////                        bundle.putString(Constant.JSON_USERNAME, username);
////                        bundle.putString(Constant.JSON_PASSWORD, password);
////                        bundle.putString(Constant.JSON_WAKTU, waktu);
////                        bundle.putString(Constant.JSON_NIK, nik);
////                        bundle.putString(Constant.JSON_NAMA, nama);
////                        i.putExtras(bundle);
////                        startActivity(i);
////
////                    } catch (Exception e) {
////                        e.printStackTrace();
////                    }
////                }
//        }
//    }


    //                try {
////                    mm = new MySQLiteHelper(getApplicationContext(), Constant.DB_NAME);
////                    mm.initApp(getApplicationContext());
//                    MySQLiteHelper.initApp(getApplicationContext());
//                    MySQLiteHelper.initDB(getApplicationContext());
//
//                    //d.insertDetailInventory();
////                    System.out.println("Hasil: "+d.getDataDetailInventory().getId_detail_inventory());
//                    // d.deleteAllDetailInventory();
////d.insertDetailInventory();
//
//
////                    b.insertPenagihan(namaBucket);
////                    System.out.println(b.getDataPenagihan().getId_bucket());
////                    System.out.println(b.getDataPenagihan().getNama_bucket());
//
//
//                    namaPenagihan=new ArrayList<Penagihan>();
//                    listisiPenagihan=new ArrayList<IsiPenagihan>();
//                    listConfig=new ArrayList<Config>();
//
//                    JSONObject jo = new JSONObject(Constant.contohJson);
//                    String keterangan = jo.getString("keterangan");
//                    String waktu = jo.getString("waktu");
//                    String rc = jo.getString("rc");
//
//                    System.out.println("Ket: " + keterangan + " " + waktu + " " + rc);
//
//
//                    cc = new Config();
//                    cc.setNama_config("KETERANGAN");
//                    cc.setValue(keterangan);
//                    listConfig.add(cc);
//                    cc = new Config();
//                    cc.setNama_config("WAKTU");
//                    cc.setValue(waktu);
//                    listConfig.add(cc);
//                    cc = new Config();
//                    cc.setNama_config("RC");
//                    cc.setValue(rc);
//                    listConfig.add(cc);
//                    cc = new Config();
//                    cc.setNama_config("Versi App");
//                    cc.setValue(getVersion());
//                    listConfig.add(cc);
//                    con.insertDataConfig(listConfig);
//                    JSONArray ja = jo.getJSONArray("penagihan");
//                    System.out.println(ja.toString());
//                    JSONObject jo2 = new JSONObject();
//
//                    JSONArray ja1 = new JSONArray(ja.toString());
//                    //INI BUAT JSON ARRAY YANG ISINYA CURRENT,1-30,dll
//
//
//                    IsiPenagihan ip;
//                    for (int l = 0; l < ja1.length(); l++) {
//                        JSONArray ja2 = ja1.getJSONArray(l);
//
//                        String strings[] = new String[ja2.length()];
//                        strings[0] = ja2.getString(1);
//                        strings[1] = ja2.getString(0);
//                        String isi = strings[1];
//                        String title = strings[0];
//
//
//                        ip = new IsiPenagihan();
//                        ip.setNama_penagihan(title);
//                        ip.setJson_penagihan(isi);
//                        listisiPenagihan.add(ip);
//
//
//                        System.out.println("Ini Object dari String Title: " + strings[1]);
//                        System.out.println("STRING: " + title);
//
//
//                        p = new Penagihan();
//                        p.setNama_penagihan(title);
//                        namaPenagihan.add(l, p);
//
//                        System.out.println("JA2 " + ja2.toString());
//                        JSONObject jo1 = ja2.getJSONObject(0);
//                        jo2 = new JSONObject(jo1.toString());
//                        System.out.println("Jo2 " + jo2.toString());
//
//                    }
//
//                    //INSERT PENAGIHAN current,1-30, dll
//                    p1.insertPenagihan(namaPenagihan);
//                    d.deleteAllDetailInventory();
//
//                    ddb.deleteAllDetailBucket();
//
//                    //npay
//                    for (IsiPenagihan ipe : listisiPenagihan) {
//                        System.out.println("ISI PenagihanLama: " + ipe.getNama_penagihan());
//                        System.out.println("ISI PenagihanLama 1: " + ipe.getJson_penagihan());
//                        JSONObject jonpay = new JSONObject(ipe.getJson_penagihan());
//
//                        String npay = jonpay.getString("npay");
//                        JSONArray janpay = new JSONArray(npay);
//
//                        for (int j = 0; j < janpay.length(); j++) {
//                            String jo3 = janpay.getString(j);
//                            //System.out.println("Nilai jo3: " + jo3);
//                            JSONObject jo4 = new JSONObject(jo3);
//                            System.out.println("Panjang: " + janpay.length());
//                            String cycle = jo4.getString("cycle");
//                            String jarak = jo4.getString("jarak");
//                            String OS = jo4.getString("OS");
//                            String nama = jo4.getString("nama");
//                            String alamat = jo4.getString("alamat");
//                            String total_bayar = jo4.getString("total_bayar");
////                            System.out.println("Cycle: " + cycle);
////                            System.out.println("nama: " + nama);
////                            System.out.println("alamat: " + alamat);
////                            System.out.println("jarak: " + jarak);
////                            System.out.println("OS: " + OS);
////                            System.out.println("total_bayar: " + total_bayar);
//                            String detail_inventory = jo4.getString("detail_inventory");
//
//                            // System.out.println("Detail Inventory: " + detail_inventory);
//                            JSONObject jo5 = new JSONObject(detail_inventory);
//                            //System.out.println("JO5: " + jo5);
//                            Detail_Inventory dd = new Detail_Inventory();
//                            dd.setSaldo_min(jo5.getString(Constant.SALDO_MIN));
//                            dd.setDpd_today(jo5.getString(Constant.DPD_TODAY));
//                            dd.setResume_nsbh(jo5.getString(Constant.RESUME_NSBH));
//                            dd.setAlmt_rumah(jo5.getString(Constant.ALMT_RUMAH));
//                            dd.setTgl_janji_bayar_terakhir(jo5.getString(Constant.TGL_JANJI_BAYAR_TERAKHIR));
//                            dd.setDenda(jo5.getString(Constant.DENDA));
//                            dd.setPola_bayar(jo5.getString(Constant.POLA_BAYAR));
//                            dd.setNo_hp(jo5.getString(Constant.NO_HP));
//                            dd.setTgl_bayar_terakhir(jo5.getString(Constant.TGL_BAYAR_TERAKHIR));
//                            dd.setAngsuran_ke(jo5.getString(Constant.ANGSURAN_KE));
//                            dd.setNama_upliner(jo5.getString(Constant.NAMA_UPLINER));
//                            dd.setPokok(jo5.getString(Constant.POKOK));
//                            dd.setTgl_sp(jo5.getString(Constant.TGL_SP));
//                            dd.setTenor(jo5.getString(Constant.TENOR));
//                            dd.setAngsuran(jo5.getString(Constant.ANGSURAN));
//                            dd.setGender(jo5.getString(Constant.GENDER));
//                            dd.setTgl_ptp(jo5.getString(Constant.TGL_PTP));
//                            dd.setAction_plan(jo5.getString(Constant.ACTION_PLAN));
//                            dd.setNo_rekening(jo5.getString(Constant.NO_REKENING));
//                            dd.setHistori_sp(jo5.getString(Constant.HISTORI_SP));
//                            dd.setNominal_janji_bayar_terakhir(jo5.getString(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR));
//                            dd.setTlpn_rekomendator(jo5.getString(Constant.TLPN_REKOMENDATOR));
//                            dd.setTlpn_upliner(jo5.getString(Constant.TLPN_UPLINER));
//                            dd.setSumber_bayar(jo5.getString(Constant.SUMBER_BAYAR));
//                            dd.setBunga(jo5.getString(Constant.BUNGA));
//                            dd.setTlpn_econ(jo5.getString(Constant.TLPN_ECON));
//                            dd.setJns_pinjaman(jo5.getString(Constant.JNS_PINJAMAN));
//                            dd.setNominal_bayar_terakhir(jo5.getString(Constant.NOMINAL_BAYAR_TERAKHIR));
//                            dd.setHarus_bayar(jo5.getString(Constant.HARUS_BAYAR));
//                            dd.setNominal_ptp(jo5.getString(Constant.NOMINAL_PTP));
//                            dd.setSaldo(jo5.getString(Constant.SALDO));
//                            dd.setNama_rekomendator(jo5.getString(Constant.NAMA_REKOMENDATOR));
//                            dd.setTlpn_mogen(jo5.getString(Constant.TLPN_MOGEN));
//                            dd.setOs_pinjaman(jo5.getString(Constant.OS_PINJAMAN));
//                            dd.setNama_econ(jo5.getString(Constant.NAMA_ECON));
//                            dd.setKeberadaan_jaminan(jo5.getString(Constant.KEBERADAAN_JAMINAN));
//                            dd.setEmail(jo5.getString(Constant.SUMBER_BAYAR));
//                            dd.setKewajiban(jo5.getString(Constant.KEWAJIBAN));
//                            dd.setTotal_kewajiban(jo5.getString(Constant.TOTAL_KEWAJIBAN));
//                            dd.setNama_mogen(jo5.getString(Constant.NAMA_MOGEN));
//                            dd.setNo_loan(jo5.getString(Constant.NO_LOAN));
//                            dd.setNama_debitur(jo5.getString(Constant.NAMA_DEBITUR));
//                            dd.setTgl_jth_tempo(jo5.getString(Constant.TGL_JTH_TEMPO));
//                            dd.setNo_tlpn(jo5.getString(Constant.NO_TLPN));
//                            dd.setTgl_gajian(jo5.getString(Constant.TGL_GAJIAN));
//                            dd.setPekerjaan(jo5.getString(Constant.PEKERJAAN));
//                            dd.setAlmt_usaha(jo5.getString(Constant.ALMT_USAHA));
//
//                            d.insertDetailInventory(dd, janpay.length());
//
//                            String id_detail_inventory = d.getIdValueDetailInventory().getId_detail_inventory();
//                            System.out.println("ID Detail Inventory: " + id_detail_inventory);
//                            String id_bucket = b.getIdBucketfromName("npay");
//                            System.out.println("ID Bucket: " + id_bucket);
//                            Detail_Bucket db = new Detail_Bucket();
//                            db.setId_bucket(id_bucket);
//                            db.setId_detail_inventory(id_detail_inventory);
//                            db.setNama_penagihan(ipe.getNama_penagihan());
//                            db.setCycle(cycle);
//                            db.setJarak(jarak);
//                            db.setOs(OS);
//                            db.setNama(nama);
//                            db.setAlamat(alamat);
//                            db.setTotal_bayar(total_bayar);
//                            db.setJumlah_bucket(String.valueOf(janpay.length()));
//
//                            db.insertDetailBucket(db);
//                        }
//                    }
//
//                    //fpay
//                    for (IsiPenagihan ipe : listisiPenagihan) {
//                        System.out.println("ISI PenagihanLama: " + ipe.getNama_penagihan());
//                        System.out.println("ISI PenagihanLama 1: " + ipe.getJson_penagihan());
//                        JSONObject jonpay = new JSONObject(ipe.getJson_penagihan());
//
//                        String fpay = jonpay.getString("fpay");
//                        JSONArray jafpay = new JSONArray(fpay);
//
//                        for (int j = 0; j < jafpay.length(); j++) {
//                            String jo3 = jafpay.getString(j);
//                            JSONObject jo4 = new JSONObject(jo3);
//                            System.out.println("Panjang: " + jafpay.length());
//                            String cycle = jo4.getString("cycle");
//                            String jarak = jo4.getString("jarak");
//                            String OS = jo4.getString("OS");
//                            String nama = jo4.getString("nama");
//                            String alamat = jo4.getString("alamat");
//                            String total_bayar = jo4.getString("total_bayar");
//                            String detail_inventory = jo4.getString("detail_inventory");
//
//                            JSONObject jo5 = new JSONObject(detail_inventory);
//
//                            Detail_Inventory dd = new Detail_Inventory();
//                            dd.setSaldo_min(jo5.getString(Constant.SALDO_MIN));
//                            dd.setDpd_today(jo5.getString(Constant.DPD_TODAY));
//                            dd.setResume_nsbh(jo5.getString(Constant.RESUME_NSBH));
//                            dd.setAlmt_rumah(jo5.getString(Constant.ALMT_RUMAH));
//                            dd.setTgl_janji_bayar_terakhir(jo5.getString(Constant.TGL_JANJI_BAYAR_TERAKHIR));
//                            dd.setDenda(jo5.getString(Constant.DENDA));
//                            dd.setPola_bayar(jo5.getString(Constant.POLA_BAYAR));
//                            dd.setNo_hp(jo5.getString(Constant.NO_HP));
//                            dd.setTgl_bayar_terakhir(jo5.getString(Constant.TGL_BAYAR_TERAKHIR));
//                            dd.setAngsuran_ke(jo5.getString(Constant.ANGSURAN_KE));
//                            dd.setNama_upliner(jo5.getString(Constant.NAMA_UPLINER));
//                            dd.setPokok(jo5.getString(Constant.POKOK));
//                            dd.setTgl_sp(jo5.getString(Constant.TGL_SP));
//                            dd.setTenor(jo5.getString(Constant.TENOR));
//                            dd.setAngsuran(jo5.getString(Constant.ANGSURAN));
//                            dd.setGender(jo5.getString(Constant.GENDER));
//                            dd.setTgl_ptp(jo5.getString(Constant.TGL_PTP));
//                            dd.setAction_plan(jo5.getString(Constant.ACTION_PLAN));
//                            dd.setNo_rekening(jo5.getString(Constant.NO_REKENING));
//                            dd.setHistori_sp(jo5.getString(Constant.HISTORI_SP));
//                            dd.setNominal_janji_bayar_terakhir(jo5.getString(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR));
//                            dd.setTlpn_rekomendator(jo5.getString(Constant.TLPN_REKOMENDATOR));
//                            dd.setTlpn_upliner(jo5.getString(Constant.TLPN_UPLINER));
//                            dd.setSumber_bayar(jo5.getString(Constant.SUMBER_BAYAR));
//                            dd.setBunga(jo5.getString(Constant.BUNGA));
//                            dd.setTlpn_econ(jo5.getString(Constant.TLPN_ECON));
//                            dd.setJns_pinjaman(jo5.getString(Constant.JNS_PINJAMAN));
//                            dd.setNominal_bayar_terakhir(jo5.getString(Constant.NOMINAL_BAYAR_TERAKHIR));
//                            dd.setHarus_bayar(jo5.getString(Constant.HARUS_BAYAR));
//                            dd.setNominal_ptp(jo5.getString(Constant.NOMINAL_PTP));
//                            dd.setSaldo(jo5.getString(Constant.SALDO));
//                            dd.setNama_rekomendator(jo5.getString(Constant.NAMA_REKOMENDATOR));
//                            dd.setTlpn_mogen(jo5.getString(Constant.TLPN_MOGEN));
//                            dd.setOs_pinjaman(jo5.getString(Constant.OS_PINJAMAN));
//                            dd.setNama_econ(jo5.getString(Constant.NAMA_ECON));
//                            dd.setKeberadaan_jaminan(jo5.getString(Constant.KEBERADAAN_JAMINAN));
//                            dd.setEmail(jo5.getString(Constant.SUMBER_BAYAR));
//                            dd.setKewajiban(jo5.getString(Constant.KEWAJIBAN));
//                            dd.setTotal_kewajiban(jo5.getString(Constant.TOTAL_KEWAJIBAN));
//                            dd.setNama_mogen(jo5.getString(Constant.NAMA_MOGEN));
//                            dd.setNo_loan(jo5.getString(Constant.NO_LOAN));
//                            dd.setNama_debitur(jo5.getString(Constant.NAMA_DEBITUR));
//                            dd.setTgl_jth_tempo(jo5.getString(Constant.TGL_JTH_TEMPO));
//                            dd.setNo_tlpn(jo5.getString(Constant.NO_TLPN));
//                            dd.setTgl_gajian(jo5.getString(Constant.TGL_GAJIAN));
//                            dd.setPekerjaan(jo5.getString(Constant.PEKERJAAN));
//                            dd.setAlmt_usaha(jo5.getString(Constant.ALMT_USAHA));
//
//                            d.insertDetailInventory(dd, jafpay.length());
//
//                            String id_detail_inventory = d.getIdValueDetailInventory().getId_detail_inventory();
//                            System.out.println("ID Detail Inventory: " + id_detail_inventory);
//                            String id_bucket = b.getIdBucketfromName("fpay");
//                            System.out.println("ID Bucket: " + id_bucket);
//                            Detail_Bucket db = new Detail_Bucket();
//                            db.setId_bucket(id_bucket);
//                            db.setId_detail_inventory(id_detail_inventory);
//                            db.setNama_penagihan(ipe.getNama_penagihan());
//                            db.setCycle(cycle);
//                            db.setJarak(jarak);
//                            db.setOs(OS);
//                            db.setNama(nama);
//                            db.setAlamat(alamat);
//                            db.setTotal_bayar(total_bayar);
//                            db.setJumlah_bucket(String.valueOf(jafpay.length()));
//
//                            db.insertDetailBucket(db);
//                        }
//                    }
//
//                    //tgt
//                    for (IsiPenagihan ipe : listisiPenagihan) {
//                        System.out.println("ISI PenagihanLama: " + ipe.getNama_penagihan());
//                        System.out.println("ISI PenagihanLama 1: " + ipe.getJson_penagihan());
//                        JSONObject jonpay = new JSONObject(ipe.getJson_penagihan());
//
//                        String fpay = jonpay.getString("tgt");
//                        JSONArray jafpay = new JSONArray(fpay);
//
//                        for (int j = 0; j < jafpay.length(); j++) {
//                            String jo3 = jafpay.getString(j);
//                            JSONObject jo4 = new JSONObject(jo3);
//                            System.out.println("Panjang: " + jafpay.length());
//                            String cycle = jo4.getString("cycle");
//                            String jarak = jo4.getString("jarak");
//                            String OS = jo4.getString("OS");
//                            String nama = jo4.getString("nama");
//                            String alamat = jo4.getString("alamat");
//                            String total_bayar = jo4.getString("total_bayar");
//                            String detail_inventory = jo4.getString("detail_inventory");
//
//                            JSONObject jo5 = new JSONObject(detail_inventory);
//
//                            Detail_Inventory dd = new Detail_Inventory();
//                            dd.setSaldo_min(jo5.getString(Constant.SALDO_MIN));
//                            dd.setDpd_today(jo5.getString(Constant.DPD_TODAY));
//                            dd.setResume_nsbh(jo5.getString(Constant.RESUME_NSBH));
//                            dd.setAlmt_rumah(jo5.getString(Constant.ALMT_RUMAH));
//                            dd.setTgl_janji_bayar_terakhir(jo5.getString(Constant.TGL_JANJI_BAYAR_TERAKHIR));
//                            dd.setDenda(jo5.getString(Constant.DENDA));
//                            dd.setPola_bayar(jo5.getString(Constant.POLA_BAYAR));
//                            dd.setNo_hp(jo5.getString(Constant.NO_HP));
//                            dd.setTgl_bayar_terakhir(jo5.getString(Constant.TGL_BAYAR_TERAKHIR));
//                            dd.setAngsuran_ke(jo5.getString(Constant.ANGSURAN_KE));
//                            dd.setNama_upliner(jo5.getString(Constant.NAMA_UPLINER));
//                            dd.setPokok(jo5.getString(Constant.POKOK));
//                            dd.setTgl_sp(jo5.getString(Constant.TGL_SP));
//                            dd.setTenor(jo5.getString(Constant.TENOR));
//                            dd.setAngsuran(jo5.getString(Constant.ANGSURAN));
//                            dd.setGender(jo5.getString(Constant.GENDER));
//                            dd.setTgl_ptp(jo5.getString(Constant.TGL_PTP));
//                            dd.setAction_plan(jo5.getString(Constant.ACTION_PLAN));
//                            dd.setNo_rekening(jo5.getString(Constant.NO_REKENING));
//                            dd.setHistori_sp(jo5.getString(Constant.HISTORI_SP));
//                            dd.setNominal_janji_bayar_terakhir(jo5.getString(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR));
//                            dd.setTlpn_rekomendator(jo5.getString(Constant.TLPN_REKOMENDATOR));
//                            dd.setTlpn_upliner(jo5.getString(Constant.TLPN_UPLINER));
//                            dd.setSumber_bayar(jo5.getString(Constant.SUMBER_BAYAR));
//                            dd.setBunga(jo5.getString(Constant.BUNGA));
//                            dd.setTlpn_econ(jo5.getString(Constant.TLPN_ECON));
//                            dd.setJns_pinjaman(jo5.getString(Constant.JNS_PINJAMAN));
//                            dd.setNominal_bayar_terakhir(jo5.getString(Constant.NOMINAL_BAYAR_TERAKHIR));
//                            dd.setHarus_bayar(jo5.getString(Constant.HARUS_BAYAR));
//                            dd.setNominal_ptp(jo5.getString(Constant.NOMINAL_PTP));
//                            dd.setSaldo(jo5.getString(Constant.SALDO));
//                            dd.setNama_rekomendator(jo5.getString(Constant.NAMA_REKOMENDATOR));
//                            dd.setTlpn_mogen(jo5.getString(Constant.TLPN_MOGEN));
//                            dd.setOs_pinjaman(jo5.getString(Constant.OS_PINJAMAN));
//                            dd.setNama_econ(jo5.getString(Constant.NAMA_ECON));
//                            dd.setKeberadaan_jaminan(jo5.getString(Constant.KEBERADAAN_JAMINAN));
//                            dd.setEmail(jo5.getString(Constant.SUMBER_BAYAR));
//                            dd.setKewajiban(jo5.getString(Constant.KEWAJIBAN));
//                            dd.setTotal_kewajiban(jo5.getString(Constant.TOTAL_KEWAJIBAN));
//                            dd.setNama_mogen(jo5.getString(Constant.NAMA_MOGEN));
//                            dd.setNo_loan(jo5.getString(Constant.NO_LOAN));
//                            dd.setNama_debitur(jo5.getString(Constant.NAMA_DEBITUR));
//                            dd.setTgl_jth_tempo(jo5.getString(Constant.TGL_JTH_TEMPO));
//                            dd.setNo_tlpn(jo5.getString(Constant.NO_TLPN));
//                            dd.setTgl_gajian(jo5.getString(Constant.TGL_GAJIAN));
//                            dd.setPekerjaan(jo5.getString(Constant.PEKERJAAN));
//                            dd.setAlmt_usaha(jo5.getString(Constant.ALMT_USAHA));
//
//                            d.insertDetailInventory(dd, jafpay.length());
//
//                            String id_detail_inventory = d.getIdValueDetailInventory().getId_detail_inventory();
//                            System.out.println("ID Detail Inventory: " + id_detail_inventory);
//                            String id_bucket = b.getIdBucketfromName("tgt");
//                            System.out.println("ID Bucket: " + id_bucket);
//                            Detail_Bucket db = new Detail_Bucket();
//                            db.setId_bucket(id_bucket);
//                            db.setId_detail_inventory(id_detail_inventory);
//                            db.setNama_penagihan(ipe.getNama_penagihan());
//                            db.setCycle(cycle);
//                            db.setJarak(jarak);
//                            db.setOs(OS);
//                            db.setNama(nama);
//                            db.setAlamat(alamat);
//                            db.setTotal_bayar(total_bayar);
//                            db.setJumlah_bucket(String.valueOf(jafpay.length()));
//
//                            db.insertDetailBucket(db);
//                        }
//                    }
//
//                    //nvst
//                    for (IsiPenagihan ipe : listisiPenagihan) {
//                        System.out.println("ISI PenagihanLama: " + ipe.getNama_penagihan());
//                        System.out.println("ISI PenagihanLama 1: " + ipe.getJson_penagihan());
//                        JSONObject jonpay = new JSONObject(ipe.getJson_penagihan());
//
//                        String fpay = jonpay.getString("nvst");
//                        JSONArray jafpay = new JSONArray(fpay);
//
//                        for (int j = 0; j < jafpay.length(); j++) {
//                            String jo3 = jafpay.getString(j);
//                            JSONObject jo4 = new JSONObject(jo3);
//                            System.out.println("Panjang: " + jafpay.length());
//                            String cycle = jo4.getString("cycle");
//                            String jarak = jo4.getString("jarak");
//                            String OS = jo4.getString("OS");
//                            String nama = jo4.getString("nama");
//                            String alamat = jo4.getString("alamat");
//                            String total_bayar = jo4.getString("total_bayar");
//                            String detail_inventory = jo4.getString("detail_inventory");
//
//                            JSONObject jo5 = new JSONObject(detail_inventory);
//
//                            Detail_Inventory dd = new Detail_Inventory();
//                            dd.setSaldo_min(jo5.getString(Constant.SALDO_MIN));
//                            dd.setDpd_today(jo5.getString(Constant.DPD_TODAY));
//                            dd.setResume_nsbh(jo5.getString(Constant.RESUME_NSBH));
//                            dd.setAlmt_rumah(jo5.getString(Constant.ALMT_RUMAH));
//                            dd.setTgl_janji_bayar_terakhir(jo5.getString(Constant.TGL_JANJI_BAYAR_TERAKHIR));
//                            dd.setDenda(jo5.getString(Constant.DENDA));
//                            dd.setPola_bayar(jo5.getString(Constant.POLA_BAYAR));
//                            dd.setNo_hp(jo5.getString(Constant.NO_HP));
//                            dd.setTgl_bayar_terakhir(jo5.getString(Constant.TGL_BAYAR_TERAKHIR));
//                            dd.setAngsuran_ke(jo5.getString(Constant.ANGSURAN_KE));
//                            dd.setNama_upliner(jo5.getString(Constant.NAMA_UPLINER));
//                            dd.setPokok(jo5.getString(Constant.POKOK));
//                            dd.setTgl_sp(jo5.getString(Constant.TGL_SP));
//                            dd.setTenor(jo5.getString(Constant.TENOR));
//                            dd.setAngsuran(jo5.getString(Constant.ANGSURAN));
//                            dd.setGender(jo5.getString(Constant.GENDER));
//                            dd.setTgl_ptp(jo5.getString(Constant.TGL_PTP));
//                            dd.setAction_plan(jo5.getString(Constant.ACTION_PLAN));
//                            dd.setNo_rekening(jo5.getString(Constant.NO_REKENING));
//                            dd.setHistori_sp(jo5.getString(Constant.HISTORI_SP));
//                            dd.setNominal_janji_bayar_terakhir(jo5.getString(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR));
//                            dd.setTlpn_rekomendator(jo5.getString(Constant.TLPN_REKOMENDATOR));
//                            dd.setTlpn_upliner(jo5.getString(Constant.TLPN_UPLINER));
//                            dd.setSumber_bayar(jo5.getString(Constant.SUMBER_BAYAR));
//                            dd.setBunga(jo5.getString(Constant.BUNGA));
//                            dd.setTlpn_econ(jo5.getString(Constant.TLPN_ECON));
//                            dd.setJns_pinjaman(jo5.getString(Constant.JNS_PINJAMAN));
//                            dd.setNominal_bayar_terakhir(jo5.getString(Constant.NOMINAL_BAYAR_TERAKHIR));
//                            dd.setHarus_bayar(jo5.getString(Constant.HARUS_BAYAR));
//                            dd.setNominal_ptp(jo5.getString(Constant.NOMINAL_PTP));
//                            dd.setSaldo(jo5.getString(Constant.SALDO));
//                            dd.setNama_rekomendator(jo5.getString(Constant.NAMA_REKOMENDATOR));
//                            dd.setTlpn_mogen(jo5.getString(Constant.TLPN_MOGEN));
//                            dd.setOs_pinjaman(jo5.getString(Constant.OS_PINJAMAN));
//                            dd.setNama_econ(jo5.getString(Constant.NAMA_ECON));
//                            dd.setKeberadaan_jaminan(jo5.getString(Constant.KEBERADAAN_JAMINAN));
//                            dd.setEmail(jo5.getString(Constant.SUMBER_BAYAR));
//                            dd.setKewajiban(jo5.getString(Constant.KEWAJIBAN));
//                            dd.setTotal_kewajiban(jo5.getString(Constant.TOTAL_KEWAJIBAN));
//                            dd.setNama_mogen(jo5.getString(Constant.NAMA_MOGEN));
//                            dd.setNo_loan(jo5.getString(Constant.NO_LOAN));
//                            dd.setNama_debitur(jo5.getString(Constant.NAMA_DEBITUR));
//                            dd.setTgl_jth_tempo(jo5.getString(Constant.TGL_JTH_TEMPO));
//                            dd.setNo_tlpn(jo5.getString(Constant.NO_TLPN));
//                            dd.setTgl_gajian(jo5.getString(Constant.TGL_GAJIAN));
//                            dd.setPekerjaan(jo5.getString(Constant.PEKERJAAN));
//                            dd.setAlmt_usaha(jo5.getString(Constant.ALMT_USAHA));
//
//                            d.insertDetailInventory(dd, jafpay.length());
//
//                            String id_detail_inventory = d.getIdValueDetailInventory().getId_detail_inventory();
//                            System.out.println("ID Detail Inventory: " + id_detail_inventory);
//                            String id_bucket = b.getIdBucketfromName("nvst");
//                            System.out.println("ID Bucket: " + id_bucket);
//                            Detail_Bucket db = new Detail_Bucket();
//                            db.setId_bucket(id_bucket);
//                            db.setId_detail_inventory(id_detail_inventory);
//                            db.setNama_penagihan(ipe.getNama_penagihan());
//                            db.setCycle(cycle);
//                            db.setJarak(jarak);
//                            db.setOs(OS);
//                            db.setNama(nama);
//                            db.setAlamat(alamat);
//                            db.setTotal_bayar(total_bayar);
//                            db.setJumlah_bucket(String.valueOf(jafpay.length()));
//
//                            db.insertDetailBucket(db);
//                        }
//                    }
//
//                    //ppay
//                    for (IsiPenagihan ipe : listisiPenagihan) {
//                        System.out.println("ISI PenagihanLama: " + ipe.getNama_penagihan());
//                        System.out.println("ISI PenagihanLama 1: " + ipe.getJson_penagihan());
//                        JSONObject jonpay = new JSONObject(ipe.getJson_penagihan());
//
//                        String fpay = jonpay.getString("ppay");
//                        JSONArray jafpay = new JSONArray(fpay);
//
//                        for (int j = 0; j < jafpay.length(); j++) {
//                            String jo3 = jafpay.getString(j);
//                            JSONObject jo4 = new JSONObject(jo3);
//                            System.out.println("Panjang: " + jafpay.length());
//                            String cycle = jo4.getString("cycle");
//                            String jarak = jo4.getString("jarak");
//                            String OS = jo4.getString("OS");
//                            String nama = jo4.getString("nama");
//                            String alamat = jo4.getString("alamat");
//                            String total_bayar = jo4.getString("total_bayar");
//                            String detail_inventory = jo4.getString("detail_inventory");
//
//                            JSONObject jo5 = new JSONObject(detail_inventory);
//
//                            Detail_Inventory dd = new Detail_Inventory();
//                            dd.setSaldo_min(jo5.getString(Constant.SALDO_MIN));
//                            dd.setDpd_today(jo5.getString(Constant.DPD_TODAY));
//                            dd.setResume_nsbh(jo5.getString(Constant.RESUME_NSBH));
//                            dd.setAlmt_rumah(jo5.getString(Constant.ALMT_RUMAH));
//                            dd.setTgl_janji_bayar_terakhir(jo5.getString(Constant.TGL_JANJI_BAYAR_TERAKHIR));
//                            dd.setDenda(jo5.getString(Constant.DENDA));
//                            dd.setPola_bayar(jo5.getString(Constant.POLA_BAYAR));
//                            dd.setNo_hp(jo5.getString(Constant.NO_HP));
//                            dd.setTgl_bayar_terakhir(jo5.getString(Constant.TGL_BAYAR_TERAKHIR));
//                            dd.setAngsuran_ke(jo5.getString(Constant.ANGSURAN_KE));
//                            dd.setNama_upliner(jo5.getString(Constant.NAMA_UPLINER));
//                            dd.setPokok(jo5.getString(Constant.POKOK));
//                            dd.setTgl_sp(jo5.getString(Constant.TGL_SP));
//                            dd.setTenor(jo5.getString(Constant.TENOR));
//                            dd.setAngsuran(jo5.getString(Constant.ANGSURAN));
//                            dd.setGender(jo5.getString(Constant.GENDER));
//                            dd.setTgl_ptp(jo5.getString(Constant.TGL_PTP));
//                            dd.setAction_plan(jo5.getString(Constant.ACTION_PLAN));
//                            dd.setNo_rekening(jo5.getString(Constant.NO_REKENING));
//                            dd.setHistori_sp(jo5.getString(Constant.HISTORI_SP));
//                            dd.setNominal_janji_bayar_terakhir(jo5.getString(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR));
//                            dd.setTlpn_rekomendator(jo5.getString(Constant.TLPN_REKOMENDATOR));
//                            dd.setTlpn_upliner(jo5.getString(Constant.TLPN_UPLINER));
//                            dd.setSumber_bayar(jo5.getString(Constant.SUMBER_BAYAR));
//                            dd.setBunga(jo5.getString(Constant.BUNGA));
//                            dd.setTlpn_econ(jo5.getString(Constant.TLPN_ECON));
//                            dd.setJns_pinjaman(jo5.getString(Constant.JNS_PINJAMAN));
//                            dd.setNominal_bayar_terakhir(jo5.getString(Constant.NOMINAL_BAYAR_TERAKHIR));
//                            dd.setHarus_bayar(jo5.getString(Constant.HARUS_BAYAR));
//                            dd.setNominal_ptp(jo5.getString(Constant.NOMINAL_PTP));
//                            dd.setSaldo(jo5.getString(Constant.SALDO));
//                            dd.setNama_rekomendator(jo5.getString(Constant.NAMA_REKOMENDATOR));
//                            dd.setTlpn_mogen(jo5.getString(Constant.TLPN_MOGEN));
//                            dd.setOs_pinjaman(jo5.getString(Constant.OS_PINJAMAN));
//                            dd.setNama_econ(jo5.getString(Constant.NAMA_ECON));
//                            dd.setKeberadaan_jaminan(jo5.getString(Constant.KEBERADAAN_JAMINAN));
//                            dd.setEmail(jo5.getString(Constant.SUMBER_BAYAR));
//                            dd.setKewajiban(jo5.getString(Constant.KEWAJIBAN));
//                            dd.setTotal_kewajiban(jo5.getString(Constant.TOTAL_KEWAJIBAN));
//                            dd.setNama_mogen(jo5.getString(Constant.NAMA_MOGEN));
//                            dd.setNo_loan(jo5.getString(Constant.NO_LOAN));
//                            dd.setNama_debitur(jo5.getString(Constant.NAMA_DEBITUR));
//                            dd.setTgl_jth_tempo(jo5.getString(Constant.TGL_JTH_TEMPO));
//                            dd.setNo_tlpn(jo5.getString(Constant.NO_TLPN));
//                            dd.setTgl_gajian(jo5.getString(Constant.TGL_GAJIAN));
//                            dd.setPekerjaan(jo5.getString(Constant.PEKERJAAN));
//                            dd.setAlmt_usaha(jo5.getString(Constant.ALMT_USAHA));
//
//                            d.insertDetailInventory(dd, jafpay.length());
//
//                            String id_detail_inventory = d.getIdValueDetailInventory().getId_detail_inventory();
//                            System.out.println("ID Detail Inventory: " + id_detail_inventory);
//                            String id_bucket = b.getIdBucketfromName("ppay");
//                            System.out.println("ID Bucket: " + id_bucket);
//                            Detail_Bucket db = new Detail_Bucket();
//                            db.setId_bucket(id_bucket);
//                            db.setId_detail_inventory(id_detail_inventory);
//                            db.setNama_penagihan(ipe.getNama_penagihan());
//                            db.setCycle(cycle);
//                            db.setJarak(jarak);
//                            db.setOs(OS);
//                            db.setNama(nama);
//                            db.setAlamat(alamat);
//                            db.setTotal_bayar(total_bayar);
//                            db.setJumlah_bucket(String.valueOf(jafpay.length()));
//
//                            db.insertDetailBucket(db);
//                        }
//                    }
//
//
//
//
//
//
//                    //  System.out.println("Check: "+p1.checkPenagihanEmpty());
//
////                    JSONObject isi = new JSONObject(listisiPenagihan.get(0).getJson_penagihan());
////                    System.out.println("Isi: " + isi.toString());
////                    //yang membedakan npay, fpay
////                    //INI NPAY
////                    String npay = jo2.getString("npay");
////                    System.out.println("npay: " + npay);
////
////                    JSONArray ja3 = new JSONArray(npay);
////                    System.out.println("JA3: " + ja3.length());
////
////                    for (int i = 0; i < ja3.length(); i++) {
////                        String jo3 = ja3.getString(i);
////                        JSONObject jo4 = new JSONObject(jo3);
////                        String cycle = jo4.getString("cycle");
////                        String jarak = jo4.getString("jarak");
////                        String OS = jo4.getString("OS");
////                        String nama = jo4.getString("nama");
////                        String alamat = jo4.getString("alamat");
////                        String total_bayar = jo4.getString("total_bayar");
////                        System.out.println("Cycle: " + cycle);
////                        System.out.println("nama: " + nama);
////                        System.out.println("alamat: " + alamat);
////                        System.out.println("jarak: " + jarak);
////                        System.out.println("OS: " + OS);
////                        System.out.println("total_bayar: " + total_bayar);
////                        String detail_inventory = jo4.getString("detail_inventory");
////
////                        System.out.println("Detail Inventory: " + detail_inventory);
////                        JSONObject jo5 = new JSONObject(detail_inventory);
////                        System.out.println("JO5: " + jo5);
////                        Detail_Inventory dd = new Detail_Inventory();
////                        dd.setSaldo_min(jo5.getString(Constant.SALDO_MIN));
////                        dd.setDpd_today(jo5.getString(Constant.DPD_TODAY));
////                        dd.setResume_nsbh(jo5.getString(Constant.RESUME_NSBH));
////                        dd.setAlmt_rumah(jo5.getString(Constant.ALMT_RUMAH));
////                        dd.setTgl_janji_bayar_terakhir(jo5.getString(Constant.TGL_JANJI_BAYAR_TERAKHIR));
////                        dd.setDenda(jo5.getString(Constant.DENDA));
////                        dd.setPola_bayar(jo5.getString(Constant.POLA_BAYAR));
////                        dd.setNo_hp(jo5.getString(Constant.NO_HP));
////                        dd.setTgl_bayar_terakhir(jo5.getString(Constant.TGL_BAYAR_TERAKHIR));
////                        dd.setAngsuran_ke(jo5.getString(Constant.ANGSURAN_KE));
////                        dd.setNama_upliner(jo5.getString(Constant.NAMA_UPLINER));
////                        dd.setPokok(jo5.getString(Constant.POKOK));
////                        dd.setTgl_sp(jo5.getString(Constant.TGL_SP));
////                        dd.setTenor(jo5.getString(Constant.TENOR));
////                        dd.setAngsuran(jo5.getString(Constant.ANGSURAN));
////                        dd.setGender(jo5.getString(Constant.GENDER));
////                        dd.setTgl_ptp(jo5.getString(Constant.TGL_PTP));
////                        dd.setAction_plan(jo5.getString(Constant.ACTION_PLAN));
////                        dd.setNo_rekening(jo5.getString(Constant.NO_REKENING));
////                        dd.setHistori_sp(jo5.getString(Constant.HISTORI_SP));
////                        dd.setNominal_janji_bayar_terakhir(jo5.getString(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR));
////                        dd.setTlpn_rekomendator(jo5.getString(Constant.TLPN_REKOMENDATOR));
////                        dd.setTlpn_upliner(jo5.getString(Constant.TLPN_UPLINER));
////                        dd.setSumber_bayar(jo5.getString(Constant.SUMBER_BAYAR));
////                        dd.setBunga(jo5.getString(Constant.BUNGA));
////                        dd.setTlpn_econ(jo5.getString(Constant.TLPN_ECON));
////                        dd.setJns_pinjaman(jo5.getString(Constant.JNS_PINJAMAN));
////                        dd.setNominal_bayar_terakhir(jo5.getString(Constant.NOMINAL_BAYAR_TERAKHIR));
////                        dd.setHarus_bayar(jo5.getString(Constant.HARUS_BAYAR));
////                        dd.setNominal_ptp(jo5.getString(Constant.NOMINAL_PTP));
////                        dd.setSaldo(jo5.getString(Constant.SALDO));
////                        dd.setNama_rekomendator(jo5.getString(Constant.NAMA_REKOMENDATOR));
////                        dd.setTlpn_mogen(jo5.getString(Constant.TLPN_MOGEN));
////                        dd.setOs_pinjaman(jo5.getString(Constant.OS_PINJAMAN));
////                        dd.setNama_econ(jo5.getString(Constant.NAMA_ECON));
////                        dd.setKeberadaan_jaminan(jo5.getString(Constant.KEBERADAAN_JAMINAN));
////                        dd.setEmail(jo5.getString(Constant.SUMBER_BAYAR));
////                        dd.setKewajiban(jo5.getString(Constant.KEWAJIBAN));
////                        dd.setTotal_kewajiban(jo5.getString(Constant.TOTAL_KEWAJIBAN));
////                        dd.setNama_mogen(jo5.getString(Constant.NAMA_MOGEN));
////                        dd.setNo_loan(jo5.getString(Constant.NO_LOAN));
////                        dd.setNama_debitur(jo5.getString(Constant.NAMA_DEBITUR));
////                        dd.setTgl_jth_tempo(jo5.getString(Constant.TGL_JTH_TEMPO));
////                        dd.setNo_tlpn(jo5.getString(Constant.NO_TLPN));
////                        dd.setTgl_gajian(jo5.getString(Constant.TGL_GAJIAN));
////                        dd.setPekerjaan(jo5.getString(Constant.PEKERJAAN));
////                        dd.setAlmt_usaha(jo5.getString(Constant.ALMT_USAHA));
////
////                        //d.deleteAllDetailInventory();
////                        // d.insertDetailInventory(dd);
////
////                    }
//
//
////                        String fpay=jo2.getString("fpay");
////                        System.out.println("fpay: " + fpay);
////                        JSONArray ja3fpay = new JSONArray(fpay);
////                        System.out.println("JA3fpay: " + ja3fpay);
////                        for (int i = 0; i < ja3fpay.length(); i++) {
////                            String jo3fpay = ja3fpay.getString(i);
////                            JSONObject jo4fpay = new JSONObject(jo3fpay);
////                            String cycle = jo4fpay.getString("cycle");
////                            String detail_inventory = jo4fpay.getString("detail_inventory");
////                            System.out.println("Cycle: " + cycle);
////                            System.out.println("Detail Inventory: " + detail_inventory);
////                            JSONObject jo5fpay = new JSONObject(detail_inventory);
////                            System.out.println("JO5fpay: " + jo5fpay);
////                        }
////
////                        String nvst=jo2.getString("nvst");
////                        System.out.println("nvst: " + nvst);
////                        JSONArray ja3nvst = new JSONArray(nvst);
////                        System.out.println("JA3nvst: " + ja3nvst);
////                        for (int i = 0; i < ja3nvst.length(); i++) {
////                            String jo3nvst = ja3nvst.getString(i);
////                            JSONObject jo4nvst = new JSONObject(jo3nvst);
////                            String cycle = jo4nvst.getString("cycle");
////                            String detail_inventory = jo4nvst.getString("detail_inventory");
////                            System.out.println("Cycle: " + cycle);
////                            System.out.println("Detail Inventory: " + detail_inventory);
////                            JSONObject jo5nvst = new JSONObject(detail_inventory);
////                            System.out.println("JO5nvst: " + jo5nvst);
////                        }
////
////                        String ppay=jo2.getString("ppay");
////                        System.out.println("ppay: " + ppay);
////                        JSONArray ja3ppay = new JSONArray(ppay);
////                        System.out.println("JA3ppay: " + ja3ppay);
////                        for (int i = 0; i < ja3ppay.length(); i++) {
////                            String jo3ppay = ja3ppay.getString(i);
////                            JSONObject jo4ppay = new JSONObject(jo3ppay);
////                            String cycle = jo4ppay.getString("cycle");
////                            String detail_inventory = jo4ppay.getString("detail_inventory");
////                            System.out.println("Cycle: " + cycle);
////                            System.out.println("Detail Inventory: " + detail_inventory);
////                            JSONObject jo5ppay = new JSONObject(detail_inventory);
////                            System.out.println("JO5ppay: " + jo5ppay);
////                        }
////
////                        String tgt=jo2.getString("tgt");
////                        System.out.println("tgt: " + tgt);
////                        JSONArray ja3tgt = new JSONArray(tgt);
////                        System.out.println("JA3tgt: " + ja3tgt);
////                        for (int i = 0; i < ja3tgt.length(); i++) {
////                            String jo3tgt = ja3tgt.getString(i);
////                            JSONObject jo4tgt = new JSONObject(jo3tgt);
////                            String cycle = jo4tgt.getString("cycle");
////                            String detail_inventory = jo4tgt.getString("detail_inventory");
////                            System.out.println("Cycle: " + cycle);
////                            System.out.println("Detail Inventory: " + detail_inventory);
////                            JSONObject jo5tgt = new JSONObject(detail_inventory);
////                            System.out.println("JO5tgt: " + jo5tgt);
////                        }
//
//
//                    //   }
//
//
//                    //b.insertPenagihan(namaBucket);
//
//
////                    JSONArray ja1=jo1.getJSONArray("npay");
////                    for(int i=0;i<ja1.length();i++){
////                        JSONObject jo2=ja1.getJSONObject(i);
////                        String cycle=jo2.getString("cycle");
////                        System.out.println(cycle);
////                    }
////
////                    JSONObject jsAll = new JSONObject(json);
////                    JSONArray jsCurrent = jsAll.getJSONArray("penagihan");
////
////                    System.out.println(jsAll.toString());
////                    for (int i = 0; i < jsCurrent.length(); i++) {
////                        JSONArray jsNpay = jsCurrent.getJSONArray(i);
////                        System.out.println(jsNpay.toString());
////                        for (int j = 0; j < jsNpay.length(); j++) {
////                            JSONArray jj = jsCurrent.getJSONArray(j);
////                            System.out.println("jj " + jj.toString());
////                            JSONObject jk = new JSONObject(jj.toString());
////                            JSONArray jl = jk.getJSONArray("npay");
////                            System.out.println("jl" + jl.toString());
////                            for (int k = 0; k < jl.length(); k++) {
////
////                            }
////
////                        }
////                    JSONObject jsonPen=jsNpay.getJSONObject(i);
////                    System.out.println("Hasil: " + jsNpay.toString());
////                        JSONObject jsonNpay=jsNpay.getJSONObject(i);
////                        String n=jsonNpay.getString("cycle");
////                      System.out.println(jsonNpay.toString());
////                        System.out.println("jsonPen "+jsonPen.toString());
////                        JSONArray jsNpay=jsonPen.getJSONArray("npay");
////                        for(int j=0;j<jsNpay.length();j++){
////                            JSONObject jsonisi=jsNpay.getJSONObject(j);
////
////                            String cycle=jsonisi.getString("cycle");
////                            System.out.println(cycle);
////                        }
////                    }
////                    for (int i = 0; i < jsCurrent.length(); i++) {
////                        JSONObject jsNpay = jsCurrent.getJSONObject(i);
////                        JSONArray jsNew=jsNpay.getJSONArray("npay");
////                        for(int j=0;j<jsNew.length();j++){
////                            JSONObject jsonNew=jsNew.getJSONObject(i);
////                            String cycle=jsonNew.getString("cycle");
////                            System.out.println(cycle);
////                        }
////                    }
////                    JSONObject js = new JSONObject(json);
////                    System.out.println("JSON"+js.toString());
////                    String waktu=js.getString("keterangan");
////                    System.out.println(waktu);
////
////                    JSONArray jsArr=js.getJSONArray("penagihan");
////                    for(int i=0;i<jsArr.length();i++){
////                        JSONObject jsNpay=jsArr.optJSONObject("npay");
////                        JSONObject jso=jsArr.getJSONObject(i);
////                        String nilai=jso.getString("npay");
////
////                        System.out.println("Nilai "+nilai);
////                    }
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }


    //new executeLogin(mContext).execute();
//                    Intent i = new Intent(getApplicationContext(), MenuUtama.class);
//                    startActivity(i);

}
