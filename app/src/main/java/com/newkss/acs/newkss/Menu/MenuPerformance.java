package com.newkss.acs.newkss.Menu;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.newkss.acs.newkss.Adapter.PenagihanLama;
import com.newkss.acs.newkss.MenuSettlement.MenuSettlement;
import com.newkss.acs.newkss.MenuSurvey.MenuListSurvey;
import com.newkss.acs.newkss.MenuSurvey.MenuSudahSurvey;
import com.newkss.acs.newkss.Model.Bucket;
import com.newkss.acs.newkss.Adapter.MenuPerformanceAdapter;
import com.newkss.acs.newkss.Adapter.MenuPerformanceAdapterPencairan;
import com.newkss.acs.newkss.Adapter.Pencairan;
import com.newkss.acs.newkss.Model.Config;
import com.newkss.acs.newkss.Model.DeleteData;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.Penagihan;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.ModelBaru.Data_Pencairan;
import com.newkss.acs.newkss.ModelBaru.Detil_Data_Pencairan;
import com.newkss.acs.newkss.ModelBaru.Detil_TGT;
import com.newkss.acs.newkss.ModelBaru.TGT;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.DialogNewPassword;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.JSONParser;
import com.newkss.acs.newkss.Util.MySQLiteHelper;
import com.newkss.acs.newkss.Util.OnCompleteListener;
import com.newkss.acs.newkss.Util.SyncData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by acs on 17/04/17.
 */

public class MenuPerformance extends ParentActivity implements OnCompleteListener {

    TextView txtTGTMenuPerformance, txtFPAYMenuPerformance, txtPPAYMenuPerformance, txtNPAYMenuPerformance, txtNVSTMenuPerformance;
    TextView txtPBLMenuPerformance, txtPBIMenuPerformance, txtSSVMenuPerformance, txtBSVMenuPerformance;
    Detil_TGT dtgt = new Detil_TGT();
    Detil_Data_Pencairan queryDDP = new Detil_Data_Pencairan();
    ImageView imgBackMenuPerformance;

    TextView txtNamaMenuPerformance, txtDivisiMenuPerformance;
    SharedPreferences shared;
    String namaShared, divisiShared, nikShared, usernameShared, passwordShared;
    Detail_Inventory queryDI = new Detail_Inventory();
    Context mContext;

    LinearLayout llMenuPerformancePopup;
    ProgressDialog progressDialog;
    String hasilJson = "";
    JSONParser jsonParser = new JSONParser();
    DeleteData queryDD = new DeleteData();
    Settle_Detil querySD = new Settle_Detil();
    Detail_Inventory querydetinv = new Detail_Inventory();
    Config cc;
    Config queryConfig = new Config();
    ArrayList<Config> listConfig = new ArrayList<>();
    String periodik_track = "10";
    TextView txtTotalAmount;
    Settle_Detil querySettleDetil = new Settle_Detil();
    ImageView imgRefreshMenuPerformance;
    TextView txtSummaryPerformance;

    SyncData querySyncData = new SyncData();
    String version = "";
    String nama = "";
    String path = "";
    String divisi = "";
    String JsonSyncData = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuperformance);
        mContext = this;
        initUI();

    }

    private void initUI() {
        File file = new File(Constant.APP_PATH + Function.getPackageName(mContext) + File.separator + Constant.HIDDEN_FOLDER + File.separator + Constant.DB_NAME);
        if (!file.exists()) {
            MySQLiteHelper.initApp(mContext);
            MySQLiteHelper.initDB(mContext);
        } else {
        }

        txtTotalAmount = (TextView) findViewById(R.id.txtTotalAmount);
        if (querySettleDetil.getTotalNominalSettlement() == null) {
            txtTotalAmount.setText("Total Amount Collect: Rp 0");
        } else {
            txtTotalAmount.setText("Total Amount Collect: Rp " + Function.returnSeparatorComa(querySettleDetil.getTotalAllNominalSettlement().toString()));
        }
        txtSummaryPerformance = (TextView) findViewById(R.id.txtSummaryPerformance);
        int monthNum = Function.getMonthNumber();
        if (monthNum == 0) {
            txtSummaryPerformance.setText("Summary Performance Januari");
        } else if (monthNum == 1) {
            txtSummaryPerformance.setText("Summary Performance Februari");
        } else if (monthNum == 2) {
            txtSummaryPerformance.setText("Summary Performance Maret");
        } else if (monthNum == 3) {
            txtSummaryPerformance.setText("Summary Performance April");
        } else if (monthNum == 4) {
            txtSummaryPerformance.setText("Summary Performance Mei");
        } else if (monthNum == 5) {
            txtSummaryPerformance.setText("Summary Performance Juni");
        } else if (monthNum == 6) {
            txtSummaryPerformance.setText("Summary Performance Juli");
        } else if (monthNum == 7) {
            txtSummaryPerformance.setText("Summary Performance Agustus");
        } else if (monthNum == 8) {
            txtSummaryPerformance.setText("Summary Performance September");
        } else if (monthNum == 9) {
            txtSummaryPerformance.setText("Summary Performance Oktober");
        } else if (monthNum == 10) {
            txtSummaryPerformance.setText("Summary Performance November");
        } else if (monthNum == 11) {
            txtSummaryPerformance.setText("Summary Performance Desember");
        }


        imgBackMenuPerformance = (ImageView) findViewById(R.id.imgBackMenuPerformance);
        imgBackMenuPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuUtama.class);
                startActivity(i);
                finish();
            }
        });


        txtNamaMenuPerformance = (TextView) findViewById(R.id.txtNamaMenuPerformance);
        txtDivisiMenuPerformance = (TextView) findViewById(R.id.txtDivisiMenuPerformance);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            divisiShared = (shared.getString("divisi", ""));
            namaShared = (shared.getString("nama", ""));
            passwordShared = (shared.getString(Constant.SHARED_PASSWORD, ""));
            usernameShared = (shared.getString(Constant.SHARED_USERNAME, ""));
            txtNamaMenuPerformance.setText(namaShared);
            txtDivisiMenuPerformance.setText(divisiShared);
        }


        llMenuPerformancePopup = (LinearLayout) findViewById(R.id.llMenuPerformancePopup);
        llMenuPerformancePopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MenuPerformance.this, llMenuPerformancePopup);
                popup.getMenuInflater().inflate(R.menu.popup_inbox, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_DATAPENDING)) {
                            Intent i = new Intent(getApplicationContext(), MenuDataOffline.class);
                            startActivity(i);
                            finish();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_REFRESH)) {
//                            initUI();
//
//                            shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
//                            if (shared.contains(Constant.SHARED_USERNAME)) {
//                                nikShared = (shared.getString(Constant.NIK, ""));
//                                usernameShared = (shared.getString(Constant.USERNAME, ""));
//                                passwordShared = (shared.getString(Constant.PASSWORD, ""));
//                            }
//
//                            new getNewData(mContext).execute();

                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            if (querySettleDetil.isSettleAvailable().equals("0")) {
                                System.out.println("Check:" + querySettleDetil.isSettleAvailable());
                                Intent i = new Intent(getApplicationContext(), MenuLogin.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                                finish();
                            } else {

                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Pesan");
                                builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Mohon Lakukan Settlement Terlebih Dahulu");

                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent i = new Intent(MenuPerformance.this, MenuSettlement.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });
                                AlertDialog alert1 = builder.create();
                                alert1.show();

                            }
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });


        txtPBLMenuPerformance = (TextView) findViewById(R.id.txtPBLMenuPerformance);
        txtPBIMenuPerformance = (TextView) findViewById(R.id.txtPBIMenuPerformance);
        txtSSVMenuPerformance = (TextView) findViewById(R.id.txtSSVMenuPerformance);
        txtBSVMenuPerformance = (TextView) findViewById(R.id.txtBSVMenuPerformance);


        txtPBLMenuPerformance.setTextColor(Color.parseColor("#5EEDEB"));
        txtPBIMenuPerformance.setTextColor(Color.parseColor("#5EEDEB"));
        txtSSVMenuPerformance.setTextColor(Color.parseColor("#5EEDEB"));
        txtBSVMenuPerformance.setTextColor(Color.parseColor("#5EEDEB"));

        txtPBLMenuPerformance.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        txtPBIMenuPerformance.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        txtSSVMenuPerformance.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        txtBSVMenuPerformance.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        txtPBLMenuPerformance.setText(queryDDP.getCountBLBS());
        txtPBIMenuPerformance.setText(queryDDP.getCountBI());
        txtSSVMenuPerformance.setText(queryDDP.getCountSSStatusOne());
        txtBSVMenuPerformance.setText(queryDI.getCountBSFromDetInv());


        txtPBLMenuPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuListSurvey.class);
                startActivity(i);
            }
        });

        txtPBIMenuPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuListSurvey.class);
                startActivity(i);
            }
        });

        txtSSVMenuPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtSSVMenuPerformance.getText().equals("0")) {
                    Function.showAlert(mContext, "Data Kosong");
                } else {
                    Intent i = new Intent(mContext, MenuListSurvey.class);
                    startActivity(i);
                }
            }
        });

        txtBSVMenuPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtBSVMenuPerformance.getText().equals("0")) {
                    Function.showAlert(mContext, "Data Kosong");
                } else {
                    Intent i = new Intent(mContext, MenuListSurvey.class);
                    startActivity(i);
                }

            }
        });


        txtNVSTMenuPerformance = (TextView) findViewById(R.id.txtNVSTMenuPerformance);
        txtTGTMenuPerformance = (TextView) findViewById(R.id.txtTGTMenuPerformance);
        txtFPAYMenuPerformance = (TextView) findViewById(R.id.txtFPAYMenuPerformance);
        txtPPAYMenuPerformance = (TextView) findViewById(R.id.txtPPAYMenuPerformance);
        txtNPAYMenuPerformance = (TextView) findViewById(R.id.txtNPAYMenuPerformance);

        txtNVSTMenuPerformance.setTextColor(Color.parseColor("#005ec9"));
        txtTGTMenuPerformance.setTextColor(Color.parseColor("#005ec9"));
        txtFPAYMenuPerformance.setTextColor(Color.parseColor("#005ec9"));
        txtPPAYMenuPerformance.setTextColor(Color.parseColor("#005ec9"));
        txtNPAYMenuPerformance.setTextColor(Color.parseColor("#005ec9"));

        txtNVSTMenuPerformance.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        txtTGTMenuPerformance.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        txtFPAYMenuPerformance.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        txtPPAYMenuPerformance.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        txtNPAYMenuPerformance.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        txtTGTMenuPerformance.setText(dtgt.getCountTgt());
        txtNVSTMenuPerformance.setText(dtgt.getCountnvst());
        txtFPAYMenuPerformance.setText(dtgt.getCountfpay());
        txtPPAYMenuPerformance.setText(dtgt.getCountppay());
        txtNPAYMenuPerformance.setText(dtgt.getCountnpay());

        txtTGTMenuPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i = new Intent(mContext, MenuListBucket.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constant.BUNDLE_TITLE, "TGT");
                bundle.putString(Constant.BUNDLE_DATA, "tgt");
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        txtNVSTMenuPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtNVSTMenuPerformance.getText().equals("0")) {
                    Function.showAlert(mContext, "Data Kosong");
                } else {
                    Intent i = new Intent(mContext, MenuListDetailBucket.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.BUNDLE_TITLE, "NVST");
                    bundle.putString(Constant.BUNDLE_DATA, "nvst");
                    i.putExtras(bundle);
                    startActivity(i);
                }
            }
        });

        txtPPAYMenuPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtPPAYMenuPerformance.getText().equals("0")) {
                    Function.showAlert(mContext, "Data Kosong");
                } else {
                    Intent i = new Intent(mContext, MenuListDetailBucket.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.BUNDLE_TITLE, "PPAY");
                    bundle.putString(Constant.BUNDLE_DATA, "ppay");
                    i.putExtras(bundle);
                    startActivity(i);
                }
            }
        });

        txtFPAYMenuPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtFPAYMenuPerformance.getText().equals("0")) {
                    Function.showAlert(mContext, "Data Kosong");
                } else {
                    Intent i = new Intent(mContext, MenuListDetailBucket.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.BUNDLE_TITLE, "FPAY");
                    bundle.putString(Constant.BUNDLE_DATA, "fpay");
                    i.putExtras(bundle);
                    startActivity(i);
                }
            }
        });

        txtNPAYMenuPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtNPAYMenuPerformance.getText().equals("0")) {
                    Function.showAlert(mContext, "Data Kosong");
                } else {
                    Intent i = new Intent(mContext, MenuListDetailBucket.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.BUNDLE_TITLE, "NPAY");
                    bundle.putString(Constant.BUNDLE_DATA, "npay");
                    i.putExtras(bundle);
                    startActivity(i);
                }
            }
        });


        imgRefreshMenuPerformance = (ImageView) findViewById(R.id.imgRefreshMenuPerformance);
        imgRefreshMenuPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Konfirmasi Sync Data");
                builder.setMessage("Lakukan Sync Data? Data Tagihan Akan Diganti dengan data baru");
                builder.setPositiveButton("Sync", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        File file = new File(Constant.APP_PATH + File.separator + Constant.HIDDEN_FOLDER + File.separator + Constant.DB_NAME);
                        if (!file.exists()) {
                            MySQLiteHelper.initApp(mContext);
                            MySQLiteHelper.initDB(mContext);
                        }
                        if (querySD.isSettleAvailable().equals("0")) {
                            new execLogin(mContext).execute();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setTitle("Pesan");
                            builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Mohon Lakukan Settlement Terlebih Dahulu");
                            builder.setIcon(R.drawable.ic_warning_black_24dp);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent i = new Intent(MenuPerformance.this, MenuSettlement.class);
                                    startActivity(i);
                                    finish();
                                }
                            });
                            AlertDialog alert1 = builder.create();
                            alert1.show();
                        }

                    }
                });
                builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
                //MySQLiteHelper.initDB(mContext);


            }
        });
    }


    @Override
    public void onComplete(String value) {
        System.out.println("Value: " + value);
        passwordShared = Function.convertStringtoMD5(value);
        //new getNewData(mContext).execute();
    }


    private class execLogin extends AsyncTask<String, String, String> {
        Context context;

        private execLogin(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle("Validasi Data");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            JSONObject json = new JSONObject();
            try {
                String n1 = "0";
                String n2 = "0";
                String n3 = "0";
                String n4 = n1 + n2 + n3;
                String header = Function.getImei(mContext) + ":" + n4 + ":" + Function.getWaktuSekarangMilis();
                json.put(Constant.USERNAME, usernameShared);
                json.put(Constant.PASSWORD, passwordShared);
                json.put(Constant.WAKTU, Function.getWaktuSekarangMilis());
                hasilJson = jsonParser.HttpRequestPost(Constant.SERVICE_LOGIN, json.toString(), Constant.TimeOutConnection, new String(Function.encodeBase64(header)));

            } catch (Exception e) {
                e.printStackTrace();
            }
            return hasilJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            try {
                JSONObject jsonAll = new JSONObject(result);
                if (result.contains(Constant.CONNECTION_LOST)) {
                    Function.showAlert(mContext, Constant.CONNECTION_LOST);
                } else if (result.contains(Constant.CONNECTION_ERROR)) {
                    Function.showAlert(mContext, Constant.CONNECTION_ERROR);
                } else {
                    String keterangan = jsonAll.getString("keterangan");
                    String rc = jsonAll.getString("rc");

                    if (rc.equals("00")) {
                        String nik = jsonAll.getString(Constant.NIK);
                        version = jsonAll.getString("version");
                        String versiSkrg = getVersion();
//                        version = "1.0.31 TEST ONLY";
//                        String versiSkrg = "1.0.31 TEST ONLY";
                        nama = jsonAll.getString("nama");
                        path = jsonAll.getString("path");
                        divisi = jsonAll.getString("divisi");
                        periodik_track = jsonAll.getString("periodik_track");
                        shared = getApplicationContext().getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                        SharedPreferences.Editor editor = shared.edit();
                        editor.putString(Constant.SHARED_USERNAME, usernameShared);
                        editor.putString(Constant.SHARED_PASSWORD, passwordShared);
                        editor.putString(Constant.NIK, nik);
                        editor.putString("divisi", divisi);
                        editor.putString("nama", nama);
                        editor.commit();
                        //TODO CHECK VERSION
                        if (!version.equals("-")) {
                            if (versiSkrg.equals(version)) {
                                new getResponse(mContext).execute();
                            } else if (!versiSkrg.equals(version)) {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Konfirmasi Update");
                                builder.setMessage("Terdapat Versi Baru, Versi" + version + "  Mohon Lakukan Login Kembali Untuk Update Aplikasi");
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent i = new Intent(mContext, MenuLogin.class);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(i);
                                        finish();
                                    }
                                });
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        } else {
                            new getResponse(mContext).execute();
                        }
                    } else if (rc.equals("T1")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage(keterangan);
                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                shared.edit().clear().commit();
                                Intent i = new Intent(MenuPerformance.this, MenuLogin.class);
                                startActivity(i);
                                finish();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else if (rc.equals("T2")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage(keterangan);
                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                shared.edit().clear().commit();
                                Intent i = new Intent(MenuPerformance.this, MenuLogin.class);
                                startActivity(i);
                                finish();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else if (rc.equals("T3")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage(keterangan);
                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                shared.edit().clear().commit();
                                Intent i = new Intent(MenuPerformance.this, MenuLogin.class);
                                startActivity(i);
                                finish();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else if (rc.equals("T4")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage(keterangan);
                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                shared.edit().clear().commit();
                                Intent i = new Intent(MenuPerformance.this, MenuLogin.class);
                                startActivity(i);
                                finish();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else if (rc.equals("10")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage(keterangan);
                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                shared.edit().clear().commit();
                                Intent i = new Intent(MenuPerformance.this, MenuLogin.class);
                                startActivity(i);
                                finish();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage(keterangan);
                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                shared.edit().clear().commit();
                                Intent i = new Intent(MenuPerformance.this, MenuLogin.class);
                                startActivity(i);
                                finish();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                }
            } catch (Exception e) {
                Function.showAlert(mContext, result);
                e.printStackTrace();
            }
        }
    }


    class getResponse extends AsyncTask<String, String, String> {
        Context context;

        private getResponse(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle("Mengecek Data");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            JSONObject json = new JSONObject();
            try {
                shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                if (shared.contains(Constant.NIK)) {
                    nikShared = (shared.getString(Constant.NIK, ""));
                }
                String header = Function.getImei(mContext) + ":" + nikShared + ":" + Function.getWaktuSekarangMilis();

                json.put(Constant.USERNAME, usernameShared);
                json.put(Constant.PASSWORD, passwordShared);
                json.put(Constant.WAKTU, Function.getWaktuSekarangMilis());
                JsonSyncData = jsonParser.HttpRequestPost(Constant.SERVICE_DATA_SYNC, json.toString(), Constant.TimeOutConnection, new String(Function.encodeBase64(header)));

            } catch (Exception e) {
                e.printStackTrace();
            }

            return JsonSyncData;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            try {
                JSONObject jsonAll = new JSONObject(result);
                if (result.contains(Constant.CONNECTION_LOST)) {
                    Function.showAlert(mContext, Constant.CONNECTION_LOST);
                } else if (result.contains(Constant.CONNECTION_ERROR)) {
                    Function.showAlert(mContext, Constant.CONNECTION_ERROR);
                } else {
                    try {
//                        String waktu = jsonAll.getString("periodik_track");
                        String rc = jsonAll.getString("rc");
                        String keterangan = jsonAll.getString("keterangan");

                        if (rc.equals("00")) {
                            String waktu = jsonAll.getString("waktu");
                            System.out.println("String waktu: " + waktu + " --- " + queryConfig.getDateConfig());
                            if (Function.compareDate(waktu, queryConfig.getDateConfig()) == "sama") {
//                                Intent i = new Intent(MenuPerformance.this, MenuUtama.class);
//                                startActivity(i);
//                                finish();
                                if (querySD.isSettleAvailable().equals("0")) {
                                    new syncNewDataDB(mContext).execute();
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setTitle("Pesan");
                                    builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Mohon Lakukan Settlement Terlebih Dahulu");
                                    builder.setIcon(R.drawable.ic_warning_black_24dp);
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent i = new Intent(MenuPerformance.this, MenuSettlement.class);
                                            startActivity(i);
                                            finish();
                                        }
                                    });
                                    AlertDialog alert1 = builder.create();
                                    alert1.show();
                                }

                                System.out.println("Waktu Sama");
                            } else if (Function.compareDate(waktu, queryConfig.getDateConfig()) == "beda") {
                                System.out.println("Waktu Beda");
                                File file = new File(Constant.APP_PATH);
                                if (!file.exists()) {
                                    MySQLiteHelper.initApp(mContext);
                                }
                                if (querySD.isSettleAvailable().equals("0")) {
                                    queryDD.deleteAllDetailInventory();
                                    queryDD.deleteAllPhoto();
                                    queryDD.deleteAllDataPencairan();
                                    queryDD.deleteAllTGT();
                                    queryDD.deleteAllDetilDataPencairan();
                                    queryDD.deleteAllDetilTGT();
                                    queryDD.deleteAllSettle();
                                    queryDD.deleteAllDetilSettle();
                                    queryDD.deleteAllDataBooked();
                                    queryDD.deleteAllMasterSettle();
                                    queryDD.deleteAllSequence();
                                    queryDD.deleteAllParameter();
                                    new syncData(mContext).execute();
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setTitle("Pesan");
                                    builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Mohon Lakukan Settlement Terlebih Dahulu");
                                    builder.setIcon(R.drawable.ic_warning_black_24dp);
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent i = new Intent(MenuPerformance.this, MenuSettlement.class);
                                            startActivity(i);
                                            finish();
                                        }
                                    });
                                    AlertDialog alert1 = builder.create();
                                    alert1.show();
                                }
                                queryDD.deleteAllDetailInventory();
                                queryDD.deleteAllPhoto();
                                queryDD.deleteAllDataPencairan();
                                queryDD.deleteAllTGT();
                                queryDD.deleteAllDetilDataPencairan();
                                queryDD.deleteAllDetilTGT();
                                queryDD.deleteAllSettle();
                                queryDD.deleteAllDetilSettle();
                                queryDD.deleteAllDataBooked();
                                queryDD.deleteAllMasterSettle();
                                queryDD.deleteAllSequence();
                                queryDD.deleteAllParameter();
                                new syncData(mContext).execute();
                            }
                        } else if (rc.equals("T1")) {
                            Function.showAlert(mContext, keterangan);
                        } else if (rc.equals("T2")) {
                            Function.showAlert(mContext, keterangan);
                        } else if (rc.equals("T3")) {
                            Function.showAlert(mContext, keterangan);
                        } else if (rc.equals("T4")) {
                            Function.showAlert(mContext, keterangan);
                        } else if (rc.equals("T5")) {
                            Function.showAlert(mContext, keterangan);
                        } else {
                            Function.showAlert(mContext, keterangan);
                        }
                    } catch (Exception e) {
                        Function.showAlert(mContext, result);
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Function.showAlert(mContext, result);
            }
            // new syncData(mContext).execute();
        }
    }


    class syncData extends AsyncTask<String, String, String> {
        Context context;

        private syncData(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle("Sync Data");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            System.out.println("Masuk SyncData");
            querySyncData.insertNewDataToDB(mContext, JsonSyncData, version, periodik_track);
            //putJson();
            return "Sukses";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            //Toast.makeText(mContext, result, Toast.LENGTH_SHORT).show();

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Pesan");
            builder.setMessage("Sukses Sync Data");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            AlertDialog alert1 = builder.create();
            alert1.show();


        }

    }

    class syncNewDataDB extends AsyncTask<String, String, String> {
        Context context;

        private syncNewDataDB(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle("Sync Data");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            queryDD.deleteAllDetilTGT();
            queryDD.deleteAllTGT();
            queryDD.deleteAllDataPencairan();
            queryDD.deleteAllDetilDataPencairan();
            queryDD.deleteAllDataBooked();
            queryDD.deleteAllDetailInventory();
            queryDD.deleteAllParameter();
            querySyncData.syncUpdatedata(JsonSyncData);
            return "Sukses";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            txtTGTMenuPerformance.setText(dtgt.getCountTgt());
            txtNVSTMenuPerformance.setText(dtgt.getCountnvst());
            txtFPAYMenuPerformance.setText(dtgt.getCountfpay());
            txtPPAYMenuPerformance.setText(dtgt.getCountppay());
            txtNPAYMenuPerformance.setText(dtgt.getCountnpay());
        }
    }


    public static void longLog(String str) {
        if (str.length() > 4000) {
            Log.d("ini contoh json", str.substring(0, 4000));
            //System.out.println("return jsonnya: "+str.substring(0, 4000));
            longLog(str.substring(4000));
        } else
            Log.d("ini contoh json", str);
        //System.out.println("return jsonnya: "+str);
    }

    private String getVersion() {
        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return packageInfo.versionName;
    }

    @Override
    public void onBackPressed() {

    }
}
