package com.newkss.acs.newkss.Menu;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.newkss.acs.newkss.Adapter.MenuPerformanceAdapter;
import com.newkss.acs.newkss.Adapter.MenuPerformanceAdapterPencairan;
import com.newkss.acs.newkss.Adapter.PenagihanLama;
import com.newkss.acs.newkss.Adapter.Pencairan;
import com.newkss.acs.newkss.Model.Bucket;
import com.newkss.acs.newkss.Model.Penagihan;
import com.newkss.acs.newkss.ModelBaru.TGT;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import java.util.ArrayList;

/**
 * Created by acs on 17/04/17.
 */

public class MenuPerformanceBackupUpdate extends ParentActivity {
    private Context mContext;
    ListView llPenagihan, llPencairan;
    ArrayList<PenagihanLama> list;
    ArrayList<Penagihan> listJmlhPenagihan;
    ArrayList<Pencairan> listPencairan;
    PenagihanLama p;
    Pencairan pencairan;

    String judul, isi;

    Bucket b = new Bucket();
    ImageView imgBackMenuPerformance;


    ArrayList<TGT> listTGT;
    ArrayList<Penagihan> listPenagihan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuperformance);
        mContext = this;
//        com.newkss.acs.newkss.Model.Bucket b = new Bucket();


        MySQLiteHelper.initDB(mContext);

        //Toast.makeText(mContext, "Nilai: " + b.getCountTgt("current"), Toast.LENGTH_SHORT).show();

        add();
        addPencairan();

        imgBackMenuPerformance = (ImageView) findViewById(R.id.imgBackMenuPerformance);
        imgBackMenuPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        llPenagihan = (ListView) findViewById(R.id.listPenagihan);
        llPencairan = (ListView) findViewById(R.id.listPencairan);
        MenuPerformanceAdapter adapter = new MenuPerformanceAdapter(mContext, list);
        MenuPerformanceAdapterPencairan adapterPencairan = new MenuPerformanceAdapterPencairan(mContext, listPencairan);
        llPencairan.setAdapter(adapterPencairan);
        setListViewHeightBasedOnChildren(llPencairan);
        llPenagihan.setAdapter(adapter);
        setListViewHeightBasedOnChildren(llPenagihan);


        //System.out.println("Nilai: " + b.getCountTgt("current"));

//        Toast.makeText(getApplicationContext(),b.getC)

//        llPenagihan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        //String nilai = adapterView.getAdapter().getItem(i).toString();

//                String value=adapterView.getSelectedItem().

//                final TextView bucketTitle = (TextView) view.findViewById(R.id.bucketlistrow);
//                final TextView TGT = (TextView) view.findViewById(R.id.tgtlistrow);
//                final TextView FPAY = (TextView) view.findViewById(R.id.fpaylistrow);
//                final TextView PPAY = (TextView) view.findViewById(R.id.ppaylistrow);
//                final TextView NPAY = (TextView) view.findViewById(R.id.npaylistrow);
//                final TextView NVST = (TextView) view.findViewById(R.id.nvstlistrow);
//
//                String title = bucketTitle.getText().toString();
//
//                String tgt = TGT.getText().toString();


        //PenagihanLama p = list.get(i);
//                PenagihanLama p=(PenagihanLama)adapterView.getSelectedItem();
        //PenagihanLama p=(PenagihanLama)adapterView.getSelectedItem();

//                PenagihanLama p = (PenagihanLama) adapterView.getAdapter().getItem(i);
        //Toast(getApplicationContext(), judul + "--||--" + isi);


//                PenagihanLama p = (PenagihanLama) adapterView.getItemAtPosition(i);

//                View v = llPenagihan.getChildAt(i-llPenagihan.getFirstVisiblePosition());
//                PenagihanLama p=(PenagihanLama) list.get(i);
//
//                Toast.makeText(mContext, p.getPPAY(), Toast.LENGTH_SHORT).show();
//                String selectedFromList = (llPenagihan.getItemAtPosition(i)).toString();
//                String value = (String)adapterView.getItemAtPosition(i).toString();
//                System.out.println("INI VALUE: "+
//                        value);
//                Toast(getApplicationContext());

//                Intent in = new Intent(getApplicationContext(), MenuListBucket.class);
//                Bundle bundle = new Bundle();
//                bundle.putString("TITLE", judul);
//                bundle.putString("ISI", isi);
//                in.putExtras(bundle);
//                startActivity(in);
        // Toast(getApplicationContext(),adapterView.getSelectedItemId()+"---"+adapterView.getSelectedItemPosition());
        // Toast.makeText(getApplicationContext(), list.get(1).toString(), Toast.LENGTH_SHORT).show();
//            }
//        });


    }


    /****
     * Method for Setting the Height of the ListView dynamically.
     * *** Hack to fix the issue of not showing all the items of the ListView
     * *** when placed inside a ScrollView
     ****/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    private void addPencairan() {
        listPencairan = new ArrayList<>();
        pencairan = new Pencairan();
        pencairan.setPBL("5");
        pencairan.setPBI("10");
        pencairan.setSSV("12");
        pencairan.setBSV("3");
        listPencairan.add(pencairan);
    }

    private void add() {
        list = new ArrayList<>();

//        p = new PenagihanLama();
//        p.setBucket("Bucket");
//        p.setTgt("TGT");
//        p.setFpay("FPAY");
//        p.setPpay("PPAY");
//        p.setNpay("NPAY");
//        p.setNvst("NVST");
//        list.add(p);
        Penagihan plist = new Penagihan();
        listJmlhPenagihan = new ArrayList<>();
        listJmlhPenagihan = plist.getListPenagihan();
        System.out.println("Jmlh: " + listJmlhPenagihan.size());
        for (int i = 0; i < listJmlhPenagihan.size(); i++) {
            System.out.println("Isi " + listJmlhPenagihan.get(i).getNama_penagihan());

            p = new PenagihanLama();
            p.setBucketTitle(listJmlhPenagihan.get(i).getNama_penagihan());
            p.setTGT(b.getCountTgt(listJmlhPenagihan.get(i).getNama_penagihan()));
            p.setFPAY(b.getCountfpay(listJmlhPenagihan.get(i).getNama_penagihan()));
            p.setNPAY(b.getCountnpay(listJmlhPenagihan.get(i).getNama_penagihan()));
            p.setPPAY(b.getCountppay(listJmlhPenagihan.get(i).getNama_penagihan()));
            p.setNVST(b.getCountnvst(listJmlhPenagihan.get(i).getNama_penagihan()));
            list.add(p);
        }


//        p = new PenagihanLama();
//        p.setBucketTitle("current");
//        p.setTGT("37");
//        p.setFPAY("5");
//        p.setPPAY("5");
//        p.setNPAY("8");
//        p.setNVST("19");
//        list.add(p);
//
//        p = new PenagihanLama();
//        p.setBucketTitle("1-30");
//        p.setTGT("40");
//        p.setFPAY("6");
//        p.setPPAY("6");
//        p.setNPAY("9");
//        p.setNVST("19");
//        list.add(p);


//        p = new PenagihanLama();
//        p.setBucket("Current");
//        p.setTgt("37");
//        p.setFpay("5");
//        p.setPpay("5");
//        p.setNpay("8");
//        p.setNvst("19");
//        list.add(p);
//
//        p = new PenagihanLama();
//        p.setBucket("1-30");
//        p.setTgt("40");
//        p.setFpay("6");
//        p.setPpay("6");
//        p.setNpay("9");
//        p.setNvst("19");
//        list.add(p);

    }


//    CheckBox checkBox;
//    TextView tv, qty;
//    TextView bucketTitle, tgtTitle, fpayTitle, ppayTitle, npayTitle, nvstTitle;
//    ImageButton addBtn, minusBtn;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.menuperformance);
//
//        TableLayout ll = (TableLayout) findViewById(R.id.displayLinear);
//
//
//        for (int i = 0; i < 2; i++) {
//
//            if(i==0){
//                TableRow row = new TableRow(this);
//                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
//                bucketTitle=new TextView(this);
//                bucketTitle.setText("Bucket");
//                bucketTitle.setBackgroundResource(R.drawable.bordermenu);
//                tes t=new tes(getApplicationContext());
//                t.setText("abc");
//                t.setNilaiid("2");
//                tgtTitle=new TextView(this);
//                tgtTitle.setText(t.text);
//
//
//                fpayTitle=new TextView(this);
//                fpayTitle.setText("FPay");
//                row.addView(bucketTitle);
//                row.addView(tgtTitle);
//                row.addView(fpayTitle);
//                ll.addView(row,i);
//            }
//            else{
//                TableRow row = new TableRow(this);
//                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
//                row.setBackgroundResource(R.drawable.bordermenu);
//                row.setLayoutParams(lp);
//                checkBox = new CheckBox(this);
//                tv = new TextView(this);
//                addBtn = new ImageButton(this);
//                addBtn.setImageResource(R.mipmap.ic_launcher);
//                minusBtn = new ImageButton(this);
//                minusBtn.setImageResource(R.mipmap.ic_launcher);
//                qty = new TextView(this);
//                //checkBox.setText("hello");
//                qty.setText("10");
//                //row.addView(checkBox);
//               row.addView(minusBtn);
//                row.addView(qty);
//                row.addView(addBtn);
//                ll.addView(row, i);
//            }
//        }
//
//
//    }
//
//    class tes extends TextView{
//        String nilaiid;
//        String text;
//
//        public tes(Context context) {
//            super(context);
//        }
//
//        public String getNilaiid() {
//            return nilaiid;
//        }
//
//        public void setNilaiid(String nilaiid) {
//            this.nilaiid = nilaiid;
//        }
//
//        public String getText() {
//            return text;
//        }
//
//        public void setText(String text) {
//            this.text = text;
//        }
//    }
}
