package com.newkss.acs.newkss.Menu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TableRow;
import android.widget.TextView;

import com.newkss.acs.newkss.MenuPhoto.MenuTakePhoto;
import com.newkss.acs.newkss.MenuSettlement.MenuSettlement;
import com.newkss.acs.newkss.MenuSurvey.MenuListSurvey;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by acs on 11/04/17.
 */

public class MenuUtama extends ParentActivity {
    private TableRow tblRowPerformance, tblRowInventoryCollect, tblRowInventorySurvey, tblRowSettlement;
    private LinearLayout llMenuUtamaPopup;
    Context mContext;

    TextView txtNamaMenuUtama, txtDivisiMenuUtama;
    String namaShared, divisiShared;
    SharedPreferences shared;
    Settle_Detil querySD = new Settle_Detil();

    String compare = "SAMA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuutama);
        mContext = this;
        initUI();
        Function.triggerAlarm(mContext);
        Function.triggerAlarmSendOfflineData(mContext);
    }

    private void initUI() {
        MySQLiteHelper.initApp(mContext);
        MySQLiteHelper.initDB(mContext);
        txtDivisiMenuUtama = (TextView) findViewById(R.id.txtDivisiMenuUtama);
        txtNamaMenuUtama = (TextView) findViewById(R.id.txtNamaMenuUtama);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtNamaMenuUtama.setText(namaShared);
            txtDivisiMenuUtama.setText(divisiShared);
            System.out.println("DIVISI: " + divisiShared);
        }


        tblRowPerformance = (TableRow) findViewById(R.id.tblRowPerformance);
        tblRowPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i = new Intent(getApplicationContext(), MenuPerformance.class);
                startActivity(i);
            }
        });

        tblRowInventoryCollect = (TableRow) findViewById(R.id.tblRowInventoryCollect);
        tblRowInventoryCollect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> listDate = new ArrayList<String>();
                listDate = querySD.getWaktuSettleDetil();

                compare = Function.compareDateDataSettlement(listDate, Function.getDateNow());

                if (compare.isEmpty()) {
                    System.out.println("KOSONG");
                    Intent i = new Intent(getApplicationContext(), MenuListBucketSelected.class);
                    startActivity(i);
                } else if (compare.equals("SAMA")) {
                    System.out.println("SAMA");
                    Intent i = new Intent(getApplicationContext(), MenuListBucketSelected.class);
                    startActivity(i);
                } else if (compare.equals("BEDA")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle("Pesan");
                    builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Mohon Lakukan Settlement Terlebih Dahulu");
                    builder.setIcon(R.drawable.ic_warning_black_24dp);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(MenuUtama.this, MenuSettlement.class);
                            startActivity(i);
                            finish();
                        }
                    });
                    AlertDialog alert1 = builder.create();
                    alert1.show();
                }


            }
        });

        tblRowInventorySurvey = (TableRow) findViewById(R.id.tblRowInventorySurvey);
        tblRowInventorySurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MenuListSurvey.class);
                startActivity(i);
            }
        });

        tblRowSettlement = (TableRow) findViewById(R.id.tblRowSettlement);
        tblRowSettlement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MenuSettlement.class);
                startActivity(i);
            }
        });

        llMenuUtamaPopup = (LinearLayout) findViewById(R.id.llMenuUtamaPopup);
        llMenuUtamaPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MenuUtama.this, llMenuUtamaPopup);
                popup.getMenuInflater().inflate(R.menu.popup_inbox, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_DATAPENDING)) {
                            Intent i = new Intent(getApplicationContext(), MenuDataOffline.class);
                            startActivity(i);
                            finish();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            if (querySD.isSettleAvailable().equals("0")) {
                                System.out.println("Check:" + querySD.isSettleAvailable());
                                Intent i = new Intent(getApplicationContext(), MenuLogin.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                                finish();
                            } else {

                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Pesan");
                                builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Mohon Lakukan Settlement Terlebih Dahulu");

                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent i = new Intent(MenuUtama.this, MenuSettlement.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });
                                AlertDialog alert1 = builder.create();
                                alert1.show();

                            }
                        } else {

                        }
                        return true;
                    }
                });
                popup.show();
            }
        });
    }

    @Override
    public void onBackPressed() {

    }
}
