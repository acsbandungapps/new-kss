package com.newkss.acs.newkss.MenuDetailVisit;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;

import com.newkss.acs.newkss.Menu.MenuDataOffline;
import com.newkss.acs.newkss.Menu.MenuDetailInventoryToday;
import com.newkss.acs.newkss.Menu.MenuInisialisasi;
import com.newkss.acs.newkss.Menu.MenuLogin;
import com.newkss.acs.newkss.Menu.MenuUtama;
import com.newkss.acs.newkss.MenuKunjungan.MenuHasilKunjungan;
import com.newkss.acs.newkss.MenuPhoto.MenuTakePhoto;
import com.newkss.acs.newkss.Model.Detail_Bucket;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.Pending;
import com.newkss.acs.newkss.Model.Photo;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.ModelBaru.Data_Pencairan;
import com.newkss.acs.newkss.ModelBaru.Detil_Data_Pencairan;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Service.ServiceSendLocation;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.DialogNewPassword;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.JSONParser;
import com.newkss.acs.newkss.Util.NumberTextWatcherForThousand;
import com.newkss.acs.newkss.Util.OnCompleteListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Erdy on 5/5/2017.
 */
public class MenuDetailVisitSurvey extends ParentActivity implements OnCompleteListener {
    //Detail_Bucket d = new Detail_Bucket();

    private String[] arrayKepuasanNasabah, arrayAlamatNoTelpEmail, arrayKondisiJaminan, arrayJenisPetani, arraySpinnerSektorUsaha, arraySpinnerSubSektorUsaha, arraySpinnerBertemuDengan, arraySpinnerKeberadaanAset;
    private String[] arraySpinnerSubSektorUsaha1, arraySpinnerSubSektorUsaha2, arraySpinnerSubSektorUsaha3, arraySpinnerSubSektorUsaha4, arraySpinnerSubSektorUsaha5, arraySpinnerSubSektorUsaha6, arraySpinnerSubSektorUsaha7, arraySpinnerSubSektorUsaha8, arraySpinnerSubSektorUsaha9;

    Spinner spinnerKepuasanNasabahMenuDetailVisit, spinnerEmailNasabahMenuDetailVisit, spinnerNoTelpNasabahMenuDetailVisit, spinnerAlamatUsahaNasabahMenuDetailVisit, spinnerAlamatNasabahMenuDetailVisit, spinnerKondisiJaminanMenuDetailVisit, spinnerJenisPetaniMenuDetailVisit, spinnerSektorUsahaMenuDetailVisit, spinnerSubSektorUsahaMenuDetailVisit, spinnerKeberadaanAsetNasabahMenuDetailVisit, spinnerBertemuDenganNasabahMenuDetailVisit;
    EditText etSektorUsahaMenuDetailVisit, etNamaNasabahMenuDetailVisit, etAlamatnasabahMenuDetailVisit, etAlamatUsahaNasabahMenuDetailVisit, etNomorHPNasabahMenuDetailVisit, etAlamatEmailNasabahMenuDetailVisit, etPekerjaanNasabahMenuDetailVisit, etLuasKebunMenuDetailVisit, etHargaKomoditiMenuDetailVisit, etjenisJaminanNasabahMenuDetailVisit;
    EditText etAlamatNasabah1, etAlamatBaruNasabah1, etAlamatUsahaNasabah1, etAlamatUsahaBaruNasabah1, etNoTelp1, etNoTelpBaru1, etEmailNasabah1, etEmailNasabahBaru1;
    static EditText etTglGajiannNasabahMenuDetailVisit;

    String alamatNasabah1, alamatBaruNasabah1, alamatUsahaNasabah, alamatUsahaNasabah1, noTelp1, noTelpBaru1, email1, emailBaru1;
    String id, type;
    Data_Pencairan querydp = new Data_Pencairan();
    Data_Pencairan datadp = new Data_Pencairan();
    Detail_Inventory queryDetInv = new Detail_Inventory();
    Detail_Inventory dataDetInv = new Detail_Inventory();
    Detil_Data_Pencairan queryDDP = new Detil_Data_Pencairan();
    Detil_Data_Pencairan dataDDP = new Detil_Data_Pencairan();


    ImageView imgBackMenuDetailInventoryToday;
    Button btnUploadDocMenuDetailVisit, btnSubmitMenuDetailVisit;

    Context mContext;

    ProgressDialog progressDialog;
    String usernameShared, passwordShared;
    String alamat_baru, nama_nasabah, sesuai_alamat_usaha, tlpn_usaha, bertemu_dengan, sesuai_alamat, no_hp, upload_doc, kepuasan_nasabah, alamat_usaha, pekerjaan_nasabah, sesuai_no_tlpn, alamat_nasabah, email, cek_dok, keberadaan_aset, tgl_gajian_nasabah, alamat_usaha_baru, sesuai_email, keterangan, img, waktu_kunjungan;

    Photo queryPhoto = new Photo();
    Photo dataPhoto = new Photo();
    ArrayList<Photo> listPhoto = new ArrayList<>();
    String noloan;
    String resultfromJson = "";
    String nikShared = "";
    JSONParser jsonParser = new JSONParser();

    ServiceSendLocation querySSL;
    TextView txtNamaMenuDetailVisit, txtDivisiMenuDetailVisit;
    SharedPreferences shared;
    String namaShared, divisiShared;

    Pending dataPending;
    Pending queryPending = new Pending();
    String jsonStr = "";

    LinearLayout llMenuDetailVisitPopup;
    List<String> listTanggal = new ArrayList<>();
    Location location;
    Double latitude = 0.0;
    Double longitude = 0.0;
    Settle_Detil querySettle_detil = new Settle_Detil();
    String unique_code = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menudetailvisit);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = this;
        querySSL = new ServiceSendLocation(mContext);
        initUI();
    }

    private void setAdapterTanggal() {
        for (int i = 1; i <= 31; i++) {
            listTanggal.add(String.valueOf(i));
        }
    }

    private void initUI() {
        setAdapterTanggal();
        Intent in = getIntent();
        id = in.getStringExtra("id");
        // id = "DI-0000001";
        String iddp = queryDetInv.getIdDataPencairanFromIdDetInv(id);
        dataDetInv = queryDetInv.getDetInventoryFromIdDetInv(id);
        datadp = querydp.getDataPencairanFromID(iddp);
        noloan = queryDetInv.getNoLoanfromIdDetInv(id);
        //  d = (Detail_Bucket) getIntent().getParcelableExtra("detailbucket");
        btnUploadDocMenuDetailVisit = (Button) findViewById(R.id.btnUploadDocMenuDetailVisit);
        btnUploadDocMenuDetailVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuTakePhoto.class);
                i.putExtra("id", id);
                startActivity(i);
            }
        });

        llMenuDetailVisitPopup = (LinearLayout) findViewById(R.id.llMenuDetailVisitPopup);
        llMenuDetailVisitPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MenuDetailVisitSurvey.this, llMenuDetailVisitPopup);
                popup.getMenuInflater().inflate(R.menu.popup_inbox, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_DATAPENDING)) {
                            Intent i = new Intent(getApplicationContext(), MenuDataOffline.class);
                            startActivity(i);
                            finish();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_REFRESH)) {
                            initUI();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            Intent i = new Intent(mContext, MenuLogin.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });
        txtDivisiMenuDetailVisit = (TextView) findViewById(R.id.txtDivisiMenuDetailVisit);
        txtNamaMenuDetailVisit = (TextView) findViewById(R.id.txtNamaMenuDetailVisit);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtNamaMenuDetailVisit.setText(namaShared);
            txtDivisiMenuDetailVisit.setText(divisiShared);
        }


        imgBackMenuDetailInventoryToday = (ImageView) findViewById(R.id.imgBackMenuDetailInventoryToday);
        imgBackMenuDetailInventoryToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        etNamaNasabahMenuDetailVisit = (EditText) findViewById(R.id.etNamaNasabahMenuDetailVisit);
        etAlamatnasabahMenuDetailVisit = (EditText) findViewById(R.id.etAlamatnasabahMenuDetailVisit);
        etAlamatUsahaNasabahMenuDetailVisit = (EditText) findViewById(R.id.etAlamatUsahaNasabahMenuDetailVisit);
        etNomorHPNasabahMenuDetailVisit = (EditText) findViewById(R.id.etNomorHPNasabahMenuDetailVisit);
        etAlamatEmailNasabahMenuDetailVisit = (EditText) findViewById(R.id.etAlamatEmailNasabahMenuDetailVisit);
        etPekerjaanNasabahMenuDetailVisit = (EditText) findViewById(R.id.etPekerjaanNasabahMenuDetailVisit);
        etTglGajiannNasabahMenuDetailVisit = (EditText) findViewById(R.id.etTglGajiannNasabahMenuDetailVisit);
        etSektorUsahaMenuDetailVisit = (EditText) findViewById(R.id.etSektorUsahaMenuDetailVisit);
        etLuasKebunMenuDetailVisit = (EditText) findViewById(R.id.etLuasKebunMenuDetailVisit);
        etHargaKomoditiMenuDetailVisit = (EditText) findViewById(R.id.etHargaKomoditiMenuDetailVisit);
        etjenisJaminanNasabahMenuDetailVisit = (EditText) findViewById(R.id.etjenisJaminanNasabahMenuDetailVisit);

//        etTglGajiannNasabahMenuDetailVisit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                DialogFragment dialogFragment = new DatePickerDialogClass();
//                dialogFragment.show(getFragmentManager(), "Date Picker Dialog");
//            }
//        });

        etAlamatNasabah1 = (EditText) findViewById(R.id.etAlamatNasabah1);
        etAlamatBaruNasabah1 = (EditText) findViewById(R.id.etAlamatBaruNasabah1);
        etAlamatUsahaNasabah1 = (EditText) findViewById(R.id.etAlamatUsahaNasabah1);
        etAlamatUsahaBaruNasabah1 = (EditText) findViewById(R.id.etAlamatUsahaBaruNasabah1);
        etNoTelp1 = (EditText) findViewById(R.id.etNoTelp1);
        etNoTelpBaru1 = (EditText) findViewById(R.id.etNoTelpBaru1);
        etEmailNasabah1 = (EditText) findViewById(R.id.etEmailNasabah1);
        etEmailNasabahBaru1 = (EditText) findViewById(R.id.etEmailNasabahBaru1);


        spinnerBertemuDenganNasabahMenuDetailVisit = (Spinner) findViewById(R.id.spinnerBertemuDenganNasabahMenuDetailVisit);
        spinnerKeberadaanAsetNasabahMenuDetailVisit = (Spinner) findViewById(R.id.spinnerKeberadaanAsetNasabahMenuDetailVisit);
        spinnerSektorUsahaMenuDetailVisit = (Spinner) findViewById(R.id.spinnerSektorUsahaMenuDetailVisit);
        spinnerSubSektorUsahaMenuDetailVisit = (Spinner) findViewById(R.id.spinnerSubSektorUsahaMenuDetailVisit);
        spinnerJenisPetaniMenuDetailVisit = (Spinner) findViewById(R.id.spinnerJenisPetaniMenuDetailVisit);
        spinnerKondisiJaminanMenuDetailVisit = (Spinner) findViewById(R.id.spinnerKondisiJaminanMenuDetailVisit);

        spinnerAlamatNasabahMenuDetailVisit = (Spinner) findViewById(R.id.spinnerAlamatNasabahMenuDetailVisit);
        spinnerAlamatUsahaNasabahMenuDetailVisit = (Spinner) findViewById(R.id.spinnerAlamatUsahaNasabahMenuDetailVisit);
        spinnerNoTelpNasabahMenuDetailVisit = (Spinner) findViewById(R.id.spinnerNoTelpNasabahMenuDetailVisit);
        spinnerEmailNasabahMenuDetailVisit = (Spinner) findViewById(R.id.spinnerEmailNasabahMenuDetailVisit);
        spinnerKepuasanNasabahMenuDetailVisit = (Spinner) findViewById(R.id.spinnerKepuasanNasabahMenuDetailVisit);


        arraySpinnerBertemuDengan = new String[]{"Nasabah", "Keluarga", "Tetangga"};
        arraySpinnerKeberadaanAset = new String[]{"Ada", "Tidak Ada"};
        arraySpinnerSektorUsaha = new String[]{"Perdagangan", "Jasa-Jasa Dunia Usaha", "Jasa-Jasa Sosial Masyarakat", "Pertanian", "Industri", "Pengangkutan,Pergudangan dan Komunikasi", "Konstruksi", "Pertambangan", "Listrik Gas Air"};
        arraySpinnerSubSektorUsaha = new String[]{"Batubara", "Emas"};
        arrayJenisPetani = new String[]{"Petani Mandiri", "Petani Plasma", "Bukan Petani"};
        arrayKondisiJaminan = new String[]{"Baik", "Buruk"};
        arrayAlamatNoTelpEmail = new String[]{"Sesuai", "Tidak"};
        arrayKepuasanNasabah = new String[]{"Puas", "Kurang Puas", "Tidak Puas"};

        arraySpinnerSubSektorUsaha1 = new String[]{"Perdagangan", "Export", "Import", "Pembelian dan Pengumpulan Barang dalam Negeri", "Distribusi", "Pedagang Eceran", "Restoran", "Hotel", "Lainnya"};
        arraySpinnerSubSektorUsaha2 = new String[]{"Perumahan Sederhana Perumnas", "Perumahan Sederhana selain Perumnas", "Real Estate Lainnya", "Profesi Selain Dokter", "Leasing", "Lainnya"};
        arraySpinnerSubSektorUsaha3 = new String[]{"Hiburan & Kebudayaan", "Kesehatan", "Pendidikan", "Lainnya"};
        arraySpinnerSubSektorUsaha4 = new String[]{"Pertanian", "Tanaman Pangan", "Perkebunan", "Perikanan Laut", "Perikanan Darat", "Peternakan", "Kehutanan & Pemotongan Kayu", "Perburuan", "Sarana", "Lainnya"};
        arraySpinnerSubSektorUsaha5 = new String[]{"Industri", "Lainnya"};
        arraySpinnerSubSektorUsaha6 = new String[]{"Pengangkutan Umum", "Biro Perjalanan", "Pergudangan", "Komunikasi", "Lainnya"};
        arraySpinnerSubSektorUsaha7 = new String[]{"Konstruksi", "Lainnya"};
        arraySpinnerSubSektorUsaha8 = new String[]{"Pertambangan", "Lainnya"};
        arraySpinnerSubSektorUsaha9 = new String[]{"Listrik", "Gas", "Air", "Lainnya"};


        ArrayAdapter<String> adapterBertemuDengan = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerBertemuDengan);
        ArrayAdapter<String> adapterKeberadaanAset = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerKeberadaanAset);
        final ArrayAdapter<String> adapterSektorUsaha = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerSektorUsaha);
        ArrayAdapter<String> adapterJenisPetani = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arrayJenisPetani);
        ArrayAdapter<String> adapterKondisiJaminan = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arrayKondisiJaminan);
        ArrayAdapter<String> adapterAlamatNotelpEmail = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arrayAlamatNoTelpEmail);
        ArrayAdapter<String> adapterKepuasanNasabah = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arrayKepuasanNasabah);

        spinnerBertemuDenganNasabahMenuDetailVisit.setAdapter(adapterBertemuDengan);
        spinnerKeberadaanAsetNasabahMenuDetailVisit.setAdapter(adapterKeberadaanAset);

        final ArrayAdapter<String> adapterSubSektorUsaha1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerSubSektorUsaha1);
        final ArrayAdapter<String> adapterSubSektorUsaha2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerSubSektorUsaha2);
        final ArrayAdapter<String> adapterSubSektorUsaha3 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerSubSektorUsaha3);
        final ArrayAdapter<String> adapterSubSektorUsaha4 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerSubSektorUsaha4);
        final ArrayAdapter<String> adapterSubSektorUsaha5 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerSubSektorUsaha5);
        final ArrayAdapter<String> adapterSubSektorUsaha6 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerSubSektorUsaha6);
        final ArrayAdapter<String> adapterSubSektorUsaha7 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerSubSektorUsaha7);
        final ArrayAdapter<String> adapterSubSektorUsaha8 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerSubSektorUsaha8);
        final ArrayAdapter<String> adapterSubSektorUsaha9 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerSubSektorUsaha9);

        spinnerSektorUsahaMenuDetailVisit.setAdapter(adapterSektorUsaha);
        spinnerSektorUsahaMenuDetailVisit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinnerSektorUsahaMenuDetailVisit.getSelectedItem().toString().equals("Perdagangan")) {
                    System.out.println("Nah: " + spinnerSektorUsahaMenuDetailVisit.getSelectedItem().toString());
                    spinnerSubSektorUsahaMenuDetailVisit.setAdapter(adapterSubSektorUsaha1);
                } else if (spinnerSektorUsahaMenuDetailVisit.getSelectedItem().toString().equals("Jasa-Jasa Dunia Usaha")) {
                    System.out.println("Nah: " + spinnerSektorUsahaMenuDetailVisit.getSelectedItem().toString());
                    spinnerSubSektorUsahaMenuDetailVisit.setAdapter(adapterSubSektorUsaha2);
                } else if (spinnerSektorUsahaMenuDetailVisit.getSelectedItem().toString().equals("Jasa-Jasa Sosial Masyarakat")) {
                    spinnerSubSektorUsahaMenuDetailVisit.setAdapter(adapterSubSektorUsaha3);
                } else if (spinnerSektorUsahaMenuDetailVisit.getSelectedItem().toString().equals("Pertanian")) {
                    spinnerSubSektorUsahaMenuDetailVisit.setAdapter(adapterSubSektorUsaha4);
                } else if (spinnerSektorUsahaMenuDetailVisit.getSelectedItem().toString().equals("Industri")) {
                    spinnerSubSektorUsahaMenuDetailVisit.setAdapter(adapterSubSektorUsaha5);
                } else if (spinnerSektorUsahaMenuDetailVisit.getSelectedItem().toString().equals("Pengangkutan,Pergudangan dan Komunikasi")) {
                    spinnerSubSektorUsahaMenuDetailVisit.setAdapter(adapterSubSektorUsaha6);
                } else if (spinnerSektorUsahaMenuDetailVisit.getSelectedItem().toString().equals("Konstruksi")) {
                    spinnerSubSektorUsahaMenuDetailVisit.setAdapter(adapterSubSektorUsaha7);
                } else if (spinnerSektorUsahaMenuDetailVisit.getSelectedItem().toString().equals("Pertambangan")) {
                    spinnerSubSektorUsahaMenuDetailVisit.setAdapter(adapterSubSektorUsaha8);
                } else if (spinnerSektorUsahaMenuDetailVisit.getSelectedItem().toString().equals("Listrik Gas Air")) {
                    spinnerSubSektorUsahaMenuDetailVisit.setAdapter(adapterSubSektorUsaha9);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        spinnerJenisPetaniMenuDetailVisit.setAdapter(adapterJenisPetani);
        spinnerKondisiJaminanMenuDetailVisit.setAdapter(adapterKondisiJaminan);

        spinnerAlamatNasabahMenuDetailVisit.setAdapter(adapterAlamatNotelpEmail);
        spinnerAlamatUsahaNasabahMenuDetailVisit.setAdapter(adapterAlamatNotelpEmail);
        spinnerNoTelpNasabahMenuDetailVisit.setAdapter(adapterAlamatNotelpEmail);
        spinnerEmailNasabahMenuDetailVisit.setAdapter(adapterAlamatNotelpEmail);
        spinnerKepuasanNasabahMenuDetailVisit.setAdapter(adapterKepuasanNasabah);

        etNamaNasabahMenuDetailVisit.setText(datadp.getNama());
        etAlamatnasabahMenuDetailVisit.setText(dataDetInv.getAlmt_rumah());
        etAlamatUsahaNasabahMenuDetailVisit.setText(dataDetInv.getAlmt_usaha());
        etNomorHPNasabahMenuDetailVisit.setText(dataDetInv.getNo_hp());
        if (dataDetInv.getEmail().equals("[text]")) {
            etAlamatEmailNasabahMenuDetailVisit.setText("");
        } else {
            etAlamatEmailNasabahMenuDetailVisit.setText(dataDetInv.getEmail());
        }


        etPekerjaanNasabahMenuDetailVisit.setText(dataDetInv.getPekerjaan());
        etTglGajiannNasabahMenuDetailVisit.setText(dataDetInv.getTgl_gajian());
        etTglGajiannNasabahMenuDetailVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                DialogFragment dialogFragment = new DatePickerDialogClass();
//                dialogFragment.show(getFragmentManager(), "Date Picker Dialog");

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MenuDetailVisitSurvey.this, android.R.layout.simple_list_item_1, listTanggal);
                AlertDialog.Builder builder = new AlertDialog.Builder(MenuDetailVisitSurvey.this);
                builder.setTitle("Tanggal Gajian Nasabah");
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strName = arrayAdapter.getItem(i);
                        etTglGajiannNasabahMenuDetailVisit.setText(strName);
                    }
                });
                builder.show();
            }
        });


        etAlamatNasabah1.setText(dataDetInv.getAlmt_rumah());
        etAlamatUsahaNasabah1.setText(dataDetInv.getAlmt_usaha());
        etNoTelp1.setText(dataDetInv.getNo_hp());

        if (dataDetInv.getEmail().equals("[text]")) {
            etEmailNasabah1.setText("");
        } else {
            etEmailNasabah1.setText(dataDetInv.getEmail());
        }


        btnSubmitMenuDetailVisit = (Button) findViewById(R.id.btnSubmitMenuDetailVisit);
        btnSubmitMenuDetailVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Function.showAlert(mContext, "Submit Data Berhasil");
//                Intent i = new Intent(mContext, MenuUtama.class);
//                startActivity(i);
//                finish();

                //Toast(mContext, etTglGajiannNasabahMenuDetailVisit.getText().toString());
                if (spinnerAlamatNasabahMenuDetailVisit.getSelectedItem().toString().equals(Constant.SESUAI)) {
//                    Toast(mContext,Constant.SESUAI);
//                    System.out.println(Constant.SESUAI);
                    alamat_baru = etAlamatnasabahMenuDetailVisit.getText().toString();
                } else if (spinnerAlamatNasabahMenuDetailVisit.getSelectedItem().toString().equals(Constant.TIDAK)) {
//                    Toast(mContext,Constant.TIDAK);
//                    System.out.println(Constant.TIDAK);
                    alamat_baru = etAlamatNasabah1.getText().toString();
                }
                if (spinnerAlamatUsahaNasabahMenuDetailVisit.getSelectedItem().toString().equals(Constant.SESUAI)) {
                    alamat_usaha = etAlamatUsahaNasabah1.getText().toString();
                } else if (spinnerAlamatUsahaNasabahMenuDetailVisit.getSelectedItem().toString().equals(Constant.TIDAK)) {
                    alamat_usaha = etAlamatUsahaBaruNasabah1.getText().toString();
                }
                if (spinnerNoTelpNasabahMenuDetailVisit.getSelectedItem().toString().equals(Constant.SESUAI)) {
                    no_hp = etNoTelp1.getText().toString();
                } else if (spinnerNoTelpNasabahMenuDetailVisit.getSelectedItem().toString().equals(Constant.TIDAK)) {
                    no_hp = etNoTelpBaru1.getText().toString();
                }
                if (spinnerEmailNasabahMenuDetailVisit.getSelectedItem().toString().equals(Constant.SESUAI)) {
                    email = etEmailNasabah1.getText().toString();
                } else if (spinnerEmailNasabahMenuDetailVisit.getSelectedItem().toString().equals(Constant.TIDAK)) {
                    email = etEmailNasabahBaru1.getText().toString();
                }

                tlpn_usaha = no_hp;
                bertemu_dengan = spinnerBertemuDenganNasabahMenuDetailVisit.getSelectedItem().toString();
                sesuai_alamat = spinnerAlamatNasabahMenuDetailVisit.getSelectedItem().toString();
                kepuasan_nasabah = spinnerKepuasanNasabahMenuDetailVisit.getSelectedItem().toString();
                pekerjaan_nasabah = etPekerjaanNasabahMenuDetailVisit.getText().toString();
                sesuai_no_tlpn = spinnerNoTelpNasabahMenuDetailVisit.getSelectedItem().toString();
                keberadaan_aset = spinnerKeberadaanAsetNasabahMenuDetailVisit.getSelectedItem().toString();
                tgl_gajian_nasabah = etTglGajiannNasabahMenuDetailVisit.getText().toString();
                sesuai_email = spinnerEmailNasabahMenuDetailVisit.getSelectedItem().toString();
                nama_nasabah = etNamaNasabahMenuDetailVisit.getText().toString();

//                shared = getApplicationContext().getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
//
//                SharedPreferences.Editor editor = shared.edit();
//                editor.putString(Constant.SHARED_USERNAME, "19655");
//                editor.putString(Constant.SHARED_PASSWORD, "527219");
//                editor.putString("nama", "Yurwanto");
//                editor.putString("nik", "00000019");
//                editor.commit();


                shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                usernameShared = (shared.getString(Constant.SHARED_USERNAME, ""));
                passwordShared = (shared.getString(Constant.SHARED_PASSWORD, ""));
                nikShared = (shared.getString(Constant.NIK, ""));
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Konfirmasi Data");
                builder.setMessage("Bertemu Dengan: \n" + bertemu_dengan + "\nKeberadaan Aset: \n" + keberadaan_aset + "\nPekerjaan Nasabah: \n" + pekerjaan_nasabah + "\nTanggal Gajian Nasabah: \n" + tgl_gajian_nasabah + "\nSektor Usaha: \n" + spinnerSektorUsahaMenuDetailVisit.getSelectedItem().toString() + "\nSub Sektor Usaha: \n" + spinnerSubSektorUsahaMenuDetailVisit.getSelectedItem().toString() + "\nSektor Usaha Versi Lapangan: \n" + etSektorUsahaMenuDetailVisit.getText().toString() + "\nJenis Petani: \n" + spinnerJenisPetaniMenuDetailVisit.getSelectedItem().toString() + "\nKondisi Jaminan: \n" + spinnerKondisiJaminanMenuDetailVisit.getSelectedItem().toString() + "\nAlamat Nasabah: \n" + alamat_baru + "\nAlamat Usaha: \n" + alamat_usaha + "\nTelepon Usaha: \n" + tlpn_usaha + "\nEmail: \n" + email);
                builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        new exechasilSurvey(mContext).execute();
                    }
                });
                builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });
    }

    public static void longLog(String str) {
        if (str.length() > 4000) {
            Log.d("ini contoh json", str.substring(0, 4000));
            //System.out.println("return jsonnya: "+str.substring(0, 4000));
            longLog(str.substring(4000));
        } else
            Log.d("ini contoh json", str);
        //System.out.println("return jsonnya: "+str);
    }

    @Override
    public void onComplete(String value) {
        passwordShared = Function.convertStringtoMD5(value);
        //Toast.makeText(mContext,value,Toast.LENGTH_SHORT).show();
        new exechasilSurvey(mContext).execute();

    }

    class exechasilSurvey extends AsyncTask<String, JSONObject, String> {

        Context context;

        public exechasilSurvey(Context mContext) {
            this.context = mContext;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Mengirim Data...");
            progressDialog.setTitle("Pesan");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result.contains(Constant.CONNECTION_LOST)) {
                //Function.showAlert(mContext, Constant.CONNECTION_LOST);


                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Pesan");
                builder.setMessage("Gagal Mengirim Karena Gangguan Koneksi. Data akan dikirim kembali saat jaringan tersedia");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        dataSettleDetil = new Settle_Detil();
//                        dataSettleDetil.setNo_loan(noloan);
//                        dataSettleDetil.setNo_rek(dataDetInv.getNo_rekening());
//                        dataSettleDetil.setNama(dataDetInv.getNama_debitur());
//                        dataSettleDetil.setNominal_bayar("");
//                        dataSettleDetil.setTotal_kewajiban(dataDetInv.getTotal_kewajiban());
//                        dataSettleDetil.setId_detail_inventory(dataDetInv.getId_detail_inventory());
//                        dataSettleDetil.setStatus("0");
//                        dataSettleDetil.setSisa_bayar(String.valueOf(Double.parseDouble(dataDetInv.getTotal_kewajiban()) - Double.parseDouble(NumberTextWatcherForThousand.trimCommaOfString(nominalygDibayar))));

                        try {
                            JSONObject jsonupdate = new JSONObject();
                            jsonupdate.put("no_loan", noloan);
                            jsonupdate.put("no_rek", dataDetInv.getNo_rekening());
                            jsonupdate.put("nama", dataDetInv.getNama_debitur());
                            jsonupdate.put("nominal_bayar", "");
                            jsonupdate.put("total_kewajiban", dataDetInv.getTotal_kewajiban());
                            jsonupdate.put("id_det_inv", dataDetInv.getId_detail_inventory());
                            jsonupdate.put("status", 0);
                            jsonupdate.put("kode_kunjungan", "visit");
                            jsonupdate.put("unique_code", unique_code);
                            if (querySettle_detil.cekUniqueCodeAvailable(unique_code).equals("kosong")) {
                                dataPending = new Pending();
                                dataPending.setWaktu(Function.getDateNow());
                                dataPending.setData(jsonStr);
                                dataPending.setUrl(Constant.SERVICE_SURVEY);
                                dataPending.setHeader(new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));
                                dataPending.setStatus("2");
                                dataPending.setJson_update(jsonupdate.toString());
                                dataPending.setKet_update("Visit");
                                queryPending.insertPendingData(dataPending);
                                queryPhoto.updateStatusPhoto(noloan);
                            } else if (querySettle_detil.cekUniqueCodeAvailable(unique_code).equals("ada")) {
                                Function.showAlert(mContext, "Data Sudah Pernah Dikirim");
                            }
                            Intent i = new Intent(mContext, MenuUtama.class);
                            startActivity(i);
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                });
                builder.setCancelable(false);
                AlertDialog alert1 = builder.create();
                alert1.show();


            } else if (result.contains(Constant.CONNECTION_ERROR)) {
                Function.showAlert(mContext, Constant.CONNECTION_ERROR);
            } else if (result.isEmpty()) {
                Function.showAlert(mContext, Constant.CONNECTION_EMPTY);
            } else if (result == null) {
                Function.showAlert(mContext, Constant.CONNECTION_EMPTY);
            } else {
                try {
                    try {
                        JSONObject jsonR = new JSONObject(result);
                        String ket = jsonR.getString("keterangan");
                        String rc = jsonR.getString("rc");
                        if (querySettle_detil.cekUniqueCodeAvailable(unique_code).equals("kosong")) {
                            if (rc.equals("00")) {
                                // Function.showAlert(mContext, "Submit Berhasil");
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Pesan");
                                builder.setMessage("Submit Berhasil");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        dataDDP.setId_data_pencairan(datadp.getId_data_pencairan());
                                        dataDDP.setNo_loan(noloan);
                                        dataDDP.setTgl_kunjungan(Function.getDateNow());
                                        dataDDP.setStatus("1");
                                        queryDDP.insertDetilDataPencairan(dataDDP);

                                        queryDetInv.updateStatusDataPencairan(noloan);
                                        queryPhoto.updateStatusPhoto(noloan);

                                        shared = getApplicationContext().getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                                        SharedPreferences.Editor editor = shared.edit();
                                        editor.putString(Constant.SHARED_PASSWORD, passwordShared);
                                        editor.commit();

                                        Intent i = new Intent(mContext, MenuUtama.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });

                                AlertDialog alert1 = builder.create();
                                alert1.show();

                            } else if (rc.equals("T1")) {
                                //Function.showAlert(mContext, keterangan);
//                            DialogNewPassword dialog1 = DialogNewPassword.newInstance();
//                            dialog1.show(getFragmentManager(), "MyDialog");
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Pesan");
                                builder.setMessage(ket);
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                    shared.edit().clear().commit();
                                        Intent i = new Intent(MenuDetailVisitSurvey.this, MenuLogin.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });

                                AlertDialog alert1 = builder.create();
                                alert1.show();

                            } else if (rc.equals("T2")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Pesan");
                                builder.setMessage(ket);
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                    shared.edit().clear().commit();
                                        Intent i = new Intent(MenuDetailVisitSurvey.this, MenuLogin.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });

                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            } else if (rc.equals("T3")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Pesan");
                                builder.setMessage(ket);
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                    shared.edit().clear().commit();
                                        Intent i = new Intent(MenuDetailVisitSurvey.this, MenuLogin.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });

                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            } else if (rc.equals("T4")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Pesan");
                                builder.setMessage(ket);
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                    shared.edit().clear().commit();
                                        Intent i = new Intent(MenuDetailVisitSurvey.this, MenuLogin.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });

                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            } else if (rc.equals("T5")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Pesan");
                                builder.setMessage(ket);
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                    shared.edit().clear().commit();
                                        Intent i = new Intent(MenuDetailVisitSurvey.this, MenuLogin.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });
                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            } else {
                                Function.showAlert(mContext, ket);
                            }
                        } else {
                            Function.showAlert(mContext, "Data Sudah Pernah Dikirim");
                        }

                    } catch (JSONException e) {
                        Function.showAlert(mContext, e.getMessage());
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    Function.showAlert(mContext, e.getMessage());
                    e.printStackTrace();
                }
            }


        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            JSONObject json1 = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            JSONArray jsonArrayImage = new JSONArray();
            JSONObject jsonAll = new JSONObject();
            try {

                querySSL = new ServiceSendLocation(context);
                location = querySSL.getLocation();

                listPhoto = queryPhoto.getimageByNoLoan(noloan);

                for (Photo p : listPhoto) {
                    json = new JSONObject();
                    String ket = p.getKeterangan();
                    String url = p.getUrl_photo();
                    String waktu = p.getWaktu();
                    if (ket == null) {
                        ket = "";
                    }
                    if (url == null) {
                        url = "";
                    }
                    if (waktu == null) {
                        waktu = "";
                    }
                    json.put("keterangan", ket);
                    // json.put("img", "");
                    json.put("img", Function.bitmapToString(Function.bitmapFromPath(url)));
                    json.put("waktu-kunjungan", waktu);
                    jsonArrayImage.put(json);
                }
                unique_code = Function.generateRandomCode();

                json1.put("no_loan", noloan);
                json1.put("alamat_baru", alamat_baru);
                json1.put("nama_nasabah", nama_nasabah);
                json1.put("sesuai_alamat_usaha", sesuai_alamat_usaha);
                json1.put("tlpn_usaha", tlpn_usaha);
                json1.put("bertemu_dengan", bertemu_dengan);
                json1.put("sesuai_alamat", sesuai_alamat);
                json1.put("no_hp", no_hp);
                json1.put("upload_doc", upload_doc);
                json1.put("kepuasan_nasabah", kepuasan_nasabah);
                json1.put("alamat_usaha", alamat_usaha);
                json1.put("pekerjaan_nasabah", pekerjaan_nasabah);
                json1.put("unique_code", unique_code);
                json1.put("sesuai_no_tlpn", sesuai_no_tlpn);
                json1.put("alamat_nasabah", alamat_nasabah);
                json1.put("email", email);
                json1.put("keberadaan_aset", keberadaan_aset);
                json1.put("tgl_gajian_nasabah", tgl_gajian_nasabah);
                json1.put("alamat_usaha_baru", alamat_usaha_baru);
                json1.put("sesuai_email", sesuai_email);

                if (location == null) {
                    latitude = 0.0;
                    longitude = 0.0;
                    location = querySSL.getLastKnownLocation();
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                } else {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
                json1.put("latitude", latitude);
                json1.put("longitude", longitude);

//                json1.put("latitude", querySSL.getLatitude());
//                json1.put("longitude", querySSL.getLongitude());


                json1.put("images", jsonArrayImage);
                //jsonArray.put(json);
                jsonArray.put(json1);
                // jsonArrayImage.put(json);

                jsonAll.put("waktu", Function.getWaktuSekarangMilis());
                jsonAll.put("username", usernameShared);
                jsonAll.put("password", passwordShared);
                jsonAll.put("hasil_survey", jsonArray);
                //jsonAll.put("cek_dok", jsonArrayImage);


                jsonStr = jsonAll.toString();
                System.out.println("HASIL JSON: " + jsonStr);


                longLog(jsonStr);
                //String nik = "C001234";


                System.out.println("Header Base 64: " + new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));
                resultfromJson = jsonParser.HttpRequestPost(Constant.SERVICE_SURVEY, jsonStr, Constant.TimeOutConnection, new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));
                System.out.println("RETURN JSON: " + resultfromJson);


                longLog(resultfromJson);


//                String nik = "C001234";
                // System.out.println("Header Base 64: " + new String(Function.encodeBase64(Function.getHeader(mContext, nik))));
//                resultfromJson = jsonParser.HttpRequestPost(Constant.SERVICE_HASIL_KUNJUNGAN, jsonStr, Constant.TimeOutConnection, new String(Function.encodeBase64(Function.getHeader(mContext, nik))));
//                System.out.println("RETURN JSON: " + resultfromJson);


                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static class DatePickerDialogClass extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            //TODO Datepicker Tahun Hilang
//            Locale locale = new Locale("in", "ID");
//            Locale.setDefault(locale);
//            final Calendar calendar = Calendar.getInstance();
//            int year = calendar.get(Calendar.YEAR);
//            int month = calendar.get(Calendar.MONTH);
//            int day = calendar.get(Calendar.DAY_OF_MONTH);
//
//            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
//                    AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
//
//            datepickerdialog.getDatePicker().findViewById(getResources().getIdentifier("year", "id", "android")).setVisibility(View.GONE);
//            return datepickerdialog;


            Locale locale = new Locale("in", "ID");
            Locale.setDefault(locale);
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            Date today = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(today);
            Calendar cMax = Calendar.getInstance();
            cMax.setTime(today);
            cMax.add(Calendar.DAY_OF_YEAR, +5); // Subtract 6 months
            long minDate = c.getTime().getTime(); // Twice!
            long maxDate = cMax.getTime().getTime();


            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
            datepickerdialog.getDatePicker().setMinDate(minDate);
            return datepickerdialog;


        }

        //        public void onDateSet(DatePicker view, int year, int month, int day) {
//            etTglGajiannNasabahMenuDetailVisit.setText(day + "-" + (month + 1));
//        }
        public void onDateSet(DatePicker view, int year, int month, int day) {
            etTglGajiannNasabahMenuDetailVisit.setText(day + "-" + (month + 1) + "-" + year);
        }
    }

    @Override
    public void onBackPressed() {

    }
}
