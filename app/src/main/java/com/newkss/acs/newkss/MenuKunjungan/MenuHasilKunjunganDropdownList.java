package com.newkss.acs.newkss.MenuKunjungan;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;

import com.newkss.acs.newkss.Menu.MenuInisialisasi;
import com.newkss.acs.newkss.Menu.MenuListBucketSelected;
import com.newkss.acs.newkss.Menu.MenuLogin;
import com.newkss.acs.newkss.Menu.MenuPerformance;
import com.newkss.acs.newkss.Menu.MenuUtama;
import com.newkss.acs.newkss.MenuPhoto.ListPhoto;
import com.newkss.acs.newkss.MenuPhoto.MenuTakePhoto;
import com.newkss.acs.newkss.Model.Detail_Bucket;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.Pending;
import com.newkss.acs.newkss.Model.Photo;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.Model.Settlement;
import com.newkss.acs.newkss.ModelBaru.TGT;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Service.ServiceSendLocation;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.DialogNewPassword;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.JSONParser;
import com.newkss.acs.newkss.Util.MySQLiteHelper;
import com.newkss.acs.newkss.Util.NumberTextWatcherForThousand;
import com.newkss.acs.newkss.Util.NumberTextWathcerSettlement;
import com.newkss.acs.newkss.Util.OnCompleteListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by acs on 02/05/17.
 */

public class MenuHasilKunjunganDropdownList extends ParentActivity implements OnCompleteListener {
    Spinner spinnerAlasanTidakBertemuNasabah, spinnerEmailMenuHasilKunjungan, spinnerNoTelpNasabahMenuHasilKunjungan, spinnerAlamatUsahaNasabah, spinnerAlamatNasabah, spinnerNasabahBayar, spinnerBertemuDengan, spinnerJikaBertemuSelainNasabah, spinnerLokasiBertemu, spinnerKarakterNasabah, spinnerBuktiPembayaran;
    private String[] arrayAlasanTidakBertemuNasabah, arrayAlasanTelatBayar, arrayBertemuNasabahTetapiTidakBayarFull, arrayAlasanKwitansi, arrayLokasiBertemu, arrayKarakterNasabah, arrayBuktiPembayaran, arraySpinner, arraySpinnerBayar, arraySpinnerBertemuDengan, arraySpinnerJikaBertemuSelainNasabah;

    EditText etemailBaruMenuHasilKunjungan, etNoTelpBaruNasabahMenuHasilKunjungan, etAlamatBaruNasabahMenuHasilKunjungan, etAlamatUsahaBaruNasabah;
    ImageView imgBackMenuHasilKunjungan;

    Button btnUploadDocMenuHasilKunjungan, btnUploadPhotoMenuHasilKunjungan, btnSubmitMenuHasilKunjungan;

    Detail_Bucket d = new Detail_Bucket();

    private Uri uriSavedImage;
    String path;
    String filename;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    String idDetailBucket = "";
    Photo ph, photodata;
    LinearLayout llNasabahBayarMenuHasilKunjungan, llBuktiPembayaranMenuHasilKunjungan;

    ProgressDialog progressDialog;

    Context mContext;
    JSONParser jsonParser = new JSONParser();
    String resultfromJson;

    String namaDebitur, alamatNasabah, alamatUsahaNasabah, noTelp, email;
    String kewajibanygDibayar, nominalygDibayar, tglJanjiBayar, nominalJanjiBayar;
    String alasanTelatBayar, bertemuDengan, jikaBertemuSelainNasabah, lokasiBertemu, waktuSurvey, karakterNasabah, resumeSingkatNasabah, buktiPembayaranBy, alasanPenggunaanKwitansi, noSlipKwitansi;
    EditText etAlasanTelatBayarMenuHasilKunjungan, etAlasanPenggunaanKwitansi, etInputNoSlipKwitansi, etNominalyangHarusDibayarMenuHasilKunjungan, etNoLoanMenuHasilKunjungan, etResumeSingkatNasabahMenuHasilKunjungan, etWaktuSurveyMenuHasilKunjungan, etNominalJanjiBayarMenuHasilKunjungan, etKewajibanYangHarusDibayarMenuHasilKunjungan, etNamaDebiturMenuHasilKunjungan, etAlamatNasabahMenuHasilKunjungan, etAlamatUsahaNasabah, etNoTelpNasabahMenuHasilKunjungan, etemailMenuHasilKunjungan;
    private static EditText etTglJanjiBayarMenuHasilKunjungan;
    String sesuaiAlamatUsaha, sesuaiAlamatNasabah, sesuaiEmail, nasabahBayar;
    String iddetinv;

    TGT querydp = new TGT();
    TGT datadp = new TGT();
    Detail_Inventory queryDetInv = new Detail_Inventory();
    Detail_Inventory dataDetInv = new Detail_Inventory();
    SharedPreferences shared;
    String nikShared, noloan;

    Photo queryPhoto = new Photo();
    Photo dataPhoto = new Photo();
    ArrayList<Photo> listPhoto = new ArrayList<>();

    String usernameShared, passwordShared;
//    Settlement querySettlement = new Settlement();
//    Settlement dataSettlement = new Settlement();

    Settle_Detil querySettleDetil = new Settle_Detil();
    Settle_Detil dataSettleDetil;

    ServiceSendLocation querySSL;

    TextView txtNamaMenuHasilKunjungan, txtAlasanTelatBayar, txtDivisiMenuHasilKunjungan;
    String namaShared, divisiShared;

    Pending queryPending = new Pending();
    Pending dataPending;
    String jsonStr;

    LinearLayout llMenuHasilKunjunganPopup;
    TextView txtStatusPayment;
    LinearLayout llJanjiBayar;

    TextView txtTglJanjiBayar, txtNominalJanjiBayar, txtJikaBertemuSelainNasabah;


    AutoCompleteTextView autoTxt, autoTxtKarakterNasabah, autoTxtAlasanTelatBayar;
    TextView txtJikaBertemuNasabahTapiTidakBayarFull;
    TextView txtAlasanPenggunaanKwitansi;

    String bertemuNasabahTetapiTidakBayarFull = "";
    String alasanTidakBertemuNasabah = "";
    TextView txtAlasanTidakBertemuNasabah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuhasilkunjungan);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = this;
        querySSL = new ServiceSendLocation(mContext);
        initUI();
    }

    public static class DatePickerDialogClass extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Locale locale = new Locale("in", "ID");
            Locale.setDefault(locale);
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
            return datepickerdialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            etTglJanjiBayarMenuHasilKunjungan.setText(day + "-" + (month + 1) + "-" + year);
        }
    }


    private void initUI() {
        autoTxt = (AutoCompleteTextView) findViewById(R.id.autoTxt);
        autoTxtKarakterNasabah = (AutoCompleteTextView) findViewById(R.id.autoTxtKarakterNasabah);
        autoTxtAlasanTelatBayar = (AutoCompleteTextView) findViewById(R.id.autoTxtAlasanTelatBayar);
        txtAlasanPenggunaanKwitansi = (TextView) findViewById(R.id.txtAlasanPenggunaanKwitansi);
        // d = (Detail_Bucket) getIntent().getParcelableExtra("detailbucket");
        MySQLiteHelper.initDB(mContext);
//        d.setId_detail_bucket("DB-0000007");
//        Detail_Inventory di = new Detail_Inventory();
//        Detail_Inventory di1 = new Detail_Inventory();
//        di1 = di.getDetInventoryFromIdDetailBucket(d.getId_detail_bucket());

        Intent in = getIntent();
        // iddetinv = "DI-0000001";
        iddetinv = in.getStringExtra("id");
        String iddp = queryDetInv.getIdTGTFromIdDetInv(iddetinv);
        dataDetInv = queryDetInv.getDetInventoryFromIdDetInv(iddetinv);
        datadp = querydp.getTGTFromID(iddp);
        noloan = queryDetInv.getNoLoanfromIdDetInv(iddetinv);

        txtStatusPayment = (TextView) findViewById(R.id.txtStatusPayment);
        txtJikaBertemuNasabahTapiTidakBayarFull = (TextView) findViewById(R.id.txtJikaBertemuNasabahTapiTidakBayarFull);

        llJanjiBayar = (LinearLayout) findViewById(R.id.llJanjiBayar);

        //txtJikaBertemuSelainNasabah = (TextView) findViewById(R.id.txtJikaBertemuSelainNasabah);


        llMenuHasilKunjunganPopup = (LinearLayout) findViewById(R.id.llMenuHasilKunjunganPopup);
        llMenuHasilKunjunganPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MenuHasilKunjunganDropdownList.this, llMenuHasilKunjunganPopup);
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_REFRESH)) {
                            initUI();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            Intent i = new Intent(getApplicationContext(), MenuLogin.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });
        imgBackMenuHasilKunjungan = (ImageView) findViewById(R.id.imgBackMenuHasilKunjungan);
        imgBackMenuHasilKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txtDivisiMenuHasilKunjungan = (TextView) findViewById(R.id.txtDivisiMenuHasilKunjungan);
        txtNominalJanjiBayar = (TextView) findViewById(R.id.txtNominalJanjiBayar);
        txtTglJanjiBayar = (TextView) findViewById(R.id.txtTglJanjiBayar);
        txtAlasanTidakBertemuNasabah = (TextView) findViewById(R.id.txtAlasanTidakBertemuNasabah);

        txtAlasanTelatBayar = (TextView) findViewById(R.id.txtAlasanTelatBayar);
//        txtAlasanTelatBayar.setVisibility(View.GONE);
        txtNamaMenuHasilKunjungan = (TextView) findViewById(R.id.txtNamaMenuHasilKunjungan);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtNamaMenuHasilKunjungan.setText(namaShared);
            txtDivisiMenuHasilKunjungan.setText(divisiShared);
        }


        llNasabahBayarMenuHasilKunjungan = (LinearLayout) findViewById(R.id.llNasabahBayarMenuHasilKunjungan);
        llBuktiPembayaranMenuHasilKunjungan = (LinearLayout) findViewById(R.id.llBuktiPembayaranMenuHasilKunjungan);

        arraySpinner = new String[]{Constant.SESUAI, Constant.TIDAK};
        arraySpinnerBayar = new String[]{"Ya", "Tidak"};
//        arraySpinnerBertemuDengan = new String[]{"Nasabah", "Tetangga", "Saudara", "Karyawan"};
//        arraySpinnerBertemuDengan = new String[]{"Nasabah", "Keluarga", "Tetangga", "Karyawan"};
        arraySpinnerBertemuDengan = new String[]{"Nasabah", "Pasangan Nasabah", "Keluarga 1 Rumah", "Keluarga Beda Rumah", "Karyawan/Pegawai/Rekan Kerja", "Tetangga", "Aparat Pemerintah Setempat/POLRI/TNI", "Pejabat KPNKL/Pengadilan", "Calon Pembeli/Funder", "Penadah"};
//        arraySpinnerJikaBertemuSelainNasabah = new String[]{"Anak", "Anak Tetangga", "Karyawan"};
        arraySpinnerJikaBertemuSelainNasabah = new String[]{"Keluarga", "Tetangga", "Karyawan"};
        arrayLokasiBertemu = new String[]{"Rumah", "Tempat Usaha/Kantor", "KPNKL", "Pengadilan", "Kantor Pemerintah/POLRI/TNI"};
        arrayKarakterNasabah = new String[]{"Kooperatif, Bisa Ditemui dan ditelepon", "Kooperatif, Susah Ditemui Tapi Bisa Ditelepon", "Cukup Kooperatif, Tidak Bisa Ditemui Tapi Bisa Ditelepon", "Cukup Kooperatif, Bisa Ditemui Tapi Tidak Bisa Ditelepon", "Tidak Kooperatif"};
        arrayBuktiPembayaran = new String[]{"Sms/Email", "Slip Kwitansi"};
        arrayAlasanKwitansi = new String[]{"Tidak ada sinyal", "Nasabah Memaksa pakai slip kwitansi"};

        arrayAlasanTidakBertemuNasabah = new String[]{"Rumah Kosong", "Pindah", "Fraud", "Skip", "Security", "Titip Pesan"};
        arrayAlasanTelatBayar = new String[]{"Tanggal Cycle Tidak Sesuai Dengan Tanggal Gaji", "Musibah", "Kesulitan Keuangan", "Pembayaran Oleh Pihak Lain"};
        arrayBertemuNasabahTetapiTidakBayarFull = new String[]{"Nasabah Minta Pickup", "Nasabah Setor Sendiri Ke Kantor", "Nasabah Setor Sendiri Ke Alfa", "Nasabah Transfer Virtual BSS", "Kunjungan Lagi"};

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_dropdown_item, arrayBertemuNasabahTetapiTidakBayarFull);
        autoTxt.setAdapter(adapter1);
        autoTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                autoTxt.showDropDown();

            }
        });

        ArrayAdapter<String> adapterAutoTextAlasanTelatBayar = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_dropdown_item, arrayAlasanTelatBayar);
        autoTxtAlasanTelatBayar.setAdapter(adapterAutoTextAlasanTelatBayar);
        autoTxtAlasanTelatBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                autoTxtAlasanTelatBayar.showDropDown();
            }
        });


        ArrayAdapter<String> adapterAutoTextKarakterNasabah = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_dropdown_item, arrayKarakterNasabah);
        autoTxtKarakterNasabah.setAdapter(adapterAutoTextKarakterNasabah);
        autoTxtKarakterNasabah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                autoTxtKarakterNasabah.showDropDown();
            }
        });


        autoTxtAlasanTelatBayar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println("autoTxtAlasanTelatBayar On itemclick " + autoTxtAlasanTelatBayar.getText().toString());
            }
        });
        autoTxt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println("On itemclick " + autoTxt.getText().toString());
            }
        });

        autoTxtKarakterNasabah.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println("autoTxtKarakterNasabah setOnItemClickListener " + autoTxtKarakterNasabah.getText().toString());
            }
        });
//        autoTxt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast(mContext,adapterView.getSelectedItem().toString());
//                System.out.println(adapterView.getSelectedItem().toString());
//                System.out.println(autoTxt.getText().toString());
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

//        etAlasanTelatBayarMenuHasilKunjungan = (EditText) findViewById(R.id.etAlasanTelatBayarMenuHasilKunjungan);
//        etAlasanTelatBayarMenuHasilKunjungan.setVisibility(View.GONE);

        etInputNoSlipKwitansi = (EditText) findViewById(R.id.etInputNoSlipKwitansi);
        etAlasanPenggunaanKwitansi = (EditText) findViewById(R.id.etAlasanPenggunaanKwitansi);

        etNoLoanMenuHasilKunjungan = (EditText) findViewById(R.id.etNoLoanMenuHasilKunjungan);
        etNamaDebiturMenuHasilKunjungan = (EditText) findViewById(R.id.etNamaDebiturMenuHasilKunjungan);

        etResumeSingkatNasabahMenuHasilKunjungan = (EditText) findViewById(R.id.etResumeSingkatNasabahMenuHasilKunjungan);
        // etWaktuSurveyMenuHasilKunjungan = (EditText) findViewById(R.id.etWaktuSurveyMenuHasilKunjungan);
        etNominalJanjiBayarMenuHasilKunjungan = (EditText) findViewById(R.id.etNominalJanjiBayarMenuHasilKunjungan);
        etTglJanjiBayarMenuHasilKunjungan = (EditText) findViewById(R.id.etTglJanjiBayarMenuHasilKunjungan);

        etKewajibanYangHarusDibayarMenuHasilKunjungan = (EditText) findViewById(R.id.etKewajibanYangHarusDibayarMenuHasilKunjungan);

        etAlamatBaruNasabahMenuHasilKunjungan = (EditText) findViewById(R.id.etAlamatBaruNasabahMenuHasilKunjungan);
        etAlamatUsahaBaruNasabah = (EditText) findViewById(R.id.etAlamatUsahaBaruNasabah);
        etNoTelpBaruNasabahMenuHasilKunjungan = (EditText) findViewById(R.id.etNoTelpBaruNasabahMenuHasilKunjungan);
        etemailBaruMenuHasilKunjungan = (EditText) findViewById(R.id.etemailBaruMenuHasilKunjungan);

        etAlamatNasabahMenuHasilKunjungan = (EditText) findViewById(R.id.etAlamatNasabahMenuHasilKunjungan);
        etAlamatUsahaNasabah = (EditText) findViewById(R.id.etAlamatUsahaNasabah);
        etNoTelpNasabahMenuHasilKunjungan = (EditText) findViewById(R.id.etNoTelpNasabahMenuHasilKunjungan);
        etemailMenuHasilKunjungan = (EditText) findViewById(R.id.etemailMenuHasilKunjungan);

        etNominalyangHarusDibayarMenuHasilKunjungan = (EditText) findViewById(R.id.etNominalyangHarusDibayarMenuHasilKunjungan);
        etNominalyangHarusDibayarMenuHasilKunjungan.addTextChangedListener(new NumberTextWatcherForThousand(etNominalyangHarusDibayarMenuHasilKunjungan));
        etNominalyangHarusDibayarMenuHasilKunjungan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    String kwjb = etKewajibanYangHarusDibayarMenuHasilKunjungan.getText().toString();
                    String val = etNominalyangHarusDibayarMenuHasilKunjungan.getText().toString();
                    if (val.isEmpty()) {
                        val = "0";
                    }
                    System.out.println("Kewajiban " + NumberTextWatcherForThousand.trimCommaOfString(kwjb));
                    System.out.println("Nominal " + NumberTextWatcherForThousand.trimCommaOfString(val));

                    if (Double.parseDouble(Function.removeSeparatorComa(val)) >= Double.parseDouble(Function.removeSeparatorComa(kwjb))) {
                        txtStatusPayment.setText("Full Payment");
                        System.out.println("Text Changed Full Payment");
                        txtTglJanjiBayar.setVisibility(View.GONE);
                        txtNominalJanjiBayar.setVisibility(View.GONE);
                        etNominalJanjiBayarMenuHasilKunjungan.setVisibility(View.GONE);
                        etTglJanjiBayarMenuHasilKunjungan.setVisibility(View.GONE);

                        txtJikaBertemuNasabahTapiTidakBayarFull.setVisibility(View.GONE);
                        autoTxt.setVisibility(View.GONE);
//                        llJanjiBayar.setVisibility(View.GONE);
//                        llNasabahBayarMenuHasilKunjungan.setVisibility(View.VISIBLE);
//                        llBuktiPembayaranMenuHasilKunjungan.setVisibility(View.VISIBLE);
//                        etAlasanTelatBayarMenuHasilKunjungan.setVisibility(View.GONE);
//                        txtAlasanTelatBayar.setVisibility(View.GONE);

//                        etTglJanjiBayarMenuHasilKunjungan.setVisibility(View.GONE);
//                        etNominalJanjiBayarMenuHasilKunjungan.setVisibility(View.GONE);


                    } else if (Double.parseDouble(Function.removeSeparatorComa(val)) < Double.parseDouble(Function.removeSeparatorComa(kwjb))) {
                        System.out.println("Text Changed Partial Payment");
                        txtStatusPayment.setText("Partial Payment");
                        txtTglJanjiBayar.setVisibility(View.VISIBLE);
                        txtNominalJanjiBayar.setVisibility(View.VISIBLE);
                        etNominalJanjiBayarMenuHasilKunjungan.setVisibility(View.VISIBLE);
                        etTglJanjiBayarMenuHasilKunjungan.setVisibility(View.VISIBLE);

                        if (spinnerBertemuDengan.getSelectedItem().toString().equals("Nasabah") && txtStatusPayment.getText().equals("Partial Payment")) {
                            txtJikaBertemuNasabahTapiTidakBayarFull.setVisibility(View.VISIBLE);
                            autoTxt.setVisibility(View.VISIBLE);
                        } else {
                            txtJikaBertemuNasabahTapiTidakBayarFull.setVisibility(View.GONE);
                            autoTxt.setVisibility(View.GONE);
                        }


                        spinnerBertemuDengan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (adapterView.getSelectedItem().toString().equals("Nasabah")) {
                                    //TODO BERTEMU NASABAH TIDAK BAYAR FULL
                                    System.out.println("Spinner Bertemu dengan: " + adapterView.getSelectedItem().toString());


//                                    txtJikaBertemuSelainNasabah.setVisibility(View.GONE);
//                                    spinnerJikaBertemuSelainNasabah.setVisibility(View.GONE);


                                    txtJikaBertemuNasabahTapiTidakBayarFull.setVisibility(View.VISIBLE);
                                    autoTxt.setVisibility(View.VISIBLE);
                                } else {
//                                    txtJikaBertemuSelainNasabah.setVisibility(View.VISIBLE);
//                                    spinnerJikaBertemuSelainNasabah.setVisibility(View.VISIBLE);


                                    txtJikaBertemuNasabahTapiTidakBayarFull.setVisibility(View.GONE);
                                    autoTxt.setVisibility(View.GONE);
                                }

//                                if (!txtStatusPayment.getText().toString().equals("Full Payment")) {
//
//                                } else {
//                                    txtJikaBertemuSelainNasabah.setVisibility(View.VISIBLE);
//                                    spinnerJikaBertemuSelainNasabah.setVisibility(View.VISIBLE);
//                                }


                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });


//                        llJanjiBayar.setVisibility(View.VISIBLE);
//                        etTglJanjiBayarMenuHasilKunjungan.setVisibility(View.VISIBLE);
//                        etNominalJanjiBayarMenuHasilKunjungan.setVisibility(View.VISIBLE);
//                        etAlasanTelatBayarMenuHasilKunjungan.setVisibility(View.VISIBLE);
//                        llNasabahBayarMenuHasilKunjungan.setVisibility(View.GONE);
//                        llBuktiPembayaranMenuHasilKunjungan.setVisibility(View.GONE);
//                        txtAlasanTelatBayar.setVisibility(View.VISIBLE);
                    }


//                    else if (Double.parseDouble(NumberTextWatcherForThousand.trimCommaOfString(val)) == 0) {
//                        System.out.println("Text Changed No Payment");
//                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


//        spinnerBertemuDengan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//                if (adapterView.getSelectedItem().equals("Nasabah")) {
//                    txtAlasanTidakBertemuNasabah.setVisibility(View.GONE);
//                    spinnerAlasanTidakBertemuNasabah.setVisibility(View.GONE);
//                } else {
//                    txtAlasanTidakBertemuNasabah.setVisibility(View.VISIBLE);
//                    spinnerAlasanTidakBertemuNasabah.setVisibility(View.VISIBLE);
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        etNamaDebiturMenuHasilKunjungan.setText(dataDetInv.getNama_debitur());
        etResumeSingkatNasabahMenuHasilKunjungan.setText(dataDetInv.getResume_nsbh());
//        etNominalJanjiBayarMenuHasilKunjungan.setText(dataDetInv.getNominal_janji_bayar_terakhir());
        etNominalJanjiBayarMenuHasilKunjungan.setText(dataDetInv.getNominal_janji_bayar_terakhir());
        etNominalJanjiBayarMenuHasilKunjungan.addTextChangedListener(new NumberTextWathcerSettlement(etNominalJanjiBayarMenuHasilKunjungan));


        //etTglJanjiBayarMenuHasilKunjungan.setText(dataDetInv.getTgl_janji_bayar_terakhir());
        etTglJanjiBayarMenuHasilKunjungan.setText("");
        etTglJanjiBayarMenuHasilKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialogFragment = new DatePickerDialogClass();
                dialogFragment.show(getFragmentManager(), "Date Picker Dialog");
            }
        });


        etKewajibanYangHarusDibayarMenuHasilKunjungan.setText(Function.returnSeparatorComa(dataDetInv.getTotal_kewajiban()));

        //etKewajibanYangHarusDibayarMenuHasilKunjungan.setText(dataDetInv.getTotal_kewajiban());


        etAlamatNasabahMenuHasilKunjungan.setText(dataDetInv.getAlmt_rumah());
        etAlamatUsahaNasabah.setText(dataDetInv.getAlmt_usaha());
        etNoTelpNasabahMenuHasilKunjungan.setText(dataDetInv.getNo_tlpn());

        if (dataDetInv.getEmail().equals("[text]")) {
            etemailMenuHasilKunjungan.setText("");
        } else {
            etemailMenuHasilKunjungan.setText(dataDetInv.getEmail());
        }

        etNoLoanMenuHasilKunjungan.setText(dataDetInv.getNo_loan());


        spinnerAlamatNasabah = (Spinner) findViewById(R.id.spinnerAlamatNasabah);
        spinnerAlamatUsahaNasabah = (Spinner) findViewById(R.id.spinnerAlamatUsahaNasabah);
        spinnerNoTelpNasabahMenuHasilKunjungan = (Spinner) findViewById(R.id.spinnerNoTelpNasabahMenuHasilKunjungan);
        spinnerEmailMenuHasilKunjungan = (Spinner) findViewById(R.id.spinnerEmailMenuHasilKunjungan);
        spinnerAlasanTidakBertemuNasabah = (Spinner) findViewById(R.id.spinnerAlasanTidakBertemuNasabah);


        spinnerNasabahBayar = (Spinner) findViewById(R.id.spinnerNasabahBayar);
        spinnerBertemuDengan = (Spinner) findViewById(R.id.spinnerBertemuDengan);
        spinnerJikaBertemuSelainNasabah = (Spinner) findViewById(R.id.spinnerJikaBertemuSelainNasabah);
        spinnerLokasiBertemu = (Spinner) findViewById(R.id.spinnerLokasiBertemu);
//        spinnerKarakterNasabah = (Spinner) findViewById(R.id.spinnerKarakterNasabah);
        spinnerBuktiPembayaran = (Spinner) findViewById(R.id.spinnerBuktiPembayaran);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinner);
        ArrayAdapter<String> adapterBayar = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerBayar);

        ArrayAdapter<String> adapterBertemuDengan = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerBertemuDengan);
        ArrayAdapter<String> adapterJikaBertemuSelainNasabah = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerJikaBertemuSelainNasabah);
        ArrayAdapter<String> adapterLokasiBertemu = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arrayLokasiBertemu);
        ArrayAdapter<String> adapterKarakterNasabah = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arrayKarakterNasabah);
        ArrayAdapter<String> adapterBuktiPembayaran = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arrayBuktiPembayaran);
        ArrayAdapter<String> adapterSelainNasabah = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arraySpinnerJikaBertemuSelainNasabah);
        ArrayAdapter<String> adapterAlasanTidakBertemuNasabah = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arrayAlasanTidakBertemuNasabah);

        spinnerBertemuDengan.setAdapter(adapterBertemuDengan);
        spinnerAlasanTidakBertemuNasabah.setAdapter(adapterAlasanTidakBertemuNasabah);
        //spinnerJikaBertemuSelainNasabah.setAdapter(adapterJikaBertemuSelainNasabah);


        spinnerLokasiBertemu.setAdapter(adapterLokasiBertemu);
//        spinnerKarakterNasabah.setAdapter(adapterKarakterNasabah);
        spinnerBuktiPembayaran.setAdapter(adapterBuktiPembayaran);
        spinnerBuktiPembayaran.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinnerBuktiPembayaran.getSelectedItem().equals("Sms/Email")) {
                    txtAlasanPenggunaanKwitansi.setVisibility(View.GONE);
                    etAlasanPenggunaanKwitansi.setVisibility(View.GONE);
                    etInputNoSlipKwitansi.setVisibility(View.GONE);
                } else if (spinnerBuktiPembayaran.getSelectedItem().equals("Slip Kwitansi")) {
                    txtAlasanPenggunaanKwitansi.setVisibility(View.VISIBLE);
                    etAlasanPenggunaanKwitansi.setVisibility(View.VISIBLE);
                    etInputNoSlipKwitansi.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        spinnerAlamatNasabah.setAdapter(adapter);
        spinnerAlamatNasabah.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getSelectedItem().toString().equals(Constant.SESUAI)) {
                    etAlamatBaruNasabahMenuHasilKunjungan.setEnabled(false);
                    alamatNasabah = etAlamatNasabahMenuHasilKunjungan.getText().toString();
                } else if (adapterView.getSelectedItem().toString().equals(Constant.TIDAK)) {
                    etAlamatBaruNasabahMenuHasilKunjungan.setEnabled(true);
                    alamatNasabah = etAlamatBaruNasabahMenuHasilKunjungan.getText().toString();
                }
                System.out.println("Tes: " + adapterView.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerAlamatUsahaNasabah.setAdapter(adapter);
        spinnerAlamatUsahaNasabah.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getSelectedItem().toString().equals(Constant.SESUAI)) {
                    etAlamatUsahaBaruNasabah.setEnabled(false);
                } else if (adapterView.getSelectedItem().toString().equals(Constant.TIDAK)) {
                    etAlamatUsahaBaruNasabah.setEnabled(true);
                }
                System.out.println("Tes: " + adapterView.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerNoTelpNasabahMenuHasilKunjungan.setAdapter(adapter);
        spinnerNoTelpNasabahMenuHasilKunjungan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getSelectedItem().toString().equals(Constant.SESUAI)) {
                    etNoTelpBaruNasabahMenuHasilKunjungan.setEnabled(false);
                } else if (adapterView.getSelectedItem().toString().equals(Constant.TIDAK)) {
                    etNoTelpBaruNasabahMenuHasilKunjungan.setEnabled(true);
                }
                System.out.println("Tes: " + adapterView.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerEmailMenuHasilKunjungan.setAdapter(adapter);
        spinnerEmailMenuHasilKunjungan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getSelectedItem().toString().equals(Constant.SESUAI)) {
                    etemailBaruMenuHasilKunjungan.setEnabled(false);
                } else if (adapterView.getSelectedItem().toString().equals(Constant.TIDAK)) {
                    etemailBaruMenuHasilKunjungan.setEnabled(true);
                }
                System.out.println("Tes: " + adapterView.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerNasabahBayar.setAdapter(adapterBayar);
        spinnerNasabahBayar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getSelectedItem().toString().equals("Ya")) {
                    llNasabahBayarMenuHasilKunjungan.setVisibility(View.VISIBLE);
                    llBuktiPembayaranMenuHasilKunjungan.setVisibility(View.VISIBLE);
                    autoTxtAlasanTelatBayar.setVisibility(View.GONE);
//                    etAlasanTelatBayarMenuHasilKunjungan.setVisibility(View.GONE);
                    txtAlasanTelatBayar.setVisibility(View.GONE);
                    txtJikaBertemuNasabahTapiTidakBayarFull.setVisibility(View.VISIBLE);
                    autoTxt.setVisibility(View.VISIBLE);

                } else if (adapterView.getSelectedItem().toString().equals("Tidak")) {
                    // etAlasanTelatBayarMenuHasilKunjungan.setVisibility(View.VISIBLE);
                    autoTxtAlasanTelatBayar.setVisibility(View.VISIBLE);
                    llNasabahBayarMenuHasilKunjungan.setVisibility(View.GONE);
                    llBuktiPembayaranMenuHasilKunjungan.setVisibility(View.GONE);
                    txtAlasanTelatBayar.setVisibility(View.VISIBLE);
                    txtJikaBertemuNasabahTapiTidakBayarFull.setVisibility(View.GONE);
                    autoTxt.setVisibility(View.GONE);
                }
                //System.out.println("Tes: " + adapterView.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btnUploadDocMenuHasilKunjungan = (Button) findViewById(R.id.btnUploadDocMenuHasilKunjungan);
        btnUploadDocMenuHasilKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuTakePhoto.class);
                i.putExtra("id", iddetinv);
                startActivity(i);
            }
        });
        btnUploadPhotoMenuHasilKunjungan = (Button) findViewById(R.id.btnUploadPhotoMenuHasilKunjungan);
        btnUploadPhotoMenuHasilKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuTakePhoto.class);
                i.putExtra("id", iddetinv);
                startActivity(i);
            }
        });

        btnSubmitMenuHasilKunjungan = (Button) findViewById(R.id.btnSubmitMenuHasilKunjungan);
        btnSubmitMenuHasilKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                tglJanjiBayar = etTglJanjiBayarMenuHasilKunjungan.getText().toString();
//                 Toast(getApplicationContext(), tglJanjiBayar);
                if (spinnerAlamatNasabah.getSelectedItem().toString().equals(Constant.SESUAI)) {
                    alamatNasabah = etAlamatNasabahMenuHasilKunjungan.getText().toString();
                } else if (spinnerAlamatNasabah.getSelectedItem().toString().equals(Constant.TIDAK)) {
                    alamatNasabah = etAlamatBaruNasabahMenuHasilKunjungan.getText().toString();
                }
                if (spinnerAlamatUsahaNasabah.getSelectedItem().toString().equals(Constant.SESUAI)) {
                    alamatUsahaNasabah = etAlamatUsahaNasabah.getText().toString();
                } else if (spinnerAlamatUsahaNasabah.getSelectedItem().toString().equals(Constant.TIDAK)) {
                    alamatUsahaNasabah = etAlamatUsahaBaruNasabah.getText().toString();
                }
                if (spinnerEmailMenuHasilKunjungan.getSelectedItem().toString().equals(Constant.SESUAI)) {
                    email = etemailMenuHasilKunjungan.getText().toString();
                } else if (spinnerAlamatUsahaNasabah.getSelectedItem().toString().equals(Constant.TIDAK)) {
                    email = etemailBaruMenuHasilKunjungan.getText().toString();
                }
                if (spinnerNoTelpNasabahMenuHasilKunjungan.getSelectedItem().toString().equals(Constant.SESUAI)) {
                    noTelp = etNoTelpNasabahMenuHasilKunjungan.getText().toString();
                } else if (spinnerAlamatUsahaNasabah.getSelectedItem().toString().equals(Constant.TIDAK)) {
                    noTelp = etNoTelpBaruNasabahMenuHasilKunjungan.getText().toString();
                }


                bertemuDengan = spinnerBertemuDengan.getSelectedItem().toString();
                //TODO Check spinner Jika Bertemu Selain Nasabah available
//                if (spinnerJikaBertemuSelainNasabah != null && spinnerJikaBertemuSelainNasabah.getSelectedItem() != null) {
//                    jikaBertemuSelainNasabah = spinnerJikaBertemuSelainNasabah.getSelectedItem().toString();
//                } else {
//                    jikaBertemuSelainNasabah = "";
//                }


                lokasiBertemu = spinnerLokasiBertemu.getSelectedItem().toString();
                //   waktuSurvey = etWaktuSurveyMenuHasilKunjungan.getText().toString();
                karakterNasabah = spinnerKarakterNasabah.getSelectedItem().toString();
                buktiPembayaranBy = spinnerBuktiPembayaran.getSelectedItem().toString();
                alasanPenggunaanKwitansi = etAlasanPenggunaanKwitansi.getText().toString();
                noSlipKwitansi = etInputNoSlipKwitansi.getText().toString();
                resumeSingkatNasabah = etResumeSingkatNasabahMenuHasilKunjungan.getText().toString();

                sesuaiAlamatUsaha = spinnerAlamatUsahaNasabah.getSelectedItem().toString();
                sesuaiAlamatNasabah = spinnerAlamatNasabah.getSelectedItem().toString();
                sesuaiEmail = spinnerEmailMenuHasilKunjungan.getSelectedItem().toString();
                nasabahBayar = spinnerNasabahBayar.getSelectedItem().toString();


                if (spinnerNasabahBayar.getSelectedItem().toString().equals("Ya")) {
                    if (nominalygDibayar.isEmpty() || nominalJanjiBayar.isEmpty()) {
                        Function.showAlert(mContext, "Mohon Isi Nominal Yang Dibayar");
                    } else {
                        nominalJanjiBayar = etNominalJanjiBayarMenuHasilKunjungan.getText().toString();
                        if (txtStatusPayment.equals("Full Payment")) {
                            nominalJanjiBayar = "0";
                        } else {
                            if (etNominalJanjiBayarMenuHasilKunjungan.getText().toString().isEmpty()) {
                                nominalJanjiBayar = "0";
                            } else {
                                nominalJanjiBayar = etNominalJanjiBayarMenuHasilKunjungan.getText().toString();
                            }
                        }
                        nominalygDibayar = etNominalyangHarusDibayarMenuHasilKunjungan.getText().toString();
                        tglJanjiBayar = etTglJanjiBayarMenuHasilKunjungan.getText().toString();

                        alasanTelatBayar = "";


                        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                        nikShared = (shared.getString(Constant.NIK, ""));
                        usernameShared = (shared.getString(Constant.SHARED_USERNAME, ""));
                        passwordShared = (shared.getString(Constant.SHARED_PASSWORD, ""));
                        new executeHasilKunjungan(mContext).execute();
                    }
                } else if (spinnerNasabahBayar.getSelectedItem().toString().equals(Constant.TIDAK)) {
                    shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                    nikShared = (shared.getString(Constant.NIK, ""));
                    usernameShared = (shared.getString(Constant.SHARED_USERNAME, ""));
                    passwordShared = (shared.getString(Constant.SHARED_PASSWORD, ""));


                    nominalygDibayar = "0";
                    tglJanjiBayar = "";
                    nominalJanjiBayar = "0";
                    alasanTelatBayar = etAlasanTelatBayarMenuHasilKunjungan.getText().toString();

                    new executeHasilKunjungan(mContext).execute();
                }
            }
        });


    }


    @Override
    public void onComplete(String value) {
        passwordShared = Function.convertStringtoMD5(value);
        new executeHasilKunjungan(mContext).execute();
    }

    class executeHasilKunjungan extends AsyncTask<String, JSONObject, String> {

        Context context;

        public executeHasilKunjungan(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result.contains(Constant.CONNECTION_LOST)) {
//                Function.showAlert(mContext, Constant.CONNECTION_LOST);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Pesan");
                builder.setMessage("Gagal Mengirim Karena Gangguan Koneksi. Data akan dikirim kembali saat jaringan tersedia");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //TODO masukin detil settle untuk di settlement nanti
                        dataSettleDetil = new Settle_Detil();
                        dataSettleDetil.setNo_loan(noloan);
                        dataSettleDetil.setNo_rek(dataDetInv.getNo_rekening());
                        dataSettleDetil.setNama(dataDetInv.getNama_debitur());
                        dataSettleDetil.setNominal_bayar(NumberTextWatcherForThousand.trimCommaOfString(nominalygDibayar));
                        dataSettleDetil.setTotal_kewajiban(dataDetInv.getTotal_kewajiban());
                        dataSettleDetil.setId_detail_inventory(dataDetInv.getId_detail_inventory());
                        dataSettleDetil.setStatus("0");
                        dataSettleDetil.setSisa_bayar(String.valueOf(Double.parseDouble(dataDetInv.getTotal_kewajiban()) - Double.parseDouble(NumberTextWatcherForThousand.trimCommaOfString(nominalygDibayar))));

                        try {
                            JSONObject jsonupdate = new JSONObject();
                            jsonupdate.put("no_loan", noloan);
                            jsonupdate.put("no_rek", dataDetInv.getNo_rekening());
                            jsonupdate.put("nama", dataDetInv.getNama_debitur());
                            jsonupdate.put("nominal_bayar", NumberTextWatcherForThousand.trimCommaOfString(nominalygDibayar));
                            jsonupdate.put("total_kewajiban", dataDetInv.getTotal_kewajiban());
                            jsonupdate.put("id_det_inv", dataDetInv.getId_detail_inventory());
                            jsonupdate.put("status", 0);
                            jsonupdate.put("sisa_bayar", String.valueOf(Double.parseDouble(dataDetInv.getTotal_kewajiban()) - Double.parseDouble(NumberTextWatcherForThousand.trimCommaOfString(nominalygDibayar))));

                            dataPending = new Pending();
                            dataPending.setWaktu(Function.getDateNow());
                            dataPending.setData(jsonStr);
                            dataPending.setUrl(Constant.SERVICE_HASIL_KUNJUNGAN);
                            dataPending.setHeader(new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));
                            dataPending.setStatus("2");
                            dataPending.setJson_update(jsonupdate.toString());
                            dataPending.setKet_update("Collect");
                            queryPending.insertPendingData(dataPending);
                            queryPhoto.updateStatusPhoto(noloan);

                            Intent i = new Intent(mContext, MenuUtama.class);
                            startActivity(i);
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                });

                AlertDialog alert1 = builder.create();
                alert1.show();
            } else if (result.contains(Constant.CONNECTION_ERROR)) {
                Function.showAlert(mContext, Constant.CONNECTION_ERROR);
            } else {
                try {

                    try {
                        JSONObject jsonR = new JSONObject(result);
                        String keterangan = jsonR.getString("keterangan");
                        String rc = jsonR.getString("rc");


                        if (rc.equals("00")) {

                            JSONArray jArr = jsonR.getJSONArray("status_kunjungan");
                            JSONArray jArr1 = new JSONArray(jArr.toString());
                            String n = jArr1.getString(0);
                            JSONObject j = new JSONObject(n);
                            String status = j.getString("status");
                            String noloan = j.getString("no_loan");
                            System.out.println(status + " ---- " + noloan);

                            // querySettlement.deleteAllSettlement();
                            dataSettleDetil = new Settle_Detil();
                            dataSettleDetil.setNo_loan(noloan);
                            dataSettleDetil.setNo_rek(dataDetInv.getNo_rekening());
                            dataSettleDetil.setNama(dataDetInv.getNama_debitur());
                            dataSettleDetil.setNominal_bayar(NumberTextWatcherForThousand.trimCommaOfString(nominalygDibayar));
                            dataSettleDetil.setTotal_kewajiban(dataDetInv.getTotal_kewajiban());
                            dataSettleDetil.setId_detail_inventory(dataDetInv.getId_detail_inventory());
                            dataSettleDetil.setStatus("0");
                            dataSettleDetil.setSisa_bayar(String.valueOf(Double.parseDouble(dataDetInv.getTotal_kewajiban()) - Double.parseDouble(NumberTextWatcherForThousand.trimCommaOfString(nominalygDibayar))));

                            queryPhoto.updateStatusPhoto(noloan);

                            shared = getApplicationContext().getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                            SharedPreferences.Editor editor = shared.edit();
                            editor.putString(Constant.SHARED_PASSWORD, passwordShared);
                            editor.commit();

                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Pesan");
                            builder.setMessage("Submit Berhasil");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    if (querySettleDetil.insertSettleDetil(dataSettleDetil).equals("Insert Sukses")) {
                                        Intent i = new Intent(mContext, MenuUtama.class);
                                        startActivity(i);
                                        finish();
                                    } else if (querySettleDetil.insertSettleDetil(dataSettleDetil).equals("Insert Gagal")) {
                                        Function.showAlert(mContext, "Insert Gagal");
                                    }
                                }
                            });

                            AlertDialog alert1 = builder.create();
                            alert1.show();

                        } else if (rc.equals("T1")) {
                            //Function.showAlert(mContext, keterangan);
                            DialogNewPassword dialog1 = DialogNewPassword.newInstance();
                            dialog1.show(getFragmentManager(), "MyDialog");
                        } else if (rc.equals("T2")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Pesan");
                            builder.setMessage(keterangan);
                            builder.setIcon(R.drawable.ic_warning_black_24dp);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
                                    shared.edit().clear().commit();
                                    Intent i = new Intent(MenuHasilKunjunganDropdownList.this, MenuInisialisasi.class);
                                    startActivity(i);
                                    finish();
                                }
                            });

                            AlertDialog alert1 = builder.create();
                            alert1.show();
                        } else if (rc.equals("T3")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Pesan");
                            builder.setMessage(keterangan);
                            builder.setIcon(R.drawable.ic_warning_black_24dp);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
                                    shared.edit().clear().commit();
                                    Intent i = new Intent(MenuHasilKunjunganDropdownList.this, MenuInisialisasi.class);
                                    startActivity(i);
                                    finish();
                                }
                            });

                            AlertDialog alert1 = builder.create();
                            alert1.show();
                        } else if (rc.equals("T4")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Pesan");
                            builder.setMessage(keterangan);
                            builder.setIcon(R.drawable.ic_warning_black_24dp);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
                                    shared.edit().clear().commit();
                                    Intent i = new Intent(MenuHasilKunjunganDropdownList.this, MenuInisialisasi.class);
                                    startActivity(i);
                                    finish();
                                }
                            });

                            AlertDialog alert1 = builder.create();
                            alert1.show();
                        } else if (rc.equals("T5")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Pesan");
                            builder.setMessage(keterangan);
                            builder.setIcon(R.drawable.ic_warning_black_24dp);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
                                    shared.edit().clear().commit();
                                    Intent i = new Intent(MenuHasilKunjunganDropdownList.this, MenuInisialisasi.class);
                                    startActivity(i);
                                    finish();
                                }
                            });

                            AlertDialog alert1 = builder.create();
                            alert1.show();
                        } else {
                            Function.showAlert(mContext, keterangan);
                        }
                    } catch (JSONException e) {
                        Function.showAlert(mContext, result);
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    Function.showAlert(mContext, e.getMessage());
                    e.printStackTrace();
                }
            }

        }

        @Override
        protected String doInBackground(String... strings) {
            JSONObject json = new JSONObject();
            JSONObject json1;
            JSONObject jsonImage = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            JSONArray jsonArrayImage = new JSONArray();
            JSONObject jsonAll = new JSONObject();
            try {


                json.put("alamat_baru", alamatNasabah);
                json.put("tgl_janji_bayar", tglJanjiBayar);
                json.put("sesuai_alamat_usaha", sesuaiAlamatUsaha);
                json.put("tlpn_usaha", noTelp);
                json.put("bertemu_dengan", bertemuDengan);
                json.put("nominal_janji_bayar", NumberTextWatcherForThousand.trimCommaOfString(nominalJanjiBayar));
                json.put("sesuai_alamat_nasabah", sesuaiAlamatNasabah);
                json.put("unique_code", Function.generateRandomCode());
                json.put("resume", resumeSingkatNasabah);
                json.put("lokasi_bertemu", lokasiBertemu);
                json.put("karakter_nasabah", karakterNasabah);
                json.put("waktu_survey", Function.getDateNow());
                json.put("nama_debitur", namaDebitur);
                json.put("no_loan", noloan);
                json.put("nominal_bayar", NumberTextWatcherForThousand.trimCommaOfString(nominalygDibayar));
                json.put("bertemu_selain_nasabah", jikaBertemuSelainNasabah);
                json.put("sesuai_email", sesuaiEmail);
                json.put("nasabah_bayar", nasabahBayar);
                json.put("alasan_telat_bayar", alasanTelatBayar);

                json.put("latitude", querySSL.getLatitude());
                json.put("longitude", querySSL.getLongitude());

                listPhoto = queryPhoto.getimageByNoLoan(noloan);

                for (Photo p : listPhoto) {
                    json1 = new JSONObject();
                    String ket = p.getKeterangan();
                    String url = p.getUrl_photo();
                    String waktu = p.getWaktu();
                    if (ket == null) {
                        ket = "";
                    }
                    if (url == null) {
                        url = "";
                    }
                    if (waktu == null) {
                        waktu = "";
                    }
                    json1.put("keterangan", ket);
                    //json1.put("img", "");
                    json1.put("img", Function.bitmapToString(Function.bitmapFromPath(url)));
                    json1.put("waktu-kunjungan", waktu);
                    jsonArrayImage.put(json1);
                }

                json.put("images", jsonArrayImage);

                jsonArray.put(json);
//                jsonArray.put(jsonImage);

                jsonAll.put("waktu", Function.getWaktuSekarangMilis());
                jsonAll.put("username", usernameShared);
                jsonAll.put("password", passwordShared);
                jsonAll.put("hasil_kunjungan", jsonArray);


                jsonStr = jsonAll.toString();
                System.out.println("HASIL JSON: " + jsonStr);

                longLog(jsonStr);
                //String nik = "C001234";
                System.out.println("Header Base 64: " + new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));
                resultfromJson = jsonParser.HttpRequestPost(Constant.SERVICE_HASIL_KUNJUNGAN, jsonStr, Constant.TimeOutConnection, new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));
                System.out.println("RETURN JSON: " + resultfromJson);
                longLog(resultfromJson);

                return resultfromJson;

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Mengirim Data...");
            progressDialog.setTitle("Pesan");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    public static void longLog(String str) {
        if (str.length() > 4000) {
            Log.d("ini contoh json", str.substring(0, 4000));
            //System.out.println("return jsonnya: "+str.substring(0, 4000));
            longLog(str.substring(4000));
        } else
            Log.d("ini contoh json", str);
        //System.out.println("return jsonnya: "+str);
    }

    public void takePicture(View view) {

        //camera stuff
        Intent imageIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        //folder stuff
        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "KSP");
        if (!imagesFolder.exists()) {
            imagesFolder.mkdirs();
        }

        filename = noloan + "_" + timeStamp + ".png";
        //filename="DB-0000007"+ "_" + timeStamp + ".png";
        File image = new File(imagesFolder, filename);
        uriSavedImage = Uri.fromFile(image);

        path = image.toString();
        System.out.println("PATH: " + image.getPath());


        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        startActivityForResult(imageIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                File dir = new File(Environment.getExternalStorageDirectory(), "KSP");
                Bitmap b = BitmapFactory.decodeFile(path);
                Bitmap out = Bitmap.createScaledBitmap(b, 320, 480, false);

                File file = new File(dir, filename);
                if (file.exists()) {
                    file.delete();
                }
                FileOutputStream fOut;
                try {
                    fOut = new FileOutputStream(file);
                    out.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();

                    MySQLiteHelper.initDB(mContext);
                    ph = new Photo();
                    photodata = new Photo();

                    photodata.setNama_photo(filename);
                    photodata.setUrl_photo(path);
                    photodata.setId_detail_inventory(iddetinv);
                    photodata.setNoloan(noloan);
                    photodata.setWaktu(Function.getDateNow());
                    ph.insertPhotoIntoSqlite(photodata);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //imageView.setImageBitmap(out);
            }
        }
    }

}
