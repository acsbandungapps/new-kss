package com.newkss.acs.newkss.MenuKunjungan;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.newkss.acs.newkss.Menu.MenuDataOffline;
import com.newkss.acs.newkss.Menu.MenuInisialisasi;
import com.newkss.acs.newkss.Menu.MenuLogin;
import com.newkss.acs.newkss.Menu.MenuPerformance;
import com.newkss.acs.newkss.Menu.MenuUtama;
import com.newkss.acs.newkss.MenuPhoto.MenuTakePhoto;
import com.newkss.acs.newkss.MenuSettlement.MenuSettlement;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.ObjectActionCode;
import com.newkss.acs.newkss.Model.Pending;
import com.newkss.acs.newkss.Model.Photo;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.ModelBaru.TGT;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Service.ServiceSendLocation;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.DialogConfirm;
import com.newkss.acs.newkss.Util.DialogNewPassword;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.JSONParser;
import com.newkss.acs.newkss.Util.MySQLiteHelper;
import com.newkss.acs.newkss.Util.NumberTextWatcherForThousand;
import com.newkss.acs.newkss.Util.NumberTextWathcerSettlement;
import com.newkss.acs.newkss.Util.OnConfirmListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by acs on 8/22/17.
 */

public class MenuKunjunganRev extends Activity {

    Context mContext;
    ImageView imgBackMenuHasilKunjungan;
    LinearLayout llMenuHasilKunjunganPopup;
    TextView txtNamaMenuHasilKunjungan, txtDivisiMenuHasilKunjungan;
    private String[] arrayAlamatNasabah;
    EditText etNamaDebiturMenuHasilKunjungan, etNoLoanMenuHasilKunjungan;
    EditText etAlamatNasabahMenuHasilKunjungan, etAlamatBaruNasabahMenuHasilKunjungan, etDropdownAlamatNasabah;
    EditText etAlamatUsahaNasabahMenuHasilKunjungan, etDropdownAlamatUsahaNasabah, etAlamatUsahaBaruNasabahMenuHasilKunjungan;
    EditText etNoTelpNasabahMenuHasilKunjungan, etDropdownNoTelpBaru, etNoTelpBaruNasabahMenuHasilKunjungan;
    EditText etEmailNasabahMenuHasilKunjungan, etDropdownEmail, etEmailNasabahBaruMenuHasilKunjungan;
    EditText etDropdownNasabahBayar;


    TextView txtNasabahBayar;
    EditText etTotalKewajiban, etNominalYangDibayar;
    LinearLayout llNasabahBayar;

    TextView txtStatusPembayaran;


    LinearLayout llJanjiBayar;

    TextView txtAlasanNasabahTidakBayarFull;
    EditText etDropdownAlasanNasabahTidakBayarFull;

    TextView txtTglJanjiBayar;
    static EditText etTglJanjiBayarMenuHasilKunjungan;

    TextView txtAlasanTelatBayar;
    EditText etDropdownAlasanTelatBayar;

    TextView txtAlasanTidakBertemuNasabah;
    EditText etAlasanTidakBertemuNasabahMenuHasilKunjungan;

    TextView txtBertemuDenganMenuHasilKunjungan;
    EditText etBertemuDenganMenuHasilKunjungan;

    TextView txtLokasiBertemu;
    EditText etLokasiBertemu;

    TextView txtKarakterNasabah;
    EditText etKarakterNasabahMenuHasilKunjungan;

    EditText etBuktiPembayaranBy;
    EditText etInputNoSlipKwitansi;

    TextView txtNominalJanjiBayar;
    EditText etNominalJanjiBayarMenuHasilKunjungan;

    EditText etResumeSingkatNasabahMenuHasilKunjungan;

    TextView txtActionCode;
    EditText etDropdownActionCodeMenuHasilKunjungan;

    List<String> listAdapter = new ArrayList<>();
    List<String> listAdapterNasabahBayar = new ArrayList<>();
    List<String> listAdapterAlasanNasabahTidakBayarFull = new ArrayList<>();
    List<String> listAdapterAlasanNasabahTelatBayar = new ArrayList<>();
    List<String> listAdapterBertemuDengan = new ArrayList<>();
    List<String> listAdapterAlasanTidakBertemuNasabah = new ArrayList<>();
    List<String> listAdapterLokasiBertemu = new ArrayList<>();
    List<String> listAdapterKarakterNasabah = new ArrayList<>();
    List<String> listAdapterBuktiPembayaranBy = new ArrayList<>();
    List<String> listAdapterActionCode = new ArrayList<>();


    String totalKewajiban = "0";
    String nominalDibayar = "";
    String statusPembayaran;

    LinearLayout llPenggunaanKwitansi;

    String valuedropdownBertemuDengan = "";
    String valuedropdownNasabahBayar = "";

    //value yang dikirim
    String kode_kunjungan = "";
    String alasan = "";
    String alamat_baru = "";
    String tgl_janji_bayar = "";
    String sesuai_alamat_usaha = "";
    String tlpn_usaha_baru = "";
    String bertemu_dengan = "";
    String nominal_janji_bayar = "";
    String email_baru = "";
    String sesuai_alamat_nasabah = "";
    String unique_code = "";
    String resume = "";
    String lokasi_bertemu = "";
    String karakter_nasabah = "";
    String waktu_survey = "";
    String images = "";
    String keterangan = "";
    String img = "";
    String nama_debitur = "";
    String no_loan = "";
    String nominal_bayar = "";
    String alamat_usaha_baru = "";
    String sesuai_telepon_usaha = "";
    String sesuai_email = "";
    String nasabah_bayar = "";
    String action_code = "";
    Double latitude = 0.0;
    Double longitude = 0.0;
    Location location;

    Button btnSubmitMenuHasilKunjungan;

    ServiceSendLocation querySSL;
    ArrayList<Photo> listPhoto = new ArrayList<>();
    Photo queryPhoto = new Photo();
    String jsonStr = "";

    SharedPreferences shared;
    String namaShared, divisiShared;
    ArrayList<ObjectActionCode> listObjectActionCode = new ArrayList<>();
    ObjectActionCode queryOAC = new ObjectActionCode();
    ProgressDialog progressDialog;
    String iddetinv = "";
    Detail_Inventory queryDetInv = new Detail_Inventory();
    Detail_Inventory dataDetInv = new Detail_Inventory();
    TGT querydp = new TGT();
    TGT datadp = new TGT();
    String noloan = "";
    String nikShared, usernameShared, passwordShared;
    JSONParser jsonParser = new JSONParser();
    String resultFromJson = "";
    Settle_Detil dataSettleDetil;
    Settle_Detil querySettleDetil = new Settle_Detil();

    Pending queryPending = new Pending();
    Pending dataPending;
    Button btnUploadDocMenuHasilKunjungan, btnUploadPhotoMenuHasilKunjungan;

    Settle_Detil querySD = new Settle_Detil();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuhasilkunjunganrev);
        initUI();
        initEvent();
    }

    private void initUI() {
        mContext = this;
        MySQLiteHelper.initDB(mContext);

        querySSL = new ServiceSendLocation(mContext);
        etNamaDebiturMenuHasilKunjungan = (EditText) findViewById(R.id.etNamaDebiturMenuHasilKunjungan);
        etNoLoanMenuHasilKunjungan = (EditText) findViewById(R.id.etNoLoanMenuHasilKunjungan);

        txtNominalJanjiBayar = (TextView) findViewById(R.id.txtNominalJanjiBayar);
        etNominalJanjiBayarMenuHasilKunjungan = (EditText) findViewById(R.id.etNominalJanjiBayarMenuHasilKunjungan);
        txtNamaMenuHasilKunjungan = (TextView) findViewById(R.id.txtNamaMenuHasilKunjungan);
        //txtNamaMenuHasilKunjungan.setText("Steven Raylianto Kerta");
        etAlamatBaruNasabahMenuHasilKunjungan = (EditText) findViewById(R.id.etAlamatBaruNasabahMenuHasilKunjungan);
        etDropdownAlamatNasabah = (EditText) findViewById(R.id.etDropdownAlamatNasabah);
        etAlamatNasabahMenuHasilKunjungan = (EditText) findViewById(R.id.etAlamatNasabahMenuHasilKunjungan);

        etAlamatUsahaNasabahMenuHasilKunjungan = (EditText) findViewById(R.id.etAlamatUsahaNasabahMenuHasilKunjungan);
        etDropdownAlamatUsahaNasabah = (EditText) findViewById(R.id.etDropdownAlamatUsahaNasabah);
        etAlamatUsahaBaruNasabahMenuHasilKunjungan = (EditText) findViewById(R.id.etAlamatUsahaBaruNasabahMenuHasilKunjungan);

        etNoTelpNasabahMenuHasilKunjungan = (EditText) findViewById(R.id.etNoTelpNasabahMenuHasilKunjungan);
        etDropdownNoTelpBaru = (EditText) findViewById(R.id.etDropdownNoTelpBaru);
        etNoTelpBaruNasabahMenuHasilKunjungan = (EditText) findViewById(R.id.etNoTelpBaruNasabahMenuHasilKunjungan);

        etEmailNasabahMenuHasilKunjungan = (EditText) findViewById(R.id.etEmailNasabahMenuHasilKunjungan);
        etDropdownEmail = (EditText) findViewById(R.id.etDropdownEmail);
        etEmailNasabahBaruMenuHasilKunjungan = (EditText) findViewById(R.id.etEmailNasabahBaruMenuHasilKunjungan);

        etDropdownNasabahBayar = (EditText) findViewById(R.id.etDropdownNasabahBayar);

        txtNasabahBayar = (TextView) findViewById(R.id.txtNasabahBayar);
        etTotalKewajiban = (EditText) findViewById(R.id.etTotalKewajiban);
        etNominalYangDibayar = (EditText) findViewById(R.id.etNominalYangDibayar);
        llNasabahBayar = (LinearLayout) findViewById(R.id.llNasabahBayar);

        txtStatusPembayaran = (TextView) findViewById(R.id.txtStatusPembayaran);

        llJanjiBayar = (LinearLayout) findViewById(R.id.llJanjiBayar);

        txtAlasanNasabahTidakBayarFull = (TextView) findViewById(R.id.txtAlasanNasabahTidakBayarFull);
        etDropdownAlasanNasabahTidakBayarFull = (EditText) findViewById(R.id.etDropdownAlasanNasabahTidakBayarFull);

        etTglJanjiBayarMenuHasilKunjungan = (EditText) findViewById(R.id.etTglJanjiBayarMenuHasilKunjungan);

        txtAlasanTelatBayar = (TextView) findViewById(R.id.txtAlasanTelatBayar);
        etDropdownAlasanTelatBayar = (EditText) findViewById(R.id.etDropdownAlasanTelatBayar);

        txtAlasanTidakBertemuNasabah = (TextView) findViewById(R.id.txtAlasanTidakBertemuNasabah);
        etAlasanTidakBertemuNasabahMenuHasilKunjungan = (EditText) findViewById(R.id.etAlasanTidakBertemuNasabahMenuHasilKunjungan);

        txtBertemuDenganMenuHasilKunjungan = (TextView) findViewById(R.id.txtBertemuDenganMenuHasilKunjungan);
        etBertemuDenganMenuHasilKunjungan = (EditText) findViewById(R.id.etBertemuDenganMenuHasilKunjungan);

        txtLokasiBertemu = (TextView) findViewById(R.id.txtLokasiBertemu);
        etLokasiBertemu = (EditText) findViewById(R.id.etLokasiBertemu);

        txtKarakterNasabah = (TextView) findViewById(R.id.txtKarakterNasabah);
        etKarakterNasabahMenuHasilKunjungan = (EditText) findViewById(R.id.etKarakterNasabahMenuHasilKunjungan);

        etBuktiPembayaranBy = (EditText) findViewById(R.id.etBuktiPembayaranBy);
        etInputNoSlipKwitansi = (EditText) findViewById(R.id.etInputNoSlipKwitansi);

        llPenggunaanKwitansi = (LinearLayout) findViewById(R.id.llPenggunaanKwitansi);
        etResumeSingkatNasabahMenuHasilKunjungan = (EditText) findViewById(R.id.etResumeSingkatNasabahMenuHasilKunjungan);

        btnSubmitMenuHasilKunjungan = (Button) findViewById(R.id.btnSubmitMenuHasilKunjungan);
        btnUploadDocMenuHasilKunjungan = (Button) findViewById(R.id.btnUploadDocMenuHasilKunjungan);
        btnUploadPhotoMenuHasilKunjungan = (Button) findViewById(R.id.btnUploadPhotoMenuHasilKunjungan);

        Intent in = getIntent();
        iddetinv = in.getStringExtra("id");
        // iddetinv = "DI-0000001";
        String iddp = queryDetInv.getIdTGTFromIdDetInv(iddetinv);
        dataDetInv = queryDetInv.getDetInventoryFromIdDetInv(iddetinv);
        datadp = querydp.getTGTFromID(iddp);
        noloan = queryDetInv.getNoLoanfromIdDetInv(iddetinv);

        etNamaDebiturMenuHasilKunjungan.setText(dataDetInv.getNama_debitur());
        etResumeSingkatNasabahMenuHasilKunjungan.setText(dataDetInv.getResume_nsbh());
        //etNominalJanjiBayarMenuHasilKunjungan.setText(dataDetInv.getNominal_janji_bayar_terakhir());
        etNominalJanjiBayarMenuHasilKunjungan.addTextChangedListener(new NumberTextWathcerSettlement(etNominalJanjiBayarMenuHasilKunjungan));


        //etTglJanjiBayarMenuHasilKunjungan.setText(dataDetInv.getTgl_janji_bayar_terakhir());
        etTglJanjiBayarMenuHasilKunjungan.setText("");
        etTglJanjiBayarMenuHasilKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialogFragment = new DatePickerDialogClass();
                dialogFragment.show(getFragmentManager(), "Date Picker Dialog");
            }
        });


        etTotalKewajiban.setText(Function.returnSeparatorComa(dataDetInv.getTotal_kewajiban()));
        etAlamatNasabahMenuHasilKunjungan.setText(dataDetInv.getAlmt_rumah());
        etAlamatUsahaNasabahMenuHasilKunjungan.setText(dataDetInv.getAlmt_usaha());
        etNoTelpNasabahMenuHasilKunjungan.setText(dataDetInv.getNo_tlpn());

        if (dataDetInv.getEmail().equals("[text]")) {
            etEmailNasabahMenuHasilKunjungan.setText("");
        } else {
            etEmailNasabahMenuHasilKunjungan.setText(dataDetInv.getEmail());
        }
        etNoLoanMenuHasilKunjungan.setText(dataDetInv.getNo_loan());

        txtNamaMenuHasilKunjungan = (TextView) findViewById(R.id.txtNamaMenuHasilKunjungan);
        txtDivisiMenuHasilKunjungan = (TextView) findViewById(R.id.txtDivisiMenuHasilKunjungan);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtNamaMenuHasilKunjungan.setText(namaShared);
            txtDivisiMenuHasilKunjungan.setText(divisiShared);
        }

        txtActionCode = (TextView) findViewById(R.id.txtActionCode);
        etDropdownActionCodeMenuHasilKunjungan = (EditText) findViewById(R.id.etDropdownActionCodeMenuHasilKunjungan);


        listObjectActionCode = queryOAC.getDataObjectActionCode();
        for (ObjectActionCode oac : listObjectActionCode) {
            listAdapterActionCode.add(oac.getValue());
        }


        listAdapter.add(Constant.SESUAI);
        listAdapter.add(Constant.TIDAK);
        listAdapterNasabahBayar.add("Ya");
        listAdapterNasabahBayar.add("Tidak");
        listAdapterNasabahBayar.add("Janji Bayar");
        listAdapterAlasanNasabahTidakBayarFull.add("Nasabah Minta Pickup");
        listAdapterAlasanNasabahTidakBayarFull.add("Nasabah Setor Sendiri Ke Kantor");
        listAdapterAlasanNasabahTidakBayarFull.add("Nasabah Setor Sendiri Ke Alfa");
        listAdapterAlasanNasabahTidakBayarFull.add("Nasabah Transfer Virtual BSS");
        listAdapterAlasanNasabahTidakBayarFull.add("Kunjungan Lagi");
        listAdapterAlasanNasabahTelatBayar.add("Tanggal Cycle Tidak Sesuai Dengan Tanggal Gajian");
        listAdapterAlasanNasabahTelatBayar.add("Musibah");
        listAdapterAlasanNasabahTelatBayar.add("Kesulitan Keuangan");
        listAdapterAlasanNasabahTelatBayar.add("Pembayaran Oleh Pihak Lain");
        listAdapterBertemuDengan.add("Nasabah");
        listAdapterBertemuDengan.add("Pasangan Nasabah");
        listAdapterBertemuDengan.add("Keluarga 1 Rumah");
        listAdapterBertemuDengan.add("Keluarga Beda Rumah");
        listAdapterBertemuDengan.add("Karyawan/Pegawai/Rekan Kerja");
        listAdapterBertemuDengan.add("Tetangga");
        listAdapterBertemuDengan.add("Aparat Pemerintah Setempat/POLRI/TNI");
        listAdapterBertemuDengan.add("Pejabat KPKNL/Pengadilan");
        listAdapterBertemuDengan.add("Calon Pembeli/Funder");
        listAdapterBertemuDengan.add("Penadah");
        listAdapterAlasanTidakBertemuNasabah.add("Rumah Kosong");
        listAdapterAlasanTidakBertemuNasabah.add("Pindah");
        listAdapterAlasanTidakBertemuNasabah.add("Fraud");
        listAdapterAlasanTidakBertemuNasabah.add("Skip");
        listAdapterAlasanTidakBertemuNasabah.add("Security");
        listAdapterAlasanTidakBertemuNasabah.add("Titip Pesan");
        listAdapterLokasiBertemu.add("Rumah");
        listAdapterLokasiBertemu.add("Tempat Usaha/Kantor");
        listAdapterLokasiBertemu.add("KPKNL");
        listAdapterLokasiBertemu.add("Pengadilan");
        listAdapterLokasiBertemu.add("Kantor Pemerintah/TNI/POLRI");
        listAdapterKarakterNasabah.add("Kooperatif, Bisa Ditemui dan Ditelpon");
        listAdapterKarakterNasabah.add("Kooperatif, Susah ditemui tapi hanya bisa di telpon");
        listAdapterKarakterNasabah.add("Cukup Kooperatif, Tidak Bisa ditemui tapi bisa ditelpon");
        listAdapterKarakterNasabah.add("Cukup Kooperatif, Bisa ditemui tp tidak bisa ditelpon");
        listAdapterKarakterNasabah.add("Tidak Kooperatif, Tidak bisa ditemui dan Tdk Bisa Ditelpon");
        listAdapterBuktiPembayaranBy.add("SMS/Email");
        listAdapterBuktiPembayaranBy.add("Slip Kwitansi");


        valuedropdownBertemuDengan = listAdapterBertemuDengan.get(0).toString();
        valuedropdownNasabahBayar = "Ya";
        getListBertemuDengan();
        getListAlamatNasabah();
        getListAlamatUsahaNasabah();
        getListNoTelpNasabah();
        getListEmailNasabah();
        getListNasabahBayar();
        getStatusPembayaran();
        getListAlasanNasabahTidakBayarFull();
        getListAlasanNasabahTelatBayar();
        getTglJanjiBayar();
        getListAlasanTidakBertemuNasabah();
        getListLokasiBertemu();
        getListKarakterNasabah();
        getListBuktiPembayaranBy();
        getListActionCode();
        btnSubmitEvent();
        btnUploadEvent();

        llMenuHasilKunjunganPopup = (LinearLayout) findViewById(R.id.llMenuHasilKunjunganPopup);
        llMenuHasilKunjunganPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MenuKunjunganRev.this, llMenuHasilKunjunganPopup);
                popup.getMenuInflater().inflate(R.menu.popup_inbox, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_DATAPENDING)) {
                            Intent i = new Intent(getApplicationContext(), MenuDataOffline.class);
                            startActivity(i);
                            finish();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            if (querySD.isSettleAvailable().equals("0")) {
                                Intent i = new Intent(getApplicationContext(), MenuLogin.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                                finish();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Pesan");
                                builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Mohon Lakukan Settlement Terlebih Dahulu");
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent i = new Intent(MenuKunjunganRev.this, MenuSettlement.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });
                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            }
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });

        imgBackMenuHasilKunjungan = (ImageView) findViewById(R.id.imgBackMenuHasilKunjungan);
        imgBackMenuHasilKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    private void btnUploadEvent() {
        btnUploadDocMenuHasilKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuTakePhoto.class);
                i.putExtra("id", iddetinv);
                startActivity(i);
            }
        });

        btnUploadPhotoMenuHasilKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuTakePhoto.class);
                i.putExtra("id", iddetinv);
                startActivity(i);
            }
        });
    }

    public static void longLog(String str) {
        if (str.length() > 4000) {
            Log.d("Menu Kunjungan Rev", str.substring(0, 4000));
            longLog(str.substring(4000));
        } else
            Log.d("Menu Kunjungan Rev", str);
    }


    private class execSubmit extends AsyncTask<String, JSONObject, String> {
        Context context;

        private execSubmit(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Mengirim Data...");
            progressDialog.setTitle("Pesan");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals(Constant.CONNECTION_LOST)) {
//                Function.showAlert(mContext, Constant.CONNECTION_LOST);
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Pesan");
                builder.setMessage("Gagal Mengirim Karena Gangguan Koneksi. Data akan dikirim kembali saat jaringan tersedia");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //TODO masukin detil settle untuk di settlement nanti
                        dataSettleDetil = new Settle_Detil();
                        dataSettleDetil.setNo_loan(noloan);
                        dataSettleDetil.setNo_rek(dataDetInv.getNo_rekening());
                        dataSettleDetil.setNama(dataDetInv.getNama_debitur());
                        dataSettleDetil.setNominal_bayar(NumberTextWatcherForThousand.trimCommaOfString(nominal_bayar));
                        dataSettleDetil.setTotal_kewajiban(dataDetInv.getTotal_kewajiban());
                        dataSettleDetil.setId_detail_inventory(dataDetInv.getId_detail_inventory());
                        dataSettleDetil.setStatus("0");
                        dataSettleDetil.setWaktu(Function.getDateNow());
                        dataSettleDetil.setUniqueCode(unique_code);

                        try {
                            JSONObject jsonupdate = new JSONObject();
                            jsonupdate.put("no_loan", noloan);
                            jsonupdate.put("no_rek", dataDetInv.getNo_rekening());
                            jsonupdate.put("nama", dataDetInv.getNama_debitur());
                            jsonupdate.put("nominal_bayar", NumberTextWatcherForThousand.trimCommaOfString(nominal_bayar));
                            jsonupdate.put("total_kewajiban", dataDetInv.getTotal_kewajiban());
                            jsonupdate.put("id_det_inv", dataDetInv.getId_detail_inventory());
                            jsonupdate.put("status", 0);
//                            jsonupdate.put("sisa_bayar", String.valueOf(Double.parseDouble(dataDetInv.getTotal_kewajiban()) - Double.parseDouble(NumberTextWatcherForThousand.trimCommaOfString(nominal_bayar))));
                            jsonupdate.put("kode_kunjungan", kode_kunjungan);
                            jsonupdate.put("unique_code", unique_code);

                            //check unique code apabila sudah ada data sama tidak bisa insert
                            if (querySettleDetil.cekUniqueCodeAvailable(unique_code).equals("kosong")) {
                                dataPending = new Pending();
                                dataPending.setWaktu(Function.getDateNow());
                                dataPending.setData(jsonStr);
                                dataPending.setUrl(Constant.SERVICE_HASIL_KUNJUNGAN);
                                dataPending.setHeader(new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));
                                dataPending.setStatus("2");
                                dataPending.setJson_update(jsonupdate.toString());
                                dataPending.setKet_update("Collect");
                                queryPending.insertPendingData(dataPending);
                                queryPhoto.updateStatusPhoto(noloan);
                            } else if (querySettleDetil.cekUniqueCodeAvailable(unique_code).equals("ada")) {
                                Function.showAlert(mContext, "Data Sudah Pernah Dikirim");
                            }

                            Intent i = new Intent(mContext, MenuUtama.class);
                            startActivity(i);
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setCancelable(false);
                AlertDialog alert1 = builder.create();
                alert1.show();
            } else if (result.isEmpty()) {
                Function.showAlert(mContext, Constant.CONNECTION_EMPTY);
            } else if (result == null) {
                Function.showAlert(mContext, Constant.CONNECTION_EMPTY);
            } else {
                try {
                    JSONObject jsonR = new JSONObject(result);
                    String keterangan = jsonR.getString("keterangan");
                    String rc = jsonR.getString("rc");
                    try {
                        if (querySettleDetil.cekUniqueCodeAvailable(unique_code).equals("kosong")) {
                            if (rc.equals("00")) {
                                if (kode_kunjungan.equals("NN") || kode_kunjungan.equals("NP") || kode_kunjungan.equals("JB")) {
                                    dataSettleDetil = new Settle_Detil();
                                    dataSettleDetil.setNo_loan(noloan);
                                    dataSettleDetil.setNo_rek(dataDetInv.getNo_rekening());
                                    dataSettleDetil.setNama(dataDetInv.getNama_debitur());
                                    dataSettleDetil.setNominal_bayar(NumberTextWatcherForThousand.trimCommaOfString(nominal_bayar));
                                    dataSettleDetil.setTotal_kewajiban(dataDetInv.getTotal_kewajiban());
                                    dataSettleDetil.setId_detail_inventory(dataDetInv.getId_detail_inventory());
                                    dataSettleDetil.setStatus("5");
                                    dataSettleDetil.setWaktu(Function.getDateNow());
                                    dataSettleDetil.setUniqueCode(unique_code);

                                } else if (kode_kunjungan.equals("FP") || kode_kunjungan.equals("PP")) {
                                    dataSettleDetil = new Settle_Detil();
                                    dataSettleDetil.setNo_loan(noloan);
                                    dataSettleDetil.setNo_rek(dataDetInv.getNo_rekening());
                                    dataSettleDetil.setNama(dataDetInv.getNama_debitur());
                                    dataSettleDetil.setNominal_bayar(NumberTextWatcherForThousand.trimCommaOfString(nominal_bayar));
                                    dataSettleDetil.setTotal_kewajiban(dataDetInv.getTotal_kewajiban());
                                    dataSettleDetil.setId_detail_inventory(dataDetInv.getId_detail_inventory());
                                    dataSettleDetil.setStatus("0");
                                    dataSettleDetil.setWaktu(Function.getDateNow());
                                    dataSettleDetil.setUniqueCode(unique_code);
                                }
                                queryPhoto.updateStatusPhoto(noloan);

                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Pesan");
                                builder.setMessage("Submit Berhasil");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (querySettleDetil.insertSettleDetil(dataSettleDetil).equals("Insert Sukses")) {
                                            Intent i = new Intent(mContext, MenuUtama.class);
                                            startActivity(i);
                                            finish();
                                        } else if (querySettleDetil.insertSettleDetil(dataSettleDetil).equals("Insert Gagal")) {
                                            Function.showAlert(mContext, "Insert Gagal");
                                        }
                                    }
                                });

                                AlertDialog alert = builder.create();
                                alert.show();
                            } else if (rc.equals("T1")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Pesan");
                                builder.setMessage(keterangan);
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                    shared.edit().clear().commit();
                                        Intent i = new Intent(MenuKunjunganRev.this, MenuLogin.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });

                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            } else if (rc.equals("T2")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Pesan");
                                builder.setMessage(keterangan);
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                    shared.edit().clear().commit();
                                        Intent i = new Intent(MenuKunjunganRev.this, MenuLogin.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });

                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            } else if (rc.equals("T3")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Pesan");
                                builder.setMessage(keterangan);
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                    shared.edit().clear().commit();
                                        Intent i = new Intent(MenuKunjunganRev.this, MenuLogin.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });

                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            } else if (rc.equals("T4")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Pesan");
                                builder.setMessage(keterangan);
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                    shared.edit().clear().commit();
                                        Intent i = new Intent(MenuKunjunganRev.this, MenuLogin.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });
                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            } else if (rc.equals("T5")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Pesan");
                                builder.setMessage(keterangan);
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                    shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                    shared.edit().clear().commit();
                                        Intent i = new Intent(MenuKunjunganRev.this, MenuLogin.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });
                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            } else {
                                Function.showAlert(mContext, keterangan);
                            }
                        } else {
                            Function.showAlert(mContext, "Data Sudah Pernah Dikirim");
                        }

                    } catch (Exception e) {
                        Function.showAlert(mContext, result);
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    Function.showAlert(mContext, e.getMessage());
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            JSONObject jsonKirim = new JSONObject();
            JSONObject jsonPhoto;
            JSONArray jsonArray = new JSONArray();
            JSONArray jsonArrayImage = new JSONArray();
            JSONObject jsonAll = new JSONObject();
            try {
                querySSL = new ServiceSendLocation(context);
                location = querySSL.getLocation();
                unique_code = Function.generateRandomCode();
                jsonKirim.put("action_code", action_code);
                jsonKirim.put("alamat_baru", alamat_baru);
                jsonKirim.put("alamat_usaha_baru", alamat_usaha_baru);
                jsonKirim.put("tlpn_usaha_baru", tlpn_usaha_baru);
                jsonKirim.put("email_baru", email_baru);
                jsonKirim.put("sesuai_alamat_nasabah", sesuai_alamat_nasabah);
                jsonKirim.put("sesuai_alamat_usaha", sesuai_alamat_usaha);
                jsonKirim.put("sesuai_telepon_usaha", sesuai_telepon_usaha);
                jsonKirim.put("sesuai_email", sesuai_email);

                jsonKirim.put("resume", resume);
                jsonKirim.put("lokasi_bertemu", lokasi_bertemu);
                jsonKirim.put("karakter_nasabah", karakter_nasabah);
                jsonKirim.put("waktu_survey", Function.getDateNow());
                jsonKirim.put("nama_debitur", nama_debitur);
                jsonKirim.put("no_loan", no_loan);

                jsonKirim.put("nominal_bayar", Function.removeSeparatorComa(nominal_bayar));
                jsonKirim.put("nominal_janji_bayar", Function.removeSeparatorComa(nominal_janji_bayar));
                jsonKirim.put("tgl_janji_bayar", tgl_janji_bayar);
                jsonKirim.put("bertemu_dengan", bertemu_dengan);
                jsonKirim.put("kode_kunjungan", kode_kunjungan);
                jsonKirim.put("alasan", alasan);
                jsonKirim.put("unique_code", unique_code);
                jsonKirim.put("nasabah_bayar", nasabah_bayar);

                if (location == null) {
                    latitude = 0.0;
                    longitude = 0.0;
                    location = querySSL.getLastKnownLocation();
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                } else {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
                jsonKirim.put("latitude", latitude);
                jsonKirim.put("longitude", longitude);


                listPhoto = queryPhoto.getimageByNoLoan(noloan);
                for (Photo p : listPhoto) {
                    jsonPhoto = new JSONObject();
                    String ket = p.getKeterangan();
                    String url = p.getUrl_photo();
                    String waktu = p.getWaktu();
                    if (ket == null) {
                        ket = "";
                    }
                    if (url == null) {
                        url = "";
                    }
                    if (waktu == null) {
                        waktu = "";
                    }
                    jsonPhoto.put("keterangan", ket);
                    jsonPhoto.put("img", Function.bitmapToString(Function.bitmapFromPath(url)));
                    jsonPhoto.put("waktu-kunjungan", waktu);
                    jsonArrayImage.put(jsonPhoto);
                }
                jsonKirim.put("images", jsonArrayImage);
                jsonArray.put(jsonKirim);

                jsonAll.put("waktu", Function.getWaktuSekarangMilis());
                jsonAll.put("username", usernameShared);
                jsonAll.put("password", passwordShared);
                jsonAll.put("hasil_kunjungan", jsonArray);

                jsonStr = jsonAll.toString();
                longLog(jsonStr);
                System.out.println("Json Kirim " + jsonStr);
                resultFromJson = jsonParser.HttpRequestPost(Constant.SERVICE_HASIL_KUNJUNGAN, jsonStr, Constant.TimeOutConnection, new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));

            } catch (Exception e) {
                e.printStackTrace();
            }

            return resultFromJson;
        }
    }

    private void btnSubmitEvent() {
        btnSubmitMenuHasilKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                System.out.println("hasil location: " + manager.isProviderEnabled(LocationManager.GPS_PROVIDER));
                if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER) == false) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
                    // Setting Dialog Title
                    alertDialog.setTitle("Setting GPS");
                    // Setting Dialog Message
                    alertDialog.setMessage("GPS Belum Diaktifkan, Mohon Aktifkan GPS dan Set ke Mode High Accuracy");
                    // On pressing Settings button
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            mContext.startActivity(intent);
                        }
                    });
                    // Showing Alert Message
                    alertDialog.show();
                } else {
                    String ddAlamatNasabah = etDropdownAlamatNasabah.getText().toString();
                    if (ddAlamatNasabah.equals(Constant.SESUAI)) {
                        sesuai_alamat_nasabah = Constant.SESUAI;
                        alamat_baru = "";
                    } else if (ddAlamatNasabah.equals(Constant.TIDAK)) {
                        sesuai_alamat_nasabah = Constant.TIDAK;
                        alamat_baru = etAlamatBaruNasabahMenuHasilKunjungan.getText().toString();
                    }
                    String ddAlamatUsaha = etDropdownAlamatUsahaNasabah.getText().toString();
                    if (ddAlamatUsaha.equals(Constant.SESUAI)) {
                        sesuai_alamat_usaha = Constant.SESUAI;
                        alamat_usaha_baru = "";
                    } else if (ddAlamatUsaha.equals(Constant.TIDAK)) {
                        sesuai_alamat_usaha = Constant.TIDAK;
                        alamat_usaha_baru = etAlamatUsahaBaruNasabahMenuHasilKunjungan.getText().toString();
                    }

                    String ddTelepon = etDropdownNoTelpBaru.getText().toString();
                    if (ddTelepon.equals(Constant.SESUAI)) {
                        sesuai_telepon_usaha = Constant.SESUAI;
                        tlpn_usaha_baru = "";
                    } else if (ddTelepon.equals(Constant.TIDAK)) {
                        sesuai_telepon_usaha = Constant.TIDAK;
                        tlpn_usaha_baru = etNoTelpBaruNasabahMenuHasilKunjungan.getText().toString();
                    }

                    String ddEmail = etDropdownEmail.getText().toString();
                    if (ddEmail.equals(Constant.SESUAI)) {
                        sesuai_email = Constant.SESUAI;
                        email_baru = "";
                    } else if (ddEmail.equals(Constant.TIDAK)) {
                        sesuai_email = Constant.TIDAK;
                        email_baru = etEmailNasabahBaruMenuHasilKunjungan.getText().toString();
                    }

                    //bertemu nasabah tgl dan nominal janji bayar wajib, slain itu tidak
                    nama_debitur = etNamaDebiturMenuHasilKunjungan.getText().toString();
                    no_loan = etNoLoanMenuHasilKunjungan.getText().toString();
                    nasabah_bayar = etDropdownNasabahBayar.getText().toString();
                    bertemu_dengan = etBertemuDenganMenuHasilKunjungan.getText().toString();
                    tgl_janji_bayar = etTglJanjiBayarMenuHasilKunjungan.getText().toString();
                    nominal_janji_bayar = etNominalJanjiBayarMenuHasilKunjungan.getText().toString();
                    resume = etResumeSingkatNasabahMenuHasilKunjungan.getText().toString();
                    lokasi_bertemu = etLokasiBertemu.getText().toString();
                    karakter_nasabah = etKarakterNasabahMenuHasilKunjungan.getText().toString();
                    action_code = etDropdownActionCodeMenuHasilKunjungan.getText().toString();
                    shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                    nikShared = (shared.getString(Constant.NIK, ""));
                    usernameShared = (shared.getString(Constant.SHARED_USERNAME, ""));
                    passwordShared = (shared.getString(Constant.SHARED_PASSWORD, ""));

                    if (nasabah_bayar.equals("Janji Bayar")) {
                        if (nominal_janji_bayar.isEmpty() || tgl_janji_bayar.isEmpty()) {
                            Function.showAlert(mContext, "Mohon Isi Tanggal Janji Bayar dan Nominal Janji Bayar");
                        } else {
                            if (bertemu_dengan.equals("Nasabah")) {
                                nominal_bayar = "";
                                kode_kunjungan = "JB";
                                alasan = etDropdownAlasanTelatBayar.getText().toString();
                                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Konfirmasi Data");
                                builder.setMessage("Nama Debitur: \n" + nama_debitur + "\nNo Loan: \n" + no_loan + "\nNasabah Bayar: \n" + nasabah_bayar + "\nBertemu Dengan: \n" + bertemu_dengan + "\nTanggal Janji Bayar: \n" + tgl_janji_bayar + "\nNominal Janji Bayar: " + nominal_janji_bayar + "\nLokasi Bertemu: \n" + lokasi_bertemu + "\nKarakter Nasabah: \n" + karakter_nasabah + "\nAction Code: \n" + action_code + "\nAlasan: \n" + alasan + "\nResume Nasabah: \n" + resume);
                                builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new execSubmit(mContext).execute();
                                    }
                                });
                                builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int i) {
                                        dialog.dismiss();

                                    }
                                });
                                AlertDialog alert = builder.create();
                                alert.show();
                            } else if (!bertemu_dengan.equals("Nasabah")) {
                                nominal_bayar = "";
                                kode_kunjungan = "JB";
                                alasan = etDropdownAlasanTelatBayar.getText().toString() + "," + etAlasanTidakBertemuNasabahMenuHasilKunjungan.getText().toString();
                                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Konfirmasi Data");
                                builder.setMessage("Nama Debitur: \n" + nama_debitur + "\nNo Loan: \n" + no_loan + "\nNasabah Bayar: \n" + nasabah_bayar + "\nBertemu Dengan: \n" + bertemu_dengan + "\nTanggal Janji Bayar: \n" + tgl_janji_bayar + "\nNominal Janji Bayar: " + nominal_janji_bayar + "\nLokasi Bertemu: \n" + lokasi_bertemu + "\nKarakter Nasabah: \n" + karakter_nasabah + "\nAction Code: \n" + action_code + "\nAlasan: \n" + alasan + "\nResume Nasabah: \n" + resume);
                                builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new execSubmit(mContext).execute();
                                    }
                                });
                                builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int i) {
                                        dialog.dismiss();

                                    }
                                });
                                AlertDialog alert = builder.create();
                                alert.show();
                            }

                        }

                    } else if (nasabah_bayar.equals(Constant.TIDAK)) {
                        nominal_bayar = "";
                        if (bertemu_dengan.equals("Nasabah")) {

                            alasan = etDropdownAlasanTelatBayar.getText().toString();
                            kode_kunjungan = "NP";
                            final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setTitle("Konfirmasi Data");
                            builder.setMessage("Nama Debitur: \n" + nama_debitur + "\nNo Loan: \n" + no_loan + "\nNasabah Bayar: \n" + nasabah_bayar + "\nBertemu Dengan: \n" + bertemu_dengan + "\nTanggal Janji Bayar: \n" + tgl_janji_bayar + "\nNominal Janji Bayar: " + nominal_janji_bayar + "\nLokasi Bertemu: \n" + lokasi_bertemu + "\nKarakter Nasabah: \n" + karakter_nasabah + "\nAction Code: \n" + action_code + "\nAlasan: \n" + alasan + "\nResume Nasabah: \n" + resume);
                            builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    new execSubmit(mContext).execute();
                                }
                            });
                            builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int i) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alert = builder.create();
                            alert.show();

                        } else if (!bertemu_dengan.equals("Nasabah")) {
                            alasan = etAlasanTidakBertemuNasabahMenuHasilKunjungan.getText().toString() + "," + etDropdownAlasanTelatBayar.getText().toString();
                            kode_kunjungan = "NN";
                            final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setTitle("Konfirmasi Data");
                            builder.setMessage("Nama Debitur: \n" + nama_debitur + "\nNo Loan: \n" + no_loan + "\nNasabah Bayar: \n" + nasabah_bayar + "\nBertemu Dengan: \n" + bertemu_dengan + "\nTanggal Janji Bayar: \n" + tgl_janji_bayar + "\nNominal Janji Bayar: \n" + nominal_janji_bayar + "\nLokasi Bertemu: \n" + lokasi_bertemu + "\nKarakter Nasabah: \n" + karakter_nasabah + "\nAction Code: \n" + action_code + "\nAlasan: \n" + alasan + "\nResume Nasabah: \n" + resume);
                            builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    new execSubmit(mContext).execute();
                                }
                            });
                            builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int i) {
                                    dialog.dismiss();

                                }
                            });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    } else if (nasabah_bayar.equals("Ya")) {
                        nominal_bayar = etNominalYangDibayar.getText().toString();
                        String status = txtStatusPembayaran.getText().toString();
                        if (nominal_bayar.isEmpty()) {
                            Function.showAlert(mContext, "Mohon Isi Nominal Bayar");
                        } else {
                            if (status.equals("Partial Payment")) {
                                kode_kunjungan = "PP";
                                if (bertemu_dengan.equals("Nasabah")) {
                                    alasan = etDropdownAlasanNasabahTidakBayarFull.getText().toString();
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                    builder.setTitle("Konfirmasi Data");
                                    builder.setMessage("Nama Debitur: \n" + nama_debitur + "\nNo Loan: \n" + no_loan + "\nNasabah Bayar: \n" + nasabah_bayar + "\nBertemu Dengan: \n" + bertemu_dengan + "\nNominal Bayar: \n" + nominal_bayar + "\nLokasi Bertemu: \n" + lokasi_bertemu + "\nKarakter Nasabah: \n" + karakter_nasabah + "\nAction Code: \n" + action_code + "\nAlasan: \n" + alasan + "\nResume Nasabah: \n" + resume);
                                    builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            new execSubmit(mContext).execute();
                                        }
                                    });
                                    builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int i) {
                                            dialog.dismiss();
                                        }
                                    });
                                    AlertDialog alert = builder.create();
                                    alert.show();

                                    //new execSubmit(mContext).execute();
                                } else if (!bertemu_dengan.equals("Nasabah")) {
                                    alasan = etDropdownAlasanNasabahTidakBayarFull.getText().toString() + "," + etAlasanTidakBertemuNasabahMenuHasilKunjungan.getText().toString();
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                    builder.setTitle("Konfirmasi Data");
                                    builder.setMessage("Nama Debitur: " + nama_debitur + "\nNo Loan: \n" + no_loan + "\nNasabah Bayar: \n" + nasabah_bayar + "\nBertemu Dengan: \n" + bertemu_dengan + "\nNominal Bayar: \n" + nominal_bayar + "\nLokasi Bertemu: \n" + lokasi_bertemu + "\nKarakter Nasabah: \n" + karakter_nasabah + "\nAction Code: \n" + action_code + "\nAlasan: \n" + alasan + "\nResume Nasabah: \n" + resume);
                                    builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            new execSubmit(mContext).execute();
                                        }
                                    });
                                    builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int i) {
                                            dialog.dismiss();
                                        }
                                    });
                                    AlertDialog alert = builder.create();
                                    alert.show();
                                    //new execSubmit(mContext).execute();
                                }
                            } else if (status.equals("Full Payment")) {
                                kode_kunjungan = "FP";
                                if (bertemu_dengan.equals("Nasabah")) {
                                    alasan = "";
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                    builder.setTitle("Konfirmasi Data");
                                    builder.setMessage("Nama Debitur: \n" + nama_debitur + "\nNo Loan: \n" + no_loan + "\nNasabah Bayar: \n" + nasabah_bayar + "\nBertemu Dengan: \n" + bertemu_dengan + "\nNominal Bayar: \n" + nominal_bayar + "\nLokasi Bertemu: \n" + lokasi_bertemu + "\nKarakter Nasabah: \n" + karakter_nasabah + "\nAction Code: \n" + action_code + "\nResume Nasabah: \n" + resume);
                                    builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            new execSubmit(mContext).execute();
                                        }
                                    });
                                    builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int i) {
                                            dialog.dismiss();
                                        }
                                    });
                                    AlertDialog alert = builder.create();
                                    alert.show();
                                } else if (!bertemu_dengan.equals("Nasabah")) {
                                    alasan = etAlasanTidakBertemuNasabahMenuHasilKunjungan.getText().toString();
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                    builder.setTitle("Konfirmasi Data");
                                    builder.setMessage("Nama Debitur: \n" + nama_debitur + "\nNo Loan: \n" + no_loan + "\nNasabah Bayar: \n" + nasabah_bayar + "\nBertemu Dengan: \n" + bertemu_dengan + "\nNominal Bayar: \n" + nominal_bayar + "\nLokasi Bertemu: \n" + lokasi_bertemu + "\nKarakter Nasabah: \n" + karakter_nasabah + "\nAction Code: \n" + action_code + "\nAlasan: \n" + alasan + "\nResume Nasabah: \n" + resume);
                                    builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            new execSubmit(mContext).execute();
                                        }
                                    });
                                    builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int i) {
                                            dialog.dismiss();
                                        }
                                    });
                                    AlertDialog alert = builder.create();
                                    alert.show();
                                }
                            }
                        }
                    }
                    //alasantdkfull,alasantelatbayar,alasantidakbrtmunasabah
                }


            }
        });
    }

    private void getListActionCode() {
        etDropdownActionCodeMenuHasilKunjungan.setText(listAdapterActionCode.get(0).toString());
        etDropdownActionCodeMenuHasilKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MenuKunjunganRev.this, android.R.layout.simple_list_item_1, listAdapterActionCode);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Action Code");
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strName = arrayAdapter.getItem(i);
                        etDropdownActionCodeMenuHasilKunjungan.setText(strName);
                    }
                });
                builder.show();
            }
        });
    }

    private void getListBuktiPembayaranBy() {
        llPenggunaanKwitansi.setVisibility(View.GONE);
        etBuktiPembayaranBy.setText(listAdapterBuktiPembayaranBy.get(0).toString());
        etBuktiPembayaranBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MenuKunjunganRev.this, android.R.layout.simple_list_item_1, listAdapterBuktiPembayaranBy);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Bukti Pembayaran By");
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strName = arrayAdapter.getItem(i);
                        if (strName.equals("SMS/Email")) {
                            llPenggunaanKwitansi.setVisibility(View.GONE);
                        } else {
                            llPenggunaanKwitansi.setVisibility(View.VISIBLE);
                        }
                        etBuktiPembayaranBy.setText(strName);
                    }
                });
                builder.show();
            }
        });
    }

    private void getListKarakterNasabah() {
        etKarakterNasabahMenuHasilKunjungan.setText(listAdapterKarakterNasabah.get(0).toString());
        etKarakterNasabahMenuHasilKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MenuKunjunganRev.this, android.R.layout.simple_list_item_1, listAdapterKarakterNasabah);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Karakter Nasabah");
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strName = arrayAdapter.getItem(i);
                        etKarakterNasabahMenuHasilKunjungan.setText(strName);
                    }
                });
                builder.show();
            }
        });
    }

    private void getListLokasiBertemu() {
        etLokasiBertemu.setText(listAdapterLokasiBertemu.get(0).toString());
        etLokasiBertemu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MenuKunjunganRev.this, android.R.layout.simple_list_item_1, listAdapterLokasiBertemu);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Lokasi Bertemu");
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strName = arrayAdapter.getItem(i);
                        etLokasiBertemu.setText(strName);
                    }
                });
                builder.show();
            }
        });
    }


    private void getListBertemuDengan() {
        if (valuedropdownBertemuDengan.equals("Nasabah")) {
            txtAlasanTidakBertemuNasabah.setVisibility(View.GONE);
            etAlasanTidakBertemuNasabahMenuHasilKunjungan.setVisibility(View.GONE);
        } else {
            txtAlasanTidakBertemuNasabah.setVisibility(View.VISIBLE);
            etAlasanTidakBertemuNasabahMenuHasilKunjungan.setVisibility(View.VISIBLE);
        }

        etBertemuDenganMenuHasilKunjungan.setText(listAdapterBertemuDengan.get(0).toString());
        etBertemuDenganMenuHasilKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MenuKunjunganRev.this, android.R.layout.simple_list_item_1, listAdapterBertemuDengan);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Bertemu Dengan");
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strName = arrayAdapter.getItem(i);
                        String nsbhBayar = etDropdownNasabahBayar.getText().toString();
                        String status = txtStatusPembayaran.getText().toString();
                        if (strName.equals("Nasabah") && nsbhBayar.equals("Ya")) {
                            txtAlasanTidakBertemuNasabah.setVisibility(View.GONE);
                            etAlasanTidakBertemuNasabahMenuHasilKunjungan.setVisibility(View.GONE);
                            if (status.equals("Partial Payment")) {
                                txtAlasanNasabahTidakBayarFull.setVisibility(View.VISIBLE);
                                etDropdownAlasanNasabahTidakBayarFull.setVisibility(View.VISIBLE);
                            } else if (status.equals("Full Payment")) {
                                txtAlasanNasabahTidakBayarFull.setVisibility(View.GONE);
                                etDropdownAlasanNasabahTidakBayarFull.setVisibility(View.GONE);
                            }
                        } else if (!strName.equals("Nasabah") && nsbhBayar.equals("Ya")) {
                            txtAlasanTidakBertemuNasabah.setVisibility(View.VISIBLE);
                            etAlasanTidakBertemuNasabahMenuHasilKunjungan.setVisibility(View.VISIBLE);
                            txtAlasanNasabahTidakBayarFull.setVisibility(View.GONE);
                            etDropdownAlasanNasabahTidakBayarFull.setVisibility(View.GONE);
                        } else if (strName.equals("Nasabah") && nsbhBayar.equals("Tidak")) {
                            txtAlasanTidakBertemuNasabah.setVisibility(View.GONE);
                            etAlasanTidakBertemuNasabahMenuHasilKunjungan.setVisibility(View.GONE);
                        } else if (!strName.equals("Nasabah") && nsbhBayar.equals("Tidak")) {
                            txtAlasanTidakBertemuNasabah.setVisibility(View.VISIBLE);
                            etAlasanTidakBertemuNasabahMenuHasilKunjungan.setVisibility(View.VISIBLE);
                        } else if (strName.equals("Nasabah") && nsbhBayar.equals("Janji Bayar")) {
                            txtAlasanTidakBertemuNasabah.setVisibility(View.GONE);
                            etAlasanTidakBertemuNasabahMenuHasilKunjungan.setVisibility(View.GONE);
                        } else if (!strName.equals("Nasabah") && nsbhBayar.equals("Janji Bayar")) {
                            txtAlasanTidakBertemuNasabah.setVisibility(View.VISIBLE);
                            etAlasanTidakBertemuNasabahMenuHasilKunjungan.setVisibility(View.VISIBLE);
                        }

                        etBertemuDenganMenuHasilKunjungan.setText(strName);
                    }
                });
                builder.show();
            }
        });
    }

    private void getListAlasanTidakBertemuNasabah() {
        etAlasanTidakBertemuNasabahMenuHasilKunjungan.setText(listAdapterAlasanTidakBertemuNasabah.get(0).toString());
        etAlasanTidakBertemuNasabahMenuHasilKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MenuKunjunganRev.this, android.R.layout.simple_list_item_1, listAdapterAlasanTidakBertemuNasabah);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Alasan Tidak Bertemu Nasabah");
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strName = arrayAdapter.getItem(i);
                        etAlasanTidakBertemuNasabahMenuHasilKunjungan.setText(strName);
                    }
                });
                builder.show();
            }
        });
    }

    private void getListAlasanNasabahTelatBayar() {
        txtAlasanTelatBayar.setVisibility(View.GONE);
        etDropdownAlasanTelatBayar.setVisibility(View.GONE);
        if (valuedropdownNasabahBayar.equals("Ya")) {
            txtAlasanTelatBayar.setVisibility(View.GONE);
            etDropdownAlasanTelatBayar.setVisibility(View.GONE);
        } else if (valuedropdownNasabahBayar.equals("Tidak")) {
            txtAlasanTelatBayar.setVisibility(View.VISIBLE);
            etDropdownAlasanTelatBayar.setVisibility(View.VISIBLE);
        }
        etDropdownAlasanTelatBayar.setText(listAdapterAlasanNasabahTelatBayar.get(0).toString());
        etDropdownAlasanTelatBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MenuKunjunganRev.this, android.R.layout.simple_list_item_1, listAdapterAlasanNasabahTelatBayar);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Alasan Nasabah Telat Bayar");
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strName = arrayAdapter.getItem(i);
                        etDropdownAlasanTelatBayar.setText(strName);
                    }
                });
                builder.show();
            }
        });
    }


    private void getTglJanjiBayar() {
        etTglJanjiBayarMenuHasilKunjungan.setText("");
        etTglJanjiBayarMenuHasilKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialogFragment = new DatePickerDialogClass();
                dialogFragment.show(getFragmentManager(), "Date Picker Dialog");
            }
        });
    }

    public static class DatePickerDialogClass extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Locale locale = new Locale("in", "ID");
            Locale.setDefault(locale);
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            Date today = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(today);
            Calendar cMax = Calendar.getInstance();
            cMax.setTime(today);
            cMax.add(Calendar.DAY_OF_YEAR, +5); // Subtract 6 months
//            c.add(Calendar.MONTH, -6); // Subtract 6 months
            long minDate = c.getTime().getTime(); // Twice!
            long maxDate = cMax.getTime().getTime();
            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
            datepickerdialog.getDatePicker().setMinDate(minDate);
            datepickerdialog.getDatePicker().setMaxDate(maxDate);
            return datepickerdialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            etTglJanjiBayarMenuHasilKunjungan.setText(day + "-" + (month + 1) + "-" + year);
        }
    }


    private void getListAlasanNasabahTidakBayarFull() {
        etDropdownAlasanNasabahTidakBayarFull.setText(listAdapterAlasanNasabahTidakBayarFull.get(0).toString());
        etDropdownAlasanNasabahTidakBayarFull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MenuKunjunganRev.this, android.R.layout.simple_list_item_1, listAdapterAlasanNasabahTidakBayarFull);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                builder.setTitle("Alasan Nasabah Tidak Bayar Full");
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strName = arrayAdapter.getItem(i);
                        etDropdownAlasanNasabahTidakBayarFull.setText(strName);
                    }
                });
                builder.show();
            }
        });
    }

    private void getStatusPembayaran() {
        //totalKewajiban = "50,000,000.00";
        //etTotalKewajiban.setText(totalKewajiban);
        totalKewajiban = etTotalKewajiban.getText().toString();
        etNominalYangDibayar.addTextChangedListener(new NumberTextWatcherForThousand(etNominalYangDibayar));
        etNominalYangDibayar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    nominalDibayar = etNominalYangDibayar.getText().toString();
                    if (!nominalDibayar.isEmpty()) {
                        Double d = Double.parseDouble(Function.removeSeparatorComa(totalKewajiban));
                        int total = d.intValue();

                        if (Integer.parseInt(Function.removeSeparatorComa(nominalDibayar)) >= total) {
                            txtStatusPembayaran.setText("Full Payment");
                            txtAlasanNasabahTidakBayarFull.setVisibility(View.GONE);
                            etDropdownAlasanNasabahTidakBayarFull.setVisibility(View.GONE);
                            txtAlasanTelatBayar.setVisibility(View.GONE);
                            etDropdownAlasanTelatBayar.setVisibility(View.GONE);
                            llJanjiBayar.setVisibility(View.GONE);
                        } else if (Integer.parseInt(Function.removeSeparatorComa(nominalDibayar)) < total) {
                            txtStatusPembayaran.setText("Partial Payment");
                            llJanjiBayar.setVisibility(View.VISIBLE);
                            valuedropdownBertemuDengan = etBertemuDenganMenuHasilKunjungan.getText().toString();
                            if (valuedropdownBertemuDengan.equals("Nasabah")) {
                                txtAlasanNasabahTidakBayarFull.setVisibility(View.VISIBLE);
                                etDropdownAlasanNasabahTidakBayarFull.setVisibility(View.VISIBLE);
                            } else {
                                txtAlasanNasabahTidakBayarFull.setVisibility(View.GONE);
                                etDropdownAlasanNasabahTidakBayarFull.setVisibility(View.GONE);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void getListNasabahBayar() {
        etDropdownNasabahBayar.setText(listAdapterNasabahBayar.get(0).toString());
        etDropdownNasabahBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MenuKunjunganRev.this, android.R.layout.simple_list_item_1, listAdapterNasabahBayar);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Nasabah Bayar");
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strName = arrayAdapter.getItem(i);
                        if (strName.equals("Ya")) {
                            etDropdownNasabahBayar.setText(strName);
                            etNominalYangDibayar.setVisibility(View.VISIBLE);
                            txtAlasanTelatBayar.setVisibility(View.GONE);
                            etDropdownAlasanTelatBayar.setVisibility(View.GONE);

                        } else if (strName.equals(Constant.TIDAK)) {
                            txtStatusPembayaran.setText("No Payment");
                            etDropdownNasabahBayar.setText(strName);
                            etNominalYangDibayar.setVisibility(View.GONE);
                            txtAlasanTelatBayar.setVisibility(View.VISIBLE);
                            etDropdownAlasanTelatBayar.setVisibility(View.VISIBLE);
                            txtAlasanNasabahTidakBayarFull.setVisibility(View.GONE);
                            etDropdownAlasanNasabahTidakBayarFull.setVisibility(View.GONE);
                        } else if (strName.equals("Janji Bayar")) {
                            txtStatusPembayaran.setText("Janji Bayar");
                            etDropdownNasabahBayar.setText(strName);
                            etNominalYangDibayar.setVisibility(View.GONE);
                            txtAlasanTelatBayar.setVisibility(View.VISIBLE);
                            etDropdownAlasanTelatBayar.setVisibility(View.VISIBLE);
                            txtAlasanNasabahTidakBayarFull.setVisibility(View.GONE);
                            etDropdownAlasanNasabahTidakBayarFull.setVisibility(View.GONE);
                        }

                    }
                });
                builder.show();
            }
        });
    }

    private void getListEmailNasabah() {
        etEmailNasabahBaruMenuHasilKunjungan.setEnabled(false);
        etDropdownEmail.setText(listAdapter.get(0).toString());
        etDropdownEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MenuKunjunganRev.this, android.R.layout.simple_list_item_1, listAdapter);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Email Nasabah");
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strName = arrayAdapter.getItem(i);
                        if (strName.equals(Constant.SESUAI)) {
                            etDropdownEmail.setText(strName);
                            etEmailNasabahBaruMenuHasilKunjungan.setText("");
                            etEmailNasabahBaruMenuHasilKunjungan.setEnabled(false);
                        } else if (strName.equals(Constant.TIDAK)) {
                            etDropdownEmail.setText(strName);
                            etEmailNasabahBaruMenuHasilKunjungan.setEnabled(true);
                        }
                    }
                });
                builder.show();
            }
        });
    }

    private void getListNoTelpNasabah() {
//        etNoTelpNasabahMenuHasilKunjungan.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                final int DRAWABLE_LEFT = 0;
//                final int DRAWABLE_TOP = 1;
//                final int DRAWABLE_RIGHT = 2;
//                final int DRAWABLE_BOTTOM = 3;
////                final String notelp="08988989832";
//                final String notelp = etNoTelpNasabahMenuHasilKunjungan.getText().toString();
//                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
//
//                    if (motionEvent.getRawX() >= (etNoTelpNasabahMenuHasilKunjungan.getRight() - etNoTelpNasabahMenuHasilKunjungan.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                        // your action here
//                        if (!notelp.isEmpty()) {
//                            final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
//                            builder.setTitle("Konfirmasi Panggilan");
//                            builder.setMessage("Lakukan Panggilan Ke Nomor: " + notelp + " ?");
//                            builder.setPositiveButton("Batal", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    dialog.dismiss();
//                                }
//                            });
//                            builder.setNegativeButton("Ya", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int i) {
//                                    Intent callIntent = new Intent(Intent.ACTION_CALL);
//                                    callIntent.setData(Uri.parse("tel:" + notelp));
//                                    startActivity(callIntent);
//                                }
//                            });
//
//                            AlertDialog alert = builder.create();
//                            alert.show();
//                        }
//
//
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });
//
//        etNoTelpBaruNasabahMenuHasilKunjungan.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                final int DRAWABLE_LEFT = 0;
//                final int DRAWABLE_TOP = 1;
//                final int DRAWABLE_RIGHT = 2;
//                final int DRAWABLE_BOTTOM = 3;
//                // final String notelp="08988989832";
//                final String notelp = etNoTelpBaruNasabahMenuHasilKunjungan.getText().toString();
//                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
//                    if (motionEvent.getRawX() >= (etNoTelpBaruNasabahMenuHasilKunjungan.getRight() - etNoTelpBaruNasabahMenuHasilKunjungan.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                        // your action here
//                        if (!notelp.isEmpty()) {
//                            final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
//                            builder.setTitle("Konfirmasi Panggilan");
//                            builder.setMessage("Lakukan Panggilan Ke Nomor: " + notelp + " ?");
//                            builder.setPositiveButton("Batal", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    dialog.dismiss();
//                                }
//                            });
//                            builder.setNegativeButton("Ya", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int i) {
//                                    Intent callIntent = new Intent(Intent.ACTION_CALL);
//                                    callIntent.setData(Uri.parse("tel:" + notelp));
//                                    startActivity(callIntent);
//                                }
//                            });
//
//                            AlertDialog alert = builder.create();
//                            alert.show();
//                        }
//
//
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });

        etNoTelpBaruNasabahMenuHasilKunjungan.setEnabled(false);
        etDropdownNoTelpBaru.setText(listAdapter.get(0).toString());
        etDropdownNoTelpBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MenuKunjunganRev.this, android.R.layout.simple_list_item_1, listAdapter);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("No Telepon Nasabah");
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strName = arrayAdapter.getItem(i);
                        if (strName.equals(Constant.SESUAI)) {
                            etDropdownNoTelpBaru.setText(strName);
                            etNoTelpBaruNasabahMenuHasilKunjungan.setText("");
                            etNoTelpBaruNasabahMenuHasilKunjungan.setEnabled(false);
                        } else if (strName.equals(Constant.TIDAK)) {
                            etDropdownNoTelpBaru.setText(strName);
                            etNoTelpBaruNasabahMenuHasilKunjungan.setEnabled(true);
                        }
                    }
                });
                builder.show();
            }
        });
    }


    private void getListAlamatUsahaNasabah() {
        etAlamatUsahaBaruNasabahMenuHasilKunjungan.setEnabled(false);
        etDropdownAlamatUsahaNasabah.setText(listAdapter.get(0).toString());
        etDropdownAlamatUsahaNasabah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MenuKunjunganRev.this, android.R.layout.simple_list_item_1, listAdapter);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Status Alamat Usaha Nasabah");
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strName = arrayAdapter.getItem(i);
                        if (strName.equals(Constant.SESUAI)) {
                            etDropdownAlamatUsahaNasabah.setText(strName);
                            etAlamatUsahaBaruNasabahMenuHasilKunjungan.setText("");
                            etAlamatUsahaBaruNasabahMenuHasilKunjungan.setEnabled(false);
                        } else if (strName.equals(Constant.TIDAK)) {
                            etDropdownAlamatUsahaNasabah.setText(strName);
                            etAlamatUsahaBaruNasabahMenuHasilKunjungan.setEnabled(true);
                        }
                    }
                });
                builder.show();
            }
        });
    }

    private void getListAlamatNasabah() {
//        listAdapter.add("Testing Kalau Teksnya dipanjangin apakah \n bakal muncul semua \n tes lagi \n tes lagi");
//        listAdapter.add("\"Kooperatif, Bisa Ditemui dan ditelepon\", \"Kooperatif, Susah Ditemui Tapi Bisa Ditelepon\", \"Cukup Kooperatif, Tidak Bisa Ditemui Tapi Bisa Ditelepon\", \"Cukup Kooperatif, Bisa Ditemui Tapi Tidak Bisa Ditelepon\", \"Tidak Kooperatif\"");
        etAlamatBaruNasabahMenuHasilKunjungan.setEnabled(false);
        etDropdownAlamatNasabah.setText(listAdapter.get(0).toString());
        etDropdownAlamatNasabah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MenuKunjunganRev.this, android.R.layout.simple_list_item_1, listAdapter);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Status Alamat Nasabah");
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strName = arrayAdapter.getItem(i);
                        if (strName.equals(Constant.SESUAI)) {
                            etDropdownAlamatNasabah.setText(strName);
                            etAlamatBaruNasabahMenuHasilKunjungan.setText("");
                            etAlamatBaruNasabahMenuHasilKunjungan.setEnabled(false);
                        } else if (strName.equals(Constant.TIDAK)) {
                            etDropdownAlamatNasabah.setText(strName);
                            etAlamatBaruNasabahMenuHasilKunjungan.setEnabled(true);
                        }
                    }
                });
                builder.show();
            }
        });
    }


    private void initEvent() {

    }

}
