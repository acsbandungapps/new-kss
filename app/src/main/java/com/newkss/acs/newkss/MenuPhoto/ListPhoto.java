package com.newkss.acs.newkss.MenuPhoto;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.newkss.acs.newkss.Adapter.ListPhotoAdapter;
import com.newkss.acs.newkss.Menu.MenuLogin;
import com.newkss.acs.newkss.Model.Photo;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by acs on 6/12/17.
 */

public class ListPhoto extends ParentActivity {
    ListPhotoAdapter adapter;
    ArrayList<Photo> list;
    ListView listPhoto;
    Photo p;
    String filename, path;
    Button btnTambahPhoto;
    String iddetinv;
    Uri uriSavedImage;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;

    Photo ph;
    Photo photodata;
    Context mContext;
    ImageView imgBackMenuListPhoto;
    LinearLayout llListPhotoPopup;
    SharedPreferences shared;

    TextView txtListPhoto,txtDivisiListPhoto;
    String namaShared,divisiShared;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listphoto);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = this;
        initUI();
    }

    private void initUI() {
        txtListPhoto=(TextView) findViewById(R.id.txtListPhoto);
        txtDivisiListPhoto=(TextView) findViewById(R.id.txtDivisiListPhoto);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtListPhoto.setText(namaShared);
            txtDivisiListPhoto.setText(divisiShared);
        }

        llListPhotoPopup = (LinearLayout) findViewById(R.id.llListPhotoPopup);
        llListPhotoPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(ListPhoto.this, llListPhotoPopup);
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_REFRESH)) {
                            initUI();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            Intent i = new Intent(mContext, MenuLogin.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });
        btnTambahPhoto = (Button) findViewById(R.id.btnTambahPhoto);
        listPhoto = (ListView) findViewById(R.id.listPhoto);
        imgBackMenuListPhoto = (ImageView) findViewById(R.id.imgBackMenuListPhoto);
        imgBackMenuListPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Intent i = getIntent();
        iddetinv = i.getStringExtra("id");

        p = new Photo();
        MySQLiteHelper.initDB(mContext);
        list = new ArrayList<>();

        list = p.getImageFromSqlite(iddetinv);
        //  list = p.getImageFromSqlite();
        for (Photo pp : list) {
            System.out.println("NAH: " + pp.getUrl_photo());
        }
        adapter = new ListPhotoAdapter(ListPhoto.this, list);
        listPhoto.setAdapter(adapter);

        btnTambahPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture(view);
                adapter.notifyDataSetChanged();
            }
        });

    }

    public void takePicture(View view) {

        //camera stuff
        Intent imageIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        //folder stuff
        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "KSP");
        imagesFolder.mkdirs();

        filename = iddetinv + "_" + timeStamp + ".png";
        File image = new File(imagesFolder, filename);
        uriSavedImage = Uri.fromFile(image);

        path = image.toString();
        System.out.println("PATH: " + image.getPath());

        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        startActivityForResult(imageIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                File dir = new File(Environment.getExternalStorageDirectory(), "KSP");
                Bitmap b = BitmapFactory.decodeFile(path);
                Bitmap out = Bitmap.createScaledBitmap(b, 320, 480, false);

                File file = new File(dir, filename);
                if (file.exists()) {
                    file.delete();
                }
                FileOutputStream fOut;
                try {
                    fOut = new FileOutputStream(file);
                    out.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();

                    ph = new Photo();
                    photodata = new Photo();

                    photodata.setNama_photo(filename);
                    photodata.setUrl_photo(path);
                    photodata.setId_detail_inventory(iddetinv);
                    ph.insertPhotoIntoSqlite(photodata);
                    list = ph.getImageFromSqlite(iddetinv);
                    adapter = new ListPhotoAdapter(ListPhoto.this, list);
                    listPhoto.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        // Toast.makeText(getApplicationContext(), "coba 200", Toast.LENGTH_SHORT).show();
//        System.out.println(requestCode+"  ---   "+resultCode+"    "+data);
//        if (requestCode == 100) {
//            if (resultCode == RESULT_OK) {
//                File dir = new File(Environment.getExternalStorageDirectory(), "KSP");
//                Bitmap b = BitmapFactory.decodeFile(path);
//                Bitmap out = Bitmap.createScaledBitmap(b, 320, 480, false);
//
//                File file = new File(dir, filename);
//                if (file.exists()) {
//                    file.delete();
//                }
//                FileOutputStream fOut;
//                try {
//                    fOut = new FileOutputStream(file);
//                    out.compress(Bitmap.CompressFormat.PNG, 100, fOut);
//                    fOut.flush();
//                    fOut.close();
//
//                    MySQLiteHelper.initDB(getApplicationContext());
//                    ph = new Photo();
//                    photodata = new Photo();
//
//                    photodata.setNama_photo(filename);
//                    photodata.setUrl_photo(path);
//                    photodata.setId_detail_bucket(d.getId_detail_bucket());
//                    ph.insertPhotoIntoSqlite(photodata);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//                //imageView.setImageBitmap(out);
//            }


//        } else if (requestCode == REQ_CHANGE) {
//            if (resultCode == RESULT_OK) {
//                Bundle extras = data.getExtras();
//                String hasil= (String) extras.get("INIDATA");
//                System.out.println("Hasil: "+hasil);
//              //  String hasil=data.getStringExtra("INIDATA");
//                Toast.makeText(getApplicationContext(), hasil, Toast.LENGTH_SHORT).show();
//            }


//        }
    }


    @Override
    public void onBackPressed() {

    }
}
