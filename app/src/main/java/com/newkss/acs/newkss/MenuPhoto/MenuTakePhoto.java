package com.newkss.acs.newkss.MenuPhoto;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.newkss.acs.newkss.Menu.MenuDataOffline;
import com.newkss.acs.newkss.Menu.MenuLogin;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.Photo;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by acs on 6/19/17.
 */

public class MenuTakePhoto extends ParentActivity {

    ImageView imgHasilPhotoMenuTakePhoto, imgBackMenuTakePhoto;
    Button btnAmbilPhotoMenuTakePhoto, btnKeGaleriMenuTakePhoto;
    Context mContext;

    private Uri uriSavedImage;
    String path;
    String filename;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    SharedPreferences shared;
    String namaShared, divisiShared;
    Photo ph;
    Photo photodata;

    String iddetinv, noloan;
    Detail_Inventory queryDI = new Detail_Inventory();
    LinearLayout llMenuTakePhotoPopup;

    TextView txtNamaMenuTakePhoto,txtDivisiMenuTakePhoto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menutakephoto);
        initUI();
    }

    private void initUI() {
        mContext = this;
        MySQLiteHelper.initDB(mContext);
        Intent i = getIntent();
        iddetinv = i.getStringExtra("id");
        noloan = queryDI.getNoLoanfromIdDetInv(iddetinv);


        txtNamaMenuTakePhoto=(TextView) findViewById(R.id.txtNamaMenuTakePehoto);
        txtDivisiMenuTakePhoto=(TextView) findViewById(R.id.txtDivisiMenuTakePhoto);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtNamaMenuTakePhoto.setText(namaShared);
            txtDivisiMenuTakePhoto.setText(divisiShared);
        }


        llMenuTakePhotoPopup = (LinearLayout) findViewById(R.id.llMenuTakePhotoPopup);
        llMenuTakePhotoPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MenuTakePhoto.this, llMenuTakePhotoPopup);
                popup.getMenuInflater().inflate(R.menu.popup_inbox, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_DATAPENDING)) {
                            Intent i = new Intent(getApplicationContext(), MenuDataOffline.class);
                            startActivity(i);
                            finish();
                        }
                        else if (menuItem.getTitle().equals(Constant.POPUP_REFRESH)) {
                            initUI();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            Intent i = new Intent(mContext, MenuLogin.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });
        imgBackMenuTakePhoto = (ImageView) findViewById(R.id.imgBackMenuTakePhoto);
        imgBackMenuTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        imgHasilPhotoMenuTakePhoto = (ImageView) findViewById(R.id.imgHasilPhotoMenuTakePhoto);
        btnAmbilPhotoMenuTakePhoto = (Button) findViewById(R.id.btnAmbilPhotoMenuTakePhoto);
        btnKeGaleriMenuTakePhoto = (Button) findViewById(R.id.btnKeGaleriMenuTakePhoto);

        btnAmbilPhotoMenuTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture(view);
            }
        });

        btnKeGaleriMenuTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), ListPhoto.class);
                i.putExtra("id", iddetinv);
                startActivity(i);
            }
        });
    }

    public void takePicture(View view) {

        //camera stuff
        Intent imageIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        //folder stuff
        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "KSP");
        if (!imagesFolder.exists()) {
            imagesFolder.mkdirs();
        }

        if (iddetinv.equals("upload")) {
            filename = iddetinv + "_" + timeStamp + ".png";
            noloan = "upload";
        } else {
            filename = noloan + "_" + timeStamp + ".png";
        }

        //filename="DB-0000007"+ "_" + timeStamp + ".png";
        File image = new File(imagesFolder, filename);
        uriSavedImage = Uri.fromFile(image);

        path = image.toString();
        System.out.println("PATH: " + image.getPath());

        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        startActivityForResult(imageIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                File dir = new File(Environment.getExternalStorageDirectory(), "KSP");
                Bitmap b = BitmapFactory.decodeFile(path);
                Bitmap out = Bitmap.createScaledBitmap(b, 320, 480, false);

                File file = new File(dir, filename);
                if (file.exists()) {
                    file.delete();
                }
                FileOutputStream fOut;
                try {
                    fOut = new FileOutputStream(file);
                    out.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();
//
                    MySQLiteHelper.initDB(mContext);
                    ph = new Photo();
                    photodata = new Photo();

                    photodata.setNama_photo(filename);
                    photodata.setUrl_photo(path);
                    photodata.setId_detail_inventory(iddetinv);
                    photodata.setWaktu(Function.getDateNow());
                    photodata.setNoloan(noloan);
                    photodata.setStatus("0");
                    ph.insertPhotoIntoSqlite(photodata);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                imgHasilPhotoMenuTakePhoto.setImageBitmap(out);
            }
        }
    }

    @Override
    public void onBackPressed() {

    }
}
