package com.newkss.acs.newkss.MenuSettlement;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.newkss.acs.newkss.Adapter.MenuSettlementAdapter;
import com.newkss.acs.newkss.Adapter.MenuSettlementAdapterKosong;
import com.newkss.acs.newkss.Adapter.MenuSettlementBayarAdapter;
import com.newkss.acs.newkss.DataTransferInterface;
import com.newkss.acs.newkss.Menu.MenuDataOffline;
import com.newkss.acs.newkss.Menu.MenuInisialisasi;
import com.newkss.acs.newkss.Menu.MenuLogin;
import com.newkss.acs.newkss.Menu.MenuPerformance;
import com.newkss.acs.newkss.Menu.MenuUtama;
import com.newkss.acs.newkss.MenuKunjungan.MenuHasilKunjungan;
import com.newkss.acs.newkss.MenuPhoto.MenuTakePhoto;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.Master_Settle;
import com.newkss.acs.newkss.Model.Penagihan;
import com.newkss.acs.newkss.Model.Pending;
import com.newkss.acs.newkss.Model.Photo;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.Model.Settlement;
import com.newkss.acs.newkss.Model.SettlementKe;
import com.newkss.acs.newkss.ModelBaru.Detil_TGT;
import com.newkss.acs.newkss.ModelBaru.TGT;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Service.ServiceSendLocation;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.DialogNewPassword;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.JSONParser;
import com.newkss.acs.newkss.Util.MySQLiteHelper;
import com.newkss.acs.newkss.Util.NumberTextWatcherForThousand;
import com.newkss.acs.newkss.Util.NumberTextWathcerSettlement;
import com.newkss.acs.newkss.Util.OnCompleteListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
import java.util.StringTokenizer;

/**
 * Created by Erdy on 5/8/2017.
 */
public class MenuSettlement extends ParentActivity implements OnCompleteListener {
    ListView listDaftarSettlement;
    ArrayList<Settle_Detil> settlementDetilList = new ArrayList<>();
    MenuSettlementAdapter adapter;
    MenuSettlementAdapterKosong adapterKosong;
    Context mContext;
    String current, namaShared, divisiShared, usernameShared, passwordShared, nikShared, settleke, nominalDibayar, sisaSettle;
    String totalNominal = "";
    ImageView imgBackMenuSettlement;
    Button btnUpload, btnSubmitMenuSettlement, btnSelesaiMenuSettlement;
    EditText etNominalMenuSettlement, etSettlementKe, etNominalDisetor, etNominalBelumdiSetor;

    Settle_Detil querySettleDetil = new Settle_Detil();
    Settlement querySettle = new Settlement();

    TextView txtNamaMenuSettlement, txtDivisiMenuSettlement;
    ProgressDialog progressDialog;
    SharedPreferences shared;

    ServiceSendLocation querySSL;
    String resultfromJson = "";
    JSONParser jsonParser = new JSONParser();
    Master_Settle queryMS = new Master_Settle();
    Master_Settle dataMS;
    Settlement dataSettle;
    Photo queryPhoto = new Photo();

    ArrayList<Photo> listPhoto = new ArrayList<>();
    LinearLayout llMenuSettlementPopup;
    Pending dataPending;
    Pending queryPending = new Pending();
    String jsonStr = "";

    int sisaBelumSettle = 0;
    String sudahDibayar = "";
    Location location;
    Double latitude = 0.0;
    Double longitude = 0.0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menusettlement);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = this;
        MySQLiteHelper.initDB(mContext);
        querySSL = new ServiceSendLocation(mContext);

        try {
            initUI();
            initEvent();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initUI() {
        etNominalMenuSettlement = (EditText) findViewById(R.id.etNominalMenuSettlement);

        etSettlementKe = (EditText) findViewById(R.id.etSettlementKe);
        etNominalBelumdiSetor = (EditText) findViewById(R.id.etNominalBelumdiSetor);
        etNominalDisetor = (EditText) findViewById(R.id.etNominalDisetor);

        listDaftarSettlement = (ListView) findViewById(R.id.listDaftarSettlement);
        txtDivisiMenuSettlement = (TextView) findViewById(R.id.txtDivisiMenuSettlement);
        txtNamaMenuSettlement = (TextView) findViewById(R.id.txtNamaMenuSettlement);
        btnSubmitMenuSettlement = (Button) findViewById(R.id.btnSubmitMenuSettlement);
        btnSelesaiMenuSettlement = (Button) findViewById(R.id.btnSelesaiMenuSettlement);
        btnUpload = (Button) findViewById(R.id.btnUpload);
        etNominalDisetor.addTextChangedListener(new NumberTextWathcerSettlement(etNominalDisetor));


    }

    private void initEvent() {
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtNamaMenuSettlement.setText(namaShared);
            txtDivisiMenuSettlement.setText(divisiShared);
        }
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuTakePhoto.class);
                i.putExtra("id", "upload");
                startActivity(i);
            }
        });
        llMenuSettlementPopup = (LinearLayout) findViewById(R.id.llMenuSettlementPopup);
        llMenuSettlementPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MenuSettlement.this, llMenuSettlementPopup);
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_REFRESH)) {
                            initUI();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            if (querySettleDetil.isSettleAvailable().equals("0")) {
                                System.out.println("Check:" + querySettleDetil.isSettleAvailable());
                                Intent i = new Intent(getApplicationContext(), MenuLogin.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                                finish();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Pesan");
                                builder.setMessage("Mohon Lakukan Settle Terlebih Dahulu");
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            }
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });
        imgBackMenuSettlement = (ImageView) findViewById(R.id.imgBackMenuSettlement);
        imgBackMenuSettlement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (querySettleDetil.isSettleAvailable().equals("0")) {
//                    Intent i = new Intent(MenuSettlement.this, MenuUtama.class);
//                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(i);
//                    finish();
//                } else {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
//                    builder.setTitle("Pesan");
//                    builder.setMessage("Mohon Lakukan Settle Terlebih Dahulu");
//                    builder.setIcon(R.drawable.ic_warning_black_24dp);
//                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//
//                        }
//                    });
//                    AlertDialog alert1 = builder.create();
//                    alert1.show();
//                }
                Intent i = new Intent(MenuSettlement.this, MenuUtama.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });


        final String setKe = querySettle.getAngkaSettlementKe();
        etSettlementKe.setText(setKe);
        settlementDetilList = querySettleDetil.getAllDataSettlementDetil();
        totalNominal = querySettleDetil.getTotalNominalSettlement();

        if (settlementDetilList.isEmpty()) {
            btnSelesaiMenuSettlement.setEnabled(false);
            listDaftarSettlement.setEmptyView(findViewById(R.id.emptyElement));
            etNominalMenuSettlement.setText("0");
        } else {
            adapter = new MenuSettlementAdapter(mContext, settlementDetilList);
            listDaftarSettlement.setAdapter(adapter);
            setListViewHeightBasedOnChildren(listDaftarSettlement);
            etNominalMenuSettlement.setText(Function.returnSeparatorComaWithout(totalNominal));
        }
        if (totalNominal != null) {
            sudahDibayar = querySettle.getCustomSudahDibayar();

            sisaBelumSettle = Integer.parseInt(totalNominal) - Integer.parseInt(sudahDibayar);

            if (sisaBelumSettle < 0) {
                etNominalBelumdiSetor.setText("0");
            } else if (sisaBelumSettle == 0) {
                if(settlementDetilList.isEmpty()){
                    btnSelesaiMenuSettlement.setEnabled(false);
                    btnSubmitMenuSettlement.setEnabled(false);

                }else{
                    btnSelesaiMenuSettlement.setEnabled(true);
                    btnSubmitMenuSettlement.setEnabled(false);

                }

                etNominalBelumdiSetor.setText("0");
            } else {
                btnSelesaiMenuSettlement.setEnabled(false);
                etNominalBelumdiSetor.setText(Function.returnSeparatorComaWithout(String.valueOf(sisaBelumSettle)));
            }
        } else {
            etNominalMenuSettlement.setText("0");
        }


        btnSubmitMenuSettlement.setOnClickListener(new View.OnClickListener()

                                                   {
                                                       @Override
                                                       public void onClick(View view) {
                                                        // querySettle.deleteCustomValueSettle();
                                                           if (settlementDetilList.isEmpty()) {
                                                               Function.showAlert(mContext, "Belum ada data yang di collect");
                                                           } else if (etNominalDisetor.getText().toString().isEmpty()) {
                                                               Function.showAlert(mContext, "Mohon Isi Nominal");
                                                           } else {
                                                               totalNominal = querySettleDetil.getTotalNominalSettlement();
                                                               sudahDibayar = querySettle.getCustomSudahDibayar();
                                                               String valNominal = etNominalDisetor.getText().toString();
                                                               if (Integer.parseInt(totalNominal) - Integer.parseInt(sudahDibayar) < 0) {
                                                                   //total dikurang yang sudah dibayar kurang dari 0
                                                                   Function.showAlert(mContext, "Total Setoran Sudah Melebihi Total Yang Di Collect");
                                                                   etNominalBelumdiSetor.setText("0");
                                                               } else if (Integer.parseInt(Function.removeSeparatorComa(valNominal)) > Integer.parseInt(totalNominal)) {
                                                                   //nominal setoran diinput lbh besar dari total nominal collect
                                                                   Function.showAlert(mContext, "Total Setoran Sudah Melebihi Total Yang Di Collect");
                                                               } else if (Integer.parseInt(totalNominal) - (Integer.parseInt(sudahDibayar) + Integer.parseInt(Function.removeSeparatorComa(valNominal))) < 0) {
                                                                   //dana yg disetor lebih besar drpd yg di collect
                                                                   Function.showAlert(mContext, "Total Setoran Sudah Melebihi Total Yang Di Collect");
                                                               } else {
//                                                                   btnSelesaiMenuSettlement.setEnabled(true);
//                                                                   etNominalMenuSettlement.setEnabled(false);
//                                                                   btnSubmitMenuSettlement.setEnabled(false);


                                                                   dataSettle = new Settlement();
                                                                   int valset = Integer.parseInt(setKe);

                                                                   dataSettle.setStatus("0");
                                                                   dataSettle.setSettle_ke(String.valueOf(valset));
                                                                   dataSettle.setSudah_dibayar(Function.removeSeparatorComa(valNominal));
                                                                   querySettle.insertSettlement(dataSettle);
                                                                   final String setKe = querySettle.getAngkaSettlementKe();
                                                                   etSettlementKe.setText(setKe);

                                                                   totalNominal = querySettleDetil.getTotalNominalSettlement();
                                                                   sudahDibayar = querySettle.getCustomSudahDibayar();
                                                                   sisaBelumSettle = Integer.parseInt(totalNominal) - Integer.parseInt(sudahDibayar);
                                                                   if(sisaBelumSettle==0){
                                                                       btnSelesaiMenuSettlement.setEnabled(true);
                                                                       btnSubmitMenuSettlement.setEnabled(false);
                                                                   }
                                                             etNominalBelumdiSetor.setText(Function.returnSeparatorComaWithout(String.valueOf(sisaBelumSettle)));
                                                                   etNominalDisetor.setText("");
                                                               }
                                                           }
                                                       }
                                                   }

        );

        btnSelesaiMenuSettlement.setOnClickListener(new View.OnClickListener()

                                                    {
                                                        @Override
                                                        public void onClick(View view) {

                                                            String sudahDibayar1 = querySettle.getCustomSudahDibayar();
                                                            settleke = etSettlementKe.getText().toString();
                                                            totalNominal = querySettleDetil.getTotalNominalSettlement();
                                                            nominalDibayar = etNominalDisetor.getText().toString();
                                                            if (settlementDetilList.isEmpty()) {
                                                                Function.showAlert(mContext, "Belum ada data yang di collect");
                                                            } else if (totalNominal == null) {
                                                                Function.showAlert(mContext, "Belum ada data yang di collect");
                                                            }
                                                            //total nominal dikurangin brp yang sudah dibayar tidak 0
//                                                            else if (Integer.parseInt(totalNominal) - Integer.parseInt(sudahDibayar1) != 0) {
                                                            else if (Integer.parseInt(totalNominal) - Integer.parseInt(sudahDibayar1) > 0) {
                                                                Function.showAlert(mContext, "Masih Ada Dana Yang Belum Disetor, Mohon Submit Sisa Dana Yang Dicollect");
//                                                            } else if (totalNominal != null && !settlementDetilList.isEmpty() && Integer.parseInt(totalNominal) - Integer.parseInt(sudahDibayar1) == 0) {
                                                            } else if (Integer.parseInt(totalNominal) - Integer.parseInt(sudahDibayar1) == 0) {
                                                                StringBuffer sb = new StringBuffer();
                                                                int count = 0;
                                                                for (Settle_Detil s : settlementDetilList) {
                                                                    //insert ke settle
                                                                    dataSettle = new Settlement();
                                                                    dataSettle.setSettle_ke(settleke);
                                                                    dataSettle.setNo_rek(s.getNo_rek());
                                                                    dataSettle.setNama(s.getNama());
                                                                    dataSettle.setNo_loan(s.getNo_loan());
                                                                    dataSettle.setStatus("1");
                                                                    dataSettle.setSudah_dibayar(s.getNominal_bayar());
                                                                    dataSettle.setTotal_settle(Function.returnDoubleBulat(NumberTextWatcherForThousand.trimCommaOfString(totalNominal)));
                                                                    if (count == 0) {
                                                                        sb.append("Nama: \n" + s.getNama()).append("\nNo Rek:\n" + s.getNo_rek()).append("\nNominal: \n" + Function.returnSeparatorComaWithout(s.getNominal_bayar()));
                                                                    } else {
                                                                        sb.append("\nNama: \n" + s.getNama()).append("\nNo Rek:\n" + s.getNo_rek()).append("\nNominal: \n" + Function.returnSeparatorComaWithout(s.getNominal_bayar()));
                                                                    }
                                                                    count++;
                                                                }
                                                                sb.append("\nTotal: \n" + Function.returnSeparatorComaWithout(totalNominal));
                                                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                                                builder.setTitle("Konfirmasi Data");
                                                                builder.setMessage(sb);
                                                                builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                                        new sendSettlement(mContext).execute();
                                                                    }
                                                                });
                                                                builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                                        dialogInterface.dismiss();
                                                                    }
                                                                });
                                                                AlertDialog alert = builder.create();
                                                                alert.show();
                                                            }
                                                        }
                                                    }

        );

    }


    private void initUI1() {
        etNominalMenuSettlement = (EditText) findViewById(R.id.etNominalMenuSettlement);


        etSettlementKe = (EditText) findViewById(R.id.etSettlementKe);
        etNominalBelumdiSetor = (EditText) findViewById(R.id.etNominalBelumdiSetor);
        etNominalDisetor = (EditText) findViewById(R.id.etNominalDisetor);
        etNominalDisetor.addTextChangedListener(new NumberTextWathcerSettlement(etNominalDisetor));
        listDaftarSettlement = (ListView) findViewById(R.id.listDaftarSettlement);
        txtDivisiMenuSettlement = (TextView) findViewById(R.id.txtDivisiMenuSettlement);
        txtNamaMenuSettlement = (TextView) findViewById(R.id.txtNamaMenuSettlement);

        final String setKe = querySettle.getAngkaSettlementKe();
        etSettlementKe.setText(setKe);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtNamaMenuSettlement.setText(namaShared);
            txtDivisiMenuSettlement.setText(divisiShared);
        }

        addSettlement();
        totalNominal = querySettleDetil.getTotalNominalSettlement();
        //etNominalBelumdiSetor.setText(querySettle.getCustomSudahDibayar());
        if (!totalNominal.isEmpty()) {
//            Double d = new Double(NumberTextWatcherForThousand.trimCommaOfString(totalNominal));
//            int val = d.intValue();
            sudahDibayar = querySettle.getCustomSudahDibayar();
            sisaBelumSettle = Integer.parseInt(totalNominal) - Integer.parseInt(sudahDibayar);

            etNominalBelumdiSetor.setText(Function.returnSeparatorComaWithout(String.valueOf(sisaBelumSettle)));
        } else {
            etNominalMenuSettlement.setText("0");
        }


        if (settlementDetilList.isEmpty()) {
            listDaftarSettlement.setEmptyView(findViewById(R.id.emptyElement));
        } else {
            adapter = new MenuSettlementAdapter(mContext, settlementDetilList);
            listDaftarSettlement.setAdapter(adapter);
            setListViewHeightBasedOnChildren(listDaftarSettlement);
        }
        if (querySettleDetil.getTotalNominalSettlement() == null) {
            etNominalMenuSettlement.setText("0");
        } else {
            String totalnominal = querySettleDetil.getTotalNominalSettlement().toString();
            etNominalMenuSettlement.setText(Function.returnSeparatorComaWithout(totalnominal));
        }


        llMenuSettlementPopup = (LinearLayout) findViewById(R.id.llMenuSettlementPopup);
        llMenuSettlementPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MenuSettlement.this, llMenuSettlementPopup);
                popup.getMenuInflater().inflate(R.menu.popup_inbox, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_DATAPENDING)) {
                            Intent i = new Intent(getApplicationContext(), MenuDataOffline.class);
                            startActivity(i);
                            finish();
                        }
                        else if (menuItem.getTitle().equals(Constant.POPUP_REFRESH)) {
                            initUI();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            if (querySettleDetil.isSettleAvailable().equals("0")) {
                                System.out.println("Check:" + querySettleDetil.isSettleAvailable());
                                Intent i = new Intent(getApplicationContext(), MenuLogin.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                                finish();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Pesan");
                                builder.setMessage("Mohon Lakukan Settle Terlebih Dahulu");
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            }


                        }
                        return true;
                    }
                });
                popup.show();
            }
        });
        btnSubmitMenuSettlement = (Button) findViewById(R.id.btnSubmitMenuSettlement);


        btnUpload = (Button) findViewById(R.id.btnUpload);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuTakePhoto.class);
                i.putExtra("id", "upload");
                startActivity(i);
            }
        });


        btnSubmitMenuSettlement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // querySettle.deleteCustomValueSettle();
                if (settlementDetilList.isEmpty()) {
                    Function.showAlert(mContext, "Belum ada data yang di collect");
                } else if (etNominalDisetor.getText().toString().isEmpty()) {
                    Function.showAlert(mContext, "Mohon Isi Nominal");
                } else {
                    dataSettle = new Settlement();
                    int valset = Integer.parseInt(setKe);
                    String valNominal = etNominalDisetor.getText().toString();
                    dataSettle.setStatus("0");
                    dataSettle.setSettle_ke(String.valueOf(valset));
                    dataSettle.setSudah_dibayar(Function.removeSeparatorComa(valNominal));
                    querySettle.insertSettlement(dataSettle);
                    System.out.println("querySettle.getAngkaSettlementKe() " + querySettle.getAngkaSettlementKe());
                    final String setKe = querySettle.getAngkaSettlementKe();
                    etSettlementKe.setText(setKe);


                    sudahDibayar = querySettle.getCustomSudahDibayar();
                    sisaBelumSettle = Integer.parseInt(totalNominal) - Integer.parseInt(sudahDibayar);

                    etNominalBelumdiSetor.setText(Function.returnSeparatorComaWithout(String.valueOf(sisaBelumSettle)));

                    etNominalDisetor.setText("");
                }


            }
        });


        btnSelesaiMenuSettlement = (Button) findViewById(R.id.btnSelesaiMenuSettlement);
        btnSelesaiMenuSettlement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast(mContext,"tes");
                settleke = etSettlementKe.getText().toString();
                totalNominal = etNominalMenuSettlement.getText().toString();
                //sisaSettle = etNominalBelumdiSetor.getText().toString();
                nominalDibayar = etNominalDisetor.getText().toString();

//                if (settlementDetilList.isEmpty()) {
//                    Function.showAlert(mContext, "Belum ada data yang di collect");
//                } else if (nominalDibayar.isEmpty()) {
//                    Function.showAlert(mContext, "Mohon Isi Nominal");
//                } else if (totalNominal.equals("0")) {
//                    new sendSettlement(mContext).execute();
//                }

                final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                System.out.println("hasil location: " + manager.isProviderEnabled(LocationManager.GPS_PROVIDER));
                if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER) == false) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
                    // Setting Dialog Title
                    alertDialog.setTitle("Setting GPS");
                    // Setting Dialog Message
                    alertDialog.setMessage("GPS Belum Diaktifkan, Mohon Aktifkan GPS dan Set ke Mode High Accuracy");
                    // On pressing Settings button
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            mContext.startActivity(intent);
                        }
                    });
                    // Showing Alert Message
                    alertDialog.show();
                } else {
                    if (!totalNominal.isEmpty() && !settlementDetilList.isEmpty()) {
                        String value = "";
                        StringBuffer sb = new StringBuffer();
                        int count = 0;
                        for (Settle_Detil s : settlementDetilList) {
                            //insert ke settle
                            dataSettle = new Settlement();
                            dataSettle.setSettle_ke(settleke);
                            dataSettle.setNo_rek(s.getNo_rek());
                            dataSettle.setNama(s.getNama());
                            dataSettle.setNo_loan(s.getNo_loan());
                            dataSettle.setStatus("1");
                            dataSettle.setSudah_dibayar(s.getNominal_bayar());
                            dataSettle.setTotal_settle(Function.returnDoubleBulat(NumberTextWatcherForThousand.trimCommaOfString(totalNominal)));
                            if (count == 0) {
                                sb.append("Nama: \n" + s.getNama()).append("\nNo Rek:\n" + s.getNo_rek()).append("\nNominal: \n" + Function.returnSeparatorComa(s.getNominal_bayar()));
                            } else {
                                sb.append("\nNama: \n" + s.getNama()).append("\nNo Rek:\n" + s.getNo_rek()).append("\nNominal: \n" + Function.returnSeparatorComa(s.getNominal_bayar()));
                            }
                            count++;
                        }
                        sb.append("\nNominal Disetor: \n" + totalNominal);
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setTitle("Konfirmasi Data");
                        builder.setMessage(sb);
                        builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                new sendSettlement(mContext).execute();
                            }
                        });
                        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();


                    }
                }

            }
        });
        imgBackMenuSettlement = (ImageView) findViewById(R.id.imgBackMenuSettlement);
        imgBackMenuSettlement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MenuSettlement.this, MenuUtama.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });


    }

    @Override
    public void onComplete(String value) {
        System.out.println("Value: " + value);
        passwordShared = Function.convertStringtoMD5(value);


        new sendSettlement(mContext).execute();
    }

    class sendSettlement extends AsyncTask<String, JSONObject, String> {

        Context context;

        public sendSettlement(Context mContext) {
            this.context = mContext;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Mengirim Data Settlement...");
            progressDialog.setTitle("Pesan");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonPhoto = new JSONObject();
            shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
            usernameShared = (shared.getString(Constant.SHARED_USERNAME, ""));
            passwordShared = (shared.getString(Constant.SHARED_PASSWORD, ""));
            nikShared = (shared.getString(Constant.NIK, ""));
            JSONObject jsonAll = new JSONObject();
            JSONArray jsonSetor = new JSONArray();
            JSONArray jsonNasabah = new JSONArray();
            JSONObject jsonObSetor = new JSONObject();
            JSONObject jsonObNasabah = new JSONObject();

            JSONArray jsonTagihan = new JSONArray();
            JSONObject jsonObTagihan = new JSONObject();
            try {
                querySSL = new ServiceSendLocation(context);
                location = querySSL.getLocation();
                listPhoto = queryPhoto.getimageByNoLoan("upload");
                String url = "";
                for (Photo p : listPhoto) {
                    jsonPhoto = new JSONObject();
                    url = p.getUrl_photo();
                    if (url == null) {
                        url = "";
                    }
                }
                for (Settle_Detil s : settlementDetilList) {
                    System.out.println("ISI SETTLE: " + s.getNama());
                    jsonObNasabah = new JSONObject();
                    jsonObTagihan = new JSONObject();

                    jsonObTagihan.put("no_loan", s.getNo_loan());


                    jsonObNasabah.put("nama", s.getNama());
                    jsonObNasabah.put("nominal_bayar", s.getNominal_bayar());
                    jsonObNasabah.put("no_rekening", s.getNo_rek());
                    System.out.println("Nama " + s.getNama() + " --- Nominal " + s.getNominal_bayar() + " ---- No Rek " + s.getNo_rek());
                    jsonNasabah.put(jsonObNasabah);
                    jsonTagihan.put(jsonObTagihan);
                }


                jsonObSetor.put("deskripsi", "setor ke " + settleke);
                System.out.println("NILAI TOTAL BAYAR: " + NumberTextWatcherForThousand.trimCommaOfString(nominalDibayar));
//                jsonObSetor.put("total_bayar", NumberTextWatcherForThousand.trimCommaOfString(nominalDibayar));

                Double d = new Double(Function.removeSeparatorComa(totalNominal));
                int i = d.intValue();
                jsonObSetor.put("nominal_setor", totalNominal);
                if (url.isEmpty()) {
                    jsonObSetor.put("upload_setor", "");
                } else {
                    jsonObSetor.put("upload_setor", Function.bitmapToString(Function.bitmapFromPath(url)));
                }

                jsonSetor.put(jsonObSetor);
                System.out.println("ini jsonNasabah: " + jsonNasabah);
                jsonAll.put("setor", jsonSetor);
                jsonAll.put("nasabah", jsonNasabah);
                jsonAll.put("tagihan_settle", jsonTagihan);
                jsonAll.put("username", usernameShared);
                jsonAll.put("password", passwordShared);

                Double dd = Double.parseDouble(Function.removeSeparatorComa(totalNominal));
                int ii = dd.intValue();

                jsonAll.put("total_nominal_bayar", String.valueOf(ii));

//                Integer.parseInt(NumberTextWatcherForThousand.trimCommaOfString(totalNominal))


                // jsonAll.put("sisa_blom_settle", Function.returnDoubleBulat(NumberTextWatcherForThousand.trimCommaOfString(sisaSettle)));
                jsonAll.put("waktu", Function.getWaktuSekarangMilis());

                if (location == null) {
                    latitude = 0.0;
                    longitude = 0.0;
                    location = querySSL.getLastKnownLocation();
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                } else {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
                jsonAll.put("latitude", latitude);
                jsonAll.put("longitude", longitude);

                jsonStr = jsonAll.toString();

//                System.out.println("Hasil Json: " + jsonStr);
//                System.out.println("Header: " + Function.getHeader(mContext, nikShared));
//                System.out.println("Header Base 64: " + new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));
//                System.out.println("URL: " + Constant.SERVICE_SETTLE);
                resultfromJson = jsonParser.HttpRequestPost(Constant.SERVICE_SETTLE, jsonStr, Constant.TimeOutConnection, new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));
//                System.out.println("RETURN JSON: " + resultfromJson);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result.contains(Constant.CONNECTION_LOST)) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                builder.setTitle("Pesan");
//                builder.setMessage("Gagal Mengirim Karena Gangguan Koneksi. Data akan dikirim kembali saat jaringan tersedia");
//                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                        try {
//                            dataPending = new Pending();
//                            dataPending.setWaktu(Function.getDateNow());
//                            dataPending.setData(jsonStr);
//                            dataPending.setUrl(Constant.SERVICE_SETTLE);
//                            dataPending.setHeader(new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));
//                            dataPending.setStatus("2");
//                            dataPending.setJson_update("upload");
//                            dataPending.setKet_update("Settle");
//                            queryPending.insertPendingData(dataPending);
//                            queryPhoto.updateStatusPhoto("upload");
//
//                            //misal data sukses
//                            dataMS = new Master_Settle();
//                            dataMS.setTotal(Function.returnDoubleBulat(NumberTextWatcherForThousand.trimCommaOfString(totalNominal)));
//                            dataMS.setSudah_dibayar(NumberTextWatcherForThousand.trimCommaOfString(nominalDibayar));
//                            dataMS.setStatus("1");
//                            queryMS.insertMasterSettlement(dataMS);
//
//                            for (Settle_Detil s : settlementDetilList) {
//                                //insert ke settle
//                                dataSettle = new Settlement();
//                                dataSettle.setSettle_ke(settleke);
//                                dataSettle.setNo_rek(s.getNo_rek());
//                                dataSettle.setNama(s.getNama());
//                                dataSettle.setNo_loan(s.getNo_loan());
//                                dataSettle.setStatus("1");
//                                dataSettle.setSudah_dibayar(s.getNominal_bayar());
//                                dataSettle.setTotal_settle(Function.returnDoubleBulat(NumberTextWatcherForThousand.trimCommaOfString(totalNominal)));
//                                querySettle.insertSettlement(dataSettle);
//                            }
//
//                            queryPhoto.updateStatusPhoto("upload");
//                            querySettleDetil.updateStatusSettleDetil();
//
//
//                            Intent i = new Intent(mContext, MenuUtama.class);
//                            startActivity(i);
//                            finish();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//
//                    }
//                });
//
//                AlertDialog alert1 = builder.create();
//                alert1.show();


                Function.showAlert(mContext, result);

            } else if (result.contains(Constant.CONNECTION_ERROR)) {
                Function.showAlert(mContext, Constant.CONNECTION_ERROR);
            } else if (result.isEmpty()) {
                Function.showAlert(mContext, Constant.CONNECTION_EMPTY);
            } else if (result == null) {
                Function.showAlert(mContext, Constant.CONNECTION_EMPTY);
            } else {
                try {
                    JSONObject jsonR = new JSONObject(result);
                    String keterangan = jsonR.getString("keterangan");
                    String rc = jsonR.getString("rc");
                    if (rc.equals("00")) {
                        //insert data tsb ke menu master settle
                        dataMS = new Master_Settle();
                        dataMS.setTotal(Function.returnDoubleBulat(NumberTextWatcherForThousand.trimCommaOfString(totalNominal)));
                        dataMS.setSudah_dibayar(nominalDibayar);
                        dataMS.setStatus("1");
                        queryMS.insertMasterSettlement(dataMS);

//                        JSONObject master=new JSONObject();
//                        master.put("total",Function.returnDoubleBulat(NumberTextWatcherForThousand.trimCommaOfString(totalNominal)));
//                        master.put("sudahdibayar",nominalDibayar);
//                        master.put("status","1");
//                        JSONArray ja=new JSONArray();


//                        for (Settle_Detil s : settlementDetilList) {
//                            //insert ke settle
//                            dataSettle = new Settlement();
//                            dataSettle.setSettle_ke(settleke);
//                            dataSettle.setNo_rek(s.getNo_rek());
//                            dataSettle.setNama(s.getNama());
//                            dataSettle.setNo_loan(s.getNo_loan());
//                            dataSettle.setStatus("1");
//                            dataSettle.setSudah_dibayar(s.getNominal_bayar());
//                            dataSettle.setTotal_settle(Function.returnDoubleBulat(NumberTextWatcherForThousand.trimCommaOfString(totalNominal)));
//                            querySettle.insertSettlement(dataSettle);
//                        }

                        querySettle.updateStatusSettlement();
                        queryPhoto.updateStatusPhoto("upload");
                        querySettleDetil.updateStatusSettleDetil();
                        Function.deleteImage();
                        //Function.showAlert(mContext, keterangan);
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage("Settlement Berhasil (" + rc + ")");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                shared = getApplicationContext().getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                                SharedPreferences.Editor editor = shared.edit();
                                editor.putString(Constant.SHARED_PASSWORD, passwordShared);
                                editor.commit();


                                Intent i = new Intent(mContext, MenuUtama.class);
                                startActivity(i);
                                finish();


                            }
                        });
                        builder.setCancelable(false);
                        AlertDialog alert1 = builder.create();
                        alert1.show();


                    } else if (rc.equals("T1")) {
//                        DialogNewPassword dialog1 = DialogNewPassword.newInstance();
//                        dialog1.show(getFragmentManager(), "MyDialog");
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage(keterangan + " (" + rc + ")");
                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                shared.edit().clear().commit();
                                Intent i = new Intent(MenuSettlement.this, MenuLogin.class);
                                startActivity(i);
                                finish();
                            }
                        });

                        AlertDialog alert1 = builder.create();
                        alert1.show();
                    } else if (rc.equals("T2")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage(keterangan + " (" + rc + ")");
                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                shared.edit().clear().commit();
                                Intent i = new Intent(MenuSettlement.this, MenuLogin.class);
                                startActivity(i);
                                finish();
                            }
                        });

                        AlertDialog alert1 = builder.create();
                        alert1.show();
                    } else if (rc.equals("T3")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage(keterangan + " (" + rc + ")");
                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                shared.edit().clear().commit();
                                Intent i = new Intent(MenuSettlement.this, MenuLogin.class);
                                startActivity(i);
                                finish();
                            }
                        });

                        AlertDialog alert1 = builder.create();
                        alert1.show();
                    } else if (rc.equals("T4")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage(keterangan + " (" + rc + ")");
                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                shared.edit().clear().commit();
                                Intent i = new Intent(MenuSettlement.this, MenuLogin.class);
                                startActivity(i);
                                finish();
                            }
                        });

                        AlertDialog alert1 = builder.create();
                        alert1.show();
                    } else if (rc.equals("T5")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage(keterangan + " (" + rc + ")");
                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                                shared.edit().clear().commit();
                                Intent i = new Intent(MenuSettlement.this, MenuLogin.class);
                                startActivity(i);
                                finish();
                            }
                        });

                        AlertDialog alert1 = builder.create();
                        alert1.show();
                    } else {
                        Function.showAlert(mContext, keterangan + " (" + rc + ")");
                    }
                } catch (JSONException e) {
                    Function.showAlert(mContext, result);
                    e.printStackTrace();
                } catch (Exception e) {
                    Function.showAlert(mContext, e.getMessage());
                    e.printStackTrace();
                }
            }
        }

    }

    private void addSettlement() {
        settlementDetilList = querySettleDetil.getAllDataSettlementDetil();
    }


    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    @Override
    public void onBackPressed() {

    }
}