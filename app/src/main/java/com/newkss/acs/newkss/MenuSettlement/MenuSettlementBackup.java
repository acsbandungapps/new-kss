package com.newkss.acs.newkss.MenuSettlement;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.newkss.acs.newkss.Adapter.MenuSettlementAdapter;
import com.newkss.acs.newkss.Adapter.MenuSettlementBayarAdapter;
import com.newkss.acs.newkss.DataTransferInterface;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.Penagihan;
import com.newkss.acs.newkss.Model.Settlement;
import com.newkss.acs.newkss.Model.SettlementKe;
import com.newkss.acs.newkss.ModelBaru.Detil_TGT;
import com.newkss.acs.newkss.ModelBaru.TGT;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erdy on 7/2/2017.
 */

public class MenuSettlementBackup extends Activity implements DataTransferInterface{
    @Override
    public void setValues(ArrayList<SettlementKe> al) {

    }
//    ArrayList<Settlement> settlementList;
//    ArrayList<SettlementKe> settlementListBayar;
//    ListView listDaftarSettlement, listSettlementKe;
//    MenuSettlementAdapter adapter;
//    MenuSettlementBayarAdapter adapterBayar;
//    Context mContext;
//    ImageView imgTambahListMenuSettlement;
//    Button btnSelesaiMenuSettlement, btnSubmitMenuSettlement;
//
//    Detail_Inventory d = new Detail_Inventory();
//
//    ImageView imgBackMenuSettlement;
//    Settlement querySettlement = new Settlement();
//    Settlement dataSettlement = new Settlement();
//
//    EditText etNominalMenuSettlement;
//
//    ArrayList<Settlement> listSettlementAmbil = new ArrayList<>();
//    ArrayList<SettlementKe> listnilaiNominal = new ArrayList<>();
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.menusettlement);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
//        mContext = this;
//
//        try {
//            MySQLiteHelper.initDB(mContext);
//            imgBackMenuSettlement = (ImageView) findViewById(R.id.imgBackMenuSettlement);
//            imgBackMenuSettlement.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    finish();
//                }
//            });
//
//            etNominalMenuSettlement = (EditText) findViewById(R.id.etNominalMenuSettlement);
//            //etNominalMenuSettlement.setText(querySettlement.getTotalNominalSettlement());
//            initUI();
//            addSettlement();
//            addSettlementKe();
//            listDaftarSettlement = (ListView) findViewById(R.id.listDaftarSettlement);
//            listSettlementKe = (ListView) findViewById(R.id.listSettlementKe);
//
//            adapter = new MenuSettlementAdapter(mContext, settlementList);
//            adapterBayar = new MenuSettlementBayarAdapter(mContext, settlementListBayar, this);
//            listDaftarSettlement.setAdapter(adapter);
//            listSettlementKe.setAdapter(adapterBayar);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    private void addSettlement() {
//
//        settlementList = new ArrayList<>();
//        //settlementList = querySettlement.getAllDataSettlement();
////        settlementList.add(new Settlement("Risya", "01234569087", "1.000.000"));
////        settlementList.add(new Settlement("Agung", "98765430089", "500.000"));
//    }
//
//    private void addSettlementKe() {
//        settlementListBayar = new ArrayList<>();
//        settlementListBayar.add(new SettlementKe("1", ""));
//
//    }
//
//    private void initUI() {
//        imgTambahListMenuSettlement = (ImageView) findViewById(R.id.imgTambahListMenuSettlement);
//        imgTambahListMenuSettlement.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                settlementListBayar.add(new SettlementKe("", ""));
////                adapterBayar = new MenuSettlementBayarAdapter(mContext, settlementListBayar);
////                listSettlementKe.setAdapter(adapterBayar);
//                adapterBayar.notifyDataSetChanged();
//            }
//        });
//
//        btnSubmitMenuSettlement = (Button) findViewById(R.id.btnSubmitMenuSettlement);
//        btnSubmitMenuSettlement.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listSettlementAmbil=new ArrayList<Settlement>();
//                for (int i = 0; i < adapter.getCount(); i++) {
//                    // dataSettlement = (Settlement) adapter.getItem(i);
//                    listSettlementAmbil.add((Settlement) adapter.getItem(i));
//                }
//
//                for (Settlement s : listSettlementAmbil) {
//                   // System.out.println("ISI SETTLE: " + s.getNamaSettlement());
//                }
//
//
////                for (int i = 0; i < adapterBayar.getCount(); i++) {
////                    adapterBayar.notifyDataSetChanged();
////                    // dataSettlement = (Settlement) adapter.getItem(i);
////                    listnilaiNominal.add((SettlementKe) adapterBayar.getItem(i));
////                }
//
//                for (SettlementKe s : listnilaiNominal) {
//                    System.out.println("ISI BAYAR: " + s.getNominal());
//                }
//
//
//            }
//        });
//        btnSelesaiMenuSettlement = (Button) findViewById(R.id.btnSelesaiMenuSettlement);
//        btnSelesaiMenuSettlement.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // putJson();
////                MySQLiteHelper.initDB(mContext);
////                Detil_TGT dt = new Detil_TGT();
////                System.out.println("Jumlah TGT: " + dt.getCountnpay());
////                System.out.println("Jumlah TGT: " + dt.getCountfpay());
////                System.out.println("Jumlah TGT: " + dt.getCountnvst());
////                System.out.println("Jumlah TGT: " + dt.getCountppay());
//
//
//                String json = "{\"waktu\":\"02-07-2017 18:59:10\",\"status_kunjungan\":[{\"status\":\"OK\",\"no_loan\":\"LD1616500015\"}],\"keterangan\":\"OK\",\"rc\":\"00\"}";
//
//                try {
//                    JSONObject jsonObject = new JSONObject(json);
//                    JSONArray jArr = jsonObject.getJSONArray("status_kunjungan");
//                    JSONArray jArr1 = new JSONArray(jArr.toString());
//                    String n = jArr1.getString(0);
//                    JSONObject j = new JSONObject(n);
//                    String status = j.getString("status");
//                    String noloan = j.getString("no_loan");
//                    System.out.println(status + " ---- " + noloan);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        });
//    }
//
//    private void putJson() {
//        try {
//            MySQLiteHelper.initApp(mContext);
//            MySQLiteHelper.initDB(mContext);
//            JSONObject jsonAll = new JSONObject(Constant.contohJson);
//            String waktu = jsonAll.getString("waktu");
//            String keterangan = jsonAll.getString("keterangan");
//            String pencairan = jsonAll.getString("pencairan");
//            String penagihan = jsonAll.getString("penagihan");
//            String rc = jsonAll.getString("rc");
//
//            System.out.println("Json Waktu: " + waktu);
//            System.out.println("Json Keterangan: " + keterangan);
//            System.out.println("Json Pencairan: " + pencairan);
//            System.out.println("Json PenagihanLama: " + penagihan);
//            System.out.println("Json RC: " + rc);
//
//            //DATA PENCAIRAN
//            JSONObject jsonPencairan = new JSONObject(pencairan);
//            JSONArray jsonArrayPencairan = jsonPencairan.getJSONArray("data_pencairan");
//            System.out.println("JsonArrayPencairan: " + jsonArrayPencairan);
//            JSONArray jsonArrayPencairan1 = new JSONArray(jsonArrayPencairan.toString());
//
//            System.out.println("JsonArrayPencairan1: " + jsonArrayPencairan1);
//            for (int i = 0; i < jsonArrayPencairan1.length(); i++) {
//                String dtlPencairan = jsonArrayPencairan1.getString(i);
//                System.out.println("NILAI: " + i + "-" + dtlPencairan);
//
//                JSONObject jsonDetilPencairan = new JSONObject(dtlPencairan);
//                String cycleDetilPencairan = jsonDetilPencairan.getString("cycle");
//                String detilInventoryPencairan = jsonDetilPencairan.getString("detail_inventory");
//                String jarakDetilPencairan = jsonDetilPencairan.getString("jarak");
//                String osDetilPencairan = jsonDetilPencairan.getString("OS");
//                String namaDetilPencairan = jsonDetilPencairan.getString("nama");
//                String alamatDetilPencairan = jsonDetilPencairan.getString("alamat");
//
//
//                System.out.println("cycleDetilPencairan: " + cycleDetilPencairan);
//                System.out.println("detilInventoryPencairan: " + detilInventoryPencairan);
//                System.out.println("jarakDetilPencairan: " + jarakDetilPencairan);
//                System.out.println("osDetilPencairan: " + osDetilPencairan);
//                System.out.println("namaDetilPencairan: " + namaDetilPencairan);
//                System.out.println("alamatDetilPencairan: " + alamatDetilPencairan);
//
//                JSONArray jsonArrDetInvPencairan = new JSONArray(detilInventoryPencairan.toString());
//                for (int j = 0; j < jsonArrDetInvPencairan.length(); j++) {
//                    String isiDetInvPencairan = jsonArrDetInvPencairan.getString(j);
//                    JSONObject jsonIsiDetInvPencairan = new JSONObject(isiDetInvPencairan);
//
//                    System.out.println("Saldo Min: " + j + "-" + jsonIsiDetInvPencairan.getString("saldo_min"));
//                    //String saldoMin = jsonIsiDetInvPencairan.getString("saldo_min");
//                    //MASUKIN KE DETAIL INVENTORY DAN KE DETAIL BUCKET DARI NILAI DATA PENCAIRAN
//
//                }
//            }
//
//            JSONArray jsonArraySS = jsonPencairan.getJSONArray("ss");
//            JSONArray jsonArraySS1 = new JSONArray(jsonArraySS.toString());
//            System.out.println("Jsonarrayss: " + jsonArraySS1);
//            for (int i = 0; i < jsonArraySS1.length(); i++) {
//                String noloan = jsonArraySS1.getString(i);
//                JSONObject jsonNoLoanSS = new JSONObject(noloan);
//                System.out.println("No_Loan ss: " + jsonNoLoanSS.getString("no_loan"));
//            }
//
//            JSONArray jsonArrayBI = jsonPencairan.getJSONArray("bi");
//            JSONArray jsonArrayBI1 = new JSONArray(jsonArrayBI.toString());
//            System.out.println("Jsonarraybi: " + jsonArrayBI1);
//            for (int i = 0; i < jsonArrayBI1.length(); i++) {
//                String noloan = jsonArrayBI1.getString(i);
//                JSONObject jsonNoLoanBI = new JSONObject(noloan);
//                System.out.println("No_Loan bi: " + jsonNoLoanBI.getString("no_loan"));
//            }
//
//            JSONArray jsonArrayBS = jsonPencairan.getJSONArray("bs");
//            JSONArray jsonArrayBS1 = new JSONArray(jsonArrayBS.toString());
//            System.out.println("Jsonarraybs: " + jsonArrayBS1);
//            for (int i = 0; i < jsonArrayBS1.length(); i++) {
//                String noloan = jsonArrayBS1.getString(i);
//                JSONObject jsonNoLoanBS = new JSONObject(noloan);
//                System.out.println("No_Loan bs: " + jsonNoLoanBS.getString("no_loan"));
//            }
//
//            JSONArray jsonArrayBLBS = jsonPencairan.getJSONArray("blbs");
//            JSONArray jsonArrayBLBS1 = new JSONArray(jsonArrayBLBS.toString());
//            System.out.println("Jsonarrayblbs: " + jsonArrayBLBS1);
//            for (int i = 0; i < jsonArrayBLBS1.length(); i++) {
//                String noloan = jsonArrayBLBS1.getString(i);
//                JSONObject jsonNoLoanBLBS = new JSONObject(noloan);
//                System.out.println("No_Loan blbs: " + jsonNoLoanBLBS.getString("no_loan"));
//            }
//
//            //DATA PENAGIHAN
//            JSONObject jsonPenagihan = new JSONObject(penagihan);
//            JSONArray jsonArrayPenagihan = jsonPenagihan.getJSONArray("tgt");
//            System.out.println("JsonArrayPenagihan: " + jsonArrayPenagihan);
//            JSONArray jsonArrayPenagihan1 = new JSONArray(jsonArrayPenagihan.toString());
//            System.out.println("JsonArrayPenagihan1: " + jsonArrayPenagihan1);
//            TGT queryTGT = new TGT();
//            Penagihan queryPenagihan = new Penagihan();
////            String noloangenerated = d.generateNoLoanSqlite();
//            for (int i = 0; i < jsonArrayPenagihan1.length(); i++) {
//                String dtlPenagihan = jsonArrayPenagihan1.getString(i);
////                System.out.println("NILAI: " + i + "-" + dtlPenagihan);
//
//                JSONObject jsonDetilPenagihan = new JSONObject(dtlPenagihan);
//                String cycleDetilPenagihan = jsonDetilPenagihan.getString("cycle");
//                String detilInventoryPenagihan = jsonDetilPenagihan.getString("detail_inventory");
//                String jarakDetilPenagihan = jsonDetilPenagihan.getString("jarak");
//                String osDetilPenagihan = jsonDetilPenagihan.getString("OS");
//                String namaDetilPenagihan = jsonDetilPenagihan.getString("nama");
//                String alamatDetilPenagihan = jsonDetilPenagihan.getString("alamat");
//
//
//                String id_tgt_generate = queryTGT.getIdTGTSqlite().id_tgt;
//                TGT dataTGT = new TGT();
//                dataTGT.setId_tgt(id_tgt_generate);
//                dataTGT.setCycle(cycleDetilPenagihan);
//                dataTGT.setJarak(jarakDetilPenagihan);
//                dataTGT.setOs(osDetilPenagihan);
//                dataTGT.setNama(namaDetilPenagihan);
//                dataTGT.setAlamat(alamatDetilPenagihan);
////                queryTGT.deleteAllTGT();
//                queryTGT.insertIdTGT(dataTGT);
//
//
////                System.out.println("cycleDetilPenagihan: "+cycleDetilPenagihan);
////                System.out.println("detilInventoryPenagihan: "+detilInventoryPenagihan);
////                System.out.println("jarakDetilPenagihan: "+jarakDetilPenagihan);
////                System.out.println("osDetilPenagihan: "+osDetilPenagihan);
////                System.out.println("namaDetilPenagihan: "+namaDetilPenagihan);
////                System.out.println("alamatDetilPenagihan: "+alamatDetilPenagihan);
//
//
//                JSONArray jsonArrDetInvPenagihan = new JSONArray(detilInventoryPenagihan.toString());
//                for (int j = 0; j < jsonArrDetInvPenagihan.length(); j++) {
//                    String isiDetInvPenagihan = jsonArrDetInvPenagihan.getString(j);
//                    JSONObject jsonIsiDetInvPenagihan = new JSONObject(isiDetInvPenagihan);
//
//                    System.out.println("Saldo Min PenagihanLama : " + j + "-" + jsonIsiDetInvPenagihan.getString("saldo_min"));
//                    String saldoMin = jsonIsiDetInvPenagihan.getString("saldo_min");
//                    //MASUKIN KE DETAIL INVENTORY DAN KE DETAIL BUCKET DARI NILAI DATA PENAGIHAN
//
//
//                    Detail_Inventory dd = new Detail_Inventory();
//                    dd.setSaldo_min(jsonIsiDetInvPenagihan.getString(Constant.SALDO_MIN));
//                    dd.setDpd_today(jsonIsiDetInvPenagihan.getString(Constant.DPD_TODAY));
//                    dd.setResume_nsbh(jsonIsiDetInvPenagihan.getString(Constant.RESUME_NSBH));
//                    dd.setAlmt_rumah(jsonIsiDetInvPenagihan.getString(Constant.ALMT_RUMAH));
//                    dd.setTgl_janji_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.TGL_JANJI_BAYAR_TERAKHIR));
//                    dd.setDenda(jsonIsiDetInvPenagihan.getString(Constant.DENDA));
//                    dd.setPola_bayar(jsonIsiDetInvPenagihan.getString(Constant.POLA_BAYAR));
//                    dd.setNo_hp(jsonIsiDetInvPenagihan.getString(Constant.NO_HP));
//                    dd.setTgl_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.TGL_BAYAR_TERAKHIR));
//                    dd.setAngsuran_ke(jsonIsiDetInvPenagihan.getString(Constant.ANGSURAN_KE));
//                    dd.setNama_upliner(jsonIsiDetInvPenagihan.getString(Constant.NAMA_UPLINER));
//                    dd.setPokok(jsonIsiDetInvPenagihan.getString(Constant.POKOK));
//                    dd.setTgl_sp(jsonIsiDetInvPenagihan.getString(Constant.TGL_SP));
//                    dd.setTenor(jsonIsiDetInvPenagihan.getString(Constant.TENOR));
//                    dd.setAngsuran(jsonIsiDetInvPenagihan.getString(Constant.ANGSURAN));
//                    dd.setGender(jsonIsiDetInvPenagihan.getString(Constant.GENDER));
//                    dd.setTgl_ptp(jsonIsiDetInvPenagihan.getString(Constant.TGL_PTP));
//                    dd.setAction_plan(jsonIsiDetInvPenagihan.getString(Constant.ACTION_PLAN));
//                    dd.setNo_rekening(jsonIsiDetInvPenagihan.getString(Constant.NO_REKENING));
//                    dd.setHistori_sp(jsonIsiDetInvPenagihan.getString(Constant.HISTORI_SP));
//                    dd.setNominal_janji_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR));
//                    dd.setTlpn_rekomendator(jsonIsiDetInvPenagihan.getString(Constant.TLPN_REKOMENDATOR));
//                    dd.setTlpn_upliner(jsonIsiDetInvPenagihan.getString(Constant.TLPN_UPLINER));
//                    dd.setSumber_bayar(jsonIsiDetInvPenagihan.getString(Constant.SUMBER_BAYAR));
//                    dd.setBunga(jsonIsiDetInvPenagihan.getString(Constant.BUNGA));
//                    dd.setTlpn_econ(jsonIsiDetInvPenagihan.getString(Constant.TLPN_ECON));
//                    dd.setJns_pinjaman(jsonIsiDetInvPenagihan.getString(Constant.JNS_PINJAMAN));
//                    dd.setNominal_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.NOMINAL_BAYAR_TERAKHIR));
//                    dd.setHarus_bayar(jsonIsiDetInvPenagihan.getString(Constant.HARUS_BAYAR));
//                    dd.setNominal_ptp(jsonIsiDetInvPenagihan.getString(Constant.NOMINAL_PTP));
//                    dd.setSaldo(jsonIsiDetInvPenagihan.getString(Constant.SALDO));
//                    dd.setNama_rekomendator(jsonIsiDetInvPenagihan.getString(Constant.NAMA_REKOMENDATOR));
//                    dd.setTlpn_mogen(jsonIsiDetInvPenagihan.getString(Constant.TLPN_MOGEN));
//                    dd.setOs_pinjaman(jsonIsiDetInvPenagihan.getString(Constant.OS_PINJAMAN));
//                    dd.setNama_econ(jsonIsiDetInvPenagihan.getString(Constant.NAMA_ECON));
//                    dd.setKeberadaan_jaminan(jsonIsiDetInvPenagihan.getString(Constant.KEBERADAAN_JAMINAN));
//                    dd.setEmail(jsonIsiDetInvPenagihan.getString(Constant.SUMBER_BAYAR));
//                    dd.setKewajiban(jsonIsiDetInvPenagihan.getString(Constant.KEWAJIBAN));
//                    dd.setTotal_kewajiban(jsonIsiDetInvPenagihan.getString(Constant.TOTAL_KEWAJIBAN));
//                    dd.setNama_mogen(jsonIsiDetInvPenagihan.getString(Constant.NAMA_MOGEN));
//                    dd.setNo_loan(jsonIsiDetInvPenagihan.getString(Constant.NO_LOAN));
//                    //   dd.setNo_loan(noloangenerated);
//                    dd.setNama_debitur(jsonIsiDetInvPenagihan.getString(Constant.NAMA_DEBITUR));
//                    dd.setTgl_jth_tempo(jsonIsiDetInvPenagihan.getString(Constant.TGL_JTH_TEMPO));
//                    dd.setNo_tlpn(jsonIsiDetInvPenagihan.getString(Constant.NO_TLPN));
//                    dd.setTgl_gajian(jsonIsiDetInvPenagihan.getString(Constant.TGL_GAJIAN));
//                    dd.setPekerjaan(jsonIsiDetInvPenagihan.getString(Constant.PEKERJAAN));
//                    dd.setAlmt_usaha(jsonIsiDetInvPenagihan.getString(Constant.ALMT_USAHA));
//                    dd.setId_tgt(id_tgt_generate);
//
//                    d.insertDetailInventoryIdTGT(dd);
////                    d.deleteAllDetailInventory();
//                }
//            }
//
//            JSONArray jsonArraynpay = jsonPenagihan.getJSONArray("npay");
//            JSONArray jsonArraynpay1 = new JSONArray(jsonArraynpay.toString());
//            System.out.println("Jsonarraynpay: " + jsonArraynpay1);
//            for (int i = 0; i < jsonArraynpay1.length(); i++) {
//                String noloan = jsonArraynpay1.getString(i);
//                JSONObject jsonNoLoanNpay = new JSONObject(noloan);
//
//                Detil_TGT dtgt = new Detil_TGT();
//                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("npay"));
//                dtgt.setNo_loan(d.generateNoLoanSqlite());
//                dtgt.setTgl_kunjungan("");
//                dtgt.setStatus("0");
//                Detil_TGT querydtgt = new Detil_TGT();
//                querydtgt.insertDetilTGT(dtgt);
//                // querydtgt.deleteAllDetilTGT();
//                System.out.println("No_Loan npay: " + jsonNoLoanNpay.getString("no_loan"));
//            }
//
//            JSONArray jsonArrayfpay = jsonPenagihan.getJSONArray("fpay");
//            JSONArray jsonArrayfpay1 = new JSONArray(jsonArrayfpay.toString());
//            System.out.println("Jsonarrayfpay: " + jsonArrayfpay1);
//            for (int i = 0; i < jsonArrayfpay1.length(); i++) {
//                String noloan = jsonArrayfpay1.getString(i);
//                JSONObject jsonNoLoanFpay = new JSONObject(noloan);
//
//                Detil_TGT dtgt = new Detil_TGT();
//                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("fpay"));
//                dtgt.setNo_loan(d.generateNoLoanSqlite());
//                dtgt.setTgl_kunjungan(jsonNoLoanFpay.getString(Constant.TGL_KUNJUNGAN));
//                dtgt.setStatus("0");
//                Detil_TGT querydtgt = new Detil_TGT();
//                querydtgt.insertDetilTGT(dtgt);
//                // querydtgt.deleteAllDetilTGT();
//
//                System.out.println("No_Loan fpay: " + jsonNoLoanFpay.getString("no_loan"));
//            }
//
//            JSONArray jsonArraynvst = jsonPenagihan.getJSONArray("nvst");
//            JSONArray jsonArraynvst1 = new JSONArray(jsonArraynvst.toString());
//            System.out.println("Jsonarraynvst: " + jsonArraynvst1);
//            for (int i = 0; i < jsonArraynvst1.length(); i++) {
//                String noloan = jsonArraynvst1.getString(i);
//                JSONObject jsonNoLoannvst = new JSONObject(noloan);
//
//                Detil_TGT dtgt = new Detil_TGT();
//                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("nvst"));
//                dtgt.setNo_loan(d.generateNoLoanSqlite());
//                dtgt.setTgl_kunjungan("");
//                dtgt.setStatus("0");
//                Detil_TGT querydtgt = new Detil_TGT();
//                querydtgt.insertDetilTGT(dtgt);
//                // querydtgt.deleteAllDetilTGT();
//
//
//                System.out.println("No_Loan nvst: " + jsonNoLoannvst.getString("no_loan"));
//            }
//
//            JSONArray jsonArrayppay = jsonPenagihan.getJSONArray("ppay");
//            JSONArray jsonArrayppay1 = new JSONArray(jsonArrayppay.toString());
//            System.out.println("Jsonarrayppay: " + jsonArrayppay1);
//            for (int i = 0; i < jsonArrayppay1.length(); i++) {
//                String noloan = jsonArrayppay1.getString(i);
//                JSONObject jsonNoLoanPpay = new JSONObject(noloan);
//
//                Detil_TGT dtgt = new Detil_TGT();
//                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("ppay"));
//                dtgt.setNo_loan(d.generateNoLoanSqlite());
//                dtgt.setTgl_kunjungan(jsonNoLoanPpay.getString(Constant.TGL_KUNJUNGAN));
//                dtgt.setStatus("0");
//                Detil_TGT querydtgt = new Detil_TGT();
//                querydtgt.insertDetilTGT(dtgt);
//                // querydtgt.deleteAllDetilTGT();
//
//                System.out.println("No_Loan ppay: " + jsonNoLoanPpay.getString("no_loan"));
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    @Override
//    public void setValues(ArrayList<SettlementKe> al) {
//        listnilaiNominal = al;
//    }
}
