package com.newkss.acs.newkss.MenuSettlement;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.newkss.acs.newkss.Adapter.MenuSettlementAdapter;
import com.newkss.acs.newkss.Adapter.MenuSettlementAdapterKosong;
import com.newkss.acs.newkss.Adapter.MenuSettlementBayarAdapter;
import com.newkss.acs.newkss.Adapter.MenuSettlementSetorAdapter;
import com.newkss.acs.newkss.DataTransferInterface;
import com.newkss.acs.newkss.Menu.MenuInisialisasi;
import com.newkss.acs.newkss.Menu.MenuLogin;
import com.newkss.acs.newkss.Menu.MenuPerformance;
import com.newkss.acs.newkss.Menu.MenuUtama;
import com.newkss.acs.newkss.MenuKunjungan.MenuHasilKunjungan;
import com.newkss.acs.newkss.MenuPhoto.MenuTakePhoto;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.Master_Settle;
import com.newkss.acs.newkss.Model.Penagihan;
import com.newkss.acs.newkss.Model.Pending;
import com.newkss.acs.newkss.Model.Photo;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.Model.Settlement;
import com.newkss.acs.newkss.Model.SettlementKe;
import com.newkss.acs.newkss.ModelBaru.Detil_TGT;
import com.newkss.acs.newkss.ModelBaru.TGT;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Service.ServiceSendLocation;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.DialogNewPassword;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.JSONParser;
import com.newkss.acs.newkss.Util.MySQLiteHelper;
import com.newkss.acs.newkss.Util.NumberTextWatcherForThousand;
import com.newkss.acs.newkss.Util.NumberTextWathcerSettlement;
import com.newkss.acs.newkss.Util.OnCompleteListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
import java.util.StringTokenizer;

/**
 * Created by Erdy on 5/8/2017.
 */
public class MenuSettlementBackup2 extends ParentActivity implements OnCompleteListener {
    ListView listDaftarSettlement;
    ListView listDaftarSetor;
    ArrayList<Settle_Detil> settlementDetilList = new ArrayList<>();
    ArrayList<Master_Settle> masterSettleList = new ArrayList<>();
    MenuSettlementAdapter adapter;
    MenuSettlementSetorAdapter adapterSetor;
    MenuSettlementAdapterKosong adapterKosong;
    Context mContext;
    String current, namaShared, divisiShared, usernameShared, passwordShared, nikShared, settleke, nominalDibayar, totalNominal, sisaSettle;

    ImageView imgBackMenuSettlement;
    Button btnUpload, btnSubmitMenuSettlement, btnSelesaiMenuSettlement;
    EditText etNominalMenuSettlement, etSettlementKe, etNominalDisetor, etNominalBelumdiSetor;

    Settle_Detil querySettleDetil = new Settle_Detil();
    Settlement querySettle = new Settlement();

    TextView txtNamaMenuSettlement, txtDivisiMenuSettlement;
    ProgressDialog progressDialog;
    SharedPreferences shared;

    ServiceSendLocation querySSL;
    String resultfromJson = "";
    JSONParser jsonParser = new JSONParser();
    Master_Settle queryMS = new Master_Settle();
    Master_Settle dataMS;
    Settlement dataSettle;
    Photo queryPhoto = new Photo();

    ArrayList<Photo> listPhoto = new ArrayList<>();
    LinearLayout llMenuSettlementPopup;
    Pending dataPending;
    Pending queryPending = new Pending();
    String jsonStr = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menusettlement);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = this;
        MySQLiteHelper.initDB(mContext);
        querySSL = new ServiceSendLocation(mContext);

        try {

            initUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initUI() {
        etNominalMenuSettlement = (EditText) findViewById(R.id.etNominalMenuSettlement);


        // etSettlementKe = (EditText) findViewById(R.id.etSettlementKe);
//        etNominalBelumdiSetor = (EditText) findViewById(R.id.etNominalBelumdiSetor);
        etNominalDisetor = (EditText) findViewById(R.id.etNominalDisetor);

        listDaftarSettlement = (ListView) findViewById(R.id.listDaftarSettlement);
        listDaftarSetor = (ListView) findViewById(R.id.listDaftarSetor);
        txtDivisiMenuSettlement = (TextView) findViewById(R.id.txtDivisiMenuSettlement);
        txtNamaMenuSettlement = (TextView) findViewById(R.id.txtNamaMenuSettlement);

        System.out.println("querySettle.getAngkaSettlementKe() " + querySettle.getAngkaSettlementKe());
        String setKe = querySettle.getAngkaSettlementKe();
        //etSettlementKe.setText(setKe);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtNamaMenuSettlement.setText(namaShared);
            txtDivisiMenuSettlement.setText(divisiShared);
        }


        llMenuSettlementPopup = (LinearLayout) findViewById(R.id.llMenuSettlementPopup);
        llMenuSettlementPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MenuSettlementBackup2.this, llMenuSettlementPopup);
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_REFRESH)) {
                            initUI();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            Intent i = new Intent(mContext, MenuLogin.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });
        btnSubmitMenuSettlement = (Button) findViewById(R.id.btnSubmitMenuSettlement);


        btnUpload = (Button) findViewById(R.id.btnUpload);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuTakePhoto.class);
                i.putExtra("id", "upload");
                startActivity(i);
            }
        });
        addSettlement();

        if (settlementDetilList.isEmpty()) {
            listDaftarSettlement.setEmptyView(findViewById(R.id.emptyElement));
        } else {
            adapter = new MenuSettlementAdapter(mContext, settlementDetilList);
            listDaftarSettlement.setAdapter(adapter);
            setListViewHeightBasedOnChildren(listDaftarSettlement);
        }
        // System.out.println("SSSSXXX "+querySettleDetil.getTotalNominalSettlement().toString());
        if (querySettleDetil.getTotalNominalSettlement() == null) {
            etNominalMenuSettlement.setText("0");
        } else {
            String totalnominal = querySettleDetil.getTotalNominalSettlement().toString();

            etNominalMenuSettlement.setText(Function.returnSeparatorComa(totalnominal));
        }



        masterSettleList = queryMS.getAllDataMasterSettle();
        for (Master_Settle m : masterSettleList) {
            System.out.println("testing: " + m.getSisa_dibayar());
        }
        //adapterSetor = new MenuSettlementSetorAdapter(mContext, masterSettleList);
        listDaftarSetor.setAdapter(adapterSetor);
        setListViewHeightBasedOnChildren(listDaftarSetor);

        btnSubmitMenuSettlement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Toast(mContext,"tes");
                //  settleke = etSettlementKe.getText().toString();
                //totalNominal = etNominalMenuSettlement.getText().toString();
                //sisaSettle = etNominalBelumdiSetor.getText().toString();
                //nominalDibayar = etNominalDisetor.getText().toString();


                if (settlementDetilList.isEmpty()) {
                    Function.showAlert(mContext, "Belum ada data yang di collect");
                } else if (nominalDibayar.isEmpty()) {
                    Function.showAlert(mContext, "Mohon Isi Nominal");
                } else if (totalNominal.equals("0")) {
                    // new sendSettlement(mContext).execute();
                } else if (!nominalDibayar.isEmpty() && !settlementDetilList.isEmpty()) {
                    String value = "";
                    StringBuffer sb = new StringBuffer();
                    int count = 0;
                    for (Settle_Detil s : settlementDetilList) {
                        //insert ke settle
                        dataSettle = new Settlement();
                        dataSettle.setSettle_ke(settleke);
                        dataSettle.setNo_rek(s.getNo_rek());
                        dataSettle.setNama(s.getNama());
                        dataSettle.setNo_loan(s.getNo_loan());
                        dataSettle.setStatus("1");
                        dataSettle.setSudah_dibayar(s.getNominal_bayar());
                        dataSettle.setTotal_settle(Function.returnDoubleBulat(NumberTextWatcherForThousand.trimCommaOfString(totalNominal)));
                        if (count == 0) {
                            sb.append("Nama: \n" + s.getNama()).append("\nNo Rek:\n" + s.getNo_rek()).append("\nNominal: \n" + Function.returnSeparatorComa(s.getNominal_bayar()));
                        } else {
                            sb.append("\nNama: \n" + s.getNama()).append("\nNo Rek:\n" + s.getNo_rek()).append("\nNominal: \n" + Function.returnSeparatorComa(s.getNominal_bayar()));
                        }
                        count++;
                    }
                    sb.append("\nNominal Disetor: \n" + etNominalDisetor.getText().toString());
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle("Konfirmasi Data");
                    builder.setMessage(sb);
                    builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // new sendSettlement(mContext).execute();
                        }
                    });
                    builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();


                }
            }
        });
        btnSelesaiMenuSettlement = (Button) findViewById(R.id.btnSelesaiMenuSettlement);
        btnSelesaiMenuSettlement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        imgBackMenuSettlement = (ImageView) findViewById(R.id.imgBackMenuSettlement);
        imgBackMenuSettlement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        System.out.println("Set" + settlementDetilList);


        //etNominalMenuSettlement.setText("0");


//        if (querySettle.getSisaBelumSettle().equals("0")) {
//            etNominalBelumdiSetor.setText(etNominalMenuSettlement.getText().toString());
//        } else {
//            etNominalBelumdiSetor.setText(queryMS.getSisa_dibayar());
//        }


        etNominalDisetor.addTextChangedListener(new NumberTextWathcerSettlement(etNominalDisetor));

    }

    @Override
    public void onComplete(String value) {
        System.out.println("Value: " + value);
        passwordShared = Function.convertStringtoMD5(value);


        new sendSettlement(mContext).execute();
    }

    class sendSettlement extends AsyncTask<String, JSONObject, String> {

        Context context;

        public sendSettlement(Context mContext) {
            this.context = mContext;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Mengirim Data Settlement...");
            progressDialog.setTitle("Pesan");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonPhoto = new JSONObject();
            shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
            usernameShared = (shared.getString(Constant.SHARED_USERNAME, ""));
            passwordShared = (shared.getString(Constant.SHARED_PASSWORD, ""));
            nikShared = (shared.getString(Constant.NIK, ""));
            JSONObject jsonAll = new JSONObject();
            JSONArray jsonSetor = new JSONArray();
            JSONArray jsonNasabah = new JSONArray();
            JSONObject jsonObSetor = new JSONObject();
            JSONObject jsonObNasabah = new JSONObject();
            try {
//                listSettlementAmbil = new ArrayList<Settlement>();
//                for (int i = 0; i < adapter.getCount(); i++) {
//                    // dataSettlement = (Settlement) adapter.getItem(i);
//                    listSettlementAmbil.add((Settlement) adapter.getItem(i));
//                }

                listPhoto = queryPhoto.getimageByNoLoan("upload");
                String url = "";
                for (Photo p : listPhoto) {
                    jsonPhoto = new JSONObject();
                    url = p.getUrl_photo();
                    if (url == null) {
                        url = "";
                    }
                }
                for (Settle_Detil s : settlementDetilList) {
                    System.out.println("ISI SETTLE: " + s.getNama());
                    jsonObNasabah = new JSONObject();

                    jsonObNasabah.put("nama", s.getNama());
                    jsonObNasabah.put("nominal_bayar", s.getNominal_bayar());
                    jsonObNasabah.put("no_rekening", s.getNo_rek());
                    System.out.println("Nama " + s.getNama() + " --- Nominal " + s.getNominal_bayar() + " ---- No Rek " + s.getNo_rek());
                    jsonNasabah.put(jsonObNasabah);
                }
                jsonObSetor.put("settle_ke", settleke);
                System.out.println("NILAI TOTAL BAYAR: " + NumberTextWatcherForThousand.trimCommaOfString(nominalDibayar));
                jsonObSetor.put("total_bayar", NumberTextWatcherForThousand.trimCommaOfString(nominalDibayar));
                if (url.isEmpty()) {
                    jsonObSetor.put("upload_setor", "");
                } else {
                    jsonObSetor.put("upload_setor", Function.bitmapToString(Function.bitmapFromPath(url)));
                }

                jsonSetor.put(jsonObSetor);
                System.out.println("ini jsonNasabah: " + jsonNasabah);
                jsonAll.put("setor", jsonSetor);
                jsonAll.put("nasabah", jsonNasabah);
                jsonAll.put("username", usernameShared);
                jsonAll.put("password", passwordShared);

                Double d = Double.parseDouble(Function.removeSeparatorComa(totalNominal));
                int i = d.intValue();

                jsonAll.put("total_nominal_bayar", String.valueOf(i));

//                Integer.parseInt(NumberTextWatcherForThousand.trimCommaOfString(totalNominal))


                // jsonAll.put("sisa_blom_settle", Function.returnDoubleBulat(NumberTextWatcherForThousand.trimCommaOfString(sisaSettle)));
                jsonAll.put("waktu", Function.getWaktuSekarangMilis());

                jsonAll.put("latitude", querySSL.getLatitude());
                jsonAll.put("longitude", querySSL.getLongitude());

                jsonStr = jsonAll.toString();
                System.out.println("Username Password: " + passwordShared + " ---- " + usernameShared);
                System.out.println("Hasil Json: " + jsonStr);
                System.out.println("Header: " + Function.getHeader(mContext, nikShared));
                System.out.println("Header Base 64: " + new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));
                System.out.println("URL: " + Constant.SERVICE_SETTLE);
                resultfromJson = jsonParser.HttpRequestPost(Constant.SERVICE_SETTLE, jsonStr, Constant.TimeOutConnection, new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));
                System.out.println("RETURN JSON: " + resultfromJson);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result.contains(Constant.CONNECTION_LOST)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Pesan");
                builder.setMessage("Gagal Mengirim Karena Gangguan Koneksi. Data akan dikirim kembali saat jaringan tersedia");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {
                            dataPending = new Pending();
                            dataPending.setWaktu(Function.getDateNow());
                            dataPending.setData(jsonStr);
                            dataPending.setUrl(Constant.SERVICE_SETTLE);
                            dataPending.setHeader(new String(Function.encodeBase64(Function.getHeader(mContext, nikShared))));
                            dataPending.setStatus("2");
                            dataPending.setJson_update("upload");
                            dataPending.setKet_update("Settle");
                            queryPending.insertPendingData(dataPending);
                            queryPhoto.updateStatusPhoto("upload");

                            //misal data sukses
                            dataMS = new Master_Settle();
                            dataMS.setTotal(Function.returnDoubleBulat(NumberTextWatcherForThousand.trimCommaOfString(totalNominal)));
                            dataMS.setSudah_dibayar(nominalDibayar);
                            dataMS.setStatus("1");
                            queryMS.insertMasterSettlement(dataMS);

                            for (Settle_Detil s : settlementDetilList) {
                                //insert ke settle
                                dataSettle = new Settlement();
                                dataSettle.setSettle_ke(settleke);
                                dataSettle.setNo_rek(s.getNo_rek());
                                dataSettle.setNama(s.getNama());
                                dataSettle.setNo_loan(s.getNo_loan());
                                dataSettle.setStatus("1");
                                dataSettle.setSudah_dibayar(s.getNominal_bayar());
                                dataSettle.setTotal_settle(Function.returnDoubleBulat(NumberTextWatcherForThousand.trimCommaOfString(totalNominal)));
                                querySettle.insertSettlement(dataSettle);
                            }

                            queryPhoto.updateStatusPhoto("upload");
                            querySettleDetil.updateStatusSettleDetil();


                            Intent i = new Intent(mContext, MenuUtama.class);
                            startActivity(i);
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                });

                AlertDialog alert1 = builder.create();
                alert1.show();
            } else if (result.contains(Constant.CONNECTION_ERROR)) {
                Function.showAlert(mContext, Constant.CONNECTION_ERROR);
            } else {
                try {

                    JSONObject jsonR = new JSONObject(result);
                    String keterangan = jsonR.getString("keterangan");
                    String rc = jsonR.getString("rc");

                    if (rc.equals("00")) {
                        //insert data tsb ke menu master settle
                        dataMS = new Master_Settle();
                        dataMS.setTotal(Function.returnDoubleBulat(NumberTextWatcherForThousand.trimCommaOfString(totalNominal)));
                        dataMS.setSudah_dibayar(nominalDibayar);
                        dataMS.setStatus("1");
                        queryMS.insertMasterSettlement(dataMS);

//                        JSONObject master=new JSONObject();
//                        master.put("total",Function.returnDoubleBulat(NumberTextWatcherForThousand.trimCommaOfString(totalNominal)));
//                        master.put("sudahdibayar",nominalDibayar);
//                        master.put("status","1");
//                        JSONArray ja=new JSONArray();
                        for (Settle_Detil s : settlementDetilList) {
                            //insert ke settle
                            dataSettle = new Settlement();
                            dataSettle.setSettle_ke(settleke);
                            dataSettle.setNo_rek(s.getNo_rek());
                            dataSettle.setNama(s.getNama());
                            dataSettle.setNo_loan(s.getNo_loan());
                            dataSettle.setStatus("1");
                            dataSettle.setSudah_dibayar(s.getNominal_bayar());
                            dataSettle.setTotal_settle(Function.returnDoubleBulat(NumberTextWatcherForThousand.trimCommaOfString(totalNominal)));
                            querySettle.insertSettlement(dataSettle);
                        }

                        queryPhoto.updateStatusPhoto("upload");
                        querySettleDetil.updateStatusSettleDetil();
                        //Function.showAlert(mContext, keterangan);
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage("Settlement Berhasil");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                shared = getApplicationContext().getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
                                SharedPreferences.Editor editor = shared.edit();
                                editor.putString(Constant.SHARED_PASSWORD, passwordShared);
                                editor.commit();

                                Intent i = new Intent(mContext, MenuUtama.class);
                                startActivity(i);
                                finish();

                            }
                        });

                        AlertDialog alert1 = builder.create();
                        alert1.show();


                    } else if (rc.equals("T1")) {
                        DialogNewPassword dialog1 = DialogNewPassword.newInstance();
                        dialog1.show(getFragmentManager(), "MyDialog");
                    } else if (rc.equals("T2")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage(keterangan);
                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
                                shared.edit().clear().commit();
                                Intent i = new Intent(MenuSettlementBackup2.this, MenuInisialisasi.class);
                                startActivity(i);
                                finish();
                            }
                        });

                        AlertDialog alert1 = builder.create();
                        alert1.show();
                    } else if (rc.equals("T3")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage(keterangan);
                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
                                shared.edit().clear().commit();
                                Intent i = new Intent(MenuSettlementBackup2.this, MenuInisialisasi.class);
                                startActivity(i);
                                finish();
                            }
                        });

                        AlertDialog alert1 = builder.create();
                        alert1.show();
                    } else if (rc.equals("T4")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage(keterangan);
                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
                                shared.edit().clear().commit();
                                Intent i = new Intent(MenuSettlementBackup2.this, MenuInisialisasi.class);
                                startActivity(i);
                                finish();
                            }
                        });

                        AlertDialog alert1 = builder.create();
                        alert1.show();
                    } else if (rc.equals("T5")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Pesan");
                        builder.setMessage(keterangan);
                        builder.setIcon(R.drawable.ic_warning_black_24dp);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                shared = mContext.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
                                shared.edit().clear().commit();
                                Intent i = new Intent(MenuSettlementBackup2.this, MenuInisialisasi.class);
                                startActivity(i);
                                finish();
                            }
                        });

                        AlertDialog alert1 = builder.create();
                        alert1.show();
                    } else {
                        Function.showAlert(mContext, keterangan);
                    }
                } catch (Exception e) {
                    Function.showAlert(mContext, e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    private void addSettlement() {

        settlementDetilList = querySettleDetil.getAllDataSettlementDetil();
        //settlementList = querySettlement.getAllDataSettlement();
//        settlementList.add(new Settlement("Risya", "01234569087", "1.000.000"));
//        settlementList.add(new Settlement("Agung", "98765430089", "500.000"));
//        settlementList.add(new Settlement("Risya", "01234569087", "1.000.000"));
//        settlementList.add(new Settlement("Agung", "98765430089", "500.000"));
    }


    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }


//
//
//                                String queryTotal = queryMS.getTotalBayarQuery();
//                                String querySudahDibayar = queryMS.getSudahDibayarQuery();
//                                String querySisaBayar = queryMS.getSisaDibayarQuery();
//
//                                //kalo dibayar full
//                                if (Integer.parseInt(totalNominal) == Integer.parseInt(nominalDibayar)) {
//                                    dataMS = new Master_Settle();
//                                    dataMS.setStatus("1");
//                                    dataMS.setSisa_dibayar("0");
//                                    dataMS.setSudah_dibayar(nominalDibayar);
//                                    dataMS.setTotal(totalNominal);
//                                    queryMS.insertMasterSettlement(dataMS);
//
//                                    String idMS = queryMS.getIDMasterSettle();
//                                    Settlement ss = new Settlement();
//                                    ss.setSettle_ke(settleke);
//                                    ss.setStatus("1");
//                                    ss.setTotal_settle(totalNominal);
//                                    ss.setSisa_settle("0");
//                                    ss.setSudah_dibayar(nominalDibayar);
//                                    ss.setId_master_settle(idMS);
//                                    querySettle.insertSettlement(ss);
//
//
//                                    queryPhoto.updateStatusPhoto("upload");
//                                    querySettleDetil.updateStatusSettleDetil();
//                                    querySettle.updateStatusSettlement();
//                                }
//                                //kalo misal ppay blm lunas
//                                //cek dulu ada apa ga sudah dibayar
//                                //kalo ada update, kalo ga ada insert
//                                else if (queryMS.getSisaDibayarQuery().equals("kosong")) {
//                                    //insert karena kosong
//                                    dataMS = new Master_Settle();
//                                    dataMS.setStatus("0");
//                                    dataMS.setSisa_dibayar("0");
//                                    dataMS.setSudah_dibayar(nominalDibayar);
//                                    dataMS.setTotal(totalNominal);
//                                    queryMS.insertMasterSettlement(dataMS);
//
//                                    String idMS = queryMS.getIDMasterSettle();
//                                    Settlement ss = new Settlement();
//                                    ss.setSettle_ke(settleke);
//                                    ss.setStatus("0");
//                                    ss.setTotal_settle(totalNominal);
//                                    ss.setSisa_settle("0");
//                                    ss.setSudah_dibayar(nominalDibayar);
//                                    ss.setId_master_settle(idMS);
//                                    querySettle.insertSettlement(ss);
//                                } else if (!queryMS.getSisaDibayarQuery().equals("kosong")) {
//                                    //update karena uda prnah bayar jd lunasin dulu
//                                    int totDibayar = Integer.parseInt(queryMS.getSudahDibayarQuery()) + Integer.parseInt(nominalDibayar);
//                                    int selisih = Integer.parseInt(queryMS.getSudahDibayarQuery()) - totDibayar;
//                                    //kalo total sudah dibyar dan dibayar lg tp masih kurang
//                                    if (totDibayar < Integer.parseInt(queryMS.getSudahDibayarQuery())) {
//                                        String idMS = queryMS.getIDMasterSettle();
//                                        Settlement ss = new Settlement();
//                                        ss.setSettle_ke(settleke);
//                                        ss.setStatus("0");
//                                        ss.setTotal_settle(totalNominal);
//                                        ss.setSisa_settle(String.valueOf(selisih));
//                                        ss.setSudah_dibayar(nominalDibayar);
//                                        ss.setId_master_settle(idMS);
//                                        querySettle.insertSettlement(ss);
//
//                                        dataMS = new Master_Settle();
//                                        dataMS.setStatus("0");
//                                        dataMS.setSisa_dibayar(String.valueOf(selisih));
//                                        dataMS.setSudah_dibayar(String.valueOf(totDibayar));
//                                        dataMS.setTotal(totalNominal);
//                                        dataMS.setId_settle(queryMS.getIDMasterSettle());
//                                        queryMS.updateMasterSettlement(dataMS);
//                                    } else if (totDibayar >= Integer.parseInt(queryMS.getSudahDibayarQuery()))
//                                        //kalo jadi lunas
//                                        dataMS = new Master_Settle();
//                                    dataMS.setStatus("1");
//                                    dataMS.setSisa_dibayar("0");
//                                    dataMS.setSudah_dibayar(String.valueOf(totDibayar));
//                                    dataMS.setTotal(totalNominal);
//                                    dataMS.setId_settle(queryMS.getIDMasterSettle());
//                                    queryMS.updateMasterSettlement(dataMS);
//
//                                    querySettleDetil.updateStatusSettleDetil();
//                                    querySettle.updateStatusSettlement();
//                                }
//
//                                //update status 1 kalo nominal uda dibayar semua
////                                if (nominalDibayar.equals(totalNominal)) {
////                                    Settlement ss = new Settlement();
////                                    ss.setSettle_ke(settleke);
////                                    ss.setStatus("0");
////                                    ss.setTotal_settle(totalNominal);
////                                    ss.setSisa_settle(sisaSettle);
////                                    ss.setSudah_dibayar(nominalDibayar);
////                                    querySettle.updateStatusSettlement();
////                                }
////                                //kalo misal ga bayar semua insert data
////                                else if (Integer.parseInt(nominalDibayar) < Integer.parseInt(totalNominal)) {
////                                    Settlement ss = new Settlement();
////                                    ss.setSettle_ke(settleke);
////                                    ss.setStatus("0");
////                                    ss.setTotal_settle(totalNominal);
////                                    ss.setSisa_settle(sisaSettle);
////                                    ss.setSudah_dibayar(nominalDibayar);
////                                    querySettle.insertSettlement(ss);
////                                }
////                                //kasus sekali settle semua total nominal
////                                if(nominalDisetor.equals(total_nominal_bayar)){
////                                    dataSk.setNominal(nominalDisetor);
////                                    dataSk.setStatus("1");
////                                    dataSk.setSettlementKe(etSettlementKe.getText().toString());
////                                    dataSk.setSisaBayar(sisa_belum_settle);
////                                    querySk.insertDataSettlementKe(dataSk);
////                                }
////                                //kasus bayar partial
////                                else{
////                                    dataSk.setNominal(nominalDisetor);
////                                    dataSk.setStatus("0");
////                                    dataSk.setSettlementKe(etSettlementKe.getText().toString());
////                                    dataSk.setSisaBayar(sisa_belum_settle);
////                                    querySk.insertDataSettlementKe(dataSk);
////                                }
//
//                                // querySettlement.updateStatusSettlement();


    @Override
    public void onBackPressed() {

    }
}
