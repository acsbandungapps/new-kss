package com.newkss.acs.newkss.MenuSurvey;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.newkss.acs.newkss.Adapter.MenuListSurveyAdapter;
import com.newkss.acs.newkss.Menu.MenuDataOffline;
import com.newkss.acs.newkss.Menu.MenuListInventoryToday;
import com.newkss.acs.newkss.Menu.MenuLogin;
import com.newkss.acs.newkss.MenuSettlement.MenuSettlement;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.ModelBaru.DataBucketBooked;
import com.newkss.acs.newkss.ModelBaru.Data_Pencairan;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by acs on 6/19/17.
 */

public class MenuListSurvey extends ParentActivity {
    Context mContext;
    ArrayList<Data_Pencairan> listDataPencairan = new ArrayList<>();
    ListView listSurvey;
    EditText etSearchMenuListSurvey;
    ImageView imgSaveMenuListSurvey, imgBackMenuListSurvey;
    ArrayList<Data_Pencairan> bucketChecked;
    MenuListSurveyAdapter adapter;
    Data_Pencairan bucket;

    ArrayList<DataBucketBooked> listDataBooked = new ArrayList<>();
    ArrayList<Detail_Inventory> listDataInventory = new ArrayList<>();

    TextView txtMenuListSurvey, txtDivisiMenuListSurvey;
    SharedPreferences shared;
    String namaShared, divisiShared;

    LinearLayout llMenuListSurveyPopup;

    Settle_Detil querySD = new Settle_Detil();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menulistsurvey);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = this;

        try {
            //MySQLiteHelper.initDB(mContext);
            initUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initUI() {
        txtMenuListSurvey = (TextView) findViewById(R.id.txtMenuListSurvey);
        txtDivisiMenuListSurvey = (TextView) findViewById(R.id.txtDivisiMenuListSurvey);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtMenuListSurvey.setText(namaShared);
            txtDivisiMenuListSurvey.setText(divisiShared);
        }

        listSurvey = (ListView) findViewById(R.id.listSurvey);
        etSearchMenuListSurvey = (EditText) findViewById(R.id.etSearchMenuListSurvey);
        imgSaveMenuListSurvey = (ImageView) findViewById(R.id.imgSaveMenuListSurvey);
        imgBackMenuListSurvey = (ImageView) findViewById(R.id.imgBackMenuListSurvey);
        imgBackMenuListSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        bucketChecked = new ArrayList<>();
        add();
        if (listDataInventory.isEmpty()) {
            //listSurvey.setVisibility(View.GONE);
            listSurvey.setEmptyView(findViewById(R.id.emptyElement));
        } else {
            adapter = new MenuListSurveyAdapter(this, listDataInventory);
            listSurvey.setAdapter(adapter);
        }

        llMenuListSurveyPopup = (LinearLayout) findViewById(R.id.llMenuListSurveyPopup);
        llMenuListSurveyPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MenuListSurvey.this, llMenuListSurveyPopup);
                popup.getMenuInflater().inflate(R.menu.popup_inbox, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_DATAPENDING)) {
                            Intent i = new Intent(getApplicationContext(), MenuDataOffline.class);
                            startActivity(i);
                            finish();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_REFRESH)) {
                            initUI();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            if (querySD.isSettleAvailable().equals("0")) {
                                Intent i = new Intent(getApplicationContext(), MenuLogin.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                                finish();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Pesan");
                                builder.setMessage("Masih Ada Settle Yang Belum Dikirim, Mohon Lakukan Settlement Terlebih Dahulu");
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent i = new Intent(MenuListSurvey.this, MenuSettlement.class);
                                        startActivity(i);
                                        finish();
                                    }
                                });
                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            }
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });
        etSearchMenuListSurvey.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    adapter.notifyDataSetChanged();
                    String text = etSearchMenuListSurvey.getText().toString().toLowerCase(Locale.getDefault());
                    adapter.filter(text);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        listSurvey.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                CheckBox cb = (CheckBox) view.findViewById(R.id.chkBoxListSurvey);
                cb.performClick();
                if (cb.isChecked()) {
                    bucketChecked.add(bucket);
                } else if (!cb.isChecked()) {
                    bucketChecked.remove(bucket);
                }
            }
        });

        imgSaveMenuListSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listDataInventory.isEmpty()) {
                    Function.showAlert(mContext, "Data Kosong");
                } else {
                    String result = "";
                    for (Detail_Inventory b : adapter.getBox()) {
                        if (b.box) {
                            result += b.getId_detail_inventory();
                        }
                    }
                    if (adapter.getBox().isEmpty()) {
                        Toast.makeText(mContext, "Mohon Pilih Data", Toast.LENGTH_SHORT).show();
                    } else {
                        for (Detail_Inventory g : adapter.getBox()) {
                            //Toast.makeText(mContext, g.getId_detail_inventory(), Toast.LENGTH_SHORT).show();
                            //System.out.println("ID Booked: " + g.getId_detail_inventory());
                            //  Toast.makeText(mContext, g.getId_data_pencairan(), Toast.LENGTH_SHORT).show();
                        }

//                    bucket = new TGT();

                        DataBucketBooked dbb = new DataBucketBooked();
                        dbb.deleteSequence();
                        dbb.deleteAllDataBookedPencairan();
                        dbb.insertDataBookedIntoSqlitePencairan(adapter.getBox());


                        Intent i = new Intent(MenuListSurvey.this, MenuListSurveySelected.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable("ARRAYLIST", (Serializable) adapter.getBox());
//                    i.putExtra("BUNDLE", bundle);
                        startActivity(i);
                        finish();
                    }
                }

            }
        });
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        add();
        adapter = new MenuListSurveyAdapter(this, listDataInventory);
        listSurvey.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        add();
//        adapter = new MenuListSurveyAdapter(this, listDataInventory);
//        listSurvey.setAdapter(adapter);
    }

    private void add() {
        try {
            Detail_Inventory di = new Detail_Inventory();
            listDataInventory = di.getAllDataPencairan();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //cari di detail inv yang datanya pencairan
        //dapet noloan terus query ke data pencairan id tsb biar dapet id data pencairannya
    }

    @Override
    public void onBackPressed() {

    }
}
