package com.newkss.acs.newkss.MenuSurvey;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.newkss.acs.newkss.Adapter.MenuListSurveySelectedAdapter;
import com.newkss.acs.newkss.Adapter.MenuSudahSurveyAdapter;
import com.newkss.acs.newkss.Menu.MenuDataOffline;
import com.newkss.acs.newkss.Menu.MenuLogin;
import com.newkss.acs.newkss.MenuSettlement.MenuSettlement;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.ModelBaru.DataBucketBooked;
import com.newkss.acs.newkss.ModelBaru.Detil_Data_Pencairan;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by acs on 7/10/17.
 */

public class MenuSudahSurvey extends ParentActivity {
    ListView lvSurveySelected;
    MenuSudahSurveyAdapter adapter;

    ImageView imgBackMenuListSurveySelected, imgSearchMenuListSurveySelected;
    EditText etSearchMenuListSurveySelected;
    //    ArrayList<DataBucketBooked> listDataBooked = new ArrayList<>();
    ArrayList<Detail_Inventory> listDInv = new ArrayList<>();
    Detail_Inventory queryDi = new Detail_Inventory();
    Context mContext;

    ArrayList<Detil_Data_Pencairan> listDDP = new ArrayList<>();
    Detil_Data_Pencairan queryDDP = new Detil_Data_Pencairan();

    TextView txtMenuSudahSurvey, txtDivisiMenuSudahSurvey;
    SharedPreferences shared;
    String namaShared, divisiShared;
    LinearLayout llMenuListSurveyPopup;
    Settle_Detil querySettleDetil = new Settle_Detil();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menusudahsurvey);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        try {
            initUI();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initUI() {
        mContext = this;

        llMenuListSurveyPopup = (LinearLayout) findViewById(R.id.llMenuListSurveyPopup);
        txtMenuSudahSurvey = (TextView) findViewById(R.id.txtMenuSudahSurvey);
        txtDivisiMenuSudahSurvey = (TextView) findViewById(R.id.txtDivisiMenuSudahSurvey);
        shared = getSharedPreferences(Constant.MYPREF, MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            namaShared = (shared.getString("nama", ""));
            divisiShared = (shared.getString("divisi", ""));
            txtMenuSudahSurvey.setText(namaShared);
            txtDivisiMenuSudahSurvey.setText(divisiShared);
        }

        lvSurveySelected = (ListView) findViewById(R.id.lvSurveySelected);
        imgBackMenuListSurveySelected = (ImageView) findViewById(R.id.imgBackMenuListSurveySelected);
        imgBackMenuListSurveySelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        imgSearchMenuListSurveySelected = (ImageView) findViewById(R.id.imgSearchMenuListSurveySelected);
        etSearchMenuListSurveySelected = (EditText) findViewById(R.id.etSearchMenuListSurveySelected);

        MySQLiteHelper.initDB(this);


        //dapetin no loannya
        //query di detail inventory
        listDDP = queryDDP.getArrayListDetilDataPencairan();
        queryDi = new Detail_Inventory();
        listDInv = queryDi.getDetailInventorySelectedPencairanSudahSurvey(listDDP);
        adapter = new MenuSudahSurveyAdapter(mContext, listDInv);
        lvSurveySelected.setAdapter(adapter);


        etSearchMenuListSurveySelected.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                adapter.notifyDataSetChanged();
                String text = etSearchMenuListSurveySelected.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }
        });

        llMenuListSurveyPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MenuSudahSurvey.this, llMenuListSurveyPopup);
                popup.getMenuInflater().inflate(R.menu.popup_inbox, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getTitle().equals(Constant.POPUP_DATAPENDING)) {
                            Intent i = new Intent(getApplicationContext(), MenuDataOffline.class);
                            startActivity(i);
                            finish();
                        }
                        else if (menuItem.getTitle().equals(Constant.POPUP_REFRESH)) {
                            initUI();
                        } else if (menuItem.getTitle().equals(Constant.POPUP_LOGOUT)) {
                            if (querySettleDetil.isSettleAvailable().equals("0")) {
                                System.out.println("Check:" + querySettleDetil.isSettleAvailable());
                                Intent i = new Intent(getApplicationContext(), MenuLogin.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                                finish();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setTitle("Pesan");
                                builder.setMessage("Mohon Lakukan Settle Terlebih Dahulu");
                                builder.setIcon(R.drawable.ic_warning_black_24dp);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                AlertDialog alert1 = builder.create();
                                alert1.show();
                            }
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });

    }
}
