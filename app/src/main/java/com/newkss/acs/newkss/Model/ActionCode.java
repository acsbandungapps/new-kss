package com.newkss.acs.newkss.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.ModelBaru.Detil_Data_Pencairan;
import com.newkss.acs.newkss.Util.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acs on 9/4/17.
 */

public class ActionCode {
    private String nama_parameter;
    private String isi_parameter;

    public String getNama_parameter() {
        return nama_parameter;
    }

    public void setNama_parameter(String nama_parameter) {
        this.nama_parameter = nama_parameter;
    }

    public String getIsi_parameter() {
        return isi_parameter;
    }

    public void setIsi_parameter(String isi_parameter) {
        this.isi_parameter = isi_parameter;
    }

    public String insertActionCodeintoDB(String nama,String jsonArrayAC) {
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("nama_parameter", nama);
            contentValues.put("isi_parameter", jsonArrayAC);
            Constant.MKSSdb.insertOrThrow(Constant.TABLE_PARAMETER, null, contentValues);
            System.out.println("Insert " + Constant.TABLE_PARAMETER + " Berhasil");
        }
        catch (Exception e){
            e.printStackTrace();
            return e.getMessage();
        }
        return "Berhasil";
    }

    

//    public List<String> getDataParameter(String nama){
//        List<String> list=new ArrayList<>();
//        String query="select isi_parameter from "+Constant.TABLE_PARAMETER+" where nama_parameter="+nama;
//        try {
//
//            Cursor c = Constant.MKSSdb.rawQuery(query, null);
//            if(c.moveToFirst()){
//                String valueParameter=c.getString(c.getColumnIndex("isi_parameter"));
//                JSONArray jsonArrayAC=new JSONArray(valueParameter.toString());
//                for (int i = 0; i < jsonArrayAC.length(); i++) {
//                    String value = jsonArrayAC.getString(i);
//                    JSONObject jsonNoLoanSS = new JSONObject(value);
//                    System.out.println("No_Loan ss: " + jsonNoLoanSS.getString("no_loan"));
//
//
//                    Detil_Data_Pencairan ddp = new Detil_Data_Pencairan();
//                    ddp.setId_data_pencairan(queryPencairan.getIdPencairanfromNama("ss"));
//                    ddp.setNo_loan(jsonNoLoanSS.getString("no_loan"));
//                    ddp.setTgl_kunjungan("");
//                    ddp.setStatus("0");
//                    Detil_Data_Pencairan queryddp = new Detil_Data_Pencairan();
//                    queryddp.insertDetilDataPencairan(ddp);
//                }
//            }
////            while(c.moveToNext()){
////
////            }
////            c.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//
//        }
//        return list;
//    }

}
