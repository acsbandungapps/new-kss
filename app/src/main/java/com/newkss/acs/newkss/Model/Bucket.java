package com.newkss.acs.newkss.Model;

import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;

/**
 * Created by acs on 5/30/17.
 */

public class Bucket {
    public String id_bucket;
    public String nama_bucket;

    public Bucket() {

    }

    public String getId_bucket() {
        return id_bucket;
    }

    public void setId_bucket(String id_bucket) {
        this.id_bucket = id_bucket;
    }

    public String getNama_bucket() {
        return nama_bucket;
    }

    public void setNama_bucket(String nama_bucket) {
        this.nama_bucket = nama_bucket;
    }

    public String getIdBucketfromName(String nama_bucket) {
        String id = "";
        String query = "Select * from " + Constant.TABLE_BUCKET + " where nama_bucket='" + nama_bucket + "'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_bucket"));
                return id;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return id;
    }

    public String getCountnvst(String bucketTitle) {
        String jmlh = "";
        String query = "Select count(*) as jmlh from " + Constant.TABLE_DETAIL_BUCKET + " where id_bucket='" + getNvstCode() + "' and nama_penagihan='" + bucketTitle + "' ";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("jmlh"));
                return jmlh;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }


    public String getCountppay(String bucketTitle) {
        String jmlh = "";
        String query = "Select count(*) as jmlh from " + Constant.TABLE_DETAIL_BUCKET + " where id_bucket='" + getPpayCode() + "' and nama_penagihan='" + bucketTitle + "' ";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("jmlh"));
                return jmlh;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }


    public String getCountfpay(String bucketTitle) {
        String jmlh = "";
        String query = "Select count(*) as jmlh from " + Constant.TABLE_DETAIL_BUCKET + " where id_bucket='" + getFpayCode() + "' and nama_penagihan='" + bucketTitle + "' ";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("jmlh"));
                return jmlh;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }


    public String getCountnpay(String bucketTitle) {
        String jmlh = "";
        String query = "Select count(*) as jmlh from " + Constant.TABLE_DETAIL_BUCKET + " where id_bucket='" + getNpayCode() + "' and nama_penagihan='" + bucketTitle + "' ";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("jmlh"));
                return jmlh;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }


    public String getCountTgt(String bucketTitle) {
        String jmlh = "";
        String query = "Select count(*) as jmlh from " + Constant.TABLE_DETAIL_BUCKET + " where id_bucket='" + getTgtCode() + "' and nama_penagihan='" + bucketTitle + "' ";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("jmlh"));
                return jmlh;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }

    public String getTgtCode() {
        String id = "";
        String query = "select id_bucket from kss_bucket where nama_bucket='tgt'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_bucket"));
                return id;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public String getNpayCode() {
        String id = "";
        String query = "select id_bucket from kss_bucket where nama_bucket='npay'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_bucket"));
                return id;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public String getFpayCode() {
        String id = "";
        String query = "select id_bucket from kss_bucket where nama_bucket='fpay'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_bucket"));
                return id;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public String getNvstCode() {
        String id = "";
        String query = "select id_bucket from kss_bucket where nama_bucket='nvst'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_bucket"));
                return id;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public String getPpayCode() {
        String id = "";
        String query = "select id_bucket from kss_bucket where nama_bucket='ppay'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_bucket"));
                return id;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }


}
