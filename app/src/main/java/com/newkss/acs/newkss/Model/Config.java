package com.newkss.acs.newkss.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;

import java.util.List;

/**
 * Created by acs on 5/31/17.
 */

public class Config {
    public String id_config;
    public String nama_config;
    public String value;

    public String getId_config() {
        return id_config;
    }

    public void setId_config(String id_config) {
        this.id_config = id_config;
    }

    public String getNama_config() {
        return nama_config;
    }

    public void setNama_config(String nama_config) {
        this.nama_config = nama_config;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void insertPeriodikTrack(Config cc) {
        try {
            System.out.println("Query insertPeriodikTrack ");
            ContentValues contentValues = new ContentValues();
            contentValues.put(Constant.NAMA_CONFIG, cc.getNama_config());
            contentValues.put(Constant.VALUE_CONFIG, cc.getValue());
            Constant.MKSSdb.insertOrThrow(Constant.TABLE_CONFIG, null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertDataConfig(List<Config> listConfig) {
        try {
            System.out.println("Query insertDataConfig ");
            deleteAllDataConfig();
            for (Config c : listConfig) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(Constant.NAMA_CONFIG, c.getNama_config());
                contentValues.put(Constant.VALUE_CONFIG, c.getValue());
                Constant.MKSSdb.insertOrThrow(Constant.TABLE_CONFIG, null, contentValues);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Config getDataConfig() {
        Config cc = new Config();
        String query = "Select * from " + Constant.TABLE_CONFIG;
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                cc.nama_config = c.getString(c.getColumnIndex("nama_config"));
                cc.value = c.getString(c.getColumnIndex("value"));
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return cc;
    }

    public void deleteAllDataConfig() {
        try {
            Constant.MKSSdb.delete(Constant.TABLE_CONFIG, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_CONFIG);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }

    public String getDateConfig() {
        String dateServer = "";
        String query = "Select * from " + Constant.TABLE_CONFIG + " where nama_config='waktu'";
        System.out.println("Query getDateConfig:"+query);
        Cursor c = Constant.MKSSdb.rawQuery(query, null);
        try {

            if (c.moveToFirst()) {
                dateServer = c.getString(c.getColumnIndex("value"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            c.close();
        }
        return dateServer;
    }

    public String checkExistPeriodicTrack() {
        String id = "";
        String query = "select count(*) from " + Constant.TABLE_CONFIG + " where nama_config='"+Constant.PERIODIK_TRACK+"'";
        System.out.println("Query checkExistPeriodicTrack " + query);

        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {

                id = c.getString(c.getColumnIndex("count(*)"));
                System.out.println("count(*) " + id);
            }
        } catch (Exception e) {
            id = "0";
            e.printStackTrace();
        }
        return id;
    }

    public void updatePeriodikTrack(String value) {
        String query = "update " + Constant.TABLE_CONFIG + " set value='" + value + "' where nama_config='"+Constant.PERIODIK_TRACK+"'";
        System.out.println("Query updatePeriodikTrack: " + query);
        try {
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil updatePeriodikTrack");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal updatePeriodikTrack");
        }
    }

    public String getValuePeriodikTrack(){
        String value="";
        String query="select value from '"+Constant.TABLE_CONFIG+"' where nama_config='"+Constant.PERIODIK_TRACK+"'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                value = c.getString(c.getColumnIndex("value"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }
}
