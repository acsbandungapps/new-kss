package com.newkss.acs.newkss.Model;

import com.newkss.acs.newkss.Util.Constant;

/**
 * Created by acs on 7/10/17.
 */

public class DeleteData {

    //tgt,settle,databooked,data_pencairan
    public void deleteAllDetailInventory() {
        String query = "DELETE from " + Constant.TABLE_DETAIL_INVENTORY;
        try {
            Constant.MKSSdb.delete(Constant.TABLE_DETAIL_INVENTORY, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_DETAIL_INVENTORY);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Hapus: "+Constant.TABLE_DETAIL_INVENTORY+ " --- " + e.getMessage());
        }
    }

    public void deleteAllPhoto() {
        String query = "DELETE from " + Constant.TABLE_PHOTO;
        try {
            Constant.MKSSdb.delete(Constant.TABLE_PHOTO, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_PHOTO);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Hapus: "+Constant.TABLE_PHOTO+ " --- " + e.getMessage());
        }
    }

    public void deleteAllSettle() {
        String query = "DELETE from " + Constant.TABLE_SETTLEMENT;
        try {
            Constant.MKSSdb.delete(Constant.TABLE_SETTLEMENT, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_SETTLEMENT);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Hapus: "+Constant.TABLE_SETTLEMENT+ " --- " + e.getMessage());
        }
    }

    public void deleteAllDetilSettle() {
        String query = "DELETE from " + Constant.TABLE_DETIL_SETTLEMENT;
        try {
            Constant.MKSSdb.delete(Constant.TABLE_DETIL_SETTLEMENT, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_DETIL_SETTLEMENT);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Hapus: "+Constant.TABLE_DETIL_SETTLEMENT+ " --- " + e.getMessage());
        }
    }

    public void deleteAllMasterSettle() {
        String query = "DELETE from " + Constant.TABLE_MASTER_SETTLEMENT;
        try {
            Constant.MKSSdb.delete(Constant.TABLE_MASTER_SETTLEMENT, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_MASTER_SETTLEMENT);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Hapus: "+Constant.TABLE_MASTER_SETTLEMENT+ " --- " + e.getMessage());
        }
    }



    public void deleteAllTGT() {
        String query = "DELETE from " + Constant.TABLE_TGT;
        try {
            Constant.MKSSdb.delete(Constant.TABLE_TGT, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_TGT);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Hapus: "+Constant.TABLE_TGT+ " --- " + e.getMessage());
        }
    }

    public void deleteAllDataPencairan() {
        String query = "DELETE from " + Constant.TABLE_DATA_PENCAIRAN;
        try {
            Constant.MKSSdb.delete(Constant.TABLE_DATA_PENCAIRAN, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_DATA_PENCAIRAN);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Hapus: "+Constant.TABLE_DATA_PENCAIRAN+ " --- " + e.getMessage());
        }
    }

    public void deleteAllDetilTGT() {
        String query = "DELETE from " + Constant.TABLE_DETIL_TGT;
        try {
            Constant.MKSSdb.delete(Constant.TABLE_DETIL_TGT, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_DETIL_TGT);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Hapus: "+Constant.TABLE_DETIL_TGT+ " --- " + e.getMessage());
        }
    }

    public void deleteAllDetilDataPencairan() {
        String query = "DELETE from " + Constant.TABLE_DETIL_DATA_PENCAIRAN;
        try {
            Constant.MKSSdb.delete(Constant.TABLE_DETIL_DATA_PENCAIRAN, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_DETIL_DATA_PENCAIRAN);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Hapus: "+Constant.TABLE_DETIL_DATA_PENCAIRAN+ " --- " + e.getMessage());
        }
    }

    public void deleteAllDataBooked() {
        String query = "DELETE from " + Constant.TABLE_DATA_BOOKED;
        try {
            Constant.MKSSdb.delete(Constant.TABLE_DATA_BOOKED, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_DATA_BOOKED);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Hapus: "+Constant.TABLE_DATA_BOOKED+ " --- " + e.getMessage());
        }
    }


    public void deleteAllSequence() {
        String query = "DELETE from " + Constant.TABLE_SEQUENCE;
        try {
            Constant.MKSSdb.delete(Constant.TABLE_SEQUENCE, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_SEQUENCE);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Hapus: "+Constant.TABLE_SEQUENCE+ " --- " + e.getMessage());
        }
    }

    public void deleteAllParameter() {
        String query = "DELETE from " + Constant.TABLE_PARAMETER;
        try {
            Constant.MKSSdb.delete(Constant.TABLE_PARAMETER, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_PARAMETER);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Hapus: "+Constant.TABLE_PARAMETER+ " --- " + e.getMessage());
        }
    }



}
