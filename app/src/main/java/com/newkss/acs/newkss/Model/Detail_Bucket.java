package com.newkss.acs.newkss.Model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.newkss.acs.newkss.Util.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acs on 5/24/17.
 */


public class Detail_Bucket implements Parcelable {
    public String id_detail_bucket;
    public String id_bucket;
    public String id_detail_inventory;
    public String nama_penagihan;
    public String cycle;
    public String jarak;
    public String os;
    public String nama;
    public String alamat;
    public String total_bayar;
    public String jumlah_bucket;
    public boolean box;

    protected Detail_Bucket(Parcel in) {
        id_detail_bucket = in.readString();
        id_bucket = in.readString();
        id_detail_inventory = in.readString();
        nama_penagihan = in.readString();
        cycle = in.readString();
        jarak = in.readString();
        os = in.readString();
        nama = in.readString();
        alamat = in.readString();
        total_bayar = in.readString();
        jumlah_bucket = in.readString();
        box = in.readByte() != 0;
    }

    public Detail_Bucket() {

    }

    public static final Creator<Detail_Bucket> CREATOR = new Creator<Detail_Bucket>() {
        @Override
        public Detail_Bucket createFromParcel(Parcel in) {
            return new Detail_Bucket(in);
        }

        @Override
        public Detail_Bucket[] newArray(int size) {
            return new Detail_Bucket[size];
        }
    };

    public boolean isBox() {
        return box;
    }

    public void setBox(boolean box) {
        this.box = box;
    }

    public String getId_detail_bucket() {
        return id_detail_bucket;
    }

    public void setId_detail_bucket(String id_detail_bucket) {
        this.id_detail_bucket = id_detail_bucket;
    }

    public String getId_bucket() {
        return id_bucket;
    }

    public void setId_bucket(String id_bucket) {
        this.id_bucket = id_bucket;
    }

    public String getId_detail_inventory() {
        return id_detail_inventory;
    }

    public void setId_detail_inventory(String id_detail_inventory) {
        this.id_detail_inventory = id_detail_inventory;
    }

    public String getCycle() {
        return cycle;
    }

    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    public String getJarak() {
        return jarak;
    }

    public void setJarak(String jarak) {
        this.jarak = jarak;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTotal_bayar() {
        return total_bayar;
    }

    public void setTotal_bayar(String total_bayar) {
        this.total_bayar = total_bayar;
    }

    public String getNama_penagihan() {
        return nama_penagihan;
    }

    public void setNama_penagihan(String nama_penagihan) {
        this.nama_penagihan = nama_penagihan;
    }

    public String getJumlah_bucket() {
        return jumlah_bucket;
    }

    public void setJumlah_bucket(String jumlah_bucket) {
        this.jumlah_bucket = jumlah_bucket;
    }

    public void insertDetailBucket(Detail_Bucket db) {
        try {
//            for (int i = 0; i < Integer.parseInt(db.getJumlah_bucket()); i++) {
            String id = "";
            String hasilakhir = "";
            if (getIdDetailBucket().getId_detail_bucket() == null) {
                id = "DB-0000000";
                System.out.println("Isi kosong");
            } else {
                id = getIdDetailBucket().getId_detail_bucket();
                System.out.println("Ada isi");
                System.out.println(getIdDetailBucket().getId_detail_bucket());
            }
            String serial = id;
            int angka = Integer.parseInt(serial.substring(3, 10));
            System.out.println("Angka: " + angka);
            angka = angka + 1;
            System.out.println("Angka +1 " + angka);
            hasilakhir = "DB-" + String.format("%07d", angka);
            System.out.println("Hasil Akhir " + hasilakhir);

            ContentValues contentValues = new ContentValues();
            contentValues.put(Constant.ID_DETAIL_BUCKET, hasilakhir);
            contentValues.put(Constant.ID_BUCKET, db.getId_bucket());
            contentValues.put(Constant.DB_ID_DETAIL_INVENTORY, db.getId_detail_inventory());
            contentValues.put(Constant.DB_NAMA_PENAGIHAN, db.getNama_penagihan());
            contentValues.put(Constant.CYCLE, db.getCycle());
            contentValues.put(Constant.JARAK, db.getJarak());
            contentValues.put(Constant.DB_OS, db.getOs());
            contentValues.put(Constant.NAMA, db.getNama());
            contentValues.put(Constant.ALAMAT, db.getAlamat());
            contentValues.put(Constant.TOTAL_BAYAR, db.getTotal_bayar());
            contentValues.put(Constant.JUMLAH_BUCKET, db.getJumlah_bucket());

            Constant.MKSSdb.insertOrThrow(Constant.TABLE_DETAIL_BUCKET, null, contentValues);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Detail_Bucket getIdDetailBucket() {
        Detail_Bucket db = new Detail_Bucket();
        String query = "Select * from " + Constant.TABLE_DETAIL_BUCKET + " order by id_detail_bucket desc";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                db.id_detail_bucket = c.getString(c.getColumnIndex("id_detail_bucket"));
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return db;
    }

    public void deleteAllDetailBucket() {
        try {
            Constant.MKSSdb.delete(Constant.TABLE_DETAIL_BUCKET, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_DETAIL_BUCKET);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }

    public ArrayList<Detail_Bucket> getListDetailBucket(String title, String nama) {
        Detail_Bucket db;
        ArrayList<Detail_Bucket> listdb = new ArrayList<>();
        String query = "select * from kss_detail_bucket where id_bucket='" + getIdfromNamaBucket(nama) + "' and nama_penagihan='" + title + "'";
        System.out.println("Query: " + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                db = new Detail_Bucket();
                db.id_detail_bucket = c.getString(c.getColumnIndex("id_detail_bucket"));
                db.id_bucket = c.getString(c.getColumnIndex("id_bucket"));
                db.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
                db.nama_penagihan = c.getString(c.getColumnIndex("nama_penagihan"));
                db.cycle = c.getString(c.getColumnIndex("cycle"));
                db.jarak = c.getString(c.getColumnIndex("jarak"));
                db.os = c.getString(c.getColumnIndex("os"));
                db.nama = c.getString(c.getColumnIndex("nama"));
                db.alamat = c.getString(c.getColumnIndex("alamat"));
                db.total_bayar = c.getString(c.getColumnIndex("total_bayar"));
                listdb.add(db);
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listdb;
    }

    public String getIdfromNamaBucket(String nama) {
        String id = "";
        String query = "select id_bucket from kss_bucket where nama_bucket='" + nama + "'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_bucket"));
                return id;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public void insertDetailBucketBookedIntoSqlite(ArrayList<Detail_Bucket> dd) {
        for (Detail_Bucket debu : dd) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(Constant.ID_DETAIL_BUCKET, debu.getId_detail_bucket());
            Constant.MKSSdb.insertOrThrow(Constant.TABLE_DETAIL_BUCKET_BOOKED, null, contentValues);
        }
    }

    public ArrayList<Detail_Bucket_Booked> getIdArrayBooked() {
        ArrayList<Detail_Bucket_Booked> listId = new ArrayList<>();
        Detail_Bucket_Booked dbb;
        String query = "select * from " + Constant.TABLE_DETAIL_BUCKET_BOOKED;
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                dbb = new Detail_Bucket_Booked();
                dbb.id_detail_bucket = c.getString(c.getColumnIndex("id_detail_bucket"));
                System.out.println(dbb.id_detail_bucket);
                listId.add(dbb);
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listId;
    }

    public ArrayList<Detail_Bucket> getArrayListDetailFromId(ArrayList<Detail_Bucket_Booked> listdbb) {
        Detail_Bucket db;
        //Detail_Bucket_Booked dbb;
        ArrayList<Detail_Bucket> listdb = new ArrayList<>();
        for (Detail_Bucket_Booked dbb : listdbb) {
            String query = "select * from " + Constant.TABLE_DETAIL_BUCKET + " where id_detail_bucket='" + dbb.id_detail_bucket + "'";
            System.out.println("Query " +query);
            System.out.println("getArrayListDetailFromId: "+dbb.getId_detail_bucket());
            try {
                Cursor c = Constant.MKSSdb.rawQuery(query, null);
                while (c.moveToNext()) {
                    db = new Detail_Bucket();
                    db.id_detail_bucket = c.getString(c.getColumnIndex("id_detail_bucket"));
                    db.id_bucket = c.getString(c.getColumnIndex("id_bucket"));
                    db.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
                    db.nama_penagihan = c.getString(c.getColumnIndex("nama_penagihan"));
                    db.cycle = c.getString(c.getColumnIndex("cycle"));
                    db.jarak = c.getString(c.getColumnIndex("jarak"));
                    db.os = c.getString(c.getColumnIndex("os"));
                    db.nama = c.getString(c.getColumnIndex("nama"));
                    db.alamat = c.getString(c.getColumnIndex("alamat"));
                    db.total_bayar = c.getString(c.getColumnIndex("total_bayar"));
                    listdb.add(db);
                }
                c.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return listdb;
    }

    public void deleteAllDetailBucketBooked() {
        try {
            Constant.MKSSdb.delete(Constant.TABLE_DETAIL_BUCKET_BOOKED, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_DETAIL_BUCKET_BOOKED);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }

    public void deleteSequence() {
        try {
            Constant.MKSSdb.delete(Constant.TABLE_SEQUENCE, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_SEQUENCE);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id_detail_bucket);
        parcel.writeString(id_bucket);
        parcel.writeString(id_detail_inventory);
        parcel.writeString(nama_penagihan);
        parcel.writeString(cycle);
        parcel.writeString(jarak);
        parcel.writeString(os);
        parcel.writeString(nama);
        parcel.writeString(alamat);
        parcel.writeString(total_bayar);
        parcel.writeString(jumlah_bucket);
        parcel.writeByte((byte) (box ? 1 : 0));
    }
}
