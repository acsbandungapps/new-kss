package com.newkss.acs.newkss.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acs on 6/12/17.
 */

public class Detail_Bucket_Booked {


    public String id_detail_bucket_booked;
    public String id_detail_bucket;

    public String getId_detail_bucket_booked() {
        return id_detail_bucket_booked;
    }

    public void setId_detail_bucket_booked(String id_detail_bucket_booked) {
        this.id_detail_bucket_booked = id_detail_bucket_booked;
    }

    public String getId_detail_bucket() {
        return id_detail_bucket;
    }

    public void setId_detail_bucket(String id_detail_bucket) {
        this.id_detail_bucket = id_detail_bucket;
    }


//    public void insertDetailBucketBookedIntoSqlite(ArrayList<Detail_Bucket> dd) {
//        for (Detail_Bucket debu : dd) {
//            ContentValues contentValues = new ContentValues();
//            contentValues.put(Constant.ID_DETAIL_BUCKET, debu.getId_detail_bucket());
//            Constant.MKSSdb.insertOrThrow(Constant.TABLE_DETAIL_BUCKET_BOOKED, null, contentValues);
//        }
//    }

    public void deleteAllDetailBucketBooked() {
        try {
            Constant.MKSSdb.delete(Constant.TABLE_DETAIL_BUCKET_BOOKED, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_DETAIL_BUCKET_BOOKED);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }

}
