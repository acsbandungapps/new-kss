package com.newkss.acs.newkss.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.ModelBaru.Data_Pencairan;
import com.newkss.acs.newkss.ModelBaru.Detail_Inventory_Pencairan;
import com.newkss.acs.newkss.ModelBaru.Detil_Data_Pencairan;
import com.newkss.acs.newkss.ModelBaru.Detil_TGT;
import com.newkss.acs.newkss.ModelBaru.TGT;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import java.util.ArrayList;

/**
 * Created by acs on 5/24/17.
 */

public class Detail_Inventory {

    String id_detail_inventory;
    String saldo_min;
    String dpd_today;
    String resume_nsbh;
    String almt_rumah;
    String tgl_janji_bayar_terakhir;
    String denda;
    String pola_bayar;
    String no_hp;
    String tgl_bayar_terakhir;
    String angsuran_ke;
    String nama_upliner;
    String pokok;
    String tgl_sp;
    String tenor;
    String angsuran;
    String gender;
    String tgl_ptp;
    String action_plan;
    String no_rekening;
    String histori_sp;
    String nominal_janji_bayar_terakhir;
    String tlpn_rekomendator;
    String tlpn_upliner;
    String sumber_bayar;
    String bunga;
    String tlpn_econ;
    String jns_pinjaman;
    String nominal_bayar_terakhir;
    String harus_bayar;
    String nominal_ptp;
    String saldo;
    String nama_rekomendator;
    String tlpn_mogen;
    String os_pinjaman;
    String nama_econ;
    String keberadaan_jaminan;
    String email;
    String kewajiban;
    String total_kewajiban;
    String nama_mogen;
    String no_loan;
    String nama_debitur;
    String tgl_jth_tempo;
    String no_tlpn;
    String tgl_gajian;
    String pekerjaan;
    String almt_usaha;
    String id_tgt;
    String id_data_pencairan;

    String status;
    String jarak_tempuh;
    String durasi;

    String bucket;

    String tgl_cair;

    public boolean synchronizeTableDetailInventory(String tableName, String columnName, String columnType) {
        String sql = "SELECT " + columnName + " FROM " + tableName;

        String sqlTable = "CREATE TABLE IF NOT EXISTS " + tableName + "(" + columnName + " " + columnType + ");";
        MySQLiteHelper.sqLiteDB.execSQL(sqlTable);
        try {
            MySQLiteHelper.sqLiteDB.rawQuery(sql, null);
            return true;

        } catch (Exception e) {
            System.out.println("ALTER TABLE, ADD NEW COLUMN: " + columnName);
            sql = "ALTER TABLE '" + Constant.TABLE_DETAIL_INVENTORY + "' ADD COLUMN " + columnName + " " + columnType;
            try {
                MySQLiteHelper.sqLiteDB.execSQL(sql);
                return true;
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        }
    }

    public String getTgl_cair() {
        return tgl_cair;
    }

    public void setTgl_cair(String tgl_cair) {
        this.tgl_cair = tgl_cair;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getJarak_tempuh() {
        return jarak_tempuh;
    }

    public void setJarak_tempuh(String jarak_tempuh) {
        this.jarak_tempuh = jarak_tempuh;
    }

    public String getDurasi() {
        return durasi;
    }

    public void setDurasi(String durasi) {
        this.durasi = durasi;
    }

    public boolean box;

    public boolean isBox() {
        return box;
    }

    public void setBox(boolean box) {
        this.box = box;
    }

    public String getId_data_pencairan() {
        return id_data_pencairan;
    }

    public void setId_data_pencairan(String id_data_pencairan) {
        this.id_data_pencairan = id_data_pencairan;
    }

    public String getId_tgt() {
        return id_tgt;
    }

    public void setId_tgt(String id_tgt) {
        this.id_tgt = id_tgt;
    }

    public String getSaldo_min() {
        return saldo_min;
    }

    public void setSaldo_min(String saldo_min) {
        this.saldo_min = saldo_min;
    }

    public String getDpd_today() {
        return dpd_today;
    }

    public void setDpd_today(String dpd_today) {
        this.dpd_today = dpd_today;
    }

    public String getResume_nsbh() {
        return resume_nsbh;
    }

    public void setResume_nsbh(String resume_nsbh) {
        this.resume_nsbh = resume_nsbh;
    }

    public String getAlmt_rumah() {
        return almt_rumah;
    }

    public void setAlmt_rumah(String almt_rumah) {
        this.almt_rumah = almt_rumah;
    }

    public String getTgl_janji_bayar_terakhir() {
        return tgl_janji_bayar_terakhir;
    }

    public void setTgl_janji_bayar_terakhir(String tgl_janji_bayar_terakhir) {
        this.tgl_janji_bayar_terakhir = tgl_janji_bayar_terakhir;
    }

    public String getDenda() {
        return denda;
    }

    public void setDenda(String denda) {
        this.denda = denda;
    }

    public String getPola_bayar() {
        return pola_bayar;
    }

    public void setPola_bayar(String pola_bayar) {
        this.pola_bayar = pola_bayar;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getTgl_bayar_terakhir() {
        return tgl_bayar_terakhir;
    }

    public void setTgl_bayar_terakhir(String tgl_bayar_terakhir) {
        this.tgl_bayar_terakhir = tgl_bayar_terakhir;
    }

    public String getAngsuran_ke() {
        return angsuran_ke;
    }

    public void setAngsuran_ke(String angsuran_ke) {
        this.angsuran_ke = angsuran_ke;
    }

    public String getNama_upliner() {
        return nama_upliner;
    }

    public void setNama_upliner(String nama_upliner) {
        this.nama_upliner = nama_upliner;
    }

    public String getPokok() {
        return pokok;
    }

    public void setPokok(String pokok) {
        this.pokok = pokok;
    }

    public String getTgl_sp() {
        return tgl_sp;
    }

    public void setTgl_sp(String tgl_sp) {
        this.tgl_sp = tgl_sp;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getAngsuran() {
        return angsuran;
    }

    public void setAngsuran(String angsuran) {
        this.angsuran = angsuran;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTgl_ptp() {
        return tgl_ptp;
    }

    public void setTgl_ptp(String tgl_ptp) {
        this.tgl_ptp = tgl_ptp;
    }

    public String getAction_plan() {
        return action_plan;
    }

    public void setAction_plan(String action_plan) {
        this.action_plan = action_plan;
    }

    public String getNo_rekening() {
        return no_rekening;
    }

    public void setNo_rekening(String no_rekening) {
        this.no_rekening = no_rekening;
    }

    public String getHistori_sp() {
        return histori_sp;
    }

    public void setHistori_sp(String histori_sp) {
        this.histori_sp = histori_sp;
    }

    public String getNominal_janji_bayar_terakhir() {
        return nominal_janji_bayar_terakhir;
    }

    public void setNominal_janji_bayar_terakhir(String nominal_janji_bayar_terakhir) {
        this.nominal_janji_bayar_terakhir = nominal_janji_bayar_terakhir;
    }

    public String getTlpn_rekomendator() {
        return tlpn_rekomendator;
    }

    public void setTlpn_rekomendator(String tlpn_rekomendator) {
        this.tlpn_rekomendator = tlpn_rekomendator;
    }

    public String getTlpn_upliner() {
        return tlpn_upliner;
    }

    public void setTlpn_upliner(String tlpn_upliner) {
        this.tlpn_upliner = tlpn_upliner;
    }

    public String getSumber_bayar() {
        return sumber_bayar;
    }

    public void setSumber_bayar(String sumber_bayar) {
        this.sumber_bayar = sumber_bayar;
    }

    public String getBunga() {
        return bunga;
    }

    public void setBunga(String bunga) {
        this.bunga = bunga;
    }

    public String getTlpn_econ() {
        return tlpn_econ;
    }

    public void setTlpn_econ(String tlpn_econ) {
        this.tlpn_econ = tlpn_econ;
    }

    public String getJns_pinjaman() {
        return jns_pinjaman;
    }

    public void setJns_pinjaman(String jns_pinjaman) {
        this.jns_pinjaman = jns_pinjaman;
    }

    public String getNominal_bayar_terakhir() {
        return nominal_bayar_terakhir;
    }

    public void setNominal_bayar_terakhir(String nominal_bayar_terakhir) {
        this.nominal_bayar_terakhir = nominal_bayar_terakhir;
    }

    public String getHarus_bayar() {
        return harus_bayar;
    }

    public void setHarus_bayar(String harus_bayar) {
        this.harus_bayar = harus_bayar;
    }

    public String getNominal_ptp() {
        return nominal_ptp;
    }

    public void setNominal_ptp(String nominal_ptp) {
        this.nominal_ptp = nominal_ptp;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getNama_rekomendator() {
        return nama_rekomendator;
    }

    public void setNama_rekomendator(String nama_rekomendator) {
        this.nama_rekomendator = nama_rekomendator;
    }

    public String getTlpn_mogen() {
        return tlpn_mogen;
    }

    public void setTlpn_mogen(String tlpn_mogen) {
        this.tlpn_mogen = tlpn_mogen;
    }

    public String getOs_pinjaman() {
        return os_pinjaman;
    }

    public void setOs_pinjaman(String os_pinjaman) {
        this.os_pinjaman = os_pinjaman;
    }

    public String getNama_econ() {
        return nama_econ;
    }

    public void setNama_econ(String nama_econ) {
        this.nama_econ = nama_econ;
    }

    public String getKeberadaan_jaminan() {
        return keberadaan_jaminan;
    }

    public void setKeberadaan_jaminan(String keberadaan_jaminan) {
        this.keberadaan_jaminan = keberadaan_jaminan;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKewajiban() {
        return kewajiban;
    }

    public void setKewajiban(String kewajiban) {
        this.kewajiban = kewajiban;
    }

    public String getTotal_kewajiban() {
        return total_kewajiban;
    }

    public void setTotal_kewajiban(String total_kewajiban) {
        this.total_kewajiban = total_kewajiban;
    }

    public String getNama_mogen() {
        return nama_mogen;
    }

    public void setNama_mogen(String nama_mogen) {
        this.nama_mogen = nama_mogen;
    }

    public String getNo_loan() {
        return no_loan;
    }

    public void setNo_loan(String no_loan) {
        this.no_loan = no_loan;
    }

    public String getNama_debitur() {
        return nama_debitur;
    }

    public void setNama_debitur(String nama_debitur) {
        this.nama_debitur = nama_debitur;
    }

    public String getTgl_jth_tempo() {
        return tgl_jth_tempo;
    }

    public void setTgl_jth_tempo(String tgl_jth_tempo) {
        this.tgl_jth_tempo = tgl_jth_tempo;
    }

    public String getNo_tlpn() {
        return no_tlpn;
    }

    public void setNo_tlpn(String no_tlpn) {
        this.no_tlpn = no_tlpn;
    }

    public String getTgl_gajian() {
        return tgl_gajian;
    }

    public void setTgl_gajian(String tgl_gajian) {
        this.tgl_gajian = tgl_gajian;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getAlmt_usaha() {
        return almt_usaha;
    }

    public void setAlmt_usaha(String almt_usaha) {
        this.almt_usaha = almt_usaha;
    }


    public String getId_detail_inventory() {
        return id_detail_inventory;
    }

    public void setId_detail_inventory(String id_detail_inventory) {
        this.id_detail_inventory = id_detail_inventory;
    }

    public String insertDetailInventory(ArrayList<Detail_Inventory> arrListdd) {
        return null;
    }

    public void insertDetailInventoryIdTGT(Detail_Inventory dd) {
        try {
            for (int i = 0; i < 1; i++) {
                String id = "";
                String hasilakhir = "";
                if (getIdValueDetailInventory().getId_detail_inventory() == null) {
                    id = "DI-0000000";
                    System.out.println("Isi kosong");
                } else {
                    id = getIdValueDetailInventory().getId_detail_inventory();
                    System.out.println("Ada isi");
                    System.out.println(getIdValueDetailInventory().getId_detail_inventory());
                }
                String serial = id;
                int angka = Integer.parseInt(serial.substring(3, 10));
                System.out.println("Angka: " + angka);
                angka = angka + 1;
                System.out.println("Angka +1 " + angka);
                hasilakhir = "DI-" + String.format("%07d", angka);
                System.out.println("Hasil Akhir " + hasilakhir);

                ContentValues contentValues = new ContentValues();
                contentValues.put(Constant.ID_DETAIL_INVENTORY, hasilakhir);
                contentValues.put(Constant.SALDO_MIN, dd.getSaldo_min());
                contentValues.put(Constant.DPD_TODAY, dd.getDpd_today());
                contentValues.put(Constant.RESUME_NSBH, dd.getResume_nsbh());
                contentValues.put(Constant.ALMT_RUMAH, dd.getAlmt_rumah());
                contentValues.put(Constant.TGL_JANJI_BAYAR_TERAKHIR, dd.getTgl_janji_bayar_terakhir());
                contentValues.put(Constant.DENDA, dd.getDenda());
                contentValues.put(Constant.POLA_BAYAR, dd.getPola_bayar());
                contentValues.put(Constant.NO_HP, dd.getNo_hp());
                contentValues.put(Constant.TGL_BAYAR_TERAKHIR, dd.getTgl_bayar_terakhir());
                contentValues.put(Constant.ANGSURAN_KE, dd.getAngsuran_ke());
                contentValues.put(Constant.NAMA_UPLINER, dd.getNama_upliner());
                contentValues.put(Constant.POKOK, dd.getPokok());
                contentValues.put(Constant.TGL_SP, dd.getTgl_sp());
                contentValues.put(Constant.TENOR, dd.getTenor());
                contentValues.put(Constant.ANGSURAN, dd.getAngsuran());

                contentValues.put(Constant.GENDER, dd.getGender());
                //contentValues.put(Constant.TGL_PTP, dd.getTgl_ptp());
                contentValues.put(Constant.ACTION_PLAN, dd.getAction_plan());
                contentValues.put(Constant.NO_REKENING, dd.getNo_rekening());
                contentValues.put(Constant.HISTORI_SP, dd.getHistori_sp());
                contentValues.put(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR, dd.getNominal_janji_bayar_terakhir());
                contentValues.put(Constant.TLPN_REKOMENDATOR, dd.getTlpn_rekomendator());
                contentValues.put(Constant.TLPN_UPLINER, dd.getTlpn_upliner());
                contentValues.put(Constant.SUMBER_BAYAR, dd.getSumber_bayar());

                contentValues.put(Constant.BUNGA, dd.getBunga());
                contentValues.put(Constant.TLPN_ECON, dd.getTlpn_econ());
                contentValues.put(Constant.JNS_PINJAMAN, dd.getJns_pinjaman());
                contentValues.put(Constant.NOMINAL_BAYAR_TERAKHIR, dd.getNominal_bayar_terakhir());
                contentValues.put(Constant.HARUS_BAYAR, dd.getHarus_bayar());
                //contentValues.put(Constant.NOMINAL_PTP, dd.getNominal_ptp());
                contentValues.put(Constant.SALDO, dd.getSaldo());
                contentValues.put(Constant.NAMA_REKOMENDATOR, dd.getNama_rekomendator());
                contentValues.put(Constant.TLPN_MOGEN, dd.getTlpn_mogen());
                contentValues.put(Constant.OS_PINJAMAN, dd.getOs_pinjaman());
                contentValues.put(Constant.NAMA_ECON, dd.getNama_econ());
                contentValues.put(Constant.KEBERADAAN_JAMINAN, dd.getKeberadaan_jaminan());
                contentValues.put(Constant.EMAIL, dd.getEmail());
                contentValues.put(Constant.KEWAJIBAN, dd.getKewajiban());
                contentValues.put(Constant.TOTAL_KEWAJIBAN, dd.getTotal_kewajiban());
                contentValues.put(Constant.NAMA_MOGEN, dd.getNama_mogen());
                contentValues.put(Constant.NO_LOAN, dd.getNo_loan());
                //contentValues.put(Constant.NO_LOAN, dd.generateNoLoanSqlite());

                contentValues.put(Constant.NAMA_DEBITUR, dd.getNama_debitur());
                contentValues.put(Constant.TGL_JTH_TEMPO, dd.getTgl_jth_tempo());
                contentValues.put(Constant.NO_TLPN, dd.getNo_tlpn());
                contentValues.put(Constant.TGL_GAJIAN, dd.getTgl_gajian());
                contentValues.put(Constant.PEKERJAAN, dd.getPekerjaan());
                contentValues.put(Constant.ALMT_USAHA, dd.getAlmt_usaha());
                contentValues.put(Constant.TABLE_TGT_ID_TGT, dd.getId_tgt());
                contentValues.put("status", dd.getStatus());

                contentValues.put("bucket", dd.getBucket());
                Constant.MKSSdb.insertOrThrow(Constant.TABLE_DETAIL_INVENTORY, null, contentValues);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void insertDetailInventoryPencairan(Detail_Inventory dd) {

        try {
            for (int i = 0; i < 1; i++) {
                String id = "";
                String hasilakhir = "";
                if (getIdValueDetailInventory().getId_detail_inventory() == null) {
                    id = "DI-0000000";
                    System.out.println("Isi kosong");
                } else {
                    id = getIdValueDetailInventory().getId_detail_inventory();
                    System.out.println("Ada isi");
                    System.out.println(getIdValueDetailInventory().getId_detail_inventory());
                }
                String serial = id;
                int angka = Integer.parseInt(serial.substring(3, 10));
                System.out.println("Angka: " + angka);
                angka = angka + 1;
                System.out.println("Angka +1 " + angka);
                hasilakhir = "DI-" + String.format("%07d", angka);
                System.out.println("Hasil Akhir " + hasilakhir);

                ContentValues contentValues = new ContentValues();
                contentValues.put(Constant.ID_DETAIL_INVENTORY, hasilakhir);
                contentValues.put(Constant.SALDO_MIN, dd.getSaldo_min());
                contentValues.put(Constant.DPD_TODAY, dd.getDpd_today());
                contentValues.put(Constant.RESUME_NSBH, dd.getResume_nsbh());
                contentValues.put(Constant.ALMT_RUMAH, dd.getAlmt_rumah());
                contentValues.put(Constant.TGL_JANJI_BAYAR_TERAKHIR, dd.getTgl_janji_bayar_terakhir());
                contentValues.put(Constant.DENDA, dd.getDenda());
                contentValues.put(Constant.POLA_BAYAR, dd.getPola_bayar());
                contentValues.put(Constant.NO_HP, dd.getNo_hp());
                contentValues.put(Constant.TGL_BAYAR_TERAKHIR, dd.getTgl_bayar_terakhir());
                contentValues.put(Constant.ANGSURAN_KE, dd.getAngsuran_ke());
                contentValues.put(Constant.NAMA_UPLINER, dd.getNama_upliner());
                contentValues.put(Constant.POKOK, dd.getPokok());
                contentValues.put(Constant.TGL_SP, dd.getTgl_sp());
                contentValues.put(Constant.TENOR, dd.getTenor());
                contentValues.put(Constant.ANGSURAN, dd.getAngsuran());

                contentValues.put(Constant.GENDER, dd.getGender());
                //contentValues.put(Constant.TGL_PTP, dd.getTgl_ptp());
                contentValues.put(Constant.ACTION_PLAN, dd.getAction_plan());
                contentValues.put(Constant.NO_REKENING, dd.getNo_rekening());
                contentValues.put(Constant.HISTORI_SP, dd.getHistori_sp());
                contentValues.put(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR, dd.getNominal_janji_bayar_terakhir());
                contentValues.put(Constant.TLPN_REKOMENDATOR, dd.getTlpn_rekomendator());
                contentValues.put(Constant.TLPN_UPLINER, dd.getTlpn_upliner());
                contentValues.put(Constant.SUMBER_BAYAR, dd.getSumber_bayar());

                contentValues.put(Constant.BUNGA, dd.getBunga());
                contentValues.put(Constant.TLPN_ECON, dd.getTlpn_econ());
                contentValues.put(Constant.JNS_PINJAMAN, dd.getJns_pinjaman());
                contentValues.put(Constant.NOMINAL_BAYAR_TERAKHIR, dd.getNominal_bayar_terakhir());
                contentValues.put(Constant.HARUS_BAYAR, dd.getHarus_bayar());
                // contentValues.put(Constant.NOMINAL_PTP, dd.getNominal_ptp());
                contentValues.put(Constant.SALDO, dd.getSaldo());
                contentValues.put(Constant.NAMA_REKOMENDATOR, dd.getNama_rekomendator());
                contentValues.put(Constant.TLPN_MOGEN, dd.getTlpn_mogen());
                contentValues.put(Constant.OS_PINJAMAN, dd.getOs_pinjaman());
                contentValues.put(Constant.NAMA_ECON, dd.getNama_econ());
                contentValues.put(Constant.KEBERADAAN_JAMINAN, dd.getKeberadaan_jaminan());
                contentValues.put(Constant.EMAIL, dd.getEmail());
                contentValues.put(Constant.KEWAJIBAN, dd.getKewajiban());
                contentValues.put(Constant.TOTAL_KEWAJIBAN, dd.getTotal_kewajiban());
                contentValues.put(Constant.NAMA_MOGEN, dd.getNama_mogen());
                contentValues.put(Constant.NO_LOAN, dd.getNo_loan());
                //contentValues.put(Constant.NO_LOAN, dd.generateNoLoanSqlite());

                contentValues.put(Constant.NAMA_DEBITUR, dd.getNama_debitur());
                contentValues.put(Constant.TGL_JTH_TEMPO, dd.getTgl_jth_tempo());
                contentValues.put(Constant.NO_TLPN, dd.getNo_tlpn());
                contentValues.put(Constant.TGL_GAJIAN, dd.getTgl_gajian());
                contentValues.put(Constant.PEKERJAAN, dd.getPekerjaan());
                contentValues.put(Constant.ALMT_USAHA, dd.getAlmt_usaha());
                contentValues.put("id_data_pencairan", dd.getId_data_pencairan());
                contentValues.put("status", dd.getStatus());
                contentValues.put("bucket", dd.getBucket());
                contentValues.put("tgl_cair", dd.getTgl_cair());
                Constant.MKSSdb.insertOrThrow(Constant.TABLE_DETAIL_INVENTORY, null, contentValues);
                System.out.println("Sukses Insert: " + Constant.TABLE_DETAIL_INVENTORY);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertDetailInventoryPenagihan(Detail_Inventory dd) {
        try {
            for (int i = 0; i < 1; i++) {
                String id = "";
                String hasilakhir = "";
                if (getIdValueDetailInventory().getId_detail_inventory() == null) {
                    id = "DI-0000000";
                    System.out.println("Isi kosong");
                } else {
                    id = getIdValueDetailInventory().getId_detail_inventory();
                    System.out.println("Ada isi");
                    System.out.println(getIdValueDetailInventory().getId_detail_inventory());
                }
                String serial = id;
                int angka = Integer.parseInt(serial.substring(3, 10));
                System.out.println("Angka: " + angka);
                angka = angka + 1;
                System.out.println("Angka +1 " + angka);
                hasilakhir = "DI-" + String.format("%07d", angka);
                System.out.println("Hasil Akhir " + hasilakhir);

                ContentValues contentValues = new ContentValues();
                contentValues.put(Constant.ID_DETAIL_INVENTORY, hasilakhir);
                contentValues.put(Constant.SALDO_MIN, dd.getSaldo_min());
                contentValues.put(Constant.DPD_TODAY, dd.getDpd_today());
                contentValues.put(Constant.RESUME_NSBH, dd.getResume_nsbh());
                contentValues.put(Constant.ALMT_RUMAH, dd.getAlmt_rumah());
                contentValues.put(Constant.TGL_JANJI_BAYAR_TERAKHIR, dd.getTgl_janji_bayar_terakhir());
                contentValues.put(Constant.DENDA, dd.getDenda());
                contentValues.put(Constant.POLA_BAYAR, dd.getPola_bayar());
                contentValues.put(Constant.NO_HP, dd.getNo_hp());
                contentValues.put(Constant.TGL_BAYAR_TERAKHIR, dd.getTgl_bayar_terakhir());
                contentValues.put(Constant.ANGSURAN_KE, dd.getAngsuran_ke());
                contentValues.put(Constant.NAMA_UPLINER, dd.getNama_upliner());
                contentValues.put(Constant.POKOK, dd.getPokok());
                contentValues.put(Constant.TGL_SP, dd.getTgl_sp());
                contentValues.put(Constant.TENOR, dd.getTenor());
                contentValues.put(Constant.ANGSURAN, dd.getAngsuran());

                contentValues.put(Constant.GENDER, dd.getGender());
                contentValues.put(Constant.TGL_PTP, dd.getTgl_ptp());
                contentValues.put(Constant.ACTION_PLAN, dd.getAction_plan());
                contentValues.put(Constant.NO_REKENING, dd.getNo_rekening());
                contentValues.put(Constant.HISTORI_SP, dd.getHistori_sp());
                contentValues.put(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR, dd.getNominal_janji_bayar_terakhir());
                contentValues.put(Constant.TLPN_REKOMENDATOR, dd.getTlpn_rekomendator());
                contentValues.put(Constant.TLPN_UPLINER, dd.getTlpn_upliner());
                contentValues.put(Constant.SUMBER_BAYAR, dd.getSumber_bayar());

                contentValues.put(Constant.BUNGA, dd.getBunga());
                contentValues.put(Constant.TLPN_ECON, dd.getTlpn_econ());
                contentValues.put(Constant.JNS_PINJAMAN, dd.getJns_pinjaman());
                contentValues.put(Constant.NOMINAL_BAYAR_TERAKHIR, dd.getNominal_bayar_terakhir());
                contentValues.put(Constant.HARUS_BAYAR, dd.getHarus_bayar());
                contentValues.put(Constant.NOMINAL_PTP, dd.getNominal_ptp());
                contentValues.put(Constant.SALDO, dd.getSaldo());
                contentValues.put(Constant.NAMA_REKOMENDATOR, dd.getNama_rekomendator());
                contentValues.put(Constant.TLPN_MOGEN, dd.getTlpn_mogen());
                contentValues.put(Constant.OS_PINJAMAN, dd.getOs_pinjaman());
                contentValues.put(Constant.NAMA_ECON, dd.getNama_econ());
                contentValues.put(Constant.KEBERADAAN_JAMINAN, dd.getKeberadaan_jaminan());
                contentValues.put(Constant.EMAIL, dd.getEmail());
                contentValues.put(Constant.KEWAJIBAN, dd.getKewajiban());
                contentValues.put(Constant.TOTAL_KEWAJIBAN, dd.getTotal_kewajiban());
                contentValues.put(Constant.NAMA_MOGEN, dd.getNama_mogen());
                contentValues.put(Constant.NO_LOAN, dd.getNo_loan());
                contentValues.put(Constant.NAMA_DEBITUR, dd.getNama_debitur());
                contentValues.put(Constant.TGL_JTH_TEMPO, dd.getTgl_jth_tempo());
                contentValues.put(Constant.NO_TLPN, dd.getNo_tlpn());
                contentValues.put(Constant.TGL_GAJIAN, dd.getTgl_gajian());
                contentValues.put(Constant.PEKERJAAN, dd.getPekerjaan());
                contentValues.put(Constant.ALMT_USAHA, dd.getAlmt_usaha());


                Constant.MKSSdb.insertOrThrow(Constant.TABLE_DETAIL_INVENTORY, null, contentValues);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void insertDetailInventory(Detail_Inventory dd, int jumlah) {
        try {

//            for (int i = 0; i < jumlah; i++) {
            String id = "";
            String hasilakhir = "";
            if (getIdValueDetailInventory().getId_detail_inventory() == null) {
                id = "DI-0000000";
                System.out.println("Isi kosong");
            } else {
                id = getIdValueDetailInventory().getId_detail_inventory();
                System.out.println("Ada isi");
                System.out.println(getIdValueDetailInventory().getId_detail_inventory());
            }
            String serial = id;
            int angka = Integer.parseInt(serial.substring(3, 10));
            System.out.println("Angka: " + angka);
            angka = angka + 1;
            System.out.println("Angka +1 " + angka);
            hasilakhir = "DI-" + String.format("%07d", angka);
            System.out.println("Hasil Akhir " + hasilakhir);

            ContentValues contentValues = new ContentValues();
            contentValues.put(Constant.ID_DETAIL_INVENTORY, hasilakhir);
            contentValues.put(Constant.SALDO_MIN, dd.getSaldo_min());
            contentValues.put(Constant.DPD_TODAY, dd.getDpd_today());
            contentValues.put(Constant.RESUME_NSBH, dd.getResume_nsbh());
            contentValues.put(Constant.ALMT_RUMAH, dd.getAlmt_rumah());
            contentValues.put(Constant.TGL_JANJI_BAYAR_TERAKHIR, dd.getTgl_janji_bayar_terakhir());
            contentValues.put(Constant.DENDA, dd.getDenda());
            contentValues.put(Constant.POLA_BAYAR, dd.getPola_bayar());
            contentValues.put(Constant.NO_HP, dd.getNo_hp());
            contentValues.put(Constant.TGL_BAYAR_TERAKHIR, dd.getTgl_bayar_terakhir());
            contentValues.put(Constant.ANGSURAN_KE, dd.getAngsuran_ke());
            contentValues.put(Constant.NAMA_UPLINER, dd.getNama_upliner());
            contentValues.put(Constant.POKOK, dd.getPokok());
            contentValues.put(Constant.TGL_SP, dd.getTgl_sp());
            contentValues.put(Constant.TENOR, dd.getTenor());
            contentValues.put(Constant.ANGSURAN, dd.getAngsuran());

            contentValues.put(Constant.GENDER, dd.getGender());
            contentValues.put(Constant.TGL_PTP, dd.getTgl_ptp());
            contentValues.put(Constant.ACTION_PLAN, dd.getAction_plan());
            contentValues.put(Constant.NO_REKENING, dd.getNo_rekening());
            contentValues.put(Constant.HISTORI_SP, dd.getHistori_sp());
            contentValues.put(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR, dd.getNominal_janji_bayar_terakhir());
            contentValues.put(Constant.TLPN_REKOMENDATOR, dd.getTlpn_rekomendator());
            contentValues.put(Constant.TLPN_UPLINER, dd.getTlpn_upliner());
            contentValues.put(Constant.SUMBER_BAYAR, dd.getSumber_bayar());

            contentValues.put(Constant.BUNGA, dd.getBunga());
            contentValues.put(Constant.TLPN_ECON, dd.getTlpn_econ());
            contentValues.put(Constant.JNS_PINJAMAN, dd.getJns_pinjaman());
            contentValues.put(Constant.NOMINAL_BAYAR_TERAKHIR, dd.getNominal_bayar_terakhir());
            contentValues.put(Constant.HARUS_BAYAR, dd.getHarus_bayar());
            contentValues.put(Constant.NOMINAL_PTP, dd.getNominal_ptp());
            contentValues.put(Constant.SALDO, dd.getSaldo());
            contentValues.put(Constant.NAMA_REKOMENDATOR, dd.getNama_rekomendator());
            contentValues.put(Constant.TLPN_MOGEN, dd.getTlpn_mogen());
            contentValues.put(Constant.OS_PINJAMAN, dd.getOs_pinjaman());
            contentValues.put(Constant.NAMA_ECON, dd.getNama_econ());
            contentValues.put(Constant.KEBERADAAN_JAMINAN, dd.getKeberadaan_jaminan());
            contentValues.put(Constant.EMAIL, dd.getEmail());
            contentValues.put(Constant.KEWAJIBAN, dd.getKewajiban());
            contentValues.put(Constant.TOTAL_KEWAJIBAN, dd.getTotal_kewajiban());
            contentValues.put(Constant.NAMA_MOGEN, dd.getNama_mogen());
            contentValues.put(Constant.NO_LOAN, dd.getNo_loan());
            contentValues.put(Constant.NAMA_DEBITUR, dd.getNama_debitur());
            contentValues.put(Constant.TGL_JTH_TEMPO, dd.getTgl_jth_tempo());
            contentValues.put(Constant.NO_TLPN, dd.getNo_tlpn());
            contentValues.put(Constant.TGL_GAJIAN, dd.getTgl_gajian());
            contentValues.put(Constant.PEKERJAAN, dd.getPekerjaan());
            contentValues.put(Constant.ALMT_USAHA, dd.getAlmt_usaha());


            Constant.MKSSdb.insertOrThrow(Constant.TABLE_DETAIL_INVENTORY, null, contentValues);

            //}

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Detail_Inventory getAllDataDetailInventory(String id) {
        Detail_Inventory dtl = new Detail_Inventory();
        String query = "select * from " + Constant.TABLE_DETAIL_INVENTORY + " where id_detail_inventory='" + id + "'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                dtl.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
                dtl.saldo_min = c.getString(c.getColumnIndex("saldo_min"));
                dtl.dpd_today = c.getString(c.getColumnIndex("dpd_today"));
                dtl.resume_nsbh = c.getString(c.getColumnIndex("resume_nsbh"));
                dtl.almt_rumah = c.getString(c.getColumnIndex("almt_rumah"));
                dtl.tgl_janji_bayar_terakhir = c.getString(c.getColumnIndex("tgl_janji_bayar_terakhir"));
                dtl.denda = c.getString(c.getColumnIndex("denda"));
                dtl.pola_bayar = c.getString(c.getColumnIndex("pola_bayar"));
                dtl.no_hp = c.getString(c.getColumnIndex("no_hp"));
                dtl.tgl_bayar_terakhir = c.getString(c.getColumnIndex("tgl_bayar_terakhir"));
                dtl.angsuran_ke = c.getString(c.getColumnIndex("angsuran_ke"));
                dtl.nama_upliner = c.getString(c.getColumnIndex("nama_upliner"));
                dtl.pokok = c.getString(c.getColumnIndex("pokok"));
                dtl.tgl_sp = c.getString(c.getColumnIndex("tgl_sp"));
                dtl.tenor = c.getString(c.getColumnIndex("tenor"));
                dtl.angsuran = c.getString(c.getColumnIndex("angsuran"));
                dtl.gender = c.getString(c.getColumnIndex("gender"));
                // dtl.tgl_ptp = c.getString(c.getColumnIndex("tgl_ptp"));

                dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));


//                dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
//                dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
//                dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
//                dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
//                dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
//                dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
//                dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
//                dtl.bunga = c.getString(c.getColumnIndex("bunga"));
//                dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
//                dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
//                dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));

                dtl.harus_bayar = c.getString(c.getColumnIndex("harus_bayar"));
                //   dtl.nominal_ptp = c.getString(c.getColumnIndex("nominal_ptp"));
                dtl.saldo = c.getString(c.getColumnIndex("saldo"));
                dtl.nama_rekomendator = c.getString(c.getColumnIndex("nama_rekomendator"));
                dtl.tlpn_mogen = c.getString(c.getColumnIndex("tlpn_mogen"));
                dtl.os_pinjaman = c.getString(c.getColumnIndex("os_pinjaman"));
                dtl.nama_econ = c.getString(c.getColumnIndex("nama_econ"));
                dtl.keberadaan_jaminan = c.getString(c.getColumnIndex("keberadaan_jaminan"));
                dtl.email = c.getString(c.getColumnIndex("email"));
                dtl.kewajiban = c.getString(c.getColumnIndex("kewajiban"));
                dtl.total_kewajiban = c.getString(c.getColumnIndex("total_kewajiban"));

                dtl.nama_mogen = c.getString(c.getColumnIndex("nama_mogen"));
                dtl.no_loan = c.getString(c.getColumnIndex("no_loan"));
                dtl.nama_debitur = c.getString(c.getColumnIndex("nama_debitur"));
                dtl.tgl_jth_tempo = c.getString(c.getColumnIndex("tgl_jth_tempo"));
                dtl.no_tlpn = c.getString(c.getColumnIndex("no_tlpn"));
                dtl.tgl_gajian = c.getString(c.getColumnIndex("tgl_gajian"));
                dtl.pekerjaan = c.getString(c.getColumnIndex("pekerjaan"));
                dtl.almt_usaha = c.getString(c.getColumnIndex("almt_usaha"));

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return dtl;
    }


    public Detail_Inventory getIdValueDetailInventory() {
        Detail_Inventory dtl = new Detail_Inventory();
        String query = "Select * from " + Constant.TABLE_DETAIL_INVENTORY + " order by id_detail_inventory desc";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                dtl.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return dtl;
    }

    public String getIdDataPencairanFromIdDetInv(String id) {
        String idDp = "";
        String query = "Select * from " + Constant.TABLE_DETAIL_INVENTORY + " where id_detail_inventory='" + id + "'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                idDp = c.getString(c.getColumnIndex("id_data_pencairan"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return idDp;
    }


    public String getIdTGTFromIdDetInv(String id) {
        String idDp = "";
        String query = "Select * from " + Constant.TABLE_DETAIL_INVENTORY + " where id_detail_inventory='" + id + "'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                idDp = c.getString(c.getColumnIndex("id_tgt"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return idDp;
    }

    public void deleteAllDetailInventory() {
        String query = "DELETE from " + Constant.TABLE_DETAIL_INVENTORY;
        try {
//            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            Constant.MKSSdb.delete(Constant.TABLE_DETAIL_INVENTORY, null, null);
            //  Constant.MKSSdb.close();
            System.out.println("Sukses Hapus " + Constant.TABLE_DETAIL_INVENTORY);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }

    public Detail_Inventory getDetInventoryFromIdDetailBucket(String idDetBucket) {
        String id_det_inv = getIdDetInventory(idDetBucket);
        System.out.println("Hasil id: " + id_det_inv);
        Detail_Inventory dtl = new Detail_Inventory();
        String query = "select * from " + Constant.TABLE_DETAIL_INVENTORY + " where id_detail_inventory='" + id_det_inv + "'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                dtl.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
                dtl.saldo_min = c.getString(c.getColumnIndex("saldo_min"));
                dtl.dpd_today = c.getString(c.getColumnIndex("dpd_today"));
                dtl.resume_nsbh = c.getString(c.getColumnIndex("resume_nsbh"));
                dtl.almt_rumah = c.getString(c.getColumnIndex("almt_rumah"));
                dtl.tgl_janji_bayar_terakhir = c.getString(c.getColumnIndex("tgl_janji_bayar_terakhir"));
                dtl.denda = c.getString(c.getColumnIndex("denda"));
                dtl.pola_bayar = c.getString(c.getColumnIndex("pola_bayar"));
                dtl.no_hp = c.getString(c.getColumnIndex("no_hp"));
                dtl.tgl_bayar_terakhir = c.getString(c.getColumnIndex("tgl_bayar_terakhir"));
                dtl.angsuran_ke = c.getString(c.getColumnIndex("angsuran_ke"));
                dtl.nama_upliner = c.getString(c.getColumnIndex("nama_upliner"));
                dtl.pokok = c.getString(c.getColumnIndex("pokok"));
                dtl.tgl_sp = c.getString(c.getColumnIndex("tgl_sp"));
                dtl.tenor = c.getString(c.getColumnIndex("tenor"));
                dtl.angsuran = c.getString(c.getColumnIndex("angsuran"));
                dtl.gender = c.getString(c.getColumnIndex("gender"));
                dtl.tgl_ptp = c.getString(c.getColumnIndex("tgl_ptp"));

                dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));


                dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));

                dtl.harus_bayar = c.getString(c.getColumnIndex("harus_bayar"));
                dtl.nominal_ptp = c.getString(c.getColumnIndex("nominal_ptp"));
                dtl.saldo = c.getString(c.getColumnIndex("saldo"));
                dtl.nama_rekomendator = c.getString(c.getColumnIndex("nama_rekomendator"));
                dtl.tlpn_mogen = c.getString(c.getColumnIndex("tlpn_mogen"));
                dtl.os_pinjaman = c.getString(c.getColumnIndex("os_pinjaman"));
                dtl.nama_econ = c.getString(c.getColumnIndex("nama_econ"));
                dtl.keberadaan_jaminan = c.getString(c.getColumnIndex("keberadaan_jaminan"));
                dtl.email = c.getString(c.getColumnIndex("email"));
                dtl.kewajiban = c.getString(c.getColumnIndex("kewajiban"));
                dtl.total_kewajiban = c.getString(c.getColumnIndex("total_kewajiban"));

                dtl.nama_mogen = c.getString(c.getColumnIndex("nama_mogen"));
                dtl.no_loan = c.getString(c.getColumnIndex("no_loan"));

                dtl.nama_debitur = c.getString(c.getColumnIndex("nama_debitur"));
                dtl.tgl_jth_tempo = c.getString(c.getColumnIndex("tgl_jth_tempo"));
                dtl.no_tlpn = c.getString(c.getColumnIndex("no_tlpn"));
                dtl.tgl_gajian = c.getString(c.getColumnIndex("tgl_gajian"));
                dtl.pekerjaan = c.getString(c.getColumnIndex("pekerjaan"));
                dtl.almt_usaha = c.getString(c.getColumnIndex("almt_usaha"));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dtl;
    }

    public String generateNoLoanSqlite() {
        String id = "";
        String query = "Select * from " + Constant.TABLE_DETAIL_INVENTORY + " order by no_loan desc";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                String serial = c.getString(c.getColumnIndex("no_loan"));
                System.out.println("Nilai Serial: " + serial);
                int angka = Integer.parseInt(serial.substring(3, 10));
                System.out.println("Angka: " + angka);
                angka = angka + 1;
                System.out.println("Angka +1 " + angka);
                id = "LD-" + String.format("%07d", angka);
                System.out.println("Hasil Akhir " + id);
            } else {
                id = "LD-0000000";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }


    public String getIdDetInventory(String iddetbucket) {
        String id_det_inv = "";
        String query = "select id_detail_inventory from " + Constant.TABLE_DETAIL_BUCKET + " where id_detail_bucket='" + iddetbucket + "'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id_det_inv = c.getString(c.getColumnIndex("id_detail_inventory"));
                System.out.println("INI: " + id_det_inv);
                return id_det_inv;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id_det_inv;
    }

    public Detail_Inventory getDetInventoryFromIdTGT(String idTGT) {
        Detail_Inventory dtl = new Detail_Inventory();
        String query = "select * from " + Constant.TABLE_DETAIL_INVENTORY + " where id_tgt='" + idTGT + "'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                dtl.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
                dtl.saldo_min = c.getString(c.getColumnIndex("saldo_min"));
                dtl.dpd_today = c.getString(c.getColumnIndex("dpd_today"));
                dtl.resume_nsbh = c.getString(c.getColumnIndex("resume_nsbh"));
                dtl.almt_rumah = c.getString(c.getColumnIndex("almt_rumah"));
                dtl.tgl_janji_bayar_terakhir = c.getString(c.getColumnIndex("tgl_janji_bayar_terakhir"));
                dtl.denda = c.getString(c.getColumnIndex("denda"));
                dtl.pola_bayar = c.getString(c.getColumnIndex("pola_bayar"));
                dtl.no_hp = c.getString(c.getColumnIndex("no_hp"));
                dtl.tgl_bayar_terakhir = c.getString(c.getColumnIndex("tgl_bayar_terakhir"));
                dtl.angsuran_ke = c.getString(c.getColumnIndex("angsuran_ke"));
                dtl.nama_upliner = c.getString(c.getColumnIndex("nama_upliner"));
                dtl.pokok = c.getString(c.getColumnIndex("pokok"));
                dtl.tgl_sp = c.getString(c.getColumnIndex("tgl_sp"));
                dtl.tenor = c.getString(c.getColumnIndex("tenor"));
                dtl.angsuran = c.getString(c.getColumnIndex("angsuran"));
                dtl.gender = c.getString(c.getColumnIndex("gender"));
                dtl.tgl_ptp = c.getString(c.getColumnIndex("tgl_ptp"));

                dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));


                dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));

                dtl.harus_bayar = c.getString(c.getColumnIndex("harus_bayar"));
                dtl.nominal_ptp = c.getString(c.getColumnIndex("nominal_ptp"));
                dtl.saldo = c.getString(c.getColumnIndex("saldo"));
                dtl.nama_rekomendator = c.getString(c.getColumnIndex("nama_rekomendator"));
                dtl.tlpn_mogen = c.getString(c.getColumnIndex("tlpn_mogen"));
                dtl.os_pinjaman = c.getString(c.getColumnIndex("os_pinjaman"));
                dtl.nama_econ = c.getString(c.getColumnIndex("nama_econ"));
                dtl.keberadaan_jaminan = c.getString(c.getColumnIndex("keberadaan_jaminan"));
                dtl.email = c.getString(c.getColumnIndex("email"));
                dtl.kewajiban = c.getString(c.getColumnIndex("kewajiban"));
                dtl.total_kewajiban = c.getString(c.getColumnIndex("total_kewajiban"));

                dtl.nama_mogen = c.getString(c.getColumnIndex("nama_mogen"));
                dtl.no_loan = c.getString(c.getColumnIndex("no_loan"));

                dtl.nama_debitur = c.getString(c.getColumnIndex("nama_debitur"));
                dtl.tgl_jth_tempo = c.getString(c.getColumnIndex("tgl_jth_tempo"));
                dtl.no_tlpn = c.getString(c.getColumnIndex("no_tlpn"));
                dtl.tgl_gajian = c.getString(c.getColumnIndex("tgl_gajian"));
                dtl.pekerjaan = c.getString(c.getColumnIndex("pekerjaan"));
                dtl.almt_usaha = c.getString(c.getColumnIndex("almt_usaha"));
                dtl.id_tgt = c.getString(c.getColumnIndex("id_tgt"));
                dtl.jarak_tempuh = c.getString(c.getColumnIndex("jarak_tempuh"));
                dtl.durasi = c.getString(c.getColumnIndex("durasi"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dtl;
    }


    public Detail_Inventory getDetInventoryFromIdDetInv(String idDetInv) {
        Detail_Inventory dtl = new Detail_Inventory();
        String query = "select * from " + Constant.TABLE_DETAIL_INVENTORY + " where id_detail_inventory='" + idDetInv + "'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                dtl.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
                dtl.saldo_min = c.getString(c.getColumnIndex("saldo_min"));
                dtl.dpd_today = c.getString(c.getColumnIndex("dpd_today"));
                dtl.resume_nsbh = c.getString(c.getColumnIndex("resume_nsbh"));
                dtl.almt_rumah = c.getString(c.getColumnIndex("almt_rumah"));
                dtl.tgl_janji_bayar_terakhir = c.getString(c.getColumnIndex("tgl_janji_bayar_terakhir"));
                dtl.denda = c.getString(c.getColumnIndex("denda"));
                dtl.pola_bayar = c.getString(c.getColumnIndex("pola_bayar"));
                dtl.no_hp = c.getString(c.getColumnIndex("no_hp"));
                dtl.tgl_bayar_terakhir = c.getString(c.getColumnIndex("tgl_bayar_terakhir"));
                dtl.angsuran_ke = c.getString(c.getColumnIndex("angsuran_ke"));
                dtl.nama_upliner = c.getString(c.getColumnIndex("nama_upliner"));
                dtl.pokok = c.getString(c.getColumnIndex("pokok"));
                dtl.tgl_sp = c.getString(c.getColumnIndex("tgl_sp"));
                dtl.tenor = c.getString(c.getColumnIndex("tenor"));
                dtl.angsuran = c.getString(c.getColumnIndex("angsuran"));
                dtl.gender = c.getString(c.getColumnIndex("gender"));
                dtl.tgl_ptp = c.getString(c.getColumnIndex("tgl_ptp"));

                dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));


                dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));

                dtl.harus_bayar = c.getString(c.getColumnIndex("harus_bayar"));
                dtl.nominal_ptp = c.getString(c.getColumnIndex("nominal_ptp"));
                dtl.saldo = c.getString(c.getColumnIndex("saldo"));
                dtl.nama_rekomendator = c.getString(c.getColumnIndex("nama_rekomendator"));
                dtl.tlpn_mogen = c.getString(c.getColumnIndex("tlpn_mogen"));
                dtl.os_pinjaman = c.getString(c.getColumnIndex("os_pinjaman"));
                dtl.nama_econ = c.getString(c.getColumnIndex("nama_econ"));
                dtl.keberadaan_jaminan = c.getString(c.getColumnIndex("keberadaan_jaminan"));
                dtl.email = c.getString(c.getColumnIndex("email"));
                dtl.kewajiban = c.getString(c.getColumnIndex("kewajiban"));
                dtl.total_kewajiban = c.getString(c.getColumnIndex("total_kewajiban"));

                dtl.nama_mogen = c.getString(c.getColumnIndex("nama_mogen"));
                dtl.no_loan = c.getString(c.getColumnIndex("no_loan"));

                dtl.nama_debitur = c.getString(c.getColumnIndex("nama_debitur"));
                dtl.tgl_jth_tempo = c.getString(c.getColumnIndex("tgl_jth_tempo"));
                dtl.no_tlpn = c.getString(c.getColumnIndex("no_tlpn"));
                dtl.tgl_gajian = c.getString(c.getColumnIndex("tgl_gajian"));
                dtl.pekerjaan = c.getString(c.getColumnIndex("pekerjaan"));
                dtl.almt_usaha = c.getString(c.getColumnIndex("almt_usaha"));
                dtl.id_tgt = c.getString(c.getColumnIndex("id_tgt"));
                dtl.jarak_tempuh = c.getString(c.getColumnIndex("jarak_tempuh"));
                dtl.durasi = c.getString(c.getColumnIndex("durasi"));
                dtl.bucket = c.getString(c.getColumnIndex("bucket"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dtl;
    }

    public ArrayList<Detail_Inventory> getAllDataPenagihan() {
        String jaraktempuh = "";
        String durasi = "";
        ArrayList<Detail_Inventory> listdtl = new ArrayList<>();
        Detail_Inventory dtl;
        String query = "select * from kss_detail_inventory where id_tgt!=''";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                dtl = new Detail_Inventory();
                dtl.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
                dtl.saldo_min = c.getString(c.getColumnIndex("saldo_min"));
                dtl.dpd_today = c.getString(c.getColumnIndex("dpd_today"));
                dtl.resume_nsbh = c.getString(c.getColumnIndex("resume_nsbh"));
                dtl.almt_rumah = c.getString(c.getColumnIndex("almt_rumah"));
                dtl.tgl_janji_bayar_terakhir = c.getString(c.getColumnIndex("tgl_janji_bayar_terakhir"));
                dtl.denda = c.getString(c.getColumnIndex("denda"));
                dtl.pola_bayar = c.getString(c.getColumnIndex("pola_bayar"));
                dtl.no_hp = c.getString(c.getColumnIndex("no_hp"));
                dtl.tgl_bayar_terakhir = c.getString(c.getColumnIndex("tgl_bayar_terakhir"));
                dtl.angsuran_ke = c.getString(c.getColumnIndex("angsuran_ke"));
                dtl.nama_upliner = c.getString(c.getColumnIndex("nama_upliner"));
                dtl.pokok = c.getString(c.getColumnIndex("pokok"));
                dtl.tgl_sp = c.getString(c.getColumnIndex("tgl_sp"));
                dtl.tenor = c.getString(c.getColumnIndex("tenor"));
                dtl.angsuran = c.getString(c.getColumnIndex("angsuran"));
                dtl.gender = c.getString(c.getColumnIndex("gender"));
                dtl.tgl_ptp = c.getString(c.getColumnIndex("tgl_ptp"));

                dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));


                dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));

                dtl.harus_bayar = c.getString(c.getColumnIndex("harus_bayar"));
                dtl.nominal_ptp = c.getString(c.getColumnIndex("nominal_ptp"));
                dtl.saldo = c.getString(c.getColumnIndex("saldo"));
                dtl.nama_rekomendator = c.getString(c.getColumnIndex("nama_rekomendator"));
                dtl.tlpn_mogen = c.getString(c.getColumnIndex("tlpn_mogen"));
                dtl.os_pinjaman = c.getString(c.getColumnIndex("os_pinjaman"));
                dtl.nama_econ = c.getString(c.getColumnIndex("nama_econ"));
                dtl.keberadaan_jaminan = c.getString(c.getColumnIndex("keberadaan_jaminan"));
                dtl.email = c.getString(c.getColumnIndex("email"));
                dtl.kewajiban = c.getString(c.getColumnIndex("kewajiban"));
                dtl.total_kewajiban = c.getString(c.getColumnIndex("total_kewajiban"));

                dtl.nama_mogen = c.getString(c.getColumnIndex("nama_mogen"));
                dtl.no_loan = c.getString(c.getColumnIndex("no_loan"));

                dtl.nama_debitur = c.getString(c.getColumnIndex("nama_debitur"));
                dtl.tgl_jth_tempo = c.getString(c.getColumnIndex("tgl_jth_tempo"));
                dtl.no_tlpn = c.getString(c.getColumnIndex("no_tlpn"));
                dtl.tgl_gajian = c.getString(c.getColumnIndex("tgl_gajian"));
                dtl.pekerjaan = c.getString(c.getColumnIndex("pekerjaan"));
                dtl.almt_usaha = c.getString(c.getColumnIndex("almt_usaha"));
                dtl.id_tgt = c.getString(c.getColumnIndex("id_tgt"));
                if (c.getString(c.getColumnIndex("jarak_tempuh")) == "") {
                    jaraktempuh = "";
                } else {
                    jaraktempuh = c.getString(c.getColumnIndex("jarak_tempuh"));
                }
                if (c.getString(c.getColumnIndex("durasi")) == "") {
                    durasi = "";
                } else {
                    durasi = c.getString(c.getColumnIndex("durasi"));
                }
                dtl.jarak_tempuh = jaraktempuh;
                dtl.durasi = durasi;
                dtl.bucket = c.getString(c.getColumnIndex("bucket"));
                listdtl.add(dtl);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listdtl;
    }


    public ArrayList<Detail_Inventory> getAllDataPencairan() {
        ArrayList<Detail_Inventory> listdtl = new ArrayList<>();
        Detail_Inventory dtl;
        String query = "select * from kss_detail_inventory where id_data_pencairan!=''";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                dtl = new Detail_Inventory();
                dtl.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
                dtl.saldo_min = c.getString(c.getColumnIndex("saldo_min"));
                dtl.dpd_today = c.getString(c.getColumnIndex("dpd_today"));
                dtl.resume_nsbh = c.getString(c.getColumnIndex("resume_nsbh"));
                dtl.almt_rumah = c.getString(c.getColumnIndex("almt_rumah"));
                dtl.tgl_janji_bayar_terakhir = c.getString(c.getColumnIndex("tgl_janji_bayar_terakhir"));
                dtl.denda = c.getString(c.getColumnIndex("denda"));
                dtl.pola_bayar = c.getString(c.getColumnIndex("pola_bayar"));
                dtl.no_hp = c.getString(c.getColumnIndex("no_hp"));
                dtl.tgl_bayar_terakhir = c.getString(c.getColumnIndex("tgl_bayar_terakhir"));
                dtl.angsuran_ke = c.getString(c.getColumnIndex("angsuran_ke"));
                dtl.nama_upliner = c.getString(c.getColumnIndex("nama_upliner"));
                dtl.pokok = c.getString(c.getColumnIndex("pokok"));
                dtl.tgl_sp = c.getString(c.getColumnIndex("tgl_sp"));
                dtl.tenor = c.getString(c.getColumnIndex("tenor"));
                dtl.angsuran = c.getString(c.getColumnIndex("angsuran"));
                dtl.gender = c.getString(c.getColumnIndex("gender"));
                dtl.tgl_ptp = c.getString(c.getColumnIndex("tgl_ptp"));

                dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));


                dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));

                dtl.harus_bayar = c.getString(c.getColumnIndex("harus_bayar"));
                dtl.nominal_ptp = c.getString(c.getColumnIndex("nominal_ptp"));
                dtl.saldo = c.getString(c.getColumnIndex("saldo"));
                dtl.nama_rekomendator = c.getString(c.getColumnIndex("nama_rekomendator"));
                dtl.tlpn_mogen = c.getString(c.getColumnIndex("tlpn_mogen"));
                dtl.os_pinjaman = c.getString(c.getColumnIndex("os_pinjaman"));
                dtl.nama_econ = c.getString(c.getColumnIndex("nama_econ"));
                dtl.keberadaan_jaminan = c.getString(c.getColumnIndex("keberadaan_jaminan"));
                dtl.email = c.getString(c.getColumnIndex("email"));
                dtl.kewajiban = c.getString(c.getColumnIndex("kewajiban"));
                dtl.total_kewajiban = c.getString(c.getColumnIndex("total_kewajiban"));

                dtl.nama_mogen = c.getString(c.getColumnIndex("nama_mogen"));
                dtl.no_loan = c.getString(c.getColumnIndex("no_loan"));

                dtl.nama_debitur = c.getString(c.getColumnIndex("nama_debitur"));
                dtl.tgl_jth_tempo = c.getString(c.getColumnIndex("tgl_jth_tempo"));
                dtl.no_tlpn = c.getString(c.getColumnIndex("no_tlpn"));
                dtl.tgl_gajian = c.getString(c.getColumnIndex("tgl_gajian"));
                dtl.pekerjaan = c.getString(c.getColumnIndex("pekerjaan"));
                dtl.almt_usaha = c.getString(c.getColumnIndex("almt_usaha"));
                dtl.id_data_pencairan = c.getString(c.getColumnIndex("id_data_pencairan"));
                dtl.jarak_tempuh = c.getString(c.getColumnIndex("jarak_tempuh"));
                dtl.durasi = c.getString(c.getColumnIndex("durasi"));
                dtl.bucket = c.getString(c.getColumnIndex("bucket"));
                dtl.tgl_cair = c.getString(c.getColumnIndex("tgl_cair"));
                listdtl.add(dtl);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listdtl;
    }

    public ArrayList<Detail_Inventory> getDetailInventorySelectedPencairan() {
        ArrayList<Detail_Inventory> listdi = new ArrayList<>();
        listdi = getIdPencairanOnDataBooked();
        ArrayList<Detail_Inventory> listdtl = new ArrayList<>();
        Detail_Inventory dtl;
        for (Detail_Inventory d : listdi) {
            String query = "select * from " + Constant.TABLE_DETAIL_INVENTORY + " where id_detail_inventory='" + d.getId_detail_inventory() + "'";
            System.out.println(query);
            try {
                Cursor c = Constant.MKSSdb.rawQuery(query, null);
                while (c.moveToNext()) {
                    dtl = new Detail_Inventory();
                    dtl.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
                    dtl.saldo_min = c.getString(c.getColumnIndex("saldo_min"));
                    dtl.dpd_today = c.getString(c.getColumnIndex("dpd_today"));
                    dtl.resume_nsbh = c.getString(c.getColumnIndex("resume_nsbh"));
                    dtl.almt_rumah = c.getString(c.getColumnIndex("almt_rumah"));
                    dtl.tgl_janji_bayar_terakhir = c.getString(c.getColumnIndex("tgl_janji_bayar_terakhir"));
                    dtl.denda = c.getString(c.getColumnIndex("denda"));
                    dtl.pola_bayar = c.getString(c.getColumnIndex("pola_bayar"));
                    dtl.no_hp = c.getString(c.getColumnIndex("no_hp"));
                    dtl.tgl_bayar_terakhir = c.getString(c.getColumnIndex("tgl_bayar_terakhir"));
                    dtl.angsuran_ke = c.getString(c.getColumnIndex("angsuran_ke"));
                    dtl.nama_upliner = c.getString(c.getColumnIndex("nama_upliner"));
                    dtl.pokok = c.getString(c.getColumnIndex("pokok"));
                    dtl.tgl_sp = c.getString(c.getColumnIndex("tgl_sp"));
                    dtl.tenor = c.getString(c.getColumnIndex("tenor"));
                    dtl.angsuran = c.getString(c.getColumnIndex("angsuran"));
                    dtl.gender = c.getString(c.getColumnIndex("gender"));
                    dtl.tgl_ptp = c.getString(c.getColumnIndex("tgl_ptp"));

                    dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                    dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                    dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                    dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                    dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                    dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                    dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                    dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                    dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                    dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                    dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));


                    dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                    dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                    dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                    dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                    dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                    dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                    dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                    dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                    dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                    dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                    dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));

                    dtl.harus_bayar = c.getString(c.getColumnIndex("harus_bayar"));
                    dtl.nominal_ptp = c.getString(c.getColumnIndex("nominal_ptp"));
                    dtl.saldo = c.getString(c.getColumnIndex("saldo"));
                    dtl.nama_rekomendator = c.getString(c.getColumnIndex("nama_rekomendator"));
                    dtl.tlpn_mogen = c.getString(c.getColumnIndex("tlpn_mogen"));
                    dtl.os_pinjaman = c.getString(c.getColumnIndex("os_pinjaman"));
                    dtl.nama_econ = c.getString(c.getColumnIndex("nama_econ"));
                    dtl.keberadaan_jaminan = c.getString(c.getColumnIndex("keberadaan_jaminan"));
                    dtl.email = c.getString(c.getColumnIndex("email"));
                    dtl.kewajiban = c.getString(c.getColumnIndex("kewajiban"));
                    dtl.total_kewajiban = c.getString(c.getColumnIndex("total_kewajiban"));

                    dtl.nama_mogen = c.getString(c.getColumnIndex("nama_mogen"));
                    dtl.no_loan = c.getString(c.getColumnIndex("no_loan"));

                    dtl.nama_debitur = c.getString(c.getColumnIndex("nama_debitur"));
                    dtl.tgl_jth_tempo = c.getString(c.getColumnIndex("tgl_jth_tempo"));
                    dtl.no_tlpn = c.getString(c.getColumnIndex("no_tlpn"));
                    dtl.tgl_gajian = c.getString(c.getColumnIndex("tgl_gajian"));
                    dtl.pekerjaan = c.getString(c.getColumnIndex("pekerjaan"));
                    dtl.almt_usaha = c.getString(c.getColumnIndex("almt_usaha"));
                    dtl.id_data_pencairan = c.getString(c.getColumnIndex("id_data_pencairan"));
                    dtl.jarak_tempuh = c.getString(c.getColumnIndex("jarak_tempuh"));
                    dtl.durasi = c.getString(c.getColumnIndex("durasi"));
                    dtl.bucket = c.getString(c.getColumnIndex("bucket"));
                    dtl.tgl_cair = c.getString(c.getColumnIndex("tgl_cair"));
                    listdtl.add(dtl);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return listdtl;
    }


    public ArrayList<Detail_Inventory> getDetailInventorySelectedPencairanSudahSurvey(ArrayList<Detil_Data_Pencairan> listDDP) {
        ArrayList<Detail_Inventory> listdi = new ArrayList<>();
        listdi = getIdPencairanOnDataBooked();
        ArrayList<Detail_Inventory> listdtl = new ArrayList<>();
        Detail_Inventory dtl;

        for (Detil_Data_Pencairan dd : listDDP) {
            String query = "select * from " + Constant.TABLE_DETAIL_INVENTORY + " where no_loan='" + dd.getNo_loan() + "' and id_data_pencairan!=''";
            System.out.println("Query getDetailInventorySelectedPencairanSudahSurvey: " + query);
            try {
                Cursor c = Constant.MKSSdb.rawQuery(query, null);
                while (c.moveToNext()) {
                    dtl = new Detail_Inventory();
                    dtl.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
                    dtl.saldo_min = c.getString(c.getColumnIndex("saldo_min"));
                    dtl.dpd_today = c.getString(c.getColumnIndex("dpd_today"));
                    dtl.resume_nsbh = c.getString(c.getColumnIndex("resume_nsbh"));
                    dtl.almt_rumah = c.getString(c.getColumnIndex("almt_rumah"));
                    dtl.tgl_janji_bayar_terakhir = c.getString(c.getColumnIndex("tgl_janji_bayar_terakhir"));
                    dtl.denda = c.getString(c.getColumnIndex("denda"));
                    dtl.pola_bayar = c.getString(c.getColumnIndex("pola_bayar"));
                    dtl.no_hp = c.getString(c.getColumnIndex("no_hp"));
                    dtl.tgl_bayar_terakhir = c.getString(c.getColumnIndex("tgl_bayar_terakhir"));
                    dtl.angsuran_ke = c.getString(c.getColumnIndex("angsuran_ke"));
                    dtl.nama_upliner = c.getString(c.getColumnIndex("nama_upliner"));
                    dtl.pokok = c.getString(c.getColumnIndex("pokok"));
                    dtl.tgl_sp = c.getString(c.getColumnIndex("tgl_sp"));
                    dtl.tenor = c.getString(c.getColumnIndex("tenor"));
                    dtl.angsuran = c.getString(c.getColumnIndex("angsuran"));
                    dtl.gender = c.getString(c.getColumnIndex("gender"));
                    dtl.tgl_ptp = c.getString(c.getColumnIndex("tgl_ptp"));

                    dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                    dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                    dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                    dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                    dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                    dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                    dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                    dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                    dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                    dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                    dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));


                    dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                    dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                    dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                    dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                    dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                    dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                    dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                    dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                    dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                    dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                    dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));

                    dtl.harus_bayar = c.getString(c.getColumnIndex("harus_bayar"));
                    dtl.nominal_ptp = c.getString(c.getColumnIndex("nominal_ptp"));
                    dtl.saldo = c.getString(c.getColumnIndex("saldo"));
                    dtl.nama_rekomendator = c.getString(c.getColumnIndex("nama_rekomendator"));
                    dtl.tlpn_mogen = c.getString(c.getColumnIndex("tlpn_mogen"));
                    dtl.os_pinjaman = c.getString(c.getColumnIndex("os_pinjaman"));
                    dtl.nama_econ = c.getString(c.getColumnIndex("nama_econ"));
                    dtl.keberadaan_jaminan = c.getString(c.getColumnIndex("keberadaan_jaminan"));
                    dtl.email = c.getString(c.getColumnIndex("email"));
                    dtl.kewajiban = c.getString(c.getColumnIndex("kewajiban"));
                    dtl.total_kewajiban = c.getString(c.getColumnIndex("total_kewajiban"));

                    dtl.nama_mogen = c.getString(c.getColumnIndex("nama_mogen"));
                    dtl.no_loan = c.getString(c.getColumnIndex("no_loan"));

                    dtl.nama_debitur = c.getString(c.getColumnIndex("nama_debitur"));
                    dtl.tgl_jth_tempo = c.getString(c.getColumnIndex("tgl_jth_tempo"));
                    dtl.no_tlpn = c.getString(c.getColumnIndex("no_tlpn"));
                    dtl.tgl_gajian = c.getString(c.getColumnIndex("tgl_gajian"));
                    dtl.pekerjaan = c.getString(c.getColumnIndex("pekerjaan"));
                    dtl.almt_usaha = c.getString(c.getColumnIndex("almt_usaha"));
                    dtl.id_data_pencairan = c.getString(c.getColumnIndex("id_data_pencairan"));
                    dtl.jarak_tempuh = c.getString(c.getColumnIndex("jarak_tempuh"));
                    dtl.durasi = c.getString(c.getColumnIndex("durasi"));
                    dtl.bucket = c.getString(c.getColumnIndex("bucket"));
                    listdtl.add(dtl);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return listdtl;
    }


    public ArrayList<Detail_Inventory> getIdPencairanOnDataBooked() {
        ArrayList<Detail_Inventory> listdi = new ArrayList<>();
        Detail_Inventory d;
        String query = "select * from " + Constant.TABLE_DATA_BOOKED + " where tipe_booking='Pencairan'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                d = new Detail_Inventory();
                d.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
                listdi.add(d);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listdi;
    }


    public ArrayList<Detail_Inventory> getDetailInventorySelectedPenagihan() {
        ArrayList<Detail_Inventory> listdi = new ArrayList<>();
        listdi = getIdPenagihanOnDataBooked();
        ArrayList<Detail_Inventory> listdtl = new ArrayList<>();
        Detail_Inventory dtl;
        for (Detail_Inventory d : listdi) {
            System.out.println("Isi " + d.getId_detail_inventory());
            String query = "select * from " + Constant.TABLE_DETAIL_INVENTORY + " where id_detail_inventory='" + d.getId_detail_inventory() + "'";
            System.out.println("getDetailInventorySelectedPenagihan " + query);
            try {
                Cursor c = Constant.MKSSdb.rawQuery(query, null);
                while (c.moveToNext()) {
                    dtl = new Detail_Inventory();
                    dtl.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
                    dtl.saldo_min = c.getString(c.getColumnIndex("saldo_min"));
                    dtl.dpd_today = c.getString(c.getColumnIndex("dpd_today"));
                    dtl.resume_nsbh = c.getString(c.getColumnIndex("resume_nsbh"));
                    dtl.almt_rumah = c.getString(c.getColumnIndex("almt_rumah"));
                    dtl.tgl_janji_bayar_terakhir = c.getString(c.getColumnIndex("tgl_janji_bayar_terakhir"));
                    dtl.denda = c.getString(c.getColumnIndex("denda"));
                    dtl.pola_bayar = c.getString(c.getColumnIndex("pola_bayar"));
                    dtl.no_hp = c.getString(c.getColumnIndex("no_hp"));
                    dtl.tgl_bayar_terakhir = c.getString(c.getColumnIndex("tgl_bayar_terakhir"));
                    dtl.angsuran_ke = c.getString(c.getColumnIndex("angsuran_ke"));
                    dtl.nama_upliner = c.getString(c.getColumnIndex("nama_upliner"));
                    dtl.pokok = c.getString(c.getColumnIndex("pokok"));
                    dtl.tgl_sp = c.getString(c.getColumnIndex("tgl_sp"));
                    dtl.tenor = c.getString(c.getColumnIndex("tenor"));
                    dtl.angsuran = c.getString(c.getColumnIndex("angsuran"));
                    dtl.gender = c.getString(c.getColumnIndex("gender"));
                    dtl.tgl_ptp = c.getString(c.getColumnIndex("tgl_ptp"));

                    dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                    dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                    dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                    dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                    dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                    dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                    dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                    dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                    dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                    dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                    dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));


                    dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                    dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                    dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                    dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                    dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                    dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                    dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                    dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                    dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                    dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                    dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));

                    dtl.harus_bayar = c.getString(c.getColumnIndex("harus_bayar"));
                    dtl.nominal_ptp = c.getString(c.getColumnIndex("nominal_ptp"));
                    dtl.saldo = c.getString(c.getColumnIndex("saldo"));
                    dtl.nama_rekomendator = c.getString(c.getColumnIndex("nama_rekomendator"));
                    dtl.tlpn_mogen = c.getString(c.getColumnIndex("tlpn_mogen"));
                    dtl.os_pinjaman = c.getString(c.getColumnIndex("os_pinjaman"));
                    dtl.nama_econ = c.getString(c.getColumnIndex("nama_econ"));
                    dtl.keberadaan_jaminan = c.getString(c.getColumnIndex("keberadaan_jaminan"));
                    dtl.email = c.getString(c.getColumnIndex("email"));
                    dtl.kewajiban = c.getString(c.getColumnIndex("kewajiban"));
                    dtl.total_kewajiban = c.getString(c.getColumnIndex("total_kewajiban"));

                    dtl.nama_mogen = c.getString(c.getColumnIndex("nama_mogen"));
                    dtl.no_loan = c.getString(c.getColumnIndex("no_loan"));

                    dtl.nama_debitur = c.getString(c.getColumnIndex("nama_debitur"));
                    dtl.tgl_jth_tempo = c.getString(c.getColumnIndex("tgl_jth_tempo"));
                    dtl.no_tlpn = c.getString(c.getColumnIndex("no_tlpn"));
                    dtl.tgl_gajian = c.getString(c.getColumnIndex("tgl_gajian"));
                    dtl.pekerjaan = c.getString(c.getColumnIndex("pekerjaan"));
                    dtl.almt_usaha = c.getString(c.getColumnIndex("almt_usaha"));
                    dtl.id_data_pencairan = c.getString(c.getColumnIndex("id_data_pencairan"));
                    dtl.jarak_tempuh = c.getString(c.getColumnIndex("jarak_tempuh"));
                    dtl.durasi = c.getString(c.getColumnIndex("durasi"));
                    dtl.bucket = c.getString(c.getColumnIndex("bucket"));
                    listdtl.add(dtl);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return listdtl;
    }


    public ArrayList<Detail_Inventory> getIdPenagihanOnDataBooked() {
        ArrayList<Detail_Inventory> listdi = new ArrayList<>();
        Detail_Inventory d;
        String query = "select * from " + Constant.TABLE_DATA_BOOKED + " where tipe_booking='Penagihan'";
        System.out.println("getIdPenagihanOnDataBooked " + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                d = new Detail_Inventory();
                d.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
                listdi.add(d);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listdi;
    }

    public String getNoLoanfromIdDetInv(String iddetinv) {
        String noloan = "";
        String query = "Select * from " + Constant.TABLE_DETAIL_INVENTORY + " where id_detail_inventory='" + iddetinv + "'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                noloan = c.getString(c.getColumnIndex("no_loan"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return noloan;
    }

    public void updateJarakDurasiDetailInventory(String noloan, String jarak, String durasi) {
        String query = "update " + Constant.TABLE_DETAIL_INVENTORY + " set jarak_tempuh='" + jarak + "',durasi='" + durasi + "'" + " where no_loan='" + noloan + "'";
        System.out.println("Query updateJarakDurasiDetailInventory: " + query);
        try {
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil Update JarakDurasi");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Update JarakDurasi");
        }
    }

    public String getCountBSFromDetInv() {
        String jmlh = "0";
        String query = "Select count(*) from " + Constant.TABLE_DETAIL_INVENTORY + " where id_data_pencairan!='' and status='0'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("count(*)"));
                return jmlh;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jmlh;
    }

    public void updateStatusDataPencairan(String noloan) {
        String query = "update " + Constant.TABLE_DETAIL_INVENTORY + " set status='1' where no_loan='" + noloan + "'";
        System.out.println("Query updateStatusDataPencairan: " + query);
        try {
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil Update StatusDataPencairan");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Update StatusDataPencairan");
        }
    }

    public String deleteDetInvByID(String id) {
        String hasil = "gagal";
        try {
            String query = "delete from " + Constant.TABLE_DETAIL_INVENTORY + " where id_detail_inventory='" + id + "'";
            System.out.println("Query deleteDetInvByID: " + query);
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil");
            hasil = "Berhasil";
            return hasil;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
            hasil = "Gagal";
            return hasil;
        }
    }

    public String deleteDetInvByNoLoan(String id) {
        String hasil = "gagal";
        try {
            String query = "delete from " + Constant.TABLE_DETAIL_INVENTORY + " where no_loan='" + id + "'";
            System.out.println("Query deleteDetInvByNoLoan: " + query);
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil");
            hasil = "Berhasil";
            return hasil;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
            hasil = "Gagal";
            return hasil;
        }
    }

    public ArrayList<Detail_Inventory> getArrayListDetInvFromArrListNoLoan(ArrayList<Detil_TGT> arrDTGT) {
        Detail_Inventory dtl;
        ArrayList<Detail_Inventory> listdtl = new ArrayList<>();
        for (Detil_TGT d : arrDTGT) {
            System.out.println("getArrayListDetInvFromArrListNoLoan No Loan: " + d.getNo_loan());
            String query = "select * from " + Constant.TABLE_DETAIL_INVENTORY + " where no_loan='" + d.getNo_loan() + "'";
            System.out.println("getArrayListDetInvFromArrListNoLoan: " + query);
            try {
                Cursor c = Constant.MKSSdb.rawQuery(query, null);
                while (c.moveToNext()) {
                    dtl = new Detail_Inventory();
                    dtl.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
                    dtl.saldo_min = c.getString(c.getColumnIndex("saldo_min"));
                    dtl.dpd_today = c.getString(c.getColumnIndex("dpd_today"));
                    dtl.resume_nsbh = c.getString(c.getColumnIndex("resume_nsbh"));
                    dtl.almt_rumah = c.getString(c.getColumnIndex("almt_rumah"));
                    dtl.tgl_janji_bayar_terakhir = c.getString(c.getColumnIndex("tgl_janji_bayar_terakhir"));
                    dtl.denda = c.getString(c.getColumnIndex("denda"));
                    dtl.pola_bayar = c.getString(c.getColumnIndex("pola_bayar"));
                    dtl.no_hp = c.getString(c.getColumnIndex("no_hp"));
                    dtl.tgl_bayar_terakhir = c.getString(c.getColumnIndex("tgl_bayar_terakhir"));
                    dtl.angsuran_ke = c.getString(c.getColumnIndex("angsuran_ke"));
                    dtl.nama_upliner = c.getString(c.getColumnIndex("nama_upliner"));
                    dtl.pokok = c.getString(c.getColumnIndex("pokok"));
                    dtl.tgl_sp = c.getString(c.getColumnIndex("tgl_sp"));
                    dtl.tenor = c.getString(c.getColumnIndex("tenor"));
                    dtl.angsuran = c.getString(c.getColumnIndex("angsuran"));
                    dtl.gender = c.getString(c.getColumnIndex("gender"));
                    dtl.tgl_ptp = c.getString(c.getColumnIndex("tgl_ptp"));

                    dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                    dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                    dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                    dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                    dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                    dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                    dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                    dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                    dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                    dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                    dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));

                    dtl.action_plan = c.getString(c.getColumnIndex("action_plan"));
                    dtl.no_rekening = c.getString(c.getColumnIndex("no_rekening"));
                    dtl.histori_sp = c.getString(c.getColumnIndex("histori_sp"));
                    dtl.nominal_janji_bayar_terakhir = c.getString(c.getColumnIndex("nominal_janji_bayar_terakhir"));
                    dtl.tlpn_rekomendator = c.getString(c.getColumnIndex("tlpn_rekomendator"));
                    dtl.tlpn_upliner = c.getString(c.getColumnIndex("tlpn_upliner"));
                    dtl.sumber_bayar = c.getString(c.getColumnIndex("sumber_bayar"));
                    dtl.bunga = c.getString(c.getColumnIndex("bunga"));
                    dtl.tlpn_econ = c.getString(c.getColumnIndex("tlpn_econ"));
                    dtl.jns_pinjaman = c.getString(c.getColumnIndex("jns_pinjaman"));
                    dtl.nominal_bayar_terakhir = c.getString(c.getColumnIndex("nominal_bayar_terakhir"));

                    dtl.harus_bayar = c.getString(c.getColumnIndex("harus_bayar"));
                    dtl.nominal_ptp = c.getString(c.getColumnIndex("nominal_ptp"));
                    dtl.saldo = c.getString(c.getColumnIndex("saldo"));
                    dtl.nama_rekomendator = c.getString(c.getColumnIndex("nama_rekomendator"));
                    dtl.tlpn_mogen = c.getString(c.getColumnIndex("tlpn_mogen"));
                    dtl.os_pinjaman = c.getString(c.getColumnIndex("os_pinjaman"));
                    dtl.nama_econ = c.getString(c.getColumnIndex("nama_econ"));
                    dtl.keberadaan_jaminan = c.getString(c.getColumnIndex("keberadaan_jaminan"));
                    dtl.email = c.getString(c.getColumnIndex("email"));
                    dtl.kewajiban = c.getString(c.getColumnIndex("kewajiban"));
                    dtl.total_kewajiban = c.getString(c.getColumnIndex("total_kewajiban"));

                    dtl.nama_mogen = c.getString(c.getColumnIndex("nama_mogen"));
                    dtl.no_loan = c.getString(c.getColumnIndex("no_loan"));

                    dtl.nama_debitur = c.getString(c.getColumnIndex("nama_debitur"));
                    dtl.tgl_jth_tempo = c.getString(c.getColumnIndex("tgl_jth_tempo"));
                    dtl.no_tlpn = c.getString(c.getColumnIndex("no_tlpn"));
                    dtl.tgl_gajian = c.getString(c.getColumnIndex("tgl_gajian"));
                    dtl.pekerjaan = c.getString(c.getColumnIndex("pekerjaan"));
                    dtl.almt_usaha = c.getString(c.getColumnIndex("almt_usaha"));
                    dtl.id_data_pencairan = c.getString(c.getColumnIndex("id_data_pencairan"));
                    dtl.jarak_tempuh = c.getString(c.getColumnIndex("jarak_tempuh"));
                    dtl.durasi = c.getString(c.getColumnIndex("durasi"));
                    dtl.bucket = c.getString(c.getColumnIndex("bucket"));
                    listdtl.add(dtl);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return listdtl;
    }
}


