package com.newkss.acs.newkss.Model;

/**
 * Created by acs on 5/30/17.
 */

public class IsiPenagihan {
    String nama_penagihan;
    String json_penagihan;

    public String getNama_penagihan() {
        return nama_penagihan;
    }

    public void setNama_penagihan(String nama_penagihan) {
        this.nama_penagihan = nama_penagihan;
    }

    public String getJson_penagihan() {
        return json_penagihan;
    }

    public void setJson_penagihan(String json_penagihan) {
        this.json_penagihan = json_penagihan;
    }
}
