package com.newkss.acs.newkss.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;

import java.util.ArrayList;

/**
 * Created by acs on 7/19/17.
 */

public class Master_Settle {

    public String id_settle;
    public String total;
    public String sudah_dibayar;
    public String sisa_dibayar;
    public String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId_settle() {
        return id_settle;
    }

    public void setId_settle(String id_settle) {
        this.id_settle = id_settle;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSudah_dibayar() {
        return sudah_dibayar;
    }

    public void setSudah_dibayar(String sudah_dibayar) {
        this.sudah_dibayar = sudah_dibayar;
    }

    public String getSisa_dibayar() {
        return sisa_dibayar;
    }

    public void setSisa_dibayar(String sisa_dibayar) {
        this.sisa_dibayar = sisa_dibayar;
    }

    public String insertMasterSettlement(Master_Settle settle) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("total", settle.getTotal());
            contentValues.put("sudah_dibayar", settle.getSudah_dibayar());
            contentValues.put("sisa_dibayar", settle.getSisa_dibayar());
            contentValues.put("status", settle.getStatus());
            Constant.MKSSdb.insertOrThrow(Constant.TABLE_MASTER_SETTLEMENT, null, contentValues);
            return "Insert Master Settlement Sukses";
        } catch (Exception e) {
            e.printStackTrace();
            return "Insert Master Settlement Gagal";
        }
    }

    public String deleteMasterSettle() {
        try {
            String query = "delete from " + Constant.TABLE_MASTER_SETTLEMENT + " where status='0'";
            Constant.MKSSdb.execSQL(query);
            return "Sukses";
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
        return "Sukses";
    }

    public String updateMasterSettlement(Master_Settle settle) {
        String query = "update " + Constant.TABLE_MASTER_SETTLEMENT + " set sudah_dibayar='" + settle.getSudah_dibayar() + "',status='" + settle.getStatus() + "',sisa_dibayar='" + settle.getSisa_dibayar() + "' where id_settle='" + settle.getId_settle() + "'";
        System.out.println("Query updateMasterSettlement: " + query);
        try {
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil Update Master Settlement");
            return "Sukses";
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Update Master Settlement");
            return "Gagal";
        }

    }

    public String getIDMasterSettle() {
        String nilai = "kosong";
        String query = "select id_settle from " + Constant.TABLE_MASTER_SETTLEMENT + " where status='0' order by id_settle desc";
        System.out.println("Query getIDMasterSettle" + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                nilai = c.getString(c.getColumnIndex("id_settle"));
                return nilai;
            } else {
                c.close();
                return nilai;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return nilai;
        }
    }

    public String getSisaDibayarQuery() {
        String nilai = "kosong";
        String query = "select sisa_dibayar from " + Constant.TABLE_MASTER_SETTLEMENT + " where status='0'";
        System.out.println("Query getSudahDibayar" + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                nilai = c.getString(c.getColumnIndex("sisa_dibayar"));
                return nilai;
            } else {
                c.close();
                return nilai;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return nilai;
        }
    }

    public String getSudahDibayarQuery() {
        String nilai = "kosong";
        String query = "select sudah_dibayar from " + Constant.TABLE_MASTER_SETTLEMENT + " where status='0'";
        System.out.println("Query getSudahDibayar" + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                nilai = c.getString(c.getColumnIndex("sudah_dibayar"));
                return nilai;
            } else {
                c.close();
                return nilai;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return nilai;
        }
    }

    public String getTotalBayarQuery() {
        String nilai = "kosong";
        String query = "select total from " + Constant.TABLE_MASTER_SETTLEMENT + " where status='0'";
        System.out.println("Query getTotalBayar" + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                nilai = c.getString(c.getColumnIndex("total"));
                return nilai;
            } else {
                c.close();
                return nilai;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return nilai;
        }
    }

    public String getAngkaSettlementKe() {
        String angka = "";
        String angka1 = "";
        String query = "Select * from " + Constant.TABLE_SETTLEMENT + " order by settle_ke desc";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                angka1 = c.getString(c.getColumnIndex("settle_ke"));
                angka = String.valueOf((Integer.parseInt(angka1) + 1));
                return angka;
            } else {
                angka = "1";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return angka;
    }

    public ArrayList<Master_Settle> getAllDataMasterSettle() {
        ArrayList<Master_Settle> listMasterSettle = new ArrayList<>();
        Master_Settle ms;
        String query = "select * from " + Constant.TABLE_MASTER_SETTLEMENT + " where status='0'";
        System.out.println("Query getAllDataMasterSettle: " + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (!c.moveToFirst()) {
                //System.out.println("empty");
            } else {
                while (c.moveToNext()) {
                    ms = new Master_Settle();
                    ms.setId_settle(c.getString(c.getColumnIndex("id_settle")));
                    ms.setTotal(c.getString(c.getColumnIndex("total")));
                    ms.setSudah_dibayar(c.getString(c.getColumnIndex("sudah_dibayar")));
                    ms.setSisa_dibayar(c.getString(c.getColumnIndex("sisa_dibayar")));
                    ms.setStatus(c.getString(c.getColumnIndex("status")));

                    listMasterSettle.add(ms);
                }
                c.close();
            }

            return listMasterSettle;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listMasterSettle;
    }

}
