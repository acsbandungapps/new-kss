package com.newkss.acs.newkss.Model;

import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.Function;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by acs on 9/8/17.
 */

public class ObjectActionCode {
    String key;
    String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ArrayList<ObjectActionCode> getDataObjectActionCode() {
        ArrayList<ObjectActionCode> listOAC = new ArrayList<>();
        ObjectActionCode dataOAC;
        try {
            String query = "select * from " + Constant.TABLE_PARAMETER + " where nama_parameter='action_code'";
            System.out.println("Query getDataObjectActionCode " + query);
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                //String jsonArrayActionCode = "[{\"13\":\"Visit rumah Broken Promise\"},{\"14\":\"Visit rumah titip pesan\"},{\"15\":\"Visit rumah invalid\\/fiktif\"},{\"00\":\"Visit bayar\"},{\"21\":\"Visit usaha bayar\"},{\"23\":\"Visit usaha Broken Promise\"},{\"24\":\"Visit usaha titip pesan\"},{\"25\":\"Visit usaha invalid\\/fiktif\"},{\"01\":\"Debitur\"},{\"02\":\"Keluarga Serumah istri\\/suami\\/anak\\/kakak\\/adik\\/orang tua\"},{\"03\":\"Keluarga tidak Serumah istri\\/suami\\/anak\\/kakak\\/adik\\/orang tua\"},{\"04\":\"Kosong, Kolektor tidak dapat menemui siapa-siapa\"},{\"06\":\"Karyawan Usaha\"},{\"07\":\"Lain-lain\"},{\"01\":\"Normal\"},{\"02\":\"Over Financing\"},{\"03\":\"Side Streaming\"},{\"04\":\"Relokasi Pasar\"},{\"05\":\"Musibah\"},{\"06\":\"Sakit-tidak ada yang meneruskan usaha\"},{\"08\":\"Indikasi Fraud\"},{\"09\":\"Family Problem\"},{\"10\":\"Penurunan Harga Jual Hasil produksi di pasar\"},{\"11\":\"Bad strategi\\/Salah Alokasi dana\"},{\"12\":\"Tertipu \"},{\"01\":\"Pembayaran Penuh\"},{\"02\":\"Pembayaran Sebagian\"},{\"03\":\"Tidak Ada Pembayaran\"},{\"05\":\"Tetangga atau lingkungan\"},{\"11\":\"Visit rumah bayar .\"},{\"12\":\"Visit rumah janji bayar Bulan depan\"},{\"99\":\"berhenti bekerja - PENDI - PULANG \"},{\"24\":\"tes\"},{\"JJ\":\"Janji janjji\"},{\"05\":\"Promises To Pay\"},{\"06\":\"Broken promises\"},{\"\":\"\"},{\"\":\"\"},{\"07\":\"Meninggal\"}]";
                String jsonArrayActionCode = c.getString(c.getColumnIndex("isi_parameter"));
                JSONArray jsonArrayActionCode1 = new JSONArray(jsonArrayActionCode.toString());

                for (int i = 0; i < jsonArrayActionCode1.length(); i++) {
                    String dtlPencairan = jsonArrayActionCode1.getString(i);
                    //System.out.println("NILAI: " + i + "-" + dtlPencairan);
                    JSONObject jojojo = jsonArrayActionCode1.getJSONObject(i);
                    //System.out.println("Json jojojo " + jsonArrayPencairan1.length());
                    //System.out.println("String action code " + Function.removeVarActionCode(jojojo.toString()));
                    String nilai = Function.removeVarActionCode(jojojo.toString());
                    // System.out.println("Count: " + nilai.length());

                    if (nilai.length() > 1) {
                        String[] items = nilai.split(":");

                        String part1 = items[0]; // 004
                        String part2 = items[1]; // 034556

                        dataOAC = new ObjectActionCode();
                        dataOAC.setKey(part1);
                        dataOAC.setValue(part2);
                        listOAC.add(dataOAC);
                    }
                }
                return listOAC;
            }
//
//                        int count = 0;
//                        int count1 = 0;
//                        int count2 = 0;
//                        List<ObjectActionCode> listOAC = new ArrayList<ObjectActionCode>();
//                        // HashMap<String, String> hashActionCode = new HashMap<String, String>();
//                        for (int i = 0; i < jsonArrayPencairan1.length(); i++) {
//                            String dtlPencairan = jsonArrayPencairan1.getString(i);
//                            //System.out.println("NILAI: " + i + "-" + dtlPencairan);
//                            JSONObject jojojo = jsonArrayPencairan1.getJSONObject(i);
//                            //System.out.println("Json jojojo " + jsonArrayPencairan1.length());
//                            //System.out.println("String action code " + Function.removeVarActionCode(jojojo.toString()));
//                            String nilai = Function.removeVarActionCode(jojojo.toString());
//                            // System.out.println("Count: " + nilai.length());
//
//                            if (nilai.length() > 1) {
//
//                                String[] items = nilai.split(":");
//
//                                String part1 = items[0]; // 004
//                                String part2 = items[1]; // 034556
////                                    System.out.println("Items 0 " + part1.toString());
////                                    System.out.println("Items 1 " + part2.toString());
//                                //hashActionCode.put(part1.toString(), part2.toString());
//                                dataOAC = new ObjectActionCode();
//                                dataOAC.setKey(part1);
//                                dataOAC.setValue(part2);
//                                listOAC.add(dataOAC);
//
//                                count2++;
//                                System.out.println("Count 2: " + count2);
//                            } else {
//                                count1++;
//                                System.out.println("Count 1: " + count1);
//                            }
//
//
////                            System.out.println("Items 0 "+items[0].toString());
////                            System.out.println("Items 1 "+items[1].toString());
//
//                        }
//                        System.out.println("Listoac size " + listOAC.size());
//                        for (int j = 0; j < listOAC.size(); j++) {
//                            System.out.println("ISI LIST " + listOAC.get(j).getValue());
//
//                        }
//                        for (ObjectActionCode oa : listOAC) {
//                            if (oa.getValue().equals("Visit bayar")) {
//                                System.out.println("ini key " + oa.getKey());
//                            }
//                        }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listOAC;
    }
}
