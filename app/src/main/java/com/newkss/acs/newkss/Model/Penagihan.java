package com.newkss.acs.newkss.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by acs on 5/29/17.
 */

public class Penagihan {
    String id_penagihan;
    String nama_penagihan;

    public String getNama_penagihan() {
        return nama_penagihan;
    }

    public void setNama_penagihan(String nama_penagihan) {
        this.nama_penagihan = nama_penagihan;
    }

    public String getId_penagihan() {
        return id_penagihan;
    }

    public void setId_penagihan(String id_penagihan) {
        this.id_penagihan = id_penagihan;
    }

    public String getIdPenagihanfromNama(String nama) {
        String id = "";
        String query = "select id_penagihan from " + Constant.TABLE_PENAGIHAN + " WHERE nama_penagihan='" + nama + "'";
        System.out.println(query);
        Cursor c = Constant.MKSSdb.rawQuery(query, null);
        try {

            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_penagihan"));
                return id;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.close();
        }
        return id;
    }

    public Penagihan getDataPenagihan() {
        Penagihan b = new Penagihan();
        String query = "Select * from " + Constant.TABLE_PENAGIHAN;
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                b.id_penagihan = c.getString(c.getColumnIndex("id_penagihan"));
                b.nama_penagihan = c.getString(c.getColumnIndex("nama_penagihan"));
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return b;
    }

    public ArrayList<Penagihan> getListPenagihan() {
        Map<String, String> temp = new HashMap<>();
        ArrayList<Penagihan> arrList = new ArrayList<>();
        Penagihan b;
        String query = "Select * from " + Constant.TABLE_PENAGIHAN;
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                b = new Penagihan();
                b.id_penagihan = c.getString(c.getColumnIndex("id_penagihan"));
                b.nama_penagihan = c.getString(c.getColumnIndex("nama_penagihan"));
                arrList.add(b);
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return arrList;
    }


    public void insertPenagihan(List<Penagihan> namaPenagihan) {
        try {
            String checkKosong = checkPenagihanEmpty();
            if (checkKosong.equals("Ada")) {
                deletePenagihan();
                deleteSequencePenagihan();
                for (Penagihan p : namaPenagihan) {
                    System.out.println("Size: " + namaPenagihan.size());
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(Constant.NAMA_PENAGIHAN, p.getNama_penagihan());
                    Constant.MKSSdb.insertOrThrow(Constant.TABLE_PENAGIHAN, null, contentValues);


                }
//isi detail inventory, dapet idnya, insert ke detail bucket berdasarkan npay fpay


            } else if (checkKosong.equals("Kosong")) {
                for (Penagihan p : namaPenagihan) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(Constant.NAMA_PENAGIHAN, p.getNama_penagihan());
                    Constant.MKSSdb.insertOrThrow(Constant.TABLE_PENAGIHAN, null, contentValues);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void deletePenagihan() {
        try {
            Constant.MKSSdb.delete(Constant.TABLE_PENAGIHAN, null, null);
            //Constant.MKSSdb.close();
            System.out.println("Sukses Hapus " + Constant.TABLE_PENAGIHAN);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }

    public static String checkPenagihanEmpty() {
        String query = "Select count(*) from " + Constant.TABLE_PENAGIHAN;
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            c.moveToFirst();
            int icount = c.getInt(0);
            if (icount > 0) {
                return "Ada";
            } else {
                c.close();
                return "Kosong";

            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    public void deleteSequencePenagihan() {
        try {
            Constant.MKSSdb.delete(Constant.TABLE_SEQUENCE, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_SEQUENCE);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }
}
