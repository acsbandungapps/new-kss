package com.newkss.acs.newkss.Model;

import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;

/**
 * Created by acs on 6/19/17.
 */

public class Pencairan {
    String id_pencairan;
    String nama_pencairan;

    public String getNama_pencairan() {
        return nama_pencairan;
    }

    public void setNama_pencairan(String nama_pencairan) {
        this.nama_pencairan = nama_pencairan;
    }

    public String getId_pencairan() {
        return id_pencairan;
    }

    public void setId_pencairan(String id_pencairan) {
        this.id_pencairan = id_pencairan;
    }

    public String getIdPencairanfromNama(String nama){
        String id="";
        String query="select id_pencairan from "+ Constant.TABLE_PENCAIRAN+" WHERE nama_pencairan='"+nama+"'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_pencairan"));
                return id;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }
}
