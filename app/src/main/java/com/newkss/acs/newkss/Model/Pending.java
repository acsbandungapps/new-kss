package com.newkss.acs.newkss.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import java.util.ArrayList;

/**
 * Created by acs on 7/19/17.
 */

public class Pending {
    public String id_pending;
    public String data;
    public String url;
    public String waktu;
    public String json_update;
    public String ket_update;
    public String status;
    public String header;
    public String keterangan;


    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getId_pending() {
        return id_pending;
    }

    public void setId_pending(String id_pending) {
        this.id_pending = id_pending;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getJson_update() {
        return json_update;
    }

    public void setJson_update(String json_update) {
        this.json_update = json_update;
    }

    public String getKet_update() {
        return ket_update;
    }

    public void setKet_update(String ket_update) {
        this.ket_update = ket_update;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String insertPendingData(Pending pending) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("data", pending.getData());
            contentValues.put("url", pending.getUrl());
            contentValues.put("waktu", pending.getWaktu());
            contentValues.put("json_update", pending.getJson_update());
            contentValues.put("ket_update", pending.getKet_update());
            contentValues.put("status", pending.getStatus());
            contentValues.put("header", pending.getHeader());
            Constant.MKSSdb.insertOrThrow(Constant.TABLE_PENDING, null, contentValues);
            return "Sukses Insert Pending Data";
        } catch (Exception e) {
            e.printStackTrace();
            return "Gagal Insert Pending Data";
        }
    }

    public String cekAvailable() {
        String status = "kosong";
        String query = "select * from " + Constant.TABLE_PENDING + " where status='0'";
        System.out.println("Query cekavailable " + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                status = c.getString(c.getColumnIndex("status"));
                return status;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return status;
        }
        return status;
    }

    public ArrayList<Pending> getArrayListDataPending() {
        ArrayList<Pending> arrPending = new ArrayList<>();
        Pending dataPending;
        String query = "select * from " + Constant.TABLE_PENDING + " where status!='1' and status!='3'";
        System.out.println("Query getArrayListDataPending " + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                dataPending = new Pending();
                dataPending.status = c.getString(c.getColumnIndex("status"));
                dataPending.id_pending = c.getString(c.getColumnIndex("id_pending"));
                dataPending.data = c.getString(c.getColumnIndex("data"));
                dataPending.url = c.getString(c.getColumnIndex("url"));
                dataPending.header = c.getString(c.getColumnIndex("header"));
                dataPending.waktu = c.getString(c.getColumnIndex("waktu"));
                dataPending.json_update = c.getString(c.getColumnIndex("json_update"));
                dataPending.ket_update = c.getString(c.getColumnIndex("ket_update"));
                arrPending.add(dataPending);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrPending;
    }

    public Pending getDataPending() {
        Pending dataPending = new Pending();
        String query = "select * from " + Constant.TABLE_PENDING + " where status='2'";
        System.out.println("Query getDataPending " + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                dataPending.status = c.getString(c.getColumnIndex("status"));
                dataPending.id_pending = c.getString(c.getColumnIndex("id_pending"));
                dataPending.data = c.getString(c.getColumnIndex("data"));
                dataPending.url = c.getString(c.getColumnIndex("url"));
                dataPending.header = c.getString(c.getColumnIndex("header"));
                dataPending.waktu = c.getString(c.getColumnIndex("waktu"));
                dataPending.json_update = c.getString(c.getColumnIndex("json_update"));
                dataPending.ket_update = c.getString(c.getColumnIndex("ket_update"));
                return dataPending;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Pending getDataPendingById(String idPending) {
        Pending dataPending = new Pending();
        String query = "select * from " + Constant.TABLE_PENDING + " where id_pending='"+idPending+"'";
        System.out.println("Query getDataPending " + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                dataPending.status = c.getString(c.getColumnIndex("status"));
                dataPending.id_pending = c.getString(c.getColumnIndex("id_pending"));
                dataPending.data = c.getString(c.getColumnIndex("data"));
                dataPending.url = c.getString(c.getColumnIndex("url"));
                dataPending.header = c.getString(c.getColumnIndex("header"));
                dataPending.waktu = c.getString(c.getColumnIndex("waktu"));
                dataPending.json_update = c.getString(c.getColumnIndex("json_update"));
                dataPending.ket_update = c.getString(c.getColumnIndex("ket_update"));
                return dataPending;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean synchronizeTablePending(String tableName, String columnName, String columnType) {
        String sql = "SELECT " + columnName + " FROM " + tableName;

        String sqlTable = "CREATE TABLE IF NOT EXISTS " + tableName + "(" + columnName + " " + columnType + ");";
        MySQLiteHelper.sqLiteDB.execSQL(sqlTable);
        try {
            MySQLiteHelper.sqLiteDB.rawQuery(sql, null);
            return true;

        } catch (Exception e) {
            System.out.println("ALTER TABLE, ADD NEW COLUMN: " + columnName);
            sql = "ALTER TABLE '" + Constant.TABLE_PENDING + "' ADD COLUMN " + columnName + " " + columnType;
            try {
                MySQLiteHelper.sqLiteDB.execSQL(sql);
                return true;
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        }
    }


    //update where id 0 jadi 1 kalo sukses

    public void deleteAllPendingData() {
        try {
            String query = "delete from " + Constant.TABLE_PENDING;
            System.out.println("Query deleteAllPendingData: " + query);
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil delete Pending");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }


    public void updateStatusPendingData(String id) {
        String query = "update " + Constant.TABLE_PENDING + " set status='1' where id_pending='" + id + "'";
        try {
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil Update Pending Data");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Update Pending Data");
        }
    }

    public void updateStatusPendingDataFailed(String id) {
        String query = "update " + Constant.TABLE_PENDING + " set status='0' where id_pending='" + id + "'";
        try {
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil Update Pending Data");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Update Pending Data");
        }
    }



    public void hapusPendingData(String id) {
        String query = "update " + Constant.TABLE_PENDING + " set status='3' where id_pending='" + id + "'";
        try {
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil Update Pending Data");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Update Pending Data");
        }
    }

    public void updateKeteranganPendingData(String id,String keterangan) {
        String query = "update " + Constant.TABLE_PENDING + " set desc='"+keterangan+"' where id_pending='" + id + "'";
        try {
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil Update Pending Data");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Update Pending Data");
        }
    }

}
