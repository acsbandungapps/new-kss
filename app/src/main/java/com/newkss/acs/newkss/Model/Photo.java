package com.newkss.acs.newkss.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;

import java.util.ArrayList;

/**
 * Created by acs on 6/12/17.
 */

public class Photo {
    String id_photo;
    String nama_photo;
    String url_photo;
    String keterangan;
    String id_detail_inventory;
    String waktu;
    String noloan;
    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNoloan() {
        return noloan;
    }

    public void setNoloan(String noloan) {
        this.noloan = noloan;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getId_photo() {
        return id_photo;
    }

    public void setId_photo(String id_photo) {
        this.id_photo = id_photo;
    }

    public String getNama_photo() {
        return nama_photo;
    }

    public void setNama_photo(String nama_photo) {
        this.nama_photo = nama_photo;
    }

    public String getUrl_photo() {
        return url_photo;
    }

    public void setUrl_photo(String url_photo) {
        this.url_photo = url_photo;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getId_detail_inventory() {
        return id_detail_inventory;
    }

    public void setId_detail_inventory(String id_detail_inventory) {
        this.id_detail_inventory = id_detail_inventory;
    }

    public void insertPhotoIntoSqlite(Photo ph) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(Constant.NAMA_PHOTO, ph.getNama_photo());
            contentValues.put(Constant.URL_PHOTO, ph.getUrl_photo());
            contentValues.put(Constant.KETERANGAN_PHOTO, ph.getKeterangan());
            contentValues.put(Constant.ID_DETAIL_INVENTORY, ph.getId_detail_inventory());
            contentValues.put(Constant.WAKTU, ph.getWaktu());
            contentValues.put(Constant.NO_LOAN, ph.getNoloan());
            contentValues.put("status", "0");
            Constant.MKSSdb.insertOrThrow(Constant.TABLE_PHOTO, null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Photo> getImageFromSqlite() {
        Photo pp;
        ArrayList<Photo> list = new ArrayList<>();
        String query = "Select * from " + Constant.TABLE_PHOTO;
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                pp = new Photo();
                pp.setId_photo(c.getString(c.getColumnIndex("id_photo")));
                pp.setNama_photo(c.getString(c.getColumnIndex("nama_photo")));
                pp.setUrl_photo(c.getString(c.getColumnIndex("url_photo")));
                pp.setKeterangan(c.getString(c.getColumnIndex("keterangan")));
                pp.setId_detail_inventory(c.getString(c.getColumnIndex("id_detail_bucket")));
                list.add(pp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<Photo> getImageFromSqlite(String id) {
        Photo pp;
        ArrayList<Photo> list = new ArrayList<>();
        String query = "Select * from " + Constant.TABLE_PHOTO + " where id_detail_inventory='" + id + "' and status='0'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                pp = new Photo();
                pp.setId_photo(c.getString(c.getColumnIndex("id_photo")));
                pp.setNama_photo(c.getString(c.getColumnIndex("nama_photo")));
                pp.setUrl_photo(c.getString(c.getColumnIndex("url_photo")));
                pp.setKeterangan(c.getString(c.getColumnIndex("keterangan")));
                pp.setId_detail_inventory(c.getString(c.getColumnIndex("id_detail_inventory")));
                list.add(pp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    public ArrayList<Photo> getimageByNoLoan(String noloan) {
        Photo pp;
        ArrayList<Photo> list = new ArrayList<>();
        String query = "Select * from " + Constant.TABLE_PHOTO + " where no_loan='" + noloan + "' and status='0'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                pp = new Photo();
                pp.setId_photo(c.getString(c.getColumnIndex("id_photo")));
                pp.setNama_photo(c.getString(c.getColumnIndex("nama_photo")));
                pp.setUrl_photo(c.getString(c.getColumnIndex("url_photo")));
                pp.setKeterangan(c.getString(c.getColumnIndex("keterangan")));
                pp.setId_detail_inventory(c.getString(c.getColumnIndex("id_detail_inventory")));
                pp.setNoloan(c.getString(c.getColumnIndex("no_loan")));
                pp.setWaktu(c.getString(c.getColumnIndex("waktu")));
                list.add(pp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public String updateKetPhoto(String id, String ket) {
        String query = "update " + Constant.TABLE_PHOTO + " set keterangan='" + ket + "' where id_photo='" + id + "'";
        try {
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil Update Photo");
            return "Sukses Ubah Keterangan Photo";
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Update Photo");
            return "Gagal Ubah Keterangan Photo";
        }
    }

    public void updateStatusPhoto(String id) {
        String query = "update " + Constant.TABLE_PHOTO + " set status='1' where no_loan='" + id + "'";
        System.out.println("Query updateStatusPhoto: "+query);
        try {
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil Update Status Photo");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Update Status Photo");
        }
    }

    public void deletePhoto(String nama) {
//        try {
//            Constant.MKSSdb.delete(Constant.TABLE_PHOTO, "", null);
//            //Constant.MKSSdb.close();
//            System.out.println("Sukses Hapus " + Constant.TABLE_PENAGIHAN);
//        } catch (Exception e) {
//            e.printStackTrace();
//            System.out.println("Gagal: " + e.getMessage());
//        }
        try {
            String query = "delete from " + Constant.TABLE_PHOTO + " where nama_photo='" + nama + "'";
            System.out.println("Query delete: " + query);
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }

    }


}
