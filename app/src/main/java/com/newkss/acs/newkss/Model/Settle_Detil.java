package com.newkss.acs.newkss.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import java.util.ArrayList;

/**
 * Created by acs on 7/19/17.
 */

public class Settle_Detil {
    public String id_detil_settle;
    public String nama;
    public String no_rek;
    public String no_loan;
    public String nominal_bayar;
    public String total_kewajiban;
    public String id_detail_inventory;
    public String status;
    public String sisa_bayar;
    public String waktu;
    public String uniqueCode;

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public boolean synchronizeTableSettleDetil(String tableName, String columnName, String columnType) {
        String sql = "SELECT " + columnName + " FROM " + tableName;

        String sqlTable = "CREATE TABLE IF NOT EXISTS " + tableName + "(" + columnName + " " + columnType + ");";
        MySQLiteHelper.sqLiteDB.execSQL(sqlTable);
        try {
            MySQLiteHelper.sqLiteDB.rawQuery(sql, null);
            return true;

        } catch (Exception e) {
            System.out.println("ALTER TABLE, ADD NEW COLUMN: " + columnName);
            sql = "ALTER TABLE '" + Constant.TABLE_DETIL_SETTLEMENT + "' ADD COLUMN " + columnName + " " + columnType;
            try {
                MySQLiteHelper.sqLiteDB.execSQL(sql);
                return true;
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        }
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getSisa_bayar() {
        return sisa_bayar;
    }

    public void setSisa_bayar(String sisa_bayar) {
        this.sisa_bayar = sisa_bayar;
    }

    public String getId_detil_settle() {
        return id_detil_settle;
    }

    public void setId_detil_settle(String id_detil_settle) {
        this.id_detil_settle = id_detil_settle;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNo_rek() {
        return no_rek;
    }

    public void setNo_rek(String no_rek) {
        this.no_rek = no_rek;
    }

    public String getNo_loan() {
        return no_loan;
    }

    public void setNo_loan(String no_loan) {
        this.no_loan = no_loan;
    }

    public String getNominal_bayar() {
        return nominal_bayar;
    }

    public void setNominal_bayar(String nominal_bayar) {
        this.nominal_bayar = nominal_bayar;
    }

    public String getTotal_kewajiban() {
        return total_kewajiban;
    }

    public void setTotal_kewajiban(String total_kewajiban) {
        this.total_kewajiban = total_kewajiban;
    }

    public String getId_detail_inventory() {
        return id_detail_inventory;
    }

    public void setId_detail_inventory(String id_detail_inventory) {
        this.id_detail_inventory = id_detail_inventory;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String cekUniqueCodeAvailable(String uniqueCode) {
        String status = "kosong";
        String query = "select * from " + Constant.TABLE_PENDING + " where unique_code='"+uniqueCode+"'";
        System.out.println("Query cekUniqueCodeAvailable " + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                status = "ada";
                return status;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return status;
        }
        return status;
    }

    public String insertSettleDetil(Settle_Detil settle_detil) {
        try {

            //System.out.println("Settle detil: "+settle_detil.getNama());
            ContentValues contentValues = new ContentValues();
            contentValues.put("nama", settle_detil.getNama());
            contentValues.put("no_rek", settle_detil.getNo_rek());
            contentValues.put("no_loan", settle_detil.getNo_loan());
            contentValues.put("nominal_bayar", settle_detil.getNominal_bayar());
            contentValues.put("total_kewajiban", settle_detil.getTotal_kewajiban());
            //  contentValues.put("sisa_bayar", settle_detil.getSisa_bayar());
            contentValues.put("id_detail_inventory", settle_detil.getId_detail_inventory());
            contentValues.put("status", settle_detil.getStatus());
            contentValues.put("waktu", settle_detil.getWaktu());
            contentValues.put("unique_code", settle_detil.getUniqueCode());
            Constant.MKSSdb.insertOrThrow(Constant.TABLE_DETIL_SETTLEMENT, null, contentValues);
            return "Insert Sukses";
        } catch (Exception e) {
            e.printStackTrace();
            return "Insert Gagal";
        }
    }

    public void updateStatusSettleDetil() {
        String query = "update " + Constant.TABLE_DETIL_SETTLEMENT + " set status='1'";
        System.out.println("Query updateStatusSettleDetil: " + query);
        try {
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil updateStatusSettleDetil");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal updateStatusSettleDetil");
        }

    }

    public ArrayList<String> getWaktuSettleDetil(){
        ArrayList<String> list = new ArrayList<>();
        Settle_Detil s;
        String query = "Select * from " + Constant.TABLE_DETIL_SETTLEMENT + " where status='0'";
        System.out.println("Query getWaktuSettleDetil " + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                String waktu=c.getString(c.getColumnIndex("waktu"));
                list.add(waktu);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }
    public ArrayList<Settle_Detil> getAllDataSettlementDetil() {
        ArrayList<Settle_Detil> list = new ArrayList<>();
        Settle_Detil s;
        String query = "Select * from " + Constant.TABLE_DETIL_SETTLEMENT + " where status='0'";
        System.out.println("Query getAllDataSettlementDetil " + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                s = new Settle_Detil();
                s.setNama(c.getString(c.getColumnIndex("nama")));
                s.setNo_rek(c.getString(c.getColumnIndex("no_rek")));
                s.setNo_loan(c.getString(c.getColumnIndex("no_loan")));
                s.setNominal_bayar(c.getString(c.getColumnIndex("nominal_bayar")));
                s.setTotal_kewajiban(c.getString(c.getColumnIndex("total_kewajiban")));
                s.setSisa_bayar(c.getString(c.getColumnIndex("sisa_bayar")));
                s.setId_detail_inventory(c.getString(c.getColumnIndex("id_detail_inventory")));
                s.setStatus(c.getString(c.getColumnIndex("status")));

                list.add(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public String getTotalNominalSettlement() {
        String jmlh = "0";
        // String query = "Select sum(nominal_bayar) as jmlh from " + Constant.TABLE_DETIL_SETTLEMENT + " where status='0'";
        String query = "Select sum(nominal_bayar) as jmlh from " + Constant.TABLE_DETIL_SETTLEMENT + " where status='0'";
        System.out.println("Query getTotalNominalSettlement: " + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                if (c.getString(c.getColumnIndex("jmlh")) == null) {
                    jmlh = "0";
                } else {
                    jmlh = c.getString(c.getColumnIndex("jmlh"));
                    System.out.println("Jumlah " + jmlh);
                }

                return jmlh;
            } else {
                jmlh = "0";
                return jmlh;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        System.out.println("Jumlah " + jmlh);
        return jmlh;
    }

    public String getTotalAllNominalSettlement() {
        String jmlh = "0";
        // String query = "Select sum(nominal_bayar) as jmlh from " + Constant.TABLE_DETIL_SETTLEMENT + " where status='0'";
        String query = "Select sum(nominal_bayar) as jmlh from " + Constant.TABLE_DETIL_SETTLEMENT + " where status='0'";
        System.out.println("Query getTotalNominalSettlement: " + query);
        Cursor c = Constant.MKSSdb.rawQuery(query, null);
        try {

            if (c.moveToFirst()) {
                if (c.getString(c.getColumnIndex("jmlh")) == null) {
                    jmlh = "0";
                } else {
                    jmlh = c.getString(c.getColumnIndex("jmlh"));
                    System.out.println("Jumlah " + jmlh);
                }

                return jmlh;
            } else {
                jmlh = "0";
                return jmlh;
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            c.close();
        }
        System.out.println("Jumlah " + jmlh);
        return jmlh;
    }


    public String isSettleAvailable() {
        String jmlh = "0";
        String query = "Select count(*) from " + Constant.TABLE_DETIL_SETTLEMENT + " where status='0'";
        System.out.println("Query isSettleAvailable " + query);
        Cursor c = Constant.MKSSdb.rawQuery(query, null);
        try {
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("count(*)"));
                System.out.println("count(*) " + jmlh);
            }
        } catch (Exception e) {
            jmlh = "0";
            e.printStackTrace();
        } finally {
            c.close();
        }
        return jmlh;
    }

    public String checkExistPeriodicTrack() {
        String id = "";
        String query = "select count(*) from " + Constant.TABLE_CONFIG + " where nama_config='" + Constant.PERIODIK_TRACK + "'";
        System.out.println("Query checkExistPeriodicTrack " + query);

        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {

                id = c.getString(c.getColumnIndex("count(*)"));
                System.out.println("count(*) " + id);
            }
        } catch (Exception e) {
            id = "0";
            e.printStackTrace();
        }
        return id;
    }


}
