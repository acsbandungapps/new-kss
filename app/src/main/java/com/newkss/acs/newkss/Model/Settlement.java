package com.newkss.acs.newkss.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by acs on 6/15/17.
 */

public class Settlement {

    public String id_settle;
    public String settle_ke;
    public String total_settle;
    public String sisa_settle;
    public String sudah_dibayar;
    public String status;
    public String id_master_settle;
    public String no_rek;
    public String nama;
    public String no_loan;

    public String getNo_rek() {
        return no_rek;
    }

    public void setNo_rek(String no_rek) {
        this.no_rek = no_rek;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNo_loan() {
        return no_loan;
    }

    public void setNo_loan(String no_loan) {
        this.no_loan = no_loan;
    }

    public String getId_master_settle() {
        return id_master_settle;
    }

    public void setId_master_settle(String id_master_settle) {
        this.id_master_settle = id_master_settle;
    }

    public String getId_settle() {
        return id_settle;
    }

    public void setId_settle(String id_settle) {
        this.id_settle = id_settle;
    }

    public String getSettle_ke() {
        return settle_ke;
    }

    public void setSettle_ke(String settle_ke) {
        this.settle_ke = settle_ke;
    }

    public String getTotal_settle() {
        return total_settle;
    }

    public void setTotal_settle(String total_settle) {
        this.total_settle = total_settle;
    }

    public String getSisa_settle() {
        return sisa_settle;
    }

    public void setSisa_settle(String sisa_settle) {
        this.sisa_settle = sisa_settle;
    }

    public String getSudah_dibayar() {
        return sudah_dibayar;
    }

    public void setSudah_dibayar(String sudah_dibayar) {
        this.sudah_dibayar = sudah_dibayar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String insertSettlement(Settlement settle) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("settle_ke", settle.getSettle_ke());
            contentValues.put("total_settle", settle.getTotal_settle());
            //contentValues.put("sisa_settle", settle.getSisa_settle());
            contentValues.put("sudah_dibayar", settle.getSudah_dibayar());
            contentValues.put("status", settle.getStatus());
            contentValues.put("nama", settle.getNama());
            contentValues.put("no_rek", settle.getNo_rek());
            contentValues.put("no_loan", settle.getNo_loan());
            Constant.MKSSdb.insertOrThrow(Constant.TABLE_SETTLEMENT, null, contentValues);
            return "Insert Settlement Sukses";
        } catch (Exception e) {
            e.printStackTrace();
            return "Insert Settlement Gagal";
        }
    }

    public static String updateSettlement(Settlement settle) {
        String query = "update " + Constant.TABLE_SETTLEMENT + " set status='" + settle.getStatus() + "',settle_ke='" + settle.getSettle_ke() + "',total_settle='" + settle.getTotal_settle() + "',sudah_dibayar='" + settle.getSudah_dibayar() + "' where id_settle='" + settle.getId_settle() + "'";
        try {
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil Update Settlement");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Update Settlement");
        }
        return "Sukses";
    }

    public void updateStatusSettlement() {
        String query = "update " + Constant.TABLE_SETTLEMENT + " set status='1'";
        System.out.println("Query updateStatusSettlement: " + query);
        try {
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil Update Settlement");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal Update Settlement");
        }
    }


    public String getAngkaSettlementKe() {
        String angka = "1";
        String angka1 = "";
        String query = "Select * from " + Constant.TABLE_SETTLEMENT + " order by settle_ke desc";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                angka1 = c.getString(c.getColumnIndex("settle_ke"));
                angka = String.valueOf((Integer.parseInt(angka1) + 1));
                return angka;
            } else {
                c.close();
                angka = "1";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return angka;
    }

    public String insertCustomSettlement(Settlement settlement) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("settle_ke", settlement.getSettle_ke());
//            contentValues.put("total_settle", settle.getTotal_settle());
            //contentValues.put("sisa_settle", settle.getSisa_settle());
            contentValues.put("sudah_dibayar", settlement.getSudah_dibayar());
            contentValues.put("status", settlement.getStatus());
//            contentValues.put("nama", settle.getNama());
//            contentValues.put("no_rek", settle.getNo_rek());
//            contentValues.put("no_loan", settle.getNo_loan());
            Constant.MKSSdb.insertOrThrow(Constant.TABLE_SETTLEMENT, null, contentValues);
            return "Insert Settlement Sukses";
        } catch (Exception e) {
            e.printStackTrace();
            return "Insert Settlement Gagal";
        }
    }

    public String getCustomSudahDibayar() {
        String angka = "0";
        String query = "select sum(sudah_dibayar) as jmlh from "+ Constant.TABLE_SETTLEMENT + " where status='0' order by settle_ke desc";
        System.out.println("Query getCustomSudahDibayar: "+query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                if(c.getString(c.getColumnIndex("jmlh"))==null){
                    angka="0";
                }
                else{
                    angka = c.getString(c.getColumnIndex("jmlh"));
                }
                //angka = String.valueOf((Integer.parseInt(angka1) + 1));
                return angka;
            } else {
                c.close();
                angka = "0";
                return angka;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return angka;
    }

    public String getSisaBelumSettle() {
        String angka = "";
        String query = "Select sum(sisa_settle) from " + Constant.TABLE_SETTLEMENT + " where status='0' order by settle_ke desc";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                angka = c.getString(c.getColumnIndex("sum(*)"));
                //angka = String.valueOf((Integer.parseInt(angka1) + 1));
                return angka;
            } else {
                c.close();
                angka = "0";
                return angka;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return angka;
    }


    public ArrayList<Settlement> getAllCustomValueSettlement() {
        ArrayList<Settlement> listdtl = new ArrayList<>();
        Settlement dtl;
        String query = "select * from " + Constant.TABLE_SETTLEMENT + " where status='0'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                dtl = new Settlement();
                dtl.id_settle = c.getString(c.getColumnIndex("id_settle"));
                dtl.total_settle = c.getString(c.getColumnIndex("total_settle"));
                listdtl.add(dtl);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listdtl;
    }


    public String insertCustomValueSettle(ArrayList<Settlement> list) {
        try {
            deleteCustomValueSettle();
            for (Settlement settle : list) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("total_settle", settle.getTotal_settle());
                contentValues.put("status", settle.getStatus());
                Constant.MKSSdb.insertOrThrow(Constant.TABLE_SETTLEMENT, null, contentValues);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Sukses";
    }

    public String deleteCustomValueSettle() {
        try {
            Constant.MKSSdb.delete(Constant.TABLE_SETTLEMENT, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_SETTLEMENT);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
        return "Sukses";
    }

    //    public String idSettlement;
//    public String namaSettlement;
//    public String norekSettlement;
//    public String nominalSettlement;
//    public String noloan;
//    public String iddetailinventory;
//    public String status;

//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public String getNoloan() {
//        return noloan;
//    }
//
//    public void setNoloan(String noloan) {
//        this.noloan = noloan;
//    }
//
//    public String getIddetailinventory() {
//        return iddetailinventory;
//    }
//
//    public void setIddetailinventory(String iddetailinventory) {
//        this.iddetailinventory = iddetailinventory;
//    }
//
//    public Settlement() {
//
//    }
//
//    public Settlement(String nama, String norek, String nominal) {
//        namaSettlement = nama;
//        norekSettlement = norek;
//        nominalSettlement = nominal;
//    }
//
//    public String getIdSettlement() {
//        return idSettlement;
//    }
//
//    public void setIdSettlement(String idSettlement) {
//        this.idSettlement = idSettlement;
//    }
//
//    public String getNamaSettlement() {
//        return namaSettlement;
//    }
//
//    public void setNamaSettlement(String namaSettlement) {
//        this.namaSettlement = namaSettlement;
//    }
//
//    public String getNorekSettlement() {
//        return norekSettlement;
//    }
//
//    public void setNorekSettlement(String norekSettlement) {
//        this.norekSettlement = norekSettlement;
//    }
//
//    public String getNominalSettlement() {
//        return nominalSettlement;
//    }
//
//    public void setNominalSettlement(String nominalSettlement) {
//        this.nominalSettlement = nominalSettlement;
//    }
//
//    public void deleteAllSettlement() {
//        try {
//            String query = "delete from " + Constant.TABLE_SETTLEMENT;
//            System.out.println("Query delete: " + query);
//            Constant.MKSSdb.execSQL(query);
//            System.out.println("Berhasil delete settlement");
//        } catch (Exception e) {
//            e.printStackTrace();
//            System.out.println("Gagal: " + e.getMessage());
//        }
//    }
//
//    public ArrayList<Settlement> getAllDataSettlement() {
//        ArrayList<Settlement> list = new ArrayList<>();
//        Settlement s;
//
//        String query = "Select * from " + Constant.TABLE_SETTLEMENT + " where status='0'";
//        try {
//            Cursor c = Constant.MKSSdb.rawQuery(query, null);
//            while (c.moveToNext()) {
//                s = new Settlement();
//                s.setNamaSettlement(c.getString(c.getColumnIndex("nama")));
//                s.setStatus(c.getString(c.getColumnIndex("status")));
//                s.setNominalSettlement(c.getString(c.getColumnIndex("nominal_bayar")));
//                s.setNorekSettlement(c.getString(c.getColumnIndex("no_rekening")));
//                s.setNoloan(c.getString(c.getColumnIndex("no_loan")));
//
//                list.add(s);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return list;
//    }
//
//    public String insertSettlement(Settlement settlement) {
//        try {
//            ContentValues contentValues = new ContentValues();
//            contentValues.put("status", settlement.getStatus());
//            contentValues.put("nama", settlement.getNamaSettlement());
//            contentValues.put("nominal_bayar", settlement.getNominalSettlement());
//            contentValues.put("no_rekening", settlement.getNorekSettlement());
//            contentValues.put("no_loan", settlement.getNoloan());
//
//            Constant.MKSSdb.insertOrThrow(Constant.TABLE_SETTLEMENT, null, contentValues);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return "Sukses";
//    }
//
//    public void updateStatusSettlement() {
//        String query = "update " + Constant.TABLE_SETTLEMENT + " set status='1'";
//        try {
//            Constant.MKSSdb.execSQL(query);
//            System.out.println("Berhasil Update Settlement");
//        } catch (Exception e) {
//            e.printStackTrace();
//            System.out.println("Gagal Update Settlement");
//        }
//    }
//
//    public String getTotalNominalSettlement() {
//        String jmlh = "";
//        String query = "Select sum(nominal_bayar) as jmlh from " + Constant.TABLE_SETTLEMENT + " where status='0'";
//        try {
//            Cursor c = Constant.MKSSdb.rawQuery(query, null);
//            if (c.moveToFirst()) {
//                jmlh = c.getString(c.getColumnIndex("jmlh"));
//                return jmlh;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//
//        }
//        return jmlh;
//    }
}
