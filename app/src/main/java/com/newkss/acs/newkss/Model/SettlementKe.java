package com.newkss.acs.newkss.Model;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;

/**
 * Created by acs on 6/15/17.
 */

public class SettlementKe {

    public String idSettlement;
    public String settlementKe;
    public String nominal;
    public String totalKewajiban;
    public String sisaBayar;
    public String status;

    public String getTotalKewajiban() {
        return totalKewajiban;
    }

    public void setTotalKewajiban(String totalKewajiban) {
        this.totalKewajiban = totalKewajiban;
    }

    public String getSisaBayar() {
        return sisaBayar;
    }

    public void setSisaBayar(String sisaBayar) {
        this.sisaBayar = sisaBayar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SettlementKe(String ke, String nominal) {
        settlementKe = ke;
        this.nominal = nominal;
    }

    public SettlementKe() {


    }

    public String getIdSettlement() {
        return idSettlement;
    }

    public void setIdSettlement(String idSettlement) {
        this.idSettlement = idSettlement;
    }

    public String getSettlementKe() {
        return settlementKe;
    }

    public void setSettlementKe(String settlementKe) {
        this.settlementKe = settlementKe;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public void insertSettlementKe() {

    }

    public String getAngkaSettlementKe() {
        String angka = "";
        String angka1 = "";
        String query = "Select * from " + Constant.TABLE_DETIL_SETTLEMENT + " order by settlement_ke desc";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                angka1 = c.getString(c.getColumnIndex("settlement_ke"));
                angka = String.valueOf((Integer.parseInt(angka1) + 1));
                return angka;
            } else {
                angka = "1";
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return angka;
    }

    public String getTotalNominalSettlement() {
        String jmlh = "";
        String query = "Select sum(nominal_bayar) as jmlh from " + Constant.TABLE_SETTLEMENT + " where status='0'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("jmlh"));
                return jmlh;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }


    public String getSisaSettlement() {
        String jmlh = "";
        String query = "Select sum(sisa_bayar) as jmlh from " + Constant.TABLE_DETIL_SETTLEMENT + " where status='0'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("jmlh"));
                return jmlh;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }


    public String insertDataSettlementKe(SettlementKe settlement) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("settlement_ke", settlement.getAngkaSettlementKe());
            contentValues.put("nominal", settlement.getNominal());
            contentValues.put("total_kewajiban", settlement.getTotalKewajiban());
            contentValues.put("sisa_bayar", settlement.getSisaBayar());
            contentValues.put("status", settlement.getStatus());

            Constant.MKSSdb.insertOrThrow(Constant.TABLE_DETIL_SETTLEMENT, null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Sukses";
    }
}
