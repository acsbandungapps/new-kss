package com.newkss.acs.newkss.ModelBaru;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Util.Constant;

import java.util.ArrayList;

/**
 * Created by acs on 6/18/17.
 */

public class DataBucketBooked {
    public String id_data_booked;
    public String id_tgt;
    public String id_penagihan;
    public String no_loan;
    public String tipe_booking;
    public String status;
    public String id_data_pencairan;
    public String id_pencairan;


    public String getId_data_pencairan() {
        return id_data_pencairan;
    }

    public void setId_data_pencairan(String id_data_pencairan) {
        this.id_data_pencairan = id_data_pencairan;
    }

    public String getId_pencairan() {
        return id_pencairan;
    }

    public void setId_pencairan(String id_pencairan) {
        this.id_pencairan = id_pencairan;
    }

    public String getId_data_booked() {
        return id_data_booked;
    }

    public void setId_data_booked(String id_data_booked) {
        this.id_data_booked = id_data_booked;
    }

    public String getId_tgt() {
        return id_tgt;
    }

    public void setId_tgt(String id_tgt) {
        this.id_tgt = id_tgt;
    }

    public String getId_penagihan() {
        return id_penagihan;
    }

    public void setId_penagihan(String id_penagihan) {
        this.id_penagihan = id_penagihan;
    }

    public String getNo_loan() {
        return no_loan;
    }

    public void setNo_loan(String no_loan) {
        this.no_loan = no_loan;
    }

    public String getTipe_booking() {
        return tipe_booking;
    }

    public void setTipe_booking(String tipe_booking) {
        this.tipe_booking = tipe_booking;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void deleteSequence() {
        try {
            Constant.MKSSdb.delete(Constant.TABLE_SEQUENCE, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_SEQUENCE);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }


    public void deleteAllDataBooked() {
        try {
            Constant.MKSSdb.delete(Constant.TABLE_DATA_BOOKED, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_DATA_BOOKED);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }


    public void deleteSelectedDataBookedPenagihan(ArrayList<Detail_Inventory> di) {
        try {
            for(Detail_Inventory d:di){
                String query = "Delete from " + Constant.TABLE_DATA_BOOKED + " where tipe_booking='" + Constant.PENAGIHAN + "' and no_loan='"+d.getNo_loan()+"'";
                System.out.println(query);
                Constant.MKSSdb.execSQL(query);
                System.out.println("Sukses Hapus " + Constant.TABLE_DATA_BOOKED);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }
    public void deleteAllDataBookedPenagihan() {
        try {
            String query = "Delete from " + Constant.TABLE_DATA_BOOKED + " where tipe_booking='" + Constant.PENAGIHAN + "'";
            System.out.println(query);
            Constant.MKSSdb.execSQL(query);
            System.out.println("Sukses Hapus " + Constant.TABLE_DATA_BOOKED);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }

    public void deleteAllDataBookedPencairan() {
        try {
            String query = "Delete from " + Constant.TABLE_DATA_BOOKED + " where tipe_booking='" + Constant.PENCAIRAN + "'";
            System.out.println(query);
            Constant.MKSSdb.execSQL(query);
            System.out.println("Sukses Hapus " + Constant.TABLE_DATA_BOOKED);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }

    public void insertDataBookedIntoSqlitePenagihan(ArrayList<Detail_Inventory> dd) {
        for (Detail_Inventory debu : dd) {
            System.out.println("ID: " + debu.getId_tgt());
            System.out.println("ID: " + debu.getNo_loan());
            System.out.println("ID: " + debu.getId_detail_inventory());

            ContentValues contentValues = new ContentValues();
            contentValues.put("id_tgt", debu.getId_tgt());
            contentValues.put("no_loan", debu.getNo_loan());
            contentValues.put("tipe_booking", Constant.PENAGIHAN_BOOKED);
            contentValues.put("id_detail_inventory", debu.getId_detail_inventory());
            Constant.MKSSdb.insertOrThrow(Constant.TABLE_DATA_BOOKED, null, contentValues);
            System.out.println("Sukses Insert Booked Penagihan " + Constant.TABLE_DATA_BOOKED);
        }
    }



    public void insertDataBookedIntoSqlitePencairan(ArrayList<Detail_Inventory> dd) {
        for (Detail_Inventory debu : dd) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("id_data_pencairan", debu.getId_data_pencairan());
            contentValues.put("no_loan", debu.getNo_loan());
            contentValues.put("tipe_booking", Constant.PENCAIRAN_BOOKED);
            contentValues.put("id_detail_inventory", debu.getId_detail_inventory());
            Constant.MKSSdb.insertOrThrow(Constant.TABLE_DATA_BOOKED, null, contentValues);
            System.out.println("Sukses Insert Booked Pencairan" + Constant.TABLE_DATA_BOOKED);
        }
    }

    public ArrayList<DataBucketBooked> getIdArrayDataBooked() {
        ArrayList<DataBucketBooked> listId = new ArrayList<>();
        DataBucketBooked dbb;
        String query = "select * from " + Constant.TABLE_DATA_BOOKED;
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                dbb = new DataBucketBooked();
                dbb.id_tgt = c.getString(c.getColumnIndex("id_tgt"));
                dbb.no_loan = c.getString(c.getColumnIndex("no_loan"));
                System.out.println(dbb.no_loan);
                listId.add(dbb);
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listId;
    }


    public ArrayList<DataBucketBooked> getIdArrayDataBookedPenagihan() {
        ArrayList<DataBucketBooked> listId = new ArrayList<>();
        DataBucketBooked dbb;
        String query = "select * from " + Constant.TABLE_DATA_BOOKED + " where tipe_booking='" + Constant.PENAGIHAN_BOOKED + "'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                dbb = new DataBucketBooked();

                dbb.id_tgt = c.getString(c.getColumnIndex("id_tgt"));
                dbb.no_loan = c.getString(c.getColumnIndex("no_loan"));
                System.out.println(dbb.no_loan);
                listId.add(dbb);
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listId;
    }


    public ArrayList<DataBucketBooked> getIdArrayDataBookedPencairan() {
        ArrayList<DataBucketBooked> listId = new ArrayList<>();
        DataBucketBooked dbb;
        String query = "select * from " + Constant.TABLE_DATA_BOOKED + " where tipe_booking='" + Constant.PENCAIRAN_BOOKED + "'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                dbb = new DataBucketBooked();
                dbb.id_data_pencairan = c.getString(c.getColumnIndex("id_data_pencairan"));
                dbb.no_loan = c.getString(c.getColumnIndex("no_loan"));
                System.out.println(dbb.no_loan);
                listId.add(dbb);
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listId;
    }


}
