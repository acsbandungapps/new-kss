package com.newkss.acs.newkss.ModelBaru;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;

import java.util.ArrayList;

/**
 * Created by acs on 6/17/17.
 */

public class Data_Pencairan {
    public String id_data_pencairan;
    public String cycle;
    public String jarak;
    public String os;
    public String nama;
    public String alamat;
    public String status;
    public boolean box;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isBox() {
        return box;
    }

    public void setBox(boolean box) {
        this.box = box;
    }

    public String getId_data_pencairan() {
        return id_data_pencairan;
    }

    public void setId_data_pencairan(String id_data_pencairan) {
        this.id_data_pencairan = id_data_pencairan;
    }

    public String getCycle() {
        return cycle;
    }

    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    public String getJarak() {
        return jarak;
    }

    public void setJarak(String jarak) {
        this.jarak = jarak;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }


    public void insertDataPencairan(Data_Pencairan data_pencairan) {
        try {
            //String hasilakhir = "";
//            if (getIdTGTSqlite().getId_tgt() == null) {
//                id = "TGT-000000";
//                System.out.println("Isi kosong");
//            } else {
//                id = getIdTGTSqlite().getId_tgt();
//                System.out.println("Ada Isi: " + id);
//            }

//            String serial = getIdDataPencairanSqlite();
//            System.out.println("HASIL SERIAL DATA PENCAIRAN: " + serial);
//            int angka = Integer.parseInt(serial.substring(4, 10));
//            System.out.println("Angka: " + angka);
//            angka = angka + 1;
//            System.out.println("Angka +1 " + angka);
//            hasilakhir = "IDP-" + String.format("%06d", angka);
//            System.out.println("Hasil Akhir " + hasilakhir);

            ContentValues contentValues = new ContentValues();
            contentValues.put("id_data_pencairan", data_pencairan.getId_data_pencairan());
            contentValues.put(Constant.CYCLE, data_pencairan.getCycle());
            contentValues.put(Constant.JARAK, data_pencairan.getJarak());
            contentValues.put(Constant.DB_OS, data_pencairan.getOs());
            contentValues.put(Constant.NAMA, data_pencairan.getNama());
            contentValues.put(Constant.ALAMAT, data_pencairan.getAlamat());
            contentValues.put(Constant.STATUS, data_pencairan.getStatus());

            Constant.MKSSdb.insertOrThrow(Constant.TABLE_DATA_PENCAIRAN, null, contentValues);
            System.out.println("Insert " + Constant.TABLE_DATA_PENCAIRAN + " Berhasil");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getIdDataPencairanSqlite() {
        String id = "";
        String query = "Select * from " + Constant.TABLE_DATA_PENCAIRAN + " order by id_data_pencairan desc";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_data_pencairan"));
                System.out.println("HASIL ID: " + id);
            } else {
                id = "IDP-000000";
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public String generateIDDataPencairan() {
        String id = "";
        String query = "Select * from " + Constant.TABLE_DATA_PENCAIRAN + " order by id_data_pencairan desc";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                String serial = getIdDataPencairanSqlite();
                System.out.println("HASIL SERIAL DATA PENCAIRAN: " + serial);
                int angka = Integer.parseInt(serial.substring(4, 10));
                System.out.println("Angka: " + angka);
                angka = angka + 1;
                System.out.println("Angka +1 " + angka);
                id = "IDP-" + String.format("%06d", angka);
                System.out.println("Hasil Akhir " + id);
//                id = c.getString(c.getColumnIndex("id_data_pencairan"));
                System.out.println("HASIL ID: " + id);
            } else {
                id = "IDP-000000";
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }


    public void deleteAllDataPencairan() {
        try {
            Constant.MKSSdb.delete(Constant.TABLE_DATA_PENCAIRAN, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_DATA_PENCAIRAN);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }

    public ArrayList<Data_Pencairan> getAllListDataPencairan() {
        ArrayList<Data_Pencairan> list = new ArrayList<>();
        Data_Pencairan t = new Data_Pencairan();
        String query = "Select * from " + Constant.TABLE_DATA_PENCAIRAN;
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                t.id_data_pencairan = c.getString(c.getColumnIndex("id_data_pencairan"));
                t.cycle = c.getString(c.getColumnIndex("cycle"));
                t.jarak = c.getString(c.getColumnIndex("jarak"));
                t.os = c.getString(c.getColumnIndex("os"));
                t.nama = c.getString(c.getColumnIndex("nama"));
                t.alamat = c.getString(c.getColumnIndex("alamat"));
                t.status = c.getString(c.getColumnIndex("status"));
                list.add(t);
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public String getIdDataPencairanFromNoLoan(String noloan) {
        String id_data_pencarian = "";
        String query = "select * from " + Constant.TABLE_DETAIL_INVENTORY + " where no_loan='" + noloan + "'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id_data_pencarian = c.getString(c.getColumnIndex("id_data_pencairan"));
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id_data_pencarian;
    }

    public Data_Pencairan getDataPencairanFromID(String id) {
        Data_Pencairan d = new Data_Pencairan();
        String query = "select * from " + Constant.TABLE_DATA_PENCAIRAN + " where id_data_pencairan='" + id + "'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                d.id_data_pencairan = c.getString(c.getColumnIndex("id_data_pencairan"));
                d.alamat = c.getString(c.getColumnIndex("alamat"));
                d.jarak = c.getString(c.getColumnIndex("jarak"));
                d.cycle = c.getString(c.getColumnIndex("cycle"));
                d.os = c.getString(c.getColumnIndex("os"));
                d.nama = c.getString(c.getColumnIndex("nama"));
                d.status = c.getString(c.getColumnIndex("status"));
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }

    public String getIdDataFromNoLoan(String id) {
        String idDP = "";
        String query = "select * from kss_detail_inventory where no_loan='" + id + "'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                idDP = c.getString(c.getColumnIndex("id_data_pencairan"));
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return idDP;
    }

}
