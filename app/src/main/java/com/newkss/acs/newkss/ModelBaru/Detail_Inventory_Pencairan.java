package com.newkss.acs.newkss.ModelBaru;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Util.Constant;

/**
 * Created by acs on 6/19/17.
 */

public class Detail_Inventory_Pencairan {

    String id_detail_inventory;
    String saldo_min;
    String dpd_today;
    String resume_nsbh;
    String almt_rumah;
    String tgl_janji_bayar_terakhir;
    String denda;
    String pola_bayar;
    String no_hp;
    String tgl_bayar_terakhir;
    String angsuran_ke;
    String nama_upliner;
    String pokok;
    String tgl_sp;
    String tenor;
    String angsuran;
    String gender;
    String tgl_ptp;
    String action_plan;
    String no_rekening;
    String histori_sp;
    String nominal_janji_bayar_terakhir;
    String tlpn_rekomendator;
    String tlpn_upliner;
    String sumber_bayar;
    String bunga;
    String tlpn_econ;
    String jns_pinjaman;
    String nominal_bayar_terakhir;
    String harus_bayar;
    String nominal_ptp;
    String saldo;
    String nama_rekomendator;
    String tlpn_mogen;
    String os_pinjaman;
    String nama_econ;
    String keberadaan_jaminan;
    String email;
    String kewajiban;
    String total_kewajiban;
    String nama_mogen;
    String no_loan;
    String nama_debitur;
    String tgl_jth_tempo;
    String no_tlpn;
    String tgl_gajian;
    String pekerjaan;
    String almt_usaha;
    String id_data_pencairan;

    public String getId_detail_inventory() {
        return id_detail_inventory;
    }

    public void setId_detail_inventory(String id_detail_inventory) {
        this.id_detail_inventory = id_detail_inventory;
    }

    public String getSaldo_min() {
        return saldo_min;
    }

    public void setSaldo_min(String saldo_min) {
        this.saldo_min = saldo_min;
    }

    public String getDpd_today() {
        return dpd_today;
    }

    public void setDpd_today(String dpd_today) {
        this.dpd_today = dpd_today;
    }

    public String getResume_nsbh() {
        return resume_nsbh;
    }

    public void setResume_nsbh(String resume_nsbh) {
        this.resume_nsbh = resume_nsbh;
    }

    public String getAlmt_rumah() {
        return almt_rumah;
    }

    public void setAlmt_rumah(String almt_rumah) {
        this.almt_rumah = almt_rumah;
    }

    public String getTgl_janji_bayar_terakhir() {
        return tgl_janji_bayar_terakhir;
    }

    public void setTgl_janji_bayar_terakhir(String tgl_janji_bayar_terakhir) {
        this.tgl_janji_bayar_terakhir = tgl_janji_bayar_terakhir;
    }

    public String getDenda() {
        return denda;
    }

    public void setDenda(String denda) {
        this.denda = denda;
    }

    public String getPola_bayar() {
        return pola_bayar;
    }

    public void setPola_bayar(String pola_bayar) {
        this.pola_bayar = pola_bayar;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getTgl_bayar_terakhir() {
        return tgl_bayar_terakhir;
    }

    public void setTgl_bayar_terakhir(String tgl_bayar_terakhir) {
        this.tgl_bayar_terakhir = tgl_bayar_terakhir;
    }

    public String getAngsuran_ke() {
        return angsuran_ke;
    }

    public void setAngsuran_ke(String angsuran_ke) {
        this.angsuran_ke = angsuran_ke;
    }

    public String getNama_upliner() {
        return nama_upliner;
    }

    public void setNama_upliner(String nama_upliner) {
        this.nama_upliner = nama_upliner;
    }

    public String getPokok() {
        return pokok;
    }

    public void setPokok(String pokok) {
        this.pokok = pokok;
    }

    public String getTgl_sp() {
        return tgl_sp;
    }

    public void setTgl_sp(String tgl_sp) {
        this.tgl_sp = tgl_sp;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getAngsuran() {
        return angsuran;
    }

    public void setAngsuran(String angsuran) {
        this.angsuran = angsuran;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTgl_ptp() {
        return tgl_ptp;
    }

    public void setTgl_ptp(String tgl_ptp) {
        this.tgl_ptp = tgl_ptp;
    }

    public String getAction_plan() {
        return action_plan;
    }

    public void setAction_plan(String action_plan) {
        this.action_plan = action_plan;
    }

    public String getNo_rekening() {
        return no_rekening;
    }

    public void setNo_rekening(String no_rekening) {
        this.no_rekening = no_rekening;
    }

    public String getHistori_sp() {
        return histori_sp;
    }

    public void setHistori_sp(String histori_sp) {
        this.histori_sp = histori_sp;
    }

    public String getNominal_janji_bayar_terakhir() {
        return nominal_janji_bayar_terakhir;
    }

    public void setNominal_janji_bayar_terakhir(String nominal_janji_bayar_terakhir) {
        this.nominal_janji_bayar_terakhir = nominal_janji_bayar_terakhir;
    }

    public String getTlpn_rekomendator() {
        return tlpn_rekomendator;
    }

    public void setTlpn_rekomendator(String tlpn_rekomendator) {
        this.tlpn_rekomendator = tlpn_rekomendator;
    }

    public String getTlpn_upliner() {
        return tlpn_upliner;
    }

    public void setTlpn_upliner(String tlpn_upliner) {
        this.tlpn_upliner = tlpn_upliner;
    }

    public String getSumber_bayar() {
        return sumber_bayar;
    }

    public void setSumber_bayar(String sumber_bayar) {
        this.sumber_bayar = sumber_bayar;
    }

    public String getBunga() {
        return bunga;
    }

    public void setBunga(String bunga) {
        this.bunga = bunga;
    }

    public String getTlpn_econ() {
        return tlpn_econ;
    }

    public void setTlpn_econ(String tlpn_econ) {
        this.tlpn_econ = tlpn_econ;
    }

    public String getJns_pinjaman() {
        return jns_pinjaman;
    }

    public void setJns_pinjaman(String jns_pinjaman) {
        this.jns_pinjaman = jns_pinjaman;
    }

    public String getNominal_bayar_terakhir() {
        return nominal_bayar_terakhir;
    }

    public void setNominal_bayar_terakhir(String nominal_bayar_terakhir) {
        this.nominal_bayar_terakhir = nominal_bayar_terakhir;
    }

    public String getHarus_bayar() {
        return harus_bayar;
    }

    public void setHarus_bayar(String harus_bayar) {
        this.harus_bayar = harus_bayar;
    }

    public String getNominal_ptp() {
        return nominal_ptp;
    }

    public void setNominal_ptp(String nominal_ptp) {
        this.nominal_ptp = nominal_ptp;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getNama_rekomendator() {
        return nama_rekomendator;
    }

    public void setNama_rekomendator(String nama_rekomendator) {
        this.nama_rekomendator = nama_rekomendator;
    }

    public String getTlpn_mogen() {
        return tlpn_mogen;
    }

    public void setTlpn_mogen(String tlpn_mogen) {
        this.tlpn_mogen = tlpn_mogen;
    }

    public String getOs_pinjaman() {
        return os_pinjaman;
    }

    public void setOs_pinjaman(String os_pinjaman) {
        this.os_pinjaman = os_pinjaman;
    }

    public String getNama_econ() {
        return nama_econ;
    }

    public void setNama_econ(String nama_econ) {
        this.nama_econ = nama_econ;
    }

    public String getKeberadaan_jaminan() {
        return keberadaan_jaminan;
    }

    public void setKeberadaan_jaminan(String keberadaan_jaminan) {
        this.keberadaan_jaminan = keberadaan_jaminan;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKewajiban() {
        return kewajiban;
    }

    public void setKewajiban(String kewajiban) {
        this.kewajiban = kewajiban;
    }

    public String getTotal_kewajiban() {
        return total_kewajiban;
    }

    public void setTotal_kewajiban(String total_kewajiban) {
        this.total_kewajiban = total_kewajiban;
    }

    public String getNama_mogen() {
        return nama_mogen;
    }

    public void setNama_mogen(String nama_mogen) {
        this.nama_mogen = nama_mogen;
    }

    public String getNo_loan() {
        return no_loan;
    }

    public void setNo_loan(String no_loan) {
        this.no_loan = no_loan;
    }

    public String getNama_debitur() {
        return nama_debitur;
    }

    public void setNama_debitur(String nama_debitur) {
        this.nama_debitur = nama_debitur;
    }

    public String getTgl_jth_tempo() {
        return tgl_jth_tempo;
    }

    public void setTgl_jth_tempo(String tgl_jth_tempo) {
        this.tgl_jth_tempo = tgl_jth_tempo;
    }

    public String getNo_tlpn() {
        return no_tlpn;
    }

    public void setNo_tlpn(String no_tlpn) {
        this.no_tlpn = no_tlpn;
    }

    public String getTgl_gajian() {
        return tgl_gajian;
    }

    public void setTgl_gajian(String tgl_gajian) {
        this.tgl_gajian = tgl_gajian;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getAlmt_usaha() {
        return almt_usaha;
    }

    public void setAlmt_usaha(String almt_usaha) {
        this.almt_usaha = almt_usaha;
    }

    public String getId_data_pencairan() {
        return id_data_pencairan;
    }

    public void setId_data_pencairan(String id_data_pencairan) {
        this.id_data_pencairan = id_data_pencairan;
    }


    public Detail_Inventory_Pencairan getIdValueDetailInventoryPencairan() {
        Detail_Inventory_Pencairan dtl = new Detail_Inventory_Pencairan();
        String query = "Select * from " + Constant.TABLE_DETAIL_INVENTORY_PENCAIRAN + " order by id_detail_inventory desc";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                dtl.id_detail_inventory = c.getString(c.getColumnIndex("id_detail_inventory"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dtl;
    }


    public String generateNoLoanSqlite() {
        String id = "";
        String query = "Select * from " + Constant.TABLE_DETAIL_INVENTORY_PENCAIRAN + " order by no_loan desc";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                String serial = c.getString(c.getColumnIndex("no_loan"));
                System.out.println("Nilai Serial: " + serial);
                int angka = Integer.parseInt(serial.substring(3, 10));
                System.out.println("Angka: " + angka);
                angka = angka + 1;
                System.out.println("Angka +1 " + angka);
                id = "LD-" + String.format("%07d", angka);
                System.out.println("Hasil Akhir " + id);
            } else {
                id = "LD-0000000";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public void insertDetailInventoryPencairan(Detail_Inventory dd) {

        try {
            for (int i = 0; i < 1; i++) {
                String id = "";
                String hasilakhir = "";
                if (getIdValueDetailInventoryPencairan().getId_detail_inventory() == null) {
                    id = "DI-0000000";
                    System.out.println("Isi kosong");
                } else {
                    id = getIdValueDetailInventoryPencairan().getId_detail_inventory();
                    System.out.println("Ada isi");
                    System.out.println(getIdValueDetailInventoryPencairan().getId_detail_inventory());
                }
                String serial = id;
                int angka = Integer.parseInt(serial.substring(3, 10));
                System.out.println("Angka: " + angka);
                angka = angka + 1;
                System.out.println("Angka +1 " + angka);
                hasilakhir = "DI-" + String.format("%07d", angka);
                System.out.println("Hasil Akhir " + hasilakhir);

                ContentValues contentValues = new ContentValues();
                contentValues.put(Constant.ID_DETAIL_INVENTORY, hasilakhir);
                contentValues.put(Constant.SALDO_MIN, dd.getSaldo_min());
                contentValues.put(Constant.DPD_TODAY, dd.getDpd_today());
                contentValues.put(Constant.RESUME_NSBH, dd.getResume_nsbh());
                contentValues.put(Constant.ALMT_RUMAH, dd.getAlmt_rumah());
                contentValues.put(Constant.TGL_JANJI_BAYAR_TERAKHIR, dd.getTgl_janji_bayar_terakhir());
                contentValues.put(Constant.DENDA, dd.getDenda());
                contentValues.put(Constant.POLA_BAYAR, dd.getPola_bayar());
                contentValues.put(Constant.NO_HP, dd.getNo_hp());
                contentValues.put(Constant.TGL_BAYAR_TERAKHIR, dd.getTgl_bayar_terakhir());
                contentValues.put(Constant.ANGSURAN_KE, dd.getAngsuran_ke());
                contentValues.put(Constant.NAMA_UPLINER, dd.getNama_upliner());
                contentValues.put(Constant.POKOK, dd.getPokok());
                contentValues.put(Constant.TGL_SP, dd.getTgl_sp());
                contentValues.put(Constant.TENOR, dd.getTenor());
                contentValues.put(Constant.ANGSURAN, dd.getAngsuran());

                contentValues.put(Constant.GENDER, dd.getGender());
                contentValues.put(Constant.TGL_PTP, dd.getTgl_ptp());
                contentValues.put(Constant.ACTION_PLAN, dd.getAction_plan());
                contentValues.put(Constant.NO_REKENING, dd.getNo_rekening());
                contentValues.put(Constant.HISTORI_SP, dd.getHistori_sp());
                contentValues.put(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR, dd.getNominal_janji_bayar_terakhir());
                contentValues.put(Constant.TLPN_REKOMENDATOR, dd.getTlpn_rekomendator());
                contentValues.put(Constant.TLPN_UPLINER, dd.getTlpn_upliner());
                contentValues.put(Constant.SUMBER_BAYAR, dd.getSumber_bayar());

                contentValues.put(Constant.BUNGA, dd.getBunga());
                contentValues.put(Constant.TLPN_ECON, dd.getTlpn_econ());
                contentValues.put(Constant.JNS_PINJAMAN, dd.getJns_pinjaman());
                contentValues.put(Constant.NOMINAL_BAYAR_TERAKHIR, dd.getNominal_bayar_terakhir());
                contentValues.put(Constant.HARUS_BAYAR, dd.getHarus_bayar());
                contentValues.put(Constant.NOMINAL_PTP, dd.getNominal_ptp());
                contentValues.put(Constant.SALDO, dd.getSaldo());
                contentValues.put(Constant.NAMA_REKOMENDATOR, dd.getNama_rekomendator());
                contentValues.put(Constant.TLPN_MOGEN, dd.getTlpn_mogen());
                contentValues.put(Constant.OS_PINJAMAN, dd.getOs_pinjaman());
                contentValues.put(Constant.NAMA_ECON, dd.getNama_econ());
                contentValues.put(Constant.KEBERADAAN_JAMINAN, dd.getKeberadaan_jaminan());
                contentValues.put(Constant.EMAIL, dd.getEmail());
                contentValues.put(Constant.KEWAJIBAN, dd.getKewajiban());
                contentValues.put(Constant.TOTAL_KEWAJIBAN, dd.getTotal_kewajiban());
                contentValues.put(Constant.NAMA_MOGEN, dd.getNama_mogen());
                //contentValues.put(Constant.NO_LOAN, dd.getNo_loan());
                contentValues.put(Constant.NO_LOAN, dd.generateNoLoanSqlite());

                contentValues.put(Constant.NAMA_DEBITUR, dd.getNama_debitur());
                contentValues.put(Constant.TGL_JTH_TEMPO, dd.getTgl_jth_tempo());
                contentValues.put(Constant.NO_TLPN, dd.getNo_tlpn());
                contentValues.put(Constant.TGL_GAJIAN, dd.getTgl_gajian());
                contentValues.put(Constant.PEKERJAAN, dd.getPekerjaan());
                contentValues.put(Constant.ALMT_USAHA, dd.getAlmt_usaha());
                contentValues.put("id_data_pencairan", dd.getId_data_pencairan());

                Constant.MKSSdb.insertOrThrow(Constant.TABLE_DETAIL_INVENTORY_PENCAIRAN, null, contentValues);
                System.out.println("Sukses Insert: " + Constant.TABLE_DETAIL_INVENTORY_PENCAIRAN);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
