package com.newkss.acs.newkss.ModelBaru;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;

import java.util.ArrayList;

/**
 * Created by acs on 6/17/17.
 */

public class Detil_Data_Pencairan {
    public String id_detil_data_pencairan;
    public String id_data_pencairan;
    public String no_loan;
    public String tgl_kunjungan;
    public String status;

    public String getId_detil_data_pencairan() {
        return id_detil_data_pencairan;
    }

    public void setId_detil_data_pencairan(String id_detil_data_pencairan) {
        this.id_detil_data_pencairan = id_detil_data_pencairan;
    }

    public String getId_data_pencairan() {
        return id_data_pencairan;
    }

    public void setId_data_pencairan(String id_data_pencairan) {
        this.id_data_pencairan = id_data_pencairan;
    }

    public String getNo_loan() {
        return no_loan;
    }

    public void setNo_loan(String no_loan) {
        this.no_loan = no_loan;
    }

    public String getTgl_kunjungan() {
        return tgl_kunjungan;
    }

    public void setTgl_kunjungan(String tgl_kunjungan) {
        this.tgl_kunjungan = tgl_kunjungan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public void insertDetilDataPencairan(Detil_Data_Pencairan dtgt) {

        try {
            String id = "";
            String hasilakhir = "";
//            if (generateNoLoanSqlite() == null) {
//                id = "LD-0000000";
//                System.out.println("Isi kosong");
//            } else {
//                id = generateNoLoanSqlite();
//                System.out.println("Ada Isi: " + id);
//            }
            String serial = generateNoLoanSqlite();
            System.out.println("Nilai Serial: " + serial);
            int angka = Integer.parseInt(serial.substring(3, 10));
            System.out.println("Angka: " + angka);
            angka = angka + 1;
            System.out.println("Angka +1 " + angka);
            hasilakhir = "LD-" + String.format("%07d", angka);
            System.out.println("Hasil Akhir " + hasilakhir);

            ContentValues contentValues = new ContentValues();
            contentValues.put("id_data_pencairan", dtgt.id_data_pencairan);
            contentValues.put(Constant.NO_LOAN, dtgt.no_loan);
            contentValues.put(Constant.TGL_KUNJUNGAN, dtgt.tgl_kunjungan);
            contentValues.put(Constant.STATUS, dtgt.status);

            Constant.MKSSdb.insertOrThrow(Constant.TABLE_DETIL_DATA_PENCAIRAN, null, contentValues);
            System.out.println("Insert " + Constant.TABLE_DETIL_DATA_PENCAIRAN + " Berhasil");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String generateNoLoanSqlite() {
        String id = "";
        String query = "Select * from " + Constant.TABLE_DETAIL_INVENTORY + " order by no_loan desc";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("no_loan"));
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }


    public String getCountSS() {
        String jmlh = "0";
        String query = "Select count(*) from " + Constant.TABLE_DETIL_DATA_PENCAIRAN + " where id_data_pencairan='" + getSSCode() + "'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("count(*)"));
                return jmlh;
            } else {
                jmlh = "0";
                return jmlh;
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }

    public String getCountSSStatusOne() {
        String jmlh = "0";
        String query = "Select count(*) from " + Constant.TABLE_DETIL_DATA_PENCAIRAN + " where status='1'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("count(*)"));
                return jmlh;
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();

        }

        return jmlh;
    }


    public String getCountBI() {
        String jmlh = "0";
        String query = "Select count(*) from " + Constant.TABLE_DETIL_DATA_PENCAIRAN + " where id_data_pencairan='" + getBICode() + "'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("count(*)"));
                return jmlh;
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }

    public String getCountBS() {
        String jmlh = "";
        String query = "Select count(*) from " + Constant.TABLE_DETIL_DATA_PENCAIRAN + " where id_data_pencairan='" + getBSCode() + "'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("count(*)"));
                return jmlh;
            } else {
                jmlh = "0";
                return jmlh;
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }

    public String getCountBLBS() {
        String jmlh = "0";
        String query = "Select count(*) from " + Constant.TABLE_DETIL_DATA_PENCAIRAN + " where id_data_pencairan='" + getBLBSCode() + "'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("count(*)"));
                return jmlh;
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }

    public String getSSCode() {
        String id = "";
        String query = "select id_pencairan from " + Constant.TABLE_PENCAIRAN + " where nama_pencairan='ss'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_pencairan"));
                return id;
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public String getBICode() {
        String id = "";
        String query = "select id_pencairan from " + Constant.TABLE_PENCAIRAN + " where nama_pencairan='bi'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_pencairan"));
                return id;
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public String getBSCode() {
        String id = "";
        String query = "select id_pencairan from " + Constant.TABLE_PENCAIRAN + " where nama_pencairan='bs'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_pencairan"));
                return id;
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public String getBLBSCode() {
        String id = "";
        String query = "select id_pencairan from " + Constant.TABLE_PENCAIRAN + " where nama_pencairan='blbs'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_pencairan"));
                return id;
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public ArrayList<Detil_Data_Pencairan> getArrayListDetilDataPencairan() {
        ArrayList<Detil_Data_Pencairan> listdp = new ArrayList<>();
        Detil_Data_Pencairan ddp;
        String query = "Select * from " + Constant.TABLE_DETIL_DATA_PENCAIRAN + " where status='1'";
        System.out.println("Query getArrayListDetilDataPencairan " + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                ddp = new Detil_Data_Pencairan();
                ddp.status = c.getString(c.getColumnIndex("status"));
                ddp.id_data_pencairan = c.getString(c.getColumnIndex("id_data_pencairan"));
                ddp.no_loan = c.getString(c.getColumnIndex("no_loan"));
                ddp.tgl_kunjungan = c.getString(c.getColumnIndex("tgl_kunjungan"));
                listdp.add(ddp);
            }
            c.close();
            return listdp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listdp;
    }
}
