package com.newkss.acs.newkss.ModelBaru;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import java.util.ArrayList;

/**
 * Created by acs on 6/17/17.
 */

public class Detil_TGT {
    public String id_detil_tgt;
    public String id_penagihan;
    public String no_loan;
    public String tgl_kunjungan;
    public String status;

    public String getId_detil_tgt() {
        return id_detil_tgt;
    }

    public void setId_detil_tgt(String id_detil_tgt) {
        this.id_detil_tgt = id_detil_tgt;
    }

    public String getId_penagihan() {
        return id_penagihan;
    }

    public void setId_penagihan(String id_penagihan) {
        this.id_penagihan = id_penagihan;
    }

    public String getNo_loan() {
        return no_loan;
    }

    public void setNo_loan(String no_loan) {
        this.no_loan = no_loan;
    }

    public String getTgl_kunjungan() {
        return tgl_kunjungan;
    }

    public void setTgl_kunjungan(String tgl_kunjungan) {
        this.tgl_kunjungan = tgl_kunjungan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void insertDetilTGT(Detil_TGT dtgt) {

        try {

            String id = "";
            String hasilakhir = "";
//            if (generateNoLoanSqlite() == null) {
//                id = "LD-0000000";
//                System.out.println("Isi kosong");
//            } else {
//                id = generateNoLoanSqlite();
//                System.out.println("Ada Isi: " + id);
//            }


//            String serial = generateNoLoanSqlite();
//            System.out.println("Nilai Serial: " + serial);
//            int angka = Integer.parseInt(serial.substring(3, 10));
//            System.out.println("Angka: " + angka);
//            angka = angka + 1;
//            System.out.println("Angka +1 " + angka);
//            hasilakhir = "LD-" + String.format("%07d", angka);
//            System.out.println("Hasil Akhir " + hasilakhir);

            ContentValues contentValues = new ContentValues();
            contentValues.put(Constant.ID_PENAGIHAN, dtgt.id_penagihan);
            contentValues.put(Constant.NO_LOAN, dtgt.no_loan);
            contentValues.put(Constant.TGL_KUNJUNGAN, dtgt.tgl_kunjungan);
            contentValues.put(Constant.STATUS, dtgt.status);

            Constant.MKSSdb.insertOrThrow(Constant.TABLE_DETIL_TGT, null, contentValues);
            System.out.println("Insert " + Constant.TABLE_DETIL_TGT + " Berhasil");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String generateNoLoanSqlite() {
        String id = "";
        String query = "Select * from " + Constant.TABLE_DETAIL_INVENTORY + " order by no_loan desc";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("no_loan"));
            }
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }


    public void deleteAllDetilTGT() {
        try {
            Constant.MKSSdb.delete(Constant.TABLE_DETIL_TGT, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_DETIL_TGT);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }

    public String getCountnvst() {
        String jmlh = "0";
        String query = "Select count(*) from " + Constant.TABLE_DETIL_TGT + " where id_penagihan='" + getNvstCode() + "'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("count(*)"));
                return jmlh;
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }


    public String getCountppay() {
        String jmlh = "0";
        String query = "Select count(*) from " + Constant.TABLE_DETIL_TGT + " where id_penagihan='" + getPpayCode() + "'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("count(*)"));
                return jmlh;
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }


    public String getCountfpay() {
        String jmlh = "0";
        String query = "Select count(*) from " + Constant.TABLE_DETIL_TGT + " where id_penagihan='" + getFpayCode() + "'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("count(*)"));
                return jmlh;
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }


    public String getCountnpay() {
        String jmlh = "0";
        String query = "Select count(*) from " + Constant.TABLE_DETIL_TGT + " where id_penagihan='" + getNpayCode() + "'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("count(*)"));
                return jmlh;
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }


    public String getCountTgt() {
        String jmlh = "0";
        String query = "Select count(*) from " + Constant.TABLE_TGT;
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                jmlh = c.getString(c.getColumnIndex("count(*)"));
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return jmlh;
    }

//    public String getTgtCode() {
//        String id = "";
//        String query = "select id_bucket from kss_bucket where nama_bucket='tgt'";
//        try {
//            Cursor c = Constant.MKSSdb.rawQuery(query, null);
//            if (c.moveToFirst()) {
//                id = c.getString(c.getColumnIndex("id_bucket"));
//                return id;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return id;
//    }

    public String getNpayCode() {
        String id = "";
        String query = "select id_penagihan from " + Constant.TABLE_PENAGIHAN + " where nama_penagihan='npay'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_penagihan"));
                return id;
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public String getFpayCode() {
        String id = "";
        String query = "select id_penagihan from " + Constant.TABLE_PENAGIHAN + " where nama_penagihan='fpay'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_penagihan"));
                return id;
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public String getNvstCode() {
        String id = "";
        String query = "select id_penagihan from " + Constant.TABLE_PENAGIHAN + " where nama_penagihan='nvst'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_penagihan"));
                return id;
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public String getPpayCode() {
        String id = "";
        String query = "select id_penagihan from " + Constant.TABLE_PENAGIHAN + " where nama_penagihan='ppay'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id = c.getString(c.getColumnIndex("id_penagihan"));
                return id;
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public ArrayList<Detil_TGT> getAllBucket(String title) {
        ArrayList<Detil_TGT> list = new ArrayList<>();
        Detil_TGT dtgt;
        String query="";
        if(title.equals("fpay")){
            query = "select * from '" + Constant.TABLE_DETIL_TGT + "' where id_penagihan='" + getFpayCode() + "' and status='0'";
            System.out.println("getAllFpay " + query);
        }
        else if(title.equals("npay")){
            query = "select * from '" + Constant.TABLE_DETIL_TGT + "' where id_penagihan='" + getNpayCode() + "' and status='0'";
            System.out.println("getAllnpay " + query);
        }
        else if(title.equals("ppay")){
            query = "select * from '" + Constant.TABLE_DETIL_TGT + "' where id_penagihan='" + getPpayCode() + "' and status='0'";
            System.out.println("getAllPpay " + query);
        }
        else if(title.equals("nvst")){
            query = "select * from '" + Constant.TABLE_DETIL_TGT + "' where id_penagihan='" + getNvstCode() + "' and status='0'";
            System.out.println("getAllNVST " + query);
        }

        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
                dtgt = new Detil_TGT();
                dtgt.id_detil_tgt = c.getString(c.getColumnIndex("id_detil_tgt"));
                dtgt.id_penagihan = c.getString(c.getColumnIndex("id_penagihan"));
                dtgt.no_loan = c.getString(c.getColumnIndex("no_loan"));
                dtgt.status = c.getString(c.getColumnIndex("status"));
                list.add(dtgt);
            }
            c.close();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }



}
