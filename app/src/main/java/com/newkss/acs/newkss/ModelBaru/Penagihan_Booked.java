package com.newkss.acs.newkss.ModelBaru;

/**
 * Created by acs on 6/17/17.
 */

public class Penagihan_Booked {
    public String id_penagihan;
    public String id_tgt;
    public String status;

    public String getId_penagihan() {
        return id_penagihan;
    }

    public void setId_penagihan(String id_penagihan) {
        this.id_penagihan = id_penagihan;
    }

    public String getId_tgt() {
        return id_tgt;
    }

    public void setId_tgt(String id_tgt) {
        this.id_tgt = id_tgt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
