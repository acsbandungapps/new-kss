package com.newkss.acs.newkss.ModelBaru;

import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

/**
 * Created by acs on 9/20/17.
 */

public class Setoran {

    public String id_setoran;
    public String total_setoran;
    public String status;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public final static String[] TABLE_HISTORY = {
            "id_setoran",
            "total_setoran",
            "status"
    };

    public boolean synchronizeSetor(String column) {
        String sql = "SELECT " + column + " FROM " + Constant.TABLE_SETOR;
        try {
            MySQLiteHelper.sqLiteDB.rawQuery(sql, null);
            return true;

        } catch (Exception e) {
            System.out.println("ALTER TABLE, ADD NEW COLUMN: " + column);
            sql = "ALTER TABLE history ADD COLUMN " + column + " TEXT";
            try {
                MySQLiteHelper.sqLiteDB.rawQuery(sql, null);
                return true;
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        }
    }

    public String getId_setoran() {
        return id_setoran;
    }

    public void setId_setoran(String id_setoran) {
        this.id_setoran = id_setoran;
    }

    public String getTotal_setoran() {
        return total_setoran;
    }

    public void setTotal_setoran(String total_setoran) {
        this.total_setoran = total_setoran;
    }

    public String insertTableMasterSettlement() {
        return "";
    }
}
