package com.newkss.acs.newkss.ModelBaru;

import android.content.ContentValues;
import android.database.Cursor;

import com.newkss.acs.newkss.Util.Constant;

import java.util.ArrayList;

/**
 * Created by acs on 6/17/17.
 */

public class TGT {

    public String id_tgt;
    public String cycle;
    public String jarak;
    public String os;
    public String nama;
    public String alamat;
    public boolean box;
    public String status;

    public boolean isBox() {
        return box;
    }

    public void setBox(boolean box) {
        this.box = box;
    }

    public String getId_tgt() {
        return id_tgt;
    }

    public void setId_tgt(String id_tgt) {
        this.id_tgt = id_tgt;
    }

    public String getCycle() {
        return cycle;
    }

    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    public String getJarak() {
        return jarak;
    }

    public void setJarak(String jarak) {
        this.jarak = jarak;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void insertIdTGT(TGT tgt) {
        try {
            String id = "";
            String hasilakhir = "";
//            if (getIdTGTSqlite().getId_tgt() == null) {
//                id = "TGT-000000";
//                System.out.println("Isi kosong");
//            } else {
//                id = getIdTGTSqlite().getId_tgt();
//                System.out.println("Ada Isi: " + id);
//            }


//            String serial = getIdTGTSqlite().getId_tgt();
//            System.out.println("HASIL SERIAL DATA TGT: " + serial);
//            int angka = Integer.parseInt(serial.substring(4, 10));
//            System.out.println("Angka: " + angka);
//            angka = angka + 1;
//            System.out.println("Angka +1 " + angka);
//            hasilakhir = "TGT-" + String.format("%06d", angka);
//            System.out.println("Hasil Akhir " + hasilakhir);

            ContentValues contentValues = new ContentValues();
            contentValues.put(Constant.TABLE_TGT_ID_TGT, tgt.getId_tgt());
            contentValues.put(Constant.CYCLE, tgt.getCycle());
            contentValues.put(Constant.JARAK, tgt.getJarak());
            contentValues.put(Constant.DB_OS, tgt.getOs());
            contentValues.put(Constant.NAMA, tgt.getNama());
            contentValues.put(Constant.ALAMAT, tgt.getAlamat());
            contentValues.put(Constant.STATUS, tgt.getStatus());

            Constant.MKSSdb.insertOrThrow(Constant.TABLE_TGT, null, contentValues);
            System.out.println("Insert " + Constant.TABLE_TGT + " Berhasil");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertIdTGTForceSync(TGT tgt) {
        try {
            String id = generateIDTGT();
            String hasilakhir = "";
//            if (getIdTGTSqlite().getId_tgt() == null) {
//                id = "TGT-000000";
//                System.out.println("Isi kosong");
//            } else {
//                id = getIdTGTSqlite().getId_tgt();
//                System.out.println("Ada Isi: " + id);
//            }


//            String serial = getIdTGTSqlite().getId_tgt();
//            System.out.println("HASIL SERIAL DATA TGT: " + serial);
//            int angka = Integer.parseInt(serial.substring(4, 10));
//            System.out.println("Angka: " + angka);
//            angka = angka + 1;
//            System.out.println("Angka +1 " + angka);
//            hasilakhir = "TGT-" + String.format("%06d", angka);
//            System.out.println("Hasil Akhir " + hasilakhir);

            ContentValues contentValues = new ContentValues();
            contentValues.put(Constant.TABLE_TGT_ID_TGT, id);
            contentValues.put(Constant.CYCLE, tgt.getCycle());
            contentValues.put(Constant.JARAK, tgt.getJarak());
            contentValues.put(Constant.DB_OS, tgt.getOs());
            contentValues.put(Constant.NAMA, tgt.getNama());
            contentValues.put(Constant.ALAMAT, tgt.getAlamat());

            Constant.MKSSdb.insertOrThrow(Constant.TABLE_TGT, null, contentValues);
            System.out.println("Insert " + Constant.TABLE_TGT + " Berhasil");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public TGT getIdTGTSqlite() {
        TGT t = new TGT();
        String query = "Select * from " + Constant.TABLE_TGT + " order by id_tgt desc";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                t.id_tgt = c.getString(c.getColumnIndex("id_tgt"));
            } else {
                t.id_tgt = "TGT-000000";
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }

    public String generateIDTGT() {
        String id = "";
        TGT t = new TGT();
        String query = "Select * from " + Constant.TABLE_TGT + " order by id_tgt desc";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                String serial = getIdTGTSqlite().getId_tgt();
                System.out.println("HASIL SERIAL DATA TGT: " + serial);
                int angka = Integer.parseInt(serial.substring(4, 10));
                System.out.println("Angka: " + angka);
                angka = angka + 1;
                System.out.println("Angka +1 " + angka);
                id = "TGT-" + String.format("%06d", angka);
                System.out.println("Hasil Akhir " + id);
//                id = c.getString(c.getColumnIndex("id_tgt"));
            } else {
                id = "TGT-000000";
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public void deleteAllTGT() {
        try {
            Constant.MKSSdb.delete(Constant.TABLE_TGT, null, null);
            System.out.println("Sukses Hapus " + Constant.TABLE_TGT);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
        }
    }

    public String deleteTGTByID(String id){
        String hasil="gagal";
        try {
            String query = "delete from " + Constant.TABLE_TGT + " where id_tgt='" + id + "'";
            System.out.println("Query deleteTGTByID: " + query);
            Constant.MKSSdb.execSQL(query);
            System.out.println("Berhasil");
            hasil="Berhasil";
            return hasil;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Gagal: " + e.getMessage());
            hasil="Gagal";
            return hasil;
        }
    }

    public TGT getAllDataTGT() {
        TGT t = new TGT();
        String query = "Select * from " + Constant.TABLE_TGT;
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                t.id_tgt = c.getString(c.getColumnIndex("id_tgt"));
                t.cycle = c.getString(c.getColumnIndex("cycle"));
                t.jarak = c.getString(c.getColumnIndex("jarak"));
                t.os = c.getString(c.getColumnIndex("os"));
                t.nama = c.getString(c.getColumnIndex("nama"));
                t.alamat = c.getString(c.getColumnIndex("alamat"));
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return t;
    }

    public TGT getAllTGTFromIdTGT(String id) {
        TGT t = new TGT();
        String query = "Select * from " + Constant.TABLE_TGT + " where id_tgt='" + id + "'";
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                t.id_tgt = c.getString(c.getColumnIndex("id_tgt"));
                t.cycle = c.getString(c.getColumnIndex("cycle"));
                t.jarak = c.getString(c.getColumnIndex("jarak"));
                t.os = c.getString(c.getColumnIndex("os"));
                t.nama = c.getString(c.getColumnIndex("nama"));
                t.alamat = c.getString(c.getColumnIndex("alamat"));
                t.status = c.getString(c.getColumnIndex("status"));
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return t;
    }

    public ArrayList<TGT> getAllListDataTGT() {
        ArrayList<TGT> list = new ArrayList<>();
        TGT t ;
        String query = "Select * from " + Constant.TABLE_TGT;
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            while (c.moveToNext()) {
               t=new TGT();
                t.id_tgt = c.getString(c.getColumnIndex("id_tgt"));
                t.cycle = c.getString(c.getColumnIndex("cycle"));
                t.jarak = c.getString(c.getColumnIndex("jarak"));
                t.os = c.getString(c.getColumnIndex("os"));
                t.nama = c.getString(c.getColumnIndex("nama"));
                t.alamat = c.getString(c.getColumnIndex("alamat"));
                list.add(t);
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }



    public ArrayList<TGT> getArrayListTGTfromArrayList(ArrayList<DataBucketBooked> listdbb) {
        TGT db;
        //Detail_Bucket_Booked dbb;
        ArrayList<TGT> listdb = new ArrayList<>();
        for (DataBucketBooked dbb : listdbb) {
            String query = "select * from " + Constant.TABLE_TGT + " where id_tgt='" + dbb.id_tgt + "'";
            System.out.println("Query " + query);
            System.out.println("getArrayListTGTfromArrayList: " + dbb.getId_tgt());
            try {
                Cursor c = Constant.MKSSdb.rawQuery(query, null);
                while (c.moveToNext()) {
                    db = new TGT();
                    db.id_tgt = c.getString(c.getColumnIndex("id_tgt"));
                    db.cycle = c.getString(c.getColumnIndex("cycle"));
                    db.jarak = c.getString(c.getColumnIndex("jarak"));
                    db.os = c.getString(c.getColumnIndex("os"));
                    db.nama = c.getString(c.getColumnIndex("nama"));
                    db.alamat = c.getString(c.getColumnIndex("alamat"));

                    listdb.add(db);
                }
                c.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return listdb;
    }


    public String getIdTGTFromNoLoan(String noloan,String iddetinv) {
        String id_tgt = "";
        String query = "select * from " + Constant.TABLE_DETAIL_INVENTORY + " where no_loan='" + noloan + "' and id_detail_inventory='"+iddetinv+"'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                id_tgt = c.getString(c.getColumnIndex("id_tgt"));
                System.out.println("nah: " + id_tgt);
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id_tgt;
    }


    public TGT getDataPenagihanFromID(String id) {
        TGT d = new TGT();
        String query = "select * from " + Constant.TABLE_TGT + " where id_tgt='" + id + "'";
        System.out.println("getDataPenagihanFromID " + query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                d.id_tgt = c.getString(c.getColumnIndex("id_tgt"));
                d.alamat = c.getString(c.getColumnIndex("alamat"));
                d.jarak = c.getString(c.getColumnIndex("jarak"));
                d.cycle = c.getString(c.getColumnIndex("cycle"));
                d.os = c.getString(c.getColumnIndex("os"));
                d.nama = c.getString(c.getColumnIndex("nama"));
                d.status = c.getString(c.getColumnIndex("status"));
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }


    public TGT getTGTFromID(String id) {
        TGT d = new TGT();
        String query = "select * from " + Constant.TABLE_TGT + " where id_tgt='" + id + "'";
        System.out.println(query);
        try {
            Cursor c = Constant.MKSSdb.rawQuery(query, null);
            if (c.moveToFirst()) {
                d.id_tgt = c.getString(c.getColumnIndex("id_tgt"));
                d.alamat = c.getString(c.getColumnIndex("alamat"));
                d.jarak = c.getString(c.getColumnIndex("jarak"));
                d.cycle = c.getString(c.getColumnIndex("cycle"));
                d.os = c.getString(c.getColumnIndex("os"));
                d.nama = c.getString(c.getColumnIndex("nama"));
                d.status = c.getString(c.getColumnIndex("status"));
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }



}
