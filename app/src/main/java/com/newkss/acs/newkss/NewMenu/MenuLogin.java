package com.newkss.acs.newkss.NewMenu;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.WindowManager;

import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import java.io.File;

/**
 * Created by acs on 9/19/17.
 */

public class MenuLogin extends Activity {

    Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menulogin);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = this;
        initUI();
    }

    private void initUI(){
        File file = new File(Constant.APP_PATH + Constant.HIDDEN_FOLDER + File.separator + Constant.DB_NAME);
        if (!file.exists()) {
            MySQLiteHelper.initApp(mContext);
        }


    }
}
