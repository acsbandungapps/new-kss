package com.newkss.acs.newkss;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.newkss.acs.newkss.Menu.MenuLogin;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.Util.Constant;

/**
 * Created by acs on 11/04/17.
 */

public class ParentActivity extends Activity {

    Context mContext;
    Settle_Detil querySD = new Settle_Detil();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mHandler.sendEmptyMessageDelayed(1, Constant.TimeOutApps);
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        mHandler.removeMessages(1);
        mHandler.sendEmptyMessageDelayed(1, Constant.TimeOutApps);
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                MessageTimeout();
            }
        }
    };

    private void MessageTimeout() {
        try {
            if (!((Activity) mContext).isFinishing()) {
                if (querySD.isSettleAvailable().equals("0")){
                    new android.app.AlertDialog.Builder(mContext).setTitle("Pesan").setMessage("Sesi Anda Telah Berakhir").
                            setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent i = new Intent(mContext, MenuLogin.class);
                                    startActivity(i);
                                    finish();
                                }
                            }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            Intent i = new Intent(mContext, MenuLogin.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void Toast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }


}
