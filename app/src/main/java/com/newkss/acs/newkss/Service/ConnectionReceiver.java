package com.newkss.acs.newkss.Service;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.newkss.acs.newkss.Menu.MenuInisialisasi;
import com.newkss.acs.newkss.Menu.MenuLogin;
import com.newkss.acs.newkss.Menu.MenuPerformance;
import com.newkss.acs.newkss.Model.Pending;
import com.newkss.acs.newkss.Model.Photo;
import com.newkss.acs.newkss.Model.Settle_Detil;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.JSONParser;
import com.newkss.acs.newkss.Util.MySQLiteHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by acs on 7/13/17.
 */

public class ConnectionReceiver extends BroadcastReceiver {
    Pending queryPending = new Pending();
    Pending dataPending;
    Pending dataPending1;
    String jsonSend = "";
    JSONParser jsonParser = new JSONParser();
    String resultfromJson;

    Settle_Detil dataSettleDetil;
    Settle_Detil querySettleDetil = new Settle_Detil();

    Photo queryPhoto = new Photo();
    SharedPreferences shared;
    String passwordShared;
    ArrayList<Pending> arrPending;
    String keterangan = "";
    String rc = "";
    String uniquecode = "";

    @Override
    public void onReceive(final Context context, final Intent intent) {
        MySQLiteHelper.initDB(context);
        if (isOnline(context)) {

            System.out.println("Koneksi Tersedia");
            queryPending.cekAvailable();
            if (queryPending.getDataPending() == null) {
                System.out.println(queryPending.getDataPending());
                System.out.println("0");
            } else if (queryPending.getDataPending() != null) {

                shared = context.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
                passwordShared = (shared.getString(Constant.SHARED_PASSWORD, ""));
//                arrPending=new ArrayList<>();
//                arrPending=queryPending.getArrayListDataPending();
//                System.out.println("Count arrPending: "+arrPending.size());

                dataPending1 = new Pending();
                dataPending1 = queryPending.getDataPending();
                //for(Pending p:arrPending){

                try {
                    JSONObject json = new JSONObject(dataPending1.getData());
                    JSONArray jsonArray = json.getJSONArray("hasil_kunjungan");

                    for (int in = 0; in < jsonArray.length(); in++) {
                        JSONObject jsonObj = jsonArray.getJSONObject(in);
                        uniquecode = jsonObj.getString("unique_code");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (querySettleDetil.cekUniqueCodeAvailable(dataPending1.getId_pending()).equals("kosong")) {
                    if (dataPending1.getDataPending().getKet_update().equals("Collect") && dataPending1.getDataPending().status.equals("2")) {
                        System.out.println("Masuk Collect");
                        dataPending = new Pending();
                        dataPending = queryPending.getDataPending();
                        System.out.println("1");
                        //System.out.println(queryPending.getDataPending());
                        //  queryPending.deleteAllPendingData();
                        new SendOfflineCollect(context).execute();
                    } else if (dataPending1.getDataPending().getKet_update().equals("Visit") && dataPending1.getDataPending().status.equals("2")) {
                        System.out.println("Masuk Visit");
                        dataPending = new Pending();
                        dataPending = queryPending.getDataPending();
                        new SendOfflineVisit(context).execute();
                    } else if (dataPending1.getDataPending().getKet_update().equals("Settle") && dataPending1.getDataPending().status.equals("2")) {
                        System.out.println("Masuk Settle");
                        dataPending = new Pending();
                        dataPending = queryPending.getDataPending();
                        new SendOfflineSettle(context).execute();
                    }
                } else {
                    queryPending.updateStatusPendingDataFailed(dataPending1.getId_pending());
                    queryPending.updateKeteranganPendingData(dataPending1.getDataPending().getId_pending(), keterangan + " " + (rc));

                }


//                }


            }
        } else if (isOnline(context) == false) {
            System.out.println("Koneksi False");
        }
    }

    public class SendOfflineSettle extends AsyncTask<String, String, String> {
        Context context;

        private SendOfflineSettle(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                JSONObject json = new JSONObject(dataPending.getDataPending().getData());
                //json.put("password", passwordShared);
                System.out.println("New Json Settle: " + json.toString());
                resultfromJson = jsonParser.HttpRequestPost(dataPending.getDataPending().getUrl(), json.toString(), Constant.TimeOutConnection, dataPending.getDataPending().getHeader());
                return resultfromJson;
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            System.out.println("Masuk Async Settle");
            System.out.println("Result: " + result);
            if (result.contains(Constant.CONNECTION_LOST)) {
                System.out.println(Constant.CONNECTION_LOST);
            } else if (result.contains(Constant.CONNECTION_ERROR)) {
                System.out.println(Constant.CONNECTION_ERROR);
            } else {
                try {

                    JSONObject jsonR = new JSONObject(result);
                    String rc = jsonR.getString("rc");
                    String keterangan = jsonR.getString("keterangan");
                    if (rc.equals("00")) {
                        System.out.println("Data Settle Terkirim");
                        queryPending.updateStatusPendingData(dataPending.getDataPending().getId_pending());
                    } else if (rc.equals("T1")) {
                        Intent i = new Intent(context, MenuLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();

                    } else if (rc.equals("T2")) {
                        Intent i = new Intent(context, MenuLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();

//                        shared = context.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                        shared.edit().clear().commit();

                    } else if (rc.equals("T3")) {
                        Intent i = new Intent(context, MenuLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();

//                        shared = context.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                        shared.edit().clear().commit();
                    } else if (rc.equals("T4")) {
                        Intent i = new Intent(context, MenuLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();

//                        shared = context.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                        shared.edit().clear().commit();
                    } else if (rc.equals("T5")) {
                        Intent i = new Intent(context, MenuLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();

//                        shared = context.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                        shared.edit().clear().commit();
                    } else {
                        queryPending.updateKeteranganPendingData(dataPending.getDataPending().getId_pending(), keterangan + " " + (rc));
                        //Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    queryPending.updateKeteranganPendingData(dataPending.getDataPending().getId_pending(), keterangan + " " + (rc));
                    e.printStackTrace();
                } catch (Exception e) {
                    queryPending.updateKeteranganPendingData(dataPending.getDataPending().getId_pending(), keterangan + " " + (rc));
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    public class SendOfflineVisit extends AsyncTask<String, String, String> {
        Context context;

        private SendOfflineVisit(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                JSONObject json = new JSONObject(dataPending.getDataPending().getData());
                //json.put("password", passwordShared);
                System.out.println("New Json Visit: " + json.toString());
                resultfromJson = jsonParser.HttpRequestPost(dataPending.getDataPending().getUrl(), json.toString(), Constant.TimeOutConnection, dataPending.getDataPending().getHeader());
                return resultfromJson;
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            System.out.println("Masuk Async Visit");
            System.out.println("Result: " + result);

            if (result.contains(Constant.CONNECTION_LOST)) {
                System.out.println(Constant.CONNECTION_LOST);
            } else if (result.contains(Constant.CONNECTION_ERROR)) {
                System.out.println(Constant.CONNECTION_ERROR);
            } else {
                try {
                    JSONObject jsonR = new JSONObject(result);
                    rc = jsonR.getString("rc");
                    keterangan = jsonR.getString("keterangan");
                    if (rc.equals("00")) {
                        System.out.println("Data visit Terkirim");
                        //    queryPhoto.updateStatusPhoto(dataPending.getJson_update());
                        queryPending.updateStatusPendingData(dataPending.getDataPending().getId_pending());
                    } else if (rc.equals("T1")) {
                        Intent i = new Intent(context, MenuLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();

                    } else if (rc.equals("T2")) {
                        Intent i = new Intent(context, MenuLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();

//                        shared = context.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                        shared.edit().clear().commit();

                    } else if (rc.equals("T3")) {
                        Intent i = new Intent(context, MenuLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();

//                        shared = context.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                        shared.edit().clear().commit();
                    } else if (rc.equals("T4")) {
                        Intent i = new Intent(context, MenuLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();

//                        shared = context.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                        shared.edit().clear().commit();
                    } else if (rc.equals("T5")) {
                        Intent i = new Intent(context, MenuLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();

//                        shared = context.getSharedPreferences(Constant.MYPREF, Context.MODE_PRIVATE);
//                        shared.edit().clear().commit();
                    } else {
                        queryPending.updateStatusPendingDataFailed(dataPending.getId_pending());
                        queryPending.updateKeteranganPendingData(dataPending.getDataPending().getId_pending(), keterangan + " " + (rc));
//                        Toast.makeText(context, keterangan, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    queryPending.updateStatusPendingDataFailed(dataPending.getId_pending());
                    queryPending.updateKeteranganPendingData(dataPending.getDataPending().getId_pending(), e.getMessage());
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    public class SendOfflineCollect extends AsyncTask<String, String, String> {
        Context context;

        private SendOfflineCollect(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                JSONObject json = new JSONObject(dataPending.getDataPending().getData());
                // json.put("password", passwordShared);
//                JSONArray jsonArray = json.getJSONArray("hasil_kunjungan");
//
//                for (int in = 0; in < jsonArray.length(); in++) {
//                    JSONObject jsonObj = jsonArray.getJSONObject(in);
//                    uniquecode = jsonObj.getString("unique_code");
//                }
                System.out.println("New Json Collect: " + json.toString());
                resultfromJson = jsonParser.HttpRequestPost(dataPending.getDataPending().getUrl(), dataPending.getDataPending().getData(), Constant.TimeOutConnection, dataPending.getDataPending().getHeader());
                return resultfromJson;
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            System.out.println("Masuk Async Collect");
            System.out.println("Result: " + result);
            if (result.contains(Constant.CONNECTION_LOST)) {
                System.out.println(Constant.CONNECTION_LOST);
            } else if (result.contains(Constant.CONNECTION_ERROR)) {
                System.out.println(Constant.CONNECTION_ERROR);
            } else {
                try {
                    JSONObject jsonR = new JSONObject(result);
                    rc = jsonR.getString("rc");
                    keterangan = jsonR.getString("keterangan");
                    if (rc.equals("00")) {
                        JSONObject json = new JSONObject(dataPending.getDataPending().getJson_update());
                        String no_loan = json.getString("no_loan");
                        String no_rek = json.getString("no_rek");
                        String nama = json.getString("nama");
                        String nominal_bayar = json.getString("nominal_bayar");
                        String total_kewajiban = json.getString("total_kewajiban");
                        String id_det_inv = json.getString("id_det_inv");
                        String status = json.getString("status");
//                        String sisa_bayar = json.getString("sisa_bayar");
                        String kode_kunjungan = json.getString("kode_kunjungan");

                        if (kode_kunjungan.equals("NN") || kode_kunjungan.equals("NP") || kode_kunjungan.equals("JB")) {

                        } else if (kode_kunjungan.equals("FP") || kode_kunjungan.equals("PP")) {
                            //masukin data ke settle detil, data ini buat nanti settlement
                            dataSettleDetil = new Settle_Detil();
                            dataSettleDetil.setNo_loan(no_loan);
                            dataSettleDetil.setNo_rek(no_rek);
                            dataSettleDetil.setNama(nama);
                            dataSettleDetil.setNominal_bayar(nominal_bayar);
                            dataSettleDetil.setTotal_kewajiban(total_kewajiban);
                            dataSettleDetil.setId_detail_inventory(id_det_inv);
                            dataSettleDetil.setStatus("0");
                            dataSettleDetil.setUniqueCode(uniquecode);
                            dataSettleDetil.setWaktu(Function.getDateNow());
//                            dataSettleDetil.setSisa_bayar(sisa_bayar);
                            querySettleDetil.insertSettleDetil(dataSettleDetil);
                        }
                        queryPending.updateStatusPendingData(dataPending.getDataPending().getId_pending());
                        //  queryPhoto.updateStatusPhoto(no_loan);
                    } else {
                        queryPending.updateStatusPendingDataFailed(dataPending.getId_pending());
                        queryPending.updateKeteranganPendingData(dataPending.getDataPending().getId_pending(), keterangan + " " + (rc));
                    }

                } catch (Exception e) {
                    queryPending.updateStatusPendingDataFailed(dataPending.getId_pending());
                    queryPending.updateKeteranganPendingData(dataPending.getDataPending().getId_pending(), e.getMessage());

                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        //System.out.println(netInfo != null && netInfo.isConnected());
        System.out.println(netInfo);
        return (netInfo != null && netInfo.isConnected());
    }

}
