package com.newkss.acs.newkss.TakePhoto;

import android.app.Activity;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.newkss.acs.newkss.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acs on 6/9/17.
 */

public class AddList extends Activity {

//    ArrayList<String> listItems=new ArrayList<String>();
//
//    //DEFINING A STRING ADAPTER WHICH WILL HANDLE THE DATA OF THE LISTVIEW
//    ArrayAdapter<String> adapter;
//
//    //RECORDING HOW MANY TIMES THE BUTTON HAS BEEN CLICKED
//    int clickCounter=0;
//
//    Button btnTes;
//
//    @Override
//    public void onCreate(Bundle icicle) {
//        super.onCreate(icicle);
//        setContentView(R.layout.list);
//        adapter=new ArrayAdapter<String>(this,
//                android.R.layout.simple_list_item_1,
//                listItems);
//        setListAdapter(adapter);
//        btnTes=(Button) findViewById(R.id.btnTes);
//        btnTes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                addItems(view);
//            }
//        });
//
//    }
//
//    //METHOD WHICH WILL HANDLE DYNAMIC INSERTION
//    public void addItems(View v) {
//        listItems.add("Clicked : "+clickCounter++);
//        adapter.notifyDataSetChanged();
//    }


    private ListView lvTes;
    List<String> listString;
    private Button btnTes;
    private EditText etTeks;
    private String teks;
    AddListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);
        lvTes = (ListView) findViewById(R.id.lvTes);
        btnTes = (Button) findViewById(R.id.btnTes);
        etTeks = (EditText) findViewById(R.id.etTeks);

        listString = new ArrayList<>();
        listString.add("Cecep");
        listString.add("Risya");

        adapter = new AddListAdapter(getApplicationContext(), listString);
        lvTes.setAdapter(adapter);

        btnTes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                teks = etTeks.getText().toString();
                listString.add(teks);
                adapter.notifyDataSetChanged();

            }
        });
    }

}

