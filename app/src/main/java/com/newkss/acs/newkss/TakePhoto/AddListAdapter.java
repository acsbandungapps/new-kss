package com.newkss.acs.newkss.TakePhoto;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newkss.acs.newkss.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acs on 6/9/17.
 */

public class AddListAdapter extends BaseAdapter {

//    Context context;
//    int layoutResourceId;
//    List<String> list;
//
//    public AddListAdapter(Context context, int layoutResourceId, List<String> arrList) {
//
//        this.layoutResourceId = layoutResourceId;
//        this.context = context;
//        this.list = arrList;
//    }
//
//    @Override
//    public int getCount() {
//        return 0;
//    }
//
//    @Override
//    public Object getItem(int i) {
//        return null;
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return 0;
//    }
//
//    static class ViewHolder {
//        TextView nama;
//    }
//
//    @Override
//    public View getView(int i, View convertView, ViewGroup viewGroup) {
//        View row = convertView;
//        ViewHolder holder = null;
//
//        if (row == null) {
//            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
//            row = inflater.inflate(layoutResourceId, viewGroup, false);
//
//            holder = new ViewHolder();
//            holder.nama = (TextView) row.findViewById(R.id.teksNama);
//            row.setTag(holder);
//        } else {
//            holder = (ViewHolder) row.getTag();
//        }
//
//        holder.nama.setText(list.get(i).toString());
//        return row;
//    }

    List<String> list;
    Context mContext;
    LayoutInflater inflater;

    public AddListAdapter(Context context, List<String> listString) {
        mContext = context;
        list = listString;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public class ViewHolder {
        TextView nama;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listadapter, viewGroup, false);
            holder.nama = (TextView) view.findViewById(R.id.teksNama);
            view.setTag(holder);
        }

        else{
            holder = (ViewHolder) view.getTag();
        }

        holder.nama.setText(list.get(i).toString());
        return view;
    }
}
