package com.newkss.acs.newkss.TakePhoto;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.newkss.acs.newkss.R;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by acs on 6/8/17.
 */

public class TakeImage1 extends Activity {
    private Button takePictureButton;
    private ImageView imageView;
    private Uri file;
    private Uri uriSavedImage;
    String path;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    Bitmap bitmap;
    String filename;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.takeimage1);

        takePictureButton = (Button) findViewById(R.id.button_image);
        imageView = (ImageView) findViewById(R.id.imageview);


//        Bitmap bmp = BitmapFactory.decodeFile("/storage/emulated/0/KSP/KSP_20170608_132524.png");
//        imageView.setImageBitmap(bmp);
    }


    public void takePicture(View view) {

        //camera stuff
        Intent imageIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        //folder stuff
        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "KSP");
        imagesFolder.mkdirs();

        filename = "KSP_" + timeStamp + ".png";
        File image = new File(imagesFolder, filename);
        uriSavedImage = Uri.fromFile(image);

        path = image.toString();
        System.out.println("PATH: " + image.getPath());


//        Bitmap bmp= BitmapFactory.decodeFile(image.toString());
//        ByteArrayOutputStream bos=new ByteArrayOutputStream();
//        bmp.compress(Bitmap.CompressFormat.JPEG,70,bos);
//        InputStream in=new ByteArrayInputStream(bos.toByteArray());
//        ContentBody foto = new InputStreamBody(in, "image/jpeg", "filename");


//        File f = new File(imagesFolder, "KSPComp_" + timeStamp + ".png");
//        try {
//            f.createNewFile();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        InputStream is = null;
//        try {
//            is = new URL(image.getPath()).openStream();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


        //bitmap = BitmapFactory.decodeFile(image.toString());

        //bitmap = resizeImage(bitmap, 320, 240);


//        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
//        byte[] bitmapdata = bos.toByteArray();
//
//
//        try {
//            FileOutputStream fos = new FileOutputStream(f);
//            fos.write(bitmapdata);
//            fos.flush();
//            fos.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


//        File file = new File(image.toString());
//        if (file.exists()) {
//            file.delete();
//        }
//        try {
//            OutputStream fOut = new FileOutputStream(file);
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
//            fOut.flush();
//            fOut.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        startActivityForResult(imageIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

    }

    public static Bitmap resizeImage(Bitmap bitmap, int newWidth, int newHeight) {
        try {
            if (bitmap != null) {
                int width = bitmap.getWidth();
                int height = bitmap.getHeight();
                float scaleWidth = ((float) newWidth) / width;
                float scaleHeight = ((float) newHeight) / height;
                Matrix matrix = new Matrix();
                matrix.postScale(scaleWidth, scaleHeight);
                Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
                return resizedBitmap;
            } else {
                return null;
            }
        } catch (Exception e) {
            System.out.println("Error Resize :" + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                File dir = new File(Environment.getExternalStorageDirectory(), "KSP");
                Bitmap b = BitmapFactory.decodeFile(path);
                Bitmap out = Bitmap.createScaledBitmap(b, 320, 480, false);

                File file = new File(dir, filename);
                if (file.exists()) {
                    file.delete();
                }
                FileOutputStream fOut;
                try {
                    fOut = new FileOutputStream(file);
                    out.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();
//                    b.recycle();
//                    out.recycle();
                } catch (Exception e) {
                }

                //imageView.setImageBitmap();
                //imageView.setImageURI(uriSavedImage);
                imageView.setImageBitmap(out);
            }
        }
    }
}
