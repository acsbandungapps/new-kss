package com.newkss.acs.newkss.Util;

import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import com.newkss.acs.newkss.ParentActivity;

import java.io.File;

/**
 * Created by acs on 19/04/17.
 */

public class Constant extends ParentActivity {

//    public static final int TimeOutApps = 600000;
    //3 jam
    public static final int TimeOutApps = 108000000;
    public static String NIK = "NIK";
    public static String POPUP_REFRESH = "Refresh";
    public static String POPUP_LOGOUT = "Logout";

    public static String SHARED_USERNAME = "username";
    public static String SHARED_PASSWORD = "password";
    public static final String MYPREF = "SharedPreferences";

    public static String USERNAME_LOGIN = "admin";
    public static String PASS_LOGIN = "admin";

    //CONSTANT SPINNER
    public static String SESUAI = "Sesuai";
    public static String TIDAK = "Tidak";

    //CONSTANT BUNDLE
    public static String BUNDLE_TITLE = "title";
    public static String BUNDLE_DATA = "data";
    public static String INIT_KODE = "activationcode";
    public static String USERNAME = "username";
    public static String PASSWORD = "password";
    public static String WAKTU = "waktu";

    //Error
    public static String CONNECTION_LOST = "Koneksi Terputus, Cek Mode Wifi/Airplane";
    public static String CONNECTION_ERROR = "Koneksi Error";
    public static String CONNECTION_EMPTY = "Server Not Response";

    //ALERT PROGRESS DIALOG
    public static String LOADING = "Loading...";
    public static String MESSAGE_PROGRESS = "Validasi Data";
    public static String MESSAGE = "Pesan";
    public static String MESSAGE_OK = "OK";


    //JSON
    public static String JSON_USERNAME = "username";
    public static String JSON_PASSWORD = "password";
    public static String JSON_WAKTU = "waktu";
    public static String JSON_NIK = "nik";
    public static String JSON_NAMA = "nama";

    //UNTUK URL
    public static String HTTP = "http://";
//   public static String HTTP = "https://";
    //   public static String IP = "172.168.100.51";
//     public static String IP = "103.3.47.42";

    //IP Dev ACS
    public static String IP = "103.30.123.15";

   //IP PROD KSS
//    public static String IP = "10.50.238.12";

//     public static String IP = "202.159.100.162";
    //public static String IP = "172.16.172.101";
//    public static String IP = "202.159.116.206";

    // public static String PORT = "81";
    public static String PORT = "8052";
    public static String API = "api";
    public static String VER_API = "v.1";
    public static String URL_INIT = "init";
    public static String URL_DATA_SYNC = "data.sync";
    public static String URL_LOGIN = "login";
    public static String URL_HASIL_KUNJUNGAN = "visit";
    public static String URL_SURVEY = "survey";
    public static String URL_SETTLE = "settle.sync";
    public static String URL_TRACK = "track";
    public static String CHANGE_PASS = "chpass";
    public static String SERVICE_URI = HTTP + IP + ":" + PORT + "/" + API + "/" + VER_API + "/";
//    public static String SERVICE_URI = HTTP + IP + "/" + API + "/" + VER_API + "/";

    public static String PERIODIK_TRACK = "periodik_track";
    public static String SERVICE_LOGIN = SERVICE_URI + URL_LOGIN;
    public static String SERVICE_INIT = SERVICE_URI + URL_INIT;
    public static String SERVICE_DATA_SYNC = SERVICE_URI + URL_DATA_SYNC;
    public static String SERVICE_HASIL_KUNJUNGAN = SERVICE_URI + URL_HASIL_KUNJUNGAN;
    public static String SERVICE_SURVEY = SERVICE_URI + URL_SURVEY;
    public static String SERVICE_SETTLE = SERVICE_URI + URL_SETTLE;
    public static String SERVICE_TRACK = SERVICE_URI + URL_TRACK;
    public static String SERVICE_CHANGE_PASS = SERVICE_URI + CHANGE_PASS;
    public static final int TimeOutConnection = 800000;

    public static String HEADER = "header";

    //TABLE PHOTO
    public static String ID_PHOTO = "id_photo";
    public static String NAMA_PHOTO = "nama_photo";
    public static String URL_PHOTO = "url_photo";
    public static String KETERANGAN_PHOTO = "keterangan";

    //TABLE PENAGIHAN
    public static String ID_PENAGIHAN = "id_penagihan";
    public static String NAMA_PENAGIHAN = "nama_penagihan";

    //TABLE DETAIL INVENTORY
    public static String ID_DETAIL_INVENTORY = "id_detail_inventory";
    public static String SALDO_MIN = "saldo_min";
    public static String DPD_TODAY = "dpd_today";
    public static String RESUME_NSBH = "resume_nsbh";
    public static String ALMT_RUMAH = "almt_rumah";
    public static String TGL_JANJI_BAYAR_TERAKHIR = "tgl_janji_bayar_terakhir";
    public static String DENDA = "denda";
    public static String POLA_BAYAR = "pola_bayar";
    public static String NO_HP = "no_hp";
    public static String TGL_BAYAR_TERAKHIR = "tgl_bayar_terakhir";
    public static String ANGSURAN_KE = "angsuran_ke";
    public static String NAMA_UPLINER = "nama_upliner";
    public static String POKOK = "pokok";
    public static String TGL_SP = "tgl_sp";
    public static String TENOR = "tenor";
    public static String ANGSURAN = "angsuran";
    public static String GENDER = "gender";
    public static String TGL_PTP = "tgl_ptp";
    public static String ACTION_PLAN = "action_plan";
    public static String NO_REKENING = "no_rekening";
    public static String HISTORI_SP = "histori_sp";
    public static String NOMINAL_JANJI_BAYAR_TERAKHIR = "nominal_janji_bayar_terakhir";
    public static String TLPN_REKOMENDATOR = "tlpn_rekomendator";
    public static String TLPN_UPLINER = "tlpn_upliner";
    public static String SUMBER_BAYAR = "sumber_bayar";
    public static String BUNGA = "bunga";
    public static String TLPN_ECON = "tlpn_econ";
    public static String JNS_PINJAMAN = "jns_pinjaman";
    public static String NOMINAL_BAYAR_TERAKHIR = "nominal_bayar_terakhir";
    public static String HARUS_BAYAR = "harus_bayar";
    public static String NOMINAL_PTP = "nominal_ptp";
    public static String SALDO = "saldo";
    public static String NAMA_REKOMENDATOR = "nama_rekomendator";
    public static String TLPN_MOGEN = "tlpn_mogen";
    public static String OS_PINJAMAN = "os_pinjaman";
    public static String NAMA_ECON = "nama_econ";
    public static String KEBERADAAN_JAMINAN = "keberadaan_jaminan";
    public static String EMAIL = "email";
    public static String KEWAJIBAN = "kewajiban";
    public static String TOTAL_KEWAJIBAN = "total_kewajiban";
    public static String NAMA_MOGEN = "nama_mogen";
    public static String NO_LOAN = "no_loan";
    public static String NAMA_DEBITUR = "nama_debitur";
    public static String TGL_JTH_TEMPO = "tgl_jth_tempo";
    public static String NO_TLPN = "no_tlpn";
    public static String TGL_GAJIAN = "tgl_gajian";
    public static String PEKERJAAN = "pekerjaan";
    public static String ALMT_USAHA = "almt_usaha";

    //TABLE DETAIL BUCKET
    public static String ID_DETAIL_BUCKET = "id_detail_bucket";
    public static String ID_BUCKET = "id_bucket";
    public static String DB_ID_DETAIL_INVENTORY = "id_detail_inventory";
    public static String DB_NAMA_PENAGIHAN = "nama_penagihan";
    public static String CYCLE = "cycle";
    public static String JARAK = "jarak";
    public static String DB_OS = "os";
    public static String NAMA = "nama";
    public static String ALAMAT = "alamat";
    public static String TOTAL_BAYAR = "total_bayar";
    public static String JUMLAH_BUCKET = "jumlah_bucket";

    //TABLE_CONFIG
    public static String NAMA_CONFIG = "nama_config";
    public static String VALUE_CONFIG = "value";

    //TABLE DETIL_TGT
    public static String TGL_KUNJUNGAN = "tgl_kunjungan";
    public static String STATUS = "status";

    public static String PENAGIHAN_BOOKED = "Penagihan";
    public static String PENCAIRAN_BOOKED = "Pencairan";

    public static String PENAGIHAN = "Penagihan";
    public static String PENCAIRAN = "Pencairan";

    //TABLE ACTION CODE
    public static String ACTION_CODE = "action_code";


    public static String POPUP_DATAPENDING="Data Pending";
    public static final String APK_LOCATION = "Environment.getExternalStorageDirectory()";
    //SQLITE
    //public static final String FOLDER_NAME_APP = ".kss";
    public static final String HIDDEN_FOLDER = ".kss";
    //public static final String FOLDER_NAME_APP = "Android" + File.separator + "data";
    public static final String FOLDER_NAME_APP = ".kss";
    //public static final String APP_PATH=Environment.getExternalStorageDirectory() + "/Lakupandai";
//    public static final String APP_PATH = Environment.getExternalStorageDirectory() + File.separator + Constant.FOLDER_NAME_APP + File.separator;
    public static final String APP_PATH = Environment.getExternalStorageDirectory() + File.separator + Constant.FOLDER_NAME_APP + File.separator;
    public static final String DB_NAME = "kss.db3";
    public static final int DB_VERSION = 3;
    public static SQLiteDatabase MKSSdb;
    public static String TABLE_DETAIL_INVENTORY = "kss_detail_inventory";
    public static String TABLE_PENAGIHAN = "kss_penagihan";
    public static String TABLE_SEQUENCE = "sqlite_sequence";
    public static String TABLE_BUCKET = "kss_bucket";
    public static String TABLE_DETAIL_BUCKET = "kss_detail_bucket";
    public static String TABLE_DETAIL_BUCKET_BOOKED = "kss_detail_bucket_booked";
    public static String TABLE_CONFIG = "kss_config";
    public static String TABLE_PHOTO = "kss_photo";
    public static String TABLE_DETIL_TGT = "kss_detil_tgt";
    public static String TABLE_DATA_BOOKED = "kss_data_booked";
    public static String TABLE_SETTLEMENT = "kss_settle";
    public static String TABLE_DETIL_SETTLEMENT = "kss_detil_settle";
    public static String TABLE_MASTER_SETTLEMENT = "kss_master_settle";
    public static String TABLE_PENDING = "kss_pending";
    public static String TABLE_PENCAIRAN = "kss_pencairan";
    public static String TABLE_DATA_PENCAIRAN = "kss_data_pencairan";
    public static String TABLE_DETIL_DATA_PENCAIRAN = "kss_detil_data_pencairan";
    public static String TABLE_DETAIL_INVENTORY_PENCAIRAN = "kss_detail_inventory_pencairan";
    public static String TABLE_PARAMETER = "kss_parameter";
    public static String TABLE_SETOR = "kss_setor";


    //TABLE BARU
    public static String TABLE_TGT = "kss_tgt";


    //TABLE TGT
    public static String TABLE_TGT_ID_TGT = "id_tgt";


    public static String responJson = "{\"waktu\":\"[TEXT]\",\"status_kunjungan\":[{\"status\":\"[TEXT]\",\"no_loan\":\"[TEXT]\"},{\"status\":\"[TEXT]\",\"no_loan\":\"[TEXT]\"}],\"keterangan\":\"[TEXT]\"}";


    public static String contohJson = "{\n" +
            "\t\"waktu\" : \"22-06-2017 09:00:53\",\n" +
            "\t\"pencairan\" : {\n" +
            "\t\t\"data_pencairan\" : [{\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-1\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"JL MANGGA BESAR IV Q/4 RT:3 RW:5 KELURAHAN:TAMAN SARI KECAMATAN:TAMAN SARI\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"24\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"26422104.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"3000170657\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"PPM - Refferal BSS\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"5.373395748E7\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"2.6422104E7\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"2.6422104E7\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1616500015\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"PHETY SOEKIATO\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"15-06-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \",08181888756\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"2.6422104E7\",\n" +
            "\t\t\t\t\"nama\" : \"PHETY SOEKIATO\",\n" +
            "\t\t\t\t\"alamat\" : \"JL MANGGA BESAR IV Q/4 RT:3 RW:5 KELURAHAN:TAMAN SARI KECAMATAN:TAMAN SARI\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-8\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"GUNUNG TUA BARU KELURAHAN:GUNUNG TUA BARU KECAMATAN:PADANG BOLAK\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"33\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"762544.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"5000093407\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"4541124.07\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"762544.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"762544.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1617300074\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"SYAHNAN HARAHAP\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"22-06-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"085297278400,085297278400\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"762544.0\",\n" +
            "\t\t\t\t\"nama\" : \"SYAHNAN HARAHAP\",\n" +
            "\t\t\t\t\"alamat\" : \"GUNUNG TUA BARU KELURAHAN:GUNUNG TUA BARU KECAMATAN:PADANG BOLAK\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-14\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"JL. KELAPA LILIN III DC-6/5 RT:6 RW:3 KELURAHAN:CURUG SANGERENG KECAMATAN:KELAPA DUA\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"35\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"28125627.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"3000290296\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pembiayaan KMG Sahabat UKM\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"18746.0\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"2.8125627E7\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"2.8125627E7\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1618397432\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"ASWAN WIRYADI\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"28-06-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \",0811841283\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"2.8125627E7\",\n" +
            "\t\t\t\t\"nama\" : \"ASWAN WIRYADI\",\n" +
            "\t\t\t\t\"alamat\" : \"JL. KELAPA LILIN III DC-6/5 RT:6 RW:3 KELURAHAN:CURUG SANGERENG KECAMATAN:KELAPA DUA\"\n" +
            "\t\t\t}\n" +
            "\t\t],\n" +
            "\t\t\"ss\" : [],\n" +
            "\t\t\"bi\" : [],\n" +
            "\t\t\"bs\" : [],\n" +
            "\t\t\"blbs\" : []\n" +
            "\t},\n" +
            "\t\"penagihan\" : {\n" +
            "\t\t\"npay\" : [],\n" +
            "\t\t\"fpay\" : [],\n" +
            "\t\t\"nvst\" : [],\n" +
            "\t\t\"ppay\" : [],\n" +
            "\t\t\"tgt\" : [{\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-1\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"JL MANGGA BESAR IV Q/4 RT:3 RW:5 KELURAHAN:TAMAN SARI KECAMATAN:TAMAN SARI\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"24\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"26422104.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"3000170657\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"PPM - Refferal BSS\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"5.373395748E7\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"2.6422104E7\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"2.6422104E7\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1616500015\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"PHETY SOEKIATO\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"15-06-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \",08181888756\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"2.6422104E7\",\n" +
            "\t\t\t\t\"nama\" : \"PHETY SOEKIATO\",\n" +
            "\t\t\t\t\"alamat\" : \"JL MANGGA BESAR IV Q/4 RT:3 RW:5 KELURAHAN:TAMAN SARI KECAMATAN:TAMAN SARI\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-21\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"JL TAMAN SARI X/110 C RT:7 RW:3 KELURAHAN:TAMAN SARI KECAMATAN:TAMAN SARI\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"34\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"702262.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"5000213103\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"11922.67\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"702262.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"702262.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1636300009\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"MOHAMAD RUSLI\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"05-07-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"088808021202,088808021202\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"702262.0\",\n" +
            "\t\t\t\t\"nama\" : \"MOHAMAD RUSLI\",\n" +
            "\t\t\t\t\"alamat\" : \"JL TAMAN SARI X/110 C RT:7 RW:3 KELURAHAN:TAMAN SARI KECAMATAN:TAMAN SARI\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-21\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"JL. TAMAN SARI X NO. 1100 RT:7 RW:3 KELURAHAN:TAMAN SARI KECAMATAN:TAMAN SARI\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"34\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"757800.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"8000008649\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"200.0\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"757800.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"757800.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1711620005\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"SYAHRONI\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"05-07-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"628989496979,628989496979\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"757800.0\",\n" +
            "\t\t\t\t\"nama\" : \"SYAHRONI\",\n" +
            "\t\t\t\t\"alamat\" : \"JL. TAMAN SARI X NO. 1100 RT:7 RW:3 KELURAHAN:TAMAN SARI KECAMATAN:TAMAN SARI\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-8\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"JL CANGKIR NO 30 MEDAN KELURAHAN:SEI PUTIH TENGAH KECAMATAN:MEDAN PETISAH\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"36\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"634720.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"5000211097\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"23016.13\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"634720.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"634720.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1635700053\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"VICTOR HASOLOAN NAPITUPULU\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"22-06-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"00,081362898534\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"634720.0\",\n" +
            "\t\t\t\t\"nama\" : \"VICTOR HASOLOAN NAPITUPULU\",\n" +
            "\t\t\t\t\"alamat\" : \"JL CANGKIR NO 30 MEDAN KELURAHAN:SEI PUTIH TENGAH KECAMATAN:MEDAN PETISAH\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-18\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"JL SURAU GG DARURAT NO.4 KELURAHAN:SEI PUTIH BARAT KECAMATAN:MEDAN PETISAH\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"37\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"679403.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"5000260675\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"20782.21\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"679403.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"679403.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1711900138\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"MARLENA TARIGAN\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"02-07-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"081374295274,081374295274\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"679403.0\",\n" +
            "\t\t\t\t\"nama\" : \"MARLENA TARIGAN\",\n" +
            "\t\t\t\t\"alamat\" : \"JL SURAU GG DARURAT NO.4 KELURAHAN:SEI PUTIH BARAT KECAMATAN:MEDAN PETISAH\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-18\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"JL PERIUK NO 32 RT:1 RW:1 KELURAHAN:SEI PUTIH TENGAH KECAMATAN:MEDAN PETISAH\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"25\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"1724332.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"3000205256\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"32122.02\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"1724332.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"1724332.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1528900032\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"FERNANDO H NAPITUPULU,SE\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"02-07-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \",0811634320\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"1724332.0\",\n" +
            "\t\t\t\t\"nama\" : \"FERNANDO H NAPITUPULU,SE\",\n" +
            "\t\t\t\t\"alamat\" : \"JL PERIUK NO 32 RT:1 RW:1 KELURAHAN:SEI PUTIH TENGAH KECAMATAN:MEDAN PETISAH\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-18\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"CIKOKO BARAT I GG IV/47 A RT:5 RW:4 KELURAHAN:CIKOKO KECAMATAN:PANCORAN\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"13\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"846201.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"5000171753\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"1239.73\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"846201.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"846201.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1624000030\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"CAPRYSAN\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"02-07-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"081213649151,085781132767\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"846201.0\",\n" +
            "\t\t\t\t\"nama\" : \"CAPRYSAN\",\n" +
            "\t\t\t\t\"alamat\" : \"CIKOKO BARAT I GG IV/47 A RT:5 RW:4 KELURAHAN:CIKOKO KECAMATAN:PANCORAN\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-21\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"PENGADEGAN BARAT III RT:1 RW:6 KELURAHAN:PENGADEGAN KECAMATAN:PANCORAN\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"21\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"1375600.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"8000007715\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"8503.47\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"1375600.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"1375600.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1709520002\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"KHAERU IBADILLAH\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"05-07-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"6281808436516,6281808436516\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"1375600.0\",\n" +
            "\t\t\t\t\"nama\" : \"KHAERU IBADILLAH\",\n" +
            "\t\t\t\t\"alamat\" : \"PENGADEGAN BARAT III RT:1 RW:6 KELURAHAN:PENGADEGAN KECAMATAN:PANCORAN\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-21\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"PENGADEGAN BARAT III NO.28 RT:1 RW:6 KELURAHAN:PENGADEGAN KECAMATAN:PANCORAN\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"21\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"1616200.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"8000004406\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"1616200.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"1616200.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1703420004\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"RAHMIYANTO\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"05-07-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"6283891017639,6283891017639\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"1616200.0\",\n" +
            "\t\t\t\t\"nama\" : \"RAHMIYANTO\",\n" +
            "\t\t\t\t\"alamat\" : \"PENGADEGAN BARAT III NO.28 RT:1 RW:6 KELURAHAN:PENGADEGAN KECAMATAN:PANCORAN\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-8\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"GUNUNG TUA BARU KELURAHAN:GUNUNG TUA BARU KECAMATAN:PADANG BOLAK\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"33\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"762544.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"5000093407\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"4541124.07\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"762544.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"762544.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1617300074\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"SYAHNAN HARAHAP\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"22-06-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"085297278400,085297278400\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"762544.0\",\n" +
            "\t\t\t\t\"nama\" : \"SYAHNAN HARAHAP\",\n" +
            "\t\t\t\t\"alamat\" : \"GUNUNG TUA BARU KELURAHAN:GUNUNG TUA BARU KECAMATAN:PADANG BOLAK\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-8\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"LINGKUNGAN V PASAR GUNUNG TUA KELURAHAN:PASAR GUNUNG TUA KECAMATAN:PADANG BOLAK\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"21\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"1278398.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"5000168566\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"141904.86\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"1278398.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"1278398.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1623200034\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"ERNITA MANURUNG\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"22-06-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"081376766363,081376766363\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"1278398.0\",\n" +
            "\t\t\t\t\"nama\" : \"ERNITA MANURUNG\",\n" +
            "\t\t\t\t\"alamat\" : \"LINGKUNGAN V PASAR GUNUNG TUA KELURAHAN:PASAR GUNUNG TUA KECAMATAN:PADANG BOLAK\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-14\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"KOMPLEK SERENIA HILLS BLOK V NO 40 RT:1 RW:3 KELURAHAN:LEBAK BULUS KECAMATAN:CILANDAK\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"24\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"761800.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"3000111208\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"6751352.69\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"761800.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"761800.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1704420008\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"INDRIANA SEPUTRA\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"28-06-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"02129042559,08161945929\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"761800.0\",\n" +
            "\t\t\t\t\"nama\" : \"INDRIANA SEPUTRA\",\n" +
            "\t\t\t\t\"alamat\" : \"KOMPLEK SERENIA HILLS BLOK V NO 40 RT:1 RW:3 KELURAHAN:LEBAK BULUS KECAMATAN:CILANDAK\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-8\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"JALAN BATAN RAYA NO. 4 PS JUMAT RT:1 RW:2 KELURAHAN:LEBAK BULUS KECAMATAN:CILANDAK\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"33\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"1584801.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"5000197938\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"1588321.2\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"1584801.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"1584801.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1632100027\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"TAUFIK\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"22-06-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"6283808087884,6288214178515\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"1584801.0\",\n" +
            "\t\t\t\t\"nama\" : \"TAUFIK\",\n" +
            "\t\t\t\t\"alamat\" : \"JALAN BATAN RAYA NO. 4 PS JUMAT RT:1 RW:2 KELURAHAN:LEBAK BULUS KECAMATAN:CILANDAK\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"8\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"JL MENTENG ATAS SELATAN MASJID AL M?UJAHIDIN NO 12 RT:8 RW:6 KELURAHAN:GUNTUR KECAMATAN:SETIA BUDI\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"13887.13\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"888462.91\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"22\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"1308752.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"5000108377\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"413455.83\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"2624557.87\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"2624557.87\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1632100030\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"YUDHI MAIKHEL\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"05-07-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \",085781010260\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"2624557.87\",\n" +
            "\t\t\t\t\"nama\" : \"YUDHI MAIKHEL\",\n" +
            "\t\t\t\t\"alamat\" : \"JL MENTENG ATAS SELATAN MASJID AL M?UJAHIDIN NO 12 RT:8 RW:6 KELURAHAN:GUNTUR KECAMATAN:SETIA BUDI\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-21\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"JL SUBUR RAYA RT:3 RW:8 KELURAHAN:MENTENG ATAS KECAMATAN:SETIA BUDI\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"22\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"1400300.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"8000004171\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"1400300.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"1400300.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1703120017\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"SRI ANNERUSI\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"05-07-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"6285780315478,6285780315478\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"1400300.0\",\n" +
            "\t\t\t\t\"nama\" : \"SRI ANNERUSI\",\n" +
            "\t\t\t\t\"alamat\" : \"JL SUBUR RAYA RT:3 RW:8 KELURAHAN:MENTENG ATAS KECAMATAN:SETIA BUDI\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-8\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"KP PEUSAR RT:8 RW:1 KELURAHAN:BINONG KECAMATAN:CURUG\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"33\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"828678.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"5000157758\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"830336.71\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"828678.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"828678.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1620000038\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"SRI\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"22-06-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"081287321955,081287321955\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"828678.0\",\n" +
            "\t\t\t\t\"nama\" : \"SRI\",\n" +
            "\t\t\t\t\"alamat\" : \"KP PEUSAR RT:8 RW:1 KELURAHAN:BINONG KECAMATAN:CURUG\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-8\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"TAMAN UBUD PERMAI V NO 0011 KELURAHAN:BINONG KECAMATAN:CURUG\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"33\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"861790.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"5000121012\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"47.22\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"861790.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"861790.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1607600008\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"RIA NINGSIH\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"22-06-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"081282023728,081282023728\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"861790.0\",\n" +
            "\t\t\t\t\"nama\" : \"RIA NINGSIH\",\n" +
            "\t\t\t\t\"alamat\" : \"TAMAN UBUD PERMAI V NO 0011 KELURAHAN:BINONG KECAMATAN:CURUG\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-21\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"JL.SRIWIJAYA VII/03 RT:4 RW:18 KELURAHAN:BENCONGAN KECAMATAN:KELAPA DUA\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"22\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"1008481.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"5000136915\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pemb Pemilikan Sepeda Motor (PPSM)\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"38816.62\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"1008481.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"1008481.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1613800003\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"SETYO WAHYU PRATOMO\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"05-07-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \"085697278553,085697278553\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"1008481.0\",\n" +
            "\t\t\t\t\"nama\" : \"SETYO WAHYU PRATOMO\",\n" +
            "\t\t\t\t\"alamat\" : \"JL.SRIWIJAYA VII/03 RT:4 RW:18 KELURAHAN:BENCONGAN KECAMATAN:KELAPA DUA\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-14\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"JL PERIUK NO 32 RT:1 RW:1 KELURAHAN:SEI PUTIH TENGAH KECAMATAN:MEDAN PETISAH\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"48\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"7254877.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"3000140995\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pembiayaan KMG Sahabat UKM\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"9987.09\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"7254877.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"7254877.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1511248386\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"FERNANDO H NAPITUPULU,SE\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"28-06-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \",0811634320\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"7254877.0\",\n" +
            "\t\t\t\t\"nama\" : \"FERNANDO H NAPITUPULU,SE\",\n" +
            "\t\t\t\t\"alamat\" : \"JL PERIUK NO 32 RT:1 RW:1 KELURAHAN:SEI PUTIH TENGAH KECAMATAN:MEDAN PETISAH\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-14\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"CIKOKO BARAT I RT:6 RW:5 KELURAHAN:CIKOKO KECAMATAN:PANCORAN\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"60\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"1815839.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"3000205574\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pembiayaan KMG Sahabat UKM\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"1815839.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"1815839.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1529230490\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"HERRU AIRLANGGA\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"28-06-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \",081288148418\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"1815839.0\",\n" +
            "\t\t\t\t\"nama\" : \"HERRU AIRLANGGA\",\n" +
            "\t\t\t\t\"alamat\" : \"CIKOKO BARAT I RT:6 RW:5 KELURAHAN:CIKOKO KECAMATAN:PANCORAN\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-14\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"DESA PASIR PINANG KELURAHAN:PASIR PINANG KECAMATAN:PORTIBI\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"36\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"586497.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"3000321701\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pembiayaan KMG Sahabat UKM\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"9993.0\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"586497.0\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"586497.0\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1629863391\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"PARAS NAGAPORAN SIREGAR\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"28-06-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \",081377145546\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"586497.0\",\n" +
            "\t\t\t\t\"nama\" : \"PARAS NAGAPORAN SIREGAR\",\n" +
            "\t\t\t\t\"alamat\" : \"DESA PASIR PINANG KELURAHAN:PASIR PINANG KECAMATAN:PORTIBI\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"cycle\" : \"[text3]\",\n" +
            "\t\t\t\t\"detail_inventory\" : [{\n" +
            "\t\t\t\t\t\t\"saldo_min\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"dpd_today\" : \"-14\",\n" +
            "\t\t\t\t\t\t\"almt_rumah\" : \"JL. KELAPA LILIN III DC-6/5 RT:6 RW:3 KELURAHAN:CURUG SANGERENG KECAMATAN:KELAPA DUA\",\n" +
            "\t\t\t\t\t\t\"resume_nsbh\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"denda\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"pola_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_hp\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"tgl_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"angsuran_ke\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nama_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pokok\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tgl_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tenor\" : \"35\",\n" +
            "\t\t\t\t\t\t\"angsuran\" : \"28125627.00\",\n" +
            "\t\t\t\t\t\t\"gender\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"action_plan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_rekening\" : \"3000290296\",\n" +
            "\t\t\t\t\t\t\"histori_sp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_upliner\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"sumber_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"bunga\" : \"0.0\",\n" +
            "\t\t\t\t\t\t\"tlpn_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"jns_pinjaman\" : \"Pembiayaan KMG Sahabat UKM\",\n" +
            "\t\t\t\t\t\t\"nominal_bayar_terakhir\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"harus_bayar\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"nominal_ptp\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"saldo\" : \"18746.0\",\n" +
            "\t\t\t\t\t\t\"nama_rekomendator\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tlpn_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"os_pinjaman\" : \"2.8125627E7\",\n" +
            "\t\t\t\t\t\t\"nama_econ\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"keberadaan_jaminan\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"email\" : \"[no entityt]\",\n" +
            "\t\t\t\t\t\t\"kewajiban\" : \"[no entity]\",\n" +
            "\t\t\t\t\t\t\"total_kewajiban\" : \"2.8125627E7\",\n" +
            "\t\t\t\t\t\t\"nama_mogen\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"no_loan\" : \"LD1618397432\",\n" +
            "\t\t\t\t\t\t\"nama_debitur\" : \"ASWAN WIRYADI\",\n" +
            "\t\t\t\t\t\t\"tgl_jth_tempo\" : \"28-06-2017\",\n" +
            "\t\t\t\t\t\t\"no_tlpn\" : \",0811841283\",\n" +
            "\t\t\t\t\t\t\"total_bayar\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"tgl_gajian\" : \"[text]\",\n" +
            "\t\t\t\t\t\t\"pekerjaan\" : \"[text]\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t],\n" +
            "\t\t\t\t\"jarak\" : \"[text3]\",\n" +
            "\t\t\t\t\"OS\" : \"2.8125627E7\",\n" +
            "\t\t\t\t\"nama\" : \"ASWAN WIRYADI\",\n" +
            "\t\t\t\t\"alamat\" : \"JL. KELAPA LILIN III DC-6/5 RT:6 RW:3 KELURAHAN:CURUG SANGERENG KECAMATAN:KELAPA DUA\"\n" +
            "\t\t\t}\n" +
            "\t\t]\n" +
            "\t}\n" +
            "}";
//    public static String contohJson = "{\n" +
//            "\"waktu\" : \"[TEXT]\",\n" +
//            "\"keterangan\" : \"Sukses Download Data Tagihan\",\n" +
//            "\"pencairan\" : {\n" +
//            "\"data_pencairan\" : [{\n" +
//            "\"cycle\" : \"[text3]\",\n" +
//            "\"detail_inventory\" : [{\n" +
//            "\"saldo_min\" : \"[text]\",\n" +
//            "\"dpd_today\" : \"[text]\",\n" +
//            "\"resume_nsbh\" : \"[text]\",\n" +
//            "\"almt_rumah\" : \"[text]\",\n" +
//            "\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"denda\" : \"[text]\",\n" +
//            "\"pola_bayar\" : \"[text]\",\n" +
//            "\"no_hp\" : \"[text]\",\n" +
//            "\"tgl_bayar_terakhir\" : \"[text]\",\n" +
//            "\"angsuran_ke\" : \"[text]\",\n" +
//            "\"nama_upliner\" : \"[text]\",\n" +
//            "\"pokok\" : \"[text]\",\n" +
//            "\"tgl_sp\" : \"[text]\",\n" +
//            "\"tenor\" : \"[text]\",\n" +
//            "\"angsuran\" : \"[text]\",\n" +
//            "\"gender\" : \"[text]\",\n" +
//            "\"tgl_ptp\" : \"[text]\",\n" +
//            "\"action_plan\" : \"[text]\",\n" +
//            "\"no_rekening\" : \"[text]\",\n" +
//            "\"histori_sp\" : \"[text]\",\n" +
//            "\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"tlpn_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_upliner\" : \"[text]\",\n" +
//            "\"sumber_bayar\" : \"[text]\",\n" +
//            "\"bunga\" : \"[text]\",\n" +
//            "\"tlpn_econ\" : \"[text]\",\n" +
//            "\"jns_pinjaman\" : \"[text]\",\n" +
//            "\"nominal_bayar_terakhir\" : \"[text]\",\n" +
//            "\"harus_bayar\" : \"[text]\",\n" +
//            "\"nominal_ptp\" : \"[text]\",\n" +
//            "\"saldo\" : \"[text]\",\n" +
//            "\"nama_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_mogen\" : \"[text]\",\n" +
//            "\"os_pinjaman\" : \"[text]\",\n" +
//            "\"nama_econ\" : \"[text]\",\n" +
//            "\"keberadaan_jaminan\" : \"[text]\",\n" +
//            "\"email\" : \"[text]\",\n" +
//            "\"kewajiban\" : \"[text]\",\n" +
//            "\"total_kewajiban\" : \"[text]\",\n" +
//            "\"nama_mogen\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\",\n" +
//            "\"nama_debitur\" : \"[text]\",\n" +
//            "\"tgl_jth_tempo\" : \"[text]\",\n" +
//            "\"no_tlpn\" : \"[text]\",\n" +
//            "\"total_bayar\" : \"[text]\",\n" +
//            "\"tgl_gajian\" : \"[text]\",\n" +
//            "\"pekerjaan\" : \"[text]\",\n" +
//            "\"almt_usaha\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"saldo_min\" : \"[text]\",\n" +
//            "\"dpd_today\" : \"[text]\",\n" +
//            "\"resume_nsbh\" : \"[text]\",\n" +
//            "\"almt_rumah\" : \"[text]\",\n" +
//            "\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"denda\" : \"[text]\",\n" +
//            "\"pola_bayar\" : \"[text]\",\n" +
//            "\"no_hp\" : \"[text]\",\n" +
//            "\"tgl_bayar_terakhir\" : \"[text]\",\n" +
//            "\"angsuran_ke\" : \"[text]\",\n" +
//            "\"nama_upliner\" : \"[text]\",\n" +
//            "\"pokok\" : \"[text]\",\n" +
//            "\"tgl_sp\" : \"[text]\",\n" +
//            "\"tenor\" : \"[text]\",\n" +
//            "\"angsuran\" : \"[text]\",\n" +
//            "\"gender\" : \"[text]\",\n" +
//            "\"tgl_ptp\" : \"[text]\",\n" +
//            "\"action_plan\" : \"[text]\",\n" +
//            "\"no_rekening\" : \"[text]\",\n" +
//            "\"histori_sp\" : \"[text]\",\n" +
//            "\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"tlpn_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_upliner\" : \"[text]\",\n" +
//            "\"sumber_bayar\" : \"[text]\",\n" +
//            "\"bunga\" : \"[text]\",\n" +
//            "\"tlpn_econ\" : \"[text]\",\n" +
//            "\"jns_pinjaman\" : \"[text]\",\n" +
//            "\"nominal_bayar_terakhir\" : \"[text]\",\n" +
//            "\"harus_bayar\" : \"[text]\",\n" +
//            "\"nominal_ptp\" : \"[text]\",\n" +
//            "\"saldo\" : \"[text]\",\n" +
//            "\"nama_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_mogen\" : \"[text]\",\n" +
//            "\"os_pinjaman\" : \"[text]\",\n" +
//            "\"nama_econ\" : \"[text]\",\n" +
//            "\"keberadaan_jaminan\" : \"[text]\",\n" +
//            "\"email\" : \"[text]\",\n" +
//            "\"kewajiban\" : \"[text]\",\n" +
//            "\"total_kewajiban\" : \"[text]\",\n" +
//            "\"nama_mogen\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\",\n" +
//            "\"nama_debitur\" : \"[text]\",\n" +
//            "\"tgl_jth_tempo\" : \"[text]\",\n" +
//            "\"no_tlpn\" : \"[text]\",\n" +
//            "\"total_bayar\" : \"[text]\",\n" +
//            "\"tgl_gajian\" : \"[text]\",\n" +
//            "\"pekerjaan\" : \"[text]\",\n" +
//            "\"almt_usaha\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"saldo_min\" : \"[text]\",\n" +
//            "\"dpd_today\" : \"[text]\",\n" +
//            "\"resume_nsbh\" : \"[text]\",\n" +
//            "\"almt_rumah\" : \"[text]\",\n" +
//            "\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"denda\" : \"[text]\",\n" +
//            "\"pola_bayar\" : \"[text]\",\n" +
//            "\"no_hp\" : \"[text]\",\n" +
//            "\"tgl_bayar_terakhir\" : \"[text]\",\n" +
//            "\"angsuran_ke\" : \"[text]\",\n" +
//            "\"nama_upliner\" : \"[text]\",\n" +
//            "\"pokok\" : \"[text]\",\n" +
//            "\"tgl_sp\" : \"[text]\",\n" +
//            "\"tenor\" : \"[text]\",\n" +
//            "\"angsuran\" : \"[text]\",\n" +
//            "\"gender\" : \"[text]\",\n" +
//            "\"tgl_ptp\" : \"[text]\",\n" +
//            "\"action_plan\" : \"[text]\",\n" +
//            "\"no_rekening\" : \"[text]\",\n" +
//            "\"histori_sp\" : \"[text]\",\n" +
//            "\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"tlpn_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_upliner\" : \"[text]\",\n" +
//            "\"sumber_bayar\" : \"[text]\",\n" +
//            "\"bunga\" : \"[text]\",\n" +
//            "\"tlpn_econ\" : \"[text]\",\n" +
//            "\"jns_pinjaman\" : \"[text]\",\n" +
//            "\"nominal_bayar_terakhir\" : \"[text]\",\n" +
//            "\"harus_bayar\" : \"[text]\",\n" +
//            "\"nominal_ptp\" : \"[text]\",\n" +
//            "\"saldo\" : \"[text]\",\n" +
//            "\"nama_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_mogen\" : \"[text]\",\n" +
//            "\"os_pinjaman\" : \"[text]\",\n" +
//            "\"nama_econ\" : \"[text]\",\n" +
//            "\"keberadaan_jaminan\" : \"[text]\",\n" +
//            "\"email\" : \"[text]\",\n" +
//            "\"kewajiban\" : \"[text]\",\n" +
//            "\"total_kewajiban\" : \"[text]\",\n" +
//            "\"nama_mogen\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\",\n" +
//            "\"nama_debitur\" : \"[text]\",\n" +
//            "\"tgl_jth_tempo\" : \"[text]\",\n" +
//            "\"no_tlpn\" : \"[text]\",\n" +
//            "\"total_bayar\" : \"[text]\",\n" +
//            "\"tgl_gajian\" : \"[text]\",\n" +
//            "\"pekerjaan\" : \"[text]\",\n" +
//            "\"almt_usaha\" : \"[text]\"\n" +
//            "}\n" +
//            "],\n" +
//            "\"jarak\" : \"[text3]\",\n" +
//            "\"OS\" : \"[text3]\",\n" +
//            "\"nama\" : \"[text3]\",\n" +
//            "\"alamat\" : \"[text3]\"\n" +
//            "}, {\n" +
//            "\"cycle\" : \"[text3]\",\n" +
//            "\"detail_inventory\" : [{\n" +
//            "\"saldo_min\" : \"[text]\",\n" +
//            "\"dpd_today\" : \"[text]\",\n" +
//            "\"resume_nsbh\" : \"[text]\",\n" +
//            "\"almt_rumah\" : \"[text]\",\n" +
//            "\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"denda\" : \"[text]\",\n" +
//            "\"pola_bayar\" : \"[text]\",\n" +
//            "\"no_hp\" : \"[text]\",\n" +
//            "\"tgl_bayar_terakhir\" : \"[text]\",\n" +
//            "\"angsuran_ke\" : \"[text]\",\n" +
//            "\"nama_upliner\" : \"[text]\",\n" +
//            "\"pokok\" : \"[text]\",\n" +
//            "\"tgl_sp\" : \"[text]\",\n" +
//            "\"tenor\" : \"[text]\",\n" +
//            "\"angsuran\" : \"[text]\",\n" +
//            "\"gender\" : \"[text]\",\n" +
//            "\"tgl_ptp\" : \"[text]\",\n" +
//            "\"action_plan\" : \"[text]\",\n" +
//            "\"no_rekening\" : \"[text]\",\n" +
//            "\"histori_sp\" : \"[text]\",\n" +
//            "\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"tlpn_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_upliner\" : \"[text]\",\n" +
//            "\"sumber_bayar\" : \"[text]\",\n" +
//            "\"bunga\" : \"[text]\",\n" +
//            "\"tlpn_econ\" : \"[text]\",\n" +
//            "\"jns_pinjaman\" : \"[text]\",\n" +
//            "\"nominal_bayar_terakhir\" : \"[text]\",\n" +
//            "\"harus_bayar\" : \"[text]\",\n" +
//            "\"nominal_ptp\" : \"[text]\",\n" +
//            "\"saldo\" : \"[text]\",\n" +
//            "\"nama_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_mogen\" : \"[text]\",\n" +
//            "\"os_pinjaman\" : \"[text]\",\n" +
//            "\"nama_econ\" : \"[text]\",\n" +
//            "\"keberadaan_jaminan\" : \"[text]\",\n" +
//            "\"email\" : \"[text]\",\n" +
//            "\"kewajiban\" : \"[text]\",\n" +
//            "\"total_kewajiban\" : \"[text]\",\n" +
//            "\"nama_mogen\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\",\n" +
//            "\"nama_debitur\" : \"[text]\",\n" +
//            "\"tgl_jth_tempo\" : \"[text]\",\n" +
//            "\"no_tlpn\" : \"[text]\",\n" +
//            "\"total_bayar\" : \"[text]\",\n" +
//            "\"tgl_gajian\" : \"[text]\",\n" +
//            "\"pekerjaan\" : \"[text]\",\n" +
//            "\"almt_usaha\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"saldo_min\" : \"[text]\",\n" +
//            "\"dpd_today\" : \"[text]\",\n" +
//            "\"resume_nsbh\" : \"[text]\",\n" +
//            "\"almt_rumah\" : \"[text]\",\n" +
//            "\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"denda\" : \"[text]\",\n" +
//            "\"pola_bayar\" : \"[text]\",\n" +
//            "\"no_hp\" : \"[text]\",\n" +
//            "\"tgl_bayar_terakhir\" : \"[text]\",\n" +
//            "\"angsuran_ke\" : \"[text]\",\n" +
//            "\"nama_upliner\" : \"[text]\",\n" +
//            "\"pokok\" : \"[text]\",\n" +
//            "\"tgl_sp\" : \"[text]\",\n" +
//            "\"tenor\" : \"[text]\",\n" +
//            "\"angsuran\" : \"[text]\",\n" +
//            "\"gender\" : \"[text]\",\n" +
//            "\"tgl_ptp\" : \"[text]\",\n" +
//            "\"action_plan\" : \"[text]\",\n" +
//            "\"no_rekening\" : \"[text]\",\n" +
//            "\"histori_sp\" : \"[text]\",\n" +
//            "\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"tlpn_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_upliner\" : \"[text]\",\n" +
//            "\"sumber_bayar\" : \"[text]\",\n" +
//            "\"bunga\" : \"[text]\",\n" +
//            "\"tlpn_econ\" : \"[text]\",\n" +
//            "\"jns_pinjaman\" : \"[text]\",\n" +
//            "\"nominal_bayar_terakhir\" : \"[text]\",\n" +
//            "\"harus_bayar\" : \"[text]\",\n" +
//            "\"nominal_ptp\" : \"[text]\",\n" +
//            "\"saldo\" : \"[text]\",\n" +
//            "\"nama_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_mogen\" : \"[text]\",\n" +
//            "\"os_pinjaman\" : \"[text]\",\n" +
//            "\"nama_econ\" : \"[text]\",\n" +
//            "\"keberadaan_jaminan\" : \"[text]\",\n" +
//            "\"email\" : \"[text]\",\n" +
//            "\"kewajiban\" : \"[text]\",\n" +
//            "\"total_kewajiban\" : \"[text]\",\n" +
//            "\"nama_mogen\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\",\n" +
//            "\"nama_debitur\" : \"[text]\",\n" +
//            "\"tgl_jth_tempo\" : \"[text]\",\n" +
//            "\"no_tlpn\" : \"[text]\",\n" +
//            "\"total_bayar\" : \"[text]\",\n" +
//            "\"tgl_gajian\" : \"[text]\",\n" +
//            "\"pekerjaan\" : \"[text]\",\n" +
//            "\"almt_usaha\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"saldo_min\" : \"[text]\",\n" +
//            "\"dpd_today\" : \"[text]\",\n" +
//            "\"resume_nsbh\" : \"[text]\",\n" +
//            "\"almt_rumah\" : \"[text]\",\n" +
//            "\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"denda\" : \"[text]\",\n" +
//            "\"pola_bayar\" : \"[text]\",\n" +
//            "\"no_hp\" : \"[text]\",\n" +
//            "\"tgl_bayar_terakhir\" : \"[text]\",\n" +
//            "\"angsuran_ke\" : \"[text]\",\n" +
//            "\"nama_upliner\" : \"[text]\",\n" +
//            "\"pokok\" : \"[text]\",\n" +
//            "\"tgl_sp\" : \"[text]\",\n" +
//            "\"tenor\" : \"[text]\",\n" +
//            "\"angsuran\" : \"[text]\",\n" +
//            "\"gender\" : \"[text]\",\n" +
//            "\"tgl_ptp\" : \"[text]\",\n" +
//            "\"action_plan\" : \"[text]\",\n" +
//            "\"no_rekening\" : \"[text]\",\n" +
//            "\"histori_sp\" : \"[text]\",\n" +
//            "\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"tlpn_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_upliner\" : \"[text]\",\n" +
//            "\"sumber_bayar\" : \"[text]\",\n" +
//            "\"bunga\" : \"[text]\",\n" +
//            "\"tlpn_econ\" : \"[text]\",\n" +
//            "\"jns_pinjaman\" : \"[text]\",\n" +
//            "\"nominal_bayar_terakhir\" : \"[text]\",\n" +
//            "\"harus_bayar\" : \"[text]\",\n" +
//            "\"nominal_ptp\" : \"[text]\",\n" +
//            "\"saldo\" : \"[text]\",\n" +
//            "\"nama_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_mogen\" : \"[text]\",\n" +
//            "\"os_pinjaman\" : \"[text]\",\n" +
//            "\"nama_econ\" : \"[text]\",\n" +
//            "\"keberadaan_jaminan\" : \"[text]\",\n" +
//            "\"email\" : \"[text]\",\n" +
//            "\"kewajiban\" : \"[text]\",\n" +
//            "\"total_kewajiban\" : \"[text]\",\n" +
//            "\"nama_mogen\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\",\n" +
//            "\"nama_debitur\" : \"[text]\",\n" +
//            "\"tgl_jth_tempo\" : \"[text]\",\n" +
//            "\"no_tlpn\" : \"[text]\",\n" +
//            "\"total_bayar\" : \"[text]\",\n" +
//            "\"tgl_gajian\" : \"[text]\",\n" +
//            "\"pekerjaan\" : \"[text]\",\n" +
//            "\"almt_usaha\" : \"[text]\"\n" +
//            "}\n" +
//            "],\n" +
//            "\"jarak\" : \"[text3]\",\n" +
//            "\"OS\" : \"[text3]\",\n" +
//            "\"nama\" : \"[text3]\",\n" +
//            "\"alamat\" : \"[text3]\"\n" +
//            "}\n" +
//            "],\n" +
//            "\"ss\" : [{\n" +
//            "\"no_loan\" : \"[Text]\"\n" +
//            "}, {\n" +
//            "\"no_loan\" : \"[Text]\"\n" +
//            "}\n" +
//            "],\n" +
//            "\"bi\" : [{\n" +
//            "\"no_loan\" : \"[Text]\"\n" +
//            "}, {\n" +
//            "\"no_loan\" : \"[Text]\"\n" +
//            "}\n" +
//            "],\n" +
//            "\"bs\" : [{\n" +
//            "\"no_loan\" : \"[Text]\"\n" +
//            "}, {\n" +
//            "\"no_loan\" : \"[Text]\"\n" +
//            "}\n" +
//            "],\n" +
//            "\"blbs\" : [{\n" +
//            "\"no_loan\" : \"[Text]\"\n" +
//            "}, {\n" +
//            "\"no_loan\" : \"[Text]\"\n" +
//            "}\n" +
//            "]\n" +
//            "},\n" +
//            "\"penagihan\" : {\n" +
//            "\"npay\" : [{\n" +
//            "\"no_loan\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"no_loan\" : \"[text]\"\n" +
//            "}\n" +
//            "],\n" +
//            "\"fpay\" : [{\n" +
//            "\"tgl_kunjungan\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"tgl_kunjungan\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"tgl_kunjungan\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"tgl_kunjungan\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"tgl_kunjungan\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\"\n" +
//            "}\n" +
//            "],\n" +
//            "\"nvst\" : [{\n" +
//            "\"no_loan\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"no_loan\" : \"[text]\"\n" +
//            "}\n" +
//            "],\n" +
//            "\"ppay\" : [{\n" +
//            "\"tgl_kunjungan\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"tgl_kunjungan\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"tgl_kunjungan\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"tgl_kunjungan\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"tgl_kunjungan\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\"\n" +
//            "}\n" +
//            "],\n" +
//            "\"tgt\" : [{\n" +
//            "\"cycle\" : \"[text3]\",\n" +
//            "\"detail_inventory\" : [{\n" +
//            "\"saldo_min\" : \"[text]\",\n" +
//            "\"dpd_today\" : \"[text]\",\n" +
//            "\"resume_nsbh\" : \"[text]\",\n" +
//            "\"almt_rumah\" : \"[text]\",\n" +
//            "\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"denda\" : \"[text]\",\n" +
//            "\"pola_bayar\" : \"[text]\",\n" +
//            "\"no_hp\" : \"[text]\",\n" +
//            "\"tgl_bayar_terakhir\" : \"[text]\",\n" +
//            "\"angsuran_ke\" : \"[text]\",\n" +
//            "\"nama_upliner\" : \"[text]\",\n" +
//            "\"pokok\" : \"[text]\",\n" +
//            "\"tgl_sp\" : \"[text]\",\n" +
//            "\"tenor\" : \"[text]\",\n" +
//            "\"angsuran\" : \"[text]\",\n" +
//            "\"gender\" : \"[text]\",\n" +
//            "\"tgl_ptp\" : \"[text]\",\n" +
//            "\"action_plan\" : \"[text]\",\n" +
//            "\"no_rekening\" : \"[text]\",\n" +
//            "\"histori_sp\" : \"[text]\",\n" +
//            "\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"tlpn_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_upliner\" : \"[text]\",\n" +
//            "\"sumber_bayar\" : \"[text]\",\n" +
//            "\"bunga\" : \"[text]\",\n" +
//            "\"tlpn_econ\" : \"[text]\",\n" +
//            "\"jns_pinjaman\" : \"[text]\",\n" +
//            "\"nominal_bayar_terakhir\" : \"[text]\",\n" +
//            "\"harus_bayar\" : \"[text]\",\n" +
//            "\"nominal_ptp\" : \"[text]\",\n" +
//            "\"saldo\" : \"[text]\",\n" +
//            "\"nama_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_mogen\" : \"[text]\",\n" +
//            "\"os_pinjaman\" : \"[text]\",\n" +
//            "\"nama_econ\" : \"[text]\",\n" +
//            "\"keberadaan_jaminan\" : \"[text]\",\n" +
//            "\"email\" : \"[text]\",\n" +
//            "\"kewajiban\" : \"[text]\",\n" +
//            "\"total_kewajiban\" : \"[text]\",\n" +
//            "\"nama_mogen\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\",\n" +
//            "\"nama_debitur\" : \"[text]\",\n" +
//            "\"tgl_jth_tempo\" : \"[text]\",\n" +
//            "\"no_tlpn\" : \"[text]\",\n" +
//            "\"total_bayar\" : \"[text]\",\n" +
//            "\"tgl_gajian\" : \"[text]\",\n" +
//            "\"pekerjaan\" : \"[text]\",\n" +
//            "\"almt_usaha\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"saldo_min\" : \"[text]\",\n" +
//            "\"dpd_today\" : \"[text]\",\n" +
//            "\"resume_nsbh\" : \"[text]\",\n" +
//            "\"almt_rumah\" : \"[text]\",\n" +
//            "\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"denda\" : \"[text]\",\n" +
//            "\"pola_bayar\" : \"[text]\",\n" +
//            "\"no_hp\" : \"[text]\",\n" +
//            "\"tgl_bayar_terakhir\" : \"[text]\",\n" +
//            "\"angsuran_ke\" : \"[text]\",\n" +
//            "\"nama_upliner\" : \"[text]\",\n" +
//            "\"pokok\" : \"[text]\",\n" +
//            "\"tgl_sp\" : \"[text]\",\n" +
//            "\"tenor\" : \"[text]\",\n" +
//            "\"angsuran\" : \"[text]\",\n" +
//            "\"gender\" : \"[text]\",\n" +
//            "\"tgl_ptp\" : \"[text]\",\n" +
//            "\"action_plan\" : \"[text]\",\n" +
//            "\"no_rekening\" : \"[text]\",\n" +
//            "\"histori_sp\" : \"[text]\",\n" +
//            "\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"tlpn_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_upliner\" : \"[text]\",\n" +
//            "\"sumber_bayar\" : \"[text]\",\n" +
//            "\"bunga\" : \"[text]\",\n" +
//            "\"tlpn_econ\" : \"[text]\",\n" +
//            "\"jns_pinjaman\" : \"[text]\",\n" +
//            "\"nominal_bayar_terakhir\" : \"[text]\",\n" +
//            "\"harus_bayar\" : \"[text]\",\n" +
//            "\"nominal_ptp\" : \"[text]\",\n" +
//            "\"saldo\" : \"[text]\",\n" +
//            "\"nama_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_mogen\" : \"[text]\",\n" +
//            "\"os_pinjaman\" : \"[text]\",\n" +
//            "\"nama_econ\" : \"[text]\",\n" +
//            "\"keberadaan_jaminan\" : \"[text]\",\n" +
//            "\"email\" : \"[text]\",\n" +
//            "\"kewajiban\" : \"[text]\",\n" +
//            "\"total_kewajiban\" : \"[text]\",\n" +
//            "\"nama_mogen\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\",\n" +
//            "\"nama_debitur\" : \"[text]\",\n" +
//            "\"tgl_jth_tempo\" : \"[text]\",\n" +
//            "\"no_tlpn\" : \"[text]\",\n" +
//            "\"total_bayar\" : \"[text]\",\n" +
//            "\"tgl_gajian\" : \"[text]\",\n" +
//            "\"pekerjaan\" : \"[text]\",\n" +
//            "\"almt_usaha\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"saldo_min\" : \"[text]\",\n" +
//            "\"dpd_today\" : \"[text]\",\n" +
//            "\"resume_nsbh\" : \"[text]\",\n" +
//            "\"almt_rumah\" : \"[text]\",\n" +
//            "\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"denda\" : \"[text]\",\n" +
//            "\"pola_bayar\" : \"[text]\",\n" +
//            "\"no_hp\" : \"[text]\",\n" +
//            "\"tgl_bayar_terakhir\" : \"[text]\",\n" +
//            "\"angsuran_ke\" : \"[text]\",\n" +
//            "\"nama_upliner\" : \"[text]\",\n" +
//            "\"pokok\" : \"[text]\",\n" +
//            "\"tgl_sp\" : \"[text]\",\n" +
//            "\"tenor\" : \"[text]\",\n" +
//            "\"angsuran\" : \"[text]\",\n" +
//            "\"gender\" : \"[text]\",\n" +
//            "\"tgl_ptp\" : \"[text]\",\n" +
//            "\"action_plan\" : \"[text]\",\n" +
//            "\"no_rekening\" : \"[text]\",\n" +
//            "\"histori_sp\" : \"[text]\",\n" +
//            "\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"tlpn_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_upliner\" : \"[text]\",\n" +
//            "\"sumber_bayar\" : \"[text]\",\n" +
//            "\"bunga\" : \"[text]\",\n" +
//            "\"tlpn_econ\" : \"[text]\",\n" +
//            "\"jns_pinjaman\" : \"[text]\",\n" +
//            "\"nominal_bayar_terakhir\" : \"[text]\",\n" +
//            "\"harus_bayar\" : \"[text]\",\n" +
//            "\"nominal_ptp\" : \"[text]\",\n" +
//            "\"saldo\" : \"[text]\",\n" +
//            "\"nama_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_mogen\" : \"[text]\",\n" +
//            "\"os_pinjaman\" : \"[text]\",\n" +
//            "\"nama_econ\" : \"[text]\",\n" +
//            "\"keberadaan_jaminan\" : \"[text]\",\n" +
//            "\"email\" : \"[text]\",\n" +
//            "\"kewajiban\" : \"[text]\",\n" +
//            "\"total_kewajiban\" : \"[text]\",\n" +
//            "\"nama_mogen\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\",\n" +
//            "\"nama_debitur\" : \"[text]\",\n" +
//            "\"tgl_jth_tempo\" : \"[text]\",\n" +
//            "\"no_tlpn\" : \"[text]\",\n" +
//            "\"total_bayar\" : \"[text]\",\n" +
//            "\"tgl_gajian\" : \"[text]\",\n" +
//            "\"pekerjaan\" : \"[text]\",\n" +
//            "\"almt_usaha\" : \"[text]\"\n" +
//            "}\n" +
//            "],\n" +
//            "\"jarak\" : \"[text3]\",\n" +
//            "\"OS\" : \"[text3]\",\n" +
//            "\"nama\" : \"[text3]\",\n" +
//            "\"alamat\" : \"[text3]\"\n" +
//            "}, {\n" +
//            "\"cycle\" : \"[text3]\",\n" +
//            "\"detail_inventory\" : [{\n" +
//            "\"saldo_min\" : \"[text]\",\n" +
//            "\"dpd_today\" : \"[text]\",\n" +
//            "\"resume_nsbh\" : \"[text]\",\n" +
//            "\"almt_rumah\" : \"[text]\",\n" +
//            "\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"denda\" : \"[text]\",\n" +
//            "\"pola_bayar\" : \"[text]\",\n" +
//            "\"no_hp\" : \"[text]\",\n" +
//            "\"tgl_bayar_terakhir\" : \"[text]\",\n" +
//            "\"angsuran_ke\" : \"[text]\",\n" +
//            "\"nama_upliner\" : \"[text]\",\n" +
//            "\"pokok\" : \"[text]\",\n" +
//            "\"tgl_sp\" : \"[text]\",\n" +
//            "\"tenor\" : \"[text]\",\n" +
//            "\"angsuran\" : \"[text]\",\n" +
//            "\"gender\" : \"[text]\",\n" +
//            "\"tgl_ptp\" : \"[text]\",\n" +
//            "\"action_plan\" : \"[text]\",\n" +
//            "\"no_rekening\" : \"[text]\",\n" +
//            "\"histori_sp\" : \"[text]\",\n" +
//            "\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"tlpn_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_upliner\" : \"[text]\",\n" +
//            "\"sumber_bayar\" : \"[text]\",\n" +
//            "\"bunga\" : \"[text]\",\n" +
//            "\"tlpn_econ\" : \"[text]\",\n" +
//            "\"jns_pinjaman\" : \"[text]\",\n" +
//            "\"nominal_bayar_terakhir\" : \"[text]\",\n" +
//            "\"harus_bayar\" : \"[text]\",\n" +
//            "\"nominal_ptp\" : \"[text]\",\n" +
//            "\"saldo\" : \"[text]\",\n" +
//            "\"nama_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_mogen\" : \"[text]\",\n" +
//            "\"os_pinjaman\" : \"[text]\",\n" +
//            "\"nama_econ\" : \"[text]\",\n" +
//            "\"keberadaan_jaminan\" : \"[text]\",\n" +
//            "\"email\" : \"[text]\",\n" +
//            "\"kewajiban\" : \"[text]\",\n" +
//            "\"total_kewajiban\" : \"[text]\",\n" +
//            "\"nama_mogen\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\",\n" +
//            "\"nama_debitur\" : \"[text]\",\n" +
//            "\"tgl_jth_tempo\" : \"[text]\",\n" +
//            "\"no_tlpn\" : \"[text]\",\n" +
//            "\"total_bayar\" : \"[text]\",\n" +
//            "\"tgl_gajian\" : \"[text]\",\n" +
//            "\"pekerjaan\" : \"[text]\",\n" +
//            "\"almt_usaha\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"saldo_min\" : \"[text]\",\n" +
//            "\"dpd_today\" : \"[text]\",\n" +
//            "\"resume_nsbh\" : \"[text]\",\n" +
//            "\"almt_rumah\" : \"[text]\",\n" +
//            "\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"denda\" : \"[text]\",\n" +
//            "\"pola_bayar\" : \"[text]\",\n" +
//            "\"no_hp\" : \"[text]\",\n" +
//            "\"tgl_bayar_terakhir\" : \"[text]\",\n" +
//            "\"angsuran_ke\" : \"[text]\",\n" +
//            "\"nama_upliner\" : \"[text]\",\n" +
//            "\"pokok\" : \"[text]\",\n" +
//            "\"tgl_sp\" : \"[text]\",\n" +
//            "\"tenor\" : \"[text]\",\n" +
//            "\"angsuran\" : \"[text]\",\n" +
//            "\"gender\" : \"[text]\",\n" +
//            "\"tgl_ptp\" : \"[text]\",\n" +
//            "\"action_plan\" : \"[text]\",\n" +
//            "\"no_rekening\" : \"[text]\",\n" +
//            "\"histori_sp\" : \"[text]\",\n" +
//            "\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"tlpn_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_upliner\" : \"[text]\",\n" +
//            "\"sumber_bayar\" : \"[text]\",\n" +
//            "\"bunga\" : \"[text]\",\n" +
//            "\"tlpn_econ\" : \"[text]\",\n" +
//            "\"jns_pinjaman\" : \"[text]\",\n" +
//            "\"nominal_bayar_terakhir\" : \"[text]\",\n" +
//            "\"harus_bayar\" : \"[text]\",\n" +
//            "\"nominal_ptp\" : \"[text]\",\n" +
//            "\"saldo\" : \"[text]\",\n" +
//            "\"nama_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_mogen\" : \"[text]\",\n" +
//            "\"os_pinjaman\" : \"[text]\",\n" +
//            "\"nama_econ\" : \"[text]\",\n" +
//            "\"keberadaan_jaminan\" : \"[text]\",\n" +
//            "\"email\" : \"[text]\",\n" +
//            "\"kewajiban\" : \"[text]\",\n" +
//            "\"total_kewajiban\" : \"[text]\",\n" +
//            "\"nama_mogen\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\",\n" +
//            "\"nama_debitur\" : \"[text]\",\n" +
//            "\"tgl_jth_tempo\" : \"[text]\",\n" +
//            "\"no_tlpn\" : \"[text]\",\n" +
//            "\"total_bayar\" : \"[text]\",\n" +
//            "\"tgl_gajian\" : \"[text]\",\n" +
//            "\"pekerjaan\" : \"[text]\",\n" +
//            "\"almt_usaha\" : \"[text]\"\n" +
//            "}, {\n" +
//            "\"saldo_min\" : \"[text]\",\n" +
//            "\"dpd_today\" : \"[text]\",\n" +
//            "\"resume_nsbh\" : \"[text]\",\n" +
//            "\"almt_rumah\" : \"[text]\",\n" +
//            "\"tgl_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"denda\" : \"[text]\",\n" +
//            "\"pola_bayar\" : \"[text]\",\n" +
//            "\"no_hp\" : \"[text]\",\n" +
//            "\"tgl_bayar_terakhir\" : \"[text]\",\n" +
//            "\"angsuran_ke\" : \"[text]\",\n" +
//            "\"nama_upliner\" : \"[text]\",\n" +
//            "\"pokok\" : \"[text]\",\n" +
//            "\"tgl_sp\" : \"[text]\",\n" +
//            "\"tenor\" : \"[text]\",\n" +
//            "\"angsuran\" : \"[text]\",\n" +
//            "\"gender\" : \"[text]\",\n" +
//            "\"tgl_ptp\" : \"[text]\",\n" +
//            "\"action_plan\" : \"[text]\",\n" +
//            "\"no_rekening\" : \"[text]\",\n" +
//            "\"histori_sp\" : \"[text]\",\n" +
//            "\"nominal_janji_bayar_terakhir\" : \"[text]\",\n" +
//            "\"tlpn_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_upliner\" : \"[text]\",\n" +
//            "\"sumber_bayar\" : \"[text]\",\n" +
//            "\"bunga\" : \"[text]\",\n" +
//            "\"tlpn_econ\" : \"[text]\",\n" +
//            "\"jns_pinjaman\" : \"[text]\",\n" +
//            "\"nominal_bayar_terakhir\" : \"[text]\",\n" +
//            "\"harus_bayar\" : \"[text]\",\n" +
//            "\"nominal_ptp\" : \"[text]\",\n" +
//            "\"saldo\" : \"[text]\",\n" +
//            "\"nama_rekomendator\" : \"[text]\",\n" +
//            "\"tlpn_mogen\" : \"[text]\",\n" +
//            "\"os_pinjaman\" : \"[text]\",\n" +
//            "\"nama_econ\" : \"[text]\",\n" +
//            "\"keberadaan_jaminan\" : \"[text]\",\n" +
//            "\"email\" : \"[text]\",\n" +
//            "\"kewajiban\" : \"[text]\",\n" +
//            "\"total_kewajiban\" : \"[text]\",\n" +
//            "\"nama_mogen\" : \"[text]\",\n" +
//            "\"no_loan\" : \"[text]\",\n" +
//            "\"nama_debitur\" : \"[text]\",\n" +
//            "\"tgl_jth_tempo\" : \"[text]\",\n" +
//            "\"no_tlpn\" : \"[text]\",\n" +
//            "\"total_bayar\" : \"[text]\",\n" +
//            "\"tgl_gajian\" : \"[text]\",\n" +
//            "\"pekerjaan\" : \"[text]\",\n" +
//            "\"almt_usaha\" : \"[text]\"\n" +
//            "}\n" +
//            "],\n" +
//            "\"jarak\" : \"[text3]\",\n" +
//            "\"OS\" : \"[text3]\",\n" +
//            "\"nama\" : \"[text3]\",\n" +
//            "\"alamat\" : \"[text3]\"\n" +
//            "}\n" +
//            "]\n" +
//            "},\n" +
//            "\"rc\" : \"00\"\n" +
//            "}";

//    public static String contohJson="{\"waktu\":\"[TEXT]\",\"keterangan\":\"Sukses Download Data Tagihan\",\"survey\":{\"ss\":[{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"}],\"bi\":[{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"}],\"bs\":[{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"}],\"blbs\":[{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"}]},\"penagihan\":[[{\"npay\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"fpay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"nvst\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"ppay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"tgt\":[{\"cycle\":\"[text3]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text3]\",\"OS\":\"[text3]\",\"nama\":\"[text3]\",\"alamat\":\"[text3]\"},{\"cycle\":\"[text3]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text3]\",\"OS\":\"[text3]\",\"nama\":\"[text3]\",\"alamat\":\"[text3]\"}]},\"survey_bulan_kemarin\"],[{\"npay\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"fpay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"nvst\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"ppay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"tgt\":[{\"cycle\":\"[text3]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text3]\",\"OS\":\"[text3]\",\"nama\":\"[text3]\",\"alamat\":\"[text3]\"},{\"cycle\":\"[text3]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text3]\",\"OS\":\"[text3]\",\"nama\":\"[text3]\",\"alamat\":\"[text3]\"}]},\"survey_bulan_ini\"]],\"rc\":\"00\"}";


//    public static String contohJson="{\"waktu\":\"[TEXT]\",\"keterangan\":\"Sukses Download Data Tagihan\",\"survey\":{\"ss\":[{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"}],\"bi\":[{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"}],\"bs\":[{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"}],\"blbs\":[{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"},{\"no_loan\":\"[Text]\"}]},\"penagihan\":[[{\"npay\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"fpay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"nvst\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"ppay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"tgt\":[{\"cycle\":\"[text]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\"},{\"cycle\":\"[text2]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text2]\",\"OS\":\"[text2]\",\"nama\":\"[text2]\",\"alamat\":\"[text2]\"},{\"cycle\":\"[text3]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text3]\",\"OS\":\"[text3]\",\"nama\":\"[text3]\",\"alamat\":\"[text3]\"}]},\"surveyBulanKemarin\"],[{\"npay\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"fpay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"nvst\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"ppay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"tgt\":[{\"cycle\":\"[text]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\"},{\"cycle\":\"[text2]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text2]\",\"OS\":\"[text2]\",\"nama\":\"[text2]\",\"alamat\":\"[text2]\"},{\"cycle\":\"[text3]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text3]\",\"OS\":\"[text3]\",\"nama\":\"[text3]\",\"alamat\":\"[text3]\"}]},\"surveyBulanIni\"],[{\"npay\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"fpay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"nvst\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"ppay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"tgt\":[{\"cycle\":\"[text]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\"},{\"cycle\":\"[text2]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text2]\",\"OS\":\"[text2]\",\"nama\":\"[text2]\",\"alamat\":\"[text2]\"},{\"cycle\":\"[text3]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text3]\",\"OS\":\"[text3]\",\"nama\":\"[text3]\",\"alamat\":\"[text3]\"}]},\"current\"],[{\"npay\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"fpay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"nvst\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"ppay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"tgt\":[{\"cycle\":\"[text]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\"},{\"cycle\":\"[text2]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text2]\",\"OS\":\"[text2]\",\"nama\":\"[text2]\",\"alamat\":\"[text2]\"},{\"cycle\":\"[text3]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text3]\",\"OS\":\"[text3]\",\"nama\":\"[text3]\",\"alamat\":\"[text3]\"}]},\"1-30\"],[{\"npay\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"fpay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"nvst\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"ppay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"tgt\":[{\"cycle\":\"[text]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\"},{\"cycle\":\"[text2]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text2]\",\"OS\":\"[text2]\",\"nama\":\"[text2]\",\"alamat\":\"[text2]\"},{\"cycle\":\"[text3]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text3]\",\"OS\":\"[text3]\",\"nama\":\"[text3]\",\"alamat\":\"[text3]\"}]},\"31-60\"],[{\"npay\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"fpay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"nvst\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"ppay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"tgt\":[{\"cycle\":\"[text]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\"},{\"cycle\":\"[text2]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text2]\",\"OS\":\"[text2]\",\"nama\":\"[text2]\",\"alamat\":\"[text2]\"},{\"cycle\":\"[text3]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text3]\",\"OS\":\"[text3]\",\"nama\":\"[text3]\",\"alamat\":\"[text3]\"}]},\"61-90\"],[{\"npay\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"fpay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"nvst\":[{\"no_loan\":\"[text]\"},{\"no_loan\":\"[text]\"}],\"ppay\":[{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"},{\"tgl_kunjungan\":\"[text]\",\"no_loan\":\"[text]\"}],\"tgt\":[{\"cycle\":\"[text]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\"},{\"cycle\":\"[text2]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text2]\",\"OS\":\"[text2]\",\"nama\":\"[text2]\",\"alamat\":\"[text2]\"},{\"cycle\":\"[text3]\",\"detail_inventory\":[{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"total_bayar\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"}],\"jarak\":\"[text3]\",\"OS\":\"[text3]\",\"nama\":\"[text3]\",\"alamat\":\"[text3]\"}]},\"91- ~\"]],\"rc\":\"00\"}";

//    public static String contohJson = "{\"waktu\":\"[TEXT]\",\"keterangan\":\"Sukses Download Data Tagihan\",\"penagihan\":[[{\"npay\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"fpay\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"},{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"nvst\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"ppay\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"tgt\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}]},\"current\"],[{\"npay\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"fpay\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"},{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"nvst\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"ppay\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"tgt\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}]},\"1-30\"],[{\"npay\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"fpay\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"},{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"nvst\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"ppay\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"tgt\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}]},\"31-60\"],[{\"npay\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"fpay\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"},{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"nvst\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"ppay\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"tgt\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}]},\"61-90\"],[{\"npay\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"fpay\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"},{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"nvst\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"ppay\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}],\"tgt\":[{\"cycle\":\"[text]\",\"detail_inventory\":{\"saldo_min\":\"[text]\",\"dpd_today\":\"[text]\",\"resume_nsbh\":\"[text]\",\"almt_rumah\":\"[text]\",\"tgl_janji_bayar_terakhir\":\"[text]\",\"denda\":\"[text]\",\"pola_bayar\":\"[text]\",\"no_hp\":\"[text]\",\"tgl_bayar_terakhir\":\"[text]\",\"angsuran_ke\":\"[text]\",\"nama_upliner\":\"[text]\",\"pokok\":\"[text]\",\"tgl_sp\":\"[text]\",\"tenor\":\"[text]\",\"angsuran\":\"[text]\",\"gender\":\"[text]\",\"tgl_ptp\":\"[text]\",\"action_plan\":\"[text]\",\"no_rekening\":\"[text]\",\"histori_sp\":\"[text]\",\"nominal_janji_bayar_terakhir\":\"[text]\",\"tlpn_rekomendator\":\"[text]\",\"tlpn_upliner\":\"[text]\",\"sumber_bayar\":\"[text]\",\"bunga\":\"[text]\",\"tlpn_econ\":\"[text]\",\"jns_pinjaman\":\"[text]\",\"nominal_bayar_terakhir\":\"[text]\",\"harus_bayar\":\"[text]\",\"nominal_ptp\":\"[text]\",\"saldo\":\"[text]\",\"nama_rekomendator\":\"[text]\",\"tlpn_mogen\":\"[text]\",\"os_pinjaman\":\"[text]\",\"nama_econ\":\"[text]\",\"keberadaan_jaminan\":\"[text]\",\"email\":\"[text]\",\"kewajiban\":\"[text]\",\"total_kewajiban\":\"[text]\",\"nama_mogen\":\"[text]\",\"no_loan\":\"[text]\",\"nama_debitur\":\"[text]\",\"tgl_jth_tempo\":\"[text]\",\"no_tlpn\":\"[text]\",\"tgl_gajian\":\"[text]\",\"pekerjaan\":\"[text]\",\"almt_usaha\":\"[text]\"},\"jarak\":\"[text]\",\"OS\":\"[text]\",\"nama\":\"[text]\",\"alamat\":\"[text]\",\"total_bayar\":\"[text]\"}]},\"91- ~\"]],\"rc\":\"00\"}";


}
