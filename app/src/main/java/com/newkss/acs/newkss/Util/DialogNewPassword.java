package com.newkss.acs.newkss.Util;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.newkss.acs.newkss.Menu.MenuLogin;
import com.newkss.acs.newkss.R;

/**
 * Created by acs on 7/27/17.
 */

public class DialogNewPassword extends DialogFragment {

    Button btnOK;
    EditText etPassword;
    int width = 450;
    int height = 350;

    String value = "";

    private OnCompleteListener mListener;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (OnCompleteListener) activity;
        } catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.konfirmasipassword, container);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        btnOK = (Button) view.findViewById(R.id.btnOK);
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ((MenuLogin)getActivity()).execLogin.execute();
                value = etPassword.getText().toString();
                mListener.onComplete(value);
                dismissAllowingStateLoss();

            }
        });
        return view;
    }



    public static DialogNewPassword newInstance() {
        DialogNewPassword s = new DialogNewPassword();
        return s;
    }

    public static DialogNewPassword newInstance1(String val) {
        DialogNewPassword s = new DialogNewPassword();
        return s;
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setLayout(width, height);
        window.setGravity(Gravity.CENTER);
    }


}
