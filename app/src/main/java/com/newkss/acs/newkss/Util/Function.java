package com.newkss.acs.newkss.Util;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;


import com.newkss.acs.newkss.Model.Config;
import com.newkss.acs.newkss.Service.AlarmSendOfflineData;
import com.newkss.acs.newkss.test.AlarmReceiver;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by acs on 5/19/17.
 */

public class Function {

    public static void generateNoteOnSD(Context context, String sFileName, String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "Notes");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
            //Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deleteImage() {
        try {
            File fileImage = new File(Environment.getExternalStorageDirectory() + File.separator + "KSP" + File.separator);
            System.out.println(fileImage.toString());
            if (fileImage.exists()) {
                if (fileImage.isDirectory()) {
                    for (File c : fileImage.listFiles()) {
                        System.out.println("Delete " + c.toString());
                        c.delete();
                    }
                }
                System.out.println("ada");
            } else {
                System.out.println("tidak ada");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getWaktuSekarangMilis() {
        Date datenow = new Date();
        //System.out.println("Waktu sekarang: " + datenow.getTime());
        return String.valueOf(datenow.getTime());
    }

    public static String getImei(Context context) {
//        String imei = "359896077580445";
        String imei="";
        TelephonyManager telemamanger = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        imei = telemamanger.getDeviceId();
        //System.out.println("Nilai Imei: " + imei);
        return imei;
    }

    public static byte[] encodeBase64(String str) {
        return Base64.encode(str.getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);
    }


    public static byte[] decodeBase64(byte[] bytesEncoded) {
        return Base64.decode(bytesEncoded, Base64.URL_SAFE | Base64.NO_WRAP);
    }

    public static void showAlert(Context mContext, String result) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(Constant.MESSAGE);
        builder.setMessage(result);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setPositiveButton(Constant.MESSAGE_OK, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog alertLogin = builder.create();
        alertLogin.show();
    }


    public static String getHeader(Context context, String nik) {
        return Function.getImei(context) + ":" + nik + ":" + Function.getWaktuSekarangMilis();
    }

    public static String compareDate(String dateServer, String dateOffline) {
        String hasil = "";
        DateFormat d1 = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        DateFormat d2 = new SimpleDateFormat("dd-MM-yyyy");

        Date waktuserver = null;
        Date waktuoffline = null;

        try {
            if (dateOffline.isEmpty()) {
                hasil = "beda";
                return hasil;
            } else {
                waktuserver = d1.parse(dateServer);
                waktuoffline = d1.parse(dateOffline);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        String wServer = d2.format(waktuserver);
        String wOffline = d2.format(waktuoffline);

        System.out.println("compareDate: " + wServer + " --- " + wOffline);
        if (wServer.equals(wOffline)) {
            hasil = "sama";
        } else {
            hasil = "beda";
        }

        return hasil;

    }

    public static String compareDateDataSettlement(ArrayList<String> list, String dateSystem) {
        String hasil = "";
        DateFormat d1 = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        DateFormat d2 = new SimpleDateFormat("dd-MM-yyyy");

        System.out.println("Date SYSTEM: " + dateSystem);
        Date waktuSistem = null;
        int i = 0;

        if (!list.isEmpty()) {
            try {
                waktuSistem = d2.parse(dateSystem);
                System.out.println("Waktu Sistem " + dateSystem);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            for (String ss : list) {
                System.out.println(ss + " --- " + i);
                i++;
                Date waktuSettle = null;

                try {
//                    if (ss != null) {
                    waktuSettle = d1.parse(ss);
                    String wSettle = d2.format(waktuSettle);
                    String wSystem = d2.format(waktuSistem);

                    System.out.println("compareDateDataSettlement: " + wSettle + " --- " + wSystem);

                    if (!wSettle.equals(wSystem)) {
                        return "BEDA";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return "SAMA";
        }
        return "SAMA";
    }


    public static String getDateNow() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        return dateFormat.format(date);
    }

    public static String convertStringtoMD5(String text) {
        StringBuffer sb = new StringBuffer();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(text.getBytes());

            byte byteData[] = md.digest();

            //convert the byte to hex format method 1

            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            System.out.println("Digest(in hex format):: " + sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();

    }

    public static String generateRandomCode() {
        return getdatetime() + generateRandom(9999, 1000);
    }

    public static String getdatetime() {
        Date datenow = new Date();
        //System.out.println("Waktu sekarang: " + datenow.getTime());
        return String.valueOf(datenow.getTime());
    }

    public static int generateRandom(int max, int min) {
        int range = (max - min) + 1;
        return (int) (Math.random() * range) + min;
    }

    public static String bitmapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public static Bitmap bitmapFromPath(String path) {
        Bitmap b = BitmapFactory.decodeFile(path);
        Bitmap out = Bitmap.createScaledBitmap(b, 320, 480, false);
        return out;
    }

    public static String removeSeparatorComa(String n) {
        String s = n.replace(",", "");
        return s;
    }

    public static String removeVarActionCode(String n) {
        String s = n.replace(",", "").replace("{", "").replace("}", "").replace("\"", "").replace("\\", "");
        return s;

    }

    public static String returnSeparatorComa(String value) {
        try {
            if (value.equals("0.0")) {
                return "0";
            } else if (value.equals(".00")) {
                return "0";
            } else if (value.equals("0.00")) {
                return "0";
            } else if (value.equals("0")) {
                return "0";
            } else if (value.equals("")) {
                return "0";
            }
//            NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
//            DecimalFormat df = (DecimalFormat)nf;
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.ENGLISH);
            otherSymbols.setDecimalSeparator('.');
            otherSymbols.setGroupingSeparator(',');
            double d = Double.parseDouble(value);
//            DecimalFormat decimalFormat = new DecimalFormat("#.00");
            DecimalFormat decimalFormat = new DecimalFormat("#.00", otherSymbols);
            decimalFormat.setGroupingUsed(true);
            decimalFormat.setGroupingSize(3);
            System.out.println("tes " + decimalFormat.format(d));
            return decimalFormat.format(d);
            //return String.format("%1$,.2f", d);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "0";
    }


    public static String returnSeparatorComaWithout(String value) {
        try {
            if (value.equals("0.0")) {
                return "0";
            } else if (value.equals(".00")) {
                return "0";
            } else if (value.equals("0.00")) {
                return "0";
            } else if (value.equals("0")) {
                return "0";
            } else if (value.equals("")) {
                return "0";
            }
//            NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
//            DecimalFormat df = (DecimalFormat)nf;
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.ENGLISH);
            otherSymbols.setDecimalSeparator('.');
            otherSymbols.setGroupingSeparator(',');
            double d = Double.parseDouble(value);
//            DecimalFormat decimalFormat = new DecimalFormat("#.00");
            DecimalFormat decimalFormat = new DecimalFormat("#", otherSymbols);
            decimalFormat.setGroupingUsed(true);
            decimalFormat.setGroupingSize(3);
            System.out.println("tes " + decimalFormat.format(d));
            return decimalFormat.format(d);
            //return String.format("%1$,.2f", d);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "0";
    }

    public static void triggerAlarm(Context mContext) {
        try {
            System.out.println("Masuk triggerAlarmSendLocation");
            Config queryConfig = new Config();
            String value = queryConfig.getValuePeriodikTrack();
            if (value.equals(null) || value.isEmpty()) {
                value = "1";
            } else {
                value = queryConfig.getValuePeriodikTrack();
            }
            AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(mContext, AlarmReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), Integer.parseInt(value)* 60000,
                    pendingIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void triggerAlarmSendOfflineData(Context mContext) {
        try {
            System.out.println("Masuk triggerAlarmSendOfflineData");
            AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(mContext, AlarmSendOfflineData.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 5 * 60000,
                    pendingIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getMonthNumber() {
        java.util.Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH);
        return month;
    }

    public static void shutdownAlarm(Context mContext) {
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(mContext, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);
        alarmManager.cancel(pendingIntent);
    }

    public static BigDecimal convertToBigDecimal(String data) {
        BigDecimal bd = new BigDecimal(data);
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

        symbols.setGroupingSeparator(',');
        formatter.setDecimalFormatSymbols(symbols);
        System.out.println(formatter.format(bd.longValue()));
        return bd;
    }

    public static String getPackageName(Context context) {
        return context.getPackageName();
    }

    public static String returnDoubleBulat(String s) {
        double answer = Double.parseDouble(s);
        DecimalFormat df = new DecimalFormat("###");
        System.out.println(df.format(answer));
        return df.format(answer);
    }

    //public static String formatAmount()

    public String isGPSEnabled(Context context) {
        String locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        System.out.println("Location " + locationProviders);
        return locationProviders;
//        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }
}
