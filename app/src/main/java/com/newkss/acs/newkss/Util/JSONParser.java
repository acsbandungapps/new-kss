package com.newkss.acs.newkss.Util;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Base64;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Erdy on 1/5/2017.
 */
public class JSONParser {

    public JSONParser() {

    }


    public String getJSONSSL(String link, String json, int timeout, String method, Context activity) {
//        System.out.println("request " + json);
        URL url;
        HttpsURLConnection urlConnection = null;

        try {
            // Load CAs from an InputStream
            // (could be from a resource or ByteArrayInputStream or ...)
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            // From https://www.washington.edu/itconnect/security/ca/load-der.crt
            AssetManager assManager = activity.getAssets();
            InputStream is = null;
            try {
                is = assManager.open("server.crt");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            InputStream caInput = new BufferedInputStream(is);
            Certificate ca;
            try {
                ca = cf.generateCertificate(caInput);
//                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
            } finally {
                caInput.close();
            }

            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            // Create an SSLContext that uses our TrustManager
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);

            HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    HostnameVerifier hv =
                            HttpsURLConnection.getDefaultHostnameVerifier();
                    hv.verify(new String(Base64.decode("YXBpLnByb3RlbG1hcnQuY29tOjQ0MzE=", Base64.DEFAULT)), session); //prod
//                    hv.verify(new String(Base64.decode("MzYuNjguMjI4Ljc2OjY0NjQy", Base64.DEFAULT)), session); //dev
                    return true;
                }
            };

            url = new URL(link);
            urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(timeout);
            urlConnection.setReadTimeout(timeout);
            urlConnection.setHostnameVerifier(hostnameVerifier);
            urlConnection.setSSLSocketFactory(context.getSocketFactory());
            urlConnection.setRequestMethod(method);
            if (method.equals("POST")) {
                urlConnection.setRequestProperty("Content-Type", "application/json");
            }

            OutputStream os = urlConnection.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(json);
            osw.flush();
            osw.close();

            urlConnection.connect();

            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
//            System.out.println("respon "+sb.toString());

            int rc = urlConnection.getResponseCode();
            if (rc == 200) {
                return sb.toString();
            } else {
                return "server error";
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }
        return "koneksi terputus";
    }


    private void print_https_cert(HttpsURLConnection con) {

        if (con != null) {

            try {

                System.out.println("Response Code : " + con.getResponseCode());
                System.out.println("Cipher Suite : " + con.getCipherSuite());
                System.out.println("\n");

                Certificate[] certs = con.getServerCertificates();
                for (Certificate cert : certs) {
                    System.out.println("Cert Type : " + cert.getType());
                    System.out.println("Cert Hash Code : " + cert.hashCode());
                    System.out.println("Cert Public Key Algorithm : "
                            + cert.getPublicKey().getAlgorithm());
                    System.out.println("Cert Public Key Format : "
                            + cert.getPublicKey().getFormat());
                    System.out.println("\n");
                }

            } catch (SSLPeerUnverifiedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    private void print_content(HttpsURLConnection con) {
        if (con != null) {

            try {

                System.out.println("****** Content of the URL ********");
                BufferedReader br =
                        new BufferedReader(
                                new InputStreamReader(con.getInputStream()));

                String input;

                while ((input = br.readLine()) != null) {
                    System.out.println(input);
                }
                br.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }


    public HttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

    public String tes(String url, String json, int timeout, String header, Context context) {


        String strRespone = "";
        try {

            HttpClient client = getNewHttpClient();
            HttpEntity entity = new StringEntity(json, "UTF-8");
            HttpPost post = new HttpPost(url);

            HttpParams httpParams = new BasicHttpParams();

            HttpConnectionParams.setConnectionTimeout(httpParams, timeout);
            HttpConnectionParams.setSoTimeout(httpParams, timeout);

            post.setParams(httpParams);
            post.setEntity(entity);
            post.setHeader("Content-type", "application/json");
            post.setHeader("Authentication", "Basic MzU5ODk2MDc3NTgwNDQ1OjAwMDoxNTA3MjcxNDIyNzQz");
            System.out.println("URL : " + url);
            System.out.println("SEND POST MESSAGE: " + json);

            BasicResponseHandler responseHandler = new BasicResponseHandler();
//            strRespone = client.execute(post, responseHandler);

            HttpResponse response = client.execute(post);
            strRespone = responseHandler.handleResponse(response);
            int code = response.getStatusLine().getStatusCode();
            System.out.println("BODY: " + strRespone);
            System.out.println("CODE: " + code);

            System.out.println("LENGTH DATA: " + strRespone.length());
            int length = strRespone.length();

            for (int i = 0; i < length; i += 1024) {
                if (i + 1024 < length) {
                    System.out.println("RESPONSE: " + strRespone.substring(i, i + 1024));
                } else {
                    System.out.println("RESPONSE: " + strRespone.substring(i, length));
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            strRespone = e.getMessage();
            throw e;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            strRespone = e.getMessage();
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
            strRespone = "Koneksi Ke Host Terputus";
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            strRespone = "Kirim Data Error";
            throw e;
        } finally {
            return strRespone;
        }
    }

//        HttpParams httpParameters = new BasicHttpParams();
//        // Set the timeout in milliseconds until a connection is established.
//        int timeoutConnection = 10000;
//        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
//        // Set the default socket timeout (SO_TIMEOUT)
//        // in milliseconds which is the timeout for waiting for data.
//        int timeoutSocket = 10000;
//        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
//
//
//
//
//
//
//        // Instantiate the custom HttpClient
//        HttpClient client = new MyHttpClient(httpParameters,
//                context);
//        // HttpGet request = new HttpGet("https://10.50.238.12/api/v.1/cekdb");
//        HttpPost httpPost = new HttpPost(url);
//
//        try {
//
//
//            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }});
//            SSLContext context1 = SSLContext.getInstance("TLS");
//            context1.init(null, new X509TrustManager[]{new X509TrustManager(){
//                public void checkClientTrusted(X509Certificate[] chain,
//                                               String authType) throws CertificateException {}
//                public void checkServerTrusted(X509Certificate[] chain,
//                                               String authType) throws CertificateException {}
//                public X509Certificate[] getAcceptedIssuers() {
//                    return new X509Certificate[0];
//                }}}, new SecureRandom());
//            HttpsURLConnection.setDefaultSSLSocketFactory(
//                    context1.getSocketFactory());
//
//
//            HttpEntity entity = new StringEntity(json, "UTF-8");
//
//            HttpParams httpParams = new BasicHttpParams();
//            HttpConnectionParams.setConnectionTimeout(httpParams, timeout);
//            HttpConnectionParams.setSoTimeout(httpParams, timeout);
//
//
//            httpPost.setParams(httpParams);
//            httpPost.setEntity(entity);
//
//
//            httpPost.setHeader("Content-Type", "application/json");
//            httpPost.setHeader("Authentication", "Basic " + header);
//
//            BufferedReader in = null;
//            try {
//                HttpResponse response = client.execute(httpPost);
//                in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//
//                StringBuffer sb = new StringBuffer("");
//                String line = "";
//                String NL = System.getProperty("line.separator");
//                while ((line = in.readLine()) != null) {
//                    sb.append(line + NL);
//                }
//                in.close();
//                String page = sb.toString();
//                System.out.println("Page: " + page);
//
//
//                return page;
////            tv.setText(page);
//            } catch (ClientProtocolException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            } finally {
//                if (in != null) {
//                    try {
//                        in.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

////            HttpPost httpPost = new HttpPost(url);
//
//            HttpPost httpPost = new HttpPost(url);
////            HttpResponse response = httpClient.execute(httpPost);
//
//
//            ResponseHandler<String> responseHandler=new BasicResponseHandler();
//            strResponse= httpClient.execute(httpPost, responseHandler);
//
//            //System.out.println("RESPONSE: " + response);
////            HttpResponse response = httpClient.execute(httpPost);
////
////
////            HttpParams httpParams = new BasicHttpParams();
////            HttpConnectionParams.setConnectionTimeout(httpParams, timeout);
////            HttpConnectionParams.setSoTimeout(httpParams, timeout);
////
////            httpPost.setParams(httpParams);
////            httpPost.setEntity(entity);
////
////            httpPost.setHeader("Content-Type", "application/json");
////            httpPost.setHeader("Authentication", "Basic " + header);
//
////            BasicResponseRHandler responseHandler = new BasicResponseHandler();
////            strResponse = httpClient.execute(httpPost, responseHandler);
////
//            System.out.println("PRINTTT: " + strResponse);


//        return "";


//    //TODO TESTING HTTPS ON HTTP
//    public String HttpRequestPost(String url, String json, int timeout, String header) {
//        String strRespone = "";
//        try {
//
//            HttpClient client = getNewHttpClient();
//            HttpEntity entity = new StringEntity(json, "UTF-8");
//            HttpPost post = new HttpPost(url);
//
//            HttpParams httpParams = new BasicHttpParams();
//
//            HttpConnectionParams.setConnectionTimeout(httpParams, timeout);
//            HttpConnectionParams.setSoTimeout(httpParams, timeout);
//
//            post.setParams(httpParams);
//            post.setEntity(entity);
//            post.setHeader("Content-type", "application/json");
//            post.setHeader("Authentication", "Basic " + header);
//            System.out.println("URL : " + url);
//            System.out.println("SEND POST MESSAGE: " + json);
//
//            BasicResponseHandler responseHandler = new BasicResponseHandler();
////            strRespone = client.execute(post, responseHandler);
//
//            HttpResponse response = client.execute(post);
//            strRespone = responseHandler.handleResponse(response);
//
//
//            int code = response.getStatusLine().getStatusCode();
//            System.out.println("BODY: " + strRespone);
//            System.out.println("CODE: " + code);
//
//            System.out.println("LENGTH DATA: " + strRespone.length());
//            int length = strRespone.length();
//
//            for (int i = 0; i < length; i += 1024) {
//                if (i + 1024 < length) {
//                    System.out.println("RESPONSE: " + strRespone.substring(i, i + 1024));
//                } else {
//                    System.out.println("RESPONSE: " + strRespone.substring(i, length));
//                }
//            }
//            return strRespone;
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//            strRespone = e.getMessage();
//            throw e;
//        } catch (ClientProtocolException e) {
//            e.printStackTrace();
//            strRespone = e.getMessage();
//            throw e;
//        } catch (IOException e) {
//            e.printStackTrace();
//            strRespone = Constant.CONNECTION_LOST;
//            throw e;
//        } catch (Exception e) {
//            e.printStackTrace();
//            strRespone = Constant.CONNECTION_ERROR;
//            throw e;
//        } finally {
//            return strRespone;
//        }
//    }

    //TODO koneksi http
    public String HttpRequestPost(String url, String json, int timeout, String header) {
        String strResponse = "";
        try {

            /*
             * end of the fix
             */


            DefaultHttpClient client = new DefaultHttpClient();
            HttpEntity entity = new StringEntity(json, "UTF-8");
            HttpPost post = new HttpPost(url);

            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, timeout);
            HttpConnectionParams.setSoTimeout(httpParams, timeout);

            //set retry
            DefaultHttpRequestRetryHandler retryHandler = new DefaultHttpRequestRetryHandler(0, false); // 0 = retry, false = disable retry
            client.setHttpRequestRetryHandler(retryHandler);

            post.setParams(httpParams);
            post.setEntity(entity);
//            post.addHeader("Content-Type", "application/json");
//            post.addHeader("Authentication", header);
//
            post.setHeader("Content-Type", "application/json");
            //post.setHeader("Content-type", "text/html");
            post.setHeader("Authentication", "Basic " + header);
            System.out.println("Nilai Header: " + header);
            //System.out.println("SEND POST MESSAGE: " + json);

            BasicResponseHandler responseHandler = new BasicResponseHandler();
            strResponse = client.execute(post, responseHandler);
            //System.out.println("LENGTH DATA: " + strResponse);
            //System.out.println("LENGTH DATA: " + strResponse.length());
            int length = strResponse.length();

            for (int i = 0; i < length; i += 1024) {
                //System.out.println("I = " + i);
                //System.out.println("LENG: " + length);
                if (i + 1024 < length) {
                    //System.out.println("RESPONSE: " + strResponse.substring(i, i + 1024));
                } else {
                    //System.out.println("RESPONSE: " + strResponse.substring(i, length));
                }
            }


        } catch (UnsupportedEncodingException e) {
            strResponse = e.getMessage();
            throw e;
        } catch (ClientProtocolException e) {
            strResponse = e.getMessage();
            throw e;
        } catch (IOException e) {
            // e.getCause();
            e.printStackTrace();
            strResponse = Constant.CONNECTION_LOST;
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            strResponse = Constant.CONNECTION_ERROR;
            throw e;
        } finally {
            return strResponse;
        }
    }



//        String strResponse = "";
//        HostnameVerifier hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
////
//        DefaultHttpClient client = new DefaultHttpClient();
//
//        SchemeRegistry registry = new SchemeRegistry();
//        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
//        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
//        registry.register(new Scheme("https", socketFactory, 443));
//        SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
//        DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());
//
//
//        HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
//        try {
//
//            HttpEntity entity = new StringEntity(json, "UTF-8");
////            HttpPost httpPost = new HttpPost(url);
//
//            HttpPost httpPost = new HttpPost(url);
////            HttpResponse response = httpClient.execute(httpPost);
//
//
//            ResponseHandler<String> responseHandler=new BasicResponseHandler();
//            strResponse= httpClient.execute(httpPost, responseHandler);

            //System.out.println("RESPONSE: " + response);
//            HttpResponse response = httpClient.execute(httpPost);
//
//
//            HttpParams httpParams = new BasicHttpParams();
//            HttpConnectionParams.setConnectionTimeout(httpParams, timeout);
//            HttpConnectionParams.setSoTimeout(httpParams, timeout);
//
//            httpPost.setParams(httpParams);
//            httpPost.setEntity(entity);
//
//            httpPost.setHeader("Content-Type", "application/json");
//            httpPost.setHeader("Authentication", "Basic " + header);

//            BasicResponseRHandler responseHandler = new BasicResponseHandler();
//            strResponse = httpClient.execute(httpPost, responseHandler);
//
//            System.out.println("PRINTTT: " + strResponse);
//        } catch (SecurityException e) {
//            e.printStackTrace();
//            strResponse = e.getMessage();
//        } catch (ClientProtocolException e) {
//            e.printStackTrace();
//            strResponse = e.getMessage();
//        } catch (IOException e) {
//            e.printStackTrace();
//            strResponse = Constant.CONNECTION_LOST;
//        } catch (Exception e) {
//            e.printStackTrace();
//            strResponse = Constant.CONNECTION_ERROR;
//        }
//
//
//        return strResponse;
//
//     Do not do this in production!!!
//        HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
//
//        DefaultHttpClient client = new DefaultHttpClient();
//
//        SchemeRegistry registry = new SchemeRegistry();
//        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
//        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
//        registry.register(new Scheme("https", socketFactory, 443));
//        SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
//        DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());
//
//// Set verifier
//        HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
//
//// Example send http request
//        try{
//           // final String url = "https://encrypted.google.com/";
//            HttpPost httpPost = new HttpPost(url);
//            HttpResponse response = httpClient.execute(httpPost);
//            return response.toString();
//        }
//        catch (Exception e){
//            e.printStackTrace();
//        }
//return "";
//
//
//        HttpParams httpParameters = new BasicHttpParams();
//        // Set the timeout in milliseconds until a connection is established.
//        int timeoutConnection = 10000;
//        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
//        // Set the default socket timeout (SO_TIMEOUT)
//        // in milliseconds which is the timeout for waiting for data.
//        int timeoutSocket = 10000;
//        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
//
//        // Instantiate the custom HttpClient
//        HttpClient client = new MyHttpClient(httpParameters,
//                context);
//        HttpGet request = new HttpGet("https://eu.battle.net/login/en/login.xml");
//
//        BufferedReader in = null;
//        try {
//            HttpResponse response = client.execute(request);
//            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//
//            StringBuffer sb = new StringBuffer("");
//            String line = "";
//            String NL = System.getProperty("line.separator");
//            while ((line = in.readLine()) != null) {
//                sb.append(line + NL);
//            }
//            in.close();
//            String page = sb.toString();
//            System.out.println("Page: "+page);
//
//
//            return page;
////            tv.setText(page);
//        } catch (ClientProtocolException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (in != null) {
//                try {
//                    in.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//




//
//
////        HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
////
////        DefaultHttpClient client = new DefaultHttpClient();
////
////        SchemeRegistry registry = new SchemeRegistry();
////        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
////        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
////        registry.register(new Scheme("https", socketFactory, 443));
////        SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
////        DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());
////
////// Set verifier
////        HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
////
////// Example send http request
////
////        try {
////            final String url1 = url;
////            HttpPost httpPost = new HttpPost(url);
////            HttpEntity entity = new StringEntity(json, "UTF-8");
////            HttpParams httpParams = new BasicHttpParams();
////            HttpConnectionParams.setConnectionTimeout(httpParams, timeout);
////            HttpConnectionParams.setSoTimeout(httpParams, timeout);
////            httpPost.setParams(httpParams);
////            httpPost.setEntity(entity);
//////            httpPost.addHeader("Content-Type", "application/json");
//////            httpPost.addHeader("Authentication", header);
////
////          //  httpPost.setHeader("Content-Type", "application/json");
////            //post.setHeader("Content-type", "text/html");
////            httpPost.setHeader("Authentication", "Basic " + header);
////
////            HttpClient httpclient = createHttpClient();
////            HttpPost httppost = new HttpPost(url);
////            httppost.setEntity(entity);
////            HttpResponse response = httpclient.execute(httppost);
////
////           // HttpResponse response = httpClient.execute(httpPost);
////            ResponseHandler<String> responseHandler = new BasicResponseHandler();
////            String responseBody = httpClient.execute(httpPost, responseHandler);
////            System.out.println("Print " + responseBody);
////            return responseBody;
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////        return "";
//
//
//
//
//
//
//        // HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
//
//        // Create all-trusting host name verifier
//
//
//
//
//        HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
//
//        DefaultHttpClient client = new DefaultHttpClient();
//
//        SchemeRegistry registry = new SchemeRegistry();
//        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
//        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
//        registry.register(new Scheme("https", socketFactory, 443));
//        SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
//        DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());
//
//
//        HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
//        try {
//
//
////            HttpEntity entity = new StringEntity(json, "UTF-8");
////            HttpPost httpPost = new HttpPost(url);
//
//            HttpPost httpPost = new HttpPost(url);
//            HttpResponse response = httpClient.execute(httpPost);
//
//            System.out.println("RESPONSE: " + response);
////            HttpResponse response = httpClient.execute(httpPost);
//
//
////            HttpParams httpParams = new BasicHttpParams();
////            HttpConnectionParams.setConnectionTimeout(httpParams, timeout);
////            HttpConnectionParams.setSoTimeout(httpParams, timeout);
////
////            httpPost.setParams(httpParams);
////            httpPost.setEntity(entity);
////
////            httpPost.setHeader("Content-Type", "application/json");
////            httpPost.setHeader("Authentication", "Basic " + header);
////
////            BasicResponseHandler responseHandler = new BasicResponseHandler();
////            strResponse = httpClient.execute(httpPost, responseHandler);
//
//            System.out.println("PRINTTT: " + strResponse);
//        } catch (SecurityException e) {
//            e.printStackTrace();
//            strResponse = e.getMessage();
//        } catch (ClientProtocolException e) {
//            e.printStackTrace();
//            strResponse = e.getMessage();
//        }  catch (IOException e) {
//            e.printStackTrace();
//            strResponse = Constant.CONNECTION_LOST;
//        } catch (Exception e) {
//            e.printStackTrace();
//            strResponse = Constant.CONNECTION_ERROR;
//        }
//
//
//        return strResponse;
//
//
//    }

    public static HttpClient createHttpClient() {
        HttpParams params = new BasicHttpParams();
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, HTTP.DEFAULT_CONTENT_CHARSET);
        HttpProtocolParams.setUseExpectContinue(params, true);

        SchemeRegistry schReg = new SchemeRegistry();
        schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params, schReg);

        return new DefaultHttpClient(conMgr, params);
    }


//    public String HttpRequestPost(String url, String json, int timeout, String header) throws Exception {
////
//        String strResponse = "";
//        try {/*
//////             *  fix for
//////             *    Exception in thread "main" javax.net.ssl.SSLHandshakeException:
//////             *       sun.security.validator.ValidatorException:
//////             *           PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException:
//////             *               unable to find valid certification path to requested target
//////             */
//////
//            TrustManager[] trustAllCerts = new TrustManager[]{
//                    new X509TrustManager() {
//                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//                            return null;
//                        }
//
//                        public void checkClientTrusted(X509Certificate[] certs, String authType) {
//                        }
//
//                        public void checkServerTrusted(X509Certificate[] certs, String authType) {
//                        }
//
//                    }
//            };
//
//            SSLContext sc = SSLContext.getInstance("SSL");
//            sc.init(null, trustAllCerts, new java.security.SecureRandom());
//            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//
//            // Create all-trusting host name verifier
//            HostnameVerifier allHostsValid = new HostnameVerifier() {
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }
//            };
//            // Install the all-trusting host verifier
//            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
//            /*
//             * end of the fix
//             */
////
////
//            URL urls = new URL(url);
//            HttpsURLConnection connection = (HttpsURLConnection) urls.openConnection();
//
//
//            connection.setRequestMethod("POST");
//            connection.setConnectTimeout(timeout);
//            connection.setRequestProperty("Authentication", "Basic " + header);
//            connection.getErrorStream();
//
//            int status = connection.getResponseCode();
//            System.out.println("Status: "+status);
//
//            BufferedReader reader = null;
//            if (connection.getResponseCode() == 200) {
//                reader = new BufferedReader(new
//                        InputStreamReader(connection.getInputStream()));
//            } else {
//                reader = new BufferedReader(new
//                        InputStreamReader(connection.getErrorStream()));
//            }
//            System.out.println("Reader: "+reader);
//
//            print_content(connection);
//            print_https_cert(connection);
////            post.setHeader("Authentication", "Basic "+header);
////            System.out.println("Nilai Header: "+header);
////            InputStream is = connection.getInputStream();
////            System.out.println("is " + is);
//
//
//            connection.setDoOutput(true);
////            connection.setDoOutput(true);
//            DataOutputStream dataOutputStream = new DataOutputStream((connection.getOutputStream()));
//            dataOutputStream.writeBytes(json);
//            //System.out.println("Send POST Parameter: " + json);
//            dataOutputStream.flush();
//            dataOutputStream.close();
//
//            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//            String line = "";
//            StringBuilder responseOutput = new StringBuilder();
//            while ((line = br.readLine()) != null) {
//                responseOutput.append(line);
//            }
//            br.close();
//            strResponse = responseOutput.toString();
//            System.out.println("Str response: " + strResponse);
//
//            return strResponse;
//
//        } catch (SecurityException e) {
//            e.printStackTrace();
//            strResponse = e.getMessage();
//        } catch (ClientProtocolException e) {
//            e.printStackTrace();
//            strResponse = e.getMessage();
//        } catch (IOException e) {
//            e.printStackTrace();
//            strResponse = Constant.CONNECTION_LOST;
//        } catch (Exception e) {
//            e.printStackTrace();
//            strResponse = Constant.CONNECTION_ERROR;
//        }
//        return strResponse;
//    }


//        String strResponse = "";
//        try {/*
//             *  fix for
//             *    Exception in thread "main" javax.net.ssl.SSLHandshakeException:
//             *       sun.security.validator.ValidatorException:
//             *           PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException:
//             *               unable to find valid certification path to requested target
//             */
//
//            TrustManager[] trustAllCerts = new TrustManager[]{
//                    new X509TrustManager() {
//                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//                            return null;
//                        }
//
//                        public void checkClientTrusted(X509Certificate[] certs, String authType) {
//                        }
//
//                        public void checkServerTrusted(X509Certificate[] certs, String authType) {
//                        }
//
//                    }
//            };
//
//            SSLContext sc = SSLContext.getInstance("SSL");
//            sc.init(null, trustAllCerts, new java.security.SecureRandom());
//            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//
//            // Create all-trusting host name verifier
//            HostnameVerifier allHostsValid = new HostnameVerifier() {
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }
//            };
//            // Install the all-trusting host verifier
//            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
            /*
             * end of the fix
             */


//            URL urls = new URL(url);
//            HttpsURLConnection conn = (HttpsURLConnection) urls.openConnection();
//            conn.setDoOutput(true);
//            conn.setRequestMethod("POST");
//            conn.setRequestProperty("Content-Type", "application/json");


//            URL urls = new URL(url);
//            HttpsURLConnection con = (HttpsURLConnection) urls.openConnection();
//            con.setDoInput(true);
//            con.setRequestMethod("GET");
//            con.setConnectTimeout(timeout);
//            //dumpl all cert info
//            print_https_cert(con);
//
//            //dump all the content
//            print_content(con);


//            String input = new Gson().toJson(params);
//            Log.i("Appygram Example","Sending: "+input);
//            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
//            os.writeBytes(json);
//            os.flush();
//            os.close();
//            long result = (long) conn.getResponseCode();
//            Log.i("Appygram Example","Appygram sent with result "+result);


//            URL urls = new URL(url);
//            HttpsURLConnection urlConnection = (HttpsURLConnection) urls.openConnection();
//
//            InputStream stream = new BufferedInputStream(urlConnection.getInputStream());
//            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
//            StringBuilder builder = new StringBuilder();
//
//            String inputString;
//            while ((inputString = bufferedReader.readLine()) != null) {
//                builder.append(inputString);
//            }
//
//            String response=builder.toString();
//            System.out.println("Response: "+response);


//            URL urls = new URL(url);
//            HttpsURLConnection connection = (HttpsURLConnection) urls.openConnection();
//            connection.setRequestMethod("POST");
//            connection.setConnectTimeout(timeout);
//            connection.setRequestProperty("Authentication","Basic "+header);
//            connection.setDoInput(false);
////            InputStream is = connection.getInputStream();
////            System.out.println("is " + is);
//            connection.setDoOutput(true);
//
//
////            connection.setDoOutput(true);
//            DataOutputStream dataOutputStream = new DataOutputStream((connection.getOutputStream()));
//            dataOutputStream.writeBytes(json);
//            //System.out.println("Send POST Parameter: " + json);
//            dataOutputStream.flush();
//            dataOutputStream.close();
//
//            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//            String line = "";
//            StringBuilder responseOutput = new StringBuilder();
//            while ((line = br.readLine()) != null) {
//                responseOutput.append(line);
//            }
//            br.close();
//            strResponse = responseOutput.toString();
//            System.out.println("Str response: "+strResponse);


//            URL urls = new URL(url);
//            HttpsURLConnection conn = (HttpsURLConnection) urls.openConnection();
//            conn.setReadTimeout(timeout);
//            conn.setConnectTimeout(timeout);
//            conn.setRequestMethod("POST");
//            conn.setRequestProperty("Authentication","Basic");
//            conn.setDoInput(true);
//
//            InputStream is = conn.getInputStream();
//            System.out.println("asdd "+is.toString());
//            //conn.setDoOutput(true);
//
//            DataOutputStream dataOutputStream = new DataOutputStream((conn.getOutputStream()));
//            dataOutputStream.writeBytes(json);
//            dataOutputStream.flush();
//            dataOutputStream.close();
//            conn.connect();
//            System.out.println("Conn "+conn.getInputStream().toString());
//
//            int respCode = ((HttpsURLConnection)conn).getResponseCode();
//            System.out.println("Response Code: "+respCode);
//            return strResponse;

//        } catch (SecurityException e) {
//            System.out.println("SecurityException " + e.getMessage());
//            e.printStackTrace();
//            strResponse = e.getMessage();
//        } catch (ProtocolException e) {
//            System.out.println("ProtocolException " + e.getMessage());
//            e.printStackTrace();
//            strResponse = e.getMessage();
//        } catch (IOException e) {
//
//            System.out.println("IOException " + e.getMessage());
//            e.printStackTrace();
//            strResponse = Constant.CONNECTION_LOST;
//        } catch (Exception e) {
//            System.out.println("Exception " + e.getMessage());
//            e.printStackTrace();
//            strResponse = Constant.CONNECTION_ERROR;
//        }
//        return strResponse;


//    public String HttpRequestPost(String url, int method) throws ClientProtocolException, SocketException, SocketTimeoutException, Exception {
//        {
//            int flag = 0;
//            String result = "{\"errorCode\":\"IB-0000\"}";
//            if (flag == 1)
//                flag = 1;
//            HttpParams httpParameters = new BasicHttpParams();
//            // Set the timeout in milliseconds until a connection is established.
//            // int timeoutConnection = 600000;
//            int timeoutConnection = 1800000;
//
//            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
//            // Set the default socket timeout (SO_TIMEOUT)
//            // in milliseconds which is the timeout for waiting for data.
//            // int timeoutSocket = 600000;
//            int timeoutSocket = 1800000;
//
//            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
//
//            // // begin disable ssl certificate validation
//            //
//            // KeyStore trustStore =
//            // KeyStore.getInstance(KeyStore.getDefaultType());
//            // trustStore.load(null, null);
//            // SSLSocketFactory sf = new CustomSSLSocketFactory(trustStore);
//            // sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
//            //
//            // HttpProtocolParams.setVersion(httpParameters, HttpVersion.HTTP_1_1);
//            // HttpProtocolParams.setContentCharset(httpParameters, HTTP.UTF_8);
//            //
//            // SchemeRegistry registry = new SchemeRegistry();
//            // registry.register(new Scheme("http",
//            // PlainSocketFactory.getSocketFactory(), 80));
//            // registry.register(new Scheme("https", sf, 8080));
//            //
//            // ClientConnectionManager ccm = new
//            // ThreadSafeClientConnManager(httpParameters, registry);
//            // HttpClient httpclient = new DefaultHttpClient(ccm,httpParameters);
//            // // end of disable ssl
//            //
//            //
//            // //HttpClient httpclient = new DefaultHttpClient(httpParameters);
//            // //HttpGet request = new HttpGet(url);
//            // ResponseHandler<String> handler = new BasicResponseHandler();
//            // //you result will be String :
//            //
//            //
//            // HostnameVerifier hostnameVerifier =
//            // org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
//            //
//            // DefaultHttpClient client = new DefaultHttpClient();
//            //
//            // //SchemeRegistry registry = new SchemeRegistry();
//            // SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
//            // socketFactory.setHostnameVerifier((X509HostnameVerifier)
//            // hostnameVerifier);
//            // registry.register(new Scheme("https", socketFactory, 443));
//            // SingleClientConnManager mgr = new
//            // SingleClientConnManager(client.getParams(), registry);
//            // DefaultHttpClient httpClient = new DefaultHttpClient(mgr,
//            // client.getParams());
//            //
//            // // Set verifier
//            // HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
//            //
//            //
//            // HttpPost httpPost = new HttpPost(url);
//            // HttpResponse response = httpClient.execute(httpPost);
//            //
//            //
//            // HttpUriRequest request;
//            String[] pins = new String[]{"1a833af69cae44d2445841530cfd67c9815aa72b"};
//
//            SchemeRegistry schemeRegistry = new SchemeRegistry();
//            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
//            schemeRegistry.register(new Scheme("https", new PinningSSLSocketFactory(pins, 0), 443));
//
//            HttpParams httpParams = new BasicHttpParams();
//            ClientConnectionManager ccm = new ThreadSafeClientConnManager(httpParams, schemeRegistry);
//
//            HttpClient httpClient = new DefaultHttpClient(ccm, httpParameters);
//
//            ResponseHandler<String> handler = new BasicResponseHandler();
//
//            HttpUriRequest request;
//            request = new HttpPost(url);
//            // request.addHeader("Accept-Language", Utility.getLanguage(context));
//            // request.setHeader("Accept-Language", Utility.getLanguage(context));
//            result = httpClient.execute(request, handler);
//
//            // System.out.println("#### Response request : "+result.toString());
//
//            return result;
//        }
//    }

//


//    public String HttpRequestPost(String url, String json, int timeout) throws Exception {
//        String strResponse = "";
//        try {/*
//             *  fix for
//             *    Exception in thread "main" javax.net.ssl.SSLHandshakeException:
//             *       sun.security.validator.ValidatorException:
//             *           PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException:
//             *               unable to find valid certification path to requested target
//             */
//
////            TrustManager[] trustAllCerts = new TrustManager[]{
////                    new X509TrustManager() {
////                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
////                            return null;
////                        }
////
////                        public void checkClientTrusted(X509Certificate[] certs, String authType) {
////                        }
////
////                        public void checkServerTrusted(X509Certificate[] certs, String authType) {
////                        }
////
////                    }
////            };
////
////            SSLContext sc = SSLContext.getInstance("SSL");
////            sc.init(null, trustAllCerts, new java.security.SecureRandom());
////            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
////
////            // Create all-trusting host name verifier
////            HostnameVerifier allHostsValid = new HostnameVerifier() {
////                public boolean verify(String hostname, SSLSession session) {
////                    return true;
////                }
////            };
////            // Install the all-trusting host verifier
////            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
////            /*
////             * end of the fix
////             */
//
//
//            URL urls = new URL(url);
//            HttpsURLConnection connection = (HttpsURLConnection) urls.openConnection();
//            connection.setRequestMethod("POST");
//            connection.setConnectTimeout(timeout);
//
//            connection.setDoOutput(true);
//            DataOutputStream dataOutputStream = new DataOutputStream((connection.getOutputStream()));
//            dataOutputStream.writeBytes(json);
//            //System.out.println("Send POST Parameter: " + json);
//            dataOutputStream.flush();
//            dataOutputStream.close();
//
//            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//            String line = "";
//            StringBuilder responseOutput = new StringBuilder();
//            while ((line = br.readLine()) != null) {
//                responseOutput.append(line);
//            }
//            br.close();
//            strResponse = responseOutput.toString();
//            return strResponse;
//
//        } catch(SecurityException e){
//                strResponse = e.getMessage();
//            }catch(ClientProtocolException e){
//                strResponse = e.getMessage();
//            }catch(IOException e){
//                strResponse = Constant.CONNECTION_LOST;
//            }catch(Exception e){
//                strResponse = Constant.CONNECTION_ERROR;
//            }
//            return strResponse;
//        }


}
