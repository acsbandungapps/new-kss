package com.newkss.acs.newkss.Util;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by acs on 5/26/17.
 */

public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = Constant.DB_VERSION;
    public static SQLiteDatabase sqLiteDB;
    private static String fileName;

    public static void deleteDB(Context context) {
        File file = new File(Constant.APP_PATH + Function.getPackageName(context) + File.separator + Constant.HIDDEN_FOLDER + File.separator + Constant.DB_NAME);
        if (file.exists()) {
            file.delete();
            System.out.println(Constant.DB_NAME + ": dihapus");
        }
    }

    public static void initApp(Context context) {
        try {
            //deleteDB();
            //File file = new File(Constant.APP_PATH + Function.getPackageName(context) + File.separator + Constant.HIDDEN_FOLDER);
            File file = new File(Constant.APP_PATH);

            if (!file.exists()) {
                file.mkdirs();
                System.out.println(file.toString() + " Folder " + Constant.APP_PATH + Function.getPackageName(context) + File.separator + Constant.HIDDEN_FOLDER + " created ...");
            }
            //copyAssets(Constant.APP_PATH + Function.getPackageName(context) + File.separator + Constant.HIDDEN_FOLDER, context);
            copyAssets(Constant.APP_PATH, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public MySQLiteHelper(Context context, String DATABASE_NAME) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        fileName = DATABASE_NAME;
    }


    public static void initDB(Context context) {
        Log.i("Masuk init DB", "MASUK");
        System.out.println("DB location: " + Constant.APP_PATH + Function.getPackageName(context) + File.separator + Constant.HIDDEN_FOLDER + File.separator + Constant.DB_NAME);
        System.out.println("DB location: " + Constant.APP_PATH);
        //MySQLiteHelper db = new MySQLiteHelper(context, Constant.APP_PATH + Function.getPackageName(context) + File.separator + Constant.HIDDEN_FOLDER + File.separator + Constant.DB_NAME);
       // System.out.println("DB LOCATION: "+context.getFilesDir());
        MySQLiteHelper db = new MySQLiteHelper(context, Constant.APP_PATH + Constant.DB_NAME);
        Constant.MKSSdb = db.openDatabase();
    }

    public static SQLiteDatabase openDatabase() throws SQLException {
        try {
            System.out.println("Sqlite Open");
            sqLiteDB = SQLiteDatabase.openDatabase(fileName, null, SQLiteDatabase.OPEN_READWRITE);
            return sqLiteDB;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void copyAssets(String path, Context context) {
        AssetManager assetManager = context.getAssets();
        String[] files = null;
        try {
            files = assetManager.list("");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("error file: " + e);
        }
        for (String filename : files) {
            InputStream in = null;
            OutputStream out = null;
            File checkFile = new File(path + filename);
            if (!checkFile.exists()) {
                System.out.println("file created: " + filename);
                try {
                    in = assetManager.open(filename);
                    File outFile = new File(path, filename);
                    out = new FileOutputStream(outFile);

                    copyFile(in, out);
                    in.close();
                    in = null;
                    out.flush();
                    out.close();
                    out = null;
                } catch (IOException e) {
                    // ignore exception
                    System.out.println("error1: " + e);
                }
            }
        }
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
