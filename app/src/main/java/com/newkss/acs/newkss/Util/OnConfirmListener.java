package com.newkss.acs.newkss.Util;

/**
 * Created by acs on 9/11/17.
 */

public interface OnConfirmListener {

    public abstract void onConfirm(String value);
}
