package com.newkss.acs.newkss.Util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.newkss.acs.newkss.Model.ActionCode;
import com.newkss.acs.newkss.Model.Config;
import com.newkss.acs.newkss.Model.Detail_Inventory;
import com.newkss.acs.newkss.Model.Penagihan;
import com.newkss.acs.newkss.Model.Pencairan;
import com.newkss.acs.newkss.ModelBaru.Data_Pencairan;
import com.newkss.acs.newkss.ModelBaru.Detil_Data_Pencairan;
import com.newkss.acs.newkss.ModelBaru.Detil_TGT;
import com.newkss.acs.newkss.ModelBaru.TGT;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by acs on 8/31/17.
 */

public class SyncData {
    Detail_Inventory querydetinv = new Detail_Inventory();
    Config cc;
    Config con = new Config();
    ArrayList<Config> listConfig;
    ActionCode actionCode;
    ActionCode queryAC = new ActionCode();

    public void insertNewDataToDB(Context mContext, String hasilJson, String version, String periodik_track) {
        try {
//            MySQLiteHelper.initApp(mContext);
//            MySQLiteHelper.initDB(mContext);
            JSONObject jsonAll = new JSONObject(hasilJson);
            String waktu = jsonAll.getString("waktu");
            String pencairan = jsonAll.getString("pencairan");
            String penagihan = jsonAll.getString("penagihan");
            String action_code = jsonAll.getString("action_code");
            System.out.println("Action Code: " + action_code);

            //TODO DATA ACTION CODE
//            JSONArray jsonArrayAC = new JSONArray(action_code.toString());
//            System.out.println("JsonArrayAC: " + jsonArrayAC.toString());
            queryAC.insertActionCodeintoDB("action_code", action_code);


            cc = new Config();
            listConfig = new ArrayList<>();
            cc.setNama_config("keterangan");
            //cc.setValue(keterangan);
            listConfig.add(cc);
            cc = new Config();
            cc.setNama_config("waktu");
            cc.setValue(waktu);
            listConfig.add(cc);
            cc = new Config();
            cc.setNama_config("rc");
            //  cc.setValue(rc);
            listConfig.add(cc);
            cc = new Config();
            cc.setNama_config("Versi App");
            cc.setValue(version);
            listConfig.add(cc);
            cc = new Config();
            cc.setNama_config(Constant.PERIODIK_TRACK);
            cc.setValue(periodik_track);
            listConfig.add(cc);
            con.insertDataConfig(listConfig);


//            JSONObject jsonActionCode = new JSONObject(action_code);
//            JSONArray jsonArrayActionCode = jsonActionCode.getJSONArray("action_code");
//            String jsonArrayParameterActionCode = jsonArrayActionCode.toString();
//            queryAC.insertActionCodeintoDB(Constant.ACTION_CODE, jsonArrayParameterActionCode);

            //DATA PENCAIRAN
            JSONObject jsonPencairan = new JSONObject(pencairan);
            JSONArray jsonArrayPencairan = jsonPencairan.getJSONArray("data_pencairan");
            //System.out.println("JsonArrayPencairan: " + jsonArrayPencairan);
            JSONArray jsonArrayPencairan1 = new JSONArray(jsonArrayPencairan.toString());

            //System.out.println("JsonArrayPencairan1: " + jsonArrayPencairan1);

            Data_Pencairan queryDP = new Data_Pencairan();
            Pencairan queryPencairan = new Pencairan();

            for (int i = 0; i < jsonArrayPencairan1.length(); i++) {
                String dtlPencairan = jsonArrayPencairan1.getString(i);
                System.out.println("NILAI: " + i + "-" + dtlPencairan);

                JSONObject jsonDetilPencairan = new JSONObject(dtlPencairan);
                String cycleDetilPencairan = jsonDetilPencairan.getString("cycle");
                String detilInventoryPencairan = jsonDetilPencairan.getString("detail_inventory");
                //String jarakDetilPencairan = jsonDetilPencairan.getString("jarak");
                String osDetilPencairan = jsonDetilPencairan.getString("OS");
                String namaDetilPencairan = jsonDetilPencairan.getString("nama");
                String alamatDetilPencairan = jsonDetilPencairan.getString("alamat");


                String id_data_pencairan_generate = queryDP.generateIDDataPencairan();
                System.out.println("ID Pencairan Generate: " + id_data_pencairan_generate);
                Data_Pencairan dataPencairan = new Data_Pencairan();
                dataPencairan.setId_data_pencairan(id_data_pencairan_generate);
                dataPencairan.setCycle(cycleDetilPencairan);
                //  dataPencairan.setJarak(jarakDetilPencairan);
                dataPencairan.setOs(osDetilPencairan);
                dataPencairan.setNama(namaDetilPencairan);
                dataPencairan.setAlamat(alamatDetilPencairan);
                dataPencairan.setStatus("0");
//                queryDP.deleteAllDataPencairan();
                queryDP.insertDataPencairan(dataPencairan);

                JSONArray jsonArrDetInvPencairan = new JSONArray(detilInventoryPencairan.toString());
                for (int j = 0; j < jsonArrDetInvPencairan.length(); j++) {
                    String isiDetInvPencairan = jsonArrDetInvPencairan.getString(j);
                    JSONObject jsonIsiDetInvPencairan = new JSONObject(isiDetInvPencairan);

                    //MASUKIN KE DETAIL INVENTORY DAN KE DETAIL BUCKET DARI NILAI DATA PENCAIRAN

                    Detail_Inventory dip = new Detail_Inventory();
                    dip.setSaldo_min(jsonIsiDetInvPencairan.getString(Constant.SALDO_MIN));
                    dip.setDpd_today(jsonIsiDetInvPencairan.getString(Constant.DPD_TODAY));
                    dip.setResume_nsbh(jsonIsiDetInvPencairan.getString(Constant.RESUME_NSBH));
                    dip.setAlmt_rumah(jsonIsiDetInvPencairan.getString(Constant.ALMT_RUMAH));
                    dip.setTgl_janji_bayar_terakhir(jsonIsiDetInvPencairan.getString(Constant.TGL_JANJI_BAYAR_TERAKHIR));
                    dip.setDenda(jsonIsiDetInvPencairan.getString(Constant.DENDA));
                    dip.setPola_bayar(jsonIsiDetInvPencairan.getString(Constant.POLA_BAYAR));

                    dip.setTgl_bayar_terakhir(jsonIsiDetInvPencairan.getString(Constant.TGL_BAYAR_TERAKHIR));

                    dip.setPokok(jsonIsiDetInvPencairan.getString(Constant.POKOK));
                    dip.setTgl_sp(jsonIsiDetInvPencairan.getString(Constant.TGL_SP));
                    dip.setTenor(jsonIsiDetInvPencairan.getString(Constant.TENOR));
                    dip.setAngsuran(jsonIsiDetInvPencairan.getString(Constant.ANGSURAN));

                    //dip.setTgl_ptp(jsonIsiDetInvPencairan.getString(Constant.TGL_PTP));
                    dip.setAction_plan(jsonIsiDetInvPencairan.getString(Constant.ACTION_PLAN));
                    dip.setNo_rekening(jsonIsiDetInvPencairan.getString(Constant.NO_REKENING));
                    dip.setHistori_sp(jsonIsiDetInvPencairan.getString(Constant.HISTORI_SP));
                    dip.setNominal_janji_bayar_terakhir(jsonIsiDetInvPencairan.getString(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR));
                    dip.setTlpn_rekomendator(jsonIsiDetInvPencairan.getString(Constant.TLPN_REKOMENDATOR));

                    dip.setSumber_bayar(jsonIsiDetInvPencairan.getString(Constant.SUMBER_BAYAR));
                    dip.setBunga(jsonIsiDetInvPencairan.getString(Constant.BUNGA));
//
                    dip.setJns_pinjaman(jsonIsiDetInvPencairan.getString(Constant.JNS_PINJAMAN));
                    dip.setNominal_bayar_terakhir(jsonIsiDetInvPencairan.getString(Constant.NOMINAL_BAYAR_TERAKHIR));
//
                    //dip.setNominal_ptp(jsonIsiDetInvPencairan.getString(Constant.NOMINAL_PTP));
                    dip.setSaldo(jsonIsiDetInvPencairan.getString(Constant.SALDO));

                    dip.setOs_pinjaman(jsonIsiDetInvPencairan.getString(Constant.OS_PINJAMAN));
//
                    dip.setKeberadaan_jaminan(jsonIsiDetInvPencairan.getString(Constant.KEBERADAAN_JAMINAN));
                    dip.setEmail(jsonIsiDetInvPencairan.getString(Constant.SUMBER_BAYAR));
//
                    dip.setTotal_kewajiban(jsonIsiDetInvPencairan.getString(Constant.TOTAL_KEWAJIBAN));
//
                    dip.setNo_loan(jsonIsiDetInvPencairan.getString(Constant.NO_LOAN));
                    //   dd.setNo_loan(noloangenerated);
                    dip.setNama_debitur(jsonIsiDetInvPencairan.getString(Constant.NAMA_DEBITUR));
                    dip.setTgl_jth_tempo(jsonIsiDetInvPencairan.getString(Constant.TGL_JTH_TEMPO));
                    dip.setNo_tlpn(jsonIsiDetInvPencairan.getString(Constant.NO_TLPN));
                    dip.setTgl_gajian(jsonIsiDetInvPencairan.getString(Constant.TGL_GAJIAN));
                    dip.setPekerjaan(jsonIsiDetInvPencairan.getString(Constant.PEKERJAAN));
                    dip.setAlmt_usaha(jsonIsiDetInvPencairan.getString(Constant.ALMT_USAHA));
                    dip.setId_data_pencairan(id_data_pencairan_generate);
                    dip.setStatus("0");


                    //data yang tadinya tidak ada
                    dip.setNo_hp(jsonIsiDetInvPencairan.getString(Constant.NO_HP));
                    dip.setAngsuran_ke(jsonIsiDetInvPencairan.getString(Constant.ANGSURAN_KE));
                    dip.setNama_upliner(jsonIsiDetInvPencairan.getString(Constant.NAMA_UPLINER));
                    dip.setGender(jsonIsiDetInvPencairan.getString(Constant.GENDER));
                    dip.setTlpn_upliner(jsonIsiDetInvPencairan.getString(Constant.TLPN_UPLINER));
                    dip.setTlpn_econ(jsonIsiDetInvPencairan.getString(Constant.TLPN_ECON));
                    dip.setHarus_bayar(jsonIsiDetInvPencairan.getString(Constant.HARUS_BAYAR));
                    dip.setNama_rekomendator(jsonIsiDetInvPencairan.getString(Constant.NAMA_REKOMENDATOR));
                    dip.setTlpn_mogen(jsonIsiDetInvPencairan.getString(Constant.TLPN_MOGEN));
                    dip.setNama_econ(jsonIsiDetInvPencairan.getString(Constant.NAMA_ECON));
                    dip.setKewajiban(jsonIsiDetInvPencairan.getString(Constant.KEWAJIBAN));
                    dip.setNama_mogen(jsonIsiDetInvPencairan.getString(Constant.NAMA_MOGEN));
                    dip.setBucket(jsonIsiDetInvPencairan.getString("bucket"));
                    dip.setTgl_cair(jsonIsiDetInvPencairan.getString("tgl_cair"));

                    querydetinv.insertDetailInventoryPencairan(dip);
                }
            }

            JSONArray jsonArraySS = jsonPencairan.getJSONArray("ss");
            JSONArray jsonArraySS1 = new JSONArray(jsonArraySS.toString());
            System.out.println("Jsonarrayss: " + jsonArraySS1);
            for (int i = 0; i < jsonArraySS1.length(); i++) {
                String noloan = jsonArraySS1.getString(i);
                JSONObject jsonNoLoanSS = new JSONObject(noloan);
                System.out.println("No_Loan ss: " + jsonNoLoanSS.getString("no_loan"));


                Detil_Data_Pencairan ddp = new Detil_Data_Pencairan();
                ddp.setId_data_pencairan(queryPencairan.getIdPencairanfromNama("ss"));
                ddp.setNo_loan(jsonNoLoanSS.getString("no_loan"));
                ddp.setTgl_kunjungan("");
                ddp.setStatus("0");
                Detil_Data_Pencairan queryddp = new Detil_Data_Pencairan();
                queryddp.insertDetilDataPencairan(ddp);
            }

            JSONArray jsonArrayBI = jsonPencairan.getJSONArray("bi");
            JSONArray jsonArrayBI1 = new JSONArray(jsonArrayBI.toString());
            System.out.println("Jsonarraybi: " + jsonArrayBI1);
            for (int i = 0; i < jsonArrayBI1.length(); i++) {
                String noloan = jsonArrayBI1.getString(i);
                JSONObject jsonNoLoanBI = new JSONObject(noloan);
                System.out.println("No_Loan bi: " + jsonNoLoanBI.getString("no_loan"));


                Detil_Data_Pencairan ddp = new Detil_Data_Pencairan();
                ddp.setId_data_pencairan(queryPencairan.getIdPencairanfromNama("bi"));
                ddp.setNo_loan(jsonNoLoanBI.getString("no_loan"));
                ddp.setTgl_kunjungan("");
                ddp.setStatus("0");
                Detil_Data_Pencairan queryddp = new Detil_Data_Pencairan();
                queryddp.insertDetilDataPencairan(ddp);
            }

            JSONArray jsonArrayBS = jsonPencairan.getJSONArray("bs");
            JSONArray jsonArrayBS1 = new JSONArray(jsonArrayBS.toString());
            System.out.println("Jsonarraybs: " + jsonArrayBS1);
            for (int i = 0; i < jsonArrayBS1.length(); i++) {
                String noloan = jsonArrayBS1.getString(i);
                JSONObject jsonNoLoanBS = new JSONObject(noloan);
                System.out.println("No_Loan bs: " + jsonNoLoanBS.getString("no_loan"));

                Detil_Data_Pencairan ddp = new Detil_Data_Pencairan();
                ddp.setId_data_pencairan(queryPencairan.getIdPencairanfromNama("bs"));
                ddp.setNo_loan(jsonNoLoanBS.getString("no_loan"));
                ddp.setTgl_kunjungan("");
                ddp.setStatus("0");
                Detil_Data_Pencairan queryddp = new Detil_Data_Pencairan();
                queryddp.insertDetilDataPencairan(ddp);
            }

            JSONArray jsonArrayBLBS = jsonPencairan.getJSONArray("blbs");
            JSONArray jsonArrayBLBS1 = new JSONArray(jsonArrayBLBS.toString());
            System.out.println("Jsonarrayblbs: " + jsonArrayBLBS1);
            for (int i = 0; i < jsonArrayBLBS1.length(); i++) {
                String noloan = jsonArrayBLBS1.getString(i);
                JSONObject jsonNoLoanBLBS = new JSONObject(noloan);
                System.out.println("No_Loan blbs: " + jsonNoLoanBLBS.getString("no_loan"));

                Detil_Data_Pencairan ddp = new Detil_Data_Pencairan();
                ddp.setId_data_pencairan(queryPencairan.getIdPencairanfromNama("blbs"));
                ddp.setNo_loan(jsonNoLoanBLBS.getString("no_loan"));
                ddp.setTgl_kunjungan("");
                ddp.setStatus("0");
                Detil_Data_Pencairan queryddp = new Detil_Data_Pencairan();
                queryddp.insertDetilDataPencairan(ddp);
            }

            //DATA PENAGIHAN
            JSONObject jsonPenagihan = new JSONObject(penagihan);
            JSONArray jsonArrayPenagihan = jsonPenagihan.getJSONArray("tgt");
            System.out.println("JsonArrayPenagihan: " + jsonArrayPenagihan);
            JSONArray jsonArrayPenagihan1 = new JSONArray(jsonArrayPenagihan.toString());
            System.out.println("JsonArrayPenagihan1: " + jsonArrayPenagihan1);
            TGT queryTGT = new TGT();
            Penagihan queryPenagihan = new Penagihan();
//            String noloangenerated = d.generateNoLoanSqlite();

            for (int i = 0; i < jsonArrayPenagihan1.length(); i++) {
           // for (int i = 0; i < 10; i++) {
//
                String dtlPenagihan = jsonArrayPenagihan1.getString(i);
                JSONObject jsonDetilPenagihan = new JSONObject(dtlPenagihan);
                String cycleDetilPenagihan = jsonDetilPenagihan.getString("cycle");
                String detilInventoryPenagihan = jsonDetilPenagihan.getString("detail_inventory");
//                String jarakDetilPenagihan = jsonDetilPenagihan.getString("jarak");
                String osDetilPenagihan = jsonDetilPenagihan.getString("OS");
                String namaDetilPenagihan = jsonDetilPenagihan.getString("nama");
                String alamatDetilPenagihan = jsonDetilPenagihan.getString("alamat");

                TGT dataTGT = new TGT();
                String id_tgt_generate = queryTGT.generateIDTGT();
                System.out.println("id_tgt_generate: " + id_tgt_generate);

                dataTGT.setId_tgt(id_tgt_generate);
                dataTGT.setCycle(cycleDetilPenagihan);
//                dataTGT.setJarak(jarakDetilPenagihan);
                dataTGT.setOs(osDetilPenagihan);
                dataTGT.setNama(namaDetilPenagihan);
                dataTGT.setAlamat(alamatDetilPenagihan);
                dataTGT.setStatus("0");
//                queryTGT.deleteAllTGT();
                queryTGT.insertIdTGT(dataTGT);


//                System.out.println("cycleDetilPenagihan: "+cycleDetilPenagihan);
//                System.out.println("detilInventoryPenagihan: "+detilInventoryPenagihan);
//                System.out.println("jarakDetilPenagihan: "+jarakDetilPenagihan);
//                System.out.println("osDetilPenagihan: "+osDetilPenagihan);
//                System.out.println("namaDetilPenagihan: "+namaDetilPenagihan);
//                System.out.println("alamatDetilPenagihan: "+alamatDetilPenagihan);


                JSONArray jsonArrDetInvPenagihan = new JSONArray(detilInventoryPenagihan.toString());
//                for (int j = 0; j < 10; j++) {
                for (int j = 0; j < jsonArrDetInvPenagihan.length(); j++) {
                    String isiDetInvPenagihan = jsonArrDetInvPenagihan.getString(j);
                    JSONObject jsonIsiDetInvPenagihan = new JSONObject(isiDetInvPenagihan);

                    //System.out.println("Saldo Min PenagihanLama : " + j + "-" + jsonIsiDetInvPenagihan.getString("saldo_min"));
                    String saldoMin = jsonIsiDetInvPenagihan.getString("saldo_min");
                    //MASUKIN KE DETAIL INVENTORY DAN KE DETAIL BUCKET DARI NILAI DATA PENAGIHAN


                    Detail_Inventory dd = new Detail_Inventory();
                    dd.setSaldo_min(jsonIsiDetInvPenagihan.getString(Constant.SALDO_MIN));
                    dd.setDpd_today(jsonIsiDetInvPenagihan.getString(Constant.DPD_TODAY));
                    dd.setResume_nsbh(jsonIsiDetInvPenagihan.getString(Constant.RESUME_NSBH));
                    dd.setAlmt_rumah(jsonIsiDetInvPenagihan.getString(Constant.ALMT_RUMAH));
                    dd.setTgl_janji_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.TGL_JANJI_BAYAR_TERAKHIR));
                    dd.setDenda(jsonIsiDetInvPenagihan.getString(Constant.DENDA));
                    dd.setPola_bayar(jsonIsiDetInvPenagihan.getString(Constant.POLA_BAYAR));

                    dd.setTgl_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.TGL_BAYAR_TERAKHIR));

                    dd.setPokok(jsonIsiDetInvPenagihan.getString(Constant.POKOK));
                    dd.setTgl_sp(jsonIsiDetInvPenagihan.getString(Constant.TGL_SP));
                    dd.setTenor(jsonIsiDetInvPenagihan.getString(Constant.TENOR));
                    dd.setAngsuran(jsonIsiDetInvPenagihan.getString(Constant.ANGSURAN));

                    // dd.setTgl_ptp(jsonIsiDetInvPenagihan.getString(Constant.TGL_PTP));
                    dd.setAction_plan(jsonIsiDetInvPenagihan.getString(Constant.ACTION_PLAN));
                    dd.setNo_rekening(jsonIsiDetInvPenagihan.getString(Constant.NO_REKENING));
                    dd.setHistori_sp(jsonIsiDetInvPenagihan.getString(Constant.HISTORI_SP));
                    dd.setNominal_janji_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR));
                    dd.setTlpn_rekomendator(jsonIsiDetInvPenagihan.getString(Constant.TLPN_REKOMENDATOR));

                    dd.setSumber_bayar(jsonIsiDetInvPenagihan.getString(Constant.SUMBER_BAYAR));
                    dd.setBunga(jsonIsiDetInvPenagihan.getString(Constant.BUNGA));
//
                    dd.setJns_pinjaman(jsonIsiDetInvPenagihan.getString(Constant.JNS_PINJAMAN));
                    dd.setNominal_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.NOMINAL_BAYAR_TERAKHIR));
//
                    //  dd.setNominal_ptp(jsonIsiDetInvPenagihan.getString(Constant.NOMINAL_PTP));
                    dd.setSaldo(jsonIsiDetInvPenagihan.getString(Constant.SALDO));
//
                    dd.setOs_pinjaman(jsonIsiDetInvPenagihan.getString(Constant.OS_PINJAMAN));
//
                    dd.setKeberadaan_jaminan(jsonIsiDetInvPenagihan.getString(Constant.KEBERADAAN_JAMINAN));
                    dd.setEmail(jsonIsiDetInvPenagihan.getString(Constant.SUMBER_BAYAR));
//
                    dd.setTotal_kewajiban(jsonIsiDetInvPenagihan.getString(Constant.TOTAL_KEWAJIBAN));
//
                    dd.setNo_loan(jsonIsiDetInvPenagihan.getString(Constant.NO_LOAN));
                    //   dd.setNo_loan(noloangenerated);
                    dd.setNama_debitur(jsonIsiDetInvPenagihan.getString(Constant.NAMA_DEBITUR));
                    dd.setTgl_jth_tempo(jsonIsiDetInvPenagihan.getString(Constant.TGL_JTH_TEMPO));
                    dd.setNo_tlpn(jsonIsiDetInvPenagihan.getString(Constant.NO_TLPN));
//
                    dd.setId_tgt(id_tgt_generate);
                    dd.setStatus("0");


                    dd.setNo_hp(jsonIsiDetInvPenagihan.getString(Constant.NO_HP));
                    dd.setAngsuran_ke(jsonIsiDetInvPenagihan.getString(Constant.ANGSURAN_KE));
                    dd.setNama_upliner(jsonIsiDetInvPenagihan.getString(Constant.NAMA_UPLINER));
                    dd.setGender(jsonIsiDetInvPenagihan.getString(Constant.GENDER));
                    dd.setTlpn_upliner(jsonIsiDetInvPenagihan.getString(Constant.TLPN_UPLINER));
                    dd.setTlpn_econ(jsonIsiDetInvPenagihan.getString(Constant.TLPN_ECON));
                    dd.setHarus_bayar(jsonIsiDetInvPenagihan.getString(Constant.HARUS_BAYAR));
                    dd.setNama_rekomendator(jsonIsiDetInvPenagihan.getString(Constant.NAMA_REKOMENDATOR));
                    dd.setTlpn_mogen(jsonIsiDetInvPenagihan.getString(Constant.TLPN_MOGEN));
                    dd.setNama_econ(jsonIsiDetInvPenagihan.getString(Constant.NAMA_ECON));
                    dd.setKewajiban(jsonIsiDetInvPenagihan.getString(Constant.KEWAJIBAN));
                    dd.setNama_mogen(jsonIsiDetInvPenagihan.getString(Constant.NAMA_MOGEN));
                    dd.setTgl_gajian(jsonIsiDetInvPenagihan.getString(Constant.TGL_GAJIAN));
                    dd.setPekerjaan(jsonIsiDetInvPenagihan.getString(Constant.PEKERJAAN));
                    dd.setAlmt_usaha(jsonIsiDetInvPenagihan.getString(Constant.ALMT_USAHA));
                    dd.setBucket(jsonIsiDetInvPenagihan.getString("bucket"));
                    querydetinv.insertDetailInventoryIdTGT(dd);
//                    d.deleteAllDetailInventory();
                }
            }

            JSONArray jsonArraynpay = jsonPenagihan.getJSONArray("npay");
            JSONArray jsonArraynpay1 = new JSONArray(jsonArraynpay.toString());
            System.out.println("Jsonarraynpay: " + jsonArraynpay1);
         //   MySQLiteHelper.initDB(mContext);
//            for (int i = 0; i < 10; i++) {
            for (int i = 0; i < jsonArraynpay1.length(); i++) {
                String noloan = jsonArraynpay1.getString(i);
                JSONObject jsonNoLoanNpay = new JSONObject(noloan);

                Detil_TGT dtgt = new Detil_TGT();
                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("npay"));
                dtgt.setNo_loan(jsonNoLoanNpay.getString("no_loan"));
                dtgt.setTgl_kunjungan("");
                dtgt.setStatus("0");
                Detil_TGT querydtgt = new Detil_TGT();
                querydtgt.insertDetilTGT(dtgt);
                // querydtgt.deleteAllDetilTGT();
                System.out.println("No_Loan npay: " + jsonNoLoanNpay.getString("no_loan"));
            }

            JSONArray jsonArrayfpay = jsonPenagihan.getJSONArray("fpay");
            JSONArray jsonArrayfpay1 = new JSONArray(jsonArrayfpay.toString());
            System.out.println("Jsonarrayfpay: " + jsonArrayfpay1);
          //  MySQLiteHelper.initDB(mContext);
//            for (int i = 0; i <10; i++) {
            for (int i = 0; i < jsonArrayfpay1.length(); i++) {
                String noloan = jsonArrayfpay1.getString(i);
                JSONObject jsonNoLoanFpay = new JSONObject(noloan);

                Detil_TGT dtgt = new Detil_TGT();
                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("fpay"));
                dtgt.setNo_loan(jsonNoLoanFpay.getString("no_loan"));
//                dtgt.setTgl_kunjungan("");
                dtgt.setStatus("0");
                Detil_TGT querydtgt = new Detil_TGT();
                querydtgt.insertDetilTGT(dtgt);
                // querydtgt.deleteAllDetilTGT();

                System.out.println("No_Loan fpay: " + jsonNoLoanFpay.getString("no_loan"));
            }

            JSONArray jsonArraynvst = jsonPenagihan.getJSONArray("nvst");
            JSONArray jsonArraynvst1 = new JSONArray(jsonArraynvst.toString());
            System.out.println("Jsonarraynvst: " + jsonArraynvst1);
//            for (int i = 0; i < 10; i++) {
           // MySQLiteHelper.initDB(mContext);
            for (int i = 0; i < jsonArraynvst1.length(); i++) {
                String noloan = jsonArraynvst1.getString(i);
                JSONObject jsonNoLoannvst = new JSONObject(noloan);

                Detil_TGT dtgt = new Detil_TGT();
                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("nvst"));
                dtgt.setNo_loan(jsonNoLoannvst.getString("no_loan"));
                dtgt.setTgl_kunjungan("");
                dtgt.setStatus("0");
                Detil_TGT querydtgt = new Detil_TGT();
                querydtgt.insertDetilTGT(dtgt);
                // querydtgt.deleteAllDetilTGT();

                System.out.println("No_Loan nvst: " + jsonNoLoannvst.getString("no_loan"));
            }

            JSONArray jsonArrayppay = jsonPenagihan.getJSONArray("ppay");
            JSONArray jsonArrayppay1 = new JSONArray(jsonArrayppay.toString());
            System.out.println("Jsonarrayppay: " + jsonArrayppay1);
           // MySQLiteHelper.initDB(mContext);
            for (int i = 0; i < jsonArrayppay1.length(); i++) {
//            for (int i = 0; i < 10; i++) {
                String noloan = jsonArrayppay1.getString(i);
                JSONObject jsonNoLoanPpay = new JSONObject(noloan);

                Detil_TGT dtgt = new Detil_TGT();
                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("ppay"));
                dtgt.setNo_loan(jsonNoLoanPpay.getString("no_loan"));
//                dtgt.setTgl_kunjungan(jsonNoLoanPpay.getString(Constant.TGL_KUNJUNGAN));
                dtgt.setTgl_kunjungan("");
                dtgt.setStatus("0");
                Detil_TGT querydtgt = new Detil_TGT();
                querydtgt.insertDetilTGT(dtgt);
                // querydtgt.deleteAllDetilTGT();

                System.out.println("No_Loan ppay: " + jsonNoLoanPpay.getString("no_loan"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void syncUpdatedata(String hasilJson) {
        try {

            JSONObject jsonAll = new JSONObject(hasilJson);
//            String waktu = jsonAll.getString("waktu");
            String pencairan = jsonAll.getString("pencairan");
            String penagihan = jsonAll.getString("penagihan");
            String action_code = jsonAll.getString("action_code");
            System.out.println("Action Code: " + action_code);

            //TODO DATA ACTION CODE
            queryAC.insertActionCodeintoDB("action_code", action_code);




            //DATA PENCAIRAN
            JSONObject jsonPencairan = new JSONObject(pencairan);
            JSONArray jsonArrayPencairan = jsonPencairan.getJSONArray("data_pencairan");
            //System.out.println("JsonArrayPencairan: " + jsonArrayPencairan);
            JSONArray jsonArrayPencairan1 = new JSONArray(jsonArrayPencairan.toString());

            //System.out.println("JsonArrayPencairan1: " + jsonArrayPencairan1);

            Data_Pencairan queryDP = new Data_Pencairan();
            Pencairan queryPencairan = new Pencairan();

            for (int i = 0; i < jsonArrayPencairan1.length(); i++) {
                String dtlPencairan = jsonArrayPencairan1.getString(i);
                System.out.println("NILAI: " + i + "-" + dtlPencairan);

                JSONObject jsonDetilPencairan = new JSONObject(dtlPencairan);
                String cycleDetilPencairan = jsonDetilPencairan.getString("cycle");
                String detilInventoryPencairan = jsonDetilPencairan.getString("detail_inventory");
                //String jarakDetilPencairan = jsonDetilPencairan.getString("jarak");
                String osDetilPencairan = jsonDetilPencairan.getString("OS");
                String namaDetilPencairan = jsonDetilPencairan.getString("nama");
                String alamatDetilPencairan = jsonDetilPencairan.getString("alamat");


                String id_data_pencairan_generate = queryDP.generateIDDataPencairan();
                System.out.println("ID Pencairan Generate: " + id_data_pencairan_generate);
                Data_Pencairan dataPencairan = new Data_Pencairan();
                dataPencairan.setId_data_pencairan(id_data_pencairan_generate);
                dataPencairan.setCycle(cycleDetilPencairan);
                //  dataPencairan.setJarak(jarakDetilPencairan);
                dataPencairan.setOs(osDetilPencairan);
                dataPencairan.setNama(namaDetilPencairan);
                dataPencairan.setAlamat(alamatDetilPencairan);
                dataPencairan.setStatus("0");
//                queryDP.deleteAllDataPencairan();
                queryDP.insertDataPencairan(dataPencairan);

                JSONArray jsonArrDetInvPencairan = new JSONArray(detilInventoryPencairan.toString());
                for (int j = 0; j < jsonArrDetInvPencairan.length(); j++) {
                    String isiDetInvPencairan = jsonArrDetInvPencairan.getString(j);
                    JSONObject jsonIsiDetInvPencairan = new JSONObject(isiDetInvPencairan);

                    //MASUKIN KE DETAIL INVENTORY DAN KE DETAIL BUCKET DARI NILAI DATA PENCAIRAN

                    Detail_Inventory dip = new Detail_Inventory();
                    dip.setSaldo_min(jsonIsiDetInvPencairan.getString(Constant.SALDO_MIN));
                    dip.setDpd_today(jsonIsiDetInvPencairan.getString(Constant.DPD_TODAY));
                    dip.setResume_nsbh(jsonIsiDetInvPencairan.getString(Constant.RESUME_NSBH));
                    dip.setAlmt_rumah(jsonIsiDetInvPencairan.getString(Constant.ALMT_RUMAH));
                    dip.setTgl_janji_bayar_terakhir(jsonIsiDetInvPencairan.getString(Constant.TGL_JANJI_BAYAR_TERAKHIR));
                    dip.setDenda(jsonIsiDetInvPencairan.getString(Constant.DENDA));
                    dip.setPola_bayar(jsonIsiDetInvPencairan.getString(Constant.POLA_BAYAR));

                    dip.setTgl_bayar_terakhir(jsonIsiDetInvPencairan.getString(Constant.TGL_BAYAR_TERAKHIR));

                    dip.setPokok(jsonIsiDetInvPencairan.getString(Constant.POKOK));
                    dip.setTgl_sp(jsonIsiDetInvPencairan.getString(Constant.TGL_SP));
                    dip.setTenor(jsonIsiDetInvPencairan.getString(Constant.TENOR));
                    dip.setAngsuran(jsonIsiDetInvPencairan.getString(Constant.ANGSURAN));

                    //dip.setTgl_ptp(jsonIsiDetInvPencairan.getString(Constant.TGL_PTP));
                    dip.setAction_plan(jsonIsiDetInvPencairan.getString(Constant.ACTION_PLAN));
                    dip.setNo_rekening(jsonIsiDetInvPencairan.getString(Constant.NO_REKENING));
                    dip.setHistori_sp(jsonIsiDetInvPencairan.getString(Constant.HISTORI_SP));
                    dip.setNominal_janji_bayar_terakhir(jsonIsiDetInvPencairan.getString(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR));
                    dip.setTlpn_rekomendator(jsonIsiDetInvPencairan.getString(Constant.TLPN_REKOMENDATOR));

                    dip.setSumber_bayar(jsonIsiDetInvPencairan.getString(Constant.SUMBER_BAYAR));
                    dip.setBunga(jsonIsiDetInvPencairan.getString(Constant.BUNGA));
//
                    dip.setJns_pinjaman(jsonIsiDetInvPencairan.getString(Constant.JNS_PINJAMAN));
                    dip.setNominal_bayar_terakhir(jsonIsiDetInvPencairan.getString(Constant.NOMINAL_BAYAR_TERAKHIR));
//
                    //dip.setNominal_ptp(jsonIsiDetInvPencairan.getString(Constant.NOMINAL_PTP));
                    dip.setSaldo(jsonIsiDetInvPencairan.getString(Constant.SALDO));

                    dip.setOs_pinjaman(jsonIsiDetInvPencairan.getString(Constant.OS_PINJAMAN));
//
                    dip.setKeberadaan_jaminan(jsonIsiDetInvPencairan.getString(Constant.KEBERADAAN_JAMINAN));
                    dip.setEmail(jsonIsiDetInvPencairan.getString(Constant.SUMBER_BAYAR));
//
                    dip.setTotal_kewajiban(jsonIsiDetInvPencairan.getString(Constant.TOTAL_KEWAJIBAN));
//
                    dip.setNo_loan(jsonIsiDetInvPencairan.getString(Constant.NO_LOAN));
                    //   dd.setNo_loan(noloangenerated);
                    dip.setNama_debitur(jsonIsiDetInvPencairan.getString(Constant.NAMA_DEBITUR));
                    dip.setTgl_jth_tempo(jsonIsiDetInvPencairan.getString(Constant.TGL_JTH_TEMPO));
                    dip.setNo_tlpn(jsonIsiDetInvPencairan.getString(Constant.NO_TLPN));
                    dip.setTgl_gajian(jsonIsiDetInvPencairan.getString(Constant.TGL_GAJIAN));
                    dip.setPekerjaan(jsonIsiDetInvPencairan.getString(Constant.PEKERJAAN));
                    dip.setAlmt_usaha(jsonIsiDetInvPencairan.getString(Constant.ALMT_USAHA));
                    dip.setId_data_pencairan(id_data_pencairan_generate);
                    dip.setStatus("0");


                    //data yang tadinya tidak ada
                    dip.setNo_hp(jsonIsiDetInvPencairan.getString(Constant.NO_HP));
                    dip.setAngsuran_ke(jsonIsiDetInvPencairan.getString(Constant.ANGSURAN_KE));
                    dip.setNama_upliner(jsonIsiDetInvPencairan.getString(Constant.NAMA_UPLINER));
                    dip.setGender(jsonIsiDetInvPencairan.getString(Constant.GENDER));
                    dip.setTlpn_upliner(jsonIsiDetInvPencairan.getString(Constant.TLPN_UPLINER));
                    dip.setTlpn_econ(jsonIsiDetInvPencairan.getString(Constant.TLPN_ECON));
                    dip.setHarus_bayar(jsonIsiDetInvPencairan.getString(Constant.HARUS_BAYAR));
                    dip.setNama_rekomendator(jsonIsiDetInvPencairan.getString(Constant.NAMA_REKOMENDATOR));
                    dip.setTlpn_mogen(jsonIsiDetInvPencairan.getString(Constant.TLPN_MOGEN));
                    dip.setNama_econ(jsonIsiDetInvPencairan.getString(Constant.NAMA_ECON));
                    dip.setKewajiban(jsonIsiDetInvPencairan.getString(Constant.KEWAJIBAN));
                    dip.setNama_mogen(jsonIsiDetInvPencairan.getString(Constant.NAMA_MOGEN));
                    dip.setBucket(jsonIsiDetInvPencairan.getString("bucket"));
                    dip.setTgl_cair(jsonIsiDetInvPencairan.getString("tgl_cair"));

                    querydetinv.insertDetailInventoryPencairan(dip);
                }
            }

            JSONArray jsonArraySS = jsonPencairan.getJSONArray("ss");
            JSONArray jsonArraySS1 = new JSONArray(jsonArraySS.toString());
            System.out.println("Jsonarrayss: " + jsonArraySS1);
            for (int i = 0; i < jsonArraySS1.length(); i++) {
                String noloan = jsonArraySS1.getString(i);
                JSONObject jsonNoLoanSS = new JSONObject(noloan);
                System.out.println("No_Loan ss: " + jsonNoLoanSS.getString("no_loan"));


                Detil_Data_Pencairan ddp = new Detil_Data_Pencairan();
                ddp.setId_data_pencairan(queryPencairan.getIdPencairanfromNama("ss"));
                ddp.setNo_loan(jsonNoLoanSS.getString("no_loan"));
                ddp.setTgl_kunjungan("");
                ddp.setStatus("0");
                Detil_Data_Pencairan queryddp = new Detil_Data_Pencairan();
                queryddp.insertDetilDataPencairan(ddp);
            }

            JSONArray jsonArrayBI = jsonPencairan.getJSONArray("bi");
            JSONArray jsonArrayBI1 = new JSONArray(jsonArrayBI.toString());
            System.out.println("Jsonarraybi: " + jsonArrayBI1);
            for (int i = 0; i < jsonArrayBI1.length(); i++) {
                String noloan = jsonArrayBI1.getString(i);
                JSONObject jsonNoLoanBI = new JSONObject(noloan);
                System.out.println("No_Loan bi: " + jsonNoLoanBI.getString("no_loan"));


                Detil_Data_Pencairan ddp = new Detil_Data_Pencairan();
                ddp.setId_data_pencairan(queryPencairan.getIdPencairanfromNama("bi"));
                ddp.setNo_loan(jsonNoLoanBI.getString("no_loan"));
                ddp.setTgl_kunjungan("");
                ddp.setStatus("0");
                Detil_Data_Pencairan queryddp = new Detil_Data_Pencairan();
                queryddp.insertDetilDataPencairan(ddp);
            }

            JSONArray jsonArrayBS = jsonPencairan.getJSONArray("bs");
            JSONArray jsonArrayBS1 = new JSONArray(jsonArrayBS.toString());
            System.out.println("Jsonarraybs: " + jsonArrayBS1);
            for (int i = 0; i < jsonArrayBS1.length(); i++) {
                String noloan = jsonArrayBS1.getString(i);
                JSONObject jsonNoLoanBS = new JSONObject(noloan);
                System.out.println("No_Loan bs: " + jsonNoLoanBS.getString("no_loan"));

                Detil_Data_Pencairan ddp = new Detil_Data_Pencairan();
                ddp.setId_data_pencairan(queryPencairan.getIdPencairanfromNama("bs"));
                ddp.setNo_loan(jsonNoLoanBS.getString("no_loan"));
                ddp.setTgl_kunjungan("");
                ddp.setStatus("0");
                Detil_Data_Pencairan queryddp = new Detil_Data_Pencairan();
                queryddp.insertDetilDataPencairan(ddp);
            }

            JSONArray jsonArrayBLBS = jsonPencairan.getJSONArray("blbs");
            JSONArray jsonArrayBLBS1 = new JSONArray(jsonArrayBLBS.toString());
            System.out.println("Jsonarrayblbs: " + jsonArrayBLBS1);
            for (int i = 0; i < jsonArrayBLBS1.length(); i++) {
                String noloan = jsonArrayBLBS1.getString(i);
                JSONObject jsonNoLoanBLBS = new JSONObject(noloan);
                System.out.println("No_Loan blbs: " + jsonNoLoanBLBS.getString("no_loan"));

                Detil_Data_Pencairan ddp = new Detil_Data_Pencairan();
                ddp.setId_data_pencairan(queryPencairan.getIdPencairanfromNama("blbs"));
                ddp.setNo_loan(jsonNoLoanBLBS.getString("no_loan"));
                ddp.setTgl_kunjungan("");
                ddp.setStatus("0");
                Detil_Data_Pencairan queryddp = new Detil_Data_Pencairan();
                queryddp.insertDetilDataPencairan(ddp);
            }

            //DATA PENAGIHAN
            JSONObject jsonPenagihan = new JSONObject(penagihan);
            JSONArray jsonArrayPenagihan = jsonPenagihan.getJSONArray("tgt");
            System.out.println("JsonArrayPenagihan: " + jsonArrayPenagihan);
            JSONArray jsonArrayPenagihan1 = new JSONArray(jsonArrayPenagihan.toString());
            System.out.println("JsonArrayPenagihan1: " + jsonArrayPenagihan1);
            TGT queryTGT = new TGT();
            Penagihan queryPenagihan = new Penagihan();
//            String noloangenerated = d.generateNoLoanSqlite();

            for (int i = 0; i < jsonArrayPenagihan1.length(); i++) {
                // for (int i = 0; i < 10; i++) {
//
                String dtlPenagihan = jsonArrayPenagihan1.getString(i);
                JSONObject jsonDetilPenagihan = new JSONObject(dtlPenagihan);
                String cycleDetilPenagihan = jsonDetilPenagihan.getString("cycle");
                String detilInventoryPenagihan = jsonDetilPenagihan.getString("detail_inventory");
//                String jarakDetilPenagihan = jsonDetilPenagihan.getString("jarak");
                String osDetilPenagihan = jsonDetilPenagihan.getString("OS");
                String namaDetilPenagihan = jsonDetilPenagihan.getString("nama");
                String alamatDetilPenagihan = jsonDetilPenagihan.getString("alamat");

                TGT dataTGT = new TGT();
                String id_tgt_generate = queryTGT.generateIDTGT();
                System.out.println("id_tgt_generate: " + id_tgt_generate);

                dataTGT.setId_tgt(id_tgt_generate);
                dataTGT.setCycle(cycleDetilPenagihan);
//                dataTGT.setJarak(jarakDetilPenagihan);
                dataTGT.setOs(osDetilPenagihan);
                dataTGT.setNama(namaDetilPenagihan);
                dataTGT.setAlamat(alamatDetilPenagihan);
                dataTGT.setStatus("0");
//                queryTGT.deleteAllTGT();
                queryTGT.insertIdTGT(dataTGT);

                JSONArray jsonArrDetInvPenagihan = new JSONArray(detilInventoryPenagihan.toString());
//                for (int j = 0; j < 10; j++) {
                for (int j = 0; j < jsonArrDetInvPenagihan.length(); j++) {
                    String isiDetInvPenagihan = jsonArrDetInvPenagihan.getString(j);
                    JSONObject jsonIsiDetInvPenagihan = new JSONObject(isiDetInvPenagihan);

                    //System.out.println("Saldo Min PenagihanLama : " + j + "-" + jsonIsiDetInvPenagihan.getString("saldo_min"));
                    String saldoMin = jsonIsiDetInvPenagihan.getString("saldo_min");
                    //MASUKIN KE DETAIL INVENTORY DAN KE DETAIL BUCKET DARI NILAI DATA PENAGIHAN


                    Detail_Inventory dd = new Detail_Inventory();
                    dd.setSaldo_min(jsonIsiDetInvPenagihan.getString(Constant.SALDO_MIN));
                    dd.setDpd_today(jsonIsiDetInvPenagihan.getString(Constant.DPD_TODAY));
                    dd.setResume_nsbh(jsonIsiDetInvPenagihan.getString(Constant.RESUME_NSBH));
                    dd.setAlmt_rumah(jsonIsiDetInvPenagihan.getString(Constant.ALMT_RUMAH));
                    dd.setTgl_janji_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.TGL_JANJI_BAYAR_TERAKHIR));
                    dd.setDenda(jsonIsiDetInvPenagihan.getString(Constant.DENDA));
                    dd.setPola_bayar(jsonIsiDetInvPenagihan.getString(Constant.POLA_BAYAR));

                    dd.setTgl_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.TGL_BAYAR_TERAKHIR));

                    dd.setPokok(jsonIsiDetInvPenagihan.getString(Constant.POKOK));
                    dd.setTgl_sp(jsonIsiDetInvPenagihan.getString(Constant.TGL_SP));
                    dd.setTenor(jsonIsiDetInvPenagihan.getString(Constant.TENOR));
                    dd.setAngsuran(jsonIsiDetInvPenagihan.getString(Constant.ANGSURAN));

                    // dd.setTgl_ptp(jsonIsiDetInvPenagihan.getString(Constant.TGL_PTP));
                    dd.setAction_plan(jsonIsiDetInvPenagihan.getString(Constant.ACTION_PLAN));
                    dd.setNo_rekening(jsonIsiDetInvPenagihan.getString(Constant.NO_REKENING));
                    dd.setHistori_sp(jsonIsiDetInvPenagihan.getString(Constant.HISTORI_SP));
                    dd.setNominal_janji_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.NOMINAL_JANJI_BAYAR_TERAKHIR));
                    dd.setTlpn_rekomendator(jsonIsiDetInvPenagihan.getString(Constant.TLPN_REKOMENDATOR));

                    dd.setSumber_bayar(jsonIsiDetInvPenagihan.getString(Constant.SUMBER_BAYAR));
                    dd.setBunga(jsonIsiDetInvPenagihan.getString(Constant.BUNGA));
//
                    dd.setJns_pinjaman(jsonIsiDetInvPenagihan.getString(Constant.JNS_PINJAMAN));
                    dd.setNominal_bayar_terakhir(jsonIsiDetInvPenagihan.getString(Constant.NOMINAL_BAYAR_TERAKHIR));
//
                    //  dd.setNominal_ptp(jsonIsiDetInvPenagihan.getString(Constant.NOMINAL_PTP));
                    dd.setSaldo(jsonIsiDetInvPenagihan.getString(Constant.SALDO));
//
                    dd.setOs_pinjaman(jsonIsiDetInvPenagihan.getString(Constant.OS_PINJAMAN));
//
                    dd.setKeberadaan_jaminan(jsonIsiDetInvPenagihan.getString(Constant.KEBERADAAN_JAMINAN));
                    dd.setEmail(jsonIsiDetInvPenagihan.getString(Constant.SUMBER_BAYAR));
//
                    dd.setTotal_kewajiban(jsonIsiDetInvPenagihan.getString(Constant.TOTAL_KEWAJIBAN));
//
                    dd.setNo_loan(jsonIsiDetInvPenagihan.getString(Constant.NO_LOAN));
                    //   dd.setNo_loan(noloangenerated);
                    dd.setNama_debitur(jsonIsiDetInvPenagihan.getString(Constant.NAMA_DEBITUR));
                    dd.setTgl_jth_tempo(jsonIsiDetInvPenagihan.getString(Constant.TGL_JTH_TEMPO));
                    dd.setNo_tlpn(jsonIsiDetInvPenagihan.getString(Constant.NO_TLPN));
//
                    dd.setId_tgt(id_tgt_generate);
                    dd.setStatus("0");


                    dd.setNo_hp(jsonIsiDetInvPenagihan.getString(Constant.NO_HP));
                    dd.setAngsuran_ke(jsonIsiDetInvPenagihan.getString(Constant.ANGSURAN_KE));
                    dd.setNama_upliner(jsonIsiDetInvPenagihan.getString(Constant.NAMA_UPLINER));
                    dd.setGender(jsonIsiDetInvPenagihan.getString(Constant.GENDER));
                    dd.setTlpn_upliner(jsonIsiDetInvPenagihan.getString(Constant.TLPN_UPLINER));
                    dd.setTlpn_econ(jsonIsiDetInvPenagihan.getString(Constant.TLPN_ECON));
                    dd.setHarus_bayar(jsonIsiDetInvPenagihan.getString(Constant.HARUS_BAYAR));
                    dd.setNama_rekomendator(jsonIsiDetInvPenagihan.getString(Constant.NAMA_REKOMENDATOR));
                    dd.setTlpn_mogen(jsonIsiDetInvPenagihan.getString(Constant.TLPN_MOGEN));
                    dd.setNama_econ(jsonIsiDetInvPenagihan.getString(Constant.NAMA_ECON));
                    dd.setKewajiban(jsonIsiDetInvPenagihan.getString(Constant.KEWAJIBAN));
                    dd.setNama_mogen(jsonIsiDetInvPenagihan.getString(Constant.NAMA_MOGEN));
                    dd.setTgl_gajian(jsonIsiDetInvPenagihan.getString(Constant.TGL_GAJIAN));
                    dd.setPekerjaan(jsonIsiDetInvPenagihan.getString(Constant.PEKERJAAN));
                    dd.setAlmt_usaha(jsonIsiDetInvPenagihan.getString(Constant.ALMT_USAHA));
                    dd.setBucket(jsonIsiDetInvPenagihan.getString("bucket"));
                    querydetinv.insertDetailInventoryIdTGT(dd);
//                    d.deleteAllDetailInventory();
                }
            }

            JSONArray jsonArraynpay = jsonPenagihan.getJSONArray("npay");
            JSONArray jsonArraynpay1 = new JSONArray(jsonArraynpay.toString());
            System.out.println("Jsonarraynpay: " + jsonArraynpay1);
            //   MySQLiteHelper.initDB(mContext);
//            for (int i = 0; i < 10; i++) {
            for (int i = 0; i < jsonArraynpay1.length(); i++) {
                String noloan = jsonArraynpay1.getString(i);
                JSONObject jsonNoLoanNpay = new JSONObject(noloan);

                Detil_TGT dtgt = new Detil_TGT();
                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("npay"));
                dtgt.setNo_loan(jsonNoLoanNpay.getString("no_loan"));
                dtgt.setTgl_kunjungan("");
                dtgt.setStatus("0");
                Detil_TGT querydtgt = new Detil_TGT();
                querydtgt.insertDetilTGT(dtgt);
                // querydtgt.deleteAllDetilTGT();
                System.out.println("No_Loan npay: " + jsonNoLoanNpay.getString("no_loan"));
            }

            JSONArray jsonArrayfpay = jsonPenagihan.getJSONArray("fpay");
            JSONArray jsonArrayfpay1 = new JSONArray(jsonArrayfpay.toString());
            System.out.println("Jsonarrayfpay: " + jsonArrayfpay1);
            //  MySQLiteHelper.initDB(mContext);
//            for (int i = 0; i <10; i++) {
            for (int i = 0; i < jsonArrayfpay1.length(); i++) {
                String noloan = jsonArrayfpay1.getString(i);
                JSONObject jsonNoLoanFpay = new JSONObject(noloan);

                Detil_TGT dtgt = new Detil_TGT();
                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("fpay"));
                dtgt.setNo_loan(jsonNoLoanFpay.getString("no_loan"));
//                dtgt.setTgl_kunjungan("");
                dtgt.setStatus("0");
                Detil_TGT querydtgt = new Detil_TGT();
                querydtgt.insertDetilTGT(dtgt);
                // querydtgt.deleteAllDetilTGT();

                System.out.println("No_Loan fpay: " + jsonNoLoanFpay.getString("no_loan"));
            }

            JSONArray jsonArraynvst = jsonPenagihan.getJSONArray("nvst");
            JSONArray jsonArraynvst1 = new JSONArray(jsonArraynvst.toString());
            System.out.println("Jsonarraynvst: " + jsonArraynvst1);
//            for (int i = 0; i < 10; i++) {
            // MySQLiteHelper.initDB(mContext);
            for (int i = 0; i < jsonArraynvst1.length(); i++) {
                String noloan = jsonArraynvst1.getString(i);
                JSONObject jsonNoLoannvst = new JSONObject(noloan);

                Detil_TGT dtgt = new Detil_TGT();
                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("nvst"));
                dtgt.setNo_loan(jsonNoLoannvst.getString("no_loan"));
                dtgt.setTgl_kunjungan("");
                dtgt.setStatus("0");
                Detil_TGT querydtgt = new Detil_TGT();
                querydtgt.insertDetilTGT(dtgt);
                // querydtgt.deleteAllDetilTGT();

                System.out.println("No_Loan nvst: " + jsonNoLoannvst.getString("no_loan"));
            }

            JSONArray jsonArrayppay = jsonPenagihan.getJSONArray("ppay");
            JSONArray jsonArrayppay1 = new JSONArray(jsonArrayppay.toString());
            System.out.println("Jsonarrayppay: " + jsonArrayppay1);
            // MySQLiteHelper.initDB(mContext);
            for (int i = 0; i < jsonArrayppay1.length(); i++) {
//            for (int i = 0; i < 10; i++) {
                String noloan = jsonArrayppay1.getString(i);
                JSONObject jsonNoLoanPpay = new JSONObject(noloan);

                Detil_TGT dtgt = new Detil_TGT();
                dtgt.setId_penagihan(queryPenagihan.getIdPenagihanfromNama("ppay"));
                dtgt.setNo_loan(jsonNoLoanPpay.getString("no_loan"));
//                dtgt.setTgl_kunjungan(jsonNoLoanPpay.getString(Constant.TGL_KUNJUNGAN));
                dtgt.setTgl_kunjungan("");
                dtgt.setStatus("0");
                Detil_TGT querydtgt = new Detil_TGT();
                querydtgt.insertDetilTGT(dtgt);
                // querydtgt.deleteAllDetilTGT();

                System.out.println("No_Loan ppay: " + jsonNoLoanPpay.getString("no_loan"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
