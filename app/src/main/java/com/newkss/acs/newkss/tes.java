package com.newkss.acs.newkss;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.newkss.acs.newkss.Adapter.Bucket;
import com.newkss.acs.newkss.Adapter.MenuBucketAdapter;
import com.newkss.acs.newkss.Adapter.MenuBucketAdapterBackup;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by acs on 25/04/17.
 */

public class tes extends ParentActivity {

    ListView list;
    MenuBucketAdapterBackup adapter;
    EditText editsearch;
    ArrayList<Bucket> listBucket = new ArrayList<Bucket>();
    Bucket bucket;
    EditText etSearchMenuListBucket;
    ImageView imgSearchMenuListBucket;
String text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menulistbucketbackup);
        add();
        list = (ListView) findViewById(R.id.listBucket);
        adapter = new MenuBucketAdapterBackup(getApplicationContext(), listBucket, bucket);
        list.setAdapter(adapter);
        editsearch = (EditText) findViewById(R.id.etSearchMenuListBucket);
        imgSearchMenuListBucket = (ImageView) findViewById(R.id.imgSearchMenuListBucket);
        imgSearchMenuListBucket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text = editsearch.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);

            }
        });

        editsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                text = editsearch.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
                Toast(getApplicationContext(), text);
//
            }
        });



        //  etSearchMenuListBucket = (EditText) findViewById(R.id.etSearchMenuListBucket);

    }

    private void add() {
//        listBucket = new ArrayList<>();
//        bucket = new Bucket();
//        bucket.setNama("Anton");
//        bucket.setCycle("17");
//        bucket.setOS("Rp 32.000.000");
//        bucket.setTotalBayar("1.434.000");
//        bucket.setAlamat("Jl. Tebet Timur no 289, Gedung Century Blok A III 19, Kelurahan Timur Dalam, Kecamatan Tomang,  Jakarta Selatan 50123");
//        listBucket.add(bucket);
//
//        bucket = new Bucket();
//        bucket.setNama("Cecep");
//        bucket.setCycle("19");
//        bucket.setOS("Rp 12.000.000");
//        bucket.setTotalBayar("4.439.000");
//        bucket.setAlamat("Jl. Tebet Timur no 289, Gedung Century Blok A III 19, Kelurahan Timur Dalam, Kecamatan Tomang,  Jakarta Selatan 50123");
//        listBucket.add(bucket);

        listBucket = new ArrayList<>();
        listBucket.add(new Bucket("Anton", "17", "Rp 32.000.000", "1.434.000", "Jl. Tebet Timur no 289, Gedung Century Blok A III 19, Kelurahan Timur Dalam, Kecamatan Tomang,  Jakarta Selatan 50123"));
        listBucket.add(new Bucket("Cecep", "19", "Rp 21.000.000", "4.085.000", "Jl. Manggarai no 289, Gedung Century Blok A III 19, Kelurahan Timur Dalam, Kecamatan Tomang,  Jakarta Selatan 50123"));


//        bucket = new Bucket();
//        bucket.setNama("Jan");
//        bucket.setCycle("17");
//        bucket.setOS("Rp 32.000.000");
//        bucket.setTotalBayar("1.434.000");
//        bucket.setAlamat("Jl. Tebet Timur no 289, Gedung Century Blok A III 19, Kelurahan Timur Dalam, Kecamatan Tomang,  Jakarta Selatan 50123");
//        listBucket.add(bucket);
//
//        bucket = new Bucket();
//        bucket.setNama("Roy");
//        bucket.setCycle("19");
//        bucket.setOS("Rp 12.000.000");
//        bucket.setTotalBayar("4.439.000");
//        bucket.setAlamat("Jl. Tebet Timur no 289, Gedung Century Blok A III 19, Kelurahan Timur Dalam, Kecamatan Tomang,  Jakarta Selatan 50123");
//        listBucket.add(bucket);
//
//        bucket = new Bucket();
//        bucket.setNama("Jaka");
//        bucket.setCycle("17");
//        bucket.setOS("Rp 32.000.000");
//        bucket.setTotalBayar("1.434.000");
//        bucket.setAlamat("Jl. Tebet Timur no 289, Gedung Century Blok A III 19, Kelurahan Timur Dalam, Kecamatan Tomang,  Jakarta Selatan 50123");
//        listBucket.add(bucket);
//
//        bucket = new Bucket();
//        bucket.setNama("Martin");
//        bucket.setCycle("19");
//        bucket.setOS("Rp 12.000.000");
//        bucket.setTotalBayar("4.439.000");
//        bucket.setAlamat("Jl. Tebet Timur no 289, Gedung Century Blok A III 19, Kelurahan Timur Dalam, Kecamatan Tomang,  Jakarta Selatan 50123");
//        listBucket.add(bucket);
    }
}
