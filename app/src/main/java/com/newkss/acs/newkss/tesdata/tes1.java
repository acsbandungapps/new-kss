package com.newkss.acs.newkss.tesdata;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ListView;

import com.newkss.acs.newkss.Adapter.Bucket;
import com.newkss.acs.newkss.R;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by acs on 26/04/17.
 */

public class tes1 extends Activity{

    // Declare Variables
    ListView list;
    ListViewAdapter adapter;
    EditText editsearch;
    String[] rank;
    String[] country;
    String[] population;
    ArrayList<WorldPopulation> arraylist = new ArrayList<WorldPopulation>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_main);

        // Generate sample data
//        rank = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
//
//        country = new String[] { "China", "India", "United States",
//                "Indonesia", "Brazil", "Pakistan", "Nigeria", "Bangladesh",
//                "Russia", "Japan" };
//
//        population = new String[] { "1,354,040,000", "1,210,193,422",
//                "315,761,000", "237,641,326", "193,946,886", "182,912,000",
//                "170,901,000", "152,518,015", "143,369,806", "127,360,000" };
//
//        // Locate the ListView in listview_main.xml
  list = (ListView) findViewById(R.id.listview);
//
//        for (int i = 0; i < rank.length; i++)
//        {
//            WorldPopulation wp = new WorldPopulation(rank[i], country[i],
//                    population[i]);
//            // Binds all strings into an array
//            arraylist.add(wp);
//        }

        add();
        // Pass results to ListViewAdapter Class
        adapter = new ListViewAdapter(this, arraylist);

        // Binds the Adapter to the ListView
        list.setAdapter(adapter);

        // Locate the EditText in listview_main.xml
        editsearch = (EditText) findViewById(R.id.search);

        // Capture Text in EditText
        editsearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = editsearch.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });
    }

    public void add(){
        arraylist = new ArrayList<>();
        arraylist.add(new WorldPopulation("1", "Anton", "Rp 32.000.000"));
        arraylist.add(new WorldPopulation("2", "Cecep", "Rp 21.000.000"));

    }
}
