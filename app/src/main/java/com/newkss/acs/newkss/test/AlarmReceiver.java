package com.newkss.acs.newkss.test;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;

import com.newkss.acs.newkss.Maps.MapsJarak;
import com.newkss.acs.newkss.Service.ServiceSendLocation;
import com.newkss.acs.newkss.Util.Constant;
import com.newkss.acs.newkss.Util.Function;
import com.newkss.acs.newkss.Util.JSONParser;

import org.json.JSONObject;

/**
 * Created by acs on 7/9/17.
 */

public class AlarmReceiver extends BroadcastReceiver {
    int count = 0;
    SharedPreferences shared;
    String usernameShared, passwordShared, nikShared, hasilJson;
    ServiceSendLocation querySSL;
    JSONParser jsonParser = new JSONParser();
    Location location;
    Double latitude, longitude;

    @Override
    public void onReceive(Context context, Intent intent) {

        querySSL = new ServiceSendLocation(context);
        location = querySSL.getLocation();
        shared = context.getSharedPreferences(Constant.MYPREF, context.MODE_PRIVATE);
        if (shared.contains(Constant.SHARED_USERNAME)) {
            usernameShared = (shared.getString(Constant.SHARED_USERNAME, ""));
            passwordShared = (shared.getString(Constant.SHARED_PASSWORD, ""));
            nikShared = (shared.getString(Constant.NIK, ""));

        }

        new sendLocation(context).execute();


//        contohservice c = new contohservice(context);
//        if (c.canGetLocation()) {
//            System.out.println("Berhasil");
//            System.out.println("Latitude: " + c.getLatitude()); // returns latitude
//            System.out.println("Longitude: " + c.getLongitude());
//        } else {
//            System.out.println("Gagal");
//        }
        System.out.println("Send Location " + Function.getDateNow());
//        count += count;
    }

    public class sendLocation extends AsyncTask<String, String, String> {
        Context context;

        private sendLocation(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected String doInBackground(String... strings) {




//            if (querySSL.canGetLocation()) {
            JSONObject json = new JSONObject();
            try {
                String header = Function.getImei(context) + ":" + nikShared + ":" + Function.getWaktuSekarangMilis();
                System.out.println("Header: " + header);
                System.out.println("Header base64: " + new String(Function.encodeBase64(header)));
                json.put(Constant.USERNAME, usernameShared);
                json.put(Constant.PASSWORD, passwordShared);
                if (location == null) {
                    latitude = 0.0;
                    longitude = 0.0;
                    location = querySSL.getLastKnownLocation();
                    latitude=location.getLatitude();
                    longitude=location.getLongitude();
//                    if (location == null) {
//                        latitude = 0.0;
//                        longitude = 0.0;
//                    }
                } else {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
//                    latitude = querySSL.getLatitude();
//                    longitude = querySSL.getLongitude();
                json.put("latitude", latitude);
                json.put("longitude", longitude);
                json.put(Constant.WAKTU, Function.getWaktuSekarangMilis());

                System.out.println("Json Kirim sendLocation: " + json.toString());
                hasilJson = jsonParser.HttpRequestPost(Constant.SERVICE_TRACK, json.toString(), Constant.TimeOutConnection, new String(Function.encodeBase64(header)));
                System.out.println("Hasil Json sendLocation: " + hasilJson);
                return "Sukses";
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "Sukses";
//            } else {
//                Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
//                intent.putExtra("enabled", true);
//                context.sendBroadcast(intent);
//                querySSL.showSettingsAlert();
//            }
//            return "Sukses";

        }
    }
}
