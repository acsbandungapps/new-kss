package com.newkss.acs.newkss.test;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;

import static android.location.LocationManager.*;

/**
 * Created by acs on 7/6/17.
 */

public class sendlocation extends ParentActivity implements LocationListener{
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    protected Context context;

    TextView txtLat;
    String lat;
    String provider;
    Button btntes;
    protected String latitude,longitude;
    protected boolean gps_enabled,network_enabled;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tes4);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(GPS_PROVIDER, 0, 0, this);
        txtLat=(TextView) findViewById(R.id.txtLat);
        btntes=(Button)findViewById(R.id.btntes);
        btntes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            }
        });

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

//                myLocation = obtainLocation();
//                handler.postDelayed(this, HANDLER_DELAY);
            }
        }, 1000);



    }

    @Override
    public void onLocationChanged(Location location) {
        txtLat = (TextView) findViewById(R.id.txtLat);
        txtLat.setText("Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude());

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        Log.d("Latitude","status");
    }

    @Override
    public void onProviderEnabled(String s) {
        Log.d("Latitude","enable");
    }

    @Override
    public void onProviderDisabled(String s) {
        Log.d("Latitude","disable");
    }


    //    Location gpslocation = null;
//    LocationManager locMan;
//    private static final int GPS_TIME_INTERVAL = 60000; // get gps location every 1 min
//    private static final int GPS_DISTANCE = 1000; // set the distance value in meter
//    private static final int HANDLER_DELAY = 1000 * 60 * 5;
//
//    Button tes;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.tes4);
//
//        tes=(Button) findViewById(R.id.tes);
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            public void run() {
//                myLocation = obtainLocation();
//                handler.postDelayed(this, HANDLER_DELAY);
//            }
//        }, GPS_TIME_INTERVAL);
    //}

//    private void obtainLocation(){
//        if(locMan==null)
//            locMan = (LocationManager) getSystemService(LOCATION_SERVICE);
//
//        if(locMan.isProviderEnabled(LocationManager.GPS_PROVIDER)){
//            gpslocation = locMan.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//            if(isLocationListener){
//                locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER,
//                        GPS_TIME_INTERVAL, GPS_DISTANCE, GPSListener);
//            }
//        }
//    }


//    private LocationListener GPSListener = new LocationListener() {
//        public void onLocationChanged(Location location) {
//            // update location
//            locMan.removeUpdates(GPSListener); // remove this listener
//        }
//
//        public void onProviderDisabled(String provider) {
//        }
//
//        public void onProviderEnabled(String provider) {
//        }
//
//        public void onStatusChanged(String provider, int status, Bundle extras) {
//        }
//    };
}
