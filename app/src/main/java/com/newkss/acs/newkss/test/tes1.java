package com.newkss.acs.newkss.test;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.newkss.acs.newkss.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Erdy on 7/2/2017.
 */

public class tes1 extends Activity {
    Button btnSubmit;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tes2);
        mContext = this;
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(mContext, fragdialog.class);
                startActivity(i);
                finish();

//                String tes = "[{\"distance\":{\"text\":\"4.1 km\",\"value\":4055},\"duration\":{\"text\":\"15 mins\",\"value\":890},\"end_address\":\"Jl. Aksan No.26, Sukahaji, Babakan Ciparay, Kota Bandung, Jawa Barat 40221, Indonesia\",\"end_location\":{\"lat\":-6.920003899999999,\"lng\":107.5812377},\"start_address\":\"Jl. Leuwi Sari IX, Kb. Lega, Bojongloa Kidul, Kota Bandung, Jawa Barat 40235, Indonesia\",\"start_location\":{\"lat\":-6.9450637,\"lng\":107.5977436},\"steps\":[{\"distance\":{\"text\":\"80 m\",\"value\":80},\"duration\":{\"text\":\"1 min\",\"value\":29},\"end_location\":{\"lat\":-6.9449481,\"lng\":107.5970262},\"html_instructions\":\"Head <b>west<\\/b> on <b>Jl. Leuwi Sari IX<\\/b> toward <b>Jl. Leuwisari III<\\/b>\",\"polyline\":{\"points\":\"rmki@{dvoSUlC\"},\"start_location\":{\"lat\":-6.9450637,\"lng\":107.5977436},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"43 m\",\"value\":43},\"duration\":{\"text\":\"1 min\",\"value\":11},\"end_location\":{\"lat\":-6.9445671,\"lng\":107.5971102},\"html_instructions\":\"Turn <b>right<\\/b> onto <b>Jl. Leuwisari III<\\/b>\",\"maneuver\":\"turn-right\",\"polyline\":{\"points\":\"|lki@m`voSkAO\"},\"start_location\":{\"lat\":-6.9449481,\"lng\":107.5970262},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.2 km\",\"value\":159},\"duration\":{\"text\":\"1 min\",\"value\":61},\"end_location\":{\"lat\":-6.9441149,\"lng\":107.5957504},\"html_instructions\":\"Turn <b>left<\\/b> at the 1st cross street onto <b>Jl. Leuwisari Raya<\\/b>\",\"maneuver\":\"turn-left\",\"polyline\":{\"points\":\"pjki@}`voSSp@Qn@[z@I^G^AZET\"},\"start_location\":{\"lat\":-6.9445671,\"lng\":107.5971102},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.7 km\",\"value\":744},\"duration\":{\"text\":\"2 mins\",\"value\":132},\"end_location\":{\"lat\":-6.9377735,\"lng\":107.5970236},\"html_instructions\":\"Turn <b>right<\\/b> onto <b>Jl. Leuwi Panjang<\\/b><div style=\\\"font-size:0.9em\\\">Pass by BANK BRI (on the left in 550&nbsp;m)<\\/div>\",\"maneuver\":\"turn-right\",\"polyline\":{\"points\":\"tgki@mxuoS?VyAKs@Go@Gc@KO?I?sAMGA]Ge@KYKSKA?w@a@yA_@C?eBWUCUBMFMDKBSAOCKA}BWSCWCIACAk@Iq@OqCc@\"},\"start_location\":{\"lat\":-6.9441149,\"lng\":107.5957504},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"1.8 km\",\"value\":1808},\"duration\":{\"text\":\"6 mins\",\"value\":357},\"end_location\":{\"lat\":-6.926810499999999,\"lng\":107.5854879},\"html_instructions\":\"Turn <b>left<\\/b> onto <b>Jl. Peta<\\/b><div style=\\\"font-size:0.9em\\\">Pass by Hybrid Autoconcept (on the left)<\\/div>\",\"maneuver\":\"turn-left\",\"polyline\":{\"points\":\"``ji@k`voSMjAG\\\\Ih@Mj@M\\\\_@~@[x@MVQ^_@x@g@jA_@r@a@t@KPwAdCQ\\\\g@fACDq@bAc@l@UXORk@n@KH}@x@y@l@y@l@{AhAeAt@k@b@o@h@sDjCoClBeAz@SLMFgAj@a@RSNaAf@QDGDc@PkAr@OJ]XiAn@YPeCvBeA~@SH\"},\"start_location\":{\"lat\":-6.9377735,\"lng\":107.5970236},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.5 km\",\"value\":512},\"duration\":{\"text\":\"1 min\",\"value\":78},\"end_location\":{\"lat\":-6.9222343,\"lng\":107.5858759},\"html_instructions\":\"Continue straight onto <b>Jl. Jamika<\\/b><div style=\\\"font-size:0.9em\\\">Pass by BPR Permata Dhanawira (on the left in 350&nbsp;m)<\\/div>\",\"maneuver\":\"straight\",\"polyline\":{\"points\":\"p{gi@ixsoSOBaADIAwAMWAkBE}DKYAu@EiACa@Ck@EuD]\"},\"start_location\":{\"lat\":-6.926810499999999,\"lng\":107.5854879},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.2 km\",\"value\":194},\"duration\":{\"text\":\"1 min\",\"value\":49},\"end_location\":{\"lat\":-6.9219008,\"lng\":107.5842535},\"html_instructions\":\"Turn <b>left<\\/b> toward <b>Jl. Aksan<\\/b><div style=\\\"font-size:0.9em\\\">Pass by Western Union (on the left)<\\/div>\",\"maneuver\":\"turn-left\",\"polyline\":{\"points\":\"|~fi@wzsoSKDC@C@E@CBCBCDCBABCTGh@EpDAPA^\"},\"start_location\":{\"lat\":-6.9222343,\"lng\":107.5858759},\"travel_mode\":\"DRIVING\"},{\"distance\":{\"text\":\"0.5 km\",\"value\":515},\"duration\":{\"text\":\"3 mins\",\"value\":173},\"end_location\":{\"lat\":-6.920003899999999,\"lng\":107.5812377},\"html_instructions\":\"Turn <b>right<\\/b> onto <b>Jl. Aksan<\\/b><div style=\\\"font-size:0.9em\\\">Destination will be on the left<\\/div>\",\"maneuver\":\"turn-right\",\"polyline\":{\"points\":\"z|fi@qpsoSuACIAaBIEAKA}@IGAA@C?EDGn@ATWlDYrEYhD\"},\"start_location\":{\"lat\":-6.9219008,\"lng\":107.5842535},\"travel_mode\":\"DRIVING\"}],\"traffic_speed_entry\":[],\"via_waypoint\":[]}]";
//                try {
//                    JSONArray jArr=new JSONArray(tes.toString());
//                    for(int i=0;i<jArr.length();i++){
//                        String isi=jArr.getString(i);
//                        System.out.println("ISI: "+isi);
//
//                        JSONObject json=new JSONObject(isi);
//                        String dis=json.getString("distance");
//                        JSONObject jsonDis=new JSONObject(dis);
//                        String textdis=jsonDis.getString("text");
//                        System.out.println("Text: "+textdis);
//                    }
//
////                    JSONObject jsonObject = new JSONObject(tes.toString());
////                    System.out.println(jsonObject);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }


//                Dialog dialog = new Dialog(mContext);
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setContentView(R.layout.tes3);
//                dialog.show();
//                GoogleMap googleMap;
//
//
//                MapView mMapView = (MapView) dialog.findViewById(R.id.mapView);
//                MapsInitializer.initialize(mContext);
//
//                mMapView = (MapView) dialog.findViewById(R.id.mapView);
//                mMapView.onCreate(dialog.onSaveInstanceState());
//                mMapView.onResume();// needed to get the map to display immediately
//                googleMap = mMapView.getMap();


                // new fragdialog().show(getApplicationContext(), null);

//                FragmentManager fm = getFragmentManager();
//                fragdialog dialogFragment = new fragdialog ();
//                dialogFragment.show(fm, "Sample Fragment");
//
//                fragdialog dialog = fragdialog.();
//                dialog.show(getActivity().getFragmentManager(), "MyDialogFragment");
            }
        });
    }
}
