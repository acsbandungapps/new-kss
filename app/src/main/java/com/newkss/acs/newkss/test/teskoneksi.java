package com.newkss.acs.newkss.test;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;
import com.newkss.acs.newkss.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by acs on 7/12/17.
 */

public class teskoneksi extends Activity {

    Button btnTes;
    static double latitude, longitute;
    Context mContext;
//    String alamat = "JL MANGGA BESAR IV Q/4 RT:3 RW:5 KELURAHAN:TAMAN SARI KECAMATAN:TAMAN SARI";

//   String alamat="LINGKUNGAN V PASAR GUNUNG TUA KELURAHAN:PASAR GUNUNG TUA KECAMATAN:PADANG BOLAK";
    String alamat="Gedung Monas";
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tes7);
        mContext = this;
        btnTes = (Button) findViewById(R.id.btnTes);
        btnTes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new getLokasi(mContext).execute();


//                if(latitude!=0.0 && longitute!=0.0){
//
//                }
//                else{
//                    System.out.println(latitude+" --- "+longitute);
//                }

                // Toast.makeText(getApplicationContext(),latitude+" --- "+longitute,Toast.LENGTH_SHORT).show();
            }
        });
    }

    class getLokasi extends AsyncTask<String, String, JSONObject> {
        Context context;

        public getLokasi(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage("Mengirim Data...");
            progressDialog.setTitle("Pesan");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            return getLocationInfo(alamat);
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            progressDialog.dismiss();
            getLatLong(jsonObject);
            System.out.println(latitude + " --- " + longitute);
            //System.out.println(longitute + " --- " + latitude);
        }
    }

    public static JSONObject getLocationInfo(String address) {
        StringBuilder stringBuilder = new StringBuilder();
        try {

            address = address.replaceAll(" ", "%20");

            HttpPost httppost = new HttpPost("http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false");
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            stringBuilder = new StringBuilder();


            response = client.execute(httppost);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }

        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println("Hasil Json: "+jsonObject);
        return jsonObject;
    }

    public static boolean getLatLong(JSONObject jsonObject) {
//        JSONObject jsonObject;
//        jsonObject = getLocationInfo("Jl Tebet Barat");
        try {

            longitute = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lng");

            latitude = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lat");

        } catch (JSONException e) {
            return false;

        }

        return true;
    }
}
