package com.newkss.acs.newkss.test;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.newkss.acs.newkss.DataTransferInterface;
import com.newkss.acs.newkss.Model.Settlement;
import com.newkss.acs.newkss.Model.SettlementKe;
import com.newkss.acs.newkss.R;

import java.util.ArrayList;

/**
 * Created by acs on 7/9/17.
 */

public class teslistadapter extends BaseAdapter {
String n="";
DataFound mDataFound;
    DataInterface di;
    Context mContext;
    LayoutInflater inflater;
    private ArrayList<SettlementKe> settlementList = null;
    private ArrayList<SettlementKe> arraylist=new ArrayList<>();

    public teslistadapter(Context context, ArrayList<SettlementKe> list) {
        mContext = context;
        settlementList = list;
        inflater = LayoutInflater.from(mContext);
    }

    public void setOnMyItemFoundListener(DataFound df){
        mDataFound=df;
    }
    @Override
    public int getCount() {
        return settlementList.size();
    }

    @Override
    public Object getItem(int i) {
        return settlementList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public class ViewHolder {
        public EditText settlementKe;
        EditText nominalDisetor;
        Button btnUpload;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.tes6, viewGroup, false);

            holder.settlementKe = (EditText) view.findViewById(R.id.etSettlementKe);
            holder.nominalDisetor = (EditText) view.findViewById(R.id.etNominalDisetor);
            holder.btnUpload = (Button) view.findViewById(R.id.btnUpload);


            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

       n=holder.nominalDisetor.getText().toString();
        if(mDataFound!=null){
            mDataFound.getData(holder.nominalDisetor.getText().toString());
        }
        return view;
    }

    public String getNilai(){
       return n;
    }


    public interface DataFound{
        public void getData(String nominal);
    }
}
