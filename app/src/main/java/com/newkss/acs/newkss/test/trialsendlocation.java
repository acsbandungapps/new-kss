package com.newkss.acs.newkss.test;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.crashlytics.android.Crashlytics;
import com.newkss.acs.newkss.ParentActivity;
import com.newkss.acs.newkss.R;
import com.newkss.acs.newkss.Util.Function;

import io.fabric.sdk.android.Fabric;
import java.util.Calendar;

/**
 * Created by acs on 7/9/17.
 */

public class trialsendlocation extends ParentActivity {

    Button btnStart,btnCancel;
    Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.service);
        mContext=this;
        btnStart=(Button) findViewById(R.id.btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Calendar cal = Calendar.getInstance();
//                cal.setTimeInMillis(System.currentTimeMillis());
//                cal.add(Calendar.SECOND, 1);
//
//
//                AlarmManager alarmManager=(AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
//                Intent intent = new Intent(mContext, AlarmReceiver.class);
//                PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);
//                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,cal.getTimeInMillis(),1000,
//                        pendingIntent);

                Function.triggerAlarm(mContext);


            }
        });
        btnCancel=(Button) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlarmManager alarmManager=(AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(mContext, AlarmReceiver.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);
                alarmManager.cancel(pendingIntent);
            }
        });

        //startService(new Intent(mContext,contohservice.class));

    }
}
